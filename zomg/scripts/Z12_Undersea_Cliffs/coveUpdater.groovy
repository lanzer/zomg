//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def coveUpdater = "coveUpdater"
myRooms.Ledge_305.createTriggerZone(coveUpdater, 550, 410, 730, 510)
myManager.onTriggerIn(myRooms.Ledge_305, coveUpdater) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(234)) {
		event.actor.updateQuest(234, "Story-VQS")
	}
}