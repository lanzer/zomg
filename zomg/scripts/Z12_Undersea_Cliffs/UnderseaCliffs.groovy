import com.gaiaonline.mmo.battle.script.*;

MinML = 8.0
ML = 8.0 
MaxML = 10.0
SubCommander = null
subCommanderLevel = ML
subCommanderSpawned = false
SUB_COMMANDER_LIMIT = 20

spawnerList = [] as Set
activeSpawners = new HashMap()

//------------------------------------------
// Brain Clam Spawners (Pre-Rubble)         
//------------------------------------------

brain1 = myRooms.Ledge_201.spawnStoppedSpawner( "brain1", "brain_clam", 3 )
brain1.setPos( 350, 420 )
brain1.setWanderBehaviorForChildren( 50, 150, 2, 6, 200)
brain1.setMonsterLevelForChildren( ML )

brain2 = myRooms.Ledge_201.spawnStoppedSpawner( "brain2", "brain_clam", 3 )
brain2.setPos( 690, 205 )
brain2.setWanderBehaviorForChildren( 50, 150, 2, 6, 200)
brain2.setMonsterLevelForChildren( ML )

brain3 = myRooms.Ledge_302.spawnStoppedSpawner( "brain3", "brain_clam", 3 )
brain3.setPos( 250, 310 )
brain3.setWanderBehaviorForChildren( 50, 150, 2, 6, 200)
brain3.setMonsterLevelForChildren( ML )

brain4 = myRooms.Ledge_302.spawnStoppedSpawner( "brain4", "brain_clam", 3 )
brain4.setPos( 630, 420 )
brain4.setWanderBehaviorForChildren( 50, 150, 2, 6, 200)
brain4.setMonsterLevelForChildren( ML )

tunnelAmbush = myRooms.Ledge_203.spawnStoppedSpawner( "tunnelAmbush", "brain_clam", 3 )
tunnelAmbush.setPos( 490, 270 )
tunnelAmbush.setWanderBehaviorForChildren( 50, 100, 2, 6, 150)
tunnelAmbush.setMonsterLevelForChildren( ML )

barnacle1 = myRooms.Ledge_202.spawnStoppedSpawner( "barnacle1", "barnacle_fluff", 1 )
barnacle1.setPos( 430, 340 )
barnacle1.setHome( barnacle1 )
barnacle1.setGuardPostForChildren( barnacle1, 225 )
barnacle1.setMonsterLevelForChildren( ML )

barnacle2 = myRooms.Ledge_102.spawnStoppedSpawner( "barnacle2", "barnacle_fluff", 1 )
barnacle2.setPos( 400, 520 )
barnacle2.setHome( barnacle2 )
barnacle2.setGuardPostForChildren( barnacle2, 45 )
barnacle2.setMonsterLevelForChildren( ML )

barnacle3 = myRooms.Ledge_302.spawnStoppedSpawner( "barnacle3", "barnacle_fluff", 1 )
barnacle3.setPos( 760, 170 )
barnacle3.setHome( barnacle3 )
barnacle3.setGuardPostForChildren( barnacle3, 135 )
barnacle3.setMonsterLevelForChildren( ML )

spawnerList << brain1
spawnerList << brain2
spawnerList << brain3
spawnerList << brain4
spawnerList << tunnelAmbush
spawnerList << barnacle1
spawnerList << barnacle2
spawnerList << barnacle3

//------------------------------------------
// Rubble Guard (runs to cliff, disappears) 
//------------------------------------------

ledgeReserves = myRooms.Ledge_105.spawnStoppedSpawner( "ledgeReserves", "grunny_sub", 3 )
ledgeReserves.setPos( 380, 485 )
ledgeReserves.setWanderBehaviorForChildren( 75, 150, 3, 7, 300)
ledgeReserves.setMonsterLevelForChildren( ML )

subRunner = myRooms.Ledge_304.spawnStoppedSpawner( "subRunner", "grunny_sub", 3 )
subRunner.setPos( 410, 300 )
subRunner.setCFH( 500 )
subRunner.setMonsterLevelForChildren( ML )
subRunner.setBaseSpeed( 160 )
subRunner.setChildrenToFollow( subRunner )
subRunner.addPatrolPointForSpawner( "Ledge_204", 740, 560, 0)
subRunner.addPatrolPointForSpawner( "Ledge_205", 180, 300, 0)
subRunner.addPatrolPointForSpawner( "Ledge_105", 300, 480, 5)
subRunner.addPatrolPointForSpawner( "Ledge_104", 860, 590, 0)
subRunner.addPatrolPointForSpawner( "Ledge_204", 390, 460, 0)
subRunner.addPatrolPointForSpawner( "Ledge_304", 410, 280, 5)
subRunner.startPatrol()

spawnerList << ledgeReserves
spawnerList << subRunner

//ALLIANCES
subRunner.allyWithSpawner( ledgeReserves )

//------------------------------------------
// Mid-zone Nests                           
//------------------------------------------

anchor1 = myRooms.Ledge_303.spawnStoppedSpawner( "anchor1", "brain_clam", 6 )
anchor1.setPos( 310, 420 )
anchor1.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
anchor1.setMonsterLevelForChildren( ML )

anchor2 = myRooms.Ledge_204.spawnStoppedSpawner( "anchor2", "sea_anchor_bug", 3 )
anchor2.setPos( 805, 515 )
anchor2.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
anchor2.setMonsterLevelForChildren( ML )

anchor3 = myRooms.Ledge_304.spawnStoppedSpawner( "anchor3", "sea_anchor_bug", 4 )
anchor3.setPos( 600, 210 )
anchor3.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
anchor3.setMonsterLevelForChildren( ML )

anchor4 = myRooms.Ledge_205.spawnStoppedSpawner( "anchor4", "sea_anchor_bug", 4 )
anchor4.setPos( 130, 560 )
anchor4.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
anchor4.setMonsterLevelForChildren( ML )

anchor5 = myRooms.Ledge_305.spawnStoppedSpawner( "anchor5", "sea_anchor_bug", 7 )
anchor5.setPos( 420, 340 )
anchor5.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
anchor5.setMonsterLevelForChildren( ML )

spawnerList << anchor1
spawnerList << anchor2
spawnerList << anchor3
spawnerList << anchor4
spawnerList << anchor5

//ALLIANCES
// Anchors ally
anchor2.allyWithSpawner( anchor3 )
anchor2.allyWithSpawner( anchor4 )
anchor2.allyWithSpawner( anchor5 )
anchor3.allyWithSpawner( anchor4 )
anchor3.allyWithSpawner( anchor5 )
anchor4.allyWithSpawner( anchor5 )

//------------------------------------------
// The Cave Approach & Patrollers           
//------------------------------------------

guard1 = myRooms.Ledge_207.spawnStoppedSpawner( "guard1", "grunny_sub", 1 )
guard1.setPos( 775, 470 )
guard1.setGuardPostForChildren( guard1, 225 )
guard1.setCFH( 500 )
guard1.setMonsterLevelForChildren( ML )

guard2 = myRooms.Ledge_207.spawnStoppedSpawner( "guard2", "grunny_sub", 1 )
guard2.setPos( 230, 470 )
guard2.setGuardPostForChildren( guard2, 315 )
guard2.setCFH( 500 )
guard2.setMonsterLevelForChildren( ML )

patrollers = myRooms.Ledge_207.spawnStoppedSpawner( "patrollers", "grunny_sub", 4 )
patrollers.setPos( 500, 200 )
patrollers.setCFH( 500 )
patrollers.setMonsterLevelForChildren( ML )
patrollers.setBaseSpeed( 160 )
patrollers.setChildrenToFollow( patrollers )
patrollers.addPatrolPointForSpawner( "Ledge_207", 510, 200, 0 )
patrollers.addPatrolPointForSpawner( "Ledge_307", 400, 130, 0 )
patrollers.addPatrolPointForSpawner( "Ledge_307", 200, 325, 0 )
patrollers.addPatrolPointForSpawner( "Ledge_306", 500, 590, 0 )
patrollers.addPatrolPointForSpawner( "Ledge_406", 430, 170, 5 )
patrollers.addPatrolPointForSpawner( "Ledge_306", 500, 590, 0 )
patrollers.addPatrolPointForSpawner( "Ledge_307", 200, 325, 0 )
patrollers.addPatrolPointForSpawner( "Ledge_307", 400, 130, 0 )
patrollers.addPatrolPointForSpawner( "Ledge_207", 510, 200, 0 )
patrollers.addPatrolPointForSpawner( "Ledge_107", 530, 250, 5 )
patrollers.startPatrol()

spawnerList << guard1
spawnerList << guard2
spawnerList << patrollers

//------------------------------------------
// Cave Guards                              
//------------------------------------------

caveReserves = myRooms.Ledge_406.spawnStoppedSpawner( "caveReserves", "grunny_sub", 4 )
caveReserves.setPos( 480, 230 )
caveReserves.setWanderBehaviorForChildren( 75, 150, 3, 7, 200)
caveReserves.setMonsterLevelForChildren( ML )

caveReserveLT = myRooms.Ledge_406.spawnStoppedSpawner( "caveReserveLT", "grunny_sub_LT", 1 )
caveReserveLT.setPos( 200, 230 )
caveReserveLT.setWanderBehaviorForChildren( 50, 100, 3, 7, 150)
caveReserveLT.setMonsterLevelForChildren( subCommanderLevel )

caveGuardRunner = myRooms.Ledge_306.spawnStoppedSpawner( "caveGuardRunner", "grunny_sub", 1 )
caveGuardRunner.setPos( 340, 500 )
caveGuardRunner.setHome( caveReserves )
caveGuardRunner.setGuardPostForChildren( caveGuardRunner, 45 )
caveGuardRunner.setCFH( 500 )
caveGuardRunner.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
caveGuardRunner.setCowardLevelForChildren( 100 )
caveGuardRunner.setMonsterLevelForChildren( ML )

caveGuardFighter = myRooms.Ledge_306.spawnStoppedSpawner( "caveGuardFighter", "grunny_sub", 1 )
caveGuardFighter.setPos( 595, 470 )
caveGuardFighter.setCFH( 500 )
caveGuardFighter.setGuardPostForChildren( caveGuardFighter, 45 )
caveGuardFighter.setMonsterLevelForChildren( ML )

spawnerList << caveReserves
spawnerList << caveReserveLT
spawnerList << caveGuardRunner
spawnerList << caveGuardFighter

//ALLIANCES
guard1.allyWithSpawner( guard2 )
guard1.allyWithSpawner( patrollers )
guard2.allyWithSpawner( patrollers )
caveGuardRunner.allyWithSpawner( caveGuardFighter )
caveGuardRunner.allyWithSpawner( caveReserves )
caveGuardFighter.allyWithSpawner( caveReserves )
caveReserveLT.allyWithSpawner( caveReserves )
caveReserveLT.allyWithSpawner( caveGuardRunner )

//==========================================
// VARIABLE INITIALIZATIONS                 
//==========================================
initialSpawnCompleted = false
marshallFollowing = false
marshallGone = false
cliffAreaSpawned = false
gambinoTowerDialogDone = false
gotGrunnySafeKey = false

difficulty = null
Marshall = null

zone = 11 //Shallow Sea Series

playerSet = [] as Set
checkSet = [] as Set
playerSet301 = [] as Set
playerSetAround301 = [] as Set
playerSet6 = [] as Set
playerSetSafe406 = [] as Set
playerSetCrate302a = [] as Set
playerSetCrate302b = [] as Set
playerSetChest305 = [] as Set
playerSetChest6 = [] as Set

onEnterBlock301 = new Object()
onEnterBlock6 = new Object()

//===============================
// ADJUST CL ROUTINE             
//===============================
def adjustML() {
	ML = MinML

	playerSet.clear()
	myRooms.Ledge_6.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_102.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_103.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_104.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_105.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_107.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_201.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_202.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_203.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_204.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_205.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_207.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_301.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_302.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_303.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_304.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_305.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_306.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_307.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Ledge_406.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	
	if( playerSet.isEmpty() == true)
	{
		return;
	}
	
	//adjust the encounter ML to the group
	random( playerSet ).getCrew().clone().each{		
		if( it.getConLevel() > ML && it.getConLevel() <= MaxML ) {
			ML = it.getConLevel()
		} else if( it.getConLevel() > MaxML ) {
			ML = MaxML
		}
	}

	//kick players out if they are cheating
	playerSet.clone().each{
		if( it.getConLevel() > MaxML ) {
			it.centerPrint( "Ah, ah, ah! Your level is too high for this scenario! Out you go!" )
			it.warp( "Beach_2", 720, 280 )
		}
	}
	
	//DIFFICULTY
	if( difficulty == 1 ) { ML = ML - 0.6 }
	if( difficulty == 3 ) { ML = ML + 0.6 }
	
	getMonsterTotal()
	
	if(monsterCounter > SUB_COMMANDER_LIMIT) {
		subCommanderLevel = ML + 10
	} else {
		subCommanderLevel = ML
	}

	//Now update all the spawners with the new ML so they start creating monsters at that new level
	brain1.setMonsterLevelForEveryone( ML )
	brain2.setMonsterLevelForEveryone( ML )
	brain3.setMonsterLevelForEveryone( ML )
	brain4.setMonsterLevelForEveryone( ML )
	tunnelAmbush.setMonsterLevelForEveryone( ML )
	barnacle1.setMonsterLevelForEveryone( ML )
	barnacle2.setMonsterLevelForEveryone( ML )
	barnacle3.setMonsterLevelForEveryone( ML )
	ledgeReserves.setMonsterLevelForEveryone( ML )
	subRunner.setMonsterLevelForEveryone( ML )
	anchor1.setMonsterLevelForEveryone( ML )
	anchor2.setMonsterLevelForEveryone( ML )
	anchor3.setMonsterLevelForEveryone( ML )
	anchor4.setMonsterLevelForEveryone( ML )
	anchor5.setMonsterLevelForEveryone( ML )
	guard1.setMonsterLevelForEveryone( ML )
	guard2.setMonsterLevelForEveryone( ML )
	patrollers.setMonsterLevelForEveryone( ML )
	caveReserves.setMonsterLevelForEveryone( ML )
	caveReserveLT.setMonsterLevelForEveryone( subCommanderLevel )
	caveGuardRunner.setMonsterLevelForEveryone( ML )
	caveGuardFighter.setMonsterLevelForEveryone( ML )
}

//==========================================
// ENTERING LEDGE FROM SHALLOW SEA          
//==========================================

myManager.onEnter( myRooms.Ledge_301) { event ->
	synchronized( onEnterBlock301 ) {
		if( isPlayer( event.actor ) ) {
			playerSet301 << event.actor
			if( Marshall == null && event.actor.hasQuestFlag( GLOBAL, "Z11MarshallFollowing" ) ) {
				Marshall = spawnNPC("Marshall-VQS", myRooms.Ledge_301, 585, 595)
				Marshall.setDisplayName( "Marshall" )
				Marshall.setVulnerableToMonsters( false )
				Marshall.startFollow( event.actor )
				marshallFollowing = true
				playerToFollow = event.actor
				myManager.schedule(1) { checkToSwitch() } //NOTE: this routine is all the way at the bottom of the script
				myManager.schedule(4) { activateMarshallLedgeIntro() }
			}
			//DIFFICULTY
			//The first player into the scenario sets the difficulty for everyone else
			if( difficulty == null ) {
				difficulty = event.actor.getTeam().getAreaVar( "Z11_Shallow_Sea", "Z11ShallowSeaDifficulty" )
				if( difficulty == 0 ) { difficulty = 1; println "**** difficulty changed to 1 ****" }
			}			
			//When the first actor enters the Ledge, check all crew members and set the scenario's level to the highest player's CL
			if( initialSpawnCompleted == false ) {
				initialSpawnCompleted = true
				adjustML()
				spawnApronArea()
			}
		}
	}
}

myManager.onExit( myRooms.Ledge_301 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet301.remove( event.actor )
		if( event.actor.hasQuestFlag( GLOBAL, "Z11MarshallFollowing" ) ) {
			playerSetAround301.clear()
			myRooms.Ledge_201.getActorList().each { if( isPlayer( it ) ) { playerSetAround301 << it } }
			myRooms.Ledge_302.getActorList().each { if( isPlayer( it ) ) { playerSetAround301 << it } }
			//if the player isn't in either of those rooms, then he zoned back into Shallow Sea and we need to deal with Marshall's behavior
			if( !playerSetAround301.contains( event.actor ) ) {
				Marshall.dispose()
				marshallFollowing = false //this is used in the follow-switching routine only used in the front area of this zone...turning it off once Marshall reaches the Overlook.
				Marshall = null
			}
		}
	}
}

//---------------------------------------------
// ENTRY INTO THE LEDGE SPEECH                 
//---------------------------------------------

def activateMarshallLedgeIntro( event ) {
	if(null == Marshall)
	{
		return;
	}
	
	zoneBroadcast("Marshall-VQS", "Sub-Commander Surprise", "The Sub-Commander can draw energy from its minions to power its engines. Thin their numbers to prevent it becoming too powerful.")
	
	def ledgeIntro = Marshall.createConversation( "ledgeIntro", true, "!Z12IntroDialogDone" )
	
	def intro1 = [id:1]
	intro1.npctext = "I've only been down here a few times, but it's pretty hostile."
	intro1.playertext = "What did you see here?"
	intro1.result = 2
	ledgeIntro.addDialog( intro1, Marshall )
	
	def intro2 = [id:2]
	intro2.npctext = "More hostile Animated and some sub-like constructs, from which I've kept my distance, and the remnants of the Gambino Tower that fell when Gambino got shot. And...something else you'll have to see to believe."
	intro2.playertext = "Oh?"
	intro2.result = 3
	ledgeIntro.addDialog( intro2, Marshall )
	
	def intro3 = [id:3]
	intro3.npctext = "Yes. I'm certain it's the source of the lights everyone's seeing on the surface of the water above."
	intro3.playertext = "What is it?"
	intro3.result = 4
	ledgeIntro.addDialog( intro3, Marshall )
	
	def intro4 = [id:4]
	intro4.npctext = "You'll have to wait and see. Trust me."
	intro4.playertext = "Okay, I guess I don't have much of a choice. Let's move."
	intro4.flag = "Z12IntroDialogDone"
	intro4.result = DONE
	ledgeIntro.addDialog( intro4, Marshall )
	
	playerSet301.clone().each { 
		Marshall.pushDialog( it, "ledgeIntro" )
	}
}

//==========================================
// INITIAL APRON AREA SPAWN                 
//==========================================

def spawnApronArea() {
	brain1.spawnAllNow()
	brain2.spawnAllNow()
	brain3.spawnAllNow()
	brain4.spawnAllNow()
	tunnelAmbush.spawnAllNow()
	barnacle1.forceSpawnNow()
	barnacle2.forceSpawnNow()
	barnacle3.forceSpawnNow()

	ledgeReserves.spawnAllNow()
	subRunner.spawnAllNow()
	anchor1.spawnAllNow()
	anchor2.spawnAllNow()
	anchor3.spawnAllNow()
	anchor4.spawnAllNow()
	anchor5.spawnAllNow()
}

//---------------------------------------------
// GAMBINO TOWER DIALOG                        
//---------------------------------------------

def gambinoStory = "gambinoStory"
myRooms.Ledge_102.createTriggerZone(gambinoStory, 500, 440, 630, 655)

myManager.onTriggerIn(myRooms.Ledge_102, gambinoStory) { event ->
	if( event.actor == Marshall && gambinoTowerDialogDone == false ) {
		gambinoTowerDialogDone = true
		Marshall.say( "Do you know what you're seeing here?" )
		myManager.schedule(4) { Marshall.say( "It's a piece of the Gambino Tower that fell a few years ago." ) }
		myManager.schedule(8) { Marshall.say( "In many ways, it was the start of the entire mess that we're dealing with now." ) }
		myManager.schedule(12) { Marshall.say( "And it's almost certainly part of why everything seems centered here, in this area of the Shallow Sea." ) }
	}
}

//---------------------------------------------
// OVERVIEW TRIGGER ZONE (Make Grunny Go Away) 
//---------------------------------------------

def overCliff = "overCliff"
myRooms.Ledge_105.createTriggerZone(overCliff, 640, 10, 870, 80)

myManager.onTriggerIn(myRooms.Ledge_105, overCliff) { event ->
	if( isMonster(event.actor) && event.actor.getMonsterType() == "grunny_sub" ) {
		event.actor.dispose()
	}
	if( isPlayer( event.actor ) && event.actor.hasQuestFlag( GLOBAL, "Z11MarshallFollowing" ) ) {
		Marshall.dispose()
	}
}


//==========================================
// ENTRANCE TO OVERLOOK LEDGE (Ledge_6)     
//==========================================
myManager.onEnter( myRooms.Ledge_6) { event ->
	synchronized( onEnterBlock6 ) {
		if( isPlayer( event.actor ) ) {
			playerSet6 << event.actor
			if( isPlayer( event.actor ) && marshallGone == false && event.actor.hasQuestFlag( GLOBAL, "Z11MarshallFollowing" ) ) {
				Marshall = spawnNPC("Marshall-VQS", myRooms.Ledge_6, 940, 1900)
				Marshall.setDisplayName( "Marshall" )
				Marshall.setVulnerableToMonsters( false )
				Marshall.startFollow( event.actor )
				marshallFollowing = false //this is used in the follow-switching routine only used in the front area of this zone...turning it off once Marshall reaches the Overlook.
				myManager.schedule(1) { Marshall.pushDialog( event.actor, "sealabConvo" ) }
			}
			if( cliffAreaSpawned == false ) {
				cliffAreaSpawned = true
				adjustML()
				spawnCliffArea() //spawn the area from Overlook Ledge to the Undersea Caves entrance (mostly grunny subs)
			}
		}
		if( event.actor == Marshall && marshallGone == false ) {
			myManager.schedule(3) { activateMarshallSealabXDialog() }
		}
	}
}

myManager.onExit( myRooms.Ledge_6 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet6.remove( event.actor )
	}
}

//---------------------------------------------
// SEALAB OVERVIEW DIALOG                      
//---------------------------------------------

def activateMarshallSealabXDialog() {
	
	def sealabConvo = Marshall.createConversation( "sealabConvo", true, "!Z12SealabConvoDone" )
	
	def sealab1 = [id:1]
	sealab1.npctext = "Do you see now why I didn't try to explain it to you, wanting you to see it for yourself instead?"
	sealab1.playertext = "What the heck IS all that?"
	sealab1.result = 2
	sealabConvo.addDialog( sealab1, Marshall )
	
	def sealab2 = [id:2]
	sealab2.npctext = "I don't know. I was in this area about a year ago, scouting the depths here for unusual wildlife, and I didn't see *any* of this here."
	sealab2.playertext = "Do you think this is made by the Animated?!?"
	sealab2.result = 3
	sealabConvo.addDialog( sealab2, Marshall )
	
	def sealab3 = [id:3]
	sealab3.npctext = "No. Or at least, I don't *think* so. The Grunny Subs make me very, very suspicious that the Gambinos and G-Corp are behind all of this."
	sealab3.playertext = "Why is that?"
	sealab3.result = 4
	sealabConvo.addDialog( sealab3, Marshall )
	
	def sealab4 = [id:4]
	sealab4.npctext = "Well...lots of folks missed details like this, but the Grunnies, G-Virus and the Gambinos seem to be really tightly tied together."
	sealab4.result = 5
	sealabConvo.addDialog( sealab4, Marshall ) 
	
	def sealab5 = [id:5]
	sealab5.npctext = "When the towns were getting overrun by zombies a few years back, the news reported that it was experiments with a 'G-Virus', created by G-Corp, that somehow caused the zombies to be created."
	sealab5.playertext = "Okay...but what does that have to do with Grunnies?"
	sealab5.result = 6
	sealabConvo.addDialog( sealab5, Marshall )
	
	def sealab6 = [id:6]
	sealab6.npctext = "I'm not sure...but the number of times that Grunnies have been mentioned in the same articles as 'G-Virus' makes me think there's some sort of connection."
	sealab6.playertext = "So, if that's the case, then what do you think the Gambinos are *doing* down there?"
	sealab6.result = 7
	sealabConvo.addDialog( sealab6, Marshall )
	
	def sealab7 = [id:7]
	sealab7.npctext = "I have no idea. In fact, this is as far as I go with you. If you keep moving down this ledge, there are grunny guards and other...things...that are too dangerous for me to get by."
	sealab7.playertext = "Yeah. I getcha. Without any rings, I wouldn't have even gone as far as you've gone so far. You're a brave man, Marshall. Go home and take care of Jesse."
	sealab7.result = 8
	sealabConvo.addDialog( sealab7, Marshall )
	
	marshallReward = ["17806", "17810"] //antique diving gear & harpoon gun
	
	def sealab8 = [id:8]
	sealab8.npctext = "Perhaps. There are a few more things I'd like to check out here first...but I wish you the best of luck. Whatever is down there is going to be challenging, I'm sure. Be careful."
	sealab8.playertext = "See ya, Marshall."
	sealab8.flag = "Z12SealabConvoDone"
	sealab8.exec = { event ->
		event.player.centerPrint( "Here. Take these gadget blueprints as thanks for saving me from those Sirens. Thanks again!" )
		if( !event.player.hasQuestFlag( GLOBAL, "Z12MarshallAwardGiven" ) ) {
			event.player.setQuestFlag( GLOBAL, "Z12MarshallAwardGiven" ) 
			event.player.grantItem( random( marshallReward ) )
		}
		event.player.unsetQuestFlag( GLOBAL, "Z11MarshallFollowing" )
		Marshall.pause()
		marshallGoAway()
	}
	sealab8.result = DONE
	sealabConvo.addDialog( sealab8, Marshall )
	
	
	playerSet6.clone().each { 
		Marshall.pushDialog( it, "sealabConvo" )
	}
}


def marshallGoAway() {
	if( playerSet6.isEmpty() == true ) {
		marshallGone = true
		Marshall.dispose()
	} else {
		myManager.schedule(2) { marshallGoAway() }
	}
}


//==========================================
// CLIFF AREA SPAWN                         
//==========================================
def spawnCliffArea() {
	guard1.spawnAllNow()
	guard2.spawnAllNow()
	patrollers.spawnAllNow()

	caveReserves.spawnAllNow()
	caveGuardRunner.spawnAllNow()
	caveGuardFighter.spawnAllNow()
}

//---------------------------------------------
// THE GRUNNY SAFE (DAVY JONES' LOCKER)        
//---------------------------------------------
spawners406 = [caveReserves, caveReserveLT]

myManager.onEnter(myRooms.Ledge_406) { event ->
	if(isPlayer(event.actor)) { adjustML() }
	if(caveReserveLT.spawnsInUse() == 0 && isPlayer(event.actor)) {
		getMonsterTotal()
		
		if(subCommanderSpawned == false) {
			subCommanderSpawned = true
			SubCommander = caveReserveLT.forceSpawnNow()
			SubCommander.setDisplayName( "Sub-Commander" )
			
			if(monsterCounter > SUB_COMMANDER_LIMIT) {
				SubCommander.setDisplayName("Super-Sub-Commander")
				SubCommander.shout("Subordinates active, power alignment successful. Engaging hyperdrive!")
			} else {
				SubCommander.shout("Power alignment failed. Normal systems active.")
			}

			runOnDeath(SubCommander) {
				removeZoneBroadcast("Sub-Commander Surprise")
				// Watch for the Sub-Commander to die, notify the player that they got the transponder, and then allow the gate to be opened via switch code, activating the trigger zone
				runOnDeath( SubCommander, { deathEvent -> safe406.unlock(); deathEvent.killer.say( "That Grunny had a passkey with a numeric combination on it..." ) } )
			}
		}
	}
}

safe406 = makeGeneric( "safe406", myRooms.Ledge_406, 130, 345 )
safe406.setStateful() //locked until the SubCommander dies, and then it can be opened.
safe406.setUsable(true)
safe406.setRange(200)

safe406.onUse() { event ->
	if(safe406.getActorState() == 0) {
		spawnerCount406 = 0
		spawners406.clone().each() {
			spawnerCount406 = spawnerCount406 + it.spawnsInUse()
		}
		
		if(spawnerCount406 == 0) {
			safe406.setActorState(1)
			event.player.centerPrint( "You try the combination out on the safe. It opens!" )
			myManager.schedule(3) { event.player.centerPrint( "Inside, you find a strange device that emits a steady, low humming sound." ) }
			gotGrunnySafeKey = true
			myManager.schedule(6) {

				//find all the players in the room
				playerSetSafe406.clear()
				myRooms.Ledge_406.getActorList().each { if( isPlayer( it ) ) { playerSetSafe406 << it } }

				//now remove any players from that Set that are not in the opening player's crew
				playerSetSafe406.clone().each{ if( !event.player.getCrew().contains( it ) ) { playerSetSafe406.remove( it ) } }

				//now give loot to everyone that's left in that list (members of the opener's crew that are also in the same room with the container)
				playerSetSafe406.each{
					if( it.getConLevel() <= CLMap[zone]+1 ) {
						goldMult = 0; orbMult = 0; lootMultiplier = 0;
						chestType = 3 //1 = basket; 2 = crate; 3 = chest; 4 = safe

						//scale the returned results down by relative con levels if the player is below the normal CL for the area
						if( it.getConLevel() > CLMap[zone] ) {
							lootMultiplier = 1.0
						} else {
							lootMultiplier = it.getConLevel() / CLMap[zone] //the lower the overall CL of a player in relation to the zone CL, the lower the reward they receive (always receive a reward appropriate to their level).
						}

						//DIFFICULTY: set multipliers. They are set so that the min of one difficulty is higher than the max of the difficulty below it
						if( difficulty == 1 ) { goldMult = 0.75; orbMult = 0.75 }
						else if( difficulty == 2 ) { goldMult = 3; orbMult = 2 }
						else if( difficulty == 3 ) { goldMult = 8; orbMult = 4 }

						//grant gold
						goldGrant = goldMap[zone] 
						goldGrant = random( (goldGrant * 0.5).intValue(), goldGrant ) 
						it.grantCoins( ( goldGrant * lootMultiplier * chestType * goldMult ).intValue() ) 

						//grant common items
						chance = (commonChance * chestType).intValue()
						roll = random( 100 )
						if( roll <= chance ) {
							commonGrant = random( commonMap[zone] )
							it.grantItem( commonGrant )
						}

						//grant uncommon items
						chance = (uncommonChance * chestType).intValue()
						roll = random( 100 )
						if( roll <= chance ) {
							uncommonGrant = random( uncommonMap[zone] )
							it.grantItem( uncommonGrant )
						}

						//grant recipes
						chance = (recipeChance * chestType).intValue()
						roll = random( 100 )
						if( roll <= chance ) {
							recipeGrant = random( recipeMap[zone] )
							it.grantItem( recipeGrant )
						}

						//grant orbs
						chance = (orbChance * chestType).intValue()
						roll = random( 100 )
						if( roll <= chance ) {
							orbGrant = orbMap[zone]
							orbGrant = ( random( (orbGrant * 0.5).intValue(), orbGrant ) * lootMultiplier * chestType * orbMult ).intValue()
							it.grantQuantityItem( 100257, orbGrant )
						}

						//grant rings
						chance = (ringChance * chestType).intValue()
						roll = random( 1, 100 )
						if( roll <= chance ) {
							ringGrant = random( ringMap[zone] )
							it.grantRing( ringGrant, true )
						}
					} else {
						it.centerPrint( "Your level is too high for loot from this container. Change your level to below ${(CLMap[zone]+1).intValue() * 1.0} when fighting here." )
					}
				}
			}
		} else {
			playerSetSafe406.clear()
			myRooms.Ledge_406.getActorList().each { if( isPlayer( it ) ) { playerSetSafe406 << it } }
			playerSetSafe406.clone().each() {
				it.centerPrint("This chest is too closely guarded to be opened")
			}
		}
	}
}

// Cave Gate Trigger to take players to Undersea Caves

complexAreaVarSet = false

def caveGateTrigger = "caveGateTrigger"
myRooms.Ledge_306.createTriggerZone( caveGateTrigger, 315, 150, 580, 260 )

myManager.onTriggerIn(myRooms.Ledge_306, caveGateTrigger) { event ->
	if( isPlayer( event.actor ) && gotGrunnySafeKey == true ) {
		event.actor.centerPrint( "The depression in the stone slab in the back of the cavern accepts the device from the safe." )
		myManager.schedule(3) { event.actor.centerPrint( "The massive stone slab slides into the floor revealing a passage into the depths." ); event.actor.warp( "Sealab_1", 100, 505 ) }
	}
	if( isPlayer( event.actor ) && gotGrunnySafeKey == false ) {
		event.actor.centerPrint( "The cave seems artificially sealed by smoothed stone at the back." )
		myManager.schedule(3) { event.actor.centerPrint( "A depression in that stone wall looks like a receptacle for something." ) }
		myManager.schedule(6) { event.actor.centerPrint( "But you see no way to activate it." ) }
	}
}



//---------------------------------------------
// BRAIN CLAM TREASURE CHESTS                  
//---------------------------------------------
spawners302 = [brain3, brain4, barnacle3]

crate302a = makeGeneric( "crate302a", myRooms.Ledge_302, 810, 360 )
crate302a.setStateful()
crate302a.setUsable(true)
crate302a.setRange(150)

crate302a.onUse() { event ->
	if(crate302a.getActorState() == 0) {
		spawner302Count = 0
		spawners302.clone().each() {
			spawner302Count = spawner302Count + it.spawnsInUse()
		}
		
		if(spawner302Count == 0) {
			//lock the chest until after reset occurs
			crate302a.setActorState(1)
	
			//find all the players in the room
			playerSetCrate302a.clear()
			myRooms.Ledge_302.getActorList().each { if( isPlayer( it ) ) { playerSetCrate302a << it } }
	
			//now remove any players from that Set that are not in the opening player's crew
			playerSetCrate302a.clone().each{ if( !event.player.getCrew().contains( it ) ) { playerSetCrate302a.remove( it ) } }
	
			//now give loot to everyone that's left in that list (members of the opener's crew that are also in the same room with the container)
			playerSetCrate302a.each{
				if( it.getConLevel() <= CLMap[zone]+1 ) {
					goldMult = 0; orbMult = 0; lootMultiplier = 0;
					chestType = 2 //1 = basket; 2 = crate; 3 = chest; 4 = safe

					//scale the returned results down by relative con levels if the player is below the normal CL for the area
					if( it.getConLevel() > CLMap[zone] ) {
						lootMultiplier = 1.0
					} else {
						lootMultiplier = it.getConLevel() / CLMap[zone] //the lower the overall CL of a player in relation to the zone CL, the lower the reward they receive (always receive a reward appropriate to their level).
					}

					//DIFFICULTY: set multipliers. They are set so that the min of one difficulty is higher than the max of the difficulty below it
					if( difficulty == 1 ) { goldMult = 0.75; orbMult = 0.75 }
					else if( difficulty == 2 ) { goldMult = 3; orbMult = 2 }
					else if( difficulty == 3 ) { goldMult = 8; orbMult = 4 }

					//grant gold
					goldGrant = goldMap[zone] 
					goldGrant = random( (goldGrant * 0.5).intValue(), goldGrant ) 
					it.grantCoins( ( goldGrant * lootMultiplier * chestType * goldMult ).intValue() ) 

					//grant common items
					chance = (commonChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						commonGrant = random( commonMap[zone] )
						it.grantItem( commonGrant )
					}

					//grant uncommon items
					chance = (uncommonChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						uncommonGrant = random( uncommonMap[zone] )
						it.grantItem( uncommonGrant )
					}

					//grant recipes
					chance = (recipeChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						recipeGrant = random( recipeMap[zone] )
						it.grantItem( recipeGrant )
					}

					//grant orbs
					chance = (orbChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						orbGrant = orbMap[zone]
						orbGrant = ( random( (orbGrant * 0.5).intValue(), orbGrant ) * lootMultiplier * chestType * orbMult ).intValue()
						it.grantQuantityItem( 100257, orbGrant )
					}

					//grant rings
					chance = (ringChance * chestType).intValue()
					roll = random( 1, 100 )
					if( roll <= chance ) {
						ringGrant = random( ringMap[zone] )
						it.grantRing( ringGrant, true )
					}
				} else {
					it.centerPrint( "Your level is too high for loot from this container. Change your level to below ${(CLMap[zone]+1).intValue() * 1.0} when fighting here." )
				}
			}
		} else {
			playerSetCrate302a.clear()
			myRooms.Ledge_302.getActorList().each { if( isPlayer( it ) ) { playerSetCrate302a << it } }
			playerSetCrate302a.clone().each() {
				it.centerPrint("This chest is too closely guarded to be opened")
			}
		}
	}
}

//======crate302b======
crate302b = makeGeneric( "crate302b", myRooms.Ledge_302, 600, 215 )
crate302b.setStateful()
crate302b.setUsable(true)
crate302b.setRange(150)

crate302b.onUse() { event ->
	if(crate302b.getActorState() == 0) {
		spawner302Count = 0
		spawners302.clone().each() {
			spawner302Count = spawner302Count + it.spawnsInUse()
		}
		
		if(spawner302Count == 0) {
			//lock the chest until after reset occurs
			crate302b.setActorState(1)
	
			//find all the players in the room
			playerSetCrate302b.clear()
			myRooms.Ledge_302.getActorList().each { if( isPlayer( it ) ) { playerSetCrate302b << it } }
	
			//now remove any players from that Set that are not in the opening player's crew
			playerSetCrate302b.clone().each{ if( !event.player.getCrew().contains( it ) ) { playerSetCrate302b.remove( it ) } }
	
			//now give loot to everyone that's left in that list (members of the opener's crew that are also in the same room with the container)
			playerSetCrate302b.each{
				if( it.getConLevel() <= CLMap[zone]+1 ) {
					goldMult = 0; orbMult = 0; lootMultiplier = 0;
					chestType = 2 //1 = basket; 2 = crate; 3 = chest; 4 = safe

					//scale the returned results down by relative con levels if the player is below the normal CL for the area
					if( it.getConLevel() > CLMap[zone] ) {
						lootMultiplier = 1.0
					} else {
						lootMultiplier = it.getConLevel() / CLMap[zone] //the lower the overall CL of a player in relation to the zone CL, the lower the reward they receive (always receive a reward appropriate to their level).
					}

					//DIFFICULTY: set multipliers. They are set so that the min of one difficulty is higher than the max of the difficulty below it
					if( difficulty == 1 ) { goldMult = 0.75; orbMult = 0.75 }
					else if( difficulty == 2 ) { goldMult = 3; orbMult = 2 }
					else if( difficulty == 3 ) { goldMult = 8; orbMult = 4 }

					//grant gold
					goldGrant = goldMap[zone] 
					goldGrant = random( (goldGrant * 0.5).intValue(), goldGrant ) 
					it.grantCoins( ( goldGrant * lootMultiplier * chestType * goldMult ).intValue() ) 

					//grant common items
					chance = (commonChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						commonGrant = random( commonMap[zone] )
						it.grantItem( commonGrant )
					}

					//grant uncommon items
					chance = (uncommonChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						uncommonGrant = random( uncommonMap[zone] )
						it.grantItem( uncommonGrant )
					}

					//grant recipes
					chance = (recipeChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						recipeGrant = random( recipeMap[zone] )
						it.grantItem( recipeGrant )
					}

					//grant orbs
					chance = (orbChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						orbGrant = orbMap[zone]
						orbGrant = ( random( (orbGrant * 0.5).intValue(), orbGrant ) * lootMultiplier * chestType * orbMult ).intValue()
						it.grantQuantityItem( 100257, orbGrant )
					}

					//grant rings
					chance = (ringChance * chestType).intValue()
					roll = random( 1, 100 )
					if( roll <= chance ) {
						ringGrant = random( ringMap[zone] )
						it.grantRing( ringGrant, true )
					}
				} else {
					it.centerPrint( "Your level is too high for loot from this container. Change your level to below ${(CLMap[zone]+1).intValue() * 1.0} when fighting here." )
				}
			}
		} else {
			playerSetCrate302b.clear()
			myRooms.Ledge_302.getActorList().each { if( isPlayer( it ) ) { playerSetCrate302b << it } }
			playerSetCrate302b.clone().each() {
				it.centerPrint("This chest is too closely guarded to be opened")
			}
		}
	}
}
//---------------------------------------------
// ANCHOR BUG TREASURE CHEST                   
//---------------------------------------------
spawners305 = [anchor5]

chest305 = makeGeneric( "chest305", myRooms.Ledge_305, 650, 500 )
chest305.setStateful()
chest305.setUsable(true)
chest305.setRange(150)

chest305.onUse() { event ->
	if(chest305.getActorState() == 0) {
		spawner305Count = 0
		spawners305.clone().each() {
			spawner305Count = spawner305Count + it.spawnsInUse()
		}
		
		if(spawner305Count == 0) {
		//lock the chest until after reset occurs
			chest305.setActorState(1)
	
			//find all the players in the room
			playerSetChest305.clear()
			myRooms.Ledge_305.getActorList().each { if( isPlayer( it ) ) { playerSetChest305 << it } }
	
			//now remove any players from that Set that are not in the opening player's crew
			playerSetChest305.clone().each{ if( !event.player.getCrew().contains( it ) ) { playerSetChest305.remove( it ) } }
	
			//now give loot to everyone that's left in that list (members of the opener's crew that are also in the same room with the container)
			playerSetChest305.each{
				if( it.getConLevel() <= CLMap[zone]+1 ) {
					goldMult = 0; orbMult = 0; lootMultiplier = 0;
					chestType = 3 //1 = basket; 2 = crate; 3 = chest; 4 = safe

					//scale the returned results down by relative con levels if the player is below the normal CL for the area
					if( it.getConLevel() > CLMap[zone] ) {
						lootMultiplier = 1.0
					} else {
						lootMultiplier = it.getConLevel() / CLMap[zone] //the lower the overall CL of a player in relation to the zone CL, the lower the reward they receive (always receive a reward appropriate to their level).
					}

					//DIFFICULTY: set multipliers. They are set so that the min of one difficulty is higher than the max of the difficulty below it
					if( difficulty == 1 ) { goldMult = 0.75; orbMult = 0.75 }
					else if( difficulty == 2 ) { goldMult = 3; orbMult = 2 }
					else if( difficulty == 3 ) { goldMult = 8; orbMult = 4 }

					//grant gold
					goldGrant = goldMap[zone] 
					goldGrant = random( (goldGrant * 0.5).intValue(), goldGrant ) 
					it.grantCoins( ( goldGrant * lootMultiplier * chestType * goldMult ).intValue() ) 

					//grant common items
					chance = (commonChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						commonGrant = random( commonMap[zone] )
						it.grantItem( commonGrant )
					}

					//grant uncommon items
					chance = (uncommonChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						uncommonGrant = random( uncommonMap[zone] )
						it.grantItem( uncommonGrant )
					}

					//grant recipes
					chance = (recipeChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						recipeGrant = random( recipeMap[zone] )
						it.grantItem( recipeGrant )
					}

					//grant orbs
					chance = (orbChance * chestType).intValue()
					roll = random( 100 )
					if( roll <= chance ) {
						orbGrant = orbMap[zone]
						orbGrant = ( random( (orbGrant * 0.5).intValue(), orbGrant ) * lootMultiplier * chestType * orbMult ).intValue()
						it.grantQuantityItem( 100257, orbGrant )
					}

					//grant rings
					chance = (ringChance * chestType).intValue()
					roll = random( 1, 100 )
					if( roll <= chance ) {
						ringGrant = random( ringMap[zone] )
						it.grantRing( ringGrant, true )
					}
				} else {
					it.centerPrint( "Your level is too high for loot from this container. Change your level to below ${(CLMap[zone]+1).intValue() * 1.0} when fighting here." )
				}
			}
		} else {
			playerSetChest305.clear()
			myRooms.Ledge_305.getActorList().each { if( isPlayer( it ) ) { playerSetChest305 << it } }
			playerSetChest305.clone().each() {
				it.centerPrint("This chest is too closely guarded to be opened")
			}
		}
	}
}

//---------------------------------------------
// OVERVIEW LEDGE TREASURE CHEST               
//---------------------------------------------

chest6 = makeSwitch( "chest6", myRooms.Ledge_6, 830, 1445 )
chest6.unlock()
chest6.off()
chest6.setRange(200)

def open6 = { event ->
	//lock the chest until after reset occurs
	chest6.lock()
	
	//find all the players in the room
	playerSetChest6.clear()
	myRooms.Ledge_6.getActorList().each { if( isPlayer( it ) ) { playerSetChest6 << it } }
	
	//now remove any players from that Set that are not in the opening player's crew
	playerSetChest6.clone().each{ if( !event.actor.getCrew().contains( it ) ) { playerSetChest6.remove( it ) } }
	
	//now give loot to everyone that's left in that list (members of the opener's crew that are also in the same room with the container)
	playerSetChest6.each{
		if( it.getConLevel() <= CLMap[zone]+1 ) {
			goldMult = 0; orbMult = 0; lootMultiplier = 0;
			chestType = 3 //1 = basket; 2 = crate; 3 = chest; 4 = safe

			//scale the returned results down by relative con levels if the player is below the normal CL for the area
			if( it.getConLevel() > CLMap[zone] ) {
				lootMultiplier = 1.0
			} else {
				lootMultiplier = it.getConLevel() / CLMap[zone] //the lower the overall CL of a player in relation to the zone CL, the lower the reward they receive (always receive a reward appropriate to their level).
			}

			//DIFFICULTY: set multipliers. They are set so that the min of one difficulty is higher than the max of the difficulty below it
			if( difficulty == 1 ) { goldMult = 0.75; orbMult = 0.75 }
			else if( difficulty == 2 ) { goldMult = 3; orbMult = 2 }
			else if( difficulty == 3 ) { goldMult = 8; orbMult = 4 }

			//grant gold
			goldGrant = goldMap[zone] 
			goldGrant = random( (goldGrant * 0.5).intValue(), goldGrant ) 
			it.grantCoins( ( goldGrant * lootMultiplier * chestType * goldMult ).intValue() ) 

			//grant common items
			chance = (commonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				commonGrant = random( commonMap[zone] )
				it.grantItem( commonGrant )
			}

			//grant uncommon items
			chance = (uncommonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				uncommonGrant = random( uncommonMap[zone] )
				it.grantItem( uncommonGrant )
			}

			//grant recipes
			chance = (recipeChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				recipeGrant = random( recipeMap[zone] )
				it.grantItem( recipeGrant )
			}

			//grant orbs
			chance = (orbChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				orbGrant = orbMap[zone]
				orbGrant = ( random( (orbGrant * 0.5).intValue(), orbGrant ) * lootMultiplier * chestType * orbMult ).intValue()
				it.grantQuantityItem( 100257, orbGrant )
			}

			//grant rings
			chance = (ringChance * chestType).intValue()
			roll = random( 1, 100 )
			if( roll <= chance ) {
				ringGrant = random( ringMap[zone] )
				it.grantRing( ringGrant, true )
			}
		} else {
			it.centerPrint( "Your level is too high for loot from this container. Change your level to below ${(CLMap[zone]+1).intValue() * 1.0} when fighting here." )
		}
	}
}

chest6.whenOn( open6 )

//====================================================================================
//====================================================================================
commonChance = 20 
uncommonChance = 10 
recipeChance = 1
orbChance = 5
ringChance = 1 

CLMap = [ 2:1, 3:2, 4:2.5, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:10, 14:1, 16:8, 18:7 ]

goldMap = [ 2:10, 3:20, 4:30, 5:40, 6:50, 7:60, 8:70, 9:80, 10:90, 11:100, 14:10, 16:90, 18:80 ]

commonMap = [ 2:["100272", "100289", "100385", "100297", "100397"], 3:["100291", "100262", "100263", "100298"], 4:["100388", "100278", "100275", "100283"], 5:["100281", "100367", "100411", "100394", "100284", "100405", "100267"], 6:["100265", "100285", "100398", "100397", "100376", "100296"], 7:["100373", "100260"], 8:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 9:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 11:["100371", "100294", "100391", "100277", "100290", "100368", "100386", "100403"], 14:[0], 16:["100287", "100409"], 18:["100393", "100293", "100259", "100292"] ]

uncommonMap = [ 2:["100280", "100279", "100270", "100380", "100384"], 3:["100378", "100261", "100271"], 4:["100276", "100258", "100268"], 5:["100381", "100382", "100282", "100383"], 6:["100390", "100299", "100286", "100369", "100400", "100387"], 7:["100365", "100410"], 8:["100389", "100264", "100406", "100399", "100402"], 9:["100389", "100264", "100406", "100399", "100402"], 10:["100395", "100413", "100407", "100266"], 11:["100396", "100269", "100401", "100372", "100375", "100366", "100379", "100274"], 14:[0], 16:["100404", "100288"], 18:["100395", "100413", "100407", "100266"] ]

recipeMap = [ 2:["17766", "17764", "17772", "17758", "17756"], 3:["17861", "17857", "17755"], 4:["17848", "17849", "17852", "17851", "17850", "17753"], 5:["17833", "17831", "17754", "17836", "17835"], 6:["17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779"], 7:["17785", "17793", "17788", "17787"], 8:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 9:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 10:["17796", "17846", "17803", "17844"], 11:["17819", "17816", "17811", "17808", "17806", "17812", "17813", "17814", "17810", "17809", "17815"], 14:[0], 16:["17786", "17791", "17790", "17794"], 18:["17796", "17846", "17803", "17844"] ]

orbMap = [ 2:1, 3:2, 4:3, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:9, 14:1, 16:8, 18:7 ]

ringMap = [ 2:["17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 3:["17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 4:["17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 5:["17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 6:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 7:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 8:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 9:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 10:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 11:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 14:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 16:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 18:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"] ]

//====================================================================================
//====================================================================================

//======================================
// MARSHALL FOLLOW-SWITCHING ROUTINE    
//======================================

def checkToSwitch() {
	//If Marshall has already been disposed of, then bail out of the routine
	if( marshallFollowing == false ) return
	//check to see if the player Marshall is following is onLine or Dazed
	if( playerToFollow.isDazed() || !isOnline( playerToFollow ) ) {
		pickNewPlayerToFollow()
	//If not, pick a new player to follow.
	} else {
		myManager.schedule(1) { checkToSwitch() }
	}
}

def pickNewPlayerToFollow() {
	//now do adjustML() just to refresh the playerSet
	//(otherwise, if no checkpoints are being passed, the list won't ever change)
	adjustML()
	
	//clone the playerSet so we're not accidentally manipulating it
	checkSet = playerSet.clone()
	
	//don't go into an infinite loop if the list is empty and no one is in the zone
	if( checkSet.isEmpty() ) return 
	
	//we only want the players that are in the front area, before players reach Overlook Ledge
	myRooms.Ledge_6.getActorList().each { if( isPlayer( it ) ) { checkSet.remove(it) } }
	myRooms.Ledge_107.getActorList().each { if( isPlayer( it ) ) { checkSet.remove(it) } }
	myRooms.Ledge_207.getActorList().each { if( isPlayer( it ) ) { checkSet.remove(it) } }
	myRooms.Ledge_306.getActorList().each { if( isPlayer( it ) ) { checkSet.remove(it) } }
	myRooms.Ledge_307.getActorList().each { if( isPlayer( it ) ) { checkSet.remove(it) } }
	myRooms.Ledge_406.getActorList().each { if( isPlayer( it ) ) { checkSet.remove(it) } }
		
	//now check to see if everyone on the list is Dazed. If so, check again in one minute
	numDazed = 0
	
	if( !checkSet.isEmpty() ) {
		checkSet.each{
			if( it.isDazed() ) { numDazed ++ }
		}
	}
	
	//if everyone is dazed, then wait one second and try again until either someone is awake, or everyone leaves the zone
	if( numDazed >= checkSet.size() ) { 
		myManager.schedule(1) { pickNewPlayerToFollow() }
	//if someone is awake still, then pick an awake person (at random)
	} else {	
		//pick someone in the zone at random
		newPick = random( checkSet )
		//if the new pick is online and not dazed, then follow them
		if( isOnline( newPick ) && !newPick.isDazed() ) {
			playerToFollow = newPick
			Marshall.startFollow( newPick )
			//now that there's a new player to follow, start checking the status of that player instead
			myManager.schedule(1) { checkToSwitch() }
		//else start over again and find a different player
		} else {
			myManager.schedule(1) { pickNewPlayerToFollow() }
		}
	}
}

def getMonsterTotal() {
	monsterCounter = 0 //Reset the counter to get a fresh count
	spawnerList.clone().each { //Step through the spawner list
		if(it.spawnsInUse() > 0) {
			//activeSpawners.put(it, it.spawnsInUse())
			monsterCounter = monsterCounter + it.spawnsInUse() //Add the number of spawns to the current total
		}
	}
	//println "^^^^ monsterCounter = ${monsterCounter} ^^^^"
	//println "^^^^ activeSpawners = ${activeSpawners} ^^^^"
	//myManager.schedule(30) { getMonsterTotal() }
}