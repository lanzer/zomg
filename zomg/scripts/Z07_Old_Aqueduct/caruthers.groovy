import com.gaiaonline.mmo.battle.script.*;

def Caruthers = spawnNPC("Agent Caruthers-VQS", myRooms.Aqueduct_205, 510, 230 )
Caruthers.setDisplayName( "Concerned Citizen" )
Caruthers.setRotation( 135 )

onQuestStep( 97, 2 ) { event -> event.player.addMiniMapQuestActorName( "Agent Caruthers-VQS" ) }
onQuestStep( 98, 2 ) { event -> event.player.addMiniMapQuestActorName( "Agent Caruthers-VQS" ) }
onQuestStep( 99, 2 ) { event -> event.player.addMiniMapQuestActorName( "Agent Caruthers-VQS" ) }
onQuestStep( 100, 2 ) { event -> event.player.addMiniMapQuestActorName( "Agent Caruthers-VQS" ) }
onQuestStep( 36, 2 ) { event -> event.player.addMiniMapQuestActorName( "Agent Caruthers-VQS" ) }

myManager.onEnter( myRooms.Aqueduct_205 ) { event ->
	if( isPlayer( event.actor ) ) {
		if(event.actor.isOnQuest(330, 2)) {
			event.actor.setQuestFlag(GLOBAL, "Z07_Caruthers_FromLogan")
		}
		if( event.actor.getConLevel() >= 6.2 ) {
			event.actor.setQuestFlag( GLOBAL, "Z07TooOldForAqueductQuests" )
		} else {
			event.actor.unsetQuestFlag( GLOBAL, "Z07TooOldForAqueductQuests" )
		}
		event.actor.unsetQuestFlag( GLOBAL, "Z07RingRewardInProgress" )
	}
}

//---------------------------------------------------------
//Coming From Logan                                        
//---------------------------------------------------------
def fromLogan = Caruthers.createConversation("fromLogan", true, "QuestStarted_330", "Z07_Caruthers_FromLogan")

def fromLogan1 = [id:1]
fromLogan1.npctext = "What brings you here, citizen?"
fromLogan1.playertext = "Logan sent me to see you."
fromLogan1.result = 2
fromLogan.addDialog(fromLogan1, Caruthers)

def fromLogan2 = [id:2]
fromLogan2.npctext = "What business would the baitmaster have with me?"
fromLogan2.playertext = "Well, I've been trying to learn about the Animated and he said you're the guy to ask."
fromLogan2.result = 3
fromLogan.addDialog(fromLogan2, Caruthers)

def fromLogan3 = [id:3]
fromLogan3.npctext = "Me? What makes him think I would have answers."
fromLogan3.playertext = "Come on, don't be so obtuse. I just want to ask a few questions... and maybe I can help you out too."
fromLogan3.result = 4
fromLogan.addDialog(fromLogan3, Caruthers)

def fromLogan4 = [id:4]
fromLogan4.npctext = "Very well..."
fromLogan4.quest = 330
fromLogan4.exec = { event ->
	event.player.removeMiniMapQuestActorName("Agent Caruthers-VQS")
	event.player.unsetQuestFlag(GLOBAL, "Z07_Caruthers_FromLogan")
	Caruthers.pushDialog(event.player, "firstConvo")
}
fromLogan4.result = DONE
fromLogan.addDialog(fromLogan4, Caruthers)

//---------------------------------------------------------
// AGENT CARUTHERS' FIRST CONVERSATION                     
//---------------------------------------------------------

def firstConvo = Caruthers.createConversation( "firstConvo", true, "!QuestStarted_97", "!QuestCompleted_97", "!QuestStarted_330", "!Z07_Caruthers_FromLogan" )

first1 = [id:1]
first1.npctext = "What can I do for you today, citizen?"
first1.options = []
first1.options << [text: "What's going on around here?", result: 7]
first1.options << [text: "Are you one of those 'Gaians in Black'?", result: 2]
first1.options << [text: "Aren't you afraid these aliens are going to attack you?", result: 18]
first1.options << [text: "Ummm...nothing. Nevermind.", result: 17]
firstConvo.addDialog( first1, Caruthers )

first2 = [id:2]
first2.npctext = "'Gaians in Black'? Never heard of 'em. I'm just a concerned citizen, like yourself."
first2.playertext = "Really? Because your sunglasses seem to be a bit high-tech. They seem out of place for a mere 'concerned citizen'."
first2.result = 3
firstConvo.addDialog( first2, Caruthers )

first3 = [id:3]
first3.npctext = "These are not 'sunglasses'. The T-stroke-36 multi-spectral analyzers are standard issue when monitoring any previously unknown alien species."
first3.playertext = "Ummm....standard issue? To whom?"
first3.result = 4
firstConvo.addDialog( first3, Caruthers )

first4 = [id:4]
first4.npctext = "Why...to any concerned citizen deemed worthy of a security Red Alpha clearance, of course."
first4.playertext = "And who can get one of those clearances?"
first4.result = 5
firstConvo.addDialog( first4, Caruthers )

first5 = [id:5]
first5.npctext = "Only GIB agents can gain that clearance."
first5.playertext = "But doesn't that make you..."
first5.result = 6
firstConvo.addDialog( first5, Caruthers )

first6 = [id:6]
first6.npctext = "A concerned citizen, helping out in a pinch. What can I do for you?"
first6.options = []
first6.options << [text:"What's going on around here?", result: 7]
first6.options << [text:"Aren't you afraid those aliens are going to attack you?", result: 18]
first6.options << [text:"Nope. Nothing. Thanks anyway.", result: 17]
firstConvo.addDialog( first6, Caruthers )

first7 = [id:7]
first7.npctext = "It appears that the Star Portal has been used by an unknown species to invade this area."
first7.options = []
first7.options << [text: "Star Portal? What's that?", result: 8]
first7.options << [text: "What unknown species?", result: 12]
firstConvo.addDialog( first7, Caruthers )

first8 = [id:8]
first8.npctext = "The Star Portal was left here by the Zurg when they invaded our planet briefly a couple years ago. It's been inactive since that time, but recently began discharging energy at around the same time that the Animated began appearing in the area."
first8.playertext = "So the Star Portal is responsible for what's going on?"
first8.result = 9
firstConvo.addDialog( first8, Caruthers )

first9 = [id:9]
first9.npctext = "That's probably a negative. Rather, we think that the portal was partially re-activated by whatever caused the Animated to start appearing."
first9.playertext = "'We'? What 'we'?"
first9.result = 10
firstConvo.addDialog( first9, Caruthers )

first10 = [id:10]
first10.npctext = "A group of concerned citizens, that's all."
first10.playertext = "Mm-hmmm...and does that group have a name?"
first10.result = 11
firstConvo.addDialog( first10, Caruthers )

first11 = [id:11]
first11.npctext = "No, it does not."
first11.playertext = "Of course not."
first11.options = []
first11.options << [text: "Are you sure you're not affiliated with the 'Gaians in Black' I've heard about?", result: 2]
first11.options << [text: "Can you tell me more about that 'unknown species' in the area?", result: 12]
first11.options << [text: "Aren't you afraid those aliens are going to attack you?", result: 18]
first11.options << [text: "I don't think you're telling me much that I need to know. I'll see you later.", result: 17]
firstConvo.addDialog( first11, Caruthers )

first12 = [id:12]
first12.npctext = "I can tell you what I'm guessing, but there are few facts available. The aliens seem to have come through the Star Portal by accident and they can't seem to use it to get back."
first12.playertext = "Oh? How can you tell that?"
first12.result = 13
firstConvo.addDialog( first12, Caruthers )

first13 = [id:13]
first13.npctext = "We had an agen...err...fellow concerned citizen here watching the portal when the ETs first came through. They seemed quite disorganized and flustered initially, only settling down to dig their burrow bunkers after they gave up on getting out through the Star Portal."
first13.playertext = "I've seen a few of them. They seem to be packing a lot of equipment."
first13.result = 14
firstConvo.addDialog( first13, Caruthers )

first14 = [id:14]
first14.npctext = "That's true. And I know some people that would give a lot to get that kind of equipment to a lab for analysis."
first14.options = []
first14.options << [text: "Is that something you need help with?", result: 15]
first14.options << [text: "Maybe if you asked nicely, they'd give you some of their equipment?", result: 20]
firstConvo.addDialog( first14, Caruthers )

first15 = [id:15]
first15.npctext = "Normally, no. But our...group of citizens...is spread pretty thin right now. Would you be willing to test yourself against these ETs and bring me back some of their equipment?"
first15.options = []
first15.options << [text: "Sure! I'd be glad to help!", result: 16]
first15.options << [text: "Not right now...but maybe later.", result: 17]
firstConvo.addDialog( first15, Caruthers )

first16 = [id:16]
first16.npctext = "All right then. I'm fairly certain that the rings you're wearing will still be effective against these ETs, even though they're not Animated. See if you can knock some of them out and then come back and tell me what sorts of things you find on them."
first16.playertext = "You got it. Be back in a jif!"
first16.quest = 97 //push the start of the "A NEW SPECIES" quest
firstConvo.addDialog( first16, Caruthers )

first17 = [id:17]
first17.npctext = "All right, citizen. You have a nice day, now."
first17.result = DONE
firstConvo.addDialog( first17, Caruthers )

first18 = [id:18]
first18.npctext = "Thus far, I haven't given them any reason to fear me. They seem to leave well-enough alone if they aren't attacked or threatened by proximity."
first18.playertext = "Yeah...but what if they're just setting up for something bigger in the future? What if more of them come through that Portal?"
first18.result = 19
firstConvo.addDialog( first18, Caruthers )

first19 = [id:19]
first19.npctext = "That would be bad. Does that mean that you consider yourself something of a concerned citizen, then?"
first19.options = []
first19.options << [text: "Heck, yeah! Who wouldn't be?", result: 16]
first19.options << [text: "Ummm...no. I can see where that's going and I don't want any part of it.", result: 17]
firstConvo.addDialog( first19, Caruthers )

first20 = [id:20]
first20.npctext = "Negative. We've had little contact with the ETs, but enough to know that they don't speak any of our known languages. Besides, they seem to be aggressively xenophobic and they tend to react to any approach...very defensively."
first20.playertext = "They shoot at you?"
first20.result = 21
firstConvo.addDialog( first20, Caruthers )

first21 = [id:21]
first21.npctext = "Correct. The peace option is a non-starter."
first21.playertext = "Gotcha."
first21.options = []
first21.options << [text: "So is there anything I can do to help? I'd like to assist, if possible.", result: 16]
first21.options << [text: "That's tough and it sounds like a great place to stay away from right now. Good luck with it.", result: 17]
firstConvo.addDialog( first21, Caruthers )


//---------------------------------------------------------
// A NEW SPECIES (Interim)                                 
//---------------------------------------------------------

def speciesInterim = Caruthers.createConversation( "speciesInterim", true, "QuestStarted_97", "!QuestStarted_97:3" )

specInt1 = [id:1]
specInt1.npctext = "How are you doing with your investigations, citizen?"
specInt1.playertext = "I think I need to do a bit more work before getting you a report."
specInt1.result = 2
speciesInterim.addDialog( specInt1, Caruthers )

specInt2 = [id:2]
specInt2.npctext = "Take your time, citizen, take your time. I'm sure that an impending alien invasion threat shouldn't hurry you at all."
specInt2.playertext = "'Invasion threat'? You didn't say anything about an 'invasion threat'!"
specInt2.result = 3
speciesInterim.addDialog( specInt2, Caruthers )

specInt3 = [id:3]
specInt3.npctext = "Oh, I'm sure everything will be fine. Don't worry about it."
specInt3.playertext = "Ummm...yeah...sure."
specInt3.result = DONE
speciesInterim.addDialog( specInt3, Caruthers )

//---------------------------------------------------------
// A NEW SPECIES (Success)                                 
//---------------------------------------------------------

def speciesSuccess = Caruthers.createConversation( "speciesSuccess", true, "QuestStarted_97:3" )

specSucc1 = [id:1]
specSucc1.npctext = "So...what did you find out?"
specSucc1.playertext = "Well...not as much as you might hope. The little guys tend to blow themselves up when they're put out of commission, but I found scraps of circuit boards that seemed to grow out of flesh, funny-feeling plastic bits, and other stuff that's clearly 'not from around here'."
specSucc1.result = 2
speciesSuccess.addDialog( specSucc1, Caruthers )

specSucc2 = [id:2]
specSucc2.npctext = "Well done! You might have the stuff to join our little...society of well-meaning citizens...if you keep that up."
specSucc2.playertext = "Cool. But this stuff is just junk. Is it really going to tell you anything?"
specSucc2.result = 3
speciesSuccess.addDialog( specSucc2, Caruthers )

specSucc3 = [id:3]
specSucc3.npctext = "Well, not me specifically. But our labs will be able to analyze these things down to the molecular level and that should tell us a thing or two."
specSucc3.playertext = "Labs?"
specSucc3.result = 4
speciesSuccess.addDialog( specSucc3, Caruthers )

specSucc4 = [id:4]
specSucc4.npctext = "Uh, right. Okay, we're a *well-funded* group of concerned citizens. Keep bringing me info on these aliens and I'll let you in on what our scientists find out about these components you've shown me."
specSucc4.playertext = "That's downright generous of you!"
specSucc4.result = 5
speciesSuccess.addDialog( specSucc4, Caruthers )

specSucc5 = [id:5]
specSucc5.npctext = "Don't get all smarmy, junior. If we had more men available right now, we'd already have shipped you off somewhere as a security risk. But for now, we're appreciative of your assistance."
specSucc5.playertext = "Ummmm...thanks?"
specSucc5.quest = 97 //push the completion of the A NEW SPECIES quest
specSucc5.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Agent Caruthers-VQS" )
	event.player.setQuestFlag( GLOBAL, "Z7WarzoneStart" )
	Caruthers.pushDialog( event.player, "warzoneStart" )
}
speciesSuccess.addDialog( specSucc5, Caruthers )

//---------------------------------------------------------
// INTO THE WARZONE (Start)                                
//---------------------------------------------------------

def warzoneStart = Caruthers.createConversation( "warzoneStart", true, "Z7WarzoneStart", "!QuestStarted_98", "!QuestCompleted_98")

def warStart1 = [id:1]
warStart1.npctext = "There's still a lot to be discovered about these ETs."
warStart1.playertext = "Okay, but...do we have to keep calling them ETs?"
warStart1.result = 2
warzoneStart.addDialog( warStart1, Caruthers )

def warStart2 = [id:2]
warStart2.npctext = "A code name is a good idea, operative. Perhaps 'Species 3-XA'?"
warStart2.playertext = "Okay...I'll admit that's catchy...but maybe something a bit easier to say?"
warStart2.result = 3
warzoneStart.addDialog( warStart2, Caruthers )

def warStart3 = [id:3]
warStart3.npctext = "What would you suggest?"
warStart3.playertext = "I don't know. They're kinda small like prairie dogs. Maybe 'space dogs' or something?"
warStart3.result = 4
warzoneStart.addDialog( warStart3, Caruthers )

def warStart4 = [id:4]
warStart4.npctext = "That's a negative. That is the single stupidest name I have ever heard. These things are predators. They hunt and kill systematically in an organized fashion. They are lethal in the extreme and a threat to our entire societal structure."
warStart4.playertext = "Okay then, let's compromise...how about 'Predator Pups', or even 'Predator Prairie Pups'?"
warStart4.result = 5
warzoneStart.addDialog( warStart4, Caruthers )

def warStart5 = [id:5]
warStart5.npctext = "...will that end this conversation topic once and for all?"
warStart5.playertext = "Sure!"
warStart5.result = 6
warzoneStart.addDialog( warStart5, Caruthers )

def warStart6 = [id:6]
warStart6.npctext = "In that case, we'll designate them as 'P3' for short and call it official."
warStart6.playertext = "Oh...all right."
warStart6.result = 7
warzoneStart.addDialog( warStart6, Caruthers )

def warStart7 = [id:7]
warStart7.npctext = "Now that we're done with that, would you like to get back to work?"
warStart7.options = []
warStart7.options << [text: "You bet. What's next on your 'concerned citizen' list?", result: 8]
warStart7.options << [text: "I think I'm done for now. There's too few answers around here.", result: 14]
warzoneStart.addDialog( warStart7, Caruthers )

def warStart8 = [id:8]
warStart8.npctext = "The P3s are engaged in a sporadic battle to the west of their main encampment with Animated units coming down from the jungle. I believe this may distract them enough for you to be able to scout their burrow positions."
warStart8.playertext = "You want me to go into the middle of all that?"
warStart8.result = 9
warzoneStart.addDialog( warStart8, Caruthers )

def warStart9 = [id:9]
warStart9.npctext = "Don't get your panties in a bunch, junior. It's not like I'm asking you to camp there overnight or anything. Just get near to each of the burrows and this scanner I'm giving you will read out all sorts of info from their burrow and transmit it back to the labs."
warStart9.playertext = "So you think the Pups will just let me in, even though they've shot at me everytime I got close to them?"
warStart9.result = 10
warzoneStart.addDialog( warStart9, Caruthers )

def warStart10 = [id:10]
warStart10.npctext = "Of course not. I expect your natural cunning and talent to preserve you in the face of danger."
warStart10.playertext = "Oh. Okay then. That sounds good."
warStart10.result = 11
warzoneStart.addDialog( warStart10, Caruthers )

def warStart11 = [id:11]
warStart11.npctext = "Good. By my count, there are eight P3 burrows out there. You need to get close to at least five of them before we'll have gathered enough data to learn more about their race."
warStart11.playertext = "Gotcha. Five burrows. Right up next to them. Okay...well...wish me luck."
warStart11.result = 12
warzoneStart.addDialog( warStart11, Caruthers )

def warStart12 = [id:12]
warStart12.npctext = "Luck is for those that do not have skill, junior. Don't rely on it."
warStart12.playertext = "Okay, Mr. Sunshine. Hey...wait a minute! What *is* your name, anyway?"
warStart12.result = 13
warzoneStart.addDialog( warStart12, Caruthers )

def warStart13 = [id:13]
warStart13.npctext = "Do your job and come back. We'll talk about my name then, if you're still alive."
warStart13.playertext = "Great. What a reward. See ya."
warStart13.result = DONE
warStart13.exec = { event ->
	event.player.setPlayerVar( "burrowCount", 0 ) //sets the number of counted burrows to zero (initializing the state for the quest)
}
warStart13.quest = 98 //start the SCANNING THE BURROWS quest
warzoneStart.addDialog( warStart13, Caruthers )

def warStart14 = [id:14]
warStart14.npctext = "Okay, citizen. If it's answers you're looking for, I'm probably not your guy anyway. Good luck to you."
warStart14.result = DONE
warzoneStart.addDialog( warStart14, Caruthers )

//---------------------------------------------------------
// INTO THE WARZONE (Interim)                              
//---------------------------------------------------------

def warInterim = Caruthers.createConversation( "warInterim", true, "QuestStarted_98:2" )

def warInt1 = [id:1]
warInt1.npctext = "You're still alive! Good for you!"
warInt1.playertext = "I find your surprise not encouraging in the slightest."
warInt1.result = 2
warInterim.addDialog( warInt1, Caruthers )

def warInt2 = [id:2]
warInt2.npctext = "How goes the scouting?"
warInt2.playertext = "Okay, okay...I've still got work to do. I'll report back soon."
warInt2.result = 3
warInterim.addDialog( warInt2, Caruthers )

def warInt3 = [id:3]
warInt3.npctext = "You do that."
warInt3.result = DONE
warInterim.addDialog( warInt3, Caruthers )

//---------------------------------------------------------
// INTO THE WARZONE (Success)                              
//---------------------------------------------------------

def warSuccess = Caruthers.createConversation( "warSuccess", true, "QuestStarted_98:3", "!QuestCompleted_98" )

def warSucc1 = [id:1]
warSucc1.npctext = "Well done, junior! The lab is ecstatic about the data you transmitted from the burrows!"
warSucc1.playertext = "Oh. So you know already then?"
warSucc1.result = 2
warSuccess.addDialog( warSucc1, Caruthers )

def warSucc2 = [id:2]
warSucc2.npctext = "Of course we do. That scanner transmitted everything you found and the white coats have been analyzing it in real-time."
warSucc2.playertext = "Ah...of course. So what did they find?"
warSucc2.result = 3
warSuccess.addDialog( warSucc2, Caruthers )

def warSucc3 = [id:3]
warSucc3.npctext = "A number of things. First, it looks like the P3 aren't here to invade, after all. Instead, they seem to have come through the Star Portal and then ended up trapped here, unable to return for some reason."
warSucc3.playertext = "So, I've been fighting *accident victims*?"
warSucc3.result = 4
warSuccess.addDialog( warSucc3, Caruthers )

def warSucc4 = [id:4]
warSucc4.npctext = "Hardly. Apparently, those burrows of theirs go pretty deep, and they seem to breeding a whole army of aliens down there. It's like a giant hotel full of P3s, going down deep into the ground."
warSucc4.playertext = "But...they just got here, you said!"
warSucc4.result = 5
warSuccess.addDialog( warSucc4, Caruthers )

def warSucc5 = [id:5]
warSucc5.npctext = "Hmmm...well...they must be frisky little aliens then. Regardless, they seem to be really high-tech, and if we don't find a way for them to leave our planet, they might not need to invade. They can just crowd us out eventually, at the rate they're multiplying."
warSucc5.playertext = "So, they *are* a danger, after all?"
warSucc5.result = 6
warSuccess.addDialog( warSucc5, Caruthers )

def warSucc6 = [id:6]
warSucc6.npctext = "You're darn tootin'. But one of them, the one that seems to be their Commander...or whatever they call their leader...has a transponder of some sort that we think activates the Star Portal."
warSucc6.playertext = "You mean they *could* leave after all? But you said they were trapped here!"
warSucc6.result = 7
warSuccess.addDialog( warSucc6, Caruthers )

def warSucc7 = [id:7]
warSucc7.npctext = "You seem to be getting a bit frustrated. I'll tell you what...I'll give you some information, in exchange for the work you've done for us recently. If you like what you see, then maybe we can keep working together to solve what's going on here."
warSucc7.playertext = "Sounds good. What kind of info are you talking about?"
warSucc7.result = 8
warSuccess.addDialog( warSucc7, Caruthers )

def warSucc8 = [id:8]
warSucc8.npctext = "You've noticed by now that the P3 have very advanced gauntlets they wear with communication devices inside them. Here are the plans that the lab has deciphered. With these plans, you should be able to make your own set of the same gauntlets."
warSucc8.playertext = "Wow! Fantastic!"
warSucc8.quest = 98 //push the completion of the INTO THE WARZONE quest
warSucc8.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Agent Caruthers-VQS" )
	event.player.setQuestFlag( GLOBAL, "Z7GauntletGiven" )
	Caruthers.pushDialog( event.player, "getTransponder" )
}
warSuccess.addDialog( warSucc8, Caruthers )

//---------------------------------------------------------
// GET THE TRANSPONDER                                     
//---------------------------------------------------------

def getTransponder = Caruthers.createConversation( "getTransponder", true, "Z7GauntletGiven", "!QuestStarted_99", "!QuestCompleted_99" )

def trans1 = [id:1]
trans1.playertext = "Hey, by the way! What's your name?"
trans1.result = 2
getTransponder.addDialog( trans1, Caruthers )

def trans2 = [id:2]
trans2.npctext = "All right. I said I'd tell you. It's Caruthers."
trans2.playertext = "Caruthers? Is that your first name or last name?"
trans2.result = 3
getTransponder.addDialog( trans2, Caruthers )

def trans3 = [id:3]
trans3.npctext = "It's Agent Caruthers."
trans3.playertext = "So now it's 'agent' instead of 'concerned citizen'?"
trans3.result = 4
getTransponder.addDialog( trans3, Caruthers )

def trans4 = [id:4]
trans4.npctext = "Don't push me, citizen. Now, if you liked that gauntlet formula, there's more where that came from. If you keep being successful for us, there's no reason we can't keep setting you up with more gizmos of your own."
trans4.options = []
trans4.options << [text: "That sounds great. I'm in! What do you need done next?", result: 6]
trans4.options << [text: "Thanks, but I'm done for now. I'm sure you'll be fine without me.", result: 5]
getTransponder.addDialog( trans4, Caruthers )

def trans5 = [id:5]
trans5.npctext = "Okay, scrub. Here's hoping you don't wake up with a hundred P3s staring at you in your bed."
trans5.result = DONE
getTransponder.addDialog( trans5, Caruthers )

def trans6 = [id:6]
trans6.npctext = "I admire your eagerness, scrub. Here's what we need to do. Those scans you made revealed that only one of the P3s is holding the transponder used to activate the Star Portal."
trans6.playertext = "I have to find a single Pup in all that mess? How am I going to do that?"
trans6.result = 7
getTransponder.addDialog( trans6, Caruthers )

def trans7 = [id:7]
trans7.npctext = "Our cumulative scans have revealed that the leader of their collective takes patrols out on his own, ranging far around the burrow perimeters on a regular basis. But he always heads home to the main bunker near the trees to the south. You can wait for him there or try to catch him and his honor guard out on patrol. It's up to you."
trans7.playertext = "And then what?"
trans7.result = 8
getTransponder.addDialog( trans7, Caruthers )

def trans8 = [id:8]
trans8.npctext = "That's where that natural cunning and skill comes into play again. I know you'll think of something."
trans8.playertext = "Great. So...find the leader, whack him somehow, and bring the transponder back to you?"
trans8.result = 9
getTransponder.addDialog( trans8, Caruthers )

def trans9 = [id:9]
trans9.npctext = "That'll do, junior. That'll do."
trans9.playertext = "I was afraid you might say that. Okay. I'm on it."
trans9.quest = 99 //start the GET THE TRANSPONDER quest
trans9.result = DONE
getTransponder.addDialog( trans9, Caruthers )

//---------------------------------------------------------
// GET THE TRANSPONDER (Interim)                           
//---------------------------------------------------------

def transInterim = Caruthers.createConversation( "transInterim", true, "QuestStarted_99", "!QuestCompleted_99" )

transInt1 = [id:1]
transInt1.npctext = "So I assume you have the transponder already?"
transInt1.playertext = "Errr...ummm..."
transInt1.result = 2
transInterim.addDialog( transInt1, Caruthers )

transInt2 = [id:2]
transInt2.npctext = "So you didn't get it then?"
transInt2.playertext = "No, not yet anyway."
transInt2.result = 3
transInterim.addDialog( transInt2, Caruthers )

transInt3 = [id:3]
transInt3.npctext = "Then what are you doing here? Get back out there and find that transponder! The fate of our world is in your hands!"
transInt3.result = DONE
transInterim.addDialog( transInt3, Caruthers )

//---------------------------------------------------------
// GET THE TRANSPONDER (Success)                           
//---------------------------------------------------------

def transSuccess = Caruthers.createConversation( "transSuccess", true, "QuestStarted_99:3", "!QuestCompleted_99" )

transSucc1 = [id:1]
transSucc1.npctext = "Well?"
transSucc1.playertext = "I got it! It was hard to find him, and that was a tough fight, but I got it!"
transSucc1.result = 2
transSuccess.addDialog( transSucc1, Caruthers )

transSucc2 = [id:2]
transSucc2.npctext = "Well done! And as per our deal, here's the blueprints to the helmets that the P3s are wearing. Use the knowledge wisely."
transSucc2.playertext = "Fantastic!"
transSucc2.quest = 99 //push the completion of the GET THE TRANSPONDER quest
transSucc2.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Agent Caruthers-VQS" )
	event.player.setQuestFlag( GLOBAL, "Z7HelmetGiven" )
	Caruthers.pushDialog( event.player, "portalStart" )
}
transSuccess.addDialog( transSucc2, Caruthers )

//---------------------------------------------------------
// INTO THE STAR PORTAL (Start)                            
//---------------------------------------------------------

def portalStart = Caruthers.createConversation( "portalStart", true, "Z7HelmetGiven", "!QuestStarted_100", "!QuestCompleted_100" )

portal1 = [id:1]
portal1.playertext = "Hey...wait a minute. You're being suspiciously civil in your tone. What's up?"
portal1.result = 2
portalStart.addDialog( portal1, Caruthers )

portal2 = [id:2]
portal2.npctext = "Me? I don't see how I'm behaving any differently than normal. What do you mean?"
portal2.playertext = "You've gone more than two sentences without insulting me or putting me down. What's wrong?"
portal2.result = 3
portalStart.addDialog( portal2, Caruthers )

portal3 = [id:3]
portal3.npctext = "Maybe I've just learned to respect you for your accomplishments...%p."
portal3.playertext = "What? You called me by NAME? Do I have a terminal disease or something? I'm gonna die, aren't I?"
portal3.result = 4
portalStart.addDialog( portal3, Caruthers )

portal4 = [id:4]
portal4.npctext = "Not immediately...no."
portal4.playertext = "WHAT?!?"
portal4.result = 5
portalStart.addDialog( portal4, Caruthers )

portal5 = [id:5]
portal5.npctext = "Well...who knows what will happen to you when you go through the Portal? I mean...anything could happen, couldn't it?"
portal5.playertext = "THROUGH the portal?"
portal5.options = []
portal5.options << [text: "Hmmm...come to think of it...I've always wanted to 'get away from it all'. Tell me more.", result: 8]
portal5.options << [text: "HELL no! I'm not leaving gaia firma...ever!", result: 6]
portalStart.addDialog( portal5, Caruthers )

portal6 = [id:6]
portal6.npctext = "That's too bad, junior. I had such high hopes for you. And this laser chestplate blueprint, well...I'm sure someone else will want it later on."
portal6.options = []
portal6.options << [text: "Ummm...maybe I should reconsider. I mean...someone else would take time to come up to speed, and y'know...you can't really afford that time.", result: 8]
portal6.options << [text: "Chestplate? But...NO. No way! I'm not going and I'm not changing my mind!", result: 7]
portalStart.addDialog( portal6, Caruthers )

portal7 = [id:7]
portal7.npctext = "When you look out your kitchen window and all you can see is burrows for miles, I hope you won't regret your situation, junior."
portal7.playertext = "I won't, I won't, I WON'T! I'm outta here!"
portal7.result = DONE
portalStart.addDialog( portal7, Caruthers )

portal8 = [id:8]
portal8.npctext = "That's what a good operative should say! I knew I could count on you, junior!"
portal8.playertext = "Thanks, senior."
portal8.result = 9
portalStart.addDialog( portal8, Caruthers )

portal9 = [id:9]
portal9.npctext = "Is that supposed to be good-natured ribbing?"
portal9.playertext = "ummm...yes? I mean...now that I know you actually *know* my name, it just seems fair, y'know?"
portal9.result = 10
portalStart.addDialog( portal9, Caruthers )

portal10 = [id:10]
portal10.npctext = "Right. Well, look junior, we need you to take that transponder you got from their leader and use it to open the Star Portal..."
portal10.playertext = "Okay. Opening it doesn't sound so bad. I thought you wanted me to go THROUGH it."
portal10.result = 11
portalStart.addDialog( portal10, Caruthers )

portal11 = [id:11]
portal11.npctext = "...and then go through to the other side to see what the P3 home world looks like, and why they aren't headed back themselves."
portal11.playertext = "Oh. Doh. So...something's stopping the P3s from going home...stopping those high-tech aliens with all the super-powered lasers and explosives...and somehow little ol' me and my rings are supposed to jump through and figure out what's stopping THEM?"
portal11.result = 12
portalStart.addDialog( portal11, Caruthers )

portal12 = [id:12]
portal12.npctext = "That's about it! Good luck, Operative %p!"
portal12.playertext = "Wait...you said you didn't believe in luck."
portal12.result = 13
portalStart.addDialog( portal12, Caruthers )

portal13 = [id:13]
portal13.npctext = "I don't. But something tells me you're going to need every bit of help you can get on this one."
portal13.playertext = "Man...you're just the king of uplifting speeches, aren't you?"
portal13.result = 14
portalStart.addDialog( portal13, Caruthers )

portal14 = [id:14]
portal14.npctext = "I've always thought so, yes."
portal14.quest = 100 //start the THROUGH THE STAR PORTAL quest
portal14.result = DONE
portalStart.addDialog( portal14, Caruthers )

//---------------------------------------------------------
// INTO THE STAR PORTAL (Finish)                           
// (No Interim conversation needed because player can't    
// come back to Caruthers without dying in Portal first)   
//---------------------------------------------------------

def portalFinish = Caruthers.createConversation( "portalFinish", true, "QuestStarted_100:3", "!QuestCompleted_100", "!Z07RingRewardInProgress" )

portFin1 = [id:1]
portFin1.npctext = "%p! You made it back! What happened? Your scanner cut off as soon as you went through the Portal."
portFin1.playertext = "Bugs. Lots and lots and LOTS of bugs. Hundreds of 'em. MILLIONS!"
portFin1.result = 2
portalFinish.addDialog( portFin1, Caruthers )

portFin2 = [id:2]
portFin2.npctext = "You mean the P3 world was invaded by insectoids?"
portFin2.playertext = "No. Yes. I don't know...maybe. But the walls of the area I was in looked like they'd been secreted by the bugs and *grown*. They didn't look natural at all. Heck...they *pulsed* all the time I was there!"
portFin2.result = 3
portalFinish.addDialog( portFin2, Caruthers )

portFin3 = [id:3]
portFin3.npctext = "Hmmm...so either the P3 world has been overrun by bugs, or the portal isn't connecting to their home world any longer. Either way, it's clear why they're not trying to leave through the portal."
portFin3.playertext = "As far as I'm concerned, those pups are smarter than we are. Dang...I'm telling you...there were bugs *everywhere*. I'm really lucky that wherever I was, it wasn't too far away from the Null Chamber for me to reform there after I got beat down!"
portFin3.result = 4
portalFinish.addDialog( portFin3, Caruthers )

portFin4 = [id:4]
portFin4.npctext = "Well...good job. I'm not sure how we're going to contain the spread of the pups now, but at least our course is clear. I suppose this means that war is inevitable between our races unless we can find a way to communicate. Time will tell, I suppose."
portFin4.playertext = "Well...good luck with that. I'm done for now."
portFin4.result = 5
portalFinish.addDialog( portFin4, Caruthers )

portFin5 = [id:5]
portFin5.npctext = "That's all right, %p. We at the GIB don't have a clear direction to proceed at this time anyway."
portFin5.playertext = "Ah HA! You said it! You *are* the Gaians in Black. I *knew* it!"
portFin5.result = 6
portalFinish.addDialog( portFin5, Caruthers )

portFin6 = [id:6]
portFin6.npctext = "And we knew you knew it, so don't get too full of yourself, Sherlock. Just be happy that I finally said it out loud to you."
portFin6.playertext = "Happy? Why is that?"
portFin6.result = 7
portalFinish.addDialog( portFin6, Caruthers )

portFin7 = [id:7]
portFin7.npctext = "Because that means we trust you enough to not erase your mind and replace it with our standard accountant template."
portFin7.playertext = "Oh good god. You would *do* that to me? An ACCOUNTANT? That's just inhuman!"
portFin7.result = 8
portalFinish.addDialog( portFin7, Caruthers )

portFin8 = [id:8]
portFin8.npctext = "Welcome to the GIB, %p. Whether you like it or not."
portFin8.playertext = "Oh. Good then, I guess."
portFin8.result = 10
portalFinish.addDialog( portFin8, Caruthers )

// portFin9 is deleted

portFin10 = [id:10]
portFin10.npctext = "If you'd like to do something else for us, while we continue to watch the P3 issue here, then report back to me. There's a lot here to investigate."
portFin10.playertext = "Okay, Caruthers. I'll think about it. Thanks."
portFin10.result = 11
portalFinish.addDialog( portFin10, Caruthers )

portFin11 = [id:11]
portFin11.npctext = "You did good here today, kid. Keep it up! And to prove that the GIB means business, here...take one of these rings."
portFin11.quest = 100 //push the completion of the INTO THE STAR PORTAL quest
portFin11.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Agent Caruthers-VQS" )
	event.player.setQuestFlag( GLOBAL, "Z05CaruthersRingGrantOkay" )
	Caruthers.pushDialog( event.player, "ringGrant" )
}
portFin11.result = DONE
portalFinish.addDialog( portFin11, Caruthers )

//---------------------------------------------------------
// The "TOO OLD" conversation                              
//---------------------------------------------------------

def tooOld = Caruthers.createConversation( "tooOld", true, "!QuestStarted_36", "QuestCompleted_100", "Z07TooOldForAqueductQuests", "!Z07RingRewardInProgress")

def old1 = [id:1]
old1.npctext = "Okay...so maybe I can't call you Junior anymore then."
old1.playertext = "What do you mean, Caruthers?"
old1.result = 2
tooOld.addDialog( old1, Caruthers )

def old2 = [id:2]
old2.npctext = "I'm just saying I think you're old enough to head out on your own. To take on some of the challenges in Gold Beach, or even over in the Otami Ruins."
old2.playertext = "Hey! Thanks for the vote of confidence! Catch you later!"
old2.exec = { event ->
	event.player.centerPrint( "To access Agent Caruthers' repeatable task, you need to change your level to 6.1 or lower." )
	myManager.schedule(3) { event.player.centerPrint( "Use the 'CHANGE LEVEL' option in the MENU to lower your level." ) }
}
old2.result = DONE
tooOld.addDialog( old2, Caruthers )

//---------------------------------------------------------
// INTO THE FRAY (P3 vs TT warzone repeatable quest)       
//---------------------------------------------------------

def frayStart = Caruthers.createConversation( "frayStart", true, "!QuestStarted_36", "QuestCompleted_100", "!Z07TooOldForAqueductQuests", "!Z07RingRewardInProgress" )

def fray1 = [id:1]
fray1.npctext = "Welcome back, field agent."
fray1.playertext = "Agent? I've come up in the world."
fray1.result = 2
frayStart.addDialog( fray1, Caruthers )

def fray2 = [id:2]
fray2.npctext = "Not far. You're a 'FIELD' agent, and that's only one rung up from errand boy."
fray2.playertext = "Ah, well. It's still up! Do you have any assignments for me?"
fray2.result = 3
frayStart.addDialog( fray2, Caruthers )

def fray3 = [id:3]
fray3.npctext = "Hmmm. You put up a good show on the earlier tasks, so you're tougher than most. I think I've got just the job for you."
fray3.playertext = "What do you need?"
fray3.result = 4
frayStart.addDialog( fray3, Caruthers )

def fray4 = [id:4]
fray4.npctext = "The Tiny Terrors are coming out of the jungle ruins and they seem to be after the old stones left from when the old aqueduct collapsed."
fray4.playertext = "What do they want with the stones?"
fray4.result = 5
frayStart.addDialog( fray4, Caruthers )

def fray5 = [id:5]
fray5.npctext = "I have no idea. But their incursions into the territories claimed by the P3s seem to have set off some hostilities. The P3s, even though they're aliens, still register as 'Gaians' to the Tiny Terrors and they attack on sight."
fray5.playertext = "And the Pups? How do they react?"
fray5.result = 6
frayStart.addDialog( fray5, Caruthers )

def fray6 = [id:6]
fray6.npctext = "How do you think? No love lost there. The whole thing has boiled up into a full-scale scrimmage that's on-going."
fray6.playertext = "Do you want me to stop the war or something?"
fray6.result = 7
frayStart.addDialog( fray6, Caruthers )

def fray7 = [id:7]
fray7.npctext = "Nope. If anything, I want you to egg the conflict on. Get down there and lay waste to both sides, and then get out again."
fray7.playertext = "What? How does that help anything?"
fray7.result = 8
frayStart.addDialog( fray7, Caruthers )

def fray8 = [id:8]
fray8.npctext = "The more they fight each other, the less they're fighting *us*. So get in there and make things worse for them."
fray8.options = []
fray8.options << [ text: "Cool. A real government 'destabilization' job. I'm an official secret agent now!", result: 9]
fray8.options << [ text: "Hmmmm...not right now, I think. I'll come back later.", result: DONE ]
frayStart.addDialog( fray8, Caruthers )

def fray9 = [id:9]
fray9.npctext = "Settle down there, junior. Just get in, get out, and report back. Your target area is the war zone at the base of the hill to the southwest. Knock out about 10 of each side and I think you'll have done enough for now."
fray9.playertext = "Will do!"
fray9.quest = 36 //Start the INTO THE FRAY task
fray9.exec = { event ->
	event.player.setPlayerVar( "Z07WarzoneTTKilled", 0 )
	event.player.setPlayerVar( "Z07WarzoneP3Killed", 0 )
}
fray9.result = DONE
frayStart.addDialog( fray9, Caruthers )

//---------------------------------------------------------
// INTO THE FRAY ( status check )                          
//---------------------------------------------------------

def frayStatus = Caruthers.createConversation( "frayStatus", true, "QuestStarted_36", "QuestCompleted_100", "!Z07FrayIncomplete", "!Z07FraySuccess", "!Z07TooOldForAqueductQuests", "!Z07RingRewardInProgress" )

def status1 = [id:1]
status1.npctext = "Good to see you again, Agent. Let's hear your report!"
status1.exec = { event ->
	if( event.player.getPlayerVar( "Z07WarzoneTTKilled" ) >= 10 && event.player.getPlayerVar( "Z07WarzoneP3Killed" ) >= 10 ) {
		event.player.setQuestFlag( GLOBAL, "Z07FraySuccess" )
		Caruthers.pushDialog( event.player, "fraySucc" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z07FrayIncomplete" )
		Caruthers.pushDialog( event.player, "frayIncomplete" )
	}
}
frayStatus.addDialog( status1, Caruthers )

//---------------------------------------------------------
// INTO THE FRAY ( incomplete )                            
//---------------------------------------------------------

def frayIncomplete = Caruthers.createConversation( "frayIncomplete", true, "QuestStarted_36:2", "QuestCompleted_100", "Z07FrayIncomplete", "!Z07TooOldForAqueductQuests", "!Z07RingRewardInProgress" )

def inc1 = [id:1]
inc1.playertext = "Ummm...errr...well...I haven't completed the job yet."
inc1.result = 2
frayIncomplete.addDialog( inc1, Caruthers )

def inc2 = [id:2]
inc2.npctext = "Well, get back out there, junior! Time's'a'wastin'!"
inc2.flag = "!Z07FrayIncomplete"
inc2.exec = { event ->
	numTTKilled = event.player.getPlayerVar( "Z07WarzoneTTKilled" ).intValue()
	if( numTTKilled > 10 ) { numTTKilled = 10 }
	numP3Killed = event.player.getPlayerVar( "Z07WarzoneP3Killed" ).intValue()
	if( numP3Killed > 10 ) { numP3Killed = 10 }
	event.player.centerPrint( "You have defeated ${numTTKilled}/10 Tiny Terrors and ${numP3Killed}/10 Preda-Pups so far." )
}
inc2.result = DONE
frayIncomplete.addDialog( inc2, Caruthers )


//---------------------------------------------------------
// INTO THE FRAY ( success )                               
//---------------------------------------------------------

def fraySucc = Caruthers.createConversation( "fraySucc", true, "QuestStarted_36:3", "QuestCompleted_100", "Z07FraySuccess", "!Z07TooOldForAqueductQuests", "!Z07RingRewardInProgress" )

def succ1 = [id:1]
succ1.playertext = "Mission accomplished!"
succ1.result = 2
fraySucc.addDialog( succ1, Caruthers )

def succ2 = [id:2]
succ2.npctext = "Did you get them all riled up?"
succ2.playertext = "I don't know. They seem kind of 'riled up' normally. But yes, I stirred them up more and they seemed to be constantly fighting when I left."
succ2.result = 3
fraySucc.addDialog( succ2, Caruthers )

def succ3 = [id:3]
succ3.npctext = "That's all that can be expected. Good job, field agent!"
succ3.playertext = "hehe. All in a day's work."
succ3.result = 4
fraySucc.addDialog( succ3, Caruthers )

def succ4 = [id:4]
succ4.npctext = "Well said. If you're interested, I can use assistance like this again, at your discretion."
succ4.playertext = "Sounds great! Thanks, Caruthers!"
succ4.result = 5
fraySucc.addDialog( succ4, Caruthers )

def succ5 = [id:5]
succ5.npctext = "Anytime, junior. Here. Take this reward as compensation for your efforts."
succ5.quest = 36
succ5.flag = "!Z07FraySuccess"
succ5.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Agent Caruthers-VQS" )
}	
succ5.result = DONE
fraySucc.addDialog( succ5, Caruthers )


//--------------------------------------------------------------
//RING GRANT CONVERSATION                                       
//--------------------------------------------------------------
def ringGrant = Caruthers.createConversation( "ringGrant", true, "Z05CaruthersRingGrantOkay", "!Z07CaruthersRingGrantReceived", "!Z07RingRewardInProgress" )

def grant1 = [id:1]
grant1.playertext = "All right! *Now* we're talkin'!"
grant1.exec = { event ->
	player = event.player
	event.player.setQuestFlag( GLOBAL, "Z07RingRewardInProgress" )
	makeMainMenu( player )
}
grant1.result = DONE
ringGrant.addDialog( grant1, Caruthers )

//====================================================
// RING GRANT MENU LOGIC                              
//====================================================

dialogBoxWidth = 400
CL = 3
CLdecimal = 0

def makeMainMenu( player ) {
	titleString = "Main Menu"
	descripString = "Choose a ring from any of these categories:"
	diffOptions = ["New Rings", "Close Combat", "Ranged Combat", "Crowd Control", "Defenses", "Healing", "Buffs", "Debuffs", "Cancel"]
	
	uiButtonMenu( player, "mainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "New Rings" ) {
			makeNewRingMenu( player )
		}
		if( event.selection == "Close Combat" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Ranged Combat" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Crowd Control" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Defenses" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Healing" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Buffs" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Debuffs" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z07RingRewardInProgress" )
		}
	}
}

//====================================================
// NEW RINGS                                          
//====================================================
def makeNewRingMenu( player ) {
	titleString = "New Rings Menu"
	descripString = "These rings are new to the list this time."
	diffOptions = [ "Gumshoe", "Improbability Sphere", "Knife Sharpen", "Shark Attack", "Main Menu"  ]
	
	uiButtonMenu( player, "newRingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Gumshoe" ) {
			makeGumshoeMenu( player )
		}
		if( event.selection == "Improbability Sphere" ) {
			makeImprobabilitySphereMenu( player )
		}
		if( event.selection == "Knife Sharpen" ) {
			makeKnifeSharpenMenu( player )
		}
		if( event.selection == "Shark Attack" ) {
			makeSharkAttackMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		

//====================================================
// CLOSE COMBAT RINGS                                 
//====================================================
def makeCombatMenu( player ) {
	titleString = "Close Combat Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bump", "Dervish", "Hack", "Mantis", "Slash", "Main Menu" ]
	
	uiButtonMenu( player, "combatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bump" ) {
			makeBumpMenu( player )
		}
		if( event.selection == "Dervish" ) {
			makeDervishMenu( player )
		}
		if( event.selection == "Hack" ) {
			makeHackMenu( player )
		}
		if( event.selection == "Mantis" ) {
			makeMantisMenu( player )
		}
		if( event.selection == "Slash" ) {
			makeSlashMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBumpMenu( player ) {
	titleString = "Bump"
	descripString = "Turn that hip around and *bump* your opponent away from you. Knocking them far away at higher Rage Ranks. The process *is* painful to your target."
	diffOptions = [ "Take the Bump ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "bumpMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bump ring!" ) {
			event.actor.grantRing( "17713", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDervishMenu( player ) {
	titleString = "Dervish"
	descripString = "Whirling at incredible speed, you deal damage to all foes close to you. Higher Rage Ranks knock your enemies farther back and increase the area you hit."
	diffOptions = [ "Take the Dervish ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DervishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Dervish ring!" ) {
			event.actor.grantRing( "17712", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHackMenu( player ) {
	titleString = "Hack"
	descripString = "Land a colossal blow to your foes! Hits things hard, even causing them to bleed for a bit after you hit them. At higher Rage Ranks, the bleeding lasts longer, thus causing more damage."
	diffOptions = [ "Take the Hack ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hack ring!" ) {
			event.actor.grantRing( "17714", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMantisMenu( player ) {
	titleString = "Mantis"
	descripString = "You create a katana from nothing to do your bidding. Does light damage, but attacks again very quickly. At higher Rage Ranks, it also drains an enemy's Willpower."
	diffOptions = [ "Take the Mantis ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MantisMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Mantis ring!" ) {
			event.actor.grantRing( "17710", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSlashMenu( player ) {
	titleString = "Slash"
	descripString = "You slash at the nearby foes in front of you, doing damage to all that you hit. Your slash becomes wider and deeper at higher Rage Ranks, allowing you to hit more enemies."
	diffOptions = [ "Take the Slash ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SlashMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Slash ring!" ) {
			event.actor.grantRing( "17711", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// RANGED COMBAT RINGS                                
//====================================================
def makeRangedMenu( player ) {
	titleString = "Ranged Attack Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Fire Rain", "Guns, Guns, Guns", "Heavy Water Balloon", "Hornet Nest", "Hot Foot", "Hunter's Bow", "Shark Attack", "Shuriken", "Solar Rays", "Main Menu" ]
	
	uiButtonMenu( player, "rangedMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Fire Rain" ) {
			makeFireRainMenu( player )
		}
		if( event.selection == "Guns, Guns, Guns" ) {
			makeGunsGunsGunsMenu( player )
		}
		if( event.selection == "Heavy Water Balloon" ) {
			makeHeavyWaterBalloonMenu( player )
		}
		if( event.selection == "Hornet Nest" ) {
			makeHornetNestMenu( player )
		}
		if( event.selection == "Hot Foot" ) {
			makeHotFootMenu( player )
		}
		if( event.selection == "Hunter's Bow" ) {
			makeHuntersBowMenu( player )
		}
		if( event.selection == "Shark Attack" ) {
			makeSharkAttackMenu( player )
		}
		if( event.selection == "Shuriken" ) {
			makeShurikenMenu( player )
		}
		if( event.selection == "Solar Rays" ) {
			makeSolarRaysMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeFireRainMenu( player ) {
	titleString = "Fire Rain"
	descripString = "Summon burning rain from the sky to fall in an area around yourself damaging your foes and draining their Willpower. Higher Rage Ranks result in bigger damage areas and greater Willpower drains."
	diffOptions = [ "Take the Fire Rain ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FireRainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fire Rain ring!" ) {
			event.actor.grantRing( "17748", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGunsGunsGunsMenu( player ) {
	titleString = "Guns, Guns, Guns"
	descripString = "When all else fails, haul out the artillery and drown your target in lead! Higher Rage Ranks create a wider spray of bullets, causing more damage in a bigger and bigger area around your target."
	diffOptions = [ "Take the Guns, Guns, Guns ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GunsGunsGunsMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Guns, Guns, Guns ring!" ) {
			event.actor.grantRing( "17747", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHeavyWaterBalloonMenu( player ) {
	titleString = "Heavy Water Balloon"
	descripString = "You create a giant water balloon and hurl it at your foes, causing a colossal splash in a large area, damaging those affected. Higher Rage Ranks make bigger splashes and Taunt the enemies in the area to attack you instead of your friends."
	diffOptions = [ "Take the Heavy Water Balloon ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HeavyWaterBalloonMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Heavy Water Balloon ring!" ) {
			event.actor.grantRing( "17719", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHornetNestMenu( player ) {
	titleString = "Hornet Nest"
	descripString = "Hurl a nest of hornets at the ground, creating a swarm that attacks nearby foes. Higher Rage Ranks increase the area affected, as well as making the target sometimes panic and run away. (Fear)"
	diffOptions = [ "Take the Hornet Nest ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HornetNestMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hornet Nest ring!" ) {
			event.actor.grantRing( "17718", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHotFootMenu( player ) {
	titleString = "Hot Foot"
	descripString = "Set your target's feet on fire, causing it pain for several seconds after the attack occurs. At higher Rage Ranks, the target also suffers a Dodge penalty, making it easier to hit. Higher Rage Ranks also make this ability affect an area around the target."
	diffOptions = [ "Take the Hot Foot ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HotFootMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hot Foot ring!" ) {
			event.actor.grantRing( "17717", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHuntersBowMenu( player ) {
	titleString = "Hunter's Bow"
	descripString = "This bow lets you fire arrows often and far, damaging your foe and slowing it down so they can't get to you easily. Higher Rage Ranks reduce the target's Footspeed still further and increase the duration of the effect."
	diffOptions = [ "Take the Hunter's Bow ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HuntersBowMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hunter's Bow ring!" ) {
			event.actor.grantRing( "17721", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSharkAttackMenu( player ) {
	titleString = "Shark Attack"
	descripString = "Groundsharks attack your foe, often knocking it away from you, and also causing some bleeding to persist after the attack. Higher Rage Ranks result in longer bleeding duration and sometimes paralyzing your target with shock. (Root)"
	diffOptions = [ "Take the Shark Attack ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SharkAttackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shark Attack ring!" ) {
			event.actor.grantRing( "17716", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeShurikenMenu( player ) {
	titleString = "Shuriken"
	descripString = "Hurl spiny metal stars at your foes! In addition to damaging your target, higher Rage Ranks increase the effect to an area around your target, plus they cause your target to have reduced Accuracy for a time."
	diffOptions = [ "Take the Shuriken ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ShurikenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shuriken ring!" ) {
			event.actor.grantRing( "17715", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSolarRaysMenu( player ) {
	titleString = "Solar Rays"
	descripString = "Focus the power of the sun into a beam that damages your foe and, at higher Rage Ranks, can knock it away from you, or even stun it to Sleep for a short time."
	diffOptions = [ "Take the Solar Rays ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SolarRaysMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Solar Rays ring!" ) {
			event.actor.grantRing( "17720", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	


//====================================================
// CROWD CONTROL RINGS                                
//====================================================
def makeCrowdControlMenu( player ) {
	titleString = "Crowd Control Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Duct Tape", "Gumshoe", "Quicksand", "Scaredy Cat", "Taunt", "Main Menu" ]
	
	uiButtonMenu( player, "crowdControlMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Duct Tape" ) {
			makeDuctTapeMenu( player )
		}
		if( event.selection == "Gumshoe" ) {
			makeGumshoeMenu( player )
		}
		if( event.selection == "Quicksand" ) {
			makeQuicksandMenu( player )
		}
		if( event.selection == "Scaredy Cat" ) {
			makeScaredyCatMenu( player )
		}
		if( event.selection == "Taunt" ) {
			makeTauntMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeDuctTapeMenu( player ) {
	titleString = "Duct Tape"
	descripString = "Wrap your target up and keep it from moving (Sleep). NOTE: Hitting a target while it is taped will weaken the tape and allow it to move again. Higher Rage Ranks start affecting foes around your original target also, as well as increasing the chance that they get bound by tape."
	diffOptions = [ "Take the Duct Tape ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DuctTapeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Duct Tape ring!" ) {
			event.actor.grantRing( "17722", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeGumshoeMenu( player ) {
	titleString = "Gumshoe"
	descripString = "Make the feet of your enemy sticky and slow its Footspeed substantially. Higher Rage Ranks make this ring affect increasingly-sized areas and slow the targets within even further."
	diffOptions = [ "Take the Gumshoe ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GumshoeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Gumshoe ring!" ) {
			event.actor.grantRing( "17743", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeQuicksandMenu( player ) {
	titleString = "Quicksand"
	descripString = "Cause the ground at an area to become mostly water and then solid mud for a short while, trapping your enemies where they stand. (Root). Higher Rage Ranks increase the area affected, as well as the chance to stick your foes in the mud."
	diffOptions = [ "Take the Quicksand ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "QuicksandMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Quicksand ring!" ) {
			event.actor.grantRing( "17723", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeScaredyCatMenu( player ) {
	titleString = "Scaredy Cat"
	descripString = "Make your foe flee from you in sheer panic! At higher Rage Ranks, this ring affects entire areas and the tendency for your foes to flee is bigger also."
	diffOptions = [ "Take the Scaredy Cat ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ScaredyCatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Scaredy Cat ring!" ) {
			event.actor.grantRing( "17725", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeTauntMenu( player ) {
	titleString = "Taunt"
	descripString = "Sometimes, you need to pull enemies away from your friends. This ring does the trick, making foes in an area angered at you for a while. Higher Rage Ranks increase the area affected and the strength of the Taunt. The highest Rage Ranks also make your foes tremble, draining their Dodge for a time."
	diffOptions = [ "Take the Taunt ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TauntMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Taunt ring!" ) {
			event.actor.grantRing( "17724", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// DEFENSE RINGS                                      
//====================================================
def makeDefenseMenu( player ) {
	titleString = "Defense Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Improbability Sphere", "Pot Lid", "Rock Armor", "Teflon Spray", "Turtle", "Main Menu" ]
	
	uiButtonMenu( player, "defenseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Improbability Sphere" ) {
			makeImprobabilitySphereMenu( player )
		}
		if( event.selection == "Pot Lid" ) {
			makePotLidMenu( player )
		}
		if( event.selection == "Rock Armor" ) {
			makeRockArmorMenu( player )
		}
		if( event.selection == "Teflon Spray" ) {
			makeTeflonSprayMenu( player )
		}
		if( event.selection == "Turtle" ) {
			makeTurtleMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeImprobabilitySphereMenu( player ) {
	titleString = "Improbability Sphere"
	descripString = "Use the Improbability Sphere to give you or a friend moderate defense (Persistent Armor), as well as to Reflect an attack back against the attacker of you or a friend! Any attack Reflected back on the attacker does the damage to the attacker instead. Higher Rage Ranks increase the amount of Armor and the...probability...that Reflection will occur."
	diffOptions = [ "Take the Improbability Sphere ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ImprobabilitySphereMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Improbability Sphere ring!" ) {
			event.actor.grantRing( "17730", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makePotLidMenu( player ) {
	titleString = "Pot Lid"
	descripString = "Use Pot Lid to give you or a friend moderate defense (Persistent Armor) and to sometimes Deflect an attack away from you or a friend completely. Any Deflected attack is nullified completely! Higher Rage Ranks make it more and more likely that a Deflection will occur on an attack."
	diffOptions = [ "Take the Pot Lid ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "PotLidMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Pot Lid ring!" ) {
			event.actor.grantRing( "17729", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeRockArmorMenu( player ) {
	titleString = "Rock Armor"
	descripString = "Cover each of your allies in Rock Armor giving them strong protection against incoming damage. (Armor Pool) The Rock Armor lasts for several minutes, or until it absorbs enough damage to break up. Higher Rage Ranks make stronger and stronger Armor."
	diffOptions = [ "Take the Rock Armor ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "RockArmorMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Rock Armor ring!" ) {
			event.actor.grantRing( "17728", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTeflonSprayMenu( player ) {
	titleString = "Teflon Spray"
	descripString = "This makes some of any incoming damage bounce away instead of hurting you or a friend (Persistent Armor). At higher Rage Ranks, it also makes your target harder to hit (Dodge) and eventually can occasionally Reflect an attack back against your foe."
	diffOptions = [ "Take the Teflon Spray ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TeflonSprayMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Teflon Spray ring!" ) {
			event.actor.grantRing( "17726", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTurtleMenu( player ) {
	titleString = "Turtle"
	descripString = "When trouble is overwhelming, the best thing to do is curl up in your shell and hope the bad things go away. This creates a protective field that can absorb an amazing amount of damage out of any incoming attack, but only lasts a short time. (Armor Pool) Higher Rage Ranks create stronger shells."
	diffOptions = [ "Take the Turtle ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TurtleMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Turtle ring!" ) {
			event.actor.grantRing( "17727", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// HEALING RINGS                                      
//====================================================
def makeHealingMenu( player ) {
	titleString = "Healing Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bandage", "Defibrillate", "Diagnose", "Divinity", "Healing Halo", "Meat", "Wish", "Main Menu" ]
	
	uiButtonMenu( player, "healingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bandage" ) {
			makeBandageMenu( player )
		}
		if( event.selection == "Defibrillate" ) {
			makeDefibrillateMenu( player )
		}
		if( event.selection == "Diagnose" ) {
			makeDiagnoseMenu( player )
		}
		if( event.selection == "Divinity" ) {
			makeDivinityMenu( player )
		}
		if( event.selection == "Healing Halo" ) {
			makeHealingHaloMenu( player )
		}
		if( event.selection == "Meat" ) {
			makeMeatMenu( player )
		}
		if( event.selection == "Wish" ) {
			makeWishMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBandageMenu( player ) {
	titleString = "Bandage"
	descripString = "Start a bandaging process on you or a friend. Healing then occurs over a short time, a bit at a time. Higher Rage Ranks increase the amount of Health recovered."
	diffOptions = [ "Take the Bandage ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "BandageMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bandage ring!" ) {
			event.actor.grantRing( "17732", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDefibrillateMenu( player ) {
	titleString = "Defibrillate"
	descripString = "Use this on a Dazed ally, and you'll instantly Awaken them. Higher Rage Ranks increase the amount of Health and Stamina recovered, as well as reducing the number of rings temporarily locked because you had been Dazed."
	diffOptions = [ "Take the Defibrillate ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DefibrillateMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Defibrillate ring!" ) {
			event.actor.grantRing( "17734", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDiagnoseMenu( player ) {
	titleString = "Diagnose"
	descripString = "You analyze the wounds of all allies in the area around you and heal them of some of their wounds, including your own! Higher Rage Ranks increase the healing effect and the area affected."
	diffOptions = [ "Take the Diagnose ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DiagnoseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Diagnose ring!" ) {
			event.actor.grantRing( "17733", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDivinityMenu( player ) {
	titleString = "Divinity"
	descripString = "Use this to draw lifeforce energy to you more quickly, increasing the rate at which you and your nearby friends regain Stamina, even during combat! Higher Rage Ranks let you recover Stamina even more quickly, and the highest Rage Ranks even help you find loot more easily. (Luck)"
	diffOptions = [ "Take the Divinity ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DivinityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Divinity ring!" ) {
			event.actor.grantRing( "17737", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHealingHaloMenu( player ) {
	titleString = "Healing Halo"
	descripString = "Create this halo over you and your nearby allies. You all then regenerate health more quickly, even during combat! Higher Rage Ranks increase this effect and the highest Rage Ranks also make all affected targets harder to knockback. (Weight)"
	diffOptions = [ "Take the Healing Halo ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HealingHaloMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Healing Halo ring!" ) {
			event.actor.grantRing( "17736", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMeatMenu( player ) {
	titleString = "Meat"
	descripString = "Be a meateater and beef up big and strong! You heal a big chunk of damage you've suffered as well as increasing your maximum Health the same amount. Higher Rage Ranks increase the amount of Health increased."
	diffOptions = [ "Take the Meat ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MeatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Meat ring!" ) {
			event.actor.grantRing( "17735", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeWishMenu( player ) {
	titleString = "Wish"
	descripString = "Heal any of your friends, one at a time with this quickly-recharging and powerful ring. Higher Rage Ranks heal targets standing around your target also. The bigger the Rage Rank, the bigger the area affected."
	diffOptions = [ "Take the Wish ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "WishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Wish ring!" ) {
			event.actor.grantRing( "17731", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// BUFFS                                              
//====================================================
def makeBuffMenu( player ) {
	titleString = "Buff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Coyote Spirit", "Fitness", "Fleet Feet", "Ghost", "Iron Will", "Keen Aye", "My Density", "Main Menu" ]
	
	uiButtonMenu( player, "buffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Coyote Spirit" ) {
			makeCoyoteSpiritMenu( player )
		}
		if( event.selection == "Fitness" ) {
			makeFitnessMenu( player )
		}
		if( event.selection == "Fleet Feet" ) {
			makeFleetFeetMenu( player )
		}
		if( event.selection == "Ghost" ) {
			makeGhostMenu( player )
		}
		if( event.selection == "Iron Will" ) {
			makeIronWillMenu( player )
		}
		if( event.selection == "Keen Aye" ) {
			makeKeenAyeMenu( player )
		}
		if( event.selection == "My Density" ) {
			makeMyDensityMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeCoyoteSpiritMenu( player ) {
	titleString = "Coyote Spirit"
	descripString = "Use this ring to give you or any friend a faster Footspeed. Higher Rage Ranks increase the Footspeed bonus, as well as providing you the Luck of the Coyote (Luck)."
	diffOptions = [ "Take the Coyote Spirit ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "CoyoteSpiritMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Coyote Spirit ring!" ) {
			event.actor.grantRing( "17738", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFitnessMenu( player ) {
	titleString = "Fitness"
	descripString = "When you wear this ring, you just get better! Accuracy, Dodge, Willpower, Weight, Health Regeneration, Stamina Regeneration and even Luck are all given minor bonuses. This ring is passive and does not need to be clicked to be fully functional. Just wear it and it works!"
	diffOptions = [ "Take the Fitness ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FitnessMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fitness ring!" ) {
			event.actor.grantRing( "17866", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFleetFeetMenu( player ) {
	titleString = "Fleet Feet"
	descripString = "Sometimes, you just need to get away. This makes you, and any friends around you, greatly increase your Footspeed for a brief time. Since you're probably running into or out of trouble, this also bolsters your Willpower with a modest bonus at higher Rage Ranks."
	diffOptions = [ "Take the Fleet Feet ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FleetFeetMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fleet Feet ring!" ) {
			event.actor.grantRing( "17749", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGhostMenu( player ) {
	titleString = "Ghost"
	descripString = "You become slightly ethereal and matter occasionally, err, passes through you in a fairly disturbing fashion. (Dodge) Higher Rage Ranks increase the amount of Dodge bonus you receive. (Dodge bonuses also decrease the chance that a monster will Critical Hit you during a fight.)"
	diffOptions = [ "Take the Ghost ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GhostMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Ghost ring!" ) {
			event.actor.grantRing( "17742", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeIronWillMenu( player ) {
	titleString = "Iron Will"
	descripString = "When you fight a foe using Sleep, Root, Fear or other Willpower-based ability, Iron Will erects defenses around your mind (or the minds of any of your friends) to help you resist their evil influence. Higher Rage Ranks amplifies your mind still further, allowing you to Deflect occasional incoming attacks."
	diffOptions = [ "Take the Iron Will ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "IronWillMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Iron Will ring!" ) {
			event.actor.grantRing( "17744", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKeenAyeMenu( player ) {
	titleString = "Keen Aye"
	descripString = "Use this on you or a friend to help them spy out where a foe *will* be, letting you hit it more easily. (Accuracy) Higher Rage Ranks increase the Accuracy boost. (Accuracy bonuses also increase the chance that you will Critical Hit a monster on any particular attack.)"
	diffOptions = [ "Take the Keen Aye ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KeenAyeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Keen Aye ring!" ) {
			event.actor.grantRing( "17740", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMyDensityMenu( player ) {
	titleString = "My Density"
	descripString = "Are you getting knocked around by monsters? There's an easy way to solve that. Weigh more! Using this ring increases your Weight and sticks you to the ground. Higher Rage Ranks actually make you dense enough to resist some damage directly! (Persistent Armor)"
	diffOptions = [ "Take the My Density ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MyDensityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the My Density ring!" ) {
			event.actor.grantRing( "17745", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// DEBUFFS                                            
//====================================================
def makeDebuffMenu( player ) {
	titleString = "Debuff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Adrenaline", "Knife Sharpen", "Main Menu" ]
	
	uiButtonMenu( player, "debuffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Adrenaline" ) {
			makeAdrenalineMenu( player )
		}
		if( event.selection == "Knife Sharpen" ) {
			makeKnifeSharpenMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeAdrenalineMenu( player ) {
	titleString = "Adrenaline"
	descripString = "You jump up the nerves of your foe, causing them to jitter and shake, spoiling their ability to Dodge your blows for a time and causing them some damage. Higher Rage Ranks increase the Dodge penalty and deal more damage."
	diffOptions = [ "Take the Adrenaline ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "AdrenalineMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Adrenaline ring!" ) {
			event.actor.grantRing( "17741", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKnifeSharpenMenu( player ) {
	titleString = "Knife Sharpen"
	descripString = "You draw the keen edge from a foe's G'hi and use it to sharpen your own metaphorical knives. Your foe suffers an Accuracy drain for a short time as you disrupt its lifeforce and suffers some damage. Higher Rage Ranks increase the Accuracy penalty and deal more damage."
	diffOptions = [ "Take the Knife Sharpen ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KnifeSharpenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Knife Sharpen ring!" ) {
			event.actor.grantRing( "17739", CL, CLdecimal, true )
			player = event.actor
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

//====================================================
// RING GRANT END LOGIC                               
//====================================================

def endRingGrant( event ) {
	event.actor.setQuestFlag( GLOBAL, "Z07CaruthersRingGrantReceived" )
}



