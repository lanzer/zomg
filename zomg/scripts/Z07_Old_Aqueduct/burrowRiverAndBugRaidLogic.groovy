import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// BURROW ONE & SCOUT                       
//------------------------------------------

p3BurrowOne = myRooms.Aqueduct_206.spawnSpawner( "p3BurrowOne", "p3", 4 )
p3BurrowOne.setPos( 240, 340 )
p3BurrowOne.setWaitTime( 5, 10 )
p3BurrowOne.setHomeTetherForChildren( 3000 )
p3BurrowOne.setHateRadiusForChildren( 3000 )
p3BurrowOne.setWanderBehaviorForChildren( 50, 150, 4, 9, 250)
p3BurrowOne.feudWithMonsterType( "alien_bug" )
p3BurrowOne.feudWithMonsterType( "alien_bug_LT" )
p3BurrowOne.feudWithMonsterType( "alien_walker" )
p3BurrowOne.feudWithMonsterType( "alien_walker_LT" )
p3BurrowOne.setMonsterLevelForChildren( 5.3 )

def respawnBurrowOne() {
	if( myRooms.Aqueduct_206.getSpawnTypeCount( "p3" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			p3BurrowOne.spawnAllNow()
			myManager.schedule( 15 ) { respawnBurrowOne() }
		}
	} else {
		myManager.schedule( 10 ) { respawnBurrowOne() }
	}
}

def p3BurrowOneScout = myRooms.Aqueduct_206.spawnSpawner( "p3BurrowOneScout", "p3", 1 )
p3BurrowOneScout.setPos( 240, 340 )
p3BurrowOneScout.setWaitTime( 60, 90 )
p3BurrowOneScout.setHomeTetherForChildren( 3000 )
p3BurrowOneScout.setHateRadiusForChildren( 3000 )
p3BurrowOneScout.setRFH( true )
p3BurrowOneScout.setCFH( 500 )
p3BurrowOneScout.setDispositionForChildren( "coward" )
p3BurrowOneScout.setCowardLevelForChildren( 100 )
p3BurrowOneScout.feudWithMonsterType( "alien_bug" )
p3BurrowOneScout.feudWithMonsterType( "alien_bug_LT" )
p3BurrowOneScout.feudWithMonsterType( "alien_walker" )
p3BurrowOneScout.feudWithMonsterType( "alien_walker_LT" )
p3BurrowOneScout.setMonsterLevelForChildren( 5.3 )
p3BurrowOneScout.addPatrolPointForChildren( "Aqueduct_206", 150, 400, 0 )
p3BurrowOneScout.addPatrolPointForChildren( "Aqueduct_205", 850, 170, 0 )
p3BurrowOneScout.addPatrolPointForChildren( "Aqueduct_105", 850, 490, 0 )
p3BurrowOneScout.addPatrolPointForChildren( "Aqueduct_106", 700, 405, 0 )
p3BurrowOneScout.addPatrolPointForChildren( "Aqueduct_107", 435, 300, 0 )
p3BurrowOneScout.addPatrolPointForChildren( "Aqueduct_107", 660, 450, 0 )
p3BurrowOneScout.addPatrolPointForChildren( "Aqueduct_207", 775, 215, 0 )
p3BurrowOneScout.addPatrolPointForChildren( "Aqueduct_207", 520, 450, 0 )
p3BurrowOneScout.addPatrolPointForChildren( "Aqueduct_206", 425, 510, 0 )

//------------------------------------------
// BURROW ONE - SUMMONING ASSISTANCE        
//------------------------------------------
// To balance the room for more players, we 
// need to spawn assistance from the LTOne  
// burrow to help us if there are too many  
// players.                                 
//------------------------------------------

playersIn206 = 0
burrowOneRunnerRespawnOkay = true

burrowOneRunner = null
LTOneReadySquadA = null
LTOneReadySquadB = null
LTOneReadySquadC = null
LTOneReadySquadLeader = null

//Count the players as they come in and out of the room
myManager.onEnter(myRooms.Aqueduct_206) { event ->
	if( isPlayer(event.actor) ) {
		playersIn206 ++
	}
}

myManager.onExit(myRooms.Aqueduct_206) { event ->
	if( isPlayer(event.actor) ) {
		playersIn206 --
	}
}

//This trips the whole mess. If no one has touched the Burrow, then there's no reason to cause an alarm.
//However, once they start dying, they start trying to figure out if the players are a threat or not.
def checkBurrowOneAlarm() {
	if( myRooms.Aqueduct_206.getSpawnTypeCount( "p3" ) < 4 ) {
		checkHelpForBurrowOne()
	} else {
		myManager.schedule(2) { checkBurrowOneAlarm() }
	}
}

p3BurrowOneRunner = myRooms.Aqueduct_206.spawnSpawner( "p3BurrowOneRunner", "p3", 1 )
p3BurrowOneRunner.setPos( 240, 340 )
p3BurrowOneRunner.setHomeTetherForChildren( 3000 )
p3BurrowOneRunner.setHateRadiusForChildren( 3000 )
p3BurrowOneRunner.feudWithMonsterType( "alien_bug" )
p3BurrowOneRunner.feudWithMonsterType( "alien_bug_LT" )
p3BurrowOneRunner.feudWithMonsterType( "alien_walker" )
p3BurrowOneRunner.feudWithMonsterType( "alien_walker_LT" )
p3BurrowOneRunner.setMonsterLevelForChildren( 5.3 )
p3BurrowOneRunner.addPatrolPointForChildren( "Aqueduct_206", 75, 400, 0 )
p3BurrowOneRunner.addPatrolPointForChildren( "Aqueduct_205", 905, 550, 0 )
p3BurrowOneRunner.addPatrolPointForChildren( "Aqueduct_305", 315, 505, 0 )

//Basically...if there's more players than pups, then it's time to send for help!
def checkHelpForBurrowOne() {
	if( playersIn206 > myRooms.Aqueduct_206.getSpawnTypeCount( "p3" ) && burrowOneRunnerRespawnOkay == true ) {
		if( p3BurrowOneRunner.spawnsInUse() == 0 ) {
			burrowOneRunner = p3BurrowOneRunner.forceSpawnNow()
			burrowOneRunner.say( "Zrrk! Kee-tr^k plik!" ) //Zrrk = Shit!. Kee-tr^k plik! = I'll get help! (Plik! = Help!)
			burrowOneRunnerRespawnOkay = false
			burrowOneRunnerDeathWatch()
		}
	} else {
		myManager.schedule(2) { checkBurrowOneAlarm() }
	}
}

//Watch the Runner. If he dies, then reset him so he can respawn again later. NOTE: If he is disposed of at the LTOne burrow, then his reset occurs at that point instead.
def burrowOneRunnerDeathWatch() {
	runOnDeath( burrowOneRunner, { burrowOneRunner = null; resetBurrowOneRunner() } )
}

//Reset the Runner if the Runner is killed or if the ReadySquad is all killed or returns to their burrow
def resetBurrowOneRunner() {
	if( myRooms.Aqueduct_206.getSpawnTypeCount( "p3" ) > 1 ) {
		burrowOneRunnerRespawnOkay = true
		checkBurrowOneAlarm()
	} else {
		myManager.schedule(5) { resetBurrowOneRunner() } //keeps checking until at least two other P3s are in the room also
	}
}

p3ReadySquadOneA = myRooms.Aqueduct_305.spawnSpawner( "p3ReadySquadOneA", "p3", 1 )
p3ReadySquadOneA.setPos( 205, 470 )
p3ReadySquadOneA.setHomeTetherForChildren( 3000 )
p3ReadySquadOneA.setHateRadiusForChildren( 3000 )
p3ReadySquadOneA.feudWithMonsterType( "alien_bug" )
p3ReadySquadOneA.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadOneA.feudWithMonsterType( "alien_walker" )
p3ReadySquadOneA.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadOneA.setMonsterLevelForChildren( 5.5 )
p3ReadySquadOneA.addPatrolPointForChildren( "Aqueduct_305", 700, 270, 0 )
p3ReadySquadOneA.addPatrolPointForChildren( "Aqueduct_205", 920, 520, 0 )
p3ReadySquadOneA.addPatrolPointForChildren( "Aqueduct_206", 250, 330, 0 )
p3ReadySquadOneA.addPatrolPointForChildren( "Aqueduct_206", 640, 400, 0 )
p3ReadySquadOneA.addPatrolPointForChildren( "Aqueduct_206", 300, 600, 0 )
p3ReadySquadOneA.addPatrolPointForChildren( "Aqueduct_205", 920, 520, 0 )
p3ReadySquadOneA.addPatrolPointForChildren( "Aqueduct_305", 315, 470, 10 )

p3ReadySquadOneB = myRooms.Aqueduct_305.spawnSpawner( "p3ReadySquadOneB", "p3", 1 )
p3ReadySquadOneB.setPos( 345, 485 )
p3ReadySquadOneB.setHomeTetherForChildren( 3000 )
p3ReadySquadOneB.setHateRadiusForChildren( 3000 )
p3ReadySquadOneB.feudWithMonsterType( "alien_bug" )
p3ReadySquadOneB.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadOneB.feudWithMonsterType( "alien_walker" )
p3ReadySquadOneB.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadOneB.setMonsterLevelForChildren( 5.5 )
p3ReadySquadOneB.addPatrolPointForChildren( "Aqueduct_305", 700, 270, 0 )
p3ReadySquadOneB.addPatrolPointForChildren( "Aqueduct_205", 920, 520, 0 )
p3ReadySquadOneB.addPatrolPointForChildren( "Aqueduct_206", 250, 330, 0 )
p3ReadySquadOneB.addPatrolPointForChildren( "Aqueduct_206", 640, 400, 0 )
p3ReadySquadOneB.addPatrolPointForChildren( "Aqueduct_206", 300, 600, 0 )
p3ReadySquadOneB.addPatrolPointForChildren( "Aqueduct_205", 920, 520, 0 )
p3ReadySquadOneB.addPatrolPointForChildren( "Aqueduct_305", 315, 470, 10 )

p3ReadySquadOneC = myRooms.Aqueduct_305.spawnSpawner( "p3ReadySquadOneC", "p3", 1 )
p3ReadySquadOneC.setPos( 500, 580 )
p3ReadySquadOneC.setHomeTetherForChildren( 3000 )
p3ReadySquadOneC.setHateRadiusForChildren( 3000 )
p3ReadySquadOneC.setMonsterLevelForChildren( 5.5 )
p3ReadySquadOneC.feudWithMonsterType( "alien_bug" )
p3ReadySquadOneC.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadOneC.feudWithMonsterType( "alien_walker" )
p3ReadySquadOneC.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadOneC.addPatrolPointForChildren( "Aqueduct_305", 700, 270, 0 )
p3ReadySquadOneC.addPatrolPointForChildren( "Aqueduct_205", 920, 520, 0 )
p3ReadySquadOneC.addPatrolPointForChildren( "Aqueduct_206", 250, 330, 0 )
p3ReadySquadOneC.addPatrolPointForChildren( "Aqueduct_206", 640, 400, 0 )
p3ReadySquadOneC.addPatrolPointForChildren( "Aqueduct_206", 300, 600, 0 )
p3ReadySquadOneC.addPatrolPointForChildren( "Aqueduct_205", 920, 520, 0 )
p3ReadySquadOneC.addPatrolPointForChildren( "Aqueduct_305", 315, 470, 10 )

p3ReadySquadOneLeader = myRooms.Aqueduct_305.spawnSpawner( "p3ReadySquadOneLeader", "p3", 1 )
p3ReadySquadOneLeader.setPos( 480, 480 )
p3ReadySquadOneLeader.setHomeTetherForChildren( 3000 )
p3ReadySquadOneLeader.setHateRadiusForChildren( 3000 )
p3ReadySquadOneLeader.feudWithMonsterType( "alien_bug" )
p3ReadySquadOneLeader.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadOneLeader.feudWithMonsterType( "alien_walker" )
p3ReadySquadOneLeader.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadOneLeader.setMonsterLevelForChildren( 5.5 )
p3ReadySquadOneLeader.addPatrolPointForChildren( "Aqueduct_305", 700, 270, 0 )
p3ReadySquadOneLeader.addPatrolPointForChildren( "Aqueduct_205", 920, 520, 0 )
p3ReadySquadOneLeader.addPatrolPointForChildren( "Aqueduct_206", 250, 330, 0 )
p3ReadySquadOneLeader.addPatrolPointForChildren( "Aqueduct_206", 640, 400, 0 )
p3ReadySquadOneLeader.addPatrolPointForChildren( "Aqueduct_206", 300, 600, 0 )
p3ReadySquadOneLeader.addPatrolPointForChildren( "Aqueduct_205", 920, 520, 0 )
p3ReadySquadOneLeader.addPatrolPointForChildren( "Aqueduct_305", 315, 470, 10 )

//This is the "DISPOSE" trigger zone at the LTOne burrow
def LTOneRunnerTrigger = "LTOneRunnerTrigger"
myRooms.Aqueduct_305.createTriggerZone(LTOneRunnerTrigger, 240, 440, 430, 560)

//This removes the Runner once he gets to the LTOne burrow (as if he went "inside" the burrow), AND it spawns the Ready Squad three seconds thereafter (as if they came out in response to the Runner).
myManager.onTriggerIn(myRooms.Aqueduct_305, LTOneRunnerTrigger) { event ->
	if( event.actor == burrowOneRunner ) {
		LTOneReadySquadPatrolFlag = false
		burrowOneRunner.dispose()
		myManager.schedule(3) { LTOneReadySquadA = p3ReadySquadOneA.forceSpawnNow() }
		myManager.schedule(4) { LTOneReadySquadB = p3ReadySquadOneB.forceSpawnNow() }
		myManager.schedule(5) { LTOneReadySquadC = p3ReadySquadOneC.forceSpawnNow() }
		myManager.schedule(6) { LTOneReadySquadLeader = p3ReadySquadOneLeader.forceSpawnNow(); LTOneReadySquadLeader.say( "Q! Kzrelt...az^kree!!" ); startLTOneReadySquadPatrol() }
	}
	if( event.actor == LTOneReadySquadA && LTOneReadySquadPatrolFlag == true ) {
		LTOneReadySquadA.dispose() //These dispose statements occur if the patrol goes to BurrowOne, finds nothing and returns on their patrol path. They "go inside" if they found nothing out there to fight.
		LTOneReadySquadA = null
	}
	if( event.actor == LTOneReadySquadB && LTOneReadySquadPatrolFlag == true ) {
		LTOneReadySquadB.dispose()
		LTOneReadySquadB = null
	}
	if( event.actor == LTOneReadySquadC && LTOneReadySquadPatrolFlag == true ) {
		LTOneReadySquadC.dispose()
		LTOneReadySquadC = null
	}
	if( event.actor == LTOneReadySquadLeader && LTOneReadySquadPatrolFlag == true ) {
		LTOneReadySquadLeader.dispose()
		LTOneReadySquadLeader = null
	}
}

//Once the Ready Squad is spawned, put them on patrol and start them on their way.
def startLTOneReadySquadPatrol() {
	checkLTOneReadySquadStatus()
	myManager.schedule(15) { LTOneReadySquadPatrolFlag = true } //to prevent them from disappearing right away while spawning in the trigger zone
}	

//Once the Ready Squad is spawned, we need to check to see if they are killed OR if they are disposed of neatly after returning from their patrol.
def checkLTOneReadySquadStatus() {
	if( ( LTOneReadySquadA == null || LTOneReadySquadA.isDead() ) && ( LTOneReadySquadB == null || LTOneReadySquadB.isDead() )  &&  ( LTOneReadySquadC == null || LTOneReadySquadC.isDead() ) &&  ( LTOneReadySquadLeader == null || LTOneReadySquadLeader.isDead() ) ) {
		resetBurrowOneRunner()
	} else {
		myManager.schedule(5) { checkLTOneReadySquadStatus() }
	}
}
	
//==========================
//ALLIANCES                 
//==========================

p3BurrowOne.allyWithSpawner( p3BurrowOneScout )
p3ReadySquadOneA.allyWithSpawner( p3ReadySquadOneB )
p3ReadySquadOneA.allyWithSpawner( p3ReadySquadOneC )
p3ReadySquadOneA.allyWithSpawner( p3ReadySquadOneLeader )
p3ReadySquadOneB.allyWithSpawner( p3ReadySquadOneC )
p3ReadySquadOneB.allyWithSpawner( p3ReadySquadOneLeader )
p3ReadySquadOneC.allyWithSpawner( p3ReadySquadOneLeader )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

p3BurrowOne.spawnAllNow()
p3BurrowOne.stopSpawning()

p3BurrowOneRunner.stopSpawning()
p3ReadySquadOneA.stopSpawning()
p3ReadySquadOneB.stopSpawning()
p3ReadySquadOneC.stopSpawning()
p3ReadySquadOneLeader.stopSpawning()

p3BurrowOneScout.forceSpawnNow()

respawnBurrowOne()

checkBurrowOneAlarm()



//------------------------------------------
// BURROW TWO & SCOUT                       
//------------------------------------------

p3BurrowTwo = myRooms.Aqueduct_406.spawnSpawner( "p3BurrowTwo", "p3", 3 )
p3BurrowTwo.setPos( 460, 100 )
p3BurrowTwo.setWaitTime( 5, 10 )
p3BurrowTwo.setHomeTetherForChildren( 3000 )
p3BurrowTwo.setHateRadiusForChildren( 3000 )
p3BurrowTwo.setWanderBehaviorForChildren( 50, 150, 4, 9, 200)
p3BurrowTwo.setMonsterLevelForChildren( 5.5 )

def respawnBurrowTwo() {
	if( myRooms.Aqueduct_406.getSpawnTypeCount( "p3" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			p3BurrowTwo.spawnAllNow();
			myManager.schedule( 15 ) { respawnBurrowTwo() }
		}
	} else {
		myManager.schedule( 10 ) { respawnBurrowTwo() }
	}
}

//------------------------------------------
// BURROW TWO - SUMMONING ASSISTANCE        
//------------------------------------------
// To balance the room for more players, we 
// need to spawn assistance from the LTTwo  
// burrow to help us if there are too many  
// players.                                 
//------------------------------------------

playersIn406 = 0
burrowTwoRunnerRespawnOkay = true

burrowTwoRunner = null
LTTwoReadySquadA = null
LTTwoReadySquadB = null
LTTwoReadySquadC = null
LTTwoReadySquadLeader = null

//Count the players as they come in and out of the room
myManager.onEnter(myRooms.Aqueduct_406) { event ->
	if( isPlayer(event.actor) ) {
		playersIn406 ++
	}
}

myManager.onExit(myRooms.Aqueduct_406) { event ->
	if( isPlayer(event.actor) ) {
		playersIn406 --
	}
}

//This trips the whole mess. If no one has touched the Burrow, then there's no reason to cause an alarm. However, once they start dying, they start trying to figure out if the players are a threat or not.
def checkBurrowTwoAlarm() {
	if( myRooms.Aqueduct_406.getSpawnTypeCount( "p3" ) < 3 ) {
		checkHelpForBurrowTwo()
	} else {
		myManager.schedule(2) { checkBurrowTwoAlarm() }
	}
}

p3BurrowTwoRunner = myRooms.Aqueduct_406.spawnSpawner( "p3BurrowTwoRunner", "p3", 1 )
p3BurrowTwoRunner.setPos( 460, 100 )
p3BurrowTwoRunner.setHomeTetherForChildren( 3000 )
p3BurrowTwoRunner.setHateRadiusForChildren( 3000 )
p3BurrowTwoRunner.setMonsterLevelForChildren( 5.5 )
p3BurrowTwoRunner.feudWithMonsterType( "alien_bug" )
p3BurrowTwoRunner.feudWithMonsterType( "alien_bug_LT" )
p3BurrowTwoRunner.feudWithMonsterType( "alien_walker" )
p3BurrowTwoRunner.feudWithMonsterType( "alien_walker_LT" )
p3BurrowTwoRunner.addPatrolPointForChildren( "Aqueduct_406", 145, 190, 0 )
p3BurrowTwoRunner.addPatrolPointForChildren( "Aqueduct_405", 840, 250, 10 )

//Basically...if there's more players than pups, then it's time to send for help!
def checkHelpForBurrowTwo() {
	if( playersIn406 > myRooms.Aqueduct_406.getSpawnTypeCount( "p3" ) && burrowTwoRunnerRespawnOkay == true ) {
		if( p3BurrowTwoRunner.spawnsInUse() == 0 ) {	
			burrowTwoRunner = p3BurrowTwoRunner.forceSpawnNow()
			burrowTwoRunner.say( "Zrrk! Kee-tr^k plik!" ) //Zrrk = Shit!. Kee-tr^k plik! = I'll get help! (Plik! = Help!)
			burrowTwoRunnerRespawnOkay = false
			burrowTwoRunnerDeathWatch()
		}
	} else {
		myManager.schedule(2) { checkBurrowTwoAlarm() }
	}
}

//Watch the Runner. If he dies, then reset him so he can respawn again later. NOTE: If he is disposed of at the LTTwo burrow, then his reset occurs at that point instead.
def burrowTwoRunnerDeathWatch() {
	runOnDeath( burrowTwoRunner, { burrowTwoRunner = null; resetBurrowTwoRunner() } )
}

//Reset the Runner if the Runner is killed or if the ReadySquad is all killed or returns to their burrow
def resetBurrowTwoRunner() {
	if( myRooms.Aqueduct_406.getSpawnTypeCount( "p3" ) > 1 ) {
		burrowTwoRunnerRespawnOkay = true
		checkBurrowTwoAlarm()
	} else {
		myManager.schedule(5) { resetBurrowTwoRunner() } //keeps checking until at least two other P3s are in the room also
	}
}

p3ReadySquadTwoA = myRooms.Aqueduct_405.spawnSpawner( "p3ReadySquadTwoA", "p3", 1 )
p3ReadySquadTwoA.setPos( 610, 185 )
p3ReadySquadTwoA.setHomeTetherForChildren( 3000 )
p3ReadySquadTwoA.setHateRadiusForChildren( 3000 )
p3ReadySquadTwoA.setMonsterLevelForChildren( 5.6 )
p3ReadySquadTwoA.feudWithMonsterType( "alien_bug" )
p3ReadySquadTwoA.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadTwoA.feudWithMonsterType( "alien_walker" )
p3ReadySquadTwoA.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadTwoA.addPatrolPointForChildren( "Aqueduct_405", 950, 250, 0 )
p3ReadySquadTwoA.addPatrolPointForChildren( "Aqueduct_406", 460, 60, 0 )
p3ReadySquadTwoA.addPatrolPointForChildren( "Aqueduct_406", 635, 215, 0 )
p3ReadySquadTwoA.addPatrolPointForChildren( "Aqueduct_406", 460, 345, 0 )
p3ReadySquadTwoA.addPatrolPointForChildren( "Aqueduct_405", 700, 190, 10 )

p3ReadySquadTwoB = myRooms.Aqueduct_405.spawnSpawner( "p3ReadySquadTwoB", "p3", 1 )
p3ReadySquadTwoB.setPos( 670, 150 )
p3ReadySquadTwoB.setHomeTetherForChildren( 3000 )
p3ReadySquadTwoB.setHateRadiusForChildren( 3000 )
p3ReadySquadTwoB.setMonsterLevelForChildren( 5.6 )
p3ReadySquadTwoB.feudWithMonsterType( "alien_bug" )
p3ReadySquadTwoB.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadTwoB.feudWithMonsterType( "alien_walker" )
p3ReadySquadTwoB.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadTwoB.addPatrolPointForChildren( "Aqueduct_405", 950, 250, 0 )
p3ReadySquadTwoB.addPatrolPointForChildren( "Aqueduct_406", 460, 60, 0 )
p3ReadySquadTwoB.addPatrolPointForChildren( "Aqueduct_406", 635, 215, 0 )
p3ReadySquadTwoB.addPatrolPointForChildren( "Aqueduct_406", 460, 345, 0 )
p3ReadySquadTwoB.addPatrolPointForChildren( "Aqueduct_405", 700, 190, 10 )

p3ReadySquadTwoC = myRooms.Aqueduct_405.spawnSpawner( "p3ReadySquadTwoC", "p3", 1 )
p3ReadySquadTwoC.setPos( 745, 150 )
p3ReadySquadTwoC.setHomeTetherForChildren( 3000 )
p3ReadySquadTwoC.setHateRadiusForChildren( 3000 )
p3ReadySquadTwoC.setMonsterLevelForChildren( 5.6 )
p3ReadySquadTwoC.feudWithMonsterType( "alien_bug" )
p3ReadySquadTwoC.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadTwoC.feudWithMonsterType( "alien_walker" )
p3ReadySquadTwoC.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadTwoC.addPatrolPointForChildren( "Aqueduct_405", 950, 250, 0 )
p3ReadySquadTwoC.addPatrolPointForChildren( "Aqueduct_406", 460, 60, 0 )
p3ReadySquadTwoC.addPatrolPointForChildren( "Aqueduct_406", 635, 215, 0 )
p3ReadySquadTwoC.addPatrolPointForChildren( "Aqueduct_406", 460, 345, 0 )
p3ReadySquadTwoC.addPatrolPointForChildren( "Aqueduct_405", 700, 190, 10 )

p3ReadySquadTwoLeader = myRooms.Aqueduct_405.spawnSpawner( "p3ReadySquadTwoLeader", "p3", 1 )
p3ReadySquadTwoLeader.setPos( 770, 185 )
p3ReadySquadTwoLeader.setHomeTetherForChildren( 3000 )
p3ReadySquadTwoLeader.setHateRadiusForChildren( 3000 )
p3ReadySquadTwoLeader.setMonsterLevelForChildren( 5.6 )
p3ReadySquadTwoLeader.feudWithMonsterType( "alien_bug" )
p3ReadySquadTwoLeader.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadTwoLeader.feudWithMonsterType( "alien_walker" )
p3ReadySquadTwoLeader.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadTwoLeader.addPatrolPointForChildren( "Aqueduct_405", 950, 250, 0 )
p3ReadySquadTwoLeader.addPatrolPointForChildren( "Aqueduct_406", 460, 60, 0 )
p3ReadySquadTwoLeader.addPatrolPointForChildren( "Aqueduct_406", 635, 215, 0 )
p3ReadySquadTwoLeader.addPatrolPointForChildren( "Aqueduct_406", 460, 345, 0 )
p3ReadySquadTwoLeader.addPatrolPointForChildren( "Aqueduct_405", 700, 190, 10 )

//This is the "DISPOSE" trigger zone at the LTTwo burrow
def LTTwoRunnerTrigger = "LTTwoRunnerTrigger"
myRooms.Aqueduct_405.createTriggerZone(LTTwoRunnerTrigger, 645, 130, 880, 335)

//This removes the Runner once he gets to the LTTwo burrow (as if he went "inside" the burrow), AND it spawns the Ready Squad three seconds thereafter (as if they came out in response to the Runner).
myManager.onTriggerIn(myRooms.Aqueduct_405, LTTwoRunnerTrigger) { event ->
	if( event.actor == burrowTwoRunner ) {
		LTTwoReadySquadPatrolFlag = false
		burrowTwoRunner.dispose()
		myManager.schedule(3) { LTTwoReadySquadA = p3ReadySquadTwoA.forceSpawnNow() }
		myManager.schedule(4) { LTTwoReadySquadB = p3ReadySquadTwoB.forceSpawnNow() }
		myManager.schedule(5) { LTTwoReadySquadC = p3ReadySquadTwoC.forceSpawnNow() }
		myManager.schedule(6) { LTTwoReadySquadLeader = p3ReadySquadTwoLeader.forceSpawnNow(); LTTwoReadySquadLeader.say( "Q! Kzrelt...az^kree!!" ); startLTTwoReadySquadPatrol() }
	}
	if( event.actor == LTTwoReadySquadA && LTTwoReadySquadPatrolFlag == true ) {
		LTTwoReadySquadA.dispose() //These dispose statements occur if the patrol goes to BurrowTwo, finds nothing and returns on their patrol path. They "go inside" if they found nothing out there to fight.
		LTTwoReadySquadA = null
	}
	if( event.actor == LTTwoReadySquadB && LTTwoReadySquadPatrolFlag == true ) {
		LTTwoReadySquadB.dispose()
		LTTwoReadySquadB = null
	}
	if( event.actor == LTTwoReadySquadC && LTTwoReadySquadPatrolFlag == true ) {
		LTTwoReadySquadC.dispose()
		LTTwoReadySquadC = null
	}
	if( event.actor == LTTwoReadySquadLeader && LTTwoReadySquadPatrolFlag == true ) {
		LTTwoReadySquadLeader.dispose()
		LTTwoReadySquadLeader = null
	}
}

//Once the Ready Squad is spawned, put them on patrol and start them on their way.
def startLTTwoReadySquadPatrol() {
	checkLTTwoReadySquadStatus()
	myManager.schedule(15) { LTTwoReadySquadPatrolFlag = true } //to prevent them from disappearing right away while spawning in the trigger zone
}	

//Once the Ready Squad is spawned, we need to check to see if they are killed OR if they are disposed of neatly after returning from their patrol.
def checkLTTwoReadySquadStatus() {
	if( ( LTTwoReadySquadA == null || LTTwoReadySquadA.isDead() ) && ( LTTwoReadySquadB == null || LTTwoReadySquadB.isDead() )  &&  ( LTTwoReadySquadC == null || LTTwoReadySquadC.isDead() ) &&  ( LTTwoReadySquadLeader == null || LTTwoReadySquadLeader.isDead() ) ) {
		resetBurrowTwoRunner()
	} else {
		myManager.schedule(5) { checkLTTwoReadySquadStatus() }
	}
}

//====================================
// ALLIANCES                          
//====================================
p3ReadySquadTwoA.allyWithSpawner( p3ReadySquadTwoB )
p3ReadySquadTwoA.allyWithSpawner( p3ReadySquadTwoC )
p3ReadySquadTwoA.allyWithSpawner( p3ReadySquadTwoLeader )
p3ReadySquadTwoB.allyWithSpawner( p3ReadySquadTwoC )
p3ReadySquadTwoB.allyWithSpawner( p3ReadySquadTwoLeader )
p3ReadySquadTwoC.allyWithSpawner( p3ReadySquadTwoLeader )


//====================================
// INITIAL LOGIC                      
//====================================

p3BurrowTwo.spawnAllNow()
p3BurrowTwo.stopSpawning()

p3BurrowTwoRunner.stopSpawning()
p3ReadySquadTwoA.stopSpawning()
p3ReadySquadTwoB.stopSpawning()
p3ReadySquadTwoC.stopSpawning()
p3ReadySquadTwoLeader.stopSpawning()

respawnBurrowTwo()

checkBurrowTwoAlarm()




//------------------------------------------
// BURROW THREE & SCOUT                     
//------------------------------------------

p3BurrowThree = myRooms.Aqueduct_403.spawnSpawner( "p3BurrowThree", "p3", 3 )
p3BurrowThree.setPos( 820, 400 )
p3BurrowThree.setWaitTime( 5, 10 )
p3BurrowThree.setHomeTetherForChildren( 3000 )
p3BurrowThree.setHateRadiusForChildren( 3000 )
p3BurrowThree.setWanderBehaviorForChildren( 50, 150, 4, 9, 200)
p3BurrowThree.feudWithMonsterType( "alien_bug" )
p3BurrowThree.feudWithMonsterType( "alien_bug_LT" )
p3BurrowThree.feudWithMonsterType( "alien_walker" )
p3BurrowThree.feudWithMonsterType( "alien_walker_LT" )
p3BurrowThree.setMonsterLevelForChildren( 5.9 )

def respawnBurrowThree() {
	if( myRooms.Aqueduct_403.getSpawnTypeCount( "p3" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			p3BurrowThree.spawnAllNow();
			myManager.schedule( 15 ) { respawnBurrowThree() }
		}
	} else {
		myManager.schedule( 10 ) { respawnBurrowThree() }
	}
}

//------------------------------------------
// BURROW THREE - SUMMONING ASSISTANCE      
//------------------------------------------
// To balance the room for more players, we 
// need to spawn assistance from the LTThree
// burrow to help us if there are too many  
// players.                                 
//------------------------------------------

playersIn403 = 0
burrowThreeRunnerRespawnOkay = true

burrowThreeRunner = null
LTThreeReadySquadA = null
LTThreeReadySquadB = null
LTThreeReadySquadC = null
LTThreeReadySquadLeader = null

//Count the players as they come in and out of the room
myManager.onEnter(myRooms.Aqueduct_403) { event ->
	if( isPlayer(event.actor) ) {
		playersIn403 ++
	}
}

myManager.onExit(myRooms.Aqueduct_403) { event ->
	if( isPlayer(event.actor) ) {
		playersIn403 --
	}
}

//This trips the whole mess. If no one has touched the Burrow, then there's no reason to cause an alarm. However, once they start dying, they start trying to figure out if the players are a threat or not.
def checkBurrowThreeAlarm() {
	if( myRooms.Aqueduct_403.getSpawnTypeCount( "p3" ) < 3 ) {
		checkHelpForBurrowThree()
	} else {
		myManager.schedule(2) { checkBurrowThreeAlarm() }
	}
}

p3BurrowThreeRunner = myRooms.Aqueduct_403.spawnSpawner( "p3BurrowThreeRunner", "p3", 1 )
p3BurrowThreeRunner.setPos( 820, 400 )
p3BurrowThreeRunner.setHomeTetherForChildren( 3000 )
p3BurrowThreeRunner.setHateRadiusForChildren( 3000 )
p3BurrowThreeRunner.setMonsterLevelForChildren( 5.9 )
p3BurrowThreeRunner.feudWithMonsterType( "alien_bug" )
p3BurrowThreeRunner.feudWithMonsterType( "alien_bug_LT" )
p3BurrowThreeRunner.feudWithMonsterType( "alien_walker" )
p3BurrowThreeRunner.feudWithMonsterType( "alien_walker_LT" )
p3BurrowThreeRunner.addPatrolPointForChildren( "Aqueduct_403", 950, 460, 0 )
p3BurrowThreeRunner.addPatrolPointForChildren( "Aqueduct_404", 485, 540, 10 )

//Basically...if there's more players than pups, then it's time to send for help!
def checkHelpForBurrowThree() {
	if( playersIn403 > myRooms.Aqueduct_403.getSpawnTypeCount( "p3" ) && burrowThreeRunnerRespawnOkay == true ) {
		if( p3BurrowThreeRunner.spawnsInUse() == 0 ) {	
			burrowThreeRunner = p3BurrowThreeRunner.forceSpawnNow()
			burrowThreeRunner.say( "Zrrk! Kee-tr^k plik!" ) //Zrrk = Shit!. Kee-tr^k plik! = I'll get help! (Plik! = Help!)
			burrowThreeRunnerRespawnOkay = false
			burrowThreeRunnerDeathWatch()
		}
	} else {
		myManager.schedule(2) { checkBurrowThreeAlarm() }
	}
}

//Watch the Runner. If he dies, then reset him so he can respawn again later. NOTE: If he is disposed of at the LTThree burrow, then his reset occurs at that point instead.
def burrowThreeRunnerDeathWatch() {
	runOnDeath( burrowThreeRunner, { burrowThreeRunner = null; resetBurrowThreeRunner() } )
}

//Reset the Runner if the Runner is killed or if the ReadySquad is all killed or returns to their burrow
def resetBurrowThreeRunner() {
	if( myRooms.Aqueduct_403.getSpawnTypeCount( "p3" ) > 1 ) {
		burrowThreeRunnerRespawnOkay = true
		checkBurrowThreeAlarm()
	} else {
		myManager.schedule(5) { resetBurrowThreeRunner() } //keeps checking until at least two other P3s are in the room also
	}
}

p3ReadySquadThreeA = myRooms.Aqueduct_404.spawnSpawner( "p3ReadySquadThreeA", "p3", 1 )
p3ReadySquadThreeA.setPos( 475, 610 )
p3ReadySquadThreeA.setHomeTetherForChildren( 3000 )
p3ReadySquadThreeA.setHateRadiusForChildren( 3000 )
p3ReadySquadThreeA.setMonsterLevelForChildren( 5.5 )
p3ReadySquadThreeA.feudWithMonsterType( "alien_bug" )
p3ReadySquadThreeA.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadThreeA.feudWithMonsterType( "alien_walker" )
p3ReadySquadThreeA.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadThreeA.addPatrolPointForChildren( "Aqueduct_404", 175, 535, 0 )
p3ReadySquadThreeA.addPatrolPointForChildren( "Aqueduct_403", 730, 200, 0 )
p3ReadySquadThreeA.addPatrolPointForChildren( "Aqueduct_403", 560, 205, 0 )
p3ReadySquadThreeA.addPatrolPointForChildren( "Aqueduct_403", 700, 460, 0 )
p3ReadySquadThreeA.addPatrolPointForChildren( "Aqueduct_404", 500, 575, 10 )

p3ReadySquadThreeB = myRooms.Aqueduct_404.spawnSpawner( "p3ReadySquadThreeB", "p3", 1 )
p3ReadySquadThreeB.setPos( 480, 505 )
p3ReadySquadThreeB.setHomeTetherForChildren( 3000 )
p3ReadySquadThreeB.setHateRadiusForChildren( 3000 )
p3ReadySquadThreeB.setMonsterLevelForChildren( 5.5 )
p3ReadySquadThreeB.feudWithMonsterType( "alien_bug" )
p3ReadySquadThreeB.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadThreeB.feudWithMonsterType( "alien_walker" )
p3ReadySquadThreeB.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadThreeB.addPatrolPointForChildren( "Aqueduct_404", 175, 535, 0 )
p3ReadySquadThreeB.addPatrolPointForChildren( "Aqueduct_403", 730, 200, 0 )
p3ReadySquadThreeB.addPatrolPointForChildren( "Aqueduct_403", 560, 205, 0 )
p3ReadySquadThreeB.addPatrolPointForChildren( "Aqueduct_403", 700, 460, 0 )
p3ReadySquadThreeB.addPatrolPointForChildren( "Aqueduct_404", 500, 575, 10 )

p3ReadySquadThreeC = myRooms.Aqueduct_404.spawnSpawner( "p3ReadySquadThreeC", "p3", 1 )
p3ReadySquadThreeC.setPos( 550, 410 )
p3ReadySquadThreeC.setHomeTetherForChildren( 3000 )
p3ReadySquadThreeC.setHateRadiusForChildren( 3000 )
p3ReadySquadThreeC.setMonsterLevelForChildren( 5.5 )
p3ReadySquadThreeC.feudWithMonsterType( "alien_bug" )
p3ReadySquadThreeC.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadThreeC.feudWithMonsterType( "alien_walker" )
p3ReadySquadThreeC.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadThreeC.addPatrolPointForChildren( "Aqueduct_404", 175, 535, 0 )
p3ReadySquadThreeC.addPatrolPointForChildren( "Aqueduct_403", 730, 200, 0 )
p3ReadySquadThreeC.addPatrolPointForChildren( "Aqueduct_403", 560, 205, 0 )
p3ReadySquadThreeC.addPatrolPointForChildren( "Aqueduct_403", 700, 460, 0 )
p3ReadySquadThreeC.addPatrolPointForChildren( "Aqueduct_404", 500, 575, 10 )

p3ReadySquadThreeLeader = myRooms.Aqueduct_404.spawnSpawner( "p3ReadySquadThreeLeader", "p3", 1 )
p3ReadySquadThreeLeader.setPos( 390, 530 )
p3ReadySquadThreeLeader.setHomeTetherForChildren( 3000 )
p3ReadySquadThreeLeader.setHateRadiusForChildren( 3000 )
p3ReadySquadThreeLeader.setMonsterLevelForChildren( 5.5 )
p3ReadySquadThreeLeader.feudWithMonsterType( "alien_bug" )
p3ReadySquadThreeLeader.feudWithMonsterType( "alien_bug_LT" )
p3ReadySquadThreeLeader.feudWithMonsterType( "alien_walker" )
p3ReadySquadThreeLeader.feudWithMonsterType( "alien_walker_LT" )
p3ReadySquadThreeLeader.addPatrolPointForChildren( "Aqueduct_404", 175, 535, 0 )
p3ReadySquadThreeLeader.addPatrolPointForChildren( "Aqueduct_403", 730, 200, 0 )
p3ReadySquadThreeLeader.addPatrolPointForChildren( "Aqueduct_403", 560, 205, 0 )
p3ReadySquadThreeLeader.addPatrolPointForChildren( "Aqueduct_403", 700, 460, 0 )
p3ReadySquadThreeLeader.addPatrolPointForChildren( "Aqueduct_404", 500, 575, 10 )

//This is the "DISPOSE" trigger zone at the LTThree burrow
def LTThreeRunnerTrigger = "LTThreeRunnerTrigger"
myRooms.Aqueduct_404.createTriggerZone(LTThreeRunnerTrigger, 420, 490, 525, 630)

//This removes the Runner once he gets to the LTThree burrow (as if he went "inside" the burrow), AND it spawns the Ready Squad three seconds thereafter (as if they came out in response to the Runner).
myManager.onTriggerIn(myRooms.Aqueduct_404, LTThreeRunnerTrigger) { event ->
	if( event.actor == burrowThreeRunner ) {
		LTThreeReadySquadPatrolFlag = false
		burrowThreeRunner.dispose()
		myManager.schedule(3) { LTThreeReadySquadA = p3ReadySquadThreeA.forceSpawnNow() }
		myManager.schedule(4) { LTThreeReadySquadB = p3ReadySquadThreeB.forceSpawnNow() }
		myManager.schedule(5) { LTThreeReadySquadC = p3ReadySquadThreeC.forceSpawnNow() }
		myManager.schedule(6) { LTThreeReadySquadLeader = p3ReadySquadThreeLeader.forceSpawnNow(); LTThreeReadySquadLeader.say( "Q! Kzrelt...az^kree!!" ); startLTThreeReadySquadPatrol() }
	}
	if( event.actor == LTThreeReadySquadA && LTThreeReadySquadPatrolFlag == true ) {
		LTThreeReadySquadA.dispose() //These dispose statements occur if the patrol goes to BurrowThree, finds nothing and returns on their patrol path. They "go inside" if they found nothing out there to fight.
		LTThreeReadySquadA = null
	}
	if( event.actor == LTThreeReadySquadB && LTThreeReadySquadPatrolFlag == true ) {
		LTThreeReadySquadB.dispose()
		LTThreeReadySquadB = null
	}
	if( event.actor == LTThreeReadySquadC && LTThreeReadySquadPatrolFlag == true ) {
		LTThreeReadySquadC.dispose()
		LTThreeReadySquadC = null
	}
	if( event.actor == LTThreeReadySquadLeader && LTThreeReadySquadPatrolFlag == true ) {
		LTThreeReadySquadLeader.dispose()
		LTThreeReadySquadLeader = null
	}
}

//Once the Ready Squad is spawned, put them on patrol and start them on their way.
def startLTThreeReadySquadPatrol() {
	checkLTThreeReadySquadStatus()
	myManager.schedule(15) { LTThreeReadySquadPatrolFlag = true } //to prevent them from disappearing right away while spawning in the trigger zone
}	

//Once the Ready Squad is spawned, we need to check to see if they are killed OR if they are disposed of neatly after returning from their patrol.
def checkLTThreeReadySquadStatus() {
	if( ( LTThreeReadySquadA == null || LTThreeReadySquadA.isDead() ) && ( LTThreeReadySquadB == null || LTThreeReadySquadB.isDead() )  &&  ( LTThreeReadySquadC == null || LTThreeReadySquadC.isDead() ) &&  ( LTThreeReadySquadLeader == null || LTThreeReadySquadLeader.isDead() ) ) {
		resetBurrowThreeRunner()
	} else {
		myManager.schedule(5) { checkLTThreeReadySquadStatus() }
	}
}

//==========================
//ALLIANCES                 
//==========================
p3ReadySquadThreeA.allyWithSpawner( p3ReadySquadThreeB )
p3ReadySquadThreeA.allyWithSpawner( p3ReadySquadThreeC )
p3ReadySquadThreeA.allyWithSpawner( p3ReadySquadThreeLeader )
p3ReadySquadThreeB.allyWithSpawner( p3ReadySquadThreeC )
p3ReadySquadThreeB.allyWithSpawner( p3ReadySquadThreeLeader )
p3ReadySquadThreeC.allyWithSpawner( p3ReadySquadThreeLeader )

//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

p3BurrowThree.spawnAllNow()
p3BurrowThree.stopSpawning()

p3BurrowThreeRunner.stopSpawning()
p3ReadySquadThreeA.stopSpawning()
p3ReadySquadThreeB.stopSpawning()
p3ReadySquadThreeC.stopSpawning()
p3ReadySquadThreeLeader.stopSpawning()

respawnBurrowThree()

checkBurrowThreeAlarm()



//------------------------------------------
// THE LT BURROWS AND HQ (The Inner Hub)    
//------------------------------------------

p3BurrowLTOne = myRooms.Aqueduct_305.spawnSpawner( "p3BurrowLTOne", "p3_LT", 3 )
p3BurrowLTOne.setPos( 315, 475 )
p3BurrowLTOne.setWaitTime( 5, 10 )
p3BurrowLTOne.setWanderBehaviorForChildren( 50, 150, 4, 9, 200)
p3BurrowLTOne.feudWithMonsterType( "alien_bug" )
p3BurrowLTOne.feudWithMonsterType( "alien_bug_LT" )
p3BurrowLTOne.feudWithMonsterType( "alien_walker" )
p3BurrowLTOne.feudWithMonsterType( "alien_walker_LT" )
p3BurrowLTOne.setMonsterLevelForChildren( 5.5 )

def respawn305() {
	if( myRooms.Aqueduct_305.getSpawnTypeCount( "p3_LT" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			p3BurrowLTOne.spawnAllNow();
			myManager.schedule( 15 ) { respawn305() }
		}
	} else {
		myManager.schedule( 10 ) { respawn305() }
	}
}

p3BurrowLTTwo = myRooms.Aqueduct_405.spawnSpawner( "p3BurrowLTTwo", "p3_LT", 3 )
p3BurrowLTTwo.setPos( 700, 160 )
p3BurrowLTTwo.setWaitTime( 5, 10 )
p3BurrowLTTwo.setWanderBehaviorForChildren( 50, 150, 4, 9, 200)
p3BurrowLTTwo.feudWithMonsterType( "alien_bug" )
p3BurrowLTTwo.feudWithMonsterType( "alien_bug_LT" )
p3BurrowLTTwo.feudWithMonsterType( "alien_walker" )
p3BurrowLTTwo.feudWithMonsterType( "alien_walker_LT" )
p3BurrowLTTwo.setMonsterLevelForChildren( 5.6 )

p3BurrowLTThree = myRooms.Aqueduct_405.spawnSpawner( "p3BurrowLTThree", "p3_LT", 3 )
p3BurrowLTThree.setPos( 305, 615 )
p3BurrowLTThree.setWaitTime( 5, 10 )
p3BurrowLTThree.setWanderBehaviorForChildren( 50, 150, 4, 9, 200)
p3BurrowLTThree.feudWithMonsterType( "alien_bug" )
p3BurrowLTThree.feudWithMonsterType( "alien_bug_LT" )
p3BurrowLTThree.feudWithMonsterType( "alien_walker" )
p3BurrowLTThree.feudWithMonsterType( "alien_walker_LT" )
p3BurrowLTThree.setMonsterLevelForChildren( 5.6 )

def respawn405() {
	if( myRooms.Aqueduct_405.getSpawnTypeCount( "p3_LT" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			p3BurrowLTTwo.spawnAllNow();
			p3BurrowLTThree.spawnAllNow();
			myManager.schedule( 15 ) { respawn405() }
		}
	} else {
		myManager.schedule( 10 ) { respawn405() }
	}
}

p3BurrowThreeAux = myRooms.Aqueduct_505.spawnSpawner( "p3BurrowThreeAux", "p3", 2 )
p3BurrowThreeAux.setPos( 215, 100 )
p3BurrowThreeAux.setWaitTime( 5, 10 )
p3BurrowThreeAux.setWanderBehaviorForChildren( 50, 150, 4, 9, 200)
p3BurrowThreeAux.feudWithMonsterType( "alien_bug" )
p3BurrowThreeAux.feudWithMonsterType( "alien_bug_LT" )
p3BurrowThreeAux.feudWithMonsterType( "alien_walker" )
p3BurrowThreeAux.feudWithMonsterType( "alien_walker_LT" )
p3BurrowThreeAux.setMonsterLevelForChildren( 5.7 )

def respawn505() {
	if( myRooms.Aqueduct_505.getSpawnTypeCount( "p3" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			p3BurrowThreeAux.spawnAllNow();
			myManager.schedule( 15 ) { respawn505() }
		}
	} else {
		myManager.schedule( 10 ) { respawn505() }
	}
}

p3BurrowFour = myRooms.Aqueduct_404.spawnSpawner( "p3BurrowFour", "p3", 3 )
p3BurrowFour.setPos( 400, 120 )
p3BurrowFour.setWaitTime( 5, 10 )
p3BurrowFour.setWanderBehaviorForChildren( 50, 150, 4, 9, 200)
p3BurrowFour.feudWithMonsterType( "alien_bug" )
p3BurrowFour.feudWithMonsterType( "alien_bug_LT" )
p3BurrowFour.feudWithMonsterType( "alien_walker" )
p3BurrowFour.feudWithMonsterType( "alien_walker_LT" )
p3BurrowFour.setMonsterLevelForChildren( 5.7 )

def respawn404_grunts() {
	if( myRooms.Aqueduct_404.getSpawnTypeCount( "p3" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			p3BurrowFour.spawnAllNow();
			myManager.schedule( 15 ) { respawn404_grunts() }
		}
	} else {
		myManager.schedule( 10 ) { respawn404_grunts() }
	}
}

p3BurrowFourAux = myRooms.Aqueduct_304.spawnSpawner( "p3BurrowFourAux", "p3", 2 )
p3BurrowFourAux.setPos( 330, 610 )
p3BurrowFourAux.setWaitTime( 5, 10 )
p3BurrowFourAux.setWanderBehaviorForChildren( 50, 150, 4, 9, 200)
p3BurrowFourAux.feudWithMonsterType( "alien_bug" )
p3BurrowFourAux.feudWithMonsterType( "alien_bug_LT" )
p3BurrowFourAux.feudWithMonsterType( "alien_walker" )
p3BurrowFourAux.feudWithMonsterType( "alien_walker_LT" )
p3BurrowFourAux.setMonsterLevelForChildren( 5.6 )

def respawn304() {
	if( myRooms.Aqueduct_304.getSpawnTypeCount( "p3" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			p3BurrowFourAux.spawnAllNow();
			myManager.schedule( 15 ) { respawn304() }
		}
	} else {
		myManager.schedule( 10 ) { respawn304() }
	}
}

//==========================
//ALLIANCES                 
//==========================
p3BurrowLTOne.allyWithSpawner( p3BurrowLTTwo )
p3BurrowLTOne.allyWithSpawner( p3BurrowLTThree )
p3BurrowLTOne.allyWithSpawner( p3BurrowThreeAux )
p3BurrowLTOne.allyWithSpawner( p3BurrowFour )
p3BurrowLTOne.allyWithSpawner( p3BurrowFourAux )
p3BurrowLTTwo.allyWithSpawner( p3BurrowLTThree )
p3BurrowLTTwo.allyWithSpawner( p3BurrowThreeAux )
p3BurrowLTTwo.allyWithSpawner( p3BurrowFour )
p3BurrowLTTwo.allyWithSpawner( p3BurrowFourAux )
p3BurrowLTThree.allyWithSpawner( p3BurrowThreeAux )
p3BurrowLTThree.allyWithSpawner( p3BurrowFour )
p3BurrowLTThree.allyWithSpawner( p3BurrowFourAux )
p3BurrowThreeAux.allyWithSpawner( p3BurrowFour )
p3BurrowThreeAux.allyWithSpawner( p3BurrowFourAux )
p3BurrowFour.allyWithSpawner( p3BurrowFourAux )

//------------------------------------------
// THE HQ BURROW                            
//------------------------------------------

p3HQBurrow = myRooms.Aqueduct_404.spawnSpawner( "p3HQBurrow", "p3_LT", 2 )
p3HQBurrow.setPos( 660, 415 )
p3HQBurrow.setWaitTime( 5, 10 )
p3HQBurrow.setWanderBehaviorForChildren( 50, 150, 4, 9, 200)
p3HQBurrow.feudWithMonsterType( "alien_bug" )
p3HQBurrow.feudWithMonsterType( "alien_bug_LT" )
p3HQBurrow.feudWithMonsterType( "alien_walker" )
p3HQBurrow.feudWithMonsterType( "alien_walker_LT" )
p3HQBurrow.setMonsterLevelForChildren( 5.9 )

def respawn404_LTs() {
	if( myRooms.Aqueduct_404.getSpawnTypeCount( "p3_LT" ) == 0 ) {
		p1 = p3HQBurrow.forceSpawnNow()
		p1.setDisplayName( "Honor Guard" )
		p2 = p3HQBurrow.forceSpawnNow()
		p2.setDisplayName( "Honor Guard" )
		myManager.schedule( random( 45, 90 ) ) { respawn404_LTs() }
	} else {
		myManager.schedule( 10 ) { respawn404_LTs() }
	}
}

commanderSpawner = myRooms.Aqueduct_404.spawnSpawner( "commanderSpawner", "p3_demi", 1 )
commanderSpawner.setPos( 660, 415 )
commanderSpawner.setWaitTime( 600, 1200 ) // 10-20 minutes before commander respawns again
commanderSpawner.setHomeTetherForChildren( 6000 )
commanderSpawner.setWanderBehaviorForChildren( 150, 300, 4, 9, 200)
commanderSpawner.setCFH( 500 )
commanderSpawner.feudWithMonsterType( "alien_bug" )
commanderSpawner.feudWithMonsterType( "alien_bug_LT" )
commanderSpawner.feudWithMonsterType( "alien_walker" )
commanderSpawner.feudWithMonsterType( "alien_walker_LT" )
commanderSpawner.setMonsterLevelForChildren( 6.0 )

//==========================
//ALLIANCES                 
//==========================
p3HQBurrow.allyWithSpawner( commanderSpawner )
p3HQBurrow.allyWithSpawner( p3BurrowLTOne )
p3HQBurrow.allyWithSpawner( p3BurrowLTTwo )
p3HQBurrow.allyWithSpawner( p3BurrowLTThree )
p3HQBurrow.allyWithSpawner( p3BurrowThreeAux )
p3HQBurrow.allyWithSpawner( p3BurrowFour )
p3HQBurrow.allyWithSpawner( p3BurrowFourAux )

//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

p3BurrowLTOne.spawnAllNow()
p3BurrowLTTwo.spawnAllNow()
p3BurrowLTThree.spawnAllNow()
p3BurrowThreeAux.spawnAllNow()
p3BurrowFour.spawnAllNow()
p3BurrowFourAux.spawnAllNow()

p1 = p3HQBurrow.forceSpawnNow()
p1.setDisplayName( "Honor Guard" )
p2 = p3HQBurrow.forceSpawnNow()
p2.setDisplayName( "Honor Guard" )

p3BurrowLTOne.stopSpawning()
p3BurrowLTTwo.stopSpawning()
p3BurrowLTThree.stopSpawning()
p3BurrowThreeAux.stopSpawning()
p3BurrowFour.stopSpawning()
p3BurrowFourAux.stopSpawning()
p3HQBurrow.stopSpawning()

respawn305()
respawn405()
respawn505()
respawn404_grunts()
respawn304()
respawn404_LTs()

commanderSpawner.forceSpawnNow()



riverML = 5.0

//------------------------------------------
// RIVER PATROL                             
//------------------------------------------

riverPatrolSpawner = myRooms.Aqueduct_307.spawnSpawner( "riverPatrolSpawner", "p3_LT", 1 )
riverPatrolSpawner.setPos( 745, 100 )
riverPatrolSpawner.setWaitTime( 60, 90 )
riverPatrolSpawner.setHomeTetherForChildren( 3000 )
riverPatrolSpawner.setHateRadiusForChildren( 3000 )
riverPatrolSpawner.setBaseSpeed( 180 )
riverPatrolSpawner.setCFH( 500 )
riverPatrolSpawner.setChildrenToFollow( riverPatrolSpawner )
riverPatrolSpawner.feudWithMonsterType( "alien_bug" )
riverPatrolSpawner.feudWithMonsterType( "alien_bug_LT" )
riverPatrolSpawner.feudWithMonsterType( "alien_walker" )
riverPatrolSpawner.feudWithMonsterType( "alien_walker_LT" )
riverPatrolSpawner.setMonsterLevelForChildren( 5.5 )
riverPatrolSpawner.addPatrolPointForSpawner( "Aqueduct_307", 770, 205, 5 )
riverPatrolSpawner.addPatrolPointForSpawner( "Aqueduct_407", 620, 190, 0 )
riverPatrolSpawner.addPatrolPointForSpawner( "Aqueduct_507", 300, 175, 5 )
riverPatrolSpawner.addPatrolPointForSpawner( "Aqueduct_506", 500, 175, 0 )
riverPatrolSpawner.addPatrolPointForSpawner( "Aqueduct_505", 530, 450, 0 )
riverPatrolSpawner.addPatrolPointForSpawner( "Aqueduct_504", 350, 360, 5 )
riverPatrolSpawner.addPatrolPointForSpawner( "Aqueduct_505", 530, 450, 0 )
riverPatrolSpawner.addPatrolPointForSpawner( "Aqueduct_506", 500, 175, 0 )
riverPatrolSpawner.addPatrolPointForSpawner( "Aqueduct_507", 300, 175, 5 )
riverPatrolSpawner.addPatrolPointForSpawner( "Aqueduct_407", 620, 190, 0 )
riverPatrolSpawner.addPatrolPointForSpawner( "Aqueduct_307", 770, 205, 5 )
riverPatrolSpawner.startPatrol()

riverPatrolFlunkies = myRooms.Aqueduct_307.spawnSpawner( "longPatrol", "p3", 3 )
riverPatrolFlunkies.setPos( 745, 100 )
riverPatrolFlunkies.setWaitTime( 20 , 40 )
riverPatrolFlunkies.setHomeTetherForChildren( 3000 )
riverPatrolFlunkies.setHateRadiusForChildren( 3000 )
riverPatrolFlunkies.setCFH( 500 )
riverPatrolFlunkies.startFollow( riverPatrolSpawner )
riverPatrolFlunkies.setChildrenToFollow( riverPatrolSpawner )
riverPatrolFlunkies.feudWithMonsterType( "alien_bug" )
riverPatrolFlunkies.feudWithMonsterType( "alien_bug_LT" )
riverPatrolFlunkies.feudWithMonsterType( "alien_walker" )
riverPatrolFlunkies.feudWithMonsterType( "alien_walker_LT" )
riverPatrolFlunkies.setMonsterLevelForChildren( 5.5 )

//------------------------------------------
// FOREST AMBUSHES                          
//------------------------------------------


ambush1 = myRooms.Aqueduct_407.spawnSpawner( "ambush1", "p3", 4 )
ambush1.setPos( 270, 245 )
ambush1.setWaitTime( 50, 70 )
ambush1.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush1.feudWithMonsterType( "alien_bug" )
ambush1.feudWithMonsterType( "alien_bug_LT" )
ambush1.feudWithMonsterType( "alien_walker" )
ambush1.feudWithMonsterType( "alien_walker_LT" )
ambush1.setMonsterLevelForChildren( riverML )

ambush2 = myRooms.Aqueduct_407.spawnSpawner( "ambush2", "p3", 4 )
ambush2.setPos( 710, 230 )
ambush2.setWaitTime( 50, 70 )
ambush2.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush2.feudWithMonsterType( "alien_bug" )
ambush2.feudWithMonsterType( "alien_bug_LT" )
ambush2.feudWithMonsterType( "alien_walker" )
ambush2.feudWithMonsterType( "alien_walker_LT" )
ambush2.setMonsterLevelForChildren( riverML )

ambush3 = myRooms.Aqueduct_407.spawnSpawner( "ambush3", "p3", 4 )
ambush3.setPos( 850, 480 )
ambush3.setWaitTime( 50, 70 )
ambush3.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush3.feudWithMonsterType( "alien_bug" )
ambush3.feudWithMonsterType( "alien_bug_LT" )
ambush3.feudWithMonsterType( "alien_walker" )
ambush3.feudWithMonsterType( "alien_walker_LT" )
ambush3.setMonsterLevelForChildren( riverML )

ambush4 = myRooms.Aqueduct_307.spawnSpawner( "ambush4", "p3", 4 )
ambush4.setPos( 450, 215 )
ambush4.setWaitTime( 50, 70 )
ambush4.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush4.feudWithMonsterType( "alien_bug" )
ambush4.feudWithMonsterType( "alien_bug_LT" )
ambush4.feudWithMonsterType( "alien_walker" )
ambush4.feudWithMonsterType( "alien_walker_LT" )
ambush4.setMonsterLevelForChildren( riverML )

ambush5 = myRooms.Aqueduct_307.spawnSpawner( "ambush5", "p3", 4 )
ambush5.setPos( 925, 430 )
ambush5.setWaitTime( 50, 70 )
ambush5.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush5.feudWithMonsterType( "alien_bug" )
ambush5.feudWithMonsterType( "alien_bug_LT" )
ambush5.feudWithMonsterType( "alien_walker" )
ambush5.feudWithMonsterType( "alien_walker_LT" )
ambush5.setMonsterLevelForChildren( riverML )

ambush6 = myRooms.Aqueduct_307.spawnSpawner( "ambush6", "p3", 4 )
ambush6.setPos( 265, 430 )
ambush6.setWaitTime( 50, 70 )
ambush6.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush6.feudWithMonsterType( "alien_bug" )
ambush6.feudWithMonsterType( "alien_bug_LT" )
ambush6.feudWithMonsterType( "alien_walker" )
ambush6.feudWithMonsterType( "alien_walker_LT" )
ambush6.setMonsterLevelForChildren( riverML )

ambush7 = myRooms.Aqueduct_504.spawnSpawner( "ambush7", "p3", 4 )
ambush7.setPos( 640, 540 )
ambush7.setWaitTime( 50, 70 )
ambush7.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush7.feudWithMonsterType( "alien_bug" )
ambush7.feudWithMonsterType( "alien_bug_LT" )
ambush7.feudWithMonsterType( "alien_walker" )
ambush7.feudWithMonsterType( "alien_walker_LT" )
ambush7.setMonsterLevelForChildren( riverML )

ambush8 = myRooms.Aqueduct_504.spawnSpawner( "ambush8", "p3", 4 )
ambush8.setPos( 230, 400 )
ambush8.setWaitTime( 50, 70 )
ambush8.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush8.feudWithMonsterType( "alien_bug" )
ambush8.feudWithMonsterType( "alien_bug_LT" )
ambush8.feudWithMonsterType( "alien_walker" )
ambush8.feudWithMonsterType( "alien_walker_LT" )
ambush8.setMonsterLevelForChildren( riverML )

ambush9 = myRooms.Aqueduct_505.spawnSpawner( "ambush9", "p3", 4 )
ambush9.setPos( 545, 565 )
ambush9.setWaitTime( 50, 70 )
ambush9.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush9.feudWithMonsterType( "alien_bug" )
ambush9.feudWithMonsterType( "alien_bug_LT" )
ambush9.feudWithMonsterType( "alien_walker" )
ambush9.feudWithMonsterType( "alien_walker_LT" )
ambush9.setMonsterLevelForChildren( riverML )

ambush10 = myRooms.Aqueduct_505.spawnSpawner( "ambush10", "p3", 4 )
ambush10.setPos( 865, 400 )
ambush10.setWaitTime( 50, 70 )
ambush10.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush10.feudWithMonsterType( "alien_bug" )
ambush10.feudWithMonsterType( "alien_bug_LT" )
ambush10.feudWithMonsterType( "alien_walker" )
ambush10.feudWithMonsterType( "alien_walker_LT" )
ambush10.setMonsterLevelForChildren( riverML )

ambush11 = myRooms.Aqueduct_506.spawnSpawner( "ambush11", "p3", 4 )
ambush11.setPos( 365, 390 )
ambush11.setWaitTime( 50, 70 )
ambush11.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush11.feudWithMonsterType( "alien_bug" )
ambush11.feudWithMonsterType( "alien_bug_LT" )
ambush11.feudWithMonsterType( "alien_walker" )
ambush11.feudWithMonsterType( "alien_walker_LT" )
ambush11.setMonsterLevelForChildren( riverML )

ambush12 = myRooms.Aqueduct_506.spawnSpawner( "ambush12", "p3", 4 )
ambush12.setPos( 675, 180 )
ambush12.setWaitTime( 50, 70 )
ambush12.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush12.feudWithMonsterType( "alien_bug" )
ambush12.feudWithMonsterType( "alien_bug_LT" )
ambush12.feudWithMonsterType( "alien_walker" )
ambush12.feudWithMonsterType( "alien_walker_LT" )
ambush12.setMonsterLevelForChildren( riverML )

ambush13 = myRooms.Aqueduct_507.spawnSpawner( "ambush13", "p3", 4 )
ambush13.setPos( 470, 100 )
ambush13.setWaitTime( 50, 70 )
ambush13.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush13.feudWithMonsterType( "alien_bug" )
ambush13.feudWithMonsterType( "alien_bug_LT" )
ambush13.feudWithMonsterType( "alien_walker" )
ambush13.feudWithMonsterType( "alien_walker_LT" )
ambush13.setMonsterLevelForChildren( riverML )

ambush14 = myRooms.Aqueduct_507.spawnSpawner( "ambush14", "p3", 4 )
ambush14.setPos( 975, 60 )
ambush14.setWaitTime( 50, 70 )
ambush14.setWanderBehaviorForChildren( 50, 150, 10, 15, 200)
ambush14.feudWithMonsterType( "alien_bug" )
ambush14.feudWithMonsterType( "alien_bug_LT" )
ambush14.feudWithMonsterType( "alien_walker" )
ambush14.feudWithMonsterType( "alien_walker_LT" )
ambush14.setMonsterLevelForChildren( riverML )

ambushList = [ ambush1, ambush2, ambush3, ambush4, ambush5, ambush6, ambush7, ambush8, ambush9, ambush10, ambush11, ambush12, ambush13, ambush14 ]

//==========================
//ALLIANCES                 
//==========================

riverPatrolSpawner.allyWithSpawner( riverPatrolFlunkies )

//==========================
//REPOPULATING LOGIC        
//==========================
// Rooms included: 504, 505, 506, 507, 407, 307
// - check if the area is below totalAreaMaxSpawn
// - spawn from a random spawner every 10 seconds
// - totalAreaCurrentSpawn = (add all SpawnsInUse together) Don't exceed totalAreaMaxSpawn total spawns in the river/woods area.
// - if the spawners is aleady at max spawns (spawnsInUse() == maxSpawn ) then spawn at another spawner

totalAreaMaxSpawn = 20
maxSpawn = 4

//calculate a random riverML between 5.1 and 6.0
def randomriverML() {
	roll = random( 10 )
	riverML = 5 + ( roll * 0.1 )
}

//continuously try to repopluate the forest and river area
def repopulate() {
	totalAreaCurrentSpawn = ambush1.spawnsInUse() + ambush2.spawnsInUse() + ambush3.spawnsInUse() + ambush4.spawnsInUse() + ambush5.spawnsInUse() + ambush6.spawnsInUse() + ambush7.spawnsInUse() + ambush8.spawnsInUse() + ambush9.spawnsInUse() + ambush10.spawnsInUse() + ambush11.spawnsInUse() + ambush12.spawnsInUse() + ambush13.spawnsInUse() + ambush14.spawnsInUse()
	if( totalAreaCurrentSpawn < totalAreaMaxSpawn ) {
		spawner = random( ambushList )
		if( spawner.spawnsInUse() < maxSpawn ) {
			randomriverML()
			spawner.setMonsterLevelForChildren( riverML )
			repop = spawner.forceSpawnNow()
			runOnDeath( repop, { event -> checkForVox( event ) } )
		}
	}
	myManager.schedule( random( 15 ) ) { repopulate() }
}

//upon the death of a pup in the area, see if the killer is on the quest and whether they find a Vox or not
denialMessage = [ "No Vox here!", "Nope. Nothing on this one.", "Gotta keep looking.", "Dangit. No Vox.", "Maybe the next one...", "Is that...? Nope. Nothing.", "Nada. Next!", "The next one's lucky. I feel it.", "Still nothing." ]

def synchronized checkForVox( event ) {
	if( isPlayer( event.killer ) && event.killer.isOnQuest( 28, 2 ) && !event.killer.hasQuestFlag( GLOBAL, "Z07FoundTheVox" ) ) {
		if( event.killer.getConLevel() < 6.2 ) {
			roll = random( 100 )
			if( roll <= event.killer.getPlayerVar( "Z07VoxPercentage" ) ) {
				event.killer.updateQuest( 28, "Tourist George-VQS" ) //push the completion of step 2 of VOX STELLA
				event.killer.setQuestFlag( GLOBAL, "Z07FoundTheVox" )
				event.killer.centerPrint( "You found a beat-up, damaged communicator!" )
			} else {
				event.killer.centerPrint( random( denialMessage ) )
				event.killer.addToPlayerVar( "Z07VoxPercentage", 1 ) //keep adding +1 to the percentage until the player finally finds the Vox
			}
		} else {
			event.killer.centerPrint( "You must change your level to 6.1 or lower to search for the Vox Stellarum." )
		}
	}
}

patrolDead = false

def synchronized checkPatrolDeath() {
	if( riverPatrolSpawner.spawnsInUse() + riverPatrolFlunkies.spawnsInUse() == 0 && patrolDead == false ) {
		patrolDead = true
		myManager.schedule( random( 120, 240 ) ) {
			riverLeader = riverPatrolSpawner.forceSpawnNow()
			riverLeader.say( "Q! Kzrelt...az^kree!!" )
			runOnDeath( riverLeader, { event -> checkForVox( event ); checkPatrolDeath() } )
			flunky1 = riverPatrolFlunkies.forceSpawnNow()
			runOnDeath( flunky1, { event -> checkForVox( event ); checkPatrolDeath() } )
			flunky2 = riverPatrolFlunkies.forceSpawnNow()
			runOnDeath( flunky2, { event -> checkForVox( event ); checkPatrolDeath() } )
			flunky3 = riverPatrolFlunkies.forceSpawnNow()
			runOnDeath( flunky3, { event -> checkForVox( event ); checkPatrolDeath() } )
			patrolDead = false
		} 
	}
}
	
		
//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

ambush1.stopSpawning()
ambush2.stopSpawning()
ambush3.stopSpawning()
ambush4.stopSpawning()
ambush5.stopSpawning()
ambush6.stopSpawning()
ambush7.stopSpawning()
ambush8.stopSpawning()
ambush9.stopSpawning()
ambush10.stopSpawning()
ambush11.stopSpawning()
ambush12.stopSpawning()
ambush13.stopSpawning()
ambush14.stopSpawning()

repopulate()

riverPatrolSpawner.stopSpawning()
riverPatrolFlunkies.stopSpawning()

checkPatrolDeath()


//==============================================
// LOGIC TO GET TO BUG WORLD FROM STAR PORTAL   
//==============================================
portal = makeSwitch( "starPortal", myRooms.Aqueduct_306, 420, 420 )

portal.lock()
portal.off()
portal.setRange( 200 )
portal.setUsable( false )

//---------------------------------------------------------
// TRIGGER ZONES FOR STAR PORTAL (Quest 100)               
//---------------------------------------------------------

def WarpGate = "WarpGate"
myRooms.Aqueduct_306.createTriggerZone( WarpGate, 385, 390, 460, 460 )

portalOn = false

myManager.onTriggerIn(myRooms.Aqueduct_306, WarpGate) { event ->
	if( isPlayer( event.actor ) && !event.actor.isDazed()) {
		event.actor.getCrew().each{
			if( it.isOnQuest( 100 ) || it.isDoneQuest( 100 ) ) {
				event.actor.setQuestFlag( GLOBAL, "Z10TempStarPortalEntryAllowed" )
			}
		}
		if( event.actor.hasQuestFlag( GLOBAL, "Z10TempStarPortalEntryAllowed" ) ) {
			if( portalOn == false ) {
				portalOn = true
				portal.on()
				sound( "portalActivate" ).toZone()
				portalTimer = myManager.schedule(4) { portal.off(); portalOn = false }
			}
			sound( "portalTravel" ).toZone()
			event.actor.warp( "Hive_1", 740, 510, 6 )
			event.actor.centerPrint( "A strange force pulls you across the cosmos..." )
			event.actor.unsetQuestFlag( GLOBAL, "Z10TempStarPortalEntryAllowed" )
		}
	}
}

