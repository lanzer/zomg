import com.gaiaonline.mmo.battle.script.*;

Roderick = spawnNPC("Tourist George-VQS", myRooms.Aqueduct_602, 745, 565)
Roderick.setRotation( 45 )
Roderick.setDisplayName( "Roderick" )

True1 = spawnNPC("TrueBeliever1-VQS", myRooms.Aqueduct_602, 945, 570)
True1.setRotation( 135 )
True1.setDisplayName( "Tom" )

True2 = spawnNPC("TrueBeliever2-VQS", myRooms.Aqueduct_602, 1135, 580)
True2.setRotation( 135 )
True2.setDisplayName( "Dick" )

True3 = spawnNPC("TrueBeliever3-VQS", myRooms.Aqueduct_602, 1320, 590)
True3.setRotation( 135 )
True3.setDisplayName( "Hairy" )

//True4 = spawnNPC("TrueBeliever1-VQS", myRooms.Aqueduct_602, 400, 400)
//True4.setRotation( 45 )

True5 = spawnNPC("TrueBeliever5-VQS", myRooms.Aqueduct_602, 830, 705)
True5.setRotation( 315 )
True5.setDisplayName( "Lola" )

True6 = spawnNPC("TrueBeliever6-VQS", myRooms.Aqueduct_602, 940, 840)
True6.setRotation( 315 )
True6.setDisplayName( "Amy" )

True7 = spawnNPC("TrueBeliever7-VQS", myRooms.Aqueduct_602, 1035, 985)
True7.setRotation( 315 )
True7.setDisplayName( "Spirit" )

True8 = spawnNPC("TrueBeliever8-VQS", myRooms.Aqueduct_602, 1175, 800)
True8.setRotation( 225 )
True8.setDisplayName( "Betty" )

itsBeenAWhile = true

myManager.onEnter( myRooms.Aqueduct_602 ) { event ->
	if( isPlayer( event.actor ) && event.actor.getConLevel() < 6.2 ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z07TooOldForAqueductQuests" )
	} else if(isPlayer(event.actor)){
		event.actor.setQuestFlag( GLOBAL, "Z07TooOldForAqueductQuests" )
	}
	if( isPlayer( event.actor ) && itsBeenAWhile ) {
		itsBeenAWhile = false
		myManager.schedule(5) {
			True1.say( "The Zurg are returning! Our time has come!" )
		}
		myManager.schedule(6) {
			True8.say( "Take us! We are the True Believers!" )
		}
		myManager.schedule(7) {
			True3.say( "I want to be a Galactic Hitchhiker!" )
		}
		myManager.schedule(8) {
			True6.say( "I've got my towel!" )
		}
		myManager.schedule(9) {
			True5.say( "The truth *is* out there!" )
		}
		myManager.schedule(10) {
			True7.say( "Beam us up!" )
		}
		myManager.schedule(11) {
			True2.say( "We want to be friends!" )
		}
		myManager.schedule(21) { itsBeenAWhile = true }
	}
	if(isPlayer(event.actor) && event.actor.isOnQuest(331, 2)) {
		event.actor.setQuestFlag(GLOBAL, "Z07_Roderick_FromRyanDialog")
	}
}
//TODO: Make random lists of things they can say and have it change a bit each time.

//---------------------------------------------------------
//Daily Chance                                             
//---------------------------------------------------------
def dailyChance = True7.createConversation("dailyChance", true, "!Z07_Roderick_True7")

def dailyChance1 = [id:1]
dailyChance1.npctext = "The Zurg will come!! They will!"
dailyChance1.playertext = "Do you have any freebies available today?"
dailyChance1.exec = { event ->
	checkDailyChance(event.player, 230, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 230)
			True7.say("Sure, I don't need this anyway since I won't be able to take it with me when the Zurg come.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Spirit today. Try again tomorrow!" )
		}
	})
}
dailyChance1.result = DONE
dailyChance.addDialog(dailyChance1, True7)

//---------------------------------------------------------
//True Rumors - Intro                                      
//---------------------------------------------------------	
def fromRyan = Roderick.createConversation("fromRyan", true, "QuestStarted_331:2", "Z07_Roderick_FromRyanDialog")

def fromRyan1 = [id:1]
fromRyan1.npctext = "Hello! I'm Roderick, leader of the True Believers. What brings you to our little clearing?"
fromRyan1.playertext = "Oh, I don't know, just looking around."
fromRyan1.result = 2
fromRyan.addDialog(fromRyan1, Roderick)

def fromRyan2 = [id:2]
fromRyan2.npctext = "Stay awhile, and await the return of the Zurg with us!"
fromRyan2.playertext = "Is that what you're doing up here?"
fromRyan2.result = 3
fromRyan.addDialog(fromRyan2, Roderick)

def fromRyan3 = [id:3]
fromRyan3.npctext = "Oh, yes, of course. What else would we be doing?"
fromRyan3.options = []
fromRyan3.options << [text:"Look, I'll be honest, I heard you were holding people here against their will...", result: 4]
fromRyan3.options << [text:"That's cool, so everyone here believes the Zurg are coming back to take them into outer-space?", result: 5]
fromRyan.addDialog(fromRyan3, Roderick)

def fromRyan4 = [id:4]
fromRyan4.npctext = "WHAT?! You have to be joking. We are the True Believers and I don't let anyone who doesn't believe stay here. Why would I force anyone to stay? You can ask them yourselves!"
fromRyan4.playertext = "Alright, since you don't mind."
fromRyan4.result = 6
fromRyan.addDialog(fromRyan4, Roderick)

def fromRyan5 = [id:5]
fromRyan5.npctext = "Oh yes, we all know the Zurg will return someday, and we will be waiting for them. Just speak with the others, everyone here truly believes."
fromRyan5.playertext = "Thanks, I'll do that."
fromRyan5.result = 6
fromRyan.addDialog(fromRyan5, Roderick)

def fromRyan6 = [id:6]
fromRyan6.npctext = "Wait, before you go I need to give you something."
fromRyan6.playertext = "Uhm, what?"
fromRyan6.flag = ["Z07_Roderick_True1", "Z07_Roderick_True2", "Z07_Roderick_True3", "Z07_Roderick_True5", "Z07_Roderick_True6", "Z07_Roderick_True7", "Z07_Roderick_True8", "!Z07_Roderick_FromRyanDialog"]
fromRyan6.exec = { event ->
	event.player.setPlayerVar("Z07_Roderick_BelieversSpokenTo", 0)
}
fromRyan6.quest = 331
fromRyan6.exec = { event ->
	event.player.removeMiniMapQuestActorName("Tourist George-VQS")
	Roderick.pushDialog(event.player, "defaultRoderick")
}
fromRyan6.result = DONE
fromRyan.addDialog(fromRyan6, Roderick)

//---------------------------------------------------------
//True Rumors - True1                                      
//---------------------------------------------------------
def true1Default = True1.createConversation("true1Default", true, "Z07_Roderick_True1")

def true1Default1 = [id:1]
true1Default1.npctext = "Hi! I'm Tom."
true1Default1.playertext = "Hi, Tom, I'm %p. So, you're here waiting for the Zurg?"
true1Default1.result = 2
true1Default.addDialog(true1Default1, True1)

def true1Default2 = [id:2]
true1Default2.npctext = "Yes! The Zurg will return and save us!"
true1Default2.playertext = "So you enjoy being here?"
true1Default2.result = 3
true1Default.addDialog(true1Default2, True1)

def true1Default3 = [id:3]
true1Default3.npctext = "I do! I can't think of anywhere I'd rather be."
true1Default3.playertext = "Cool, thanks."
true1Default3.exec = { event ->
	event.player.unsetQuestFlag(GLOBAL, "Z07_Roderick_True1")
	event.player.addToPlayerVar("Z07_Roderick_BelieversSpokenTo", 1)
	event.player.centerPrint("True Believers Questioned ${event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo").intValue()}/7")
	if(event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo") >= 7) {
		event.player.updateQuest(331, "Tourist George-VQS")
	}
}
true1Default3.result = DONE
true1Default.addDialog(true1Default3, True1)

//---------------------------------------------------------
//True Rumors - True2                                      
//---------------------------------------------------------
def true2Default = True2.createConversation("true2Default", true, "Z07_Roderick_True2")

def true2Default1 = [id:1]
true2Default1.npctext = "Hello, visitor."
true2Default1.playertext = "Hi, how's life in the True Believers?"
true2Default1.result = 2
true2Default.addDialog(true2Default1, True2)

def true2Default2 = [id:2]
true2Default2.npctext = "It's amazing! We will be the first saved when the Zurg return!"
true2Default2.playertext = "Sounds like you're looking forward to it."
true2Default2.result = 3
true2Default.addDialog(true2Default2, True2)

def true2Default3 = [id:3]
true2Default3.npctext = "I am! It will be the best thing that's happened, ever."
true2Default3.playertext = "Sounds like it."
true2Default3.exec = { event ->
	event.player.unsetQuestFlag(GLOBAL, "Z07_Roderick_True2")
	event.player.addToPlayerVar("Z07_Roderick_BelieversSpokenTo", 1)
	event.player.centerPrint("True Believers Questioned ${event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo").intValue()}/7")
	if(event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo") >= 7) {
		event.player.updateQuest(331, "Tourist George-VQS")
	}
}
true2Default3.result = DONE
true2Default.addDialog(true2Default3, True2)

//---------------------------------------------------------
//True Rumors - True3                                      
//---------------------------------------------------------
def true3Default = True3.createConversation("true3Default", true, "Z07_Roderick_True3")

def true3Default1 = [id:1]
true3Default1.npctext = "Hi! I'm Hairy."
true3Default1.playertext = "You sure are!"
true3Default1.result = 2
true3Default.addDialog(true3Default1, True3)

def true3Default2 = [id:2]
true3Default2.npctext = "What's that supposed to mean?"
true3Default2.playertext = "Nothing, just kidding around. So, how do you like being a True Believer?"
true3Default2.result = 3
true3Default.addDialog(true3Default2, True3)

def true3Default3 = [id:3]
true3Default3.npctext = "I love it! It's the best! I've never been a part of something so meaningful."
true3Default3.playertext = "Gotcha."
true3Default3.exec = { event ->
	event.player.unsetQuestFlag(GLOBAL, "Z07_Roderick_True3")
	event.player.addToPlayerVar("Z07_Roderick_BelieversSpokenTo", 1)
	event.player.centerPrint("True Believers Questioned ${event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo").intValue()}/7")
	if(event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo") >= 7) {
		event.player.updateQuest(331, "Tourist George-VQS")
	}
}
true3Default3.result = DONE
true3Default.addDialog(true3Default3, True3)

//---------------------------------------------------------
//True Rumors - True5                                      
//---------------------------------------------------------
def true5Default = True5.createConversation("true5Default", true, "Z07_Roderick_True5")

def true5Default1 = [id:1]
true5Default1.npctext = "The truth is out there, it is coming, and it is brilliant."
true5Default1.playertext = "What truth is that?"
true5Default1.result = 2
true5Default.addDialog(true5Default1, True5)

def true5Default2 = [id:2]
true5Default2.npctext = "That the Zurg are coming!"
true5Default2.playertext = "How can you be sure?"
true5Default2.result = 3
true5Default.addDialog(true5Default2, True5)

def true5Default3 = [id:3]
true5Default3.npctext = "I... I just am! I know they are coming!"
true5Default3.playertext = "Uhm, I'm not sure I understand... but ok."
true5Default3.exec = { event ->
	event.player.unsetQuestFlag(GLOBAL, "Z07_Roderick_True5")
	event.player.addToPlayerVar("Z07_Roderick_BelieversSpokenTo", 1)
	event.player.centerPrint("True Believers Questioned ${event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo").intValue()}/7")
	if(event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo") >= 7) {
		event.player.updateQuest(331, "Tourist George-VQS")
	}
}
true5Default3.result = DONE
true5Default.addDialog(true5Default3, True5)

//---------------------------------------------------------
//True Rumors - True6                                      
//---------------------------------------------------------
def true6Default = True6.createConversation("true6Default", true, "Z07_Roderick_True6")

def true6Default1 = [id:1]
true6Default1.npctext = "The non-Believers won't think we're crazy when the Zurg come!"
true6Default1.playertext = "What if the Zurg don't come?"
true6Default1.result = 2
true6Default.addDialog(true6Default1, True6)

def true6Default2 = [id:2]
true6Default2.npctext = "Don't come? I think you must be the crazy one."
true6Default2.playertext = "No doubt in your mind, huh?"
true6Default2.result = 3
true6Default.addDialog(true6Default2, True6)

def true6Default3 = [id:3]
true6Default3.npctext = "None."
true6Default3.playertext = "Good to know."
true6Default3.exec = { event ->
	event.player.unsetQuestFlag(GLOBAL, "Z07_Roderick_True6")
	event.player.addToPlayerVar("Z07_Roderick_BelieversSpokenTo", 1)
	event.player.centerPrint("True Believers Questioned ${event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo").intValue()}/7")
	if(event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo") >= 7) {
		event.player.updateQuest(331, "Tourist George-VQS")
	}
}
true6Default3.result = DONE
true6Default.addDialog(true6Default3, True6)

//---------------------------------------------------------
//True Rumors - True7                                      
//---------------------------------------------------------
def true7Default = True7.createConversation("true7Default", true, "Z07_Roderick_True7")

def true7Default1 = [id:1]
true7Default1.npctext = "The Zurg will come!! They will!"
true7Default1.playertext = "What makes you think that?"
true7Default1.result = 2
true7Default.addDialog(true7Default1, True7)

def true7Default2 = [id:2]
true7Default2.npctext = "The truth is out there!"
true7Default2.playertext = "So you're actually happy to be here waiting for the Zurg?"
true7Default2.result = 3
true7Default.addDialog(true7Default2, True7)

def true7Default3 = [id:3]
true7Default3.npctext = "I couldn't be happier. I can't wait till the Zurg arrive."
true7Default3.playertext = "That will be an interesting day."
true7Default3.exec = { event ->
	event.player.unsetQuestFlag(GLOBAL, "Z07_Roderick_True7")
	event.player.addToPlayerVar("Z07_Roderick_BelieversSpokenTo", 1)
	event.player.centerPrint("True Believers Questioned ${event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo").intValue()}/7")
	if(event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo") >= 7) {
		event.player.updateQuest(331, "Tourist George-VQS")
	}
}
true7Default3.result = DONE
true7Default.addDialog(true7Default3, True7)

//---------------------------------------------------------
//True Rumors - True8                                      
//---------------------------------------------------------
def true8Default = True8.createConversation("true8Default", true, "Z07_Roderick_True8")

def true8Default1 = [id:1]
true8Default1.npctext = "A visitor! Are you going to join us?"
true8Default1.playertext = "I dunno... should I?"
true8Default1.result = 2
true8Default.addDialog(true8Default1, True8)

def true8Default2 = [id:2]
true8Default2.npctext = "Oh, yes! We all love it here."
true8Default2.playertext = "So you're actually happy to be here waiting for the Zurg?"
true8Default2.result = 3
true8Default.addDialog(true8Default2, True8)

def true8Default3 = [id:3]
true8Default3.npctext = "You better believe it. Salvation is coming and I'm first in line!"
true8Default3.playertext = "Lucky you."
true8Default3.exec = { event ->
	event.player.unsetQuestFlag(GLOBAL, "Z07_Roderick_True8")
	event.player.addToPlayerVar("Z07_Roderick_BelieversSpokenTo", 1)
	event.player.centerPrint("True Believers Questioned ${event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo").intValue()}/7")
	if(event.player.getPlayerVar("Z07_Roderick_BelieversSpokenTo") >= 7) {
		event.player.updateQuest(331, "Tourist George-VQS")
	}
}
true8Default3.result = DONE
true8Default.addDialog(true8Default3, True8)

//---------------------------------------------------------
// TOURIST GEORGE (the BELIEVER)                           
//---------------------------------------------------------

def defaultRoderick = Roderick.createConversation( "defaultRoderick", true, "!Z07RoderickDefaultDone", "!Z07_Roderick_FromRyanDialog" )
defaultRoderick.setUrgent( true )

default1 = [id:1]
default1.npctext = "Hey there. Here...put this thought shield on. It'll keep the alien mind scanners from reading your thoughts!"
default1.playertext = "Ummm...that's a fold-up hat made of aluminum, buddy."
default1.result = 2
defaultRoderick.addDialog( default1, Roderick )

default2 = [id:2]
default2.npctext = "No! It just *looks* like that because this is the only portion of its fourth-dimensional aspects that we can see here in our limited perception space!"
default2.playertext = "Hoooo boy. You're a real live one, aren't you?"
default2.result = 3
defaultRoderick.addDialog( default2, Roderick )

default3 = [id:3]
default3.npctext = "I am for the moment. But who knows? Once the saucers come down from the sky, it'll be every man for himself!"
default3.playertext = "Riiiight. I'm going to slowly back away now..."
default3.result = 4
defaultRoderick.addDialog( default3, Roderick )

default4 = [id:4]
default4.npctext = "We will be the survivors that rebuild the world!"
default4.playertext = "Then the world is in big trouble. See ya."
default4.flag = [ "Z07RoderickDefaultDone" ]
default4.exec = { event ->
	if( event.player.getConLevel() >= 6.2 ) {
		event.player.setQuestFlag( GLOBAL, "Z07StartedVoxStellaOnceAlready" )
		Roderick.pushDialog( event.player, "tooOld" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z07VoxStartOkay" )
		Roderick.pushDialog( event.player, "voxStart" )
	}
}
default4.result = DONE
defaultRoderick.addDialog( default4, Roderick )

//---------------------------------------------------------
// The VOX STELLA (original start)                         
//---------------------------------------------------------

def voxStart = Roderick.createConversation( "voxStart", true, "Z07VoxStartOkay", "!Z07TooOldForAqueductQuests" )

def start1 = [id:1]
start1.npctext = "Wait! Don't go!"
start1.playertext = "Give me one good reason why I shouldn't."
start1.result = 2
voxStart.addDialog( start1, Roderick )

def start2 = [id:2]
start2.npctext = "We can talk to the stars!"
start2.options = []
start2.options << [ text: "Okay. Good reason. But, seriously?", result: 4 ]
start2.options << [ text: "Yeah, yeah. Pull the other leg. I'm outta here.", result: 3 ]
voxStart.addDialog( start2, Roderick )

def start3 = [id:3]
start3.npctext = "All right, all right. We won't force you to see the truth. But if you decide you want to hear more about it, come back to us. It might not be too late."
start3.playertext = "Uh...huh. You bet. Good luck."
start3.result = DONE
voxStart.addDialog( start3, Roderick )

def start4 = [id:4]
start4.npctext = "Very seriously. But to tell you the truth, we know *how* to talk to the stars, but we can't do it yet."
start4.playertext = "Oh, why am I not surprised?"
start4.result = 5
voxStart.addDialog( start4, Roderick )

def start5 = [id:5]
start5.npctext = "The key to us being able to do so is those aliens down the hill. Or more specifically, the scouting patrols that they send out."
start5.playertext = "I don't get it."
start5.result = 6
voxStart.addDialog( start5, Roderick )

def start6 = [id:6]
start6.npctext = "Neither did we. Eventually, we got curious enough and drew straws for someone to go down and nose around. Lola here drew the short straw."
start6.result = 7
voxStart.addDialog( start6, Roderick )

def start7 = [id:7]
start7.npctext = "It was scary, too!"
start7.playertext = "You sent *her* into danger? Big man there, Roderick. Big man."
start7.result = 8
voxStart.addDialog( start7, True5 )

def start8 = [id:8]
start8.npctext = "Hey! We all agreed to the rules ahead of time. She took her chances. And hey...she's okay, isn't she?"
start8.result = 9
voxStart.addDialog( start8, Roderick )

def start9 = [id:9]
start9.npctext = "Yeah...but Joie never came back, did she?"
start9.result = 10
voxStart.addDialog( start9, True7 )

def start10 = [id:10]
start10.npctext = "No, Spirit. But, remember? We all agreed that she got away and was back in Barton Town already. Didn't we agree that?"
start10.result = 11
voxStart.addDialog( start10, Roderick )

def start11 = [id:11]
start11.npctext = "Yes..."
start11.result = 12
voxStart.addDialog( start11, True7 )

def start12 = [id:12]
start12.npctext = "Good. Then back to the matter at hand. Lola found out that the patrols use communicators to report their positions from the field."
start12.options = []
start12.options << [text: "All right. What's so important about those communicators?", result: 14 ]
start12.options << [text: "Ummm...this seems kinda sketchy. I'm going to take off for now. Thanks anyway.", result: 13 ]
voxStart.addDialog( start12, Roderick )

def start13 = [id:13]
start13.npctext = "No! Wait...oh, drat. All right. But come back another time! We need your help!"
start13.result = DONE
voxStart.addDialog( start13, Roderick )

def start14 = [id:14]
start14.npctext = "We think...no...we're *sure* that the pups are using those communicators to send packet communications back to their home world!"
start14.playertext = "Um, wow. That's amazing. What makes you think that?"
start14.result = 15
voxStart.addDialog( start14, Roderick )

def start15 = [id:15]
start15.npctext = "Okay. We're guessing. But it makes sense if you watch their behaviors and how they use those dish arrays on their burrows. They're broadcasting signals!"
start15.playertext = "So get to the point. What do you need help with?"
start15.result = 16
voxStart.addDialog( start15, Roderick )

def start16 = [id:16]
start16.npctext = "We want you to get one of those communicators for us! If we can figure out their communications, then maybe we can learn to talk to them and become allies instead of enemies!"
start16.playertext = "You want *me* to do that? Why me?!?"
start16.result = 17
voxStart.addDialog( start16, Roderick )

def start17 = [id:17]
start17.npctext = "We see those fistfuls of rings you're wearing and the sensitive among us can sense the power you're radiating. If anyone can do it, then you're the one."
start17.options = []
start17.options << [text: "So how do I find this communicator?", result: 19 ]
start17.options << [text: "Oh no. Flattery will get you nowhere!", result: 18 ]
voxStart.addDialog( start17, Roderick )

def start18 = [id:18]
start18.npctext = "All right. But we really do need your help. Please come back to us after you consider it for a while."
start18.result = DONE
voxStart.addDialog( start18, Roderick )

def start19 = [id:19]
start19.npctext = "Yes! You'll help! Lola, tell him where you saw the pups using the Vox."
start19.playertext = "Vox?"
start19.result = 20
voxStart.addDialog( start19, Roderick )

def start20 = [id:20]
start20.npctext = "Vox Stellarum. It's Roderick's name for the communicator."
start20.result = 21
voxStart.addDialog( start20, True5 )

def start21 = [id:21]
start21.npctext = "It's Latin! It means 'Voice of the Stars'!"
start21.result = 22
voxStart.addDialog( start21, Roderick )

def start22 = [id:22]
start22.npctext = "Okay. I admit it. We all think that's kinda cool."
start22.playertext = "Okay, so I need to get this Vox thing. Where did you see it?"
start22.result = 23
voxStart.addDialog( start22, True5 )

def start23 = [id:23]
start23.npctext = "They don't seem to use them near the burrows much. But you can hear them using the units in the trees along the river and east of the burrows."
start23.playertext = "So find a pup in the woods that's using a Vox, bonk it on the head, grab the unit and return, right?"
start23.result = 24
voxStart.addDialog( start23, True5 )

def start24 = [id:24]
start24.npctext = "You've done this sort of thing before!"
start24.playertext = "Once or twice. How do I tell which ones have a Vox?"
start24.result = 25
voxStart.addDialog( start24, True5 )

def start25 = [id:25]
start25.npctext = "I think it's built into the armor they wear."
start25.playertext = "And I don't think they're going to offer that armor to me. That's going to take some doing."
start25.result = 26
voxStart.addDialog( start25, True5 )

def start26 = [id:26]
start26.npctext = "Which is precisely why I wanted to talk to you! We're not asking you to do it for free. We plan to reward you for your efforts."
start26.playertext = "Okay. The idea is good, reward or not. I'll see you when I see you."
start26.result = 27
voxStart.addDialog( start26, Roderick )

def start27 = [id:27]
start27.npctext = "Good luck! The hopes of interstellar peace go with you!"
start27.quest = 28 //start the VOX STELLA quest
start27.flag = [ "Z07StartedVoxStellaOnceAlready", "!Z07VoxStartOkay" ]
start27.exec = { event ->
	event.player.setPlayerVar( "Z07VoxPercentage", 0 )
}
start27.result = DONE
voxStart.addDialog( start27, Roderick )

//---------------------------------------------------------
//The "TOO OLD" conversation                               
//---------------------------------------------------------

def tooOld = Roderick.createConversation( "tooOld", true, "!Z07VoxStartOkay", "Z07StartedVoxStellaOnceAlready", "!QuestStarted_28", "Z07TooOldForAqueductQuests" )

def old1 = [id:1]
old1.npctext = "I thought I had a task worthy of your nature, but Spirit tells me that our petty concerns are beneath you. My apologies, worldly one."
old1.result = 2
tooOld.addDialog( old1, Roderick )

def old2 = [id:2]
old2.npctext = "What Roderick is trying to say, in his comic book nerd sort of way, is that your talents are wasted here. Go hit something more challenging and we'll find someone less powerful to help us."
old2.playertext = "Ah. Gotcha. Okay then. Thanks."
old2.exec = { event ->
	event.player.centerPrint( "To access the True Believers' task, you need to change your level to 6.1 or lower." )
	myManager.schedule(3) { event.player.centerPrint( "Use the 'CHANGE LEVEL' option in the MENU to lower your level." ) }
}
old2.result = DONE
tooOld.addDialog( old2, True7 )

//---------------------------------------------------------
// The VOX STELLA (subsequent starts)                      
//---------------------------------------------------------

def voxSubStart = Roderick.createConversation( "voxSubStart", true, "Z07StartedVoxStellaOnceAlready", "!QuestStarted_28", "!Z07TooOldForAqueductQuests" )

def subStart1 = [id:1]
subStart1.npctext = "Feeling like having another go at it, %p?"
subStart1.options = []
subStart1.options << [text: "Definitely. Interstellar peace seems like a heck of a goal. Same deal as last time?", result: 2]
subStart1.options << [text: "Yeah, eventually...but not now. I'll come back later.", result: DONE ]
voxSubStart.addDialog( subStart1, Roderick )

def subStart2 = [id:2]
subStart2.npctext = "Absolutely! Broken or whole, every piece we find gets us closer. Thanks!"
subStart2.quest = 28 //restart the VOX STELLA quest
subStart2.exec = { event ->
	event.player.setPlayerVar( "Z07VoxPercentage", 0 )
}
subStart2.result = DONE
voxSubStart.addDialog( subStart2, Roderick )


//---------------------------------------------------------
// The VOX STELLA (interim)                                
//---------------------------------------------------------

def voxInterim = Roderick.createConversation( "voxInterim", true, "QuestStarted_28:2", "!Z07TooOldForAqueductQuests" )

def int1 = [id:1]
int1.npctext = "Have you brought back the Vox already?"
int1.playertext = "No. Not yet. I still haven't found one. Are there any other clues?"
int1.result = 2
voxInterim.addDialog( int1, Roderick )

def int2 = [id:2]
int2.npctext = "Not really. But you need to watch yourself out there. With the combination of their stealth armor and the tunnel complex entrances they have in the area, you never know when one will pop up out of nowhere."
int2.playertext = "Okay, it was worth asking anyway."
int2.result = 3
voxInterim.addDialog( int2, Roderick )

def int3 = [id:3]
int3.npctext = "Good luck to you!"
int3.result = DONE
voxInterim.addDialog( int3, Roderick )

//---------------------------------------------------------
// The VOX STELLA (success)                                
//---------------------------------------------------------

def voxSucc = Roderick.createConversation( "voxSucc", true, "QuestStarted_28:3", "!Z07TooOldForAqueductQuests" )

def succ1 = [id:1]
succ1.npctext = "Did you find the Vox? Did you?"
succ1.playertext = "Ta da!"
succ1.result = 2
voxSucc.addDialog( succ1, Roderick )

def succ2 = [id:2]
succ2.npctext = "Oh. Wow..."
succ2.playertext = "Okay, okay. So it's damaged. It's the only one I could find!"
succ2.result = 3
voxSucc.addDialog( succ2, True5 )

def succ3 = [id:3]
succ3.npctext = "No. I just didn't expect quite...I mean, I'm sure we can salvage info from what you brought back. Thanks!"
succ3.playertext = "Well...you did say 'in any condition'."
succ3.result = 4
voxSucc.addDialog( succ3, True5 )

def succ4 = [id:4]
succ4.npctext = "And we meant it! Thanks for helping us out! If you're free another time, we'll offer you the same deal to find another Vox for us."
succ4.playertext = "Will do, Roderick. Good luck with your mission!"
succ4.quest = 28 //push the completion step of VOX STELLA
succ4.flag = "!Z07FoundTheVox" //unset this flag so you can find the Vox again the next time you take this quest
succ4.exec = { event ->
	sayGoodbye( event )
}
succ4.result = DONE
voxSucc.addDialog( succ4, Roderick )


def synchronized sayGoodbye( event ) {
	myManager.schedule(3) {
		True1.say( "Goodbye, ${event.player}!" )
		True2.say( "Thank you, ${event.player}!" )
		True3.say( "Let the Pax be with you, ${event.player}!" )
		True5.say( "Thanks, ${event.player}!" )
		True6.say( "Be safe, ${event.player}!" )
		True7.say( "Thanks again, ${event.player}!" )
		True8.say( "Thanks for helping!" )
	}
}
	




