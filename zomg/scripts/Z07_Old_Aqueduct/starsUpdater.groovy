def starsUpdater = "starsUpdater"
myRooms.Aqueduct_602.createTriggerZone(starsUpdater, 1055, 1090, 1620, 1200)
myManager.onTriggerIn(myRooms.Aqueduct_602, starsUpdater) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(229)) {
		event.actor.updateQuest(229, "Story-VQS")
	}
}