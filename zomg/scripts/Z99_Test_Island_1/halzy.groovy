/*
import halzy.Killer;

mrhalzy = spawnNPC( "halzy",  myRooms.TestIsland1_1, 300, 300 )
mrhalzy.setDisplayName("[NPC] Mr Halzy")

def createMenu(actor, id, text, subtext, options, width) {
	uiButtonMenu(actor, id, text, subtext, options, width, 5) { event->
		mrhalzy.say("You chose ${event.selection}");
	}
}

def conv1 = mrhalzy.createConversation("halzysFirstTestConv", true)

def halzyMain1 = [id:1]
halzyMain1.npctext = "Chose"
halzyMain1.options = []
halzyMain1.options << [text:"Special of the day", exec:{ event -> new Killer(event.player) }, result:1]
halzyMain1.options << [text:"Create 1 2 Menu", exec:{ event -> createMenu(event.player, "twoMenu", "Two Options", null, ["one","two"], 400) }, result:1]
halzyMain1.options << [text:"Create 1 2 3 Menu", exec:{ event -> createMenu(event.player, "threeMenu", "Three Options", null, ["one","two","three"], 400) }, result:1]
halzyMain1.options << [text:"Create 1 2 3 4 Menu", exec:{ event -> createMenu(event.player, "fourMenu", "Four Options", "Now with subtext!!!", ["one","two","three","four"], 400) }, result:1]
halzyMain1.options << [text:"add quest", exec:{ event -> event.player.updateQuest(92, "BFG-Remo") }, result:1]
halzyMain1.options << [text:"remove quest", exec:{ event -> event.player.removeQuest(92) }, result:1]
conv1.addDialog(halzyMain1, mrhalzy)
*/
