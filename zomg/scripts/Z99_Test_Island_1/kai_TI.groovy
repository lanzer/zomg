THIS_ROOM = myRooms.TestIsland1_2

////////// MehYam's menu
////////////////////////

def bigTest()
{
	makeZoneTimer("Big Test Timer", "05:00:10", "00:00:0").start()
    makeZoneCounter("Big Test Counter", 0)
	zoneBroadcast("MehYam", "Big Test is Active", "There's a broadcast, counter, and timer open permanently")
}

def miniMapX = 100
def miniMapY = 100
def addMiniMapFlags()
{
	addMiniMapMarker("flag one", "markerNew", THIS_ROOM, miniMapX, miniMapY)
	
	miniMapX += 100
	if (miniMapX > 600)
	{
		miniMapX = 100
		miniMapY += 100
	}
}

dailyChanceCheck = false;
def runDailyChance(event)
{
	dailyChanceCheck = !dailyChanceCheck;
	if (dailyChanceCheck)
	{
		checkDailyChance(event.actor, 210, { result -> event.actor.centerPrint( "Daily Chance availability result: " + result ) }); 
	}
	else
	{
		runDailyChance(event.actor, 210);
	}
}

def kaiNPC = spawnNPC( "MehYam",  THIS_ROOM, 500, 400 )
kaiNPC.setDisplayName("[NPC] MehYam")

def kaiConv = kaiNPC.createConversation("kaisconv", true)
def kaiDialog = [id:1]
kaiDialog.npctext = "What shall we test, oh great one?"
kaiDialog.options = []
kaiDialog.options << [text: "Counters", result: 2]
kaiDialog.options << [text: "Timers", result: 3]
kaiDialog.options << [text: "Zone broadcasts", result: 4]
kaiDialog.options << [text: "Big world event test", result: 1, exec: { bigTest() }]
kaiDialog.options << [text: "eventDone()", result: 1, exec: { eventDone() }]
kaiDialog.options << [text: "Daily Chance", result: 1, exec: { event -> runDailyChance(event) }]
//kaiDialog.options << [text: "Add a minimap flag", result: 1, exec: { addMiniMapFlags() }]
kaiConv.addDialog(kaiDialog, kaiNPC)


PLAYER_COUNTER = "Player counter"
KILL_COUNTER = "Trashcans canned"
ZONE_COUNTER = "Zone counter"
TEST_COUNTER = "Per-player zone counter"
MOB_TYPE = "fluff_training"

def onAddPlayerCounter(event)
{
    makeCounter(event.actor, PLAYER_COUNTER, 0)
}
def onRemovePlayerCounter(event)
{
	removeCounter(event.actor, PLAYER_COUNTER)
}
def onAddKillCounter(event)
{
    makeCounter(event.actor, KILL_COUNTER, 0).watchForKill(MOB_TYPE).setGoal(3).onCompletion(
    	{evt-> removeCounter(evt.player, KILL_COUNTER)}
	)
}
def onRemoveKillCounter(event)
{
	removeCounter(event.actor, KILL_COUNTER)
}
def onAddZoneCounter()
{
    makeZoneCounter(ZONE_COUNTER, 0)
}
def onRemoveZoneCounter()
{
    removeZoneCounter(ZONE_COUNTER)
}
def onAddPerPlayerZoneCouner()
{
    makeZoneCounter(TEST_COUNTER, 0, true)
}
def onIncPerPlayerZoneCouner(event)
{
	getZoneCounter(TEST_COUNTER).getPlayerCounter(event.actor).increment()
}
def onRemovePerPlayerZoneCouner()
{
	removeZoneCounter(TEST_COUNTER)
}

kaiDialog = [id:2]
kaiDialog.npctext = "Do what with counters?"
kaiDialog.options = []
kaiDialog.options << [text: "Add per-player zone counter", result: 1, exec: { onAddPerPlayerZoneCouner() }]
kaiDialog.options << [text: "Inc per-player zone counter", result: 1, exec: { event -> onIncPerPlayerZoneCouner(event) }]
kaiDialog.options << [text: "Remove per-player zone counter", result: 1, exec: { onRemovePerPlayerZoneCouner() }]
kaiDialog.options << [text: "Add player counter", result: 1, exec: { event -> onAddPlayerCounter(event) }]
kaiDialog.options << [text: "Remove player counter", result: 1, exec: { event -> onRemovePlayerCounter(event) }]
kaiDialog.options << [text: "Add kill counter", result: 1, exec: { event -> onAddKillCounter(event) }]
kaiDialog.options << [text: "Remove kill counter", result: 1, exec: { event -> onRemoveKillCounter(event) }]
kaiDialog.options << [text: "Add zone counter", result: 1, exec: { onAddZoneCounter() }]
kaiDialog.options << [text: "Remove zone counter", result: 1, exec: { onRemoveZoneCounter() }]
kaiConv.addDialog(kaiDialog, kaiNPC)

PLAYER_TIMER = "Player timer"
ZONE_TIMER = "Zone timer"
def onAddPlayerTimer(event)
{
	makeTimer(event.actor, PLAYER_TIMER, "00:00:00", "00:0:20")
		.onCompletion({
			evt-> removeTimer(evt.player, PLAYER_TIMER)
			event.actor.centerPrint( "Player timer has completed" )
		})
		.start()
}
def onRemovePlayerTimer(event)
{
	removeTimer(event.actor, PLAYER_TIMER)
}
def showRemainingTimerSeconds(event)
{
	event.actor.centerPrint( "Seconds left: " + getTimer(event.actor, PLAYER_TIMER).getRemainingSeconds());
}
def onAddZoneTimer(eventIn)
{
	makeZoneTimer(ZONE_TIMER, "00:00:20", "00:0:0")
		.onCompletion({
		    removeZoneTimer(ZONE_TIMER)
			println("Timer onCompletion firing")
		})
		.start()
}
def onRemoveZoneTimer(event)
{
	removeZoneTimer(ZONE_TIMER)
}
def toggleTimer(timer)
{
	if (timer)
	{
	    if (timer.isPlaying())
	    {
	    	timer.stop()
	    }
	    else
	    {
	    	timer.start()
	    }
	}
}
def toggleTimers(event)
{
	toggleTimer(getTimer(event.actor, PLAYER_TIMER))
	toggleTimer(getZoneTimer(ZONE_TIMER))
}

kaiDialog = [id:3]
kaiDialog.npctext = "Do what with timers?"
kaiDialog.options = []
kaiDialog.options << [text: "Add player timer", result: 1, exec: { event -> onAddPlayerTimer(event) }]
kaiDialog.options << [text: "Show player time left", result: 1, exec: { event -> showRemainingTimerSeconds(event) }]
kaiDialog.options << [text: "Remove player timer", result: 1, exec: { event -> onRemovePlayerTimer(event) }]
kaiDialog.options << [text: "Add zone timer", result: 1, exec: { onAddZoneTimer() }]
kaiDialog.options << [text: "Remove zone timer", result: 1, exec: { onRemoveZoneTimer() }]
kaiDialog.options << [text: "Toggle timers", result: 1, exec: { event -> toggleTimers(event) }]
kaiConv.addDialog(kaiDialog, kaiNPC)

BROADCAST = "MehYam's Pwnage"
def onAddZoneBroadcast()
{
	zoneBroadcast("MehYam", BROADCAST, "MehYam's just pwned you!  This event lasts for the rest of ur n00b life!");
}
def onAddTempZoneBroadcast()
{
	zoneBroadcast("MehYam", BROADCAST, "MehYam's just pwned you!  This event lasts for the rest of ur n00b life, or 10 seconds, whatever comes first!");

	myManager.schedule(10)
	{
		removeZoneBroadcast(BROADCAST)
	} 
}
def onRemoveZoneBroadcast()
{
	removeZoneBroadcast(BROADCAST)
}

kaiDialog = [id:4]
kaiDialog.npctext = "Do what with zone broadcasts?"
kaiDialog.options = []
kaiDialog.options << [text: "Add zone broadcast", result: 1, exec: { onAddZoneBroadcast() }]
kaiDialog.options << [text: "Add temporary (10 second) zone broadcast", result: 1, exec: { onAddTempZoneBroadcast() }]
kaiDialog.options << [text: "Remove zone broadcast", result: 1, exec: { onRemoveZoneBroadcast() }]
kaiConv.addDialog(kaiDialog, kaiNPC)

//////// Trash can killfest
///////////////////////////

garbage = THIS_ROOM.spawnSpawner( "garbage", MOB_TYPE, 1 )
garbage2 = THIS_ROOM.spawnSpawner( "garbage", MOB_TYPE, 1 )
garbage3 = THIS_ROOM.spawnSpawner( "garbage", MOB_TYPE, 1 )
garbage.setPos( 650, 495 ) 
garbage2.setPos( 600, 495 ) 
garbage3.setPos( 550, 495 ) 
garbage.setMonsterLevelForChildren( 1.0 )
garbage.setGuardPostForChildren( "TestIsland1_2", 635, 485, 315 )

garbage.stopSpawning()
garbage2.stopSpawning()
garbage3.stopSpawning()

practiceTarget = garbage.forceSpawnNow()
practiceTarget2 = garbage2.forceSpawnNow()
practiceTarget3 = garbage3.forceSpawnNow()

targetDeathWatch()
targetDeathWatch2()
targetDeathWatch3()

//Once activated, these routines run indefinitely, allowing the player to practice against them as much as desired.
def targetDeathWatch() {
	runOnDeath(practiceTarget, { event ->
		myManager.schedule(3) { 
			practiceTarget = garbage.forceSpawnNow()
			
			targetDeathWatch()
		}
		updateCounter(event.actor)
	})
}
def targetDeathWatch2() {
	runOnDeath(practiceTarget2, { event ->
		myManager.schedule(3) { 
			practiceTarget2 = garbage2.forceSpawnNow()
			
			targetDeathWatch2()
		}
		updateCounter(event.actor)
	})
}
def targetDeathWatch3() {
	runOnDeath(practiceTarget3, { event ->
		myManager.schedule(3) { 
			practiceTarget3 = garbage3.forceSpawnNow()
			
			targetDeathWatch3()
		}
		updateCounter(event.actor)
	})
}

def updateCounter(monster)
{
    collector = []
    collector.addAll(monster.getHated())
    collector.each
    {
    	def counter = getCounter(it, PLAYER_COUNTER);
    	if (counter)
    	{
    		counter.increment();
    	}
	}
	def counter2 = getZoneCounter(ZONE_COUNTER)
	if (counter2)
	{
		counter2.increment();
	}
}
