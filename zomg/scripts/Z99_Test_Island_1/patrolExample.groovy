//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

patrolExampleSpawner1 = myRooms.TestIsland1_102.spawnSpawner( "patrolExampleSpawner1", "omg", 1)
patrolExampleSpawner1.setPos(65, 610)
patrolExampleSpawner1.setHomeTetherForChildren(2000)
patrolExampleSpawner1.setSpawnWhenPlayersAreInRoom(true)
patrolExampleSpawner1.setMonsterLevelForChildren(1.0)
patrolExampleSpawner1.addPatrolPointForChildren("TestIsland1_102", 980, 65, 10)
patrolExampleSpawner1.addPatrolPointForChildren("TestIsland1_102", 65, 610, 10)

patrolExampleSpawner1.spawnAllNow()

patrolExampleSpawner2 = myRooms.TestIsland1_102.spawnStoppedSpawner( "patrolExampleSpawner2", "omg", 1)
patrolExampleSpawner2.setPos(65, 65)
patrolExampleSpawner2.setHomeTetherForChildren(2000)
patrolExampleSpawner2.setSpawnWhenPlayersAreInRoom(true)
patrolExampleSpawner2.setMonsterLevelForChildren(1.0)

patrolMonster2 = patrolExampleSpawner2.forceSpawnNow()

patrol2 = makeNewPatrol()
patrol2.addPatrolPoint("TestIsland1_102", 980, 610, 10)
patrol2.addPatrolPoint("TestIsland1_102", 65, 65, 10)

patrolMonster2.setPatrol(patrol2)
patrolMonster2.startPatrol()