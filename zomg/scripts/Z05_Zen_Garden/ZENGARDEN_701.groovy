import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_701.spawnSpawner("ZG_701_Spawner1", "cherry_fluff", 3) 
spawner1.setPos(930, 230)
spawner1.setWanderBehaviorForChildren( 50, 120, 2, 5, 300)
spawner1.setWaitTime( 30, 50 )
spawner1.setMonsterLevelForChildren( 2.8 )

//STARTUP LOGIC
spawner1.spawnAllNow()