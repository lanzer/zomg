//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def duremBlockadeUpdater = "duremBlockadeUpdater"
myRooms.ZENGARDEN_401.createTriggerZone(duremBlockadeUpdater, 70, 260, 155, 395)
myManager.onTriggerIn(myRooms.ZENGARDEN_401, duremBlockadeUpdater) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(224)) {
		event.actor.updateQuest(224, "Story-VQS")
	}
}