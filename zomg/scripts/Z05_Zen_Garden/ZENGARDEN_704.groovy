import com.gaiaonline.mmo.battle.script.*;

//===========================
// ZENGARDEN_704 ELEMENTS    
//===========================

def spawner1 = myRooms.ZENGARDEN_704.spawnSpawner("spawner1", "cherry_fluff", 2) 
spawner1.setPos( 630, 120 )
spawner1.setWaitTime( 30, 50 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 5, 250)
spawner1.setMonsterLevelForChildren( 3.1 )

LT1 = myRooms.ZENGARDEN_704.spawnSpawner( "LT1", "kokeshi_doll_LT", 1 )
LT1.setPos( 680, 360 )
LT1.setWaitTime( 120, 240 )
LT1.setMonsterLevelForChildren( 3.3 )
LT1.addPatrolPointForChildren( "ZENGARDEN_704", 920, 565, 3 )
LT1.addPatrolPointForChildren( "ZENGARDEN_704", 680, 360, 3 )

LT2 = myRooms.ZENGARDEN_704.spawnSpawner( "LT2", "kokeshi_doll_LT", 1 )
LT2.setPos( 920, 565 )
LT2.setWaitTime( 120, 240 )
LT2.setMonsterLevelForChildren( 3.3 )
LT2.addPatrolPointForChildren( "ZENGARDEN_704", 680, 360, 3 )
LT2.addPatrolPointForChildren( "ZENGARDEN_704", 920, 565, 3 )

doll1 = myRooms.ZENGARDEN_704.spawnSpawner( "doll1", "kokeshi_doll", 1 )
doll1.setPos( 620, 320 )
doll1.setGuardPostForChildren( "ZENGARDEN_704", 620, 320, 225 )
doll1.setWaitTime( 50, 70 )
doll1.setMonsterLevelForChildren( 3.2 )

doll2 = myRooms.ZENGARDEN_704.spawnSpawner( "doll2", "kokeshi_doll", 1 )
doll2.setPos( 420, 460 )
doll2.setGuardPostForChildren( "ZENGARDEN_704", 420, 460, 225 )
doll2.setWaitTime( 50, 70 )
doll2.setMonsterLevelForChildren( 3.2 )

//ALLIANCES
doll1.allyWithSpawner( doll2 )

LT1.allyWithSpawner( LT2 )


//STARTUP LOGIC
spawner1.spawnAllNow()
LT1.forceSpawnNow()
LT2.forceSpawnNow()
doll1.forceSpawnNow()
doll2.forceSpawnNow()

//===========================================
// THE DOLL SHRINE SWITCH AND LOGIC          
//===========================================

timerMap = [:]
onClickArchBlock = new Object()

shrineArch = makeSwitch( "ShrineArchway_In", myRooms.ZENGARDEN_704, 970, 340 )
shrineArch.unlock()
shrineArch.off()
shrineArch.setRange( 300 )
shrineArch.setMouseoverText("The Shrine of the Doll")

def enterArch = { event ->
	synchronized( onClickArchBlock ) {
		shrineArch.off()
		if( isPlayer( event.actor ) && myRooms.ZENGARDEN_704.getSpawnTypeCount( "kokeshi_doll_LT" ) == 0 ) {
			event.actor.getCrew().each() { 
				if( it.isOnQuest( 47 ) || it.isDoneQuest( 47 ) ) {
					event.actor.setQuestFlag( GLOBAL, "Z05TempKatDollEntranceAllowed" )
				}
			}
			if( event.actor.hasQuestFlag( GLOBAL, "Z05TempKatDollEntranceAllowed" ) ) {
				event.actor.unsetQuestFlag( GLOBAL, "Z05TempKatDollEntranceAllowed" )
				if( event.actor.getConLevel() < 3.0 ) {
					event.actor.centerPrint( "You must have a level of at least 3.0 to enter the Shrine of the Doll." )

				//********************* START DIFFICULTY SETTINGS ****************************
				} else if( event.actor.getTeam().hasAreaVar( "Z26_Katsumis_Doll", "Z26DollShrineDifficulty" ) == false ) {
					if( event.actor.getCrewVar( "Z26DollDifficultySelectionInProgress" ) == 0 ) {
						event.actor.setCrewVar( "Z26DollDifficultySelectionInProgress", 1 )
					
						//check to see if the Crew leader is in the zone
						if( isInZone( event.actor.getTeam().getLeader() ) ) {
							leader = event.actor.getTeam().getLeader()
							makeMenu()

							event.actor.getCrew().each{ if( it != event.actor.getTeam().getLeader() ) { it.centerPrint( "Your Crew Leader is choosing the challenge level for this encounter." ) } }

							//Everyone else gets the following message while the leader chooses a setting. If no choice is made after 10 seconds, then everyone gets a message that says:
							decisionTimer = myManager.schedule(30) {
								event.actor.getCrew().each{
									it.centerPrint( "No choice was made within 30 seconds. Click the Shrine to try again." )
								}
								event.actor.setCrewVar( "Z26DollDifficultySelectionInProgress", 0 )
								//if no selection was made, then remove the leader and timer from the timerMap
								timerMap.remove( event.actor.getTeam().getLeader() )
							}
							
							//add the leader and timer to a map so the timer can be canceled later, if necessary
							timerMap.put( event.actor.getTeam().getLeader(), decisionTimer )

						//If the leader is not in the zone then tell the crew the leader must be present
						} else {
							event.actor.getCrew().each{
								if( it != event.actor.getTeam().getLeader() ) {
									it.centerPrint( "Your Crew Leader must be in Zen Gardens before you can enter the Shrine of the Doll" )
								} else {
									it.centerPrint( "Your Crew is trying to enter the Shrine of the Doll. You must be in Zen Gardens for that to occur." )
								}
							}
							event.actor.setCrewVar( "Z26DollDifficultySelectionInProgress", 0 )
						}
					} else if( event.actor.getCrewVar( "Z26DollDifficultySelectionInProgress" ) == 1 ) {
						if( event.actor != event.actor.getTeam().getLeader() ) {
							event.actor.centerPrint( "Your Crew Leader is already making a challenge level choice. One moment, please..." )
						}
					}

				//If the Difficulty is not zero, then it has been set and the player can enter the Shrine
				} else if( event.actor.getTeam().hasAreaVar( "Z26_Katsumis_Doll", "Z26DollShrineDifficulty" ) == true ) { 					
					event.actor.centerPrint( "You pass through the archway and into the Shrine of the Doll!" )
					event.actor.warp( "KatsumisDoll_1", 185, 405 )	
				//********************* END DIFFICULTY SETTINGS ****************************

				//If the actor is too veteran for the scenario then tell them they must suppress first
				}
			} else {
				event.actor.centerPrint( "You may not enter...yet. Speak with Katsumi or Crew with someone that has previously entered the Shrine." )
			}
		} else if( isPlayer( event.actor ) && myRooms.ZENGARDEN_704.getSpawnTypeCount( "kokeshi_doll_LT" ) > 0 ) {
			event.actor.centerPrint( "You must defeat the nearby guardians before you may pass through the archway." )
		}
	}
}

shrineArch.whenOn( enterArch )

def synchronized makeMenu() {
	descripString = "You must choose a Challenge Level to enter the Shrine of the Doll. You have 30 seconds to decide."
	diffOptions = ["Easy (small rewards)", "Normal (moderate rewards)", "Hard (large rewards)", "Cancel"]
	
	println "**** trying to create the menu ****"
	uiButtonMenu( leader, "diffMenu", descripString, diffOptions, 300, 30 ) { event ->
		if( event.selection == "Easy (small rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Easy' challenge setting for this scenario. Click the archway again to enter." )
				} else {
					it.centerPrint( "You chose the 'Easy' challenge setting for this scenario. Click the archway again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z26_Katsumis_Doll", "Z26DollShrineDifficulty", 1 )
			event.actor.setCrewVar( "Z26DollDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Normal (moderate rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Normal' challenge setting for this scenario. Click the archway again to enter." )
				} else {
					it.centerPrint( "You chose the 'Normal' challenge setting for this scenario. Click the archway again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z26_Katsumis_Doll", "Z26DollShrineDifficulty", 2 )
			event.actor.setCrewVar( "Z26DollDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Hard (large rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Hard' challenge setting for this scenario. Click the archway again to enter." )
				} else {
					it.centerPrint( "You chose the 'Hard' challenge setting for this scenario. Click the archway again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z26_Katsumis_Doll", "Z26DollShrineDifficulty", 3 )
			event.actor.setCrewVar( "Z26DollDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Cancel" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose to not select a challenge setting at this time." )
				}
			}
			event.actor.setCrewVar( "Z26DollDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
	}
}