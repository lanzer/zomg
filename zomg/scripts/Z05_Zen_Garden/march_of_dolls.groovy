import com.gaiaonline.mmo.battle.script.*;

//================================================================
//THE MARCH OF THE DOLLS                                          
//Below is the patrol path that the dolls follow when marching    
//around the shrine path (one clockwise, the other counter-clock).
//Spawners are down in rooms 704, 705, and 605.                   
//When respawned, the dolls will head north to join the march.    
//================================================================

//CLOCKWISE
March1 = myRooms.ZENGARDEN_605.spawnSpawner("March1", "kokeshi_doll", 2)
March1.setPos(840, 475)
March1.setMonsterLevelForChildren( 3.1 )
March1.addPatrolPointForChildren( "ZENGARDEN_605", 700, 295, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_605", 200, 430, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_604", 960, 540, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_604", 440, 600, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_704", 140, 80, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_703", 785, 90, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_603", 470, 470, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_603", 900, 490, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_604", 120, 330, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_604", 160, 110, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_504", 390, 630, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_504", 840, 525, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_505", 205, 515, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_505", 420, 470, 0 )
March1.addPatrolPointForChildren( "ZENGARDEN_505", 575, 600, 0 )

//CLOCKWISE
March2 = myRooms.ZENGARDEN_704.spawnSpawner("March2", "kokeshi_doll", 2)
March2.setPos(300, 470)
March2.setMonsterLevelForChildren( 3.1 )
March2.addPatrolPointForChildren( "ZENGARDEN_604", 960, 540, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_604", 440, 600, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_704", 140, 80, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_703", 785, 90, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_603", 470, 470, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_603", 900, 490, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_604", 120, 330, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_604", 160, 110, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_504", 390, 630, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_504", 840, 525, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_505", 205, 515, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_505", 420, 470, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_505", 575, 600, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_605", 700, 295, 0 )
March2.addPatrolPointForChildren( "ZENGARDEN_605", 200, 430, 0 )

//COUNTER-CLOCKWISE
March3 = myRooms.ZENGARDEN_605.spawnSpawner("March3", "kokeshi_doll", 2)
March3.setPos(680, 550)
March3.setMonsterLevelForChildren( 3.2 )
March3.addPatrolPointForChildren( "ZENGARDEN_605", 700, 295, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_505", 575, 600, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_505", 420, 470, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_505", 205, 515, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_504", 840, 525, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_504", 390, 630, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_604", 160, 110, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_604", 120, 330, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_603", 900, 490, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_603", 470, 470, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_703", 785, 90, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_704", 140, 80, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_604", 440, 600, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_604", 960, 540, 0 )
March3.addPatrolPointForChildren( "ZENGARDEN_605", 200, 430, 0 )

//CLOCKWISE
March4 = myRooms.ZENGARDEN_605.spawnSpawner("March4", "kokeshi_doll", 2)
March4.setPos(950, 270)
March4.setMonsterLevelForChildren( 3.2 )
March4.addPatrolPointForChildren( "ZENGARDEN_605", 700, 295, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_605", 200, 430, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_604", 960, 540, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_604", 440, 600, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_704", 140, 80, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_703", 785, 90, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_603", 470, 470, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_603", 900, 490, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_604", 120, 330, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_604", 160, 110, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_504", 390, 630, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_504", 840, 525, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_505", 205, 515, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_505", 420, 470, 0 )
March4.addPatrolPointForChildren( "ZENGARDEN_505", 575, 600, 0 )


//COUNTER-CLOCKWISE
March5 = myRooms.ZENGARDEN_704.spawnSpawner("March5", "kokeshi_doll", 2)
March5.setPos(810, 110)
March5.setMonsterLevelForChildren( 3.2 )
March5.addPatrolPointForChildren( "ZENGARDEN_605", 700, 295, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_505", 575, 600, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_505", 420, 470, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_505", 205, 515, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_504", 840, 525, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_504", 390, 630, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_604", 160, 110, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_604", 120, 330, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_603", 900, 490, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_603", 470, 470, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_703", 785, 90, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_704", 140, 80, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_604", 440, 600, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_604", 960, 540, 0 )
March5.addPatrolPointForChildren( "ZENGARDEN_605", 200, 430, 0 )

//MISCELLANEOUS DOLL SPAWNERS
spawner405 = myRooms.ZENGARDEN_405.spawnSpawner("spawner405", "kokeshi_doll", 2) 
spawner405.setPos(410, 315)
spawner405.setWaitTime( 50, 70 )
spawner405.setWanderBehaviorForChildren( 50, 150, 2, 5, 350)
spawner405.setMonsterLevelForChildren( 2.8 )

spawner505 = myRooms.ZENGARDEN_505.spawnSpawner("spawner505", "kokeshi_doll", 2) 
spawner505.setPos(745, 415)
spawner505.setWaitTime( 50, 70 )
spawner505.setWanderBehaviorForChildren( 50, 150, 2, 5, 200)
spawner505.setMonsterLevelForChildren( 3.0 )

spawner602A = myRooms.ZENGARDEN_602.spawnSpawner("spawner602A", "kokeshi_doll", 2) 
spawner602A.setPos( 790, 630 )
spawner602A.setWaitTime( 50, 70 )
spawner602A.setWanderBehaviorForChildren( 50, 150, 2, 5, 250)
spawner602A.setMonsterLevelForChildren( 2.9 )

spawner702A = myRooms.ZENGARDEN_702.spawnSpawner("spawner702A", "kokeshi_doll", 2) 
spawner702A.setPos( 590, 430 )
spawner702A.setWaitTime( 50, 70 )
spawner702A.setWanderBehaviorForChildren( 50, 150, 2, 5, 250)
spawner702A.setMonsterLevelForChildren( 2.9 )

spawner703Random = myRooms.ZENGARDEN_703.spawnSpawner("spawner703Random", "kokeshi_doll", 2) 
spawner703Random.setPos(240, 130)
spawner703Random.setWaitTime( 50, 70 )
spawner703Random.setWanderBehaviorForChildren( 50, 150, 2, 5, 200)
spawner703Random.setMonsterLevelForChildren( 3.0 )

spawner503Random = myRooms.ZENGARDEN_503.spawnSpawner("spawner503Random", "kokeshi_doll", 2) 
spawner503Random.setPos(640, 220)
spawner503Random.setWaitTime( 50, 70 )
spawner503Random.setWanderBehaviorForChildren( 50, 150, 2, 5, 200)
spawner503Random.setMonsterLevelForChildren( 3.0 )

spawner504Random = myRooms.ZENGARDEN_504.spawnSpawner("spawner504Random", "kokeshi_doll", 2) 
spawner504Random.setPos(490, 120)
spawner504Random.setWaitTime( 50, 70 )
spawner504Random.setWanderBehaviorForChildren( 50, 150, 2, 5, 200)
spawner504Random.setMonsterLevelForChildren( 3.0 )

//A bit of fun randomness. An LT blocks this path occasionally.
pathGuardian = myRooms.ZENGARDEN_503.spawnSpawner("pathGuardian", "kokeshi_doll_LT", 1) 
pathGuardian.setPos(870, 470)
pathGuardian.setWaitTime( 3600, 4800 )
pathGuardian.setGuardPostForChildren( "ZENGARDEN_503", 870, 470, 90 )
pathGuardian.setMonsterLevelForChildren( 3.5 )

//===========================================
//ALLIANCES                                  
//===========================================

March1.allyWithSpawner(March2)
March1.allyWithSpawner(March3)
March1.allyWithSpawner(March4)
March1.allyWithSpawner(March5)
March2.allyWithSpawner(March3)
March2.allyWithSpawner(March4)
March2.allyWithSpawner(March5)
March3.allyWithSpawner(March4)
March3.allyWithSpawner(March5)
March4.allyWithSpawner(March5)

March1.stopSpawning()
March2.stopSpawning()
March3.stopSpawning()
March4.stopSpawning()
March5.stopSpawning()
spawner405.stopSpawning()
spawner505.stopSpawning()
spawner702A.stopSpawning()

//===========================
//REPOPULATING LOGIC - DOLLS 
//===========================

totalAreaMaxSpawn = 16
maxSpawn = 2
dollList = [ March1, March2, March3, March4, March5, spawner405, spawner505, spawner702A ]
dollHateCollector = []
weavePercent = 60

//continuously try to repopluate the area near the road
def repopulateDolls() {
	totalAreaCurrentSpawn = March1.spawnsInUse() + March2.spawnsInUse() + March3.spawnsInUse() + March4.spawnsInUse() + March5.spawnsInUse() + spawner405.spawnsInUse() + spawner505.spawnsInUse() + spawner702A.spawnsInUse()
	if( totalAreaCurrentSpawn < totalAreaMaxSpawn ) {
		dollSpawner = random( dollList )
		if( dollSpawner.spawnsInUse() < maxSpawn ) {
			newDoll = dollSpawner.forceSpawnNow()
			runOnDeath( newDoll, { event -> dollHateCollector = []; dollHateCollector.addAll( event.actor.getHated() ); checkForDollLoot() } )
		}
	}
	myManager.schedule( random( 10 ) ) { repopulateDolls() }
}

//upon the death of a gnome, see if it drops loot for a player on the quest
//weaveDenialMessage = [ "Hmmm...no mystic weave here!", "Nope. Nada. Maybe next time!", "No weaves on this one!", "Rats. No weaves.", "Maybe next time!", "You don't find any mystic weave.", "No love here. It has no weave." ]

def synchronized checkForDollLoot() {
	dollHateCollector.each{ 
		if( isPlayer( it ) && it.isOnQuest( 285, 2 ) ) { //if the player is on the DREAM WEAVER quest
			if( it.getConLevel() < 4.2 ) {
				roll = random( 100 )
				if( roll <= weavePercent ) {
					it.addPlayerVar( "Z05MysticWeavesCollected", 1 )
					it.grantItem( "100381" )
					//if the player has already collected ten or more leaves, then complete the mission
					if( it.getPlayerVar( "Z05MysticWeavesCollected" ) >= 15 ) {
						//it.centerPrint( "You have all the Mystic Weaves that Katsumi needs!" )  -  Commenting this out for now since it is not necessarily the same as when they actually finish the quest
						it.deletePlayerVar( "Z05MysticWeavesCollected" ) //clean up this playerVar so it's not in the player record forever.
					}					
				}
			} else {
				it.centerPrint( "You must change your level to below 4.2 to gather Mystic Weaves for Katsumi." )
			}
		}
	}
}

repopulateDolls()
