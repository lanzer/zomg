import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_2.spawnSpawner("spawner1", "taiko_drum", 1)
spawner1.setPos(250, 200)
spawner1.setWanderBehaviorForChildren( 50, 100, 2, 7, 300)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 3.6 )

def spawner2 = myRooms.ZENGARDEN_2.spawnSpawner("spawner2", "taiko_drum", 2)
spawner2.setPos(250, 500)
spawner2.setWanderBehaviorForChildren( 50, 100, 2, 5, 300)
spawner2.setWaitTime( 50, 70 )
spawner2.setMonsterLevelForChildren( 3.6 )

def spawner3 = myRooms.ZENGARDEN_2.spawnSpawner("spawner3", "taiko_drum", 2)
spawner3.setPos(640, 300)
spawner3.setWanderBehaviorForChildren( 50, 100, 3, 6, 300)
spawner3.setWaitTime( 50, 70 )
spawner3.setMonsterLevelForChildren( 3.6 )


//STARTUP LOGIC
spawner1.spawnAllNow()
spawner2.spawnAllNow()
spawner3.spawnAllNow()
