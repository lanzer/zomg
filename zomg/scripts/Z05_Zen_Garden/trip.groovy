import com.gaiaonline.mmo.battle.script.*;

Z5Trip = spawnNPC("BFG-Trip", myRooms.ZENGARDEN_401, 137, 275)
Z5Trip.setDisplayName( "Trip" )

onQuestStep( 58, 2 ) { event -> event.player.addMiniMapQuestActorName( "BFG-Trip" ) }
onQuestStep( 283, 2 ) { event -> event.player.addMiniMapQuestActorName( "BFG-Trip" ) }

//---------------------------------------------------------
// Default Conversation - Durem Blockade Guard             
//---------------------------------------------------------

playerSet401 = [] as Set

myManager.onEnter(myRooms.ZENGARDEN_401) { event ->
	if( isPlayer( event.actor ) ) {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z5NowTalk" ) && !event.actor.isOnQuest( 58 ) && !event.actor.isDoneQuest( 58 ) ) {
			myManager.schedule(2) {
				Z5Trip.say( "Halt!" )
				myManager.schedule(1) { Z5Trip.say( "Stand and be recognized!" ) }
				myManager.schedule(2) { event.actor.setQuestFlag(GLOBAL, "Z5NowTalk"); TripGuard.setUrgent( true ) }
			}
		}
		if( event.actor.getConLevel() >= 4.1 ) {
			event.actor.setQuestFlag( GLOBAL, "Z05tooOldForZenQuests" )
		} else {
			event.actor.unsetQuestFlag( GLOBAL, "Z05tooOldForZenQuests" )
		}
		playerSet401 << event.actor
	}
}

myManager.onExit( myRooms.ZENGARDEN_401 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet401.remove( event.actor ) 
	}
}

TripGuard = Z5Trip.createConversation("TripGuard", true, "!QuestStarted_58", "!QuestCompleted_58", "Z5NowTalk")

def Trip1 = [id:1]
Trip1.npctext = "I'm sorry, friend. I can't let you pass this blockade right now."
Trip1.playertext = "Why is that? How else can I get to Durem?"
Trip1.result = 2
TripGuard.addDialog(Trip1, Z5Trip)

def Trip2 = [id:2]
Trip2.npctext = "Well, to be honest, you can't right now. Durem is completely blocked off. There are some really bad things happening along the road past the Reclamation Facility and a number of people that have gone that direction haven't been seen since."
Trip2.playertext = "They disappeared?"
Trip2.result = 3
TripGuard.addDialog(Trip2, Z5Trip)

def Trip3 = [id:3]
Trip3.npctext = "Well...we're pretty sure they were *taken* by the Animated. For what...we don't know. But until the road can be cleared, we're closing it to any other travelers."
Trip3.playertext = "That makes sense. But, wow..."
Trip3.result = 4
TripGuard.addDialog(Trip3, Z5Trip)

def Trip4 = [id:4]
Trip4.npctext = "Yes, wow. We've got troubles here near Barton Town...but from what I've been told, it's nothing like the problems around Durem and Aekea."
Trip4.playertext = "You seem to know a lot for someone posted so far from Town."
Trip4.result = 5
TripGuard.addDialog(Trip4, Z5Trip)

def Trip5 = [id:5]
Trip5.npctext = "Leon keeps us up to speed...and a lot of people have been by here in the last couple of days. Since there's not a lot to do except listen to those dang walking drums, we ask a lot of questions. Did you want to know about anything else?"
Trip5.options = []
Trip5.options << [text:"What can you tell me about the Animated?", result: 6]
Trip5.options << [text:"Is there anything I can do to help you guards?", result: 8]
Trip5.options << [text:"No thanks. I think I'll just mosey along.", result: 9]
TripGuard.addDialog(Trip5, Z5Trip)

def Trip6 = [id:6]
Trip6.npctext = "Not a lot, frankly. We've seen a bunch of things coming to life, but oddly...it doesn't seem to be truly random. We see clusters of a certain kind of thing coming to life, and once they are Animated, they seem to somehow be able to breed more of themselves."
Trip6.playertext = "So you mean that not everything is coming to life?"
Trip6.result = 7
TripGuard.addDialog(Trip6, Z5Trip)

def Trip7 = [id:7]
Trip7.npctext = "Yeah...it's weird, but it's almost as if whatever is causing this is sort of random, but once it finds a type of thing to Animate, it tends to eddy there, creating more and more of that same Animated creature."
Trip7.playertext = "That's really strange."
Trip7.options = []
Trip7.options << [text:"Is there some way I can help you out?", result: 8]
Trip7.options << [text:"Thanks for the info. Good luck to you! I'm going to keep moving.", result: 9]
TripGuard.addDialog(Trip7, Z5Trip)

def Trip8 = [id:8]
Trip8.npctext = "Those Drums are driving us crazy around here. If you could thin a few of those out, we'd really appreciate it. We'd have to leave our post to do anything about them...and Leon would have our stripes if we did that."
Trip8.playertext = "Well...we need all the guards we can get right now."
Trip8.options = []
Trip8.options << [text:"Sure! I can help you out.", result: 10]
Trip8.options << [text:"I'd like to help, but I just can't right now. Sorry.", result: 9]
TripGuard.addDialog(Trip8, Z5Trip)

def Trip9 = [id:9]
Trip9.npctext = "All right then, friend. Good luck to you."
Trip9.flag = "!Z5NowTalk"
Trip9.result = DONE
TripGuard.addDialog(Trip9, Z5Trip)

def Trip10 = [id:10]
Trip10.npctext = "Great! If you could take out ten or so of those dratted Drums, we'd be forever in your debt. Thanks!"
Trip10.playertext = "You bet. I'll come back after I'm done."
Trip10.quest = 58 //push the start of the "The Drums! The Drums! I can't stand the DRUMS!" quest
Trip10.result = DONE
TripGuard.addDialog(Trip10, Z5Trip)

//---------------------------------------------------------
// Interim Speech (Drum Kill Quest)                        
//---------------------------------------------------------

def TripKillQuestInterim = Z5Trip.createConversation("TripKillQuestInterim", true, "QuestStarted_58:2", "!QuestStarted_58:3")

def Interim1 = [id:1]
Interim1.npctext = "The Drums! The Drums! I can't stand the DRUMS! Make them STOP!"
Interim1.result = DONE
TripKillQuestInterim.addDialog(Interim1, Z5Trip)

//---------------------------------------------------------
// Success Speech (Drum Kill Quest)                        
//---------------------------------------------------------

def TripKillQuestEnd = Z5Trip.createConversation("TripKillQuestEnd", true, "QuestStarted_58:3")
TripKillQuestEnd.setUrgent( true )

def Success1 = [id:1]
Success1.npctext = "Fantastic! My name is Trip, by the way. What's your name, friend? I want to mention you to Leon when he comes through."
Success1.playertext = "I'm %p. It was no problem at all."
Success1.result = 2
TripKillQuestEnd.addDialog(Success1, Z5Trip)

def Success2 = [id:2]
Success2.npctext = "I'm in your debt...I can take these ear plugs out finally!"
Success2.playertext = "Thanks!"
Success2.exec = { event ->
	event.player.updateQuest( 58, "BFG-Trip" ) //push the completion of "The Drums! The Drums! I can't stand the DRUMS!" quest
	event.player.removeMiniMapQuestActorName( "BFG-Trip" )
}
Success2.result = DONE
TripKillQuestEnd.addDialog(Success2, Z5Trip)

//---------------------------------------------------------
// Post-Drum Kill Quest (Repeatable Drum Kill Quest)       
//---------------------------------------------------------

def TripAfterQuest = Z5Trip.createConversation("TripAfterQuest", true, "!Z05tooOldForZenQuests", "QuestCompleted_58", "!QuestStarted_283")

def PostKill1 = [id:1]
PostKill1.npctext = "Hi there, %p! How are things?"
PostKill1.playertext = "Great, great. Are things all quiet out here on the western front?"
PostKill1.result = 2
TripAfterQuest.addDialog(PostKill1, Z5Trip)

def PostKill2 = [id:2]
PostKill2.npctext = "HA! Quiet? Here? You helped me out with those drums before, but sure enough, there's more of them now. Would you be interested in beating them down a bit more again?"
PostKill2.options = []
PostKill2.options << [text:"I just want to bang on de drum all day!", result: 4]
PostKill2.options << [text:"No thanks. My ears are still ringing from last time.", result: 3]
PostKill2.result = DONE
TripAfterQuest.addDialog(PostKill2, Z5Trip)

def PostKill3 = [id:3]
PostKill3.npctext = "I hear ya! Or rather, I *don't* hear you. Not anymore. Those dang drums. Okay, I'll see you next time then."
PostKill3.result = DONE
TripAfterQuest.addDialog(PostKill3, Z5Trip)

def PostKill4 = [id:4]
PostKill4.npctext = "Hehe. I'll take that as a 'yes'. Great. Go put the beat down on them for me!"
PostKill4.playertext = "Will do! See you soon, Trip."
PostKill4.quest = 283 //Begin the BEAT DOWN quest.
PostKill4.result = DONE
TripAfterQuest.addDialog( PostKill4, Z5Trip )

//========================================
// This conversation occurs if the player 
// is too old to receive repeatable tasks 
// in Zen Gardens anymore.                
//========================================
def tooOld = Z5Trip.createConversation( "tooOld", true, "Z05tooOldForZenQuests", "QuestCompleted_58" )

def old1 = [id:1]
old1.npctext = "...what?"
old1.playertext = "I didn't say anything."
old1.result = 2
tooOld.addDialog( old1, Z5Trip )

def old2 = [id:2]
old2.npctext = "Oh. I keep worrying that those drums are going to make me deaf. Sorry."
old2.playertext = "Heh. I'll bet. Is there anything you need done around here, Trip?"
old2.result = 3
tooOld.addDialog( old2, Z5Trip )

def old3 = [id:3]
old3.npctext = "Nah. The less-experienced folks around here can help me with the drums. Someone as veteran as you should head up north and help folks out around Bass'ken Lake."
old3.playertext = "Hmmm...I'll think about it. Meanwhile...consider ear plugs, my friend."
old3.result = 4
tooOld.addDialog( old3, Z5Trip )

def old4 = [id:4]
old4.npctext = "You think I'm not wearing them? I've got two sets crammed in my ear canals now!"
old4.playertext = "Oh. Well, good luck then!"
old4.exec = { event ->
	event.player.centerPrint( "You must be level 4.0 or lower to do Trip's repeatable task." )
	myManager.schedule(2) { event.player.centerPrint( "Use the CHANGE LEVEL option in the MENU to lower your level." ) }
	suppressSet << event.player
	checkOldPlayersForSuppression()
}
old4.result = DONE
tooOld.addDialog( old4, Z5Trip )


suppressSet = [] as Set
checkSet = [] as Set

def checkOldPlayersForSuppression() {
	if( !suppressSet.isEmpty() && !playerSet401.isEmpty() ) {
		suppressSet.each{ 
			checkSet << it
		}
		checkSet.each{			
			if( it.getConLevel() < 4.1 ) {
				it.unsetQuestFlag( GLOBAL, "Z05tooOldForZenQuests" )
				suppressSet.remove( it )
			}
		}
		if( playerSet401.isEmpty() ) { suppressSet.clear(); checkSet.clear() }
		myManager.schedule(2) { checkOldPlayersForSuppression() }
	}
}

//---------------------------------------------------------
// Repeatable Drum Kill Quest (Interim)                    
//---------------------------------------------------------

def BeatDownInterim = Z5Trip.createConversation("BeatDownInterim", true, "QuestStarted_283:2", "!QuestStarted_283:3")

def BeatInt1 = [id:1]
BeatInt1.npctext = "You can't be done! I still hear the drums!!!"
BeatInt1.playertext = "Wait a minute...I don't hear anything at all! What drums?!?"
BeatInt1.result = 2
BeatDownInterim.addDialog( BeatInt1, Z5Trip )

def BeatInt2 = [id:2]
BeatInt2.npctext = "Wha...? No! NO! I still hear them even when they're not there!!! I've gone MAD!"
BeatInt2.playertext = "Nah. I'm just messing with you. I haven't finished the job yet."
BeatInt2.result = 3
BeatDownInterim.addDialog( BeatInt2, Z5Trip )

def BeatInt3 = [id:3]
BeatInt3.npctext = "...oh. good. i was afraid that..."
BeatInt3.playertext = "Nope. You're not any more of a nutcase than the rest of us around here. Don't worry...I'll go trample the timpani again soon."
BeatInt3.result = 4
BeatDownInterim.addDialog( BeatInt3, Z5Trip )

def BeatInt4 = [id:4]
BeatInt4.npctext = "...thanks..."
BeatInt4.result = DONE
BeatDownInterim.addDialog( BeatInt4, Z5Trip )

//---------------------------------------------------------
// Repeatable Drum Kill Quest (Success)                    
//---------------------------------------------------------

def BeatDownSuccess = Z5Trip.createConversation("BeatDownSuccess", true, "QuestStarted_283:3")
BeatDownSuccess.setUrgent( true )

def BeatSucc1 = [id:1]
BeatSucc1.playertext = "Hear that?"
BeatSucc1.result = 2
BeatDownSuccess.addDialog( BeatSucc1, Z5Trip )

def BeatSucc2 = [id:2]
BeatSucc2.npctext = "Hear what?"
BeatSucc2.playertext = "That's my point! The drums are stilled again for a few moments!"
BeatSucc2.result = 3
BeatDownSuccess.addDialog( BeatSucc2, Z5Trip )

def BeatSucc3 = [id:3]
BeatSucc3.npctext = "Ahhhhhhh! Blessed silence..."
BeatSucc3.result = 4
BeatDownSuccess.addDialog( BeatSucc3, Z5Trip )

def BeatSucc4 = [id:4]
BeatSucc4.npctext = "...(hums quietly and happily)..."
BeatSucc4.result = 5
BeatDownSuccess.addDialog( BeatSucc4, Z5Trip )

def BeatSucc5 = [id:5]
BeatSucc5.npctext = "...okay. I know it won't last, but that was worth a lot to me. Let me give you something for your trouble."
BeatSucc5.playertext = "Thanks, Trip. Glad to help!"
BeatSucc5.exec = { event ->
	event.player.updateQuest( 283, "BFG-Trip" ) //Complete the BEAT DOWN quest.
	event.player.removeMiniMapQuestActorName( "BFG-Trip" )
}
BeatSucc5.result = DONE
BeatDownSuccess.addDialog( BeatSucc5, Z5Trip )
