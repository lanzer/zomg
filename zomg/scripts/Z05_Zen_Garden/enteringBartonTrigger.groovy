import com.gaiaonline.mmo.battle.script.*;

//===============================
// ENTERING BARTON FROM GARDENS  
// (Avoiding the Total Charge    
// check when coming from this   
// side. )                       
//===============================

def enteringBartonTownFromZenGardens = "enteringBartonTownFromZenGardens"
myRooms.ZENGARDEN_305.createTriggerZone( enteringBartonTownFromZenGardens, 765, 265, 955, 660 )

myManager.onTriggerIn( myRooms.ZENGARDEN_305, enteringBartonTownFromZenGardens ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z01ComingFromZenGardens" )
	}
}