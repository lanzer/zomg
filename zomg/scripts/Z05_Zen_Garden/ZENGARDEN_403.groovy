import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_403.spawnSpawner("spawner1", "cherry_fluff", 3) 
spawner1.setPos(300, 610)
spawner1.setWanderBehaviorForChildren( 25, 75, 2, 5, 250)
spawner1.setWaitTime( 30, 50 )
spawner1.setMonsterLevelForChildren( 3.4 )

//STARTUP LOGIC
spawner1.spawnAllNow()
