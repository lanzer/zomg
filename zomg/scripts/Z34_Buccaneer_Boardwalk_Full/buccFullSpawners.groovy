//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//Sets, Maps, Variables, Constants
monsterCounter = 0
landsharkSpawner = null
landsharkBoss = null
landsharkSpawned = false
landsharkDisposed = false

buccFullSpawnerList = [] as Set
landsharkSpawnerList = [] as Set

//Spawners
def static201_1 = myRooms.BuccFull_201.spawnSpawner("static201_1", "sand_golem_boardwalk", 1)
static201_1.setPos(620, 490)
static201_1.setGuardPostForChildren("BuccFull_201", 620, 490, 45)
static201_1.setHome("BuccFull_201", 620, 490)
static201_1.setSpawnWhenPlayersAreInRoom(true)
static201_1.setWaitTime(120 , 180)
static201_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static201_1

def static201_2 = myRooms.BuccFull_201.spawnSpawner("static201_2", "sand_golem_boardwalk", 1)
static201_2.setPos(900, 450)
static201_2.setGuardPostForChildren("BuccFull_201", 900, 450, 45)
static201_2.setHome("BuccFull_201", 900, 450)
static201_2.setSpawnWhenPlayersAreInRoom(true)
static201_2.setWaitTime(120 , 180)
static201_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static201_2

def static201_3 = myRooms.BuccFull_201.spawnSpawner("static201_3", "sand_golem_boardwalk", 1)
static201_3.setPos(660, 160)
static201_3.setGuardPostForChildren("BuccFull_201", 660, 160, 45)
static201_3.setHome("BuccFull_201", 660, 160)
static201_3.setSpawnWhenPlayersAreInRoom(true)
static201_3.setWaitTime(120 , 180)
static201_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static201_3

def static202_1 = myRooms.BuccFull_202.spawnSpawner("static202_1", "sand_golem_boardwalk", 1)
static202_1.setPos(730, 240)
static202_1.setGuardPostForChildren("BuccFull_202", 730, 240, 45)
static202_1.setHome("BuccFull_202", 730, 240)
static202_1.setSpawnWhenPlayersAreInRoom(true)
static202_1.setWaitTime(120 , 180)
static202_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static202_1

def static202_2 = myRooms.BuccFull_202.spawnSpawner("static202_2", "sand_golem_boardwalk", 1)
static202_2.setPos(520, 420)
static202_2.setGuardPostForChildren("BuccFull_202", 520, 420, 45)
static202_2.setHome("BuccFull_202", 520, 420)
static202_2.setSpawnWhenPlayersAreInRoom(true)
static202_2.setWaitTime(120 , 180)
static202_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static202_2

def static202_3 = myRooms.BuccFull_202.spawnSpawner("static202_3", "sand_golem_boardwalk", 1)
static202_3.setPos(220, 480)
static202_3.setGuardPostForChildren("BuccFull_202", 220, 480, 45)
static202_3.setHome("BuccFull_202", 220, 480)
static202_3.setSpawnWhenPlayersAreInRoom(true)
static202_3.setWaitTime(120 , 180)
static202_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static202_3

def static202_4 = myRooms.BuccFull_202.spawnSpawner("static202_4", "anchor_bug_boardwalk", 1)
static202_4.setPos(970, 550)
static202_4.setGuardPostForChildren("BuccFull_202", 970, 550, 45)
static202_4.setHome("BuccFull_202", 970, 550)
static202_4.setSpawnWhenPlayersAreInRoom(true)
static202_4.setWaitTime(120 , 180)
static202_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static202_4

def static202_5 = myRooms.BuccFull_202.spawnSpawner("static202_5", "anchor_bug_boardwalk", 1)
static202_5.setPos(730, 580)
static202_5.setGuardPostForChildren("BuccFull_202", 730, 580, 45)
static202_5.setHome("BuccFull_202", 730, 580)
static202_5.setSpawnWhenPlayersAreInRoom(true)
static202_5.setWaitTime(120 , 180)
static202_5.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static202_5

def static203_1 = myRooms.BuccFull_203.spawnSpawner("static203_1", "sand_golem_boardwalk", 1)
static203_1.setPos(710, 370)
static203_1.setGuardPostForChildren("BuccFull_203", 710, 370, 45)
static203_1.setHome("BuccFull_203", 710, 370)
static203_1.setSpawnWhenPlayersAreInRoom(true)
static203_1.setWaitTime(120 , 180)
static203_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static203_1

def static203_2 = myRooms.BuccFull_203.spawnSpawner("static203_2", "sand_golem_boardwalk", 1)
static203_2.setPos(710, 510)
static203_2.setGuardPostForChildren("BuccFull_203", 710, 510, 45)
static203_2.setHome("BuccFull_203", 710, 510)
static203_2.setSpawnWhenPlayersAreInRoom(true)
static203_2.setWaitTime(120 , 180)
static203_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static203_2

def static203_3 = myRooms.BuccFull_203.spawnSpawner("static203_3", "sand_golem_boardwalk", 1)
static203_3.setPos(300, 210)
static203_3.setGuardPostForChildren("BuccFull_203", 300, 210, 45)
static203_3.setHome("BuccFull_203", 300, 210)
static203_3.setSpawnWhenPlayersAreInRoom(true)
static203_3.setWaitTime(120 , 180)
static203_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static203_3

def static203_4 = myRooms.BuccFull_203.spawnSpawner("static203_4", "sand_golem_boardwalk", 1)
static203_4.setPos(90, 180)
static203_4.setGuardPostForChildren("BuccFull_203", 90, 180, 45)
static203_4.setHome("BuccFull_203", 90, 180)
static203_4.setSpawnWhenPlayersAreInRoom(true)
static203_4.setWaitTime(120 , 180)
static203_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static203_4

def static203_5 = myRooms.BuccFull_203.spawnSpawner("static203_5", "anchor_bug_boardwalk", 1)
static203_5.setPos(140, 520)
static203_5.setGuardPostForChildren("BuccFull_203", 140, 520, 45)
static203_5.setHome("BuccFull_203", 140, 520)
static203_5.setSpawnWhenPlayersAreInRoom(true)
static203_5.setWaitTime(120 , 180)
static203_5.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static203_5

def static203_6 = myRooms.BuccFull_203.spawnSpawner("static203_6", "anchor_bug_boardwalk", 1)
static203_6.setPos(420, 490)
static203_6.setGuardPostForChildren("BuccFull_203", 420, 490, 45)
static203_6.setHome("BuccFull_203", 420, 490)
static203_6.setSpawnWhenPlayersAreInRoom(true)
static203_6.setWaitTime(120 , 180)
static203_6.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static203_6

/*def static204_1 = myRooms.BuccFull_204.spawnSpawner("static204_1", "ufo", 1)
static204_1.setPos(510, 318)
static204_1.setGuardPostForChildren("BuccFull_204", 510, 318, 45)
static204_1.setHome("BuccFull_204", 510, 318)
static204_1.setSpawnWhenPlayersAreInRoom(true)
static204_1.setWaitTime(120 , 180)
static204_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList static204_1*/

def static204_2 = myRooms.BuccFull_204.spawnSpawner("static204_2", "sand_golem_boardwalk", 1)
static204_2.setPos(820, 170)
static204_2.setGuardPostForChildren("BuccFull_204", 820, 170, 45)
static204_2.setHome("BuccFull_204", 820, 170)
static204_2.setSpawnWhenPlayersAreInRoom(true)
static204_2.setWaitTime(120 , 180)
static204_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static204_2

def static204_3 = myRooms.BuccFull_204.spawnSpawner("static204_3", "sand_golem_boardwalk", 1)
static204_3.setPos(820, 260)
static204_3.setGuardPostForChildren("BuccFull_204", 820, 260, 45)
static204_3.setHome("BuccFull_204", 820, 260)
static204_3.setSpawnWhenPlayersAreInRoom(true)
static204_3.setWaitTime(120 , 180)
static204_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static204_3

def static204_4 = myRooms.BuccFull_204.spawnSpawner("static204_4", "sand_golem_boardwalk", 1)
static204_4.setPos(160, 170)
static204_4.setGuardPostForChildren("BuccFull_204", 160, 170, 45)
static204_4.setHome("BuccFull_204", 160, 170)
static204_4.setSpawnWhenPlayersAreInRoom(true)
static204_4.setWaitTime(120 , 180)
static204_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static204_4

def static204_5 = myRooms.BuccFull_204.spawnSpawner("static204_5", "sand_golem_boardwalk", 1)
static204_5.setPos(160, 260)
static204_5.setGuardPostForChildren("BuccFull_204", 160, 260, 45)
static204_5.setHome("BuccFull_204", 160, 260)
static204_5.setSpawnWhenPlayersAreInRoom(true)
static204_5.setWaitTime(120 , 180)
static204_5.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static204_5

def static205_1 = myRooms.BuccFull_205.spawnSpawner("static205_1", "sand_golem_boardwalk", 1)
static205_1.setPos(560, 590)
static205_1.setGuardPostForChildren("BuccFull_205", 560, 590, 45)
static205_1.setHome("BuccFull_205", 560, 620)
static205_1.setSpawnWhenPlayersAreInRoom(true)
static205_1.setWaitTime(120 , 180)
static205_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static205_1

def static205_2 = myRooms.BuccFull_205.spawnSpawner("static205_2", "sand_golem_boardwalk", 1)
static205_2.setPos(750, 600)
static205_2.setGuardPostForChildren("BuccFull_205", 750, 570, 45)
static205_2.setHome("BuccFull_205", 750, 570)
static205_2.setSpawnWhenPlayersAreInRoom(true)
static205_2.setWaitTime(120 , 180)
static205_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static205_2

def static205_3 = myRooms.BuccFull_205.spawnSpawner("static205_3", "sand_golem_boardwalk", 1)
static205_3.setPos(940, 450)
static205_3.setGuardPostForChildren("BuccFull_205", 940, 450, 45)
static205_3.setHome("BuccFull_205", 940, 450)
static205_3.setSpawnWhenPlayersAreInRoom(true)
static205_3.setWaitTime(120 , 180)
static205_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static205_3

def static206_1 = myRooms.BuccFull_206.spawnSpawner("static206_1", "sand_golem_boardwalk", 1)
static206_1.setPos(130, 570)
static206_1.setGuardPostForChildren("BuccFull_206", 130, 570, 45)
static206_1.setHome("BuccFull_206", 130, 570)
static206_1.setSpawnWhenPlayersAreInRoom(true)
static206_1.setWaitTime(120 , 180)
static206_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static206_1

def static206_2 = myRooms.BuccFull_206.spawnSpawner("static206_2", "sand_golem_boardwalk", 1)
static206_2.setPos(330, 620)
static206_2.setGuardPostForChildren("BuccFull_206", 330, 620, 45)
static206_2.setHome("BuccFull_206", 330, 620)
static206_2.setSpawnWhenPlayersAreInRoom(true)
static206_2.setWaitTime(120 , 180)
static206_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static206_2

def static206_3 = myRooms.BuccFull_206.spawnSpawner("static206_3", "sand_golem_boardwalk", 1)
static206_3.setPos(560, 320)
static206_3.setGuardPostForChildren("BuccFull_206", 560, 320, 45)
static206_3.setHome("BuccFull_206", 560, 320)
static206_3.setSpawnWhenPlayersAreInRoom(true)
static206_3.setWaitTime(120 , 180)
static206_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static206_3

def static206_4 = myRooms.BuccFull_206.spawnSpawner("static206_4", "sand_golem_boardwalk", 1)
static206_4.setPos(800, 200)
static206_4.setGuardPostForChildren("BuccFull_206", 800, 200, 45)
static206_4.setHome("BuccFull_206", 800, 200)
static206_4.setSpawnWhenPlayersAreInRoom(true)
static206_4.setWaitTime(120 , 180)
static206_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static206_4

def static301_1 = myRooms.BuccFull_301.spawnSpawner("static301_1", "anchor_bug_boardwalk", 1)
static301_1.setPos(660, 70)
static301_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static301_1.childrenWander( true )
static301_1.setHome("BuccFull_301", 660, 70)
static301_1.setSpawnWhenPlayersAreInRoom(true)
static301_1.setWaitTime(120 , 180)
static301_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static301_1

def static301_2 = myRooms.BuccFull_301.spawnSpawner("static301_2", "anchor_bug_boardwalk", 1)
static301_2.setPos(740, 200)
static301_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static301_2.childrenWander( true )
static301_2.setHome("BuccFull_301", 740, 200)
static301_2.setSpawnWhenPlayersAreInRoom(true)
static301_2.setWaitTime(120 , 180)
static301_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static301_2

def static301_3 = myRooms.BuccFull_301.spawnSpawner("static301_3", "anchor_bug_boardwalk", 1)
static301_3.setPos(930, 290)
static301_3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static301_3.childrenWander( true )
static301_3.setHome("BuccFull_301", 930, 290)
static301_3.setSpawnWhenPlayersAreInRoom(true)
static301_3.setWaitTime(120 , 180)
static301_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static301_3

def static302_1 = myRooms.BuccFull_302.spawnSpawner("static302_1", "anchor_bug_boardwalk", 1)
static302_1.setPos(240, 420)
static302_1.setGuardPostForChildren("BuccFull_302", 240, 420, 45)
static302_1.setHome("BuccFull_302", 240, 420)
static302_1.setSpawnWhenPlayersAreInRoom(true)
static302_1.setWaitTime(120 , 180)
static302_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static302_1

def static302_2 = myRooms.BuccFull_302.spawnSpawner("static302_2", "anchor_bug_boardwalk", 1)
static302_2.setPos(470, 530)
static302_2.setGuardPostForChildren("BuccFull_302", 470, 530, 45)
static302_2.setHome("BuccFull_302", 470, 530)
static302_2.setSpawnWhenPlayersAreInRoom(true)
static302_2.setWaitTime(120 , 180)
static302_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static302_2

def static302_3 = myRooms.BuccFull_302.spawnSpawner("static302_3", "anchor_bug_boardwalk", 1)
static302_3.setPos(670, 530)
static302_3.setGuardPostForChildren("BuccFull_302", 670, 530, 45)
static302_3.setHome("BuccFull_302", 670, 530)
static302_3.setSpawnWhenPlayersAreInRoom(true)
static302_3.setWaitTime(120 , 180)
static302_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static302_3

def static302_4 = myRooms.BuccFull_302.spawnSpawner("static302_4", "sand_golem_boardwalk", 1)
static302_4.setPos(550, 160)
static302_4.setGuardPostForChildren("BuccFull_302", 550, 160, 45)
static302_4.setHome("BuccFull_302", 550, 160)
static302_4.setSpawnWhenPlayersAreInRoom(true)
static302_4.setWaitTime(120 , 180)
static302_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static302_4

def static302_5 = myRooms.BuccFull_302.spawnSpawner("static302_5", "anchor_bug_boardwalk", 1)
static302_5.setPos(740, 250)
static302_5.setGuardPostForChildren("BuccFull_302", 740, 250, 45)
static302_5.setHome("BuccFull_302", 740, 250)
static302_5.setSpawnWhenPlayersAreInRoom(true)
static302_5.setWaitTime(120 , 180)
static302_5.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static302_5

def static303_1 = myRooms.BuccFull_303.spawnSpawner("static303_1", "anchor_bug_boardwalk", 1)
static303_1.setPos(260, 620)
static303_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static303_1.childrenWander( true )
static303_1.setHome("BuccFull_303", 260, 620)
static303_1.setSpawnWhenPlayersAreInRoom(true)
static303_1.setWaitTime(120 , 180)
static303_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static303_1

def static303_2 = myRooms.BuccFull_303.spawnSpawner("static303_2", "anchor_bug_boardwalk", 1)
static303_2.setPos(410, 580)
static303_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static303_2.childrenWander( true )
static303_2.setHome("BuccFull_303", 410, 580)
static303_2.setSpawnWhenPlayersAreInRoom(true)
static303_2.setWaitTime(120 , 180)
static303_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static303_2

def static303_3 = myRooms.BuccFull_303.spawnSpawner("static303_3", "anchor_bug_boardwalk", 1)
static303_3.setPos(360, 350)
static303_3.setGuardPostForChildren("BuccFull_303", 360, 350, 45)
static303_3.setHome("BuccFull_303", 360, 350)
static303_3.setSpawnWhenPlayersAreInRoom(true)
static303_3.setWaitTime(120 , 180)
static303_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static303_3

def static303_4 = myRooms.BuccFull_303.spawnSpawner("static303_4", "anchor_bug_boardwalk", 1)
static303_4.setPos(500, 280)
static303_4.setGuardPostForChildren("BuccFull_303", 500, 280, 45)
static303_4.setHome("BuccFull_303", 500, 280)
static303_4.setSpawnWhenPlayersAreInRoom(true)
static303_4.setWaitTime(120 , 180)
static303_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static303_4

def static303_5 = myRooms.BuccFull_303.spawnSpawner("static303_5", "sand_golem_boardwalk", 1)
static303_5.setPos(370, 140)
static303_5.setGuardPostForChildren("BuccFull_303", 370, 140, 45)
static303_5.setHome("BuccFull_303", 370, 140)
static303_5.setSpawnWhenPlayersAreInRoom(true)
static303_5.setWaitTime(120 , 180)
static303_5.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static303_5

def static303_6 = myRooms.BuccFull_303.spawnSpawner("static303_6", "anchor_bug_boardwalk", 1)
static303_6.setPos(910, 400)
static303_6.setGuardPostForChildren("BuccFull_303", 910, 400, 45)
static303_6.setHome("BuccFull_303", 910, 400)
static303_6.setSpawnWhenPlayersAreInRoom(true)
static303_6.setWaitTime(120 , 180)
static303_6.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static303_6

def static402_1 = myRooms.BuccFull_402.spawnSpawner("static402_1", "anchor_bug_boardwalk", 1)
static402_1.setPos(650, 60)
static402_1.setGuardPostForChildren("BuccFull_402", 650, 60, 45)
static402_1.setHome("BuccFull_402", 650, 60)
static402_1.setSpawnWhenPlayersAreInRoom(true)
static402_1.setWaitTime(120 , 180)
static402_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static402_1

def static305_1 = myRooms.BuccFull_305.spawnSpawner("static305_1", "anchor_bug_boardwalk", 1)
static305_1.setPos(830, 310)
static305_1.setGuardPostForChildren("BuccFull_305", 830, 310, 45)
static305_1.setHome("BuccFull_305", 830, 310)
static305_1.setSpawnWhenPlayersAreInRoom(true)
static305_1.setWaitTime(120 , 180)
static305_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static305_1

def static305_2 = myRooms.BuccFull_305.spawnSpawner("static305_2", "anchor_bug_boardwalk", 1)
static305_2.setPos(580, 500)
static305_2.setGuardPostForChildren("BuccFull_305", 580, 500, 45)
static305_2.setHome("BuccFull_305", 580, 500)
static305_2.setSpawnWhenPlayersAreInRoom(true)
static305_2.setWaitTime(120 , 180)
static305_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static305_2

def static305_3 = myRooms.BuccFull_305.spawnSpawner("static305_3", "anchor_bug_boardwalk", 1)
static305_3.setPos(420, 320)
static305_3.setGuardPostForChildren("BuccFull_305", 420, 320, 45)
static305_3.setHome("BuccFull_305", 420, 320)
static305_3.setSpawnWhenPlayersAreInRoom(true)
static305_3.setWaitTime(120 , 180)
static305_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static305_3

def static305_4 = myRooms.BuccFull_305.spawnSpawner("static305_4", "anchor_bug_boardwalk", 1)
static305_4.setPos(130, 380)
static305_4.setGuardPostForChildren("BuccFull_305", 130, 380, 45)
static305_4.setHome("BuccFull_305", 130, 380)
static305_4.setSpawnWhenPlayersAreInRoom(true)
static305_4.setWaitTime(120 , 180)
static305_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static305_4

def static306_1 = myRooms.BuccFull_306.spawnSpawner("static306_1", "anchor_bug_boardwalk", 1)
static306_1.setPos(720, 580)
static306_1.setGuardPostForChildren("BuccFull_306", 720, 580, 45)
static306_1.setHome("BuccFull_306", 720, 580)
static306_1.setSpawnWhenPlayersAreInRoom(true)
static306_1.setWaitTime(120 , 180)
static306_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static306_1

def static306_2 = myRooms.BuccFull_306.spawnSpawner("static306_2", "anchor_bug_boardwalk", 1)
static306_2.setPos(430, 500)
static306_2.setGuardPostForChildren("BuccFull_306", 430, 500, 45)
static306_2.setHome("BuccFull_306", 430, 500)
static306_2.setSpawnWhenPlayersAreInRoom(true)
static306_2.setWaitTime(120 , 180)
static306_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static306_2

def static306_3 = myRooms.BuccFull_306.spawnSpawner("static306_3", "sand_golem_boardwalk", 1)
static306_3.setPos(260, 200)
static306_3.setGuardPostForChildren("BuccFull_306", 260, 200, 45)
static306_3.setHome("BuccFull_306", 260, 200)
static306_3.setSpawnWhenPlayersAreInRoom(true)
static306_3.setWaitTime(120 , 180)
static306_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static306_3

def static402_2 = myRooms.BuccFull_402.spawnSpawner("static402_2", "anchor_bug_boardwalk", 1)
static402_2.setPos(400, 200)
static402_2.setGuardPostForChildren("BuccFull_402", 400, 200, 45)
static402_2.setHome("BuccFull_402", 400, 200)
static402_2.setSpawnWhenPlayersAreInRoom(true)
static402_2.setWaitTime(120 , 180)
static402_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static402_2

def static402_3 = myRooms.BuccFull_402.spawnSpawner("static402_3", "anchor_bug_boardwalk", 1)
static402_3.setPos(400, 490)
static402_3.setGuardPostForChildren("BuccFull_402", 400, 490, 45)
static402_3.setHome("BuccFull_402", 400, 490)
static402_3.setSpawnWhenPlayersAreInRoom(true)
static402_3.setWaitTime(120 , 180)
static402_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static402_3

def static402_4 = myRooms.BuccFull_402.spawnSpawner("static402_4", "anchor_bug_boardwalk", 1)
static402_4.setPos(600, 620)
static402_4.setGuardPostForChildren("BuccFull_402", 600, 620, 45)
static402_4.setHome("BuccFull_402", 600, 620)
static402_4.setSpawnWhenPlayersAreInRoom(true)
static402_4.setWaitTime(120 , 180)
static402_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static402_4

def static402_5 = myRooms.BuccFull_402.spawnSpawner("static402_5", "anchor_bug_boardwalk", 1)
static402_5.setPos(880, 540)
static402_5.setGuardPostForChildren("BuccFull_402", 880, 540, 45)
static402_5.setHome("BuccFull_402", 880, 540)
static402_5.setSpawnWhenPlayersAreInRoom(true)
static402_5.setWaitTime(120 , 180)
static402_5.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static402_5

def static403_1 = myRooms.BuccFull_403.spawnSpawner("static403_1", "anchor_bug_boardwalk", 1)
static403_1.setPos(160, 200)
static403_1.setGuardPostForChildren("BuccFull_403", 160, 200, 45)
static403_1.setHome("BuccFull_403", 160, 200)
static403_1.setSpawnWhenPlayersAreInRoom(true)
static403_1.setWaitTime(120 , 180)
static403_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static403_1

def static403_2 = myRooms.BuccFull_403.spawnSpawner("static403_2", "anchor_bug_boardwalk", 1)
static403_2.setPos(330, 200)
static403_2.setGuardPostForChildren("BuccFull_403", 330, 200, 45)
static403_2.setHome("BuccFull_403", 330, 200)
static403_2.setSpawnWhenPlayersAreInRoom(true)
static403_2.setWaitTime(120 , 180)
static403_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static403_2

def static403_3 = myRooms.BuccFull_403.spawnSpawner("static403_3", "anchor_bug_boardwalk", 1)
static403_3.setPos(160, 530)
static403_3.setGuardPostForChildren("BuccFull_403", 160, 530, 45)
static403_3.setHome("BuccFull_403", 160, 530)
static403_3.setSpawnWhenPlayersAreInRoom(true)
static403_3.setWaitTime(120 , 180)
static403_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static403_3

def static403_4 = myRooms.BuccFull_403.spawnSpawner("static403_4", "anchor_bug_boardwalk", 1)
static403_4.setPos(330, 530)
static403_4.setGuardPostForChildren("BuccFull_403", 330, 530, 45)
static403_4.setHome("BuccFull_403", 330, 530)
static403_4.setSpawnWhenPlayersAreInRoom(true)
static403_4.setWaitTime(120 , 180)
static403_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static403_4

def static405_1 = myRooms.BuccFull_405.spawnSpawner("static405_1", "anchor_bug_boardwalk", 1)
static405_1.setPos(910, 540)
static405_1.setGuardPostForChildren("BuccFull_405", 910, 540, 45)
static405_1.setHome("BuccFull_405", 910, 540)
static405_1.setSpawnWhenPlayersAreInRoom(true)
static405_1.setWaitTime(120 , 180)
static405_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static405_1

def static405_2 = myRooms.BuccFull_405.spawnSpawner("static405_2", "anchor_bug_boardwalk", 1)
static405_2.setPos(800, 380)
static405_2.setGuardPostForChildren("BuccFull_405", 800, 380, 45)
static405_2.setHome("BuccFull_405", 800, 380)
static405_2.setSpawnWhenPlayersAreInRoom(true)
static405_2.setWaitTime(120 , 180)
static405_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static405_2

def static405_3 = myRooms.BuccFull_405.spawnSpawner("static405_3", "anchor_bug_boardwalk", 1)
static405_3.setPos(930, 180)
static405_3.setGuardPostForChildren("BuccFull_405", 930, 180, 45)
static405_3.setHome("BuccFull_405", 930, 180)
static405_3.setSpawnWhenPlayersAreInRoom(true)
static405_3.setWaitTime(120 , 180)
static405_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static405_3

def static406_1 = myRooms.BuccFull_406.spawnSpawner("static406_1", "anchor_bug_boardwalk", 1)
static406_1.setPos(200, 570)
static406_1.setGuardPostForChildren("BuccFull_406", 200, 570, 45)
static406_1.setHome("BuccFull_406", 200, 570)
static406_1.setSpawnWhenPlayersAreInRoom(true)
static406_1.setWaitTime(120 , 180)
static406_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static406_1

def static406_2 = myRooms.BuccFull_406.spawnSpawner("static406_2", "anchor_bug_boardwalk", 1)
static406_2.setPos(420, 460)
static406_2.setGuardPostForChildren("BuccFull_406", 420, 460, 45)
static406_2.setHome("BuccFull_406", 420, 460)
static406_2.setSpawnWhenPlayersAreInRoom(true)
static406_2.setWaitTime(120 , 180)
static406_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static406_2

def static407_1 = myRooms.BuccFull_407.spawnSpawner("static407_1", "anchor_bug_boardwalk", 1)
static407_1.setPos(260, 430)
static407_1.setGuardPostForChildren("BuccFull_407", 260, 430, 45)
static407_1.setHome("BuccFull_407", 260, 430)
static407_1.setSpawnWhenPlayersAreInRoom(true)
static407_1.setWaitTime(120 , 180)
static407_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static407_1

def static407_2 = myRooms.BuccFull_407.spawnSpawner("static407_2", "anchor_bug_boardwalk", 1)
static407_2.setPos(310, 360)
static407_2.setGuardPostForChildren("BuccFull_407", 310, 360, 45)
static407_2.setHome("BuccFull_407", 310, 360)
static407_2.setSpawnWhenPlayersAreInRoom(true)
static407_2.setWaitTime(120 , 180)
static407_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static407_2

def static406_3 = myRooms.BuccFull_406.spawnSpawner("static406_3", "anchor_bug_boardwalk", 1)
static406_3.setPos(550, 370)
static406_3.setGuardPostForChildren("BuccFull_406", 550, 370, 45)
static406_3.setHome("BuccFull_406", 550, 370)
static406_3.setSpawnWhenPlayersAreInRoom(true)
static406_3.setWaitTime(120 , 180)
static406_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static406_3

def static501_1 = myRooms.BuccFull_501.spawnSpawner("static501_1", "anchor_bug_boardwalk", 1)
static501_1.setPos(230, 520)
static501_1.setGuardPostForChildren("BuccFull_501", 230, 520, 45)
static501_1.setHome("BuccFull_501", 230, 520)
static501_1.setSpawnWhenPlayersAreInRoom(true)
static501_1.setWaitTime(120 , 180)
static501_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static501_1

def static501_2 = myRooms.BuccFull_501.spawnSpawner("static501_2", "sand_golem_boardwalk", 1)
static501_2.setPos(160, 240)
static501_2.setGuardPostForChildren("BuccFull_501", 160, 240, 45)
static501_2.setHome("BuccFull_501", 160, 240)
static501_2.setSpawnWhenPlayersAreInRoom(true)
static501_2.setWaitTime(120 , 180)
static501_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static501_2

def static501_3 = myRooms.BuccFull_501.spawnSpawner("static501_3", "sand_golem_boardwalk", 1)
static501_3.setPos(370, 300)
static501_3.setGuardPostForChildren("BuccFull_501", 370, 300, 45)
static501_3.setHome("BuccFull_501", 370, 300)
static501_3.setSpawnWhenPlayersAreInRoom(true)
static501_3.setWaitTime(120 , 180)
static501_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static501_3

def static501_4 = myRooms.BuccFull_501.spawnSpawner("static501_4", "anchor_bug_boardwalk", 1)
static501_4.setPos(720, 180)
static501_4.setGuardPostForChildren("BuccFull_501", 720, 180, 45)
static501_4.setHome("BuccFull_501", 720, 180)
static501_4.setSpawnWhenPlayersAreInRoom(true)
static501_4.setWaitTime(120 , 180)
static501_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static501_4

def static501_5 = myRooms.BuccFull_501.spawnSpawner("static501_5", "anchor_bug_boardwalk", 1)
static501_5.setPos(930, 330)
static501_5.setGuardPostForChildren("BuccFull_501", 930, 330, 45)
static501_5.setHome("BuccFull_501", 930, 330)
static501_5.setSpawnWhenPlayersAreInRoom(true)
static501_5.setWaitTime(120 , 180)
static501_5.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static501_5

def static502_1 = myRooms.BuccFull_502.spawnSpawner("static502_1", "anchor_bug_boardwalk", 1)
static502_1.setPos(600, 180)
static502_1.setGuardPostForChildren("BuccFull_502", 600, 180, 45)
static502_1.setHome("BuccFull_502", 600, 180)
static502_1.setSpawnWhenPlayersAreInRoom(true)
static502_1.setWaitTime(120 , 180)
static502_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static502_1

def static502_2 = myRooms.BuccFull_502.spawnSpawner("static502_2", "anchor_bug_boardwalk", 1)
static502_2.setPos(600, 270)
static502_2.setGuardPostForChildren("BuccFull_502", 600, 270, 45)
static502_2.setHome("BuccFull_502", 600, 270)
static502_2.setSpawnWhenPlayersAreInRoom(true)
static502_2.setWaitTime(120 , 180)
static502_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static502_2

def static502_3 = myRooms.BuccFull_502.spawnSpawner("static502_3", "anchor_bug_boardwalk", 1)
static502_3.setPos(240, 310)
static502_3.setGuardPostForChildren("BuccFull_502", 240, 310, 45)
static502_3.setHome("BuccFull_502", 240, 310)
static502_3.setSpawnWhenPlayersAreInRoom(true)
static502_3.setWaitTime(120 , 180)
static502_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static502_3

def static502_4 = myRooms.BuccFull_502.spawnSpawner("static502_4", "anchor_bug_boardwalk", 1)
static502_4.setPos(940, 250)
static502_4.setGuardPostForChildren("BuccFull_502", 940, 250, 45)
static502_4.setHome("BuccFull_502", 940, 250)
static502_4.setSpawnWhenPlayersAreInRoom(true)
static502_4.setWaitTime(120 , 180)
static502_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static502_4

def static502_5 = myRooms.BuccFull_502.spawnSpawner("static502_5", "anchor_bug_boardwalk", 1)
static502_5.setPos(400, 510)
static502_5.setGuardPostForChildren("BuccFull_502", 400, 510, 45)
static502_5.setHome("BuccFull_502", 400, 510)
static502_5.setSpawnWhenPlayersAreInRoom(true)
static502_5.setWaitTime(120 , 180)
static502_5.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static502_5

def static503_1 = myRooms.BuccFull_503.spawnSpawner("static503_1", "anchor_bug_boardwalk", 1)
static503_1.setPos(160, 250)
static503_1.setGuardPostForChildren("BuccFull_503", 160, 250, 45)
static503_1.setHome("BuccFull_503", 160, 250)
static503_1.setSpawnWhenPlayersAreInRoom(true)
static503_1.setWaitTime(120 , 180)
static503_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static503_1

def static503_2 = myRooms.BuccFull_503.spawnSpawner("static503_2", "anchor_bug_boardwalk", 1)
static503_2.setPos(400, 250)
static503_2.setGuardPostForChildren("BuccFull_503", 400, 250, 45)
static503_2.setHome("BuccFull_503", 400, 250)
static503_2.setSpawnWhenPlayersAreInRoom(true)
static503_2.setWaitTime(120 , 180)
static503_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static503_2

def static505_1 = myRooms.BuccFull_505.spawnSpawner("static505_1", "anchor_bug_boardwalk", 1)
static505_1.setPos(830, 150)
static505_1.setGuardPostForChildren("BuccFull_505", 830, 150, 45)
static505_1.setHome("BuccFull_505", 830, 150)
static505_1.setSpawnWhenPlayersAreInRoom(true)
static505_1.setWaitTime(120 , 180)
static505_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static505_1

def static505_2 = myRooms.BuccFull_505.spawnSpawner("static505_2", "anchor_bug_boardwalk", 1)
static505_2.setPos(900, 270)
static505_2.setGuardPostForChildren("BuccFull_505", 900, 270, 45)
static505_2.setHome("BuccFull_505", 900, 270)
static505_2.setSpawnWhenPlayersAreInRoom(true)
static505_2.setWaitTime(120 , 180)
static505_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static505_2

def static506_1 = myRooms.BuccFull_506.spawnSpawner("static506_1", "anchor_bug_boardwalk", 1)
static506_1.setPos(330, 330)
static506_1.setGuardPostForChildren("BuccFull_506", 330, 330, 45)
static506_1.setHome("BuccFull_506", 330, 330)
static506_1.setSpawnWhenPlayersAreInRoom(true)
static506_1.setWaitTime(120 , 180)
static506_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static506_1

def static506_2 = myRooms.BuccFull_506.spawnSpawner("static506_2", "anchor_bug_boardwalk", 1)
static506_2.setPos(540, 300)
static506_2.setGuardPostForChildren("BuccFull_506", 540, 300, 45)
static506_2.setHome("BuccFull_506", 540, 300)
static506_2.setSpawnWhenPlayersAreInRoom(true)
static506_2.setWaitTime(120 , 180)
static506_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static506_2

def static601_1 = myRooms.BuccFull_601.spawnSpawner("static601_1", "anchor_bug_boardwalk", 1)
static601_1.setPos(360, 560)
static601_1.setGuardPostForChildren("BuccFull_601", 360, 560, 45)
static601_1.setHome("BuccFull_601", 360, 560)
static601_1.setSpawnWhenPlayersAreInRoom(true)
static601_1.setWaitTime(120 , 180)
static601_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static601_1

def static601_2 = myRooms.BuccFull_601.spawnSpawner("static601_2", "anchor_bug_boardwalk", 1)
static601_2.setPos(210, 480)
static601_2.setGuardPostForChildren("BuccFull_601", 210, 480, 45)
static601_2.setHome("BuccFull_601", 210, 480)
static601_2.setSpawnWhenPlayersAreInRoom(true)
static601_2.setWaitTime(120 , 180)
static601_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static601_2

def static601_3 = myRooms.BuccFull_601.spawnSpawner("static601_3", "anchor_bug_boardwalk", 1)
static601_3.setPos(270, 270)
static601_3.setGuardPostForChildren("BuccFull_601", 270, 270, 45)
static601_3.setHome("BuccFull_601", 270, 270)
static601_3.setSpawnWhenPlayersAreInRoom(true)
static601_3.setWaitTime(120 , 180)
static601_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static601_3

def static602_1 = myRooms.BuccFull_602.spawnSpawner("static602_1", "sand_golem_boardwalk", 1)
static602_1.setPos(550, 290)
static602_1.setGuardPostForChildren("BuccFull_602", 550, 290, 45)
static602_1.setHome("BuccFull_602", 550, 290)
static602_1.setSpawnWhenPlayersAreInRoom(true)
static602_1.setWaitTime(120 , 180)
static602_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static602_1

def static602_2 = myRooms.BuccFull_602.spawnSpawner("static602_2", "sand_golem_boardwalk", 1)
static602_2.setPos(870, 210)
static602_2.setGuardPostForChildren("BuccFull_602", 870, 210, 45)
static602_2.setHome("BuccFull_602", 870, 210)
static602_2.setSpawnWhenPlayersAreInRoom(true)
static602_2.setWaitTime(120 , 180)
static602_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static602_2

def static602_3 = myRooms.BuccFull_602.spawnSpawner("static602_3", "anchor_bug_boardwalk", 1)
static602_3.setPos(260, 590)
static602_3.setGuardPostForChildren("BuccFull_602", 260, 590, 45)
static602_3.setHome("BuccFull_602", 260, 590)
static602_3.setSpawnWhenPlayersAreInRoom(true)
static602_3.setWaitTime(120 , 180)
static602_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static602_3

def static602_4 = myRooms.BuccFull_602.spawnSpawner("static602_4", "anchor_bug_boardwalk", 1)
static602_4.setPos(820, 490)
static602_4.setGuardPostForChildren("BuccFull_602", 820, 490, 45)
static602_4.setHome("BuccFull_602", 820, 490)
static602_4.setSpawnWhenPlayersAreInRoom(true)
static602_4.setWaitTime(120 , 180)
static602_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static602_4

def static602_5 = myRooms.BuccFull_602.spawnSpawner("static602_5", "anchor_bug_boardwalk", 1)
static602_5.setPos(780, 600)
static602_5.setGuardPostForChildren("BuccFull_602", 780, 600, 45)
static602_5.setHome("BuccFull_602", 780, 600)
static602_5.setSpawnWhenPlayersAreInRoom(true)
static602_5.setWaitTime(120 , 180)
static602_5.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static602_5

def static603_1 = myRooms.BuccFull_603.spawnSpawner("static603_1", "anchor_bug_boardwalk", 1)
static603_1.setPos(90, 490)
static603_1.setGuardPostForChildren("BuccFull_603", 90, 490, 45)
static603_1.setHome("BuccFull_603", 90, 490)
static603_1.setSpawnWhenPlayersAreInRoom(true)
static603_1.setWaitTime(120 , 180)
static603_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static603_1

def static603_2 = myRooms.BuccFull_603.spawnSpawner("static603_2", "anchor_bug_boardwalk", 1)
static603_2.setPos(220, 390)
static603_2.setGuardPostForChildren("BuccFull_603", 220, 390, 45)
static603_2.setHome("BuccFull_603", 220, 390)
static603_2.setSpawnWhenPlayersAreInRoom(true)
static603_2.setWaitTime(120 , 180)
static603_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static603_2

def static603_3 = myRooms.BuccFull_603.spawnSpawner("static603_3", "anchor_bug_boardwalk", 1)
static603_3.setPos(470, 600)
static603_3.setGuardPostForChildren("BuccFull_603", 470, 600, 45)
static603_3.setHome("BuccFull_603", 470, 600)
static603_3.setSpawnWhenPlayersAreInRoom(true)
static603_3.setWaitTime(120 , 180)
static603_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static603_3

def static603_4 = myRooms.BuccFull_603.spawnSpawner("static603_4", "anchor_bug_boardwalk", 1)
static603_4.setPos(630, 580)
static603_4.setGuardPostForChildren("BuccFull_603", 630, 580, 45)
static603_4.setHome("BuccFull_603", 630, 580)
static603_4.setSpawnWhenPlayersAreInRoom(true)
static603_4.setWaitTime(120 , 180)
static603_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static603_4

def static604_1 = myRooms.BuccFull_604.spawnSpawner("static604_1", "sand_fluff_boardwalk", 1)
static604_1.setPos(70, 420)
static604_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static604_1.childrenWander( true )
static604_1.setHome("BuccFull_604", 70, 420)
static604_1.setSpawnWhenPlayersAreInRoom(true)
static604_1.setWaitTime(120 , 180)
static604_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static604_1

def static604_2 = myRooms.BuccFull_604.spawnSpawner("static604_2", "sand_fluff_boardwalk", 1)
static604_2.setPos(110, 600)
static604_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static604_2.childrenWander( true )
static604_2.setHome("BuccFull_604", 110, 600)
static604_2.setSpawnWhenPlayersAreInRoom(true)
static604_2.setWaitTime(120 , 180)
static604_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static604_2

def static606_1 = myRooms.BuccFull_606.spawnSpawner("static606_1", "anchor_bug_boardwalk", 1)
static606_1.setPos(520, 580)
static606_1.setGuardPostForChildren("BuccFull_606", 520, 580, 45)
static606_1.setHome("BuccFull_606", 520, 580)
static606_1.setSpawnWhenPlayersAreInRoom(true)
static606_1.setWaitTime(120 , 180)
static606_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static606_1

def static606_2 = myRooms.BuccFull_606.spawnSpawner("static606_2", "anchor_bug_boardwalk", 1)
static606_2.setPos(700, 220)
static606_2.setGuardPostForChildren("BuccFull_606", 700, 220, 45)
static606_2.setHome("BuccFull_606", 700, 220)
static606_2.setSpawnWhenPlayersAreInRoom(true)
static606_2.setWaitTime(120 , 180)
static606_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static606_2

def static702_1 = myRooms.BuccFull_702.spawnSpawner("static702_1", "anchor_bug_boardwalk", 1)
static702_1.setPos(870, 290)
static702_1.setGuardPostForChildren("BuccFull_702", 870, 290, 45)
static702_1.setHome("BuccFull_702", 870, 290)
static702_1.setSpawnWhenPlayersAreInRoom(true)
static702_1.setWaitTime(120 , 180)
static702_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static702_1

def static702_2 = myRooms.BuccFull_702.spawnSpawner("static702_2", "anchor_bug_boardwalk", 1)
static702_2.setPos(700, 570)
static702_2.setGuardPostForChildren("BuccFull_702", 700, 570, 45)
static702_2.setHome("BuccFull_702", 700, 570)
static702_2.setSpawnWhenPlayersAreInRoom(true)
static702_2.setWaitTime(120 , 180)
static702_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static702_2

def static702_3 = myRooms.BuccFull_702.spawnSpawner("static702_3", "anchor_bug_boardwalk", 1)
static702_3.setPos(540, 200)
static702_3.setGuardPostForChildren("BuccFull_702", 540, 200, 45)
static702_3.setHome("BuccFull_702", 540, 200)
static702_3.setSpawnWhenPlayersAreInRoom(true)
static702_3.setWaitTime(120 , 180)
static702_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static702_3

def static702_4 = myRooms.BuccFull_702.spawnSpawner("static702_4", "anchor_bug_boardwalk", 1)
static702_4.setPos(120, 590)
static702_4.setGuardPostForChildren("BuccFull_702", 120, 590, 45)
static702_4.setHome("BuccFull_702", 120, 590)
static702_4.setSpawnWhenPlayersAreInRoom(true)
static702_4.setWaitTime(120 , 180)
static702_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static702_4

def static703_1 = myRooms.BuccFull_703.spawnSpawner("static703_1", "sand_fluff_boardwalk", 1)
static703_1.setPos(910, 570)
static703_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static703_1.childrenWander( true )
static703_1.setHome("BuccFull_703", 910, 570)
static703_1.setSpawnWhenPlayersAreInRoom(true)
static703_1.setWaitTime(120 , 180)
static703_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static703_1

def static703_2 = myRooms.BuccFull_703.spawnSpawner("static703_2", "sand_fluff_boardwalk", 1)
static703_2.setPos(660, 550)
static703_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static703_2.childrenWander( true )
static703_2.setHome("BuccFull_703", 660, 550)
static703_2.setSpawnWhenPlayersAreInRoom(true)
static703_2.setWaitTime(120 , 180)
static703_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static703_2

def static703_3 = myRooms.BuccFull_703.spawnSpawner("static703_3", "anchor_bug_boardwalk", 1)
static703_3.setPos(550, 210)
static703_3.setGuardPostForChildren("BuccFull_703", 550, 210, 45)
static703_3.setHome("BuccFull_703", 550, 210)
static703_3.setSpawnWhenPlayersAreInRoom(true)
static703_3.setWaitTime(120 , 180)
static703_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static703_3

def static703_4 = myRooms.BuccFull_703.spawnSpawner("static703_4", "anchor_bug_boardwalk", 1)
static703_4.setPos(410, 540)
static703_4.setGuardPostForChildren("BuccFull_703", 410, 540, 45)
static703_4.setHome("BuccFull_703", 410, 540)
static703_4.setSpawnWhenPlayersAreInRoom(true)
static703_4.setWaitTime(120 , 180)
static703_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static703_4

def static703_5 = myRooms.BuccFull_703.spawnSpawner("static703_5", "anchor_bug_boardwalk", 1)
static703_5.setPos(210, 530)
static703_5.setGuardPostForChildren("BuccFull_703", 210, 530, 45)
static703_5.setHome("BuccFull_703", 210, 530)
static703_5.setSpawnWhenPlayersAreInRoom(true)
static703_5.setWaitTime(120 , 180)
static703_5.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static703_5

def static704_1 = myRooms.BuccFull_704.spawnSpawner("static704_1", "sand_fluff_boardwalk", 1)
static704_1.setPos(80, 110)
static704_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static704_1.childrenWander( true )
static704_1.setHome("BuccFull_704", 80, 110)
static704_1.setSpawnWhenPlayersAreInRoom(true)
static704_1.setWaitTime(120 , 180)
static704_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static704_1

def static704_2 = myRooms.BuccFull_704.spawnSpawner("static704_2", "sand_fluff_boardwalk", 1)
static704_2.setPos(110, 280)
static704_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static704_2.childrenWander( true )
static704_2.setHome("BuccFull_704", 110, 280)
static704_2.setSpawnWhenPlayersAreInRoom(true)
static704_2.setWaitTime(120 , 180)
static704_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static704_2

def static704_3 = myRooms.BuccFull_704.spawnSpawner("static704_3", "sand_fluff_boardwalk", 1)
static704_3.setPos(70, 560)
static704_3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static704_3.childrenWander( true )
static704_3.setHome("BuccFull_704", 70, 560)
static704_3.setSpawnWhenPlayersAreInRoom(true)
static704_3.setWaitTime(120 , 180)
static704_3.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static704_3

def static704_4 = myRooms.BuccFull_704.spawnSpawner("static704_4", "sand_fluff_boardwalk", 1)
static704_4.setPos(200, 460)
static704_4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
static704_4.childrenWander( true )
static704_4.setHome("BuccFull_704", 200, 460)
static704_4.setSpawnWhenPlayersAreInRoom(true)
static704_4.setWaitTime(120 , 180)
static704_4.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static704_4

def static705_1 = myRooms.BuccFull_705.spawnSpawner("static705_1", "anchor_bug_boardwalk", 1)
static705_1.setPos(920, 300)
static705_1.setGuardPostForChildren("BuccFull_705", 920, 300, 45)
static705_1.setHome("BuccFull_705", 920, 300)
static705_1.setSpawnWhenPlayersAreInRoom(true)
static705_1.setWaitTime(120 , 180)
static705_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static705_1

def static705_2 = myRooms.BuccFull_705.spawnSpawner("static705_2", "anchor_bug_boardwalk", 1)
static705_2.setPos(920, 400)
static705_2.setGuardPostForChildren("BuccFull_705", 920, 400, 45)
static705_2.setHome("BuccFull_705", 920, 400)
static705_2.setSpawnWhenPlayersAreInRoom(true)
static705_2.setWaitTime(120 , 180)
static705_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static705_2

/*def static706_1 = myRooms.BuccFull_706.spawnSpawner("static706_1", "anchor_bug_boardwalk", 1)
static706_1.setPos(760, 330)
static706_1.setGuardPostForChildren("BuccFull_706", 760, 330, 45)
static706_1.setHome("BuccFull_706", 760, 330)
static706_1.setSpawnWhenPlayersAreInRoom(true)
static706_1.setWaitTime(120 , 180)
static706_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static706_1*/

def static802_1 = myRooms.BuccFull_802.spawnSpawner("static802_1", "anchor_bug_boardwalk", 1)
static802_1.setPos(380, 130)
static802_1.setGuardPostForChildren("BuccFull_802", 380, 130, 45)
static802_1.setHome("BuccFull_802", 380, 130)
static802_1.setSpawnWhenPlayersAreInRoom(true)
static802_1.setWaitTime(120 , 180)
static802_1.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static802_1

def static802_2 = myRooms.BuccFull_802.spawnSpawner("static802_2", "anchor_bug_boardwalk", 1)
static802_2.setPos(880, 210)
static802_2.setGuardPostForChildren("BuccFull_802", 880, 210, 45)
static802_2.setHome("BuccFull_802", 880, 210)
static802_2.setSpawnWhenPlayersAreInRoom(true)
static802_2.setWaitTime(120 , 180)
static802_2.setMonsterLevelForChildren(10.5)

buccFullSpawnerList << static802_2

def roamer203_1 = myRooms.BuccFull_203.spawnSpawner("roamer203_1", "sand_fluff_boardwalk", 1)
roamer203_1.setPos(150, 290)
roamer203_1.setHome("BuccFull_203", 150, 290)
roamer203_1.setSpawnWhenPlayersAreInRoom(true)
roamer203_1.setWaitTime(120 , 180)
roamer203_1.setMonsterLevelForChildren(10.5)
roamer203_1.addPatrolPointForChildren("BuccFull_202", 400, 490, 0)
roamer203_1.addPatrolPointForChildren("BuccFull_201", 920, 530, 10)
roamer203_1.addPatrolPointForChildren("BuccFull_202", 400, 490, 0)
roamer203_1.addPatrolPointForChildren("BuccFull_203", 150, 290, 10)

buccFullSpawnerList << roamer203_1

def roamer203_2 = myRooms.BuccFull_203.spawnSpawner("roamer203_2", "sand_fluff_boardwalk", 1)
roamer203_2.setPos(600, 430)
roamer203_2.setHome("BuccFull_203", 600, 430)
roamer203_2.setSpawnWhenPlayersAreInRoom(true)
roamer203_2.setWaitTime(120 , 180)
roamer203_2.setMonsterLevelForChildren(10.5)
roamer203_2.addPatrolPointForChildren("BuccFull_204", 60, 220, 0)
roamer203_2.addPatrolPointForChildren("BuccFull_204", 300, 220, 10)
roamer203_2.addPatrolPointForChildren("BuccFull_204", 60, 220, 0)
roamer203_2.addPatrolPointForChildren("BuccFull_203", 600, 430, 10)

buccFullSpawnerList << roamer203_2

def roamer205_1 = myRooms.BuccFull_205.spawnSpawner("roamer205_1", "sand_fluff_boardwalk", 1)
roamer205_1.setPos(430, 430)
roamer205_1.setHome("BuccFull_205", 430, 430)
roamer205_1.setSpawnWhenPlayersAreInRoom(true)
roamer205_1.setWaitTime(120 , 180)
roamer205_1.setMonsterLevelForChildren(10.5)
roamer205_1.addPatrolPointForChildren("BuccFull_204", 960, 220, 0)
roamer205_1.addPatrolPointForChildren("BuccFull_204", 680, 220, 10)
roamer205_1.addPatrolPointForChildren("BuccFull_204", 960, 220, 0)
roamer205_1.addPatrolPointForChildren("BuccFull_205", 430, 430, 10)

buccFullSpawnerList << roamer205_1

def roamer205_2 = myRooms.BuccFull_205.spawnSpawner("roamer205_2", "sand_fluff_boardwalk", 1)
roamer205_2.setPos(920, 530)
roamer205_2.setHome("BuccFull_205", 920, 530)
roamer205_2.setSpawnWhenPlayersAreInRoom(true)
roamer205_2.setWaitTime(120 , 180)
roamer205_2.setMonsterLevelForChildren(10.5)
roamer205_2.addPatrolPointForChildren("BuccFull_206", 150, 630, 0)
roamer205_2.addPatrolPointForChildren("BuccFull_306", 500, 100, 10)
roamer205_2.addPatrolPointForChildren("BuccFull_206", 150, 630, 0)
roamer205_2.addPatrolPointForChildren("BuccFull_205", 920, 530, 10)

buccFullSpawnerList << roamer205_2

def roamer302_1 = myRooms.BuccFull_302.spawnSpawner("roamer302_1", "sand_fluff_boardwalk", 1)
roamer302_1.setPos(590, 410)
roamer302_1.setHome("BuccFull_302", 590, 410)
roamer302_1.setSpawnWhenPlayersAreInRoom(true)
roamer302_1.setWaitTime(120 , 180)
roamer302_1.setMonsterLevelForChildren(10.5)
roamer302_1.addPatrolPointForChildren("BuccFull_402", 520, 140, 0)
roamer302_1.addPatrolPointForChildren("BuccFull_402", 520, 470, 10)
roamer302_1.addPatrolPointForChildren("BuccFull_402", 520, 140, 0)
roamer302_1.addPatrolPointForChildren("BuccFull_302", 590, 410, 10)

buccFullSpawnerList << roamer302_1

def roamer303_1 = myRooms.BuccFull_303.spawnSpawner("roamer303_1", "sand_fluff_boardwalk", 1)
roamer303_1.setPos(200, 170)
roamer303_1.setHome("BuccFull_303", 200, 170)
roamer303_1.setSpawnWhenPlayersAreInRoom(true)
roamer303_1.setWaitTime(120 , 180)
roamer303_1.setMonsterLevelForChildren(10.5)
roamer303_1.addPatrolPointForChildren("BuccFull_302", 670, 170, 0)
roamer303_1.addPatrolPointForChildren("BuccFull_202", 510, 600, 10)
roamer303_1.addPatrolPointForChildren("BuccFull_302", 670, 170, 0)
roamer303_1.addPatrolPointForChildren("BuccFull_303", 200, 170, 10)

buccFullSpawnerList << roamer303_1

def roamer305_1 = myRooms.BuccFull_305.spawnSpawner("roamer305_1", "sand_fluff_boardwalk", 1)
roamer305_1.setPos(930, 140)
roamer305_1.setHome("BuccFull_305", 930, 140)
roamer305_1.setSpawnWhenPlayersAreInRoom(true)
roamer305_1.setWaitTime(120 , 180)
roamer305_1.setMonsterLevelForChildren(10.5)
roamer305_1.addPatrolPointForChildren("BuccFull_306", 160, 530, 0)
roamer305_1.addPatrolPointForChildren("BuccFull_406", 640, 150, 10)
roamer305_1.addPatrolPointForChildren("BuccFull_306", 160, 530, 0)
roamer305_1.addPatrolPointForChildren("BuccFull_305", 930, 140, 10)

buccFullSpawnerList << roamer305_1

def roamer305_2 = myRooms.BuccFull_305.spawnSpawner("roamer305_2", "sand_fluff_boardwalk", 1)
roamer305_2.setPos(930, 550)
roamer305_2.setHome("BuccFull_305", 930, 550)
roamer305_2.setSpawnWhenPlayersAreInRoom(true)
roamer305_2.setWaitTime(120 , 180)
roamer305_2.setMonsterLevelForChildren(10.5)
roamer305_2.addPatrolPointForChildren("BuccFull_405", 930, 350, 0)
roamer305_2.addPatrolPointForChildren("BuccFull_406", 80, 610, 10)
roamer305_2.addPatrolPointForChildren("BuccFull_405", 930, 350, 0)
roamer305_2.addPatrolPointForChildren("BuccFull_305", 930, 550, 10)

buccFullSpawnerList << roamer305_2

def roamer506_1 = myRooms.BuccFull_506.spawnSpawner("roamer506_1", "sand_fluff_boardwalk", 1)
roamer506_1.setPos(410, 150)
roamer506_1.setHome("BuccFull_506", 410, 150)
roamer506_1.setSpawnWhenPlayersAreInRoom(true)
roamer506_1.setWaitTime(120 , 180)
roamer506_1.setMonsterLevelForChildren(10.5)
roamer506_1.addPatrolPointForChildren("BuccFull_506", 590, 640, 0)
roamer506_1.addPatrolPointForChildren("BuccFull_606", 610, 550, 10)
roamer506_1.addPatrolPointForChildren("BuccFull_506", 590, 640, 0)
roamer506_1.addPatrolPointForChildren("BuccFull_506", 410, 150, 10)

buccFullSpawnerList << roamer506_1

def roamer601_1 = myRooms.BuccFull_601.spawnSpawner("roamer601_1", "sand_fluff_boardwalk", 1)
roamer601_1.setPos(390, 420)
roamer601_1.setHome("BuccFull_601", 390, 420)
roamer601_1.setSpawnWhenPlayersAreInRoom(true)
roamer601_1.setWaitTime(120 , 180)
roamer601_1.setMonsterLevelForChildren(10.5)
roamer601_1.addPatrolPointForChildren("BuccFull_601", 200, 100, 0)
roamer601_1.addPatrolPointForChildren("BuccFull_501", 250, 370, 10)
roamer601_1.addPatrolPointForChildren("BuccFull_601", 200, 100, 0)
roamer601_1.addPatrolPointForChildren("BuccFull_601", 390, 420, 10)

buccFullSpawnerList << roamer601_1

def roamer601_2 = myRooms.BuccFull_601.spawnSpawner("roamer601_2", "sand_fluff_boardwalk", 1)
roamer601_2.setPos(390, 470)
roamer601_2.setHome("BuccFull_601", 390, 470)
roamer601_2.setSpawnWhenPlayersAreInRoom(true)
roamer601_2.setWaitTime(120 , 180)
roamer601_2.setMonsterLevelForChildren(10.5)
roamer601_2.addPatrolPointForChildren("BuccFull_602", 350, 470, 10)
roamer601_2.addPatrolPointForChildren("BuccFull_601", 390, 470, 10)

buccFullSpawnerList << roamer601_2

def roamer602_1 = myRooms.BuccFull_602.spawnSpawner("roamer602_1", "sand_fluff_boardwalk", 1)
roamer602_1.setPos(700, 430)
roamer602_1.setHome("BuccFull_602", 700, 430)
roamer602_1.setSpawnWhenPlayersAreInRoom(true)
roamer602_1.setWaitTime(120 , 180)
roamer602_1.setMonsterLevelForChildren(10.5)
roamer602_1.addPatrolPointForChildren("BuccFull_603", 110, 410, 0)
roamer602_1.addPatrolPointForChildren("BuccFull_603", 440, 440, 10)
roamer602_1.addPatrolPointForChildren("BuccFull_603", 110, 410, 0)
roamer602_1.addPatrolPointForChildren("BuccFull_602", 700, 430, 10)

buccFullSpawnerList << roamer602_1

def roamer602_2 = myRooms.BuccFull_602.spawnSpawner("roamer602_2", "sand_fluff_boardwalk", 1)
roamer602_2.setPos(710, 240)
roamer602_2.setHome("BuccFull_602", 710, 240)
roamer602_2.setSpawnWhenPlayersAreInRoom(true)
roamer602_2.setWaitTime(120 , 180)
roamer602_2.setMonsterLevelForChildren(10.5)
roamer602_2.addPatrolPointForChildren("BuccFull_502", 560, 630, 0)
roamer602_2.addPatrolPointForChildren("BuccFull_502", 500, 220, 10)
roamer602_2.addPatrolPointForChildren("BuccFull_502", 560, 630, 0)
roamer602_2.addPatrolPointForChildren("BuccFull_602", 710, 240, 10)

buccFullSpawnerList << roamer602_2

def roamer702_1 = myRooms.BuccFull_702.spawnSpawner("roamer702_1", "sand_fluff_boardwalk", 1)
roamer702_1.setPos(700, 250)
roamer702_1.setHome("BuccFull_702", 700, 250)
roamer702_1.setSpawnWhenPlayersAreInRoom(true)
roamer702_1.setWaitTime(120 , 180)
roamer702_1.setMonsterLevelForChildren(10.5)
roamer702_1.addPatrolPointForChildren("BuccFull_702", 250, 480, 0)
roamer702_1.addPatrolPointForChildren("BuccFull_802", 290, 90, 0)
roamer702_1.addPatrolPointForChildren("BuccFull_802", 630, 140, 10)
roamer702_1.addPatrolPointForChildren("BuccFull_802", 290, 90, 0)
roamer702_1.addPatrolPointForChildren("BuccFull_702", 250, 480, 0)
roamer702_1.addPatrolPointForChildren("BuccFull_702", 700, 250, 10)

buccFullSpawnerList << roamer702_1

def roamer702_2 = myRooms.BuccFull_702.spawnSpawner("roamer702_2", "sand_fluff_boardwalk", 1)
roamer702_2.setPos(980, 180)
roamer702_2.setHome("BuccFull_702", 980, 180)
roamer702_2.setSpawnWhenPlayersAreInRoom(true)
roamer702_2.setWaitTime(120 , 180)
roamer702_2.setMonsterLevelForChildren(10.5)
roamer702_2.addPatrolPointForChildren("BuccFull_703", 240, 170, 0)
roamer702_2.addPatrolPointForChildren("BuccFull_603", 750, 590, 10)
roamer702_2.addPatrolPointForChildren("BuccFull_703", 240, 170, 0)
roamer702_2.addPatrolPointForChildren("BuccFull_702", 980, 180, 10)

buccFullSpawnerList << roamer702_2

landsharkSpawner1 = myRooms.BuccFull_306.spawnStoppedSpawner("roamer306_2", "landshark_boardwalk", 1)
landsharkSpawner1.setPos(130, 570)
landsharkSpawner1.setHome("BuccFull_306", 130, 570)
landsharkSpawner1.setSpawnWhenPlayersAreInRoom(true)
landsharkSpawner1.setWaitTime(120 , 180)
landsharkSpawner1.setMonsterLevelForChildren(11)
landsharkSpawner1.addPatrolPointForChildren("BuccFull_206", 100, 620, 0)
landsharkSpawner1.addPatrolPointForChildren("BuccFull_205", 850, 590, 0)
landsharkSpawner1.addPatrolPointForChildren("BuccFull_305", 940, 570, 0)
landsharkSpawner1.addPatrolPointForChildren("BuccFull_405", 960, 370, 0)
landsharkSpawner1.addPatrolPointForChildren("BuccFull_406", 720, 570, 0)
landsharkSpawner1.addPatrolPointForChildren("BuccFull_306", 530, 110, 0)

landsharkSpawnerList << landsharkSpawner1

landsharkSpawner2 = myRooms.BuccFull_302.spawnStoppedSpawner("roamer302_2", "landshark_boardwalk", 1)
landsharkSpawner2.setPos(650, 210)
landsharkSpawner2.setHome("BuccFull_202", 710, 300)
landsharkSpawner2.setSpawnWhenPlayersAreInRoom(true)
landsharkSpawner2.setWaitTime(120 , 180)
landsharkSpawner2.setMonsterLevelForChildren(11)
landsharkSpawner2.addPatrolPointForChildren("BuccFull_303", 240, 210, 0)
landsharkSpawner2.addPatrolPointForChildren("BuccFull_203", 490, 340, 0)
landsharkSpawner2.addPatrolPointForChildren("BuccFull_202", 400, 510, 0)
landsharkSpawner2.addPatrolPointForChildren("BuccFull_201", 940, 580, 0)
landsharkSpawner2.addPatrolPointForChildren("BuccFull_301", 940, 180, 0)
landsharkSpawner2.addPatrolPointForChildren("BuccFull_302", 650, 210, 0)

landsharkSpawnerList << landsharkSpawner2

/*def landsharkPen1 = "landsharkPen1"
myRooms.BuccFull_203.createTriggerZone(landsharkPen1, 610, 340, 760, 580)
myManager.onTriggerIn(myRooms.BuccFull_203, landsharkPen1) { event ->
	if(event.actor == landsharkBoss) {
		println "^^^ Caught entry of landshark. Hate list = ${event.actor.getHated()} ^^^^"
		event.actor.getHated()
	}
}*/

//Game Logic
def checkForLandy() {
	getMonsterTotal()
	if(monsterCounter <= 57 && landsharkSpawned == false) {
		landsharkSpawned = true
		landsharkSpawner = random(landsharkSpawnerList)
		landsharkBoss = landsharkSpawner.forceSpawnNow()
		landsharkBoss.setClientDisplayScale(1.3)
		
		//println "^^^^ Spawned ${landsharkBoss} ^^^^"
		
		landsharkDisposed = false
		omnomnomnom()
		
		runOnDeath(landsharkBoss) { 
			//println "^^^^ Caught death of ${landsharkBoss} ^^^^"
			landsharkSpawned = false 
			myManager.schedule(random(3600, 10800)) { checkForLandy() }
		}
		myManager.schedule(600) { checkForDisposal() }
	} else {
		myManager.schedule(600) { checkForLandy() }
	}
}

def getMonsterTotal() {
	monsterCounter = 0 //Reset the counter to get a fresh count
	buccFullSpawnerList.clone().each { //Step through the spawner list
		monsterCounter = monsterCounter + it.spawnsInUse() //Add the number of spawns to the current total
	}
	//println "^^^^ monsterCounter = ${monsterCounter} ^^^^"
}

def checkForDisposal() {
	if(!landsharkBoss.isDead()) {
		if(landsharkBoss.getHated().size() == 0) {
			//println "^^^ Disposing ${landsharkBoss} ^^^^"
			landsharkDisposed = true
			landsharkSpawned = false
			landsharkBoss.dispose()
			
			myManager.schedule(random(3600, 10800)) { checkForLandy() }
		} else {
			myManager.schedule(10) { checkForDisposal() }
		}
	}
}

getMonsterTotal()

def setAlliances() {
	buccFullSpawnerList.clone().each { //Step through the spawner list
		allianceSpawner = it //Define the spawner to be modified
		buccFullSpawnerList.clone().each { //Step through again
			if(allianceSpawner != it) {
				allianceSpawner.allyWithSpawner(it) //Add the spawner to the defined spawners alliance list, if it is not the same spawner
			}
		}
	}
}

def omnomnomnom() {
	if( !landsharkBoss.isDead() ) {
		landsharkBoss.shout( "OMNOMNOMNOM!" )
		myManager.schedule( random( 10, 20 ) ) { omnomnomnom() }
	}
}

//Startup
myManager.schedule (300) { checkForLandy() }
setAlliances()
