//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def Marcy = spawnNPC("BFG-Marcy", myRooms.Beach_1006, 100, 440)
Marcy.setDisplayName( "Marcy" )
Marcy.setRotation( 345 )

/*
//DEBUG SCRIPT ONLY
myManager.onEnter( myRooms.Beach_503 ) { event ->
	if( isPlayer( event.actor ) && !event.actor.isOnQuest( 258 ) ) {
		event.actor.updateQuest( 258, "BFG-Marcy" )
		event.actor.setQuestFlag( GLOBAL, "Z9_MetalDetector103" )
		event.actor.setQuestFlag( GLOBAL, "Z9_MetalDetector304" )
		event.actor.setQuestFlag( GLOBAL, "Z9_MetalDetector502" )
		event.actor.setQuestFlag( GLOBAL, "Z9_MetalDetector703" )
		event.actor.setQuestFlag( GLOBAL, "Z9_MetalDetector1003" )
		event.actor.centerPrint( "You've started the metal detection quest" )
	}
}
*/

onQuestStep( 258, 2 ) { event -> event.player.addMiniMapQuestActorName( "BFG-Marcy" ) } 

//---------------------------------------------------------------
//Player has talked to Mark                                      
//---------------------------------------------------------------
def marcyStart = Marcy.createConversation("marcyStart", true, "!QuestStarted_258", "!QuestCompleted_258")

def marcyStart1 = [id:1]
marcyStart1.npctext = "%p! I'm Marcy with the Barton Regulars and I need your help."
marcyStart1.options = []
marcyStart1.options << [text:"Okay Marcy, I suppose I can't refuse the Regulars no matter how boring the task may be.", result: 2]
marcyStart1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 232, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 232)
			Marcy.say("Of course I have free stuff for my little helper.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Marcy today. Try again tomorrow!" )
		}
	})
}, result: DONE]
marcyStart.addDialog(marcyStart1, Marcy)

def marcyStart2 = [id:2]
marcyStart2.npctext = "Actually it should be pretty fun. Jacques, the aged lighthouse keeper, has lost some mementos in the sand. Fortunately, they're all metallic so finding them should only require walking around the beach with a metal detector. I'd do it myself but I already have enough sand in my armor!"
marcyStart2.options = []
marcyStart2.options << [text:"Sounds like fun, I'll do it. How do I get started?", result: 3]
marcyStart2.options << [text:"Bah. Waste of time.", result: 4]
marcyStart.addDialog(marcyStart2, Marcy)

def marcyStart3 = [id:3]
marcyStart3.npctext = "It's easy! Just walk around the beach and listen for beeps. The faster the beeps come the closer you are to one of Jacques mementos. Find all five of them and bring them back to me."
marcyStart3.playertext = "That does seem easy."
marcyStart3.result = 5
marcyStart.addDialog(marcyStart3, Marcy)

def marcyStart4 = [id:4]
marcyStart4.npctext = "Erp. I hope you change your mind. I'm not looking forward to digging through the sand in this armor."
marcyStart4.flag = "Z9_Marcy_Memento_Break"
marcyStart4.exec = { event -> event.player.removeMiniMapQuestActorName( "BFG-Marcy" ) }
marcyStart4.result = DONE
marcyStart.addDialog(marcyStart4, Marcy)

def marcyStart5 = [id:5]
marcyStart5.npctext = "Do be careful, though. The Animated in this area are very vicious and I'd hate to see you injured."
marcyStart5.quest = 258
marcyStart5.flag = ["Z9_MetalDetector103", "Z9_MetalDetector304", "Z9_MetalDetector502", "Z9_MetalDetector703", "Z9_MetalDetector1003"]
marcyStart5.exec = { event ->
	event.player.removeMiniMapQuestActorName( "BFG-Marcy" )
	event.player.setPlayerVar( "metalCount", 0 )
	if(!event.player.hasQuestFlag(GLOBAL, "markMarcyStarted1")) {
		event.player.setQuestFlag(GLOBAL, "markMarcyStarted1")
	}
}
marcyStart5.result = DONE
marcyStart.addDialog(marcyStart5, Marcy)

//---------------------------------------------------------------
//Memento Detector Break                                         
//---------------------------------------------------------------
def marcyMementoBreak = Marcy.createConversation("mementoBreak", true, "Z9_Marcy_Memento_Break")

def marcyMementoBreak1 = [id:1]
marcyMementoBreak1.npctext = "Decided to help Jacque and I out after all, %p?"
marcyMementoBreak1.options = []
marcyMementoBreak1.options << [text:"Yeah, so how do I do this?", result: 2]
marcyMementoBreak1.options << [text:"No. Not yet.", result: 4]
marcyMementoBreak1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 232, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 232)
			Marcy.say("Of course I have free stuff for my little helper.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Marcy today. Try again tomorrow!" )
		}
	})
}, result: DONE]
marcyMementoBreak.addDialog(marcyMementoBreak1, Marcy)

def marcyMementoBreak2 = [id:2]
marcyMementoBreak2.npctext = "It's easy! Just walk around the beach and listen for beeps. The faster the beeps come the closer you are to one of Jacques mementos. Find all five of them and bring them back to me."
marcyMementoBreak2.playertext = "That does seem easy."
marcyMementoBreak2.result = 3
marcyMementoBreak.addDialog(marcyMementoBreak2, Marcy)

def marcyMementoBreak3 = [id:3]
marcyMementoBreak3.npctext = "Do be careful, though. The Animated in this area are very vicious and I'd hate to see you injured."
marcyMementoBreak3.quest = 258
marcyMementoBreak3.flag = ["Z9_MetalDetector103", "Z9_MetalDetector304", "Z9_MetalDetector502", "Z9_MetalDetector703", "Z9_MetalDetector1003", "!Z9_Marcy_Memento_Break"]
marcyMementoBreak3.exec = { event ->
	event.player.removeMiniMapQuestActorName( "BFG-Marcy" )
	event.player.setPlayerVar( "metalCount", 0 )
}
marcyMementoBreak3.result = DONE
marcyMementoBreak.addDialog(marcyMementoBreak3, Marcy)

def marcyMementoBreak4 = [id:4]
marcyMementoBreak4.npctext = "Don't be shy if you change your mind."
marcyMementoBreak4.result = DONE
marcyMementoBreak.addDialog(marcyMementoBreak4, Marcy)

//---------------------------------------------------------------
//Memento Detector Active                                        
//---------------------------------------------------------------
def mementoActive = Marcy.createConversation("mementoActive", true, "QuestStarted_258:2")

def mementoActive1 = [id:1]
mementoActive1.npctext = "Remember, listen for the beeps. The more frequent the beeps the closer you are to a memento. Good luck!"
mementoActive1.playertext = "Do you have any freebies available today?"
mementoActive1.exec = { event ->
	checkDailyChance(event.player, 232, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 232)
			Marcy.say("Of course I have free stuff for my little helper.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Marcy today. Try again tomorrow!" )
		}
	})
}
mementoActive1.result = DONE
mementoActive.addDialog(mementoActive1, Marcy)

//---------------------------------------------------------------
//Memento Detector Complete                                      
//---------------------------------------------------------------
def mementoComplete = Marcy.createConversation("mementoComplete", true, "QuestStarted_258:3")
mementoComplete.setUrgent(true)

def mementoComplete1 = [id:1]
mementoComplete1.npctext = "%p you found them! Oh what a relief. It wasn't too sandy was it?"
mementoComplete1.options = []
mementoComplete1.options << [text:"It wasn't bad at all.", result: 2]
mementoComplete1.options << [text:"It was miserable!", result: 3]
mementoComplete.addDialog(mementoComplete1, Marcy)

def mementoComplete2 = [id:2]
mementoComplete2.npctext = "Good to hear. Jacques will be so pleased. Thank you again for your help, %p."
mementoComplete2.result = DONE
mementoComplete2.exec = { event ->
	event.player.updateQuest( 258, "BFG-Marcy" )
	event.player.unsetQuestFlag(GLOBAL, "Z3_Albert_Active")
	event.player.grantItem("17750")
	event.player.removeMiniMapQuestActorName("BFG-Marcy")
}
mementoComplete.addDialog(mementoComplete2, Marcy)

def mementoComplete3 = [id:3]
mementoComplete3.npctext = "Sorry to hear that, %p. Still, I think Jacques will be very pleased to have his mementos back. Thank you for your help with what proved to be an arduous task."
mementoComplete3.result = DONE
mementoComplete3.exec = { event ->
	event.player.updateQuest( 258, "BFG-Marcy" )
	event.player.unsetQuestFlag(GLOBAL, "Z3_Albert_Active")
	event.player.grantItem("17750")
	event.player.removeMiniMapQuestActorName("BFG-Marcy")
}
mementoComplete.addDialog(mementoComplete3, Marcy)

//---------------------------------------------------------------
//Quest Chain Ended                                              
//---------------------------------------------------------------
def mementoEnd = Marcy.createConversation("mementoEnd", true, "QuestCompleted_258")

def mementoEnd1 = [id:1]
mementoEnd1.npctext = "Hello again, %p."
mementoEnd1.playertext = "Do you have any freebies available today?"
mementoEnd1.exec = { event ->
	checkDailyChance(event.player, 232, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 232)
			Marcy.say("Of course I have free stuff for my little helper.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Marcy today. Try again tomorrow!" )
		}
	})
}
mementoEnd1.result = DONE
mementoEnd.addDialog(mementoEnd1, Marcy)