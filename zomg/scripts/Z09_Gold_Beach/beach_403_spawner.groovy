//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
def spawner1 = myRooms.Beach_403.spawnSpawner( "spawner_403_1", "anchor_bug", 1)
spawner1.setPos( 745, 100 )
spawner1.setHome( "Beach_403", 745, 100 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 80, 100 )
spawner1.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 6.8 )

// Spawner two
def spawner2 = myRooms.Beach_403.spawnSpawner( "spawner_403_2", "anchor_bug", 1)
spawner2.setPos( 610, 280 )
spawner2.setHome( "Beach_403", 610, 280 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 80, 100 )
spawner2.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 6.8 )

// Spawner three
def spawner3 = myRooms.Beach_403.spawnSpawner( "spawner_403_3", "anchor_bug", 1)
spawner3.setPos( 765, 375 )
spawner3.setHome( "Beach_403", 765, 375 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 80, 100 )
spawner3.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 6.8 )

//Alliances
spawner1.allyWithSpawner( spawner2 )
spawner1.allyWithSpawner( spawner3 )
spawner2.allyWithSpawner( spawner3 )

//Spawning
spawner1.forceSpawnNow()
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()