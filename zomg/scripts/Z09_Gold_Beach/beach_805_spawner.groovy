//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
spawner_805_1 = myRooms.Beach_805.spawnSpawner( "spawner_805_1", "water_spout", 1)
spawner_805_1.setPos( 135, 190 )
spawner_805_1.setHome( "Beach_805", 135, 190 )
spawner_805_1.setSpawnWhenPlayersAreInRoom( true )
spawner_805_1.setWaitTime( 30 , 40 )
spawner_805_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_805_1.childrenWander( true )
spawner_805_1.setMonsterLevelForChildren( 6.2 )

// Spawner two
spawner_805_2 = myRooms.Beach_805.spawnSpawner( "spawner_805_2", "water_spout", 1)
spawner_805_2.setPos( 675, 110 )
spawner_805_2.setHome( "Beach_805", 675, 110 )
spawner_805_2.setSpawnWhenPlayersAreInRoom( true )
spawner_805_2.setWaitTime( 30 , 40 )
spawner_805_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_805_2.childrenWander( true )
spawner_805_2.setMonsterLevelForChildren( 6.2 )

// Spawner three
spawner_805_3 = myRooms.Beach_805.spawnSpawner( "spawner_805_3", "water_spout", 1)
spawner_805_3.setPos( 740, 530 )
spawner_805_3.setHome( "Beach_805", 770, 530 )
spawner_805_3.setSpawnWhenPlayersAreInRoom( true )
spawner_805_3.setWaitTime( 30 , 40 )
spawner_805_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_805_3.childrenWander( true )
spawner_805_3.setMonsterLevelForChildren( 6.2 )

// Spawner four
spawner_805_4 = myRooms.Beach_805.spawnSpawner( "spawner_805_4", "water_spout", 1)
spawner_805_4.setPos( 225, 520 )
spawner_805_4.setHome( "Beach_805", 225, 520 )
spawner_805_4.setSpawnWhenPlayersAreInRoom( true )
spawner_805_4.setWaitTime( 30 , 40 )
spawner_805_4.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_805_4.childrenWander( true )
spawner_805_4.setMonsterLevelForChildren( 6.2 )

// Spawner five
spawner_805_5 = myRooms.Beach_805.spawnSpawner( "spawner_805_5", "water_spout", 1)
spawner_805_5.setPos( 440, 340 )
spawner_805_5.setHome( "Beach_805", 440, 340 )
spawner_805_5.setSpawnWhenPlayersAreInRoom( true )
spawner_805_5.setWaitTime( 30 , 40 )
spawner_805_5.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_805_5.childrenWander( true )
spawner_805_5.setMonsterLevelForChildren( 6.2 )

spawner_805_1.stopSpawning()
spawner_805_2.stopSpawning()
spawner_805_3.stopSpawning()
spawner_805_4.stopSpawning()
spawner_805_5.stopSpawning()

//Alliances
spawner_805_1.allyWithSpawner( spawner_805_2 )
spawner_805_1.allyWithSpawner( spawner_805_3 )
spawner_805_1.allyWithSpawner( spawner_805_4 )
spawner_805_1.allyWithSpawner( spawner_805_5 )
spawner_805_2.allyWithSpawner( spawner_805_3 )
spawner_805_2.allyWithSpawner( spawner_805_4 )
spawner_805_2.allyWithSpawner( spawner_805_5 )
spawner_805_3.allyWithSpawner( spawner_805_4 )
spawner_805_3.allyWithSpawner( spawner_805_5 )
spawner_805_4.allyWithSpawner( spawner_805_5 )

//Variable definition
spawnTime = false
spawnedSpout1 = false
spawnedSpout2 = false
spawnedSpout3 = false
spawnedSpout4 = false
spawnedSpout5 = false
spoutRespawning = false
spout_805_1 = null
spout_805_2 = null
spout_805_3 = null
spout_805_4 = null
spout_805_5 = null

def clockChecker() { 
	myManager.schedule(30) { spawnOrNoSpawn() } // ; //println "***** GST = ${ gst() } ******" }
}

def spawnChecker() {
	if( ( gst() > 1900 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 200 ) ) { //party goes from 7pm to 2am.
//	if( gst() > 0 ) { //This line for each setting of time parameters for testing.
		spawnTime = true
	} else {
		spawnTime = false
	}
}	

def checkForDespawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == false ) {
		clockChecker()
		if( spawnedSpout1 == true ) {
			if(spout_805_1.getHated().size() == 0) {
				spout_805_1.dispose()
				spawnedSpout1 = false
			}
		}
		if( spawnedSpout2 == true ) {
			if(spout_805_2.getHated().size() == 0) {
				spout_805_2.dispose()
				spawnedSpout2 = false
			}
		}
		if( spawnedSpout3 == true ) {
			if(spout_805_3.getHated().size() == 0) {
				spout_805_3.dispose()
				spawnedSpout3 = false
			}
		}
		if( spawnedSpout4 == true ) {
			if(spout_805_4.getHated().size() == 0) {
				spout_805_4.dispose()
				spawnedSpout4 = false
			}
		}
		if( spawnedSpout5 == true ) {
			if(spout_805_5.getHated().size() == 0) {
				spout_805_5.dispose()
				spawnedSpout5 = false
			}
		}
	}
	myManager.schedule(30) { checkForDespawn() }
}

def spawnOrNoSpawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == true ) {
		spoutRespawning = false
		if( spawnedSpout1 == false ) {
			spout_805_1 = spawner_805_1.forceSpawnNow()
			spawnedSpout1 = true
			runOnDeath( spout_805_1 ) { 
				spawnedSpout1 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout2 == false ) {
			spout_805_2 = spawner_805_2.forceSpawnNow()
			spawnedSpout2 = true
			runOnDeath( spout_805_2 ) { 
				spawnedSpout2 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout3 == false ) {
			spout_805_3 = spawner_805_3.forceSpawnNow()
			spawnedSpout3 = true
			runOnDeath( spout_805_3 ) { 
				spawnedSpout3 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout4 == false ) {
			spout_805_4 = spawner_805_4.forceSpawnNow()
			spawnedSpout4 = true
			runOnDeath( spout_805_4 ) { 
				spawnedSpout4 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout5 == false ) {
			spout_805_5 = spawner_805_5.forceSpawnNow()
			spawnedSpout5 = true
			runOnDeath( spout_805_5 ) { 
				spawnedSpout5 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
	}
}

//Initialization
clockChecker()
checkForDespawn()