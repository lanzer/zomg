//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//Variable, Map, List init
timerMap = new HashMap()

duneslamEnter = makeSwitch( "IslandBoat", myRooms.Beach_604, 755, 505 )
duneslamEnter.setMouseoverText("Row Boat")
duneslamEnter.unlock()
duneslamEnter.off()
duneslamEnter.setRange(500)

def goToDuneslam = { event ->
	difficulty = event.actor.getCrewVar( "Z29DuneslamDifficulty" ) 
	selectionInProgress = event.actor.getCrewVar( "Z29DuneslamDifficultySelectionInProgress" )
	event.actor.getCrew().each() { 
		if(it.isOnQuest(93) || it.isDoneQuest(93)) { 
			event.actor.setQuestFlag(GLOBAL, "Z09_Duneslam_Allowed") 
		} 
	}
	if(!event.actor.hasQuestFlag(GLOBAL, "Z09_Duneslam_Allowed")) {
		event.actor.centerPrint("You don't see anywhere useful to take the boat.")
	}
	if(event.actor.hasQuestFlag(GLOBAL, "Z09_Duneslam_Allowed")) {
		if( event.actor.getTeam().hasAreaVar( "Z29_Dune_Slam", "Z29DuneslamDifficulty" ) == false ) {
			if( event.actor.getCrewVar( "Z29DuneslamDifficultySelectionInProgress" ) == 0 ) {
				//set the "being selected" playerVar to 1 while selection process is being done (This prevents multiple players from initiating the process of selecting difficulties)
				event.actor.setCrewVar( "Z29DuneslamDifficultySelectionInProgress", 1 )
				
				//If the leader is in the zone then...
				if(isInZone( event.actor.getTeam().getLeader())) {
					
					event.actor.unsetQuestFlag(GLOBAL, "Z09_Duneslam_Allowed")
					leader = event.actor.getTeam().getLeader()
					makeMenu()
					event.actor.getCrew().each{ if( it != event.actor.getTeam().getLeader() ) { it.centerPrint( "Your Crew Leader is choosing the challenge level for this encounter." ) } }
					
					//Everyone else gets the following message while the leader chooses a setting. If no choice is made after 10 seconds, then everyone gets a message that says:
					decisionTimer = myManager.schedule(30) {
						event.actor.getCrew().each{
							it.centerPrint( "No choice was made within 30 seconds. Click the Row Boat to try again." )
						}
						event.actor.setCrewVar( "Z29DuneslamDifficultySelectionInProgress", 0 )
						//if no selection was made, then remove the leader and timer from the timerMap
						timerMap.remove( event.actor.getTeam().getLeader() )
					}
					//add the leader and timer to a map so the timer can be canceled later, if necessary
					timerMap.put( event.actor.getTeam().getLeader(), decisionTimer )
					
					//If the leader is not in the zone then tell the crew the leader must be present
				} else {
					event.actor.getCrew().each{
						if( it != event.actor.getTeam().getLeader() ) {
							it.centerPrint( "Your Crew Leader must be in Gold Beach before you can enter Duneslam's Island" )
						} else {
							it.centerPrint( "Your Crew is trying to go to Duneslam's Island. You must be in Gold Beach for that to occur." )
						}
					}
					event.actor.setCrewVar( "Z29DuneslamDifficultySelectionInProgress", 0 )
				}
			} else if( event.actor.getCrewVar( "Z29DuneslamDifficultySelectionInProgress" ) == 1 ) {
				if( event.actor != event.actor.getTeam().getLeader() ) {
					event.actor.centerPrint( "Your Crew Leader is already making a challenge level choice. One moment, please..." )
				}
			}
		}
		if( event.actor.getTeam().hasAreaVar( "Z29_Dune_Slam", "Z29DuneslamDifficulty" ) == true ) { 					
			event.actor.centerPrint( "You board the Row Boat and begin paddling toward the tiny island Jacques described." )
			event.actor.unsetQuestFlag(GLOBAL, "Z09_Duneslam_Allowed")
			event.actor.warp( "DuneSlam_1", 305, 310, 6 )	
			//********************* END DIFFICULTY SETTINGS ****************************
		}
	}
	
	myManager.schedule(1) { duneslamEnter.off() }
}

duneslamEnter.whenOn(goToDuneslam)

def synchronized makeMenu() {
	descripString = "You must choose a Challenge Level to enter Duneslam's Island. You have 30 seconds to decide."
	diffOptions = ["Easy (small rewards)", "Normal (moderate rewards)", "Hard (large rewards)", "Cancel"]
	
	//println "**** trying to create the menu ****"
	uiButtonMenu( leader, "diffMenu", descripString, diffOptions, 300, 30 ) { event ->
		if( event.selection == "Easy (small rewards)" ) {
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Easy' challenge setting for this scenario. Click the Row Boat again to enter." )
				} else {
					it.centerPrint( "You chose the 'Easy' challenge setting for this scenario. Click the Row Boat again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z29_Dune_Slam", "Z29DuneslamDifficulty", 1 )
			event.actor.setCrewVar( "Z29DuneslamDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Normal (moderate rewards)" ) {
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Normal' challenge setting for this scenario. Click the Row Boat again to enter." )
				} else {
					it.centerPrint( "You chose the 'Normal' challenge setting for this scenario. Click the Row Boat again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z29_Dune_Slam", "Z29DuneslamDifficulty", 2 )
			event.actor.setCrewVar( "Z29DuneslamDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Hard (large rewards)" ) {
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Hard' challenge setting for this scenario. Click the Row Boat again to enter." )
				} else {
					it.centerPrint( "You chose the 'Hard' challenge setting for this scenario. Click the Row Boat again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z29_Dune_Slam", "Z29DuneslamDifficulty", 3 )
			event.actor.setCrewVar( "Z29DuneslamDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Cancel" ) {
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose to not select a challenge setting at this time." )
				}
			}
			event.actor.setCrewVar("Z29DuneslamDifficultySelectionInProgress", 0)
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
	}
}