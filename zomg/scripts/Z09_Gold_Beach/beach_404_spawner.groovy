//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
def spawner1 = myRooms.Beach_404.spawnSpawner( "spawner_404_1", "sand_golem", 1)
spawner1.setPos( 160, 270 )
spawner1.setHome( "Beach_404", 160, 270 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 80, 100 )
spawner1.setGuardPostForChildren( spawner1, 120 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 6.7 )

// Spawner two
def spawner2 = myRooms.Beach_404.spawnSpawner( "spawner_404_2", "anchor_bug", 1)
spawner2.setPos( 825, 645 )
spawner2.setInitialMoveForChildren( "Beach_404", 340, 600 )
spawner2.setHome( "Beach_404", 340, 600 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 80, 100 )
spawner2.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 6.7 )

// Spawner three
def spawner3 = myRooms.Beach_404.spawnSpawner( "spawner_404_3", "anchor_bug", 1)
spawner3.setPos( 825, 645 )
spawner3.setInitialMoveForChildren( "Beach_404", 450, 520 )
spawner3.setHome( "Beach_404", 450, 520 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 80, 100 )
spawner3.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 6.7 )

// Spawner four
def spawner4 = myRooms.Beach_404.spawnSpawner( "spawner_404_4", "anchor_bug", 1)
spawner4.setPos( 825, 645 )
spawner4.setInitialMoveForChildren( "Beach_404", 750, 515 )
spawner4.setHome( "Beach_404", 750, 515 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 80, 100 )
spawner4.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 6.7 )

// Spawner five
def spawner5 = myRooms.Beach_404.spawnSpawner( "spawner_404_5", "anchor_bug", 1)
spawner5.setPos( 825, 645 )
spawner5.setInitialMoveForChildren( "Beach_404", 575, 550 )
spawner5.setHome( "Beach_404", 575, 550 )
spawner5.setSpawnWhenPlayersAreInRoom( true )
spawner5.setWaitTime( 80, 100 )
spawner5.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner5.childrenWander( true )
spawner5.setMonsterLevelForChildren( 6.7 )

// Spawner 6
def spawner6 = myRooms.Beach_404.spawnSpawner( "spawner_404_6", "sand_golem", 1)
spawner6.setPos( 410, 270 )
spawner6.setHome( "Beach_404", 410, 270 )
spawner6.setSpawnWhenPlayersAreInRoom( true )
spawner6.setWaitTime( 80, 100 )
spawner6.setGuardPostForChildren( spawner6, 30 )
spawner6.childrenWander( true )
spawner6.setMonsterLevelForChildren( 6.7 )

//Alliances
spawner1.allyWithSpawner( spawner6 )
spawner2.allyWithSpawner( spawner3 )
spawner4.allyWithSpawner( spawner5 )


//Initialization
spawner1.forceSpawnNow()
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()
spawner4.forceSpawnNow()
spawner5.forceSpawnNow()
spawner6.forceSpawnNow()