//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
spawner_505_1 = myRooms.Beach_505.spawnSpawner( "spawner_505_1", "water_spout", 1)
spawner_505_1.setPos( 230, 130 )
spawner_505_1.setHome( "Beach_505", 230, 130 )
spawner_505_1.setSpawnWhenPlayersAreInRoom( true )
spawner_505_1.setWaitTime( 30 , 40 )
spawner_505_1.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner_505_1.childrenWander( true )
spawner_505_1.setMonsterLevelForChildren( 6.5 )

// Spawner two
spawner_505_2 = myRooms.Beach_505.spawnSpawner( "spawner_505_2", "water_spout", 1)
spawner_505_2.setPos( 300, 530 )
spawner_505_2.setHome( "Beach_505", 300, 530 )
spawner_505_2.setSpawnWhenPlayersAreInRoom( true )
spawner_505_2.setWaitTime( 30 , 40 )
spawner_505_2.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner_505_2.childrenWander( true )
spawner_505_2.setMonsterLevelForChildren( 6.5 )

// Spawner three
spawner_505_3 = myRooms.Beach_505.spawnSpawner( "spawner_505_3", "water_spout", 1)
spawner_505_3.setPos( 315, 340 )
spawner_505_3.setHome( "Beach_505", 315, 340 )
spawner_505_3.setSpawnWhenPlayersAreInRoom( true )
spawner_505_3.setWaitTime( 30 , 40 )
spawner_505_3.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner_505_3.childrenWander( true )
spawner_505_3.setMonsterLevelForChildren( 6.5 )

spawner_505_1.stopSpawning()
spawner_505_2.stopSpawning()
spawner_505_3.stopSpawning()

//Alliances
spawner_505_1.allyWithSpawner( spawner_505_2 )
spawner_505_1.allyWithSpawner( spawner_505_3 )
spawner_505_2.allyWithSpawner( spawner_505_3 )

//Variable definition
spawnTime = false
spawnedSpout1 = false
spawnedSpout2 = false
spawnedSpout3 = false
spoutRespawning = false
spout_505_1 = null
spout_505_2 = null
spout_505_3 = null

def clockChecker() { 
	myManager.schedule(30) { spawnOrNoSpawn() } // ; //println "***** GST = ${ gst() } ******" }
}

def spawnChecker() {
	if( ( gst() > 1900 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 200 ) ) { //party goes from 7pm to 2am.
//	if( gst() > 0 ) { //This line for each setting of time parameters for testing.
		spawnTime = true
	} else {
		spawnTime = false
	}
}	

def checkForDespawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == false ) {
		clockChecker()
		if( spawnedSpout1 == true ) {
			if(spout_505_1.getHated().size() == 0) {
				spout_505_1.dispose()
				spawnedSpout1 = false
			}
		}
		if( spawnedSpout2 == true ) {
			if(spout_505_2.getHated().size() == 0) {
				spout_505_2.dispose()
				spawnedSpout2 = false
			}
		}
		if( spawnedSpout3 == true ) {
			if(spout_505_3.getHated().size() == 0) {
				spout_505_3.dispose()
				spawnedSpout3 = false
			}
		}
	}
	myManager.schedule(30) { checkForDespawn() }
}

def spawnOrNoSpawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == true ) {
		spoutRespawning = false
		if( spawnedSpout1 == false ) {
			spout_505_1 = spawner_505_1.forceSpawnNow()
			spawnedSpout1 = true
			runOnDeath( spout_505_1 ) { 
				spawnedSpout1 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout2 == false ) {
			spout_505_2 = spawner_505_2.forceSpawnNow()
			spawnedSpout2 = true
			runOnDeath( spout_505_2 ) { 
				spawnedSpout2 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout3 == false ) {
			spout_505_3 = spawner_505_3.forceSpawnNow()
			spawnedSpout3 = true
			runOnDeath( spout_505_3 ) { 
				spawnedSpout3 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
	}
}

//Initialization
clockChecker()
checkForDespawn()