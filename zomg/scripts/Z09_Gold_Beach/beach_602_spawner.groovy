import com.gaiaonline.mmo.battle.script.*

sandblastSpawner502 = myRooms.Beach_502.spawnStoppedSpawner( "sandblastSpawner502", "sand_golem_LT", 1)
sandblastSpawner502.setPos( 710, 540 )
sandblastSpawner502.setHome( "Beach_502", 710, 540 )
sandblastSpawner502.setGuardPostForChildren( sandblastSpawner502, 135 )
sandblastSpawner502.feudWithMonsterType( "p3" )
sandblastSpawner502.feudWithMonsterType( "p3_LT" )
sandblastSpawner502.setMonsterLevelForChildren( 6.5 )

sandblastSpawner602 = myRooms.Beach_602.spawnStoppedSpawner( "sandblastSpawner602", "sand_golem_LT", 1)
sandblastSpawner602.setPos( 930, 300 )
sandblastSpawner602.setHome( "Beach_602", 930, 300 )
sandblastSpawner602.setGuardPostForChildren( sandblastSpawner602, 225 )
sandblastSpawner602.feudWithMonsterType( "p3" )
sandblastSpawner602.feudWithMonsterType( "p3_LT" )
sandblastSpawner602.setMonsterLevelForChildren( 6.5 )

guardOne = myRooms.Beach_602.spawnStoppedSpawner( "guardOne", "sand_golem", 1)
guardOne.setPos( 665, 190 )
guardOne.setHome( "Beach_602", 665, 190 )
guardOne.setGuardPostForChildren( guardOne, 270 )
guardOne.feudWithMonsterType( "p3" )
guardOne.feudWithMonsterType( "p3_LT" )
guardOne.setMonsterLevelForChildren( 6.5 )

guardTwo = myRooms.Beach_502.spawnStoppedSpawner( "guardTwo", "sand_golem", 1)
guardTwo.setPos( 740, 585 )
guardTwo.setHome( "Beach_502", 740, 585 )
guardTwo.setGuardPostForChildren( guardTwo, 125 )
guardTwo.feudWithMonsterType( "p3" )
guardTwo.feudWithMonsterType( "p3_LT" )
guardTwo.setMonsterLevelForChildren( 6.5 )

//======duneSlamSafe======
safe602 = makeSwitch( "duneSlamSafe", myRooms.Beach_602, 940, 105 )
safe602.lock()
safe602.off()
safe602.setUsable( false )

hateCollector = [] as Set
playerSet602 = [] as Set
playerSet502 = [] as Set
sandblastSet = [] as Set
zone = 9 //Gold Beach

def openSafe602() {
	safe602.on()
	//look at every player that participated in fighting the guards and sandblast and give them rewards IF they are in rooms 602 or 502
	myRooms.Beach_602.getActorList().each { if( isPlayer( it ) ) { sandblastSet << it } }
	myRooms.Beach_502.getActorList().each { if( isPlayer( it ) ) { sandblastSet << it } }
	sandblastSet.each{
		if( hateCollector.contains( it ) && it.getConLevel() <= CLMap[zone]+1 ) {
			chestType = 3 //1 = basket; 2 = crate; 3 = chest; 4 = safe NOTE: This is just a chest now (not a safe)

			//scale the returned results down by relative con levels if the player is below the normal CL for the area
			if( it.getConLevel() > CLMap[zone] ) {
				lootMultiplier = 1.0
			} else {
				lootMultiplier = it.getConLevel() / CLMap[zone] //the lower the overall CL of a player in relation to the zone CL, the lower the reward they receive (always receive a reward appropriate to their level).
			}

			//grant gold
			goldGrant = goldMap[zone] 
			goldGrant = random( (goldGrant * 0.5).intValue(), goldGrant ) 
			it.grantCoins( ( goldGrant * lootMultiplier * chestType ).intValue() ) 

			//grant common items
			chance = (commonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				commonGrant = random( commonMap[zone] )
				it.grantItem( commonGrant )
			}

			//grant uncommon items
			chance = (uncommonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				uncommonGrant = random( uncommonMap[zone] )
				it.grantItem( uncommonGrant )
			}

			//grant recipes
			chance = (recipeChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				recipeGrant = random( recipeMap[zone] )
				it.grantItem( recipeGrant )
			}

			//grant orbs
			chance = (orbChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				orbGrant = orbMap[zone]
				orbGrant = ( random( (orbGrant * 0.5).intValue(), orbGrant ) * lootMultiplier * chestType ).intValue()
				it.grantQuantityItem( 100257, orbGrant )
			}

			//grant rings
			chance = (ringChance * chestType).intValue()
			roll = random( 1, 100 )
			if( roll <= chance ) {
				ringGrant = random( ringMap[zone] )
				it.grantRing( ringGrant, true )
			}
		} else if( hateCollector.contains( it ) && it.getConLevel() > CLMap[zone]+1 ) {
			it.centerPrint( "Your level is too high for loot from this chest. Change your level to below ${(CLMap[zone]+1).intValue() * 1.0} when fighting here." )
		} else if( !hateCollector.contains( it ) ) {
			it.centerPrint( "You did not help defeat Sandblast and his minions. No risk = no reward." )
		}
	}
	playerSet602.clear()
	playerSet502.clear()
	sandblastSet.clear()
	hateCollector.clear()
	//respawn the camp after a suitable delay
	myManager.schedule( random( 300, 500 ) ) {
		//close and reset the chest
		safe602.off()
		spawnGuards()
	}
}


//Respawn Logic
sandblast = null

def checkForSandblast() {
	//Don't do anything if both guards are not killed first
	if( guardOne.spawnsInUse() + guardTwo.spawnsInUse() == 0 ) {
		//Make sure Sandblast doesn't already exist
		if( sandblast == null || sandblast.isDead() ) {
			myManager.schedule(2) { 
				myRooms.Beach_602.getActorList().each { if( isPlayer( it ) ) { playerSet602 << it } }
				myRooms.Beach_502.getActorList().each { if( isPlayer( it ) ) { playerSet502 << it } }
				if( playerSet602.size() + playerSet502.size() > 0 ) {
					//If there are more players in 502 than 602, then spawn it there
					if( playerSet502.size() > playerSet602.size() ) {
						playerSet502.each{
							it.centerPrint( "The dunes around you slump together to form a gigantic sandy form!" )
						}
						sandblast = sandblastSpawner502.forceSpawnNow()
						sandblast.setDisplayName("Sandblast")
						sandblast.addHate( random( playerSet502 ) , 50 )
					//Otherwise, spawn it in the room with the safe
					} else {
						playerSet602.each{
							it.centerPrint( "The dunes around you slump together to form a gigantic sandy form!" )
						}
						sandblast = sandblastSpawner602.forceSpawnNow()
						sandblast.setDisplayName("Sandblast")
						sandblast.addHate( random( playerSet602 ) , 50 )
					}						
					runOnDeath( sandblast, { event ->
						event.actor.getHated().each{ if( isPlayer(it) ) { hateCollector << it } }
						myManager.schedule(2) { openSafe602() }
					} )
				//just reset the camp guards if the rooms are empty (instead of spawning Sandblast)
				} else {
					playerSet602.clear()
					playerSet502.clear()
					sandblastSet.clear()
					hateCollector.clear()
					myManager.schedule(30) { spawnGuards() }
				}
			}
		}
	} else {
		myManager.schedule(2) { checkForSandblast() }
	}
}	

//Alliances
guardOne.allyWithSpawner( guardTwo )

sandblastSpawner502.allyWithSpawner( guardOne )
sandblastSpawner502.allyWithSpawner( guardTwo )
sandblastSpawner602.allyWithSpawner( guardOne )
sandblastSpawner602.allyWithSpawner( guardTwo )


//Initialization
def spawnGuards() {
	guard1 = guardOne.forceSpawnNow()
	guard1.setDisplayName( "Beach Bully" )
	runOnDeath( guard1, { event -> event.actor.getHated().each{ if( isPlayer(it) ) { hateCollector << it } } } )
	guard2 = guardTwo.forceSpawnNow()
	guard2.setDisplayName( "98-Pound Weakling" )
	runOnDeath( guard2, { event -> event.actor.getHated().each{ if( isPlayer(it) ) { hateCollector << it } } } )
	checkForSandblast()
}

spawnGuards()

//====================================================================================
//====================================================================================
commonChance = 20 
uncommonChance = 10 
recipeChance = 1
orbChance = 5
ringChance = 1 

CLMap = [ 2:1, 3:2, 4:2.5, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:9, 14:1, 16:8, 18:7 ]

goldMap = [ 2:10, 3:20, 4:30, 5:40, 6:50, 7:60, 8:70, 9:80, 10:90, 11:100, 14:10, 16:90, 18:80 ]

commonMap = [ 2:["100272", "100289", "100385", "100297", "100397"], 3:["100291", "100262", "100263", "100298"], 4:["100388", "100278", "100275", "100283"], 5:["100281", "100367", "100411", "100394", "100284", "100405", "100267"], 6:["100265", "100285", "100398", "100397", "100376", "100296"], 7:["100373", "100260"], 8:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 9:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 11:["100371", "100294", "100391", "100277", "100290", "100368", "100386", "100403"], 14:[0], 16:["100287", "100409"], 18:["100393", "100293", "100259", "100292"] ]

uncommonMap = [ 2:["100280", "100279", "100270", "100380", "100384"], 3:["100378", "100261", "100271"], 4:["100276", "100258", "100268"], 5:["100381", "100382", "100282", "100383"], 6:["100390", "100299", "100286", "100369", "100400", "100387"], 7:["100365", "100410"], 8:["100389", "100264", "100406", "100399", "100402"], 9:["100389", "100264", "100406", "100399", "100402"], 10:["100395", "100413", "100407", "100266"], 11:["100396", "100269", "100401", "100372", "100375", "100366", "100379", "100274"], 14:[0], 16:["100404", "100288"], 18:["100395", "100413", "100407", "100266"] ]

recipeMap = [ 2:["17766", "17764", "17772", "17758", "17756"], 3:["17861", "17857", "17755"], 4:["17848", "17849", "17852", "17851", "17850", "17753"], 5:["17833", "17831", "17754", "17836", "17835"], 6:["17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779"], 7:["17785", "17793", "17788", "17787"], 8:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 9:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 10:["17796", "17846", "17803", "17844"], 11:["17819", "17816", "17811", "17808", "17806", "17812", "17813", "17814", "17810", "17809", "17815"], 14:[0], 16:["17786", "17791", "17790", "17794"], 18:["17796", "17846", "17803", "17844"] ]

orbMap = [ 2:1, 3:2, 4:3, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:9, 14:1, 16:8, 18:7 ]

ringMap = [ 2:["17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 3:["17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 4:["17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 5:["17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 6:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 7:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 8:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 9:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 10:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 11:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 14:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 16:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 18:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"] ]

//====================================================================================
//====================================================================================

