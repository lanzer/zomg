//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
spawner_304_1 = myRooms.Beach_304.spawnSpawner( "spawner_304_1", "water_spout", 1)
spawner_304_1.setPos( 825, 480 )
spawner_304_1.setHome( "Beach_304", 825, 480 )
spawner_304_1.setSpawnWhenPlayersAreInRoom( true )
spawner_304_1.setWaitTime( 30 , 40 )
spawner_304_1.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner_304_1.childrenWander( true )
spawner_304_1.setMonsterLevelForChildren( 6.8 )

// Spawner two
spawner_304_2 = myRooms.Beach_304.spawnSpawner( "spawner_304_2", "water_spout", 1)
spawner_304_2.setPos( 870, 125 )
spawner_304_2.setHome( "Beach_304", 875, 125 )
spawner_304_2.setSpawnWhenPlayersAreInRoom( true )
spawner_304_2.setWaitTime( 30 , 40 )
spawner_304_2.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner_304_2.childrenWander( true )
spawner_304_2.setMonsterLevelForChildren( 6.8 )

// Spawner three
def spawner3 = myRooms.Beach_304.spawnSpawner( "spawner_304_3", "anchor_bug", 1)
spawner3.setPos( 210, 400 )
spawner3.setHome( "Beach_304", 210, 400 )
spawner3.setSpawnWhenPlayersAreInRoom( false )
spawner3.setWaitTime( 30 , 40 )
spawner3.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 6.8 )

// Spawner four
def spawner4 = myRooms.Beach_304.spawnSpawner( "spawner_304_4", "anchor_bug", 1)
spawner4.setPos( 100, 340 )
spawner4.setHome( "Beach_304", 100, 340 )
spawner4.setSpawnWhenPlayersAreInRoom( false )
spawner4.setWaitTime( 30 , 40 )
spawner4.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 6.8 )

// Spawner five
def spawner5 = myRooms.Beach_304.spawnSpawner( "spawner_304_5", "anchor_bug", 1)
spawner5.setPos( 175, 605 )
spawner5.setHome( "Beach_304", 175, 605 )
spawner5.setSpawnWhenPlayersAreInRoom( false )
spawner5.setWaitTime( 30 , 40 )
spawner5.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner5.childrenWander( true )
spawner5.setMonsterLevelForChildren( 6.8 )

// Spawner six
def spawner6 = myRooms.Beach_304.spawnSpawner( "spawner_304_6", "anchor_bug", 1)
spawner6.setPos( 460, 580 )
spawner6.setHome( "Beach_304", 460, 580 )
spawner6.setSpawnWhenPlayersAreInRoom( false )
spawner6.setWaitTime( 30 , 40 )
spawner6.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner6.childrenWander( true )
spawner6.setMonsterLevelForChildren( 6.8 )

spawner_304_1.stopSpawning()
spawner_304_2.stopSpawning()

//Alliances
spawner_304_1.allyWithSpawner( spawner_304_2 )
spawner_304_1.allyWithSpawner( spawner3 )
spawner_304_2.allyWithSpawner( spawner3 )

//Variable definition
spawnTime = false
spawnedSpout1 = false
spawnedSpout2 = false
spoutRespawning = false
spout_304_1 = null
spout_304_2 = null

def clockChecker() { 
	myManager.schedule(30) { spawnOrNoSpawn() } // ; //println "***** GST = ${ gst() } ******" }
}

def spawnChecker() {
	if( ( gst() > 1900 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 200 ) ) { //party goes from 7pm to 2am.
//	if( gst() > 0 ) { //This line for each setting of time parameters for testing.
		spawnTime = true
	} else {
		spawnTime = false
	}
}	

def checkForDespawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == false ) {
		clockChecker()
		if( spawnedSpout1 == true ) {
			if(spout_304_1.getHated().size() == 0) {
				spout_304_1.dispose()
				spawnedSpout1 = false
			}
		}
		if( spawnedSpout2 == true ) {
			if(spout_304_2.getHated().size() == 0) {
				spout_304_2.dispose()
				spawnedSpout2 = false
			}
		}
	}
	myManager.schedule(30) { checkForDespawn() }
}

def spawnOrNoSpawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == true ) {
		spoutRespawning = false
		if( spawnedSpout1 == false ) {
			spout_304_1 = spawner_304_1.forceSpawnNow()
			spawnedSpout1 = true
			runOnDeath( spout_304_1 ) { 
				spawnedSpout1 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout2 == false ) {
			spout_304_2 = spawner_304_2.forceSpawnNow()
			spawnedSpout2 = true
			runOnDeath( spout_304_2 ) { 
				spawnedSpout2 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
	}
}

//Alliances
spawner3.allyWithSpawner( spawner4 )
spawner3.allyWithSpawner( spawner5 )
spawner3.allyWithSpawner( spawner6 )
spawner4.allyWithSpawner( spawner5 )
spawner4.allyWithSpawner( spawner6 )
spawner5.allyWithSpawner( spawner6 )

//Initialization
clockChecker()
checkForDespawn()
spawner3.forceSpawnNow()
spawner4.forceSpawnNow()
spawner5.forceSpawnNow()
spawner6.forceSpawnNow()