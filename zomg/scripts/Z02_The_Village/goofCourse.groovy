

import com.gaiaonline.mmo.battle.script.*;

//Add a map marker when the script starts up and never turn it off
//addMiniMapMarker("goofTee", "markerOther", "VILLAGE_205", 925, 175, "Goof Course Start")

def Mac = spawnNPC( "MacTaggert-VQS", myRooms.VILLAGE_205, 925, 175 )
Mac.setRotation( 135 )
Mac.setDisplayName( "MacTaggert" )

/*

myManager.onEnter( myRooms.VILLAGE_205 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z02CameFromIntroPartOne" )
		event.actor.unsetQuestFlag( GLOBAL, "Z02ViewingGoofTeeMenus" )
	}
}

def goofCourseIntro = Mac.createConversation( "goofCourseIntro", true, "!Z02CameFromIntroPartOne", "!Z02UseThisConvoFromNowOn" )

def intro1 = [id:1]
intro1.npctext = "Struth! Tis a bonny mornin', is innae? Mah name is MacTaggert and ahm here t'tell ye all y'need to know about the glorious game o'Goof!"
intro1.options = []
intro1.options << [text:"Thanks, MacTaggert. I'd like to hear more!", result: 2]
intro1.options << [text:"Oops. Nevermind. I'll talk to you later.", result: DONE]
goofCourseIntro.addDialog( intro1, Mac )

def intro2 = [id:2]
intro2.npctext = "Of course y'would! This game is th' finest ever made. Y'whack the ball toward each o'nine markers, each one in turn. Y'kin play fer strokes or fer speed, whichever y'prefer."
intro2.playertext = "For strokes or for speed...gotcha."
intro2.result = 3
goofCourseIntro.addDialog( intro2, Mac )

def intro3 = [id:3]
intro3.npctext = "But, ach...I've n'got time to babysit ye all day. Jest read the pamphlet. When yer done, touch th'great goof tee to create yer goofball n'give it a bonnie whack!"
intro3.playertext = "Oooookay. So you don't like explaining things, I get it. I'll read the pamphlet!"
intro3.exec = { event ->
	event.player.setQuestFlag( GLOBAL, "Z02CameFromIntroPartOne" )
	tutorialNPC.pushDialog( event.player, "goofTutorial" )
}
intro3.result = DONE
goofCourseIntro.addDialog( intro3, Mac )

//Tutorials
def goofTutorial = tutorialNPC.createConversation( "goofTutorial", true )

def goof1 = [id:1]
goof1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/goofballInstruct1.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
goof1.result = 2
goofTutorial.addDialog( goof1, tutorialNPC )

def goof2 = [id:2]
goof2.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/goofballInstruct4.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
goof2.result = 3
goofTutorial.addDialog( goof2, tutorialNPC )

def goof3 = [id:3]
goof3.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/goofballInstruct3.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
goof3.result = 4
goofTutorial.addDialog( goof3, tutorialNPC )

def goof4 = [id:4]
goof4.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/goofballInstruct2.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
goof4.exec = { event ->
	if( event.player.hasQuestFlag( GLOBAL, "Z02CameFromIntroPartOne" ) ) {
		event.player.unsetQuestFlag( GLOBAL, "Z02CameFromIntroPartOne" )
		event.player.setQuestFlag( GLOBAL, "Z02UseThisConvoFromNowOn" )
		Mac.pushDialog( event.player, "goofCourseIntro2" )
	} else {
		if( event.actor.getRoom() == myRooms.VILLAGE_205 ) {
			player = event.player
			makeTeeMainMenu( player )
		}
	}
}
goof4.result = DONE
goofTutorial.addDialog( goof4, tutorialNPC )

//Second part of convo after the tutorials
def goofCourseIntro2 = Mac.createConversation( "goofCourseIntro2", true, "Z02UseThisConvoFromNowOn" )

def introEnd1 = [id:1]
introEnd1.npctext = "If ye make or beat Par for the course TEN times...nae nine...nae eight...but TEN, then I'll make ye an honorary greenskeeper and give y'yer own lawn mower!"
introEnd1.playertext = "My very own lawn mower? How...generous."
introEnd1.result = 2
goofCourseIntro2.addDialog( introEnd1, Mac )

def introEnd2 = [id:2]
introEnd2.npctext = "Faugh! I'm nae generous! Y'take that foul insult 'n stick it wher it's dark n'moist. Ye'll hae EARNED that mower...or y'won't get it!"
introEnd2.playertext = "Okay, okay. I'll earn it. Thanks, MacTaggert."
introEnd2.result = DONE
goofCourseIntro2.addDialog( introEnd2, Mac )

//Tips and Hints for the First Tee menus (below)
def tipsHints = tutorialNPC.createConversation( "tipsHints", true )

def hint1 = [id:1]
hint1.npctext = "<zOMG dialogWidth='300'><![CDATA[<h1><b><font face='Arial' size='14'>Tips and Hints</font></b></h1><font face='Arial' size ='12'><br>There are a number of little things you can do to help you do better with Goof:<br><br>* Make sure your PDA Map is open. Judge the angle you need to hit the ball by imagining a line between your gold dot on the map, and the flag icon.<br><br>* Grab a goof ball, but don't worry about playing the course. Just practice knocking the ball different distances. Once you get the hang for that, you'll do better hole-to-hole.<br><br>* Walk ahead and scout the terrain. Nothing cuts into your score faster than when you don't see the tree ahead and your goof ball bounces off it, heading in a direction you didn't desire.<br><br>]]></zOMG>"
hint1.exec = { event ->
	if( event.actor.getRoom() == myRooms.VILLAGE_205 ) {
		player = event.actor
		makeTeeMainMenu( player )
	}
}
hint1.result = 4
tipsHints.addDialog( hint1, tutorialNPC )


//===========================================
// FIRST TEE (goofball creation in Rm 205)   
//===========================================

firstTee = makeSwitch( "golfTee", myRooms.VILLAGE_205, 750, 150 )
firstTee.off()
firstTee.unlock()
firstTee.setRange( 200 )
firstTee.setMouseoverText("Click for the Goof Game!")

teeBlocker = new Object()

def teeTalk = { event ->
	firstTee.off()	
	synchronized( teeBlocker ) {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z02ViewingGoofTeeMenus" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z02ViewingGoofTeeMenus" )
			player = event.actor
			makeTeeMainMenu( player )
		}
	}
}

firstTee.whenOn( teeTalk )

def synchronized makeTeeMainMenu( player ) {
	titleString = "You and the Game of Goof!"
	descripString = "Start a game, read the pamphlet about the game, or check out the Best Scores for the course!"
	diffOptions = [ "Start a Game of Goof!", "The Goof Pamphlet", "Tips and Hints", "Best Scores", "Cancel" ]
	
	uiButtonMenu( player, "teeMenu", titleString, descripString, diffOptions, 300 ) { event ->
		event.actor.setQuestFlag( GLOBAL, "Z02ViewingGoofTeeMenus" )
		if( event.selection == "Start a Game of Goof!" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02ViewingGoofTeeMenus" )
			startGame( event )			
		}
		if( event.selection == "The Goof Pamphlet" ) {
			tutorialNPC.pushDialog( event.actor, "goofTutorial" )
		}
		if( event.selection == "Tips and Hints" ) {
			tutorialNPC.pushDialog( event.actor, "tipsHints" )
		}
		if( event.selection == "Best Scores" ) {
			makeHighScoreMenu( player )
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02ViewingGoofTeeMenus" )
		}
	}
}

def makeHighScoreMenu( player ) {
	titleString = "Goof Course Best Scores"
	descripString = "The two ways to set a best mark in Goof are either by finishing with the fewest strokes, or finishing with the fastest time from tee to the ninth mark."
	diffOptions = [ "Lowest Number of Strokes", "Fastest Course Time", "Main Menu" ]
	
	uiButtonMenu( player, "highScoreMenu", titleString, descripString, diffOptions, 300 ) { event ->
		if( event.selection == "Lowest Number of Strokes" ) {
			if( lowScoreHolder == null ) {
				event.actor.centerPrint( "No course record has been set yet. Be the first!" )
			} else {
				event.actor.centerPrint( "The Lowest Score is ${currentLowScore} set by ${lowScoreHolder}!" )
			}
			myManager.schedule(2) { player = event.actor; makeHighScoreMenu( player ) }
		}
		if( event.selection == "Fastest Course Time" ) {
			if( lowScoreHolder == null ) {
				event.actor.centerPrint( "No course record has been set yet. Be the first!" )
			} else {
				event.actor.centerPrint( "The Fastest Time is ${(currentLowTime/60).intValue()} minutes, ${(currentLowTime%60).intValue()} seconds set by ${lowTimeHolder}!" )
			}
			myManager.schedule(2) { player = event.actor; makeHighScoreMenu( player ) }
		}
		if( event.selection == "Main Menu" ) {
			makeTeeMainMenu( player )
		}
	}
}

def greetingMessages( event ) {
	//inform the player of the course records and what they need to do to beat them
	event.actor.centerPrint( "Par for the course is ${ coursePar }." )
	if( event.actor.getPlayerVar( "Z02CheckOnExitMessageAlreadySeen" ) == 0 && event.actor.getPlayerVar( "Z02OnTimeoutMessageAlreadySeen" ) == 0 ) {
		firstDelay = myManager.schedule(3) {
			event.actor.centerPrint( "Par for the first marker is ${ parMap[1] }." )
			if( event.actor.getPlayerVar( "Z02CheckOnExitMessageAlreadySeen" ) == 0 && event.actor.getPlayerVar( "Z02OnTimeoutMessageAlreadySeen" ) == 0 ) {
				secondDelay = myManager.schedule(2) { event.actor.centerPrint( "Good luck!" ) }
			}
		}
	}
}	

def removeFlagsOrdinary( event ) { 
	event.actor.removeMiniMapQuestLocation( "flag1" )
	event.actor.removeMiniMapQuestLocation( "flag2" )
	event.actor.removeMiniMapQuestLocation( "flag3" )
	event.actor.removeMiniMapQuestLocation( "flag4" )
	event.actor.removeMiniMapQuestLocation( "flag5" )
	event.actor.removeMiniMapQuestLocation( "flag6" )
	event.actor.removeMiniMapQuestLocation( "flag7" )
	event.actor.removeMiniMapQuestLocation( "flag8" )
	event.actor.removeMiniMapQuestLocation( "flag9" )
}	

def removeFlags( event ) {
	event.actor.ballOwner().removeMiniMapQuestLocation( "flag1" )
	event.actor.ballOwner().removeMiniMapQuestLocation( "flag2" )
	event.actor.ballOwner().removeMiniMapQuestLocation( "flag3" )
	event.actor.ballOwner().removeMiniMapQuestLocation( "flag4" )
	event.actor.ballOwner().removeMiniMapQuestLocation( "flag5" )
	event.actor.ballOwner().removeMiniMapQuestLocation( "flag6" )
	event.actor.ballOwner().removeMiniMapQuestLocation( "flag7" )
	event.actor.ballOwner().removeMiniMapQuestLocation( "flag8" )
	event.actor.ballOwner().removeMiniMapQuestLocation( "flag9" )
}


def startGame( event ) {
	
	//spawn the goofball
	def goofball = makeGoofBall(myRooms.VILLAGE_205, 746, 130, event.actor)
	event.actor.setPlayerVar( "Z02CheckOnExitMessageAlreadySeen", 0 )
	event.actor.setPlayerVar( "Z02OnTimeoutMessageAlreadySeen", 0 )

	//if the player leaves the start room without hitting the ball, dispose of it
	def checkOnExit = {
		println "**** hitCount = ${goofball.getHitCount()} ****"
		if( goofball.getHitCount() == 0 ) {
			if( event.actor.getPlayerVar( "Z02CheckOnExitMessageAlreadySeen" ) == 0 ) {
				event.actor.setPlayerVar( "Z02CheckOnExitMessageAlreadySeen", 1 )
				removeFlagsOrdinary( event )
				goofball.getOwnerPlayer().disposeGoofBall()
				event.actor.centerPrint( "Your Goofball was removed because you left the Tee area without hitting it." )
				firstDelay.cancel()
				secondDelay.cancel()
			}
		}
	}
	runOnExit( myRooms.VILLAGE_205, event.actor, checkOnExit )

	//if the player doesn't hit the ball for more than two minutes, then time the ball out and let the player know about its removal
	goofball.onTimeout( event.actor ) { 
		if( event.actor.getPlayerVar( "Z02OnTimeoutMessageAlreadySeen" ) == 0 ) {
			event.actor.setPlayerVar( "Z02OnTimeoutMessageAlreadySeen", 1 )
			removeFlagsOrdinary( event )
			event.actor.centerPrint( "Your goofball goes away due to inactivity. Click the tee to start the game again." )
		}
	}
	
	//initialize some player variables that will be useful later on
	event.actor.setPlayerVar( "lastScore", 0 ) //used for determining how many strokes used on currently scored hole
	event.actor.setPlayerVar( "currentFlag", 1 ) //what flag is the player currently shooting for?
	event.actor.removeMiniMapQuestLocation( "flag1" )
	event.actor.removeMiniMapQuestLocation( "flag2" )
	event.actor.removeMiniMapQuestLocation( "flag3" )
	event.actor.removeMiniMapQuestLocation( "flag4" )
	event.actor.removeMiniMapQuestLocation( "flag5" )
	event.actor.removeMiniMapQuestLocation( "flag6" )
	event.actor.removeMiniMapQuestLocation( "flag7" )
	event.actor.removeMiniMapQuestLocation( "flag8" )
	event.actor.removeMiniMapQuestLocation( "flag9" )
	event.actor.setPlayerVar( "Z02GoofCourseTotalGold", 0 )
	event.actor.setPlayerVar( "Z02GoofCourseTotalOrbs", 0 )
	
	//set the location of the first flag on the player's mini-map
//	curFlag = event.actor.getPlayerVar( "currentFlag" )
//	println "**** Current Flag Player is Shoot for = ${ curFlag } ****"
	event.actor.addMiniMapQuestLocation( "flag1", "VILLAGE_306", 300, 310, "Marker 1" )
	
	greetingMessages( event )
	
}

//===========================================
// FLAG LOCATIONS                            
//===========================================

//Flag 1
flag1 = makeFlag( "golfmarker1", myRooms.VILLAGE_306, 300, 310 )
flag1.setDisplayName( "Marker 1" )
flag2 = makeFlag( "golfmarker2", myRooms.VILLAGE_304, 310, 365 )
flag2.setDisplayName( "Marker 2" )
flag3 = makeFlag( "golfmarker3", myRooms.VILLAGE_104, 420, 530 )
flag3.setDisplayName( "Marker 3" )
flag4 = makeFlag( "golfmarker4", myRooms.VILLAGE_5, 675, 450 )
flag4.setDisplayName( "Marker 4" )
flag5 = makeFlag( "golfmarker5", myRooms.VILLAGE_103, 705, 300 )
flag5.setDisplayName( "Marker 5" )
flag6 = makeFlag( "golfmarker6", myRooms.VILLAGE_202, 825, 590 )
flag6.setDisplayName( "Marker 6" )
flag7 = makeFlag( "golfmarker7", myRooms.VILLAGE_401, 670, 215 )
flag7.setDisplayName( "Marker 7" )
flag8 = makeFlag( "golfmarker8", myRooms.VILLAGE_201, 480, 600 )
flag8.setDisplayName( "Marker 8" )
flag9 = makeFlag( "genericflag", myRooms.VILLAGE_402, 820, 490 )
flag9.setDisplayName( "Marker 9" )

//PAR RECORDS
parMap = [1:4, 2:4, 3:4, 4:3, 5:5, 6:5, 7:4, 8:4, 9:4] //flag# / par

coursePar = parMap[1] + parMap[2] + parMap[3] + parMap[4] + parMap[5] + parMap[6] + parMap[7] + parMap[8] + parMap[9]
//println "**** PAR FOR THE COURSE = ${coursePar} ****"

//===========================================
// SET UP LOGIC FOR FLAG AWARENESSES         
//===========================================

flagSayings = [ "Hi there, Goofer!", "Don't fall in!", "Be the ball, Danny...be the ball.", "na-na-na-na-na-na", "Gunga galunga...gunga, gunga-galunga.", "You can DO eet!", "Greatness courts failure." ]

//TODO: Reward the player with gold and orbs each hole. Compare their score for that hole against par and reward accordingly.

//Flag 1
myManager.onAwarenessIn( flag1 ) { event ->
	if( isGoofBall( event.actor ) ) {
		flag1.say( "I see a Goofball!" )
//		curFlag = event.actor.ballOwner().getPlayerVar( "currentFlag" )
//		println "****Current Flag = ${curFlag} ****"
		if( event.actor.ballOwner().getPlayerVar( "currentFlag" ) == 1 ) { //if the player is currently aiming for this hole
			
			//tell the player their score for the hole and then reset the "lastScore" variable for next round.
			
			sound("flag1Cupin").toPlayer( event.actor.ballOwner() )
			myManager.schedule(0.5) { sound("flag1Sound").toPlayer( event.actor.ballOwner() ) }
			event.actor.ballOwner().setPlayerVar( "currentHoleScore", event.actor.getHitCount() - event.actor.ballOwner().getPlayerVar( "lastScore" ) )
			//next line just creates a temp variable to make the centerPrint below it easier to handle
			curScore = event.actor.ballOwner().getPlayerVar( "currentHoleScore" ).intValue()
			myManager.schedule(1) { event.actor.ballOwner().centerPrint( "Congrats! It's in the hole! Your score for this hole is ${ curScore }." ) } 
			myManager.schedule(1) { event.actor.ballOwner().setPlayerVar( "lastScore", event.actor.getHitCount() ) }
			//check currentHoleScore against the mapPar and tell the player how they did
			myManager.schedule(3) { checkPar( event ) }
		}
	}
	if( isPlayer( event.actor ) ) { 
		flag1.say( random( flagSayings ) )
	}
}

//Flag 2
myManager.onAwarenessIn( flag2 ) { event ->
	if( isGoofBall( event.actor ) ) {
		flag2.say( "I see a Goofball!" )

		if( event.actor.ballOwner().getPlayerVar( "currentFlag" ) == 2 ) { //if the player is currently aiming for this hole
			
			//tell the player their score for the hole and then reset the "lastScore" variable for next round.
			
			sound("flag2Cupin").toPlayer( event.actor.ballOwner() )
			myManager.schedule(0.5) { sound("flag2Sound").toPlayer( event.actor.ballOwner() ) }
			event.actor.ballOwner().setPlayerVar( "currentHoleScore", event.actor.getHitCount() - event.actor.ballOwner().getPlayerVar( "lastScore" ) )
			//next line just creates a temp variable to make the centerPrint below it easier to handle
			curScore = event.actor.ballOwner().getPlayerVar( "currentHoleScore" ).intValue()
			myManager.schedule(1) { event.actor.ballOwner().centerPrint( "Congrats! It's in the hole! Your score for this hole is ${ curScore }." ) } 
			myManager.schedule(1) { event.actor.ballOwner().setPlayerVar( "lastScore", event.actor.getHitCount() ) }
			//check currentHoleScore against the mapPar and tell the player how they did
			myManager.schedule(3) { checkPar( event ) }
		}
	}
	if( isPlayer( event.actor ) ) { 
		flag2.say( random( flagSayings ) )
	}
}
//Flag 3
myManager.onAwarenessIn( flag3 ) { event ->
	if( isGoofBall( event.actor ) ) {
		flag3.say( "I see a Goofball!" )

		if( event.actor.ballOwner().getPlayerVar( "currentFlag" ) == 3 ) { //if the player is currently aiming for this hole
			
			//tell the player their score for the hole and then reset the "lastScore" variable for next round.
			
			sound("flag3Cupin").toPlayer( event.actor.ballOwner() )
			myManager.schedule(0.5) { sound("flag3Sound").toPlayer( event.actor.ballOwner() ) }
			event.actor.ballOwner().setPlayerVar( "currentHoleScore", event.actor.getHitCount() - event.actor.ballOwner().getPlayerVar( "lastScore" ) )
			//next line just creates a temp variable to make the centerPrint below it easier to handle
			curScore = event.actor.ballOwner().getPlayerVar( "currentHoleScore" ).intValue()
			myManager.schedule(1) { event.actor.ballOwner().centerPrint( "Congrats! It's in the hole! Your score for this hole is ${ curScore }." ) } 
			myManager.schedule(1) { event.actor.ballOwner().setPlayerVar( "lastScore", event.actor.getHitCount() ) }
			//check currentHoleScore against the mapPar and tell the player how they did
			myManager.schedule(3) { checkPar( event ) }
		}
	}
	if( isPlayer( event.actor ) ) { 
		flag3.say( random( flagSayings ) )
	}
}
//Flag 4
myManager.onAwarenessIn( flag4 ) { event ->
	if( isGoofBall( event.actor ) ) {
		flag4.say( "I see a Goofball!" )

		if( event.actor.ballOwner().getPlayerVar( "currentFlag" ) == 4 ) { //if the player is currently aiming for this hole
			
			//tell the player their score for the hole and then reset the "lastScore" variable for next round.
			
			sound("flag4Cupin").toPlayer( event.actor.ballOwner() )
			myManager.schedule(0.5) { sound("flag4Sound").toPlayer( event.actor.ballOwner() ) }
			event.actor.ballOwner().setPlayerVar( "currentHoleScore", event.actor.getHitCount() - event.actor.ballOwner().getPlayerVar( "lastScore" ) )
			//next line just creates a temp variable to make the centerPrint below it easier to handle
			curScore = event.actor.ballOwner().getPlayerVar( "currentHoleScore" ).intValue()
			myManager.schedule(1) { event.actor.ballOwner().centerPrint( "Congrats! It's in the hole! Your score for this hole is ${ curScore }." ) } 
			myManager.schedule(1) { event.actor.ballOwner().setPlayerVar( "lastScore", event.actor.getHitCount() ) }
			//check currentHoleScore against the mapPar and tell the player how they did
			myManager.schedule(3) { checkPar( event ) }
		}
	}
	if( isPlayer( event.actor ) ) { 
		flag4.say( random( flagSayings ) )
	}
}
//Flag 5
myManager.onAwarenessIn( flag5 ) { event ->
	if( isGoofBall( event.actor ) ) {
		flag5.say( "I see a Goofball!" )

		if( event.actor.ballOwner().getPlayerVar( "currentFlag" ) == 5 ) { //if the player is currently aiming for this hole
			
			//tell the player their score for the hole and then reset the "lastScore" variable for next round.
			
			sound("flag5Cupin").toPlayer( event.actor.ballOwner() )
			myManager.schedule(0.5) { sound("flag5Sound").toPlayer( event.actor.ballOwner() ) }
			event.actor.ballOwner().setPlayerVar( "currentHoleScore", event.actor.getHitCount() - event.actor.ballOwner().getPlayerVar( "lastScore" ) )
			//next line just creates a temp variable to make the centerPrint below it easier to handle
			curScore = event.actor.ballOwner().getPlayerVar( "currentHoleScore" ).intValue()
			myManager.schedule(1) { event.actor.ballOwner().centerPrint( "Congrats! It's in the hole! Your score for this hole is ${ curScore }." ) } 
			myManager.schedule(1) { event.actor.ballOwner().setPlayerVar( "lastScore", event.actor.getHitCount() ) }
			//check currentHoleScore against the mapPar and tell the player how they did
			myManager.schedule(3) { checkPar( event ) }
		}
	}
	if( isPlayer( event.actor ) ) { 
		flag5.say( random( flagSayings ) )
	}
}
//Flag 6
myManager.onAwarenessIn( flag6 ) { event ->
	if( isGoofBall( event.actor ) ) {
		flag6.say( "I see a Goofball!" )

		if( event.actor.ballOwner().getPlayerVar( "currentFlag" ) == 6 ) { //if the player is currently aiming for this hole
			
			//tell the player their score for the hole and then reset the "lastScore" variable for next round.
			
			sound("flag6Cupin").toPlayer( event.actor.ballOwner() )
			myManager.schedule(0.5) { sound("flag6Sound").toPlayer( event.actor.ballOwner() ) }
			event.actor.ballOwner().setPlayerVar( "currentHoleScore", event.actor.getHitCount() - event.actor.ballOwner().getPlayerVar( "lastScore" ) )
			//next line just creates a temp variable to make the centerPrint below it easier to handle
			curScore = event.actor.ballOwner().getPlayerVar( "currentHoleScore" ).intValue()
			myManager.schedule(1) { event.actor.ballOwner().centerPrint( "Congrats! It's in the hole! Your score for this hole is ${ curScore }." ) } 
			myManager.schedule(1) { event.actor.ballOwner().setPlayerVar( "lastScore", event.actor.getHitCount() ) }
			//check currentHoleScore against the mapPar and tell the player how they did
			myManager.schedule(3) { checkPar( event ) }
		}
	}
	if( isPlayer( event.actor ) ) { 
		flag6.say( random( flagSayings ) )
	}
}
//Flag 7
myManager.onAwarenessIn( flag7 ) { event ->
	if( isGoofBall( event.actor ) ) {
		flag7.say( "I see a Goofball!" )

		if( event.actor.ballOwner().getPlayerVar( "currentFlag" ) == 7 ) { //if the player is currently aiming for this hole
			
			//tell the player their score for the hole and then reset the "lastScore" variable for next round.
			
			sound("flag7Cupin").toPlayer( event.actor.ballOwner() )
			myManager.schedule(0.5) { sound("flag7Sound").toPlayer( event.actor.ballOwner() ) }
			event.actor.ballOwner().setPlayerVar( "currentHoleScore", event.actor.getHitCount() - event.actor.ballOwner().getPlayerVar( "lastScore" ) )
			//next line just creates a temp variable to make the centerPrint below it easier to handle
			curScore = event.actor.ballOwner().getPlayerVar( "currentHoleScore" ).intValue()
			myManager.schedule(1) { event.actor.ballOwner().centerPrint( "Congrats! It's in the hole! Your score for this hole is ${ curScore }." ) } 
			myManager.schedule(1) { event.actor.ballOwner().setPlayerVar( "lastScore", event.actor.getHitCount() ) }
			//check currentHoleScore against the mapPar and tell the player how they did
			myManager.schedule(3) { checkPar( event ) }
		}
	}
	if( isPlayer( event.actor ) ) { 
		flag7.say( random( flagSayings ) )
	}
}
//Flag 8
myManager.onAwarenessIn( flag8 ) { event ->
	if( isGoofBall( event.actor ) ) {
		flag8.say( "I see a Goofball!" )

		if( event.actor.ballOwner().getPlayerVar( "currentFlag" ) == 8 ) { //if the player is currently aiming for this hole
			
			//tell the player their score for the hole and then reset the "lastScore" variable for next round.
			
			sound("flag8Cupin").toPlayer( event.actor.ballOwner() )
			myManager.schedule(0.5) { sound("flag8Sound").toPlayer( event.actor.ballOwner() ) }
			event.actor.ballOwner().setPlayerVar( "currentHoleScore", ( event.actor.getHitCount() - event.actor.ballOwner().getPlayerVar( "lastScore" ) ).intValue() )
			//next line just creates a temp variable to make the centerPrint below it easier to handle
			curScore = event.actor.ballOwner().getPlayerVar( "currentHoleScore" ).intValue()
			myManager.schedule(1) { event.actor.ballOwner().centerPrint( "Congrats! It's in the hole! Your score for this hole is ${ curScore }." ) } 
			myManager.schedule(1) { event.actor.ballOwner().setPlayerVar( "lastScore", event.actor.getHitCount() ) }
			//check currentHoleScore against the mapPar and tell the player how they did
			myManager.schedule(3) { checkPar( event ) }
		}
	}
	if( isPlayer( event.actor ) ) { 
		flag8.say( random( flagSayings ) )
	}
}
//Flag 9
myManager.onAwarenessIn( flag9 ) { event ->
	if( isGoofBall( event.actor ) ) {
		flag9.say( "I see a Goofball!" )

		if( event.actor.ballOwner().getPlayerVar( "currentFlag" ) == 9 ) { //if the player is currently aiming for this hole
			
			//tell the player their score for the hole and then reset the "lastScore" variable for next round.
			
			sound("flag9Cupin").toPlayer( event.actor.ballOwner() )
			myManager.schedule(0.5) { sound("flag9Sound").toPlayer( event.actor.ballOwner() ) }
			event.actor.ballOwner().setPlayerVar( "currentHoleScore", event.actor.getHitCount() - event.actor.ballOwner().getPlayerVar( "lastScore" ) )
			//next line just creates a temp variable to make the centerPrint below it easier to handle
			curScore = event.actor.ballOwner().getPlayerVar( "currentHoleScore" ).intValue()
			myManager.schedule(1) { event.actor.ballOwner().centerPrint( "Congrats! It's in the hole! Your score for this hole is ${ curScore }." ) } 
			myManager.schedule(1) { event.actor.ballOwner().setPlayerVar( "lastScore", event.actor.getHitCount() ) }
			//check currentHoleScore against the mapPar and tell the player how they did
			myManager.schedule(3) { checkPar( event ) }
		}
	}
	if( isPlayer( event.actor ) ) { 
		flag9.say( random( flagSayings ) )
	}
}

//===========================================
// PAR CALCULATIONS                          
//===========================================

def synchronized checkPar( event ) {
	currentPar = parMap[ event.actor.ballOwner().getPlayerVar( "currentFlag" ).intValue() ]
	
	if( event.actor.ballOwner().getPlayerVar( "currentHoleScore" ) == 1 ) {
		event.actor.ballOwner().say( "Oh, yeah! A hole in one!!!" )
		event.actor.say(" Woohoo, ${event.actor.ballOwner()}! IT'S IN THE HOLE!" ) //the ball announces it too so that everyone in the area knows about the hole-in-one
		baseGold = 300
		baseOrbs = 5
		holeReward( event )
		moveFlag( event )
	}
	if( event.actor.ballOwner().getPlayerVar( "currentHoleScore" ) <= currentPar - 2 && event.actor.ballOwner().getPlayerVar( "currentHoleScore" ) != 1 ) {
		event.actor.ballOwner().centerPrint( "A high-flying EAGLE!" )
		baseGold = 200
		baseOrbs = 4
		holeReward( event )
		moveFlag( event )
	}
	if( event.actor.ballOwner().getPlayerVar( "currentHoleScore" ) == currentPar - 1 ) {
		event.actor.ballOwner().centerPrint( "Fantastic! A birdie!" )
		baseGold = 100
		baseOrbs = 3
		holeReward( event )
		moveFlag( event )
	}
	if( event.actor.ballOwner().getPlayerVar( "currentHoleScore" ) == currentPar ) {
		event.actor.ballOwner().centerPrint( "Well done. You made par for this hole!" )
		baseGold = 75
		baseOrbs = 2
		holeReward( event )
		moveFlag( event )
	}
	if( event.actor.ballOwner().getPlayerVar( "currentHoleScore" ) == currentPar + 1) {
		event.actor.ballOwner().centerPrint( "Not too bad. You bogied it!" )
		baseGold = 50
		baseOrbs = 1
		holeReward( event )
		moveFlag( event )
	}
	if( event.actor.ballOwner().getPlayerVar( "currentHoleScore" ) >= currentPar + 2 ) {
		event.actor.ballOwner().centerPrint( "Well...all's well that ends well!" )
		baseGold = 25
		baseOrbs = 0
		holeReward( event )
		moveFlag( event )
	}
}

lootMultMap = [ 1:0.1, 2:0.2, 3:0.3, 4:0.5, 5:0.7, 6:1.0, 7:1.5, 8:2.0, 9:3.0 ]

def holeReward( event ) {
	goldGrant = ( baseGold * lootMultMap.get( event.actor.ballOwner().getPlayerVar( "currentFlag" ).intValue() ) ).intValue()
	if( goldGrant < 1 ) { goldGrant = 1 }
	orbGrant = ( baseOrbs * lootMultMap.get( event.actor.ballOwner().getPlayerVar( "currentFlag" ).intValue() ) ).intValue()
	event.actor.ballOwner().grantCoins( goldGrant )
	event.actor.ballOwner().addPlayerVar( "Z02GoofCourseTotalGold", goldGrant )
	if( orbGrant >= 1 ) {
		event.actor.ballOwner().grantQuantityItem( 100257, orbGrant )
		event.actor.ballOwner().addPlayerVar( "Z02GoofCourseTotalOrbs", orbGrant )
	}
}

	
//===========================================
// ADJUST THE FLAG TO THE NEXT LOCATION      
//===========================================

def moveFlag( event ) {
	removeFlags( event )
	curFlag = event.actor.ballOwner().getPlayerVar( "currentFlag" ).intValue()
//	println "**** curFlag = ${curFlag} ****"
	
	//flag9
	if( curFlag == 9 ) {
		//announce the final score and then dispose of the goofball
		event.actor.ballOwner().setPlayerVar( "finalScore", event.actor.getHitCount().intValue() )
		event.actor.ballOwner().setPlayerVar( "finalTime", event.actor.getTimeOfGameInSeconds().intValue() )
		saveGoofballScore(event.actor.ballOwner() )
		myManager.schedule(2) { event.actor.ballOwner().setQuestFlag( GLOBAL, "Z02QualifiedForAllZoneGoofCourse" ); event.actor.ballOwner().centerPrint( "Woohoo! You finished the game with a total score of ${ event.actor.getHitCount() }!"); event.actor.ballOwner().centerPrint( "Course par is ${ coursePar }." ); myManager.schedule(3) { checkForStrokes( event ) } }
	}
	//flag8
	if( curFlag == 8 ) {
		event.actor.ballOwner().addMiniMapQuestLocation( "flag9", "VILLAGE_402", 820, 490, "Marker 9" )
		event.actor.ballOwner().setPlayerVar( "currentFlag", 9 )
		myManager.schedule(2) { event.actor.ballOwner().centerPrint( "Now for the trickiest hole of all. Hit it into the doorway of the Lawn Gnome mushroom outpost!" ) }
		myManager.schedule(5) { event.actor.ballOwner().centerPrint( "And no, the gnomes don't like you doing that at all! Be careful!" ) }
		myManager.schedule(7) { event.actor.say( "The next hole is a par ${ parMap[ curFlag + 1 ] }." ) }
	}
	//flag7
	if( curFlag == 7 ) {
		event.actor.ballOwner().addMiniMapQuestLocation( "flag8", "VILLAGE_201", 480, 600, "Marker 8" )
		event.actor.ballOwner().setPlayerVar( "currentFlag", 8 )
		myManager.schedule(2) { event.actor.ballOwner().centerPrint( "And up to the corner!" ) }
		myManager.schedule(4) { event.actor.say( "The next hole is a par ${ parMap[ curFlag + 1 ] }." ) }
	}
	//flag6
	if( curFlag == 6 ) {
		event.actor.ballOwner().addMiniMapQuestLocation( "flag7", "VILLAGE_401", 670, 215, "Marker 7" )
		event.actor.ballOwner().setPlayerVar( "currentFlag", 7 )
		myManager.schedule(2) { event.actor.ballOwner().centerPrint( "Cross down to the SW corner of the greens now!" ) }
		myManager.schedule(4) { event.actor.say( "The next hole is a par ${ parMap[ curFlag + 1 ] }." ) }
	}
	//flag5
	if( curFlag == 5 ) {
		event.actor.ballOwner().addMiniMapQuestLocation( "flag6", "VILLAGE_202", 825, 590, "Marker 6" )
		event.actor.ballOwner().setPlayerVar( "currentFlag", 6 )
		myManager.schedule(2) { event.actor.ballOwner().centerPrint( "Just a chip shot away, but the hazards are trickier now!" ) }
		myManager.schedule(4) { event.actor.say( "The next hole is a par ${ parMap[ curFlag + 1 ] }." ) }
	}
	//flag4
	if( curFlag == 4 ) {
		event.actor.ballOwner().addMiniMapQuestLocation( "flag5", "VILLAGE_103", 705, 300, "Marker 5" )
		event.actor.ballOwner().setPlayerVar( "currentFlag", 5 )
		myManager.schedule(2) { event.actor.ballOwner().centerPrint( "And now we head toward the greens area. Watch those sand traps!" ) }
		myManager.schedule(4) { event.actor.say( "The next hole is a par ${ parMap[ curFlag + 1 ] }." ) }
	}
	//flag3
	if( curFlag == 3 ) {
		event.actor.ballOwner().addMiniMapQuestLocation( "flag4", "VILLAGE_5", 675, 450, "Marker 4" )
		event.actor.ballOwner().setPlayerVar( "currentFlag", 4 )
		myManager.schedule(2) { event.actor.ballOwner().centerPrint( "And now all the way up by Barton Town!" ) }
		myManager.schedule(4) { event.actor.say( "The next hole is a par ${ parMap[ curFlag + 1 ] }." ) }
	}
	//flag2
	if( curFlag == 2 ) {
		event.actor.ballOwner().addMiniMapQuestLocation( "flag3", "VILLAGE_104", 420, 530, "Marker 3" )
		event.actor.ballOwner().setPlayerVar( "currentFlag", 3 )
		myManager.schedule(2) { event.actor.ballOwner().centerPrint( "Now back toward the road!" ) }
		myManager.schedule(4) { event.actor.say( "The next hole is a par ${ parMap[ curFlag + 1 ] }." ) }
	}
	//flag1
	if( curFlag == 1 ) {
		event.actor.ballOwner().addMiniMapQuestLocation( "flag2", "VILLAGE_304", 310, 365, "Marker 2" )
		event.actor.ballOwner().setPlayerVar( "currentFlag", 2 )
		myManager.schedule(2) { event.actor.ballOwner().centerPrint( "Now it gets a little more tricky! Watch yourself around this next marker!" ) }
		myManager.schedule(4) { event.actor.say( "The next hole is a par ${ parMap[ curFlag + 1 ] }." ) }
	}
}

lowScoreHolder = null
currentLowScore = 999 //strokes
lowTimeHolder = null
currentLowTime = 999 //seconds

firstPlayerScored = true
firstPlayerTimed = true

def checkForStrokes( event ) {
	score = event.actor.ballOwner().getPlayerVar( "finalScore" ).intValue()
	if( score < currentLowScore && firstPlayerScored == false) {
		event.actor.ballOwner().centerPrint( "You set the course's new Stroke Record! Your score of ${score.intValue()} is ${ (currentLowScore - score).intValue() } strokes lower than the old record!" )
		currentLowScore = score
		lowScoreHolder = event.actor.ballOwner()
	} else if( score == currentLowScore && firstPlayerScored == false ) {
		event.actor.ballOwner().centerPrint( "Fantastic! You tied the course record of ${ currentLowScore.intValue() } currently held by ${lowScoreHolder}! Great job!" )
	} else if( score > currentLowScore && firstPlayerScored == false ) {
		event.actor.ballOwner().centerPrint( "Shoot for the course record next time! The current course record is ${ currentLowScore.intValue() }, held by ${lowScoreHolder}." ) 
	} else if( firstPlayerScored ) {
		firstPlayerScored = false
		event.actor.ballOwner().centerPrint( "You're the first player to grace these greens. You've set the new course record at ${score.intValue()} strokes!" )
		currentLowScore = score
		lowScoreHolder = event.actor.ballOwner()
	}
	//Now display time information
	myManager.schedule(3) {
		time = event.actor.ballOwner().getPlayerVar( "finalTime" ).intValue()
		if( time < currentLowTime && firstPlayerTimed == false ) {
			event.actor.ballOwner().centerPrint( "You beat the course's Time Record! Your time of ${ (time/60).intValue() }minutes and ${ (time%60).intValue() } seconds is ${(currentLowTime - time).intValue()} seconds faster than the old record!" )
			currentLowTime = time
			lowTimeHolder = event.actor.ballOwner()
		} else if( time == currentLowTime && firstPlayerTimed == false ) {
			event.actor.ballOwner().centerPrint( "You TIED the fastest course time of ${(currentLowTime/60).intValue()} minutes and ${(currentLowTime%60).intValue()} seconds held by ${lowTimeHolder}!" )
		} else if( time > currentLowTime && firstPlayerTimed == false ) {
			event.actor.ballOwner().centerPrint( "The current time to beat is ${lowTimeHolder}'s record of ${(currentLowTime/60).intValue()} minutes and ${(currentLowTime%60).intValue()} seconds." )
		} else if( firstPlayerTimed ) {
			firstPlayerTimed = false
			event.actor.ballOwner().centerPrint( "And you've set the time to beat at ${ (time/60).intValue() } minutes and ${ (time%60).intValue() } seconds!" ) 
			currentLowTime = time
			lowTimeHolder = event.actor.ballOwner()
		}			
	}
	//Now display the matrix of top 10 scores
	myManager.schedule(6) { checkForBadges( event ) }
}

def checkForBadges( event ) {
	//check for Badges
	if( !event.actor.ballOwner().isDoneQuest( 241) && event.actor.ballOwner().getPlayerVar( "finalScore" ) >= coursePar + 20 ) {
		event.actor.ballOwner().updateQuest( 241, "Story-VQS" ) //The 800-Pound Gorilla
		event.actor.ballOwner().centerPrint( "You drove that goofball all over the place and you've earned a badge appropriate to your actions." )
	}
	if( !event.actor.ballOwner().isDoneQuest( 242) && event.actor.ballOwner().getPlayerVar( "finalScore" ) > coursePar && score < coursePar + 20 ) {
		event.actor.ballOwner().updateQuest( 242, "Story-VQS" ) //Missed the Cut
		event.actor.ballOwner().centerPrint( "Not quite good enough for the professional tour, but keep up the good work! You'll get there!" )
	}
	if( !event.actor.ballOwner().isDoneQuest( 243) && event.actor.ballOwner().getPlayerVar( "finalScore" ) > coursePar - 10 && score <= coursePar ) {
		event.actor.ballOwner().updateQuest( 243, "Story-VQS" ) //On the Tour
		event.actor.ballOwner().centerPrint( "Well done! You're an up-and-coming putter of prodigious proportions! You made the tour!" )
	}
	if( !event.actor.ballOwner().isDoneQuest( 244) && event.actor.ballOwner().getPlayerVar( "finalScore" ) <= coursePar - 10 ) {
		event.actor.ballOwner().updateQuest( 244, "Story-VQS" ) //The Next Tiger 
		event.actor.ballOwner().centerPrint( "Wow! An absolutely incredible score! You're the next great thing on the circuit!" )
	}
	myManager.schedule(3) { cashReward( event ) }
}

def cashReward( event ) {
	event.actor.ballOwner().centerPrint( "You earned a total of ${ event.actor.ballOwner().getPlayerVar( "Z02GoofCourseTotalGold" ).intValue() } gold!" )
	if( event.actor.ballOwner().getPlayerVar( "Z02GoofCourseTotalOrbs" ) > 0 ) {
		event.actor.ballOwner().centerPrint( "And you also got ${ event.actor.ballOwner().getPlayerVar( "Z02GoofCourseTotalOrbs" ).intValue() } charge orbs!" )
	}
	event.actor.ballOwner().centerPrint( "Congratulations!" )
		
	//keep track of the number of times that a player has beaten the Goof Course and reward them with the Lawn Mower once they get to 10 wins of par or less.
	if( event.actor.ballOwner().getPlayerVar( "finalScore" ).intValue() < coursePar ) {
		//if the player beat par, add one to the playerVar that keeps track of that value
		event.actor.ballOwner().addPlayerVar( "Z02TimesBeatingPar", 1 )
		//if this is the 10th time the player has beaten par, then give them the mower
		if( event.actor.ballOwner().getPlayerVar( "Z02TimesBeatingPar" ) == 10 ) {
			event.actor.ballOwner().centerPrint( "You've beaten par for the course ten times!" )
			myManager.schedule(3) { event.actor.ballOwner().centerPrint( "MacTaggert sends you your very own Lawn Mower as a reward!" ); event.actor.ballOwner().grantItem( "17619" ) }
			myManager.schedule(6) { event.actor.ballOwner().centerPrint( "Now get to work! The greens are not going to mow themselves!" ) }
		}
	}
	//eliminate the goofball after saving the high-score
	//NOTE: This must be the last thing that gets executed so that there is still a "ballOwner()" to check.
	myManager.schedule(7) { event.actor.getOwnerPlayer().disposeGoofBall() }
}	

//def displayAllTimeHighScores( event ) {
//	displayGoofballHiScore( event.actor.ballOwner() )
//	myManager.schedule(2) { event.actor.getOwnerPlayer().disposeGoofBall() } //eliminate the goofball after saving the high-score
//}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//*******************************************
//             SANDTRAP LOGIC                
//*******************************************
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//===========================================
// PLAYER LISTS FOR ROOMS                    
//===========================================

playerList202 = []
playerList203 = []
playerList301 = []
playerList302 = []
playerList303 = []

//Player List 202
myManager.onEnter( myRooms.VILLAGE_202) { event ->
	if( isPlayer( event.actor ) ) {
		playerList202 << event.actor
	}
}

myManager.onExit( myRooms.VILLAGE_202 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList202.remove( event.actor )
	}
}

//Player List 203
myManager.onEnter( myRooms.VILLAGE_203) { event ->
	if( isPlayer( event.actor ) ) {
		playerList203 << event.actor
	}
}

myManager.onExit( myRooms.VILLAGE_203 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList203.remove( event.actor )
	}
}

//Player List 301
myManager.onEnter( myRooms.VILLAGE_301) { event ->
	if( isPlayer( event.actor ) ) {
		playerList301 << event.actor
	}
}

myManager.onExit( myRooms.VILLAGE_301 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList301.remove( event.actor )
	}
}

//Player List 302
myManager.onEnter( myRooms.VILLAGE_302) { event ->
	if( isPlayer( event.actor ) ) {
		playerList302 << event.actor
	}
}

myManager.onExit( myRooms.VILLAGE_302 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList302.remove( event.actor )
	}
}

//Player List 303
myManager.onEnter( myRooms.VILLAGE_303) { event ->
	if( isPlayer( event.actor ) ) {
		playerList303 << event.actor
	}
}

myManager.onExit( myRooms.VILLAGE_303 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList303.remove( event.actor )
	}
}

//------------------------------------------
// SAND TRAP FLUFF SPAWNERS                 
//------------------------------------------

sandTrapSpawner202 = myRooms.VILLAGE_202.spawnSpawner( "sandTrapSpawner202", "sandtrap_fluff", 3 )
sandTrapSpawner202.setPos( 330, 430 )
sandTrapSpawner202.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandTrapSpawner202.setWaitTime( 5, 10 )
sandTrapSpawner202.setMonsterLevelForChildren( 1.2 )

sandTrapSpawner203 = myRooms.VILLAGE_203.spawnSpawner( "sandTrapSpawner203", "sandtrap_fluff", 3 )
sandTrapSpawner203.setPos( 430, 195 )
sandTrapSpawner203.setWanderBehaviorForChildren( 25, 100, 2, 5, 200)
sandTrapSpawner203.setWaitTime( 5, 10 )
sandTrapSpawner203.setMonsterLevelForChildren( 1.1 )

sandTrapSpawner301 = myRooms.VILLAGE_301.spawnSpawner( "sandTrapSpawner301", "sandtrap_fluff", 3 )
sandTrapSpawner301.setPos( 640, 160 )
sandTrapSpawner301.setWanderBehaviorForChildren( 25, 100, 2, 5, 200)
sandTrapSpawner301.setWaitTime( 5, 10 )
sandTrapSpawner301.setMonsterLevelForChildren( 1.4 )

sandTrapSpawner302A = myRooms.VILLAGE_302.spawnSpawner( "sandTrapSpawner302A", "sandtrap_fluff", 3 )
sandTrapSpawner302A.setPos( 305, 450 )
sandTrapSpawner302A.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandTrapSpawner302A.setWaitTime( 5, 10 )
sandTrapSpawner302A.setMonsterLevelForChildren( 1.3 )

sandTrapSpawner302B = myRooms.VILLAGE_302.spawnSpawner( "sandTrapSpawner302B", "sandtrap_fluff", 3 )
sandTrapSpawner302B.setPos( 820, 540 )
sandTrapSpawner302B.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandTrapSpawner302B.setWaitTime( 5, 10 )
sandTrapSpawner302B.setMonsterLevelForChildren( 1.3 )

sandTrapSpawner302C = myRooms.VILLAGE_302.spawnSpawner( "sandTrapSpawner302C", "sandtrap_fluff", 2 )
sandTrapSpawner302C.setPos( 990, 150 )
sandTrapSpawner302C.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandTrapSpawner302C.setWaitTime( 5, 10 )
sandTrapSpawner302C.setMonsterLevelForChildren( 1.3 )

sandTrapSpawner303 = myRooms.VILLAGE_303.spawnSpawner( "sandTrapSpawner303", "sandtrap_fluff", 2 )
sandTrapSpawner303.setPos( 100, 125 )
sandTrapSpawner303.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandTrapSpawner303.setWaitTime( 5, 10 )
sandTrapSpawner303.setMonsterLevelForChildren( 1.2 )

//===========================================
// SANDTRAP TRIGGER ZONE LOCATIONS           
//===========================================
def sandTrap202 = "sandTrap202"
myRooms.VILLAGE_202.createTriggerZone( sandTrap202, 100, 350, 530, 490 )

def sandTrap203 = "sandTrap203"
myRooms.VILLAGE_203.createTriggerZone( sandTrap203, 265, 115, 610, 275 )

def sandTrap301 = "sandTrap301"
myRooms.VILLAGE_301.createTriggerZone( sandTrap301, 470, 90, 830, 220 )

def sandTrap302A = "sandTrap302A"
myRooms.VILLAGE_302.createTriggerZone( sandTrap302A, 150, 370, 470, 550 )

def sandTrap302B = "sandTrap302B"
myRooms.VILLAGE_302.createTriggerZone( sandTrap302B, 620, 435, 1040, 640 )

def sandTrap302C = "sandTrap302C"
myRooms.VILLAGE_302.createTriggerZone( sandTrap302C, 890, 70, 1040, 225 )

def sandTrap303 = "sandTrap303"
myRooms.VILLAGE_303.createTriggerZone( sandTrap303, 0, 40, 230, 225 )

//===========================================
// SANDTRAP FLUFF AMBUSHES                   
//===========================================

delay = 30 //time until the sand traps CAN reset (playerList for that room must be empty before they can respawn...but they cannot respawn more often than this)
sandTrap202Reset = true
sandTrap203Reset = true
sandTrap301Reset = true
sandTrap302AReset = true
sandTrap302BReset = true
sandTrap302CReset = true
sandTrap303Reset = true

//AMBUSH 202
myManager.onTriggerIn(myRooms.VILLAGE_202, sandTrap202 ) { event ->
	if( isGoofBall( event.actor) && sandTrap202Reset == true ) { //check for a golf ball here instead of a player when goofball detection scripts are refined
		event.actor.ballOwner().centerPrint( "Your goofball landed in a sandtrap. Beware!" )
		sandTrap202Reset = false
		sand202A = sandTrapSpawner202.forceSpawnNow()
		sand202B = sandTrapSpawner202.forceSpawnNow()
		sand202C = sandTrapSpawner202.forceSpawnNow()
		sand202A.addHate( event.actor.ballOwner(), 1 )
		sand202B.addHate( event.actor.ballOwner(), 1 )
		sand202C.addHate( event.actor.ballOwner(), 1 )
		myManager.schedule( delay ) { checkToReset202() }
	}
}

def checkToReset202() {
	if( playerList202.isEmpty() ) {
		sandTrap202Reset = true
		if( !sand202A.isDead() ) {
			sand202A.dispose()
		}
		if( !sand202B.isDead() ) {
			sand202B.dispose()
		}
		if( !sand202C.isDead() ) {
			sand202C.dispose()
		}
	} else {
		myManager.schedule(5) { checkToReset202() }
	}
}

//AMBUSH 203
myManager.onTriggerIn(myRooms.VILLAGE_203, sandTrap203 ) { event ->
	if( isGoofBall( event.actor) && sandTrap203Reset == true ) { //check for a golf ball here instead of a player when goofball detection scripts are refined
		event.actor.ballOwner().centerPrint( "Your goofball landed in a sandtrap. Beware!" )
		sandTrap203Reset = false
		sand203A = sandTrapSpawner203.forceSpawnNow()
		sand203B = sandTrapSpawner203.forceSpawnNow()
		sand203C = sandTrapSpawner203.forceSpawnNow()
		sand203A.addHate( event.actor.ballOwner(), 1 )
		sand203B.addHate( event.actor.ballOwner(), 1 )
		sand203C.addHate( event.actor.ballOwner(), 1 )
		myManager.schedule( delay ) { checkToReset203() }
	}
}

def checkToReset203() {
	if( playerList203.isEmpty() ) {
		sandTrap203Reset = true
		if( !sand203A.isDead() ) {
			sand203A.dispose()
		}
		if( !sand203B.isDead() ) {
			sand203B.dispose()
		}
		if( !sand203C.isDead() ) {
			sand203C.dispose()
		}
	} else {
		myManager.schedule(5) { checkToReset203() }
	}
}

//AMBUSH 301
myManager.onTriggerIn(myRooms.VILLAGE_301, sandTrap301 ) { event ->
	if( isGoofBall( event.actor) && sandTrap301Reset == true ) { //check for a golf ball here instead of a player when goofball detection scripts are refined
		event.actor.ballOwner().centerPrint( "Your goofball landed in a sandtrap. Beware!" )
		sandTrap301Reset = false
		sand301A = sandTrapSpawner301.forceSpawnNow()
		sand301B = sandTrapSpawner301.forceSpawnNow()
		sand301C = sandTrapSpawner301.forceSpawnNow()
		sand301A.addHate( event.actor.ballOwner(), 1 )
		sand301B.addHate( event.actor.ballOwner(), 1 )
		sand301C.addHate( event.actor.ballOwner(), 1 )
		myManager.schedule( delay ) { checkToReset301() }
	}
}

def checkToReset301() {
	if( playerList301.isEmpty() ) {
		sandTrap301Reset = true
		if( !sand301A.isDead() ) {
			sand301A.dispose()
		}
		if( !sand301B.isDead() ) {
			sand301B.dispose()
		}
		if( !sand301C.isDead() ) {
			sand301C.dispose()
		}
	} else {
		myManager.schedule(5) { checkToReset301() }
	}
}

//AMBUSH 302A
myManager.onTriggerIn(myRooms.VILLAGE_302, sandTrap302A ) { event ->
	if( isGoofBall( event.actor) && sandTrap302AReset == true ) { //check for a golf ball here instead of a player when goofball detection scripts are refined
		event.actor.ballOwner().centerPrint( "Your goofball landed in a sandtrap. Beware!" )
		sandTrap302AReset = false
		sand302A1 = sandTrapSpawner302A.forceSpawnNow()
		sand302A2 = sandTrapSpawner302A.forceSpawnNow()
		sand302A3 = sandTrapSpawner302A.forceSpawnNow()
		sand302A1.addHate( event.actor.ballOwner(), 1 )
		sand302A2.addHate( event.actor.ballOwner(), 1 )
		sand302A3.addHate( event.actor.ballOwner(), 1 )
		myManager.schedule( delay ) { checkToReset302A() }
	}
}

def checkToReset302A() {
	if( playerList302.isEmpty() ) {
		sandTrap302AReset = true
		if( !sand302A1.isDead() ) {
			sand302A1.dispose()
		}
		if( !sand302A2.isDead() ) {
			sand302A2.dispose()
		}
		if( !sand302A3.isDead() ) {
			sand302A3.dispose()
		}
	} else {
		myManager.schedule(5) { checkToReset302A() }
	}
}

//AMBUSH 302B
myManager.onTriggerIn(myRooms.VILLAGE_302, sandTrap302B ) { event ->
	if( isGoofBall( event.actor) && sandTrap302BReset == true ) { //check for a golf ball here instead of a player when goofball detection scripts are refined
		event.actor.ballOwner().centerPrint( "Your goofball landed in a sandtrap. Beware!" )
		sandTrap302BReset = false
		sand302B1 = sandTrapSpawner302B.forceSpawnNow()
		sand302B2 = sandTrapSpawner302B.forceSpawnNow()
		sand302B3 = sandTrapSpawner302B.forceSpawnNow()
		sand302B1.addHate( event.actor.ballOwner(), 1 )
		sand302B2.addHate( event.actor.ballOwner(), 1 )
		sand302B3.addHate( event.actor.ballOwner(), 1 )
		myManager.schedule( delay ) { checkToReset302B() }
	}
}

def checkToReset302B() {
	if( playerList302.isEmpty() ) {
		sandTrap302BReset = true
		if( !sand302B1.isDead() ) {
			sand302B1.dispose()
		}
		if( !sand302B2.isDead() ) {
			sand302B2.dispose()
		}
		if( !sand302B3.isDead() ) {
			sand302B3.dispose()
		}
	} else {
		myManager.schedule(5) { checkToReset302B() }
	}
}

//AMBUSH 302C
myManager.onTriggerIn(myRooms.VILLAGE_302, sandTrap302C ) { event ->
	if( isGoofBall( event.actor) && sandTrap302CReset == true ) { //check for a golf ball here instead of a player when goofball detection scripts are refined
		event.actor.ballOwner().centerPrint( "Your goofball landed in a sandtrap. Beware!" )
		sandTrap302CReset = false
		sand302C1 = sandTrapSpawner302C.forceSpawnNow()
		sand302C2 = sandTrapSpawner302C.forceSpawnNow()
		sand302C1.addHate( event.actor.ballOwner(), 1 )
		sand302C2.addHate( event.actor.ballOwner(), 1 )
		myManager.schedule( delay ) { checkToReset302C() }
	}
}

def checkToReset302C() {
	if( playerList302.isEmpty() ) {
		sandTrap302CReset = true
		if( !sand302C1.isDead() ) {
			sand302C1.dispose()
		}
		if( !sand302C2.isDead() ) {
			sand302C2.dispose()
		}
	} else {
		myManager.schedule(5) { checkToReset302C() }
	}
}

//AMBUSH 303
myManager.onTriggerIn(myRooms.VILLAGE_303, sandTrap303 ) { event ->
	if( isGoofBall( event.actor) && sandTrap303Reset == true ) { //check for a golf ball here instead of a player when goofball detection scripts are refined
		event.actor.ballOwner().centerPrint( "Your goofball landed in a sandtrap. Beware!" )
		sandTrap303Reset = false
		sand303A = sandTrapSpawner303.forceSpawnNow()
		sand303B = sandTrapSpawner303.forceSpawnNow()
		sand303A.addHate( event.actor.ballOwner(), 1 )
		sand303B.addHate( event.actor.ballOwner(), 1 )
		myManager.schedule( delay ) { checkToReset303() }
	}
}

def checkToReset303() {
	if( playerList303.isEmpty() ) {
		sandTrap303Reset = true
		if( !sand303A.isDead() ) {
			sand303A.dispose()
		}
		if( !sand303B.isDead() ) {
			sand303B.dispose()
		}
	} else {
		myManager.schedule(5) { checkToReset303() }
	}
}


//===========================================
// INITIAL LOGIC                             
//===========================================
sandTrapSpawner202.stopSpawning()
sandTrapSpawner203.stopSpawning()
sandTrapSpawner301.stopSpawning()
sandTrapSpawner302A.stopSpawning()
sandTrapSpawner302B.stopSpawning()
sandTrapSpawner302C.stopSpawning()
sandTrapSpawner303.stopSpawning()

*/
		
