import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// LOOKOUT MUSHROOM GUARDS                  
//------------------------------------------

LookoutMushroom = myRooms.VILLAGE_902.spawnSpawner( "LookoutMushroom", "lawn_gnome", 3 )
LookoutMushroom.setPos( 925, 320 )
LookoutMushroom.setWanderBehaviorForChildren( 50, 150, 2, 5, 250)
LookoutMushroom.setWaitTime( 1, 2 )
LookoutMushroom.setMonsterLevelForChildren( 1.9 )

def respawn902() {
	if( myRooms.VILLAGE_902.getSpawnTypeCount( "lawn_gnome" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			LookoutMushroom.spawnAllNow();
			myManager.schedule( 15 ) { respawn902() }
		}
	} else {
		myManager.schedule( 15 ) { respawn902() }
	}
}

//------------------------------------------
// WINDMILL GUARDS                          
//------------------------------------------

WindmillDoor = myRooms.VILLAGE_903.spawnSpawner( "WindmillDoor", "lawn_gnome", 4 )
WindmillDoor.setPos( 530, 420 )
WindmillDoor.setWanderBehaviorForChildren( 50, 150, 2, 5, 300)
WindmillDoor.setHomeTetherForChildren( 2000 )
WindmillDoor.setHateRadiusForChildren( 2000 )
WindmillDoor.setWaitTime( 1, 2 )
WindmillDoor.setMonsterLevelForChildren( 1.6 )

PerimeterThree = myRooms.VILLAGE_903.spawnSpawner( "PerimeterThree", "pink_flamingo", 1 )
PerimeterThree.setPos( 590, 260 )
PerimeterThree.setWaitTime( 1, 2 )
PerimeterThree.setHome( WindmillDoor ) // warn the Windmill guards
PerimeterThree.setGuardPostForChildren( "VILLAGE_903", 360, 540, 180 )
PerimeterThree.setCFH( 400 )
PerimeterThree.setMonsterLevelForChildren( 1.6 )

PerimeterFour = myRooms.VILLAGE_903.spawnSpawner( "PerimeterFour", "pink_flamingo", 1 )
PerimeterFour.setPos( 590, 260 )
PerimeterFour.setWaitTime( 1, 2 )
PerimeterFour.setHome( WindmillDoor ) // warn the Windmill guards
PerimeterFour.setGuardPostForChildren( "VILLAGE_903", 260, 335, 135 )
PerimeterThree.setCFH( 400 )
PerimeterFour.setMonsterLevelForChildren( 1.6 )

def respawn903() {
	if( myRooms.VILLAGE_903.getSpawnTypeCount( "lawn_gnome" ) + myRooms.VILLAGE_903.getSpawnTypeCount( "pink_flamingo" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			WindmillDoor.spawnAllNow();
			PerimeterThree.spawnAllNow();
			PerimeterFour.spawnAllNow();
			
			myManager.schedule( 15 ) { respawn903() }
		}
	} else {
		myManager.schedule( 15 ) { respawn903() }
	}
}

//------------------------------------------
// BLUE MUSHROOM OUTPOST                    
//------------------------------------------

BlueMushroom = myRooms.VILLAGE_1003.spawnSpawner( "BlueMushroom", "lawn_gnome", 5 )
BlueMushroom.setPos( 470, 395 )
BlueMushroom.setWanderBehaviorForChildren( 50, 150, 2, 5, 300)
BlueMushroom.setHomeTetherForChildren( 2000 )
BlueMushroom.setHateRadiusForChildren( 2000 )
BlueMushroom.setWaitTime( 1, 2 )
BlueMushroom.setMonsterLevelForChildren( 1.7 )

PerimeterTwo = myRooms.VILLAGE_1003.spawnSpawner( "PerimeterTwo", "pink_flamingo", 1 )
PerimeterTwo.setPos( 500, 200 )
PerimeterTwo.setWaitTime( 1, 2 )
PerimeterTwo.setHome( BlueMushroom ) // warn the Blue Mushroom guards
PerimeterTwo.setGuardPostForChildren( "VILLAGE_1003", 80, 200, 225 )
PerimeterThree.setCFH( 400 )
PerimeterTwo.setMonsterLevelForChildren( 1.7 )

def respawn1003() {
	if( myRooms.VILLAGE_1003.getSpawnTypeCount( "lawn_gnome" ) + myRooms.VILLAGE_1003.getSpawnTypeCount( "pink_flamingo" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			BlueMushroom.spawnAllNow();
			PerimeterTwo.spawnAllNow();
			myManager.schedule( 15 ) { respawn1003() }
		}
	} else {
		myManager.schedule( 15 ) { respawn1003() }
	}
}

//------------------------------------------
// FLAMINGO BREEDING GROUNDS                
//------------------------------------------

def FlamingoBreeding = myRooms.VILLAGE_1004.spawnSpawner( "FlamingoBreeding", "pink_flamingo", 6 )
FlamingoBreeding.setPos( 325, 200 )
FlamingoBreeding.setWanderBehaviorForChildren( 50, 200, 3, 7, 350)
FlamingoBreeding.setHomeTetherForChildren( 1500 )
FlamingoBreeding.setWaitTime( 50, 70 )
FlamingoBreeding.setCFH( 0 )
FlamingoBreeding.setMonsterLevelForChildren( 1.6 )


//------------------------------------------
// SOUTH GATE PATROLLING GUARDS             
//------------------------------------------

SouthGateOne = myRooms.VILLAGE_1002.spawnSpawner( "SouthGateOne", "lawn_gnome", 1 )
SouthGateOne.setPos( 200, 600 )
SouthGateOne.setWaitTime( 1, 2 )
SouthGateOne.setHomeTetherForChildren( 3000 ) // set to very large distances so that these monsters can fight back against archers from 902 ledge.
SouthGateOne.setHateRadiusForChildren( 3000 )
SouthGateOne.addPatrolPointForChildren( "VILLAGE_1002", 600, 400, 1 )
SouthGateOne.addPatrolPointForChildren( "VILLAGE_1002", 400, 415, 1 )
SouthGateOne.setMonsterLevelForChildren( 1.9 )

SouthGateTwo = myRooms.VILLAGE_1002.spawnSpawner( "SouthGateTwo", "lawn_gnome", 1 )
SouthGateTwo.setPos( 885, 590 )
SouthGateTwo.setWaitTime( 1, 2 )
SouthGateTwo.setHomeTetherForChildren( 3000 )
SouthGateTwo.setHateRadiusForChildren( 3000 )
SouthGateTwo.addPatrolPointForChildren( "VILLAGE_1002", 440, 480, 1 )
SouthGateTwo.addPatrolPointForChildren( "VILLAGE_1002", 700, 540, 1 )
SouthGateTwo.setMonsterLevelForChildren( 1.9 )

def respawn1002() {
	if( SouthGateOne.spawnsInUse() + SouthGateTwo.spawnsInUse() == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			SouthGateOne.spawnAllNow();
			SouthGateTwo.spawnAllNow();
			myManager.schedule( 15 ) { respawn1002() }
		}
	} else {
		myManager.schedule( 15 ) { respawn1002() }
	}
}


//ALLIANCES
WindmillDoor.allyWithSpawner( PerimeterThree )
WindmillDoor.allyWithSpawner( PerimeterFour )
PerimeterThree.allyWithSpawner( PerimeterFour )

BlueMushroom.allyWithSpawner( PerimeterTwo )

SouthGateOne.allyWithSpawner( SouthGateTwo )


//SERVER STARTUP COMMANDS
LookoutMushroom.spawnAllNow()
WindmillDoor.spawnAllNow()
BlueMushroom.spawnAllNow()
PerimeterTwo.spawnAllNow()
PerimeterThree.spawnAllNow()
PerimeterFour.spawnAllNow()
SouthGateOne.spawnAllNow()
SouthGateTwo.spawnAllNow()

LookoutMushroom.stopSpawning()
WindmillDoor.stopSpawning()
BlueMushroom.stopSpawning()
PerimeterTwo.stopSpawning()
PerimeterThree.stopSpawning()
PerimeterFour.stopSpawning()
SouthGateOne.stopSpawning()
SouthGateTwo.stopSpawning()

respawn902()
respawn903()
respawn1003()
respawn1002()

FlamingoBreeding.spawnAllNow()

