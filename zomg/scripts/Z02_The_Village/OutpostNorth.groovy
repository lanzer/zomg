import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// MAIN OUTPOST LIEUTENANTS (for Leon quest)
//------------------------------------------

NortheastOutpostLT = myRooms.VILLAGE_503.spawnSpawner( "NortheastOutpostLT", "lawn_gnome_LT2", 1 )
NortheastOutpostLT.setPos( 570, 180 )
NortheastOutpostLT.setWaitTime( 1, 2 )
NortheastOutpostLT.setCFH( 0 )
NortheastOutpostLT.setHateRadiusForChildren( 2000 )
NortheastOutpostLT.setGuardPostForChildren(NortheastOutpostLT, 160)
NortheastOutpostLT.setMonsterLevelForChildren( 1.5 )

NorthwestOutpostLT = myRooms.VILLAGE_501.spawnSpawner( "NorthwestOutpostLT", "lawn_gnome_LT2", 1 )
NorthwestOutpostLT.setPos( 645, 320 )
NorthwestOutpostLT.setWaitTime( 1, 2 )
NorthwestOutpostLT.setCFH( 0 )
NorthwestOutpostLT.setHateRadiusForChildren( 2000 )
NorthwestOutpostLT.setGuardPostForChildren(NorthwestOutpostLT, 160)
NorthwestOutpostLT.setMonsterLevelForChildren( 1.7 )

NorthOutpostLT = myRooms.VILLAGE_402.spawnSpawner( "NorthOutpostLT", "lawn_gnome_LT2", 1 )
NorthOutpostLT.setPos( 580, 290 )
NorthOutpostLT.setWaitTime( 1, 2 )
NorthOutpostLT.setCFH( 0 )
NorthOutpostLT.setHateRadiusForChildren( 2000 )
NorthOutpostLT.setGuardPostForChildren( "VILLAGE_402", 670, 585, 160)
NorthOutpostLT.setMonsterLevelForChildren( 1.4 )

//------------------------------------------
// RIGHT GUARD MUSHROOM                     
// (TOP AND BOTTOM SPAWNERS)                
//------------------------------------------

RightGuardMushroomTop = myRooms.VILLAGE_403.spawnSpawner( "RightGuardMushroomTop", "lawn_gnome", 3 )
RightGuardMushroomTop.setPos( 750, 520 )
RightGuardMushroomTop.setWanderBehaviorForChildren( 25, 75, 2, 5, 200)
RightGuardMushroomTop.setCFH( 0 )
RightGuardMushroomTop.setWaitTime( 1, 2 )
RightGuardMushroomTop.setMonsterLevelForChildren( 1.3 )

//============================
//Camp Spawning Logic  - Top  
//============================

def respawn403() {
	if( myRooms.VILLAGE_403.getSpawnTypeCount( "lawn_gnome" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			RightGuardMushroomTop.spawnAllNow();
			myManager.schedule( 15 ) { respawn403() }
		}
	} else {
		myManager.schedule( 15 ) { respawn403() }
	}
}

respawn403() //start it up!

//=============================

RightGuardMushroomBottom = myRooms.VILLAGE_503.spawnSpawner( "RightGuardMushroomBottom", "lawn_gnome", 3 )
RightGuardMushroomBottom.setPos( 585, 220 )
RightGuardMushroomBottom.setWanderBehaviorForChildren( 50, 150, 2, 5, 200)
RightGuardMushroomBottom.setCFH( 0 )
RightGuardMushroomBottom.setWaitTime( 1, 2 )
RightGuardMushroomBottom.setMonsterLevelForChildren( 1.5 )

//=============================
//Camp Spawning Logic  - Bottom
//=============================

def respawn503() {
	if( myRooms.VILLAGE_503.getSpawnTypeCount( "lawn_gnome" ) == 0 && myRooms.VILLAGE_503.getSpawnTypeCount( "mushroom_turret" ) == 0 && majorDisaster.isDead() ) {
		//if the camp is cleared, then respawn it all again in 50-70 seconds
		myManager.schedule( random( 50, 70 ) ) { 
			RightGuardMushroomBottom.spawnAllNow()
			majorDisaster = NortheastOutpostLT.forceSpawnNow()
			majorDisaster.setDisplayName( "Major Disaster" )
			//clear the hate list for the Major
			majorDisasterHateList = []
			//when Major Disaster is killed, check for quest updates
			runOnDeath( majorDisaster ) { event ->
				majorDisasterHateList.addAll( event.actor.getHated() )
				majorDisasterHateList.each {
					if( isPlayer(it) && it.isOnQuest( 89, 2 ) && !it.hasQuestFlag(GLOBAL, "majorDisasterKilled" ) ) {
						it.removeMiniMapQuestLocation("Major Disaster")
						it.setQuestFlag( GLOBAL, "majorDisasterKilled" )
						it.addToPlayerVar( "lieutenantCount", 1 )
						it.centerPrint("Major Disaster defeated.")
					}
					//println "#### majorTragedyKilled = ${it.hasQuestFlag(GLOBAL, "majorTragedyKilled")} majorCatastropheKilled = ${it.hasQuestFlag(GLOBAL, "majorCatastropheKilled")} majorDisasterKilled = ${it.hasQuestFlag(GLOBAL, "majorDisasterKilled")} ####"
					if(isPlayer(it) && it.isOnQuest(89, 2) && it.hasQuestFlag(GLOBAL, "majorTragedyKilled") && it.hasQuestFlag(GLOBAL, "majorCatastropheKilled") ) {
						//println "#### Attempting to update quest 89 on ${it} ####"
						it.updateQuest(89, "Leon-VQS")
					}
				}
			}	
			//now that the respawn is done, start the check loop again
			myManager.schedule( 15 ) { respawn503() }
		}
	} else {
		//since a respawn wasn't necessary yet, try again in 15 seconds
		myManager.schedule( 15 ) { respawn503() }
	}
}

//do initial spawns so respawn routine initial checks work
RightGuardMushroomBottom.spawnAllNow()
majorDisaster = NortheastOutpostLT.forceSpawnNow()
majorDisaster?.setDisplayName( "Major Disaster" )
runOnDeath( majorDisaster ) { event ->
	majorDisasterHateList.addAll( event.actor.getHated() )
	majorDisasterHateList.each {
		if( isPlayer(it) && it.isOnQuest( 89, 2 ) && !it.hasQuestFlag(GLOBAL, "majorDisasterKilled" ) ) {
			it.removeMiniMapQuestLocation("Major Disaster")
			it.setQuestFlag( GLOBAL, "majorDisasterKilled" )
			it.addToPlayerVar( "lieutenantCount", 1 )
			it.centerPrint("Major Disaster defeated.")
		}
		//println "#### Player = ${it} majorTragedyKilled = ${it.hasQuestFlag(GLOBAL, "majorTragedyKilled")} majorCatastropheKilled = ${it.hasQuestFlag(GLOBAL, "majorCatastropheKilled")} majorDisasterKilled = ${it.hasQuestFlag(GLOBAL, "majorDisasterKilled")} ####"
		if(isPlayer(it) && it.isOnQuest(89, 2) && it.hasQuestFlag(GLOBAL, "majorTragedyKilled") && it.hasQuestFlag(GLOBAL, "majorCatastropheKilled") ) {
			//println "#### Attempting to update quest 89 on ${it} ####"
			it.updateQuest(89, "Leon-VQS")
		}
	}
}
//start it up!
respawn503() 

//------------------------------------------
// LEFT GUARD MUSHROOM                      
//------------------------------------------

LeftGuardMushroom = myRooms.VILLAGE_501.spawnSpawner( "LeftGuardMushroom", "lawn_gnome", 4 )
LeftGuardMushroom.setPos( 510, 275 )
LeftGuardMushroom.setWanderBehaviorForChildren( 50, 150, 2, 5, 350)
LeftGuardMushroom.setCFH( 0 )
LeftGuardMushroom.setWaitTime( 1, 2 )
LeftGuardMushroom.setHateRadiusForChildren( 2000 )
LeftGuardMushroom.setMonsterLevelForChildren( 1.7 )

//=====================
//Camp Spawning Logic  
//=====================

def respawn501() {
	if( myRooms.VILLAGE_501.getSpawnTypeCount( "lawn_gnome" ) == 0 && majorCatastrophe.isDead() ) {
		myManager.schedule( random( 50, 70 ) ) { 
			LeftGuardMushroom.spawnAllNow()
			majorCatastrophe = NorthwestOutpostLT.forceSpawnNow()
			majorCatastrophe.setDisplayName( "Major Catastrophe" )
			majorCatastropheHateList = []
			runOnDeath( majorCatastrophe ) { event ->
				majorCatastropheHateList.addAll(event.actor.getHated())
				majorCatastropheHateList.each {
					if(isPlayer(it) && it.isOnQuest(89, 2) && !it.hasQuestFlag(GLOBAL, "majorCatastropheKilled") ) {
						it.removeMiniMapQuestLocation("Major Catastrophe")
						it.setQuestFlag(GLOBAL, "majorCatastropheKilled")
						it.addToPlayerVar( "lieutenantCount", 1 )
						it.centerPrint("Major Catastrophe defeated.")
					}
					//println "#### majorTragedyKilled = ${it.hasQuestFlag(GLOBAL, "majorTragedyKilled")} majorCatastropheKilled = ${it.hasQuestFlag(GLOBAL, "majorCatastropheKilled")} majorDisasterKilled = ${it.hasQuestFlag(GLOBAL, "majorDisasterKilled")} ####"
					if(isPlayer(it) && it.isOnQuest(89, 2) && it.hasQuestFlag(GLOBAL, "majorTragedyKilled") && it.hasQuestFlag(GLOBAL, "majorDisasterKilled") ) {
						it.updateQuest(89, "Leon-VQS")
					}
				}
			}
			myManager.schedule( 15 ) { respawn501() }
		}
	} else {
		myManager.schedule( 15 ) { respawn501() }
	}
}

//do initial spawns so respawn routine initial checks work
LeftGuardMushroom.spawnAllNow()
majorCatastrophe = NorthwestOutpostLT.forceSpawnNow()
majorCatastrophe?.setDisplayName( "Major Catastrophe" )
runOnDeath( majorCatastrophe ) { event ->
	majorCatastropheHateList.addAll(event.actor.getHated())
	majorCatastropheHateList.each {
		if(isPlayer(it) && it.isOnQuest(89, 2) && !it.hasQuestFlag(GLOBAL, "majorCatastropheKilled") ) {
			it.removeMiniMapQuestLocation("Major Catastrophe")
			it.setQuestFlag(GLOBAL, "majorCatastropheKilled")
			it.addToPlayerVar( "lieutenantCount", 1 )
			it.centerPrint("Major Catastrophe defeated.")
		}
		//println "#### majorTragedyKilled = ${it.hasQuestFlag(GLOBAL, "majorTragedyKilled")} majorCatastropheKilled = ${it.hasQuestFlag(GLOBAL, "majorCatastropheKilled")} majorDisasterKilled = ${it.hasQuestFlag(GLOBAL, "majorDisasterKilled")} ####"
		if(isPlayer(it) && it.isOnQuest(89, 2) && it.hasQuestFlag(GLOBAL, "majorTragedyKilled") && it.hasQuestFlag(GLOBAL, "majorDisasterKilled") ) {
			it.updateQuest(89, "Leon-VQS")
		}
	}
}
//start it up!
respawn501() 

//------------------------------------------
// MAIN OUTPOST MUSHROOM                    
// (LEFT AND RIGHT SPAWNERS)                
//------------------------------------------

OutpostNorthLeft = myRooms.VILLAGE_402.spawnSpawner( "OutpostNorthLeft", "lawn_gnome", 3 )
OutpostNorthLeft.setPos( 820, 480 )
OutpostNorthLeft.setWanderBehaviorForChildren( 50, 150, 2, 5, 350)
OutpostNorthLeft.setWaitTime( 1, 2 )
OutpostNorthLeft.setCFH( 0 )
OutpostNorthLeft.setRotationForChildren( 270 ) //face west when spawned
OutpostNorthLeft.setMonsterLevelForChildren( 1.4 )

OutpostNorthRight = myRooms.VILLAGE_402.spawnSpawner( "OutpostNorthRight", "lawn_gnome", 3 )
OutpostNorthRight.setPos( 580, 290 )
OutpostNorthRight.setWanderBehaviorForChildren( 50, 150, 2, 5, 350)
OutpostNorthRight.setWaitTime( 1, 2 )
OutpostNorthRight.setCFH( 0 )
OutpostNorthRight.setRotationForChildren( 90 ) //face east when spawned
OutpostNorthRight.setMonsterLevelForChildren( 1.4 )

//=====================
//Camp Spawning Logic  
//=====================

majorTragedy = null

def respawn402() {
	if( myRooms.VILLAGE_402.getSpawnTypeCount( "lawn_gnome" ) == 0 && majorTragedy.isDead() ) {
		myManager.schedule( random( 50, 70 ) ) { 
			OutpostNorthLeft.spawnAllNow()
			OutpostNorthRight.spawnAllNow()
			majorTragedy = NorthOutpostLT.forceSpawnNow()
			majorTragedy.setDisplayName( "Major Tragedy" )
			majorTragedyHateList = []
			runOnDeath( majorTragedy ) { event ->
				majorTragedyHateList.addAll(event.actor.getHated())
				majorTragedyHateList.each {
					if(isPlayer(it) && it.isOnQuest(89, 2) && !it.hasQuestFlag(GLOBAL, "majorTragedyKilled") ) {
						it.removeMiniMapQuestLocation("Major Tragedy")
						it.setQuestFlag(GLOBAL, "majorTragedyKilled")
						it.addToPlayerVar( "lieutenantCount", 1 )
						it.centerPrint("Major Tragedy defeated.")
					}
					//println "#### majorTragedyKilled = ${it.hasQuestFlag(GLOBAL, "majorTragedyKilled")} majorCatastropheKilled = ${it.hasQuestFlag(GLOBAL, "majorCatastropheKilled")} majorDisasterKilled = ${it.hasQuestFlag(GLOBAL, "majorDisasterKilled")} ####"
					if(isPlayer(it) && it.isOnQuest(89, 2) && it.hasQuestFlag(GLOBAL, "majorCatastropheKilled") && it.hasQuestFlag(GLOBAL, "majorDisasterKilled") ) {
						it.updateQuest(89, "Leon-VQS")
					}
				}
			}
			myManager.schedule( 15 ) { respawn402() }
		}
	} else {
		myManager.schedule( 15 ) { respawn402() }
	}
}

//do initial spawns so respawn routine initial checks work
OutpostNorthLeft.spawnAllNow()
OutpostNorthRight.spawnAllNow()
majorTragedy = NorthOutpostLT.forceSpawnNow()
majorTragedy?.setDisplayName( "Major Tragedy" )
runOnDeath( majorTragedy ) { event ->
	majorTragedyHateList.addAll(event.actor.getHated())
	majorTragedyHateList.each {
		if(isPlayer(it) && it.isOnQuest(89, 2) && !it.hasQuestFlag(GLOBAL, "majorTragedyKilled") ) {
			it.removeMiniMapQuestLocation("Major Tragedy")
			it.setQuestFlag(GLOBAL, "majorTragedyKilled")
			it.addToPlayerVar( "lieutenantCount", 1 )
			it.centerPrint("Major Tragedy defeated.")
		}
		//println "#### majorTragedyKilled = ${it.hasQuestFlag(GLOBAL, "majorTragedyKilled")} majorCatastropheKilled = ${it.hasQuestFlag(GLOBAL, "majorCatastropheKilled")} majorDisasterKilled = ${it.hasQuestFlag(GLOBAL, "majorDisasterKilled")} ####"
		if(isPlayer(it) && it.isOnQuest(89, 2) && it.hasQuestFlag(GLOBAL, "majorCatastropheKilled") && it.hasQuestFlag(GLOBAL, "majorDisasterKilled") ) {
			it.updateQuest(89, "Leon-VQS")
		}
	}
}
//start it up!
respawn402() 

//------------------------------------------
// ROAD GUARDS                              
//------------------------------------------

//Road Guard One alerts Left Guard Mushroom and the Road Guard Two alerts the Right Guard Mushroom
def roadGuardOne = myRooms.VILLAGE_501.spawnSpawner( "roadGuardOne", "pink_flamingo", 1 )
roadGuardOne.setPos( 780, 200 )
roadGuardOne.setHateRadiusForChildren( 1500 )
roadGuardOne.setWaitTime( 50, 70 )
roadGuardOne.setHome( "VILLAGE_502", 485, 400 )
roadGuardOne.setGuardPostForChildren( "VILLAGE_502", 485, 400, 45 )
roadGuardOne.setCFH( 400 )
roadGuardOne.setMonsterLevelForChildren( 1.6 )

def roadGuardTwo = myRooms.VILLAGE_501.spawnSpawner( "windmillGuardTwo", "pink_flamingo", 1 )
roadGuardTwo.setPos( 780, 200 )
roadGuardTwo.setHateRadiusForChildren( 1500 )
roadGuardTwo.setWaitTime( 50, 70 )
roadGuardTwo.setHome( "VILLAGE_502", 700, 265 )
roadGuardTwo.setGuardPostForChildren( "VILLAGE_502", 700, 265, 45 )
roadGuardTwo.setCFH( 400 )
roadGuardTwo.setMonsterLevelForChildren( 1.6 )

//OUTPOST NORTH ALLIANCES
roadGuardOne.allyWithSpawner( roadGuardTwo )

//SERVER STARTUP COMMANDS

RightGuardMushroomTop.stopSpawning()
RightGuardMushroomBottom.stopSpawning()
LeftGuardMushroom.stopSpawning()
OutpostNorthLeft.stopSpawning()
OutpostNorthRight.stopSpawning()
NortheastOutpostLT.stopSpawning()
NorthwestOutpostLT.stopSpawning()
NorthOutpostLT.stopSpawning()

roadGuardOne.forceSpawnNow()
roadGuardTwo.forceSpawnNow()

majorDisasterHateList = []
majorTragedyHateList = []
majorCatastropheHateList = []