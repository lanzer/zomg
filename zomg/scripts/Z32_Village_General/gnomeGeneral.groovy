//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//Variables
monsterLevel = 1.6
generalLightWaveTimer = 15
generalHeavyWaveTimer = 25
generalDifficulty = null
generalWaveCounter = 0
generalActive = false
generalInitialized = false
allowMushroom1Attack = false
allowMushroom2Attack = false
allowGeneralAttack = false
spawningWave = false

//Constants
GENERAL_START = new Object()
GENERAL_INITIALIZE = new Object()
PHASE_ONE_LIMIT = 5
PHASE_TWO_LIMIT = 10
MAX_MONSTER_LEVEL = 2.5
MAX_PLAYER_LEVEL = 2.5
MUSHROOM_ATTACK_WAVE_1 = 1
MUSHROOM_ATTACK_WAVE_2 = 6
MUSHROOM_ATTACK_WAVE_3 = 11

//Specs to disable/enable attack
def mushroom1Spec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = allowMushroom1Attack == true
		if( player == attacker && !allowed ) { player.centerPrint( "The Mushroom Turret's cannons are retracted and your attacks bounce of its hardened top." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

def mushroom2Spec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = allowMushroom2Attack == true
		if( player == attacker && !allowed ) { player.centerPrint( "The Mushroom Turret's cannons are retracted and your attacks bounce of its hardened top." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

def generalSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer(attacker) ? attacker : target
		def allowed = allowGeneralAttack == true
		if(player == attacker && !allowed) {
			player.centerPrint("General Mayhem blocks your attack and shoves you away forcefully.")
			knockPlayerBack(player)
		}
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

//Spawners
turretSpawner1 = myRooms.VillageGeneral_1.spawnStoppedSpawner( "turretSpawner1", "mushroom_turret_general", 1 )
turretSpawner1.setMiniEventSpec(mushroom1Spec)
turretSpawner1.setPos( 365, 250 )
turretSpawner1.setRotationForChildren( 315 )
turretSpawner1.setMonsterLevelForChildren( monsterLevel )

turretSpawner2 = myRooms.VillageGeneral_1.spawnStoppedSpawner( "turretSpawner2", "mushroom_turret_general", 1 )
turretSpawner2.setMiniEventSpec(mushroom2Spec)
turretSpawner2.setPos( 560, 290 )
turretSpawner2.setRotationForChildren( 315 )
turretSpawner2.setMonsterLevelForChildren( monsterLevel )

generalSpawner = myRooms.VillageGeneral_1.spawnStoppedSpawner( "generalSpawner", "lawn_gnome_demi", 1 )
generalSpawner.setMiniEventSpec(generalSpec)
generalSpawner.setPos( 485, 210 )
generalSpawner.setGuardPostForChildren("VillageGeneral_1", 485, 210, 115)
generalSpawner.setMonsterLevelForChildren( monsterLevel )

gnomeSpawner1 = myRooms.VillageGeneral_1.spawnStoppedSpawner("gnomeSpawner1", "lawn_gnome", 50)
gnomeSpawner1.setPos(300, 470)
gnomeSpawner1.setMonsterLevelForChildren(monsterLevel)

gnomeSpawner2 = myRooms.VillageGeneral_1.spawnStoppedSpawner("gnomeSpawner2", "lawn_gnome", 50)
gnomeSpawner2.setPos(400, 470)
gnomeSpawner2.setMonsterLevelForChildren(monsterLevel)

gnomeSpawner3 = myRooms.VillageGeneral_1.spawnStoppedSpawner("gnomeSpawner3", "lawn_gnome", 50)
gnomeSpawner3.setPos(500, 470)
gnomeSpawner3.setMonsterLevelForChildren(monsterLevel)

gnomeLTSpawner1 = myRooms.VillageGeneral_1.spawnStoppedSpawner("gnomeLTSpawner1", "lawn_gnome_LT", 50)
gnomeLTSpawner1.setPos(300, 470)
gnomeLTSpawner1.setMonsterLevelForChildren(monsterLevel)

gnomeLTSpawner2 = myRooms.VillageGeneral_1.spawnStoppedSpawner("gnomeLTSpawner2", "lawn_gnome_LT", 50)
gnomeLTSpawner2.setPos(400, 470)
gnomeLTSpawner2.setMonsterLevelForChildren(monsterLevel)

gnomeLTSpawner3 = myRooms.VillageGeneral_1.spawnStoppedSpawner("gnomeLTSpawner3", "lawn_gnome_LT", 50)
gnomeLTSpawner3.setPos(500, 470)
gnomeLTSpawner3.setMonsterLevelForChildren(monsterLevel)


//Lists and maps
playerList = [] as Set
generalSpawnerList = [turretSpawner1, turretSpawner2, generalSpawner, gnomeSpawner1, gnomeSpawner2, gnomeSpawner3, gnomeLTSpawner1, gnomeLTSpawner2, gnomeLTSpawner3]
generalLightSpawners = [gnomeSpawner1, gnomeSpawner2, gnomeSpawner3]
generalHeavySpawners = [gnomeLTSpawner1, gnomeLTSpawner2, gnomeLTSpawner3]
generalSpawnerMap = [(gnomeSpawner1):gnomeLTSpawner1, (gnomeSpawner2):gnomeLTSpawner2, (gnomeSpawner3):gnomeLTSpawner3]

//Trigger zone for staging - event begins when players move out of it
stagingArea = "stagingArea"
myRooms.VillageGeneral_1.createTriggerZone(stagingArea, 350, 390, 450, 490)

//Initial spawn and init
myManager.onEnter(myRooms.VillageGeneral_1) { event ->
	if(isPlayer(event.actor)) {	
		if(!playerList.contains(event.actor)) { playerList << event.actor }
		if(generalInitialized == false) {
			synchronized(GENERAL_INITIALIZE) {
				generalInitialized = true
			
				generalDifficulty = event.actor.getTeam().getAreaVar("Z32_Village_General", "Z32_General_Difficulty") //Lookup selected difficulty and set it
				if(generalDifficulty == 0) { generalDifficulty = 2 } //Default difficulty if none selected (for testing when warping in)
				
				event.actor.getCrew().clone().each() {
					if(it.getConLevel() > monsterLevel && it.getConLevel() <= MAX_MONSTER_LEVEL) {
						monsterLevel = it.getConLevel()
					} else if(it.getConLevel() > MAX_MONSTER_LEVEL) {
						monsterLevel = MAX_MONSTER_LEVEL
						if(it.getConLevel() > 2.6 && playerList.contains(it)) {
							it.centerPrint("You must be level 2.5 or lower for this task.")
							it.centerPrint("Click 'MENU' and select 'Change Level' to lower your level temporarily.")
							it.warp( "VILLAGE_901", 650, 630, 6 )
						}
					}
				}

				if(generalDifficulty == 1) { monsterLevel = monsterLevel - 0.6 }
				if(generalDifficulty == 3) { monsterLevel = monsterLevel + 0.5 }

				generalSpawnerList.each() {
					it.setMonsterLevelForEveryone(monsterLevel)
				}

				general = generalSpawner.forceSpawnNow()
				
				//general.setBaseMaxHP(75)
				//general.instantPercentHeal(100)
				
				myManager.onHealth(general) { health ->
					if(health.didTransition(66) && health.isDecrease()) {
						general.say("Faugh. Tis but a scratch. I've had worse from MacTaggert's weed wacker!")
					} else if(health.didTransition(33) && health.isDecrease()) {
						general.say("Y'pack a mean punch, but y've not beaten me yet! Have at ya!")
					}
				}

				if(generalDifficulty == 3) {
					turret1 = turretSpawner1.forceSpawnNow()
					turret2 = turretSpawner2.forceSpawnNow()
					
					//turret1.setBaseMaxHP(50)
					//turret2.setBaseMaxHP(50)
					
					//turret1.instantPercentHeal(100)
					//turret2.instantPercentHeal(100)
				
					myManager.onHealth(turret1) { health ->
						if(health.didTransition(66) && health.isDecrease() && allowMushroom1Attack == true && generalWaveCounter <= PHASE_ONE_LIMIT) {
							allowMushroom1Attack = false
							if(!turret1.isDead()) {
								turret1.dispose()
							}
							turret1 = turretSpawner1.forceSpawnNow()
							turret1.instantPercentDamage(33)
							
							myManager.onHealth(turret1) { health2 ->
								if(health2.didTransition(33) && health2.isDecrease() && allowMushroom1Attack == true && generalWaveCounter <= PHASE_TWO_LIMIT) {
									allowMushroom1Attack = false
									if(!turret1.isDead()) {
										turret1.dispose()
									}
									turret1 = turretSpawner1.forceSpawnNow()
									turret1.instantPercentDamage(66)
								}
							}
						}
						if(health.didTransition(33) && health.isDecrease() && allowMushroom1Attack == true && generalWaveCounter <= PHASE_TWO_LIMIT) {
							allowMushroom1Attack = false
							if(!turret1.isDead()) {
								turret1.dispose()
							}
							turret1 = turretSpawner1.forceSpawnNow()
							turret1.instantPercentDamage(66)
						}
					}
					myManager.onHealth(turret2) { health ->
						if(health.didTransition(66) && health.isDecrease() && allowMushroom2Attack == true && generalWaveCounter <= PHASE_ONE_LIMIT) {
							allowMushroom2Attack = false
							if(!turret2.isDead()) {
								turret2.dispose()
							}
							turret2 = turretSpawner2.forceSpawnNow()
							turret2.instantPercentDamage(33)
							
							myManager.onHealth(turret2) { health2 ->
								if(health2.didTransition(33) && health2.isDecrease() && allowMushroom2Attack == true && generalWaveCounter <= PHASE_TWO_LIMIT) {
									allowMushroom2Attack = false
									if(!turret2.isDead()) {
										turret2.dispose()
									}
									turret2 = turretSpawner2.forceSpawnNow()
									turret2.instantPercentDamage(66)
								}
							}
						}
						if(health.didTransition(33) && health.isDecrease() && allowMushroom2Attack == true && generalWaveCounter <= PHASE_TWO_LIMIT) {
							allowMushroom2Attack = false
							if(!turret2.isDead()) {
								turret2.dispose()
							}
							turret2 = turretSpawner2.forceSpawnNow()
							turret2.instantPercentDamage(66)
						}
					}
				}
			}
		}
	}
}

myManager.onExit(myRooms.VillageGeneral_1) { event ->
	if(playerList.contains(event.actor)) {
		playerList.remove(event.actor)
	}
}

//Event logic
myManager.onTriggerOut(myRooms.VillageGeneral_1, stagingArea) { event ->
	if(isPlayer(event.actor)) {
		synchronized(GENERAL_START) { //Synchronize the start so it only happens once
			if(generalActive == false) { //If general hasn't started, start it
				generalActive = true
				
				setGeneralLevel(event) //Start the first wave
				checkDazedStatus() //Initiate periodic Dazed check for warp out
				
				general.say("How dare ye enter my home?! Get'em, lads!")
			}
		}
	}
}

def setGeneralLevel(event) {
	monsterLevel = 1.6
	event.actor.getCrew().clone().each() {
		if(it.getConLevel() > monsterLevel && it.getConLevel() <= MAX_MONSTER_LEVEL) {
			monsterLevel = it.getConLevel()
		} else if(it.getConLevel() > MAX_MONSTER_LEVEL) {
			monsterLevel = MAX_MONSTER_LEVEL
			if(it.getConLevel() > 2.6 && playerList.contains(it)) {
				it.centerPrint("You must be level 2.5 or lower for this task.")
				it.centerPrint("Click 'MENU' and select 'Change Level' to lower your level temporarily.")
				it.warp( "VILLAGE_901", 650, 630, 6 )
			}
		}
	}
	if(generalDifficulty == 1) { monsterLevel = monsterLevel - 0.6 }
	if(generalDifficulty == 3) { monsterLevel = monsterLevel + 0.5 }
	
	generalSpawnerList.each() {
		it.setMonsterLevelForEveryone(monsterLevel)
	}
	
	nextGeneralWave(event)
}

def nextGeneralWave(event) {
	generalWaveCounter++ //Increment the wave counter
	crewSize = event.actor.getCrew().plus(playerList).unique().size()
	//crewSize = 6
	if(generalWaveCounter <= PHASE_ONE_LIMIT) { //Spawn appropriate light wave
		if(generalDifficulty == 1) { spawnLightWaveEasy(event) }
		if(generalDifficulty == 2) { spawnLightWaveMedium(event) }
		if(generalDifficulty == 3) { spawnLightWaveHard(event) }
	}
	if(generalWaveCounter > PHASE_ONE_LIMIT && generalWaveCounter <= PHASE_TWO_LIMIT) { //Spawn appropriate heavy wave
		if(generalDifficulty == 1) { spawnHeavyWaveEasy(event) }
		if(generalDifficulty == 2) { spawnHeavyWaveMedium(event) }
		if(generalDifficulty == 3) { spawnHeavyWaveHard(event) }
	}
	if(generalWaveCounter == PHASE_TWO_LIMIT + 1) { //Unlock the general
		unlockGeneral()
	}
	if(generalDifficulty == 3 && (generalWaveCounter == MUSHROOM_ATTACK_WAVE_1 || generalWaveCounter == MUSHROOM_ATTACK_WAVE_2 || generalWaveCounter == MUSHROOM_ATTACK_WAVE_3)) {
		allowMushroom1Attack = true
		allowMushroom2Attack = true
		
		hateListCleanUp()
		turret1.addHate(random(hateList), 1)
		turret2.addHate(random(hateList), 1)
	}
}

def spawnLightWaveEasy(event) {
	//myManager.schedule()
	if(crewSize <= 3) {
		lightSpawner1 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner1) //Remove from list so it isn't duplicated
		
		gnome1 = lightSpawner1.forceSpawnNow() //Spawn monster
		generalLightSpawners << lightSpawner1 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
	} else {
		lightSpawner1 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner1) //Remove from list so it isn't duplicated
		
		lightSpawner2 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner2) //Remove from list so it isn't duplicated
		
		gnome1 = lightSpawner1.forceSpawnNow() //Spawn monster
		gnome2 = lightSpawner2.forceSpawnNow() //Spawn monster
		
		generalLightSpawners << lightSpawner1 //Re-add to list for next wave
		generalLightSpawners << lightSpawner2 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome2) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
		gnome2.addHate(random(hateList), 1) //Add hate to a random player
	}
}

def spawnLightWaveMedium(event) {
	if(crewSize <= 3) {
		lightSpawner1 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner1) //Remove from list so it isn't duplicated
		
		lightSpawner2 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner2) //Remove from list so it isn't duplicated
		
		gnome1 = lightSpawner1.forceSpawnNow() //Spawn monster
		gnome2 = lightSpawner2.forceSpawnNow() //Spawn monster
		
		generalLightSpawners << lightSpawner1 //Re-add to list for next wave
		generalLightSpawners << lightSpawner2 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome2) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
		gnome2.addHate(random(hateList), 1) //Add hate to a random player
	} else {
		lightSpawner1 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner1) //Remove from list so it isn't duplicated
		
		lightSpawner2 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner2) //Remove from list so it isn't duplicated
		
		lightSpawner3 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner3) //Remove from list so it isn't duplicated
		
		gnome1 = lightSpawner1.forceSpawnNow() //Spawn monster
		gnome2 = lightSpawner2.forceSpawnNow() //Spawn monster
		gnome3 = lightSpawner3.forceSpawnNow() //Spawn monster
		
		generalLightSpawners << lightSpawner1 //Re-add to list for next wave
		generalLightSpawners << lightSpawner2 //Re-add to list for next wave
		generalLightSpawners << lightSpawner3 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome2) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome3) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
		gnome2.addHate(random(hateList), 1) //Add hate to a random player
		gnome3.addHate(random(hateList), 1) //Add hate to a random player
	}
}

def spawnLightWaveHard(event) {
	if(crewSize <= 3) {
		lightSpawner1 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner1) //Remove from list so it isn't duplicated
		
		lightSpawner2 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner2) //Remove from list so it isn't duplicated
		
		lightSpawner3 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner3) //Remove from list so it isn't duplicated
		
		gnome1 = lightSpawner1.forceSpawnNow() //Spawn monster
		gnome2 = lightSpawner2.forceSpawnNow() //Spawn monster
		gnome3 = lightSpawner3.forceSpawnNow() //Spawn monster
		
		generalLightSpawners << lightSpawner1 //Re-add to list for next wave
		generalLightSpawners << lightSpawner2 //Re-add to list for next wave
		generalLightSpawners << lightSpawner3 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome2) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome3) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
		gnome2.addHate(random(hateList), 1) //Add hate to a random player
		gnome3.addHate(random(hateList), 1) //Add hate to a random player
	} else {
		lightSpawner1 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner1) //Remove from list so it isn't duplicated
		
		lightSpawner2 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner2) //Remove from list so it isn't duplicated
		
		lightSpawner3 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(lightSpawner3) //Remove from list so it isn't duplicated

		gnome1 = lightSpawner1.forceSpawnNow() //Spawn monster
		gnome2 = lightSpawner2.forceSpawnNow() //Spawn monster
		gnome3 = generalSpawnerMap.get(lightSpawner3).forceSpawnNow() //Spawn monster
		
		generalLightSpawners << lightSpawner1 //Re-add to list for next wave
		generalLightSpawners << lightSpawner2 //Re-add to list for next wave
		generalLightSpawners << lightSpawner3 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome2) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome3) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
		gnome2.addHate(random(hateList), 1) //Add hate to a random player
		gnome3.addHate(random(hateList), 1) //Add hate to a random player
	}
}

def spawnHeavyWaveEasy(event) {
	if(crewSize <= 3) {
		heavySpawner1 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(heavySpawner1) //Remove from list so it isn't duplicated
		
		heavySpawner2 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(heavySpawner2) //Remove from list so it isn't duplicated
		
		gnome1 = heavySpawner1.forceSpawnNow() //Spawn monster
		gnome2 = heavySpawner2.forceSpawnNow() //Spawn monster
		
		generalLightSpawners << heavySpawner1 //Re-add to list for next wave
		generalLightSpawners << heavySpawner2 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome2) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
		gnome2.addHate(random(hateList), 1) //Add hate to a random player
	} else {
		heavySpawner1 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(heavySpawner1) //Remove from list so it isn't duplicated
		
		heavySpawner2 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(heavySpawner2) //Remove from list so it isn't duplicated
		
		heavySpawner3 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(heavySpawner3) //Remove from list so it isn't duplicated
		
		gnome1 = heavySpawner1.forceSpawnNow() //Spawn monster
		gnome2 = heavySpawner2.forceSpawnNow() //Spawn monster
		gnome3 = generalSpawnerMap.get(heavySpawner3).forceSpawnNow() //Spawn monster
		
		generalLightSpawners << heavySpawner1 //Re-add to list for next wave
		generalLightSpawners << heavySpawner2 //Re-add to list for next wave
		generalLightSpawners << heavySpawner3 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome2) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome3) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
		gnome2.addHate(random(hateList), 1) //Add hate to a random player
		gnome3.addHate(random(hateList), 1) //Add hate to a random player
		
	}
}

def spawnHeavyWaveMedium(event) {
	if(crewSize <= 3) {
		heavySpawner1 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(heavySpawner1) //Remove from list so it isn't duplicated
		
		heavySpawner2 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(heavySpawner2) //Remove from list so it isn't duplicated
		
		heavySpawner3 = random(generalLightSpawners) //Select the spawner
		generalLightSpawners.remove(heavySpawner3) //Remove from list so it isn't duplicated
		
		gnome1 = heavySpawner1.forceSpawnNow() //Spawn monster
		gnome2 = heavySpawner2.forceSpawnNow() //Spawn monster
		gnome3 = generalSpawnerMap.get(heavySpawner3).forceSpawnNow() //Spawn monster
		
		generalLightSpawners << heavySpawner1 //Re-add to list for next wave
		generalLightSpawners << heavySpawner2 //Re-add to list for next wave
		generalLightSpawners << heavySpawner3 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome2) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome3) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
		gnome2.addHate(random(hateList), 1) //Add hate to a random player
		gnome3.addHate(random(hateList), 1) //Add hate to a random player
	} else {
		heavySpawner1 = random(generalHeavySpawners) //Select the spawner
		generalHeavySpawners.remove(heavySpawner1) //Remove from list so it isn't duplicated
		
		heavySpawner2 = random(generalHeavySpawners) //Select the spawner
		generalHeavySpawners.remove(heavySpawner2) //Remove from the list so it isn't duplicated
		
		gnome1 = heavySpawner1.forceSpawnNow() //Spawn monster
		gnome2 = heavySpawner2.forceSpawnNow() //Spawn monster
		
		generalHeavySpawners << heavySpawner1 //Re-add to list for next wave
		generalHeavySpawners << heavySpawner2 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome2) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
		gnome2.addHate(random(hateList), 1) //Add hate to a random player
	}
}

def spawnHeavyWaveHard(event) {
	if(crewSize <= 3) {
		heavySpawner1 = random(generalHeavySpawners) //Select the spawner
		generalHeavySpawners.remove(heavySpawner1) //Remove from list so it isn't duplicated
		
		heavySpawner2 = random(generalHeavySpawners) //Select the spawner
		generalHeavySpawners.remove(heavySpawner2) //Remove from the list so it isn't duplicated
		
		gnome1 = heavySpawner1.forceSpawnNow() //Spawn monster
		gnome2 = heavySpawner2.forceSpawnNow() //Spawn monster
		
		generalHeavySpawners << heavySpawner1 //Re-add to list for next wave
		generalHeavySpawners << heavySpawner2 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome2) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
		gnome2.addHate(random(hateList), 1) //Add hate to a random player
	} else {
		heavySpawner1 = random(generalHeavySpawners) //Select the spawner
		generalHeavySpawners.remove(heavySpawner1) //Remove from list so it isn't duplicated
		
		heavySpawner2 = random(generalHeavySpawners) //Select the spawner
		generalHeavySpawners.remove(heavySpawner2) //Remove from the list so it isn't duplicated
		
		heavySpawner3 = random(generalHeavySpawners) //Select the spawner
		generalHeavySpawners.remove(heavySpawner3) //Remove from the list so it isn't duplicated
		
		gnome1 = heavySpawner1.forceSpawnNow() //Spawn monster
		gnome2 = heavySpawner2.forceSpawnNow() //Spawn monster
		gnome3 = heavySpawner3.forceSpawnNow() //Spawn monster
		
		generalHeavySpawners << heavySpawner1 //Re-add to list for next wave
		generalHeavySpawners << heavySpawner2 //Re-add to list for next wave
		generalHeavySpawners << heavySpawner3 //Re-add to list for next wave
		
		runOnDeath(gnome1) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome2) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		runOnDeath(gnome3) { endWaveCheck(event) } //Check for end of wave to schedule spawn of next
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		gnome1.addHate(random(hateList), 1) //Add hate to a random player
		gnome2.addHate(random(hateList), 1) //Add hate to a random player
		gnome3.addHate(random(hateList), 1) //Add hate to a random player
	}
}

def unlockGeneral() {
	general.say("I've na'had me coffee and I'm grumpy as hell. Who wants to die?") //General announce unlock
	myManager.schedule(1) {
		allowGeneralAttack = true //Unlock general
		
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		general.addHate(random(hateList), 1) //Add hate to a random player
	}
}

def knockPlayerBack(player) {
	knockBack(player, general, 600) //Step back, cos he's a fucking handful
}

def synchronized endWaveCheck(event) {
	if(gnomeSpawner1.spawnsInUse() + gnomeSpawner2.spawnsInUse() + gnomeSpawner3.spawnsInUse() + gnomeLTSpawner1.spawnsInUse() + gnomeLTSpawner2.spawnsInUse() + gnomeLTSpawner3.spawnsInUse() == 0 && spawningWave == false) {
		spawningWave = true
		if(generalWaveCounter <= PHASE_ONE_LIMIT) {
			generalLightWaveTimer = (6 / generalDifficulty).intValue()
			myManager.schedule(generalLightWaveTimer) { setGeneralLevel(event); spawningWave = false }
		}
		if(generalWaveCounter > PHASE_ONE_LIMIT && generalWaveCounter <= PHASE_TWO_LIMIT) {
			generalHeavyWaveTimer = (8 / generalDifficulty).intValue()
			myManager.schedule(generalHeavyWaveTimer) { setGeneralLevel(event); spawningWave = false }
		}
		if(generalWaveCounter == PHASE_TWO_LIMIT + 1) {
			setGeneralLevel(event)
		}
	}
}

def checkDazedStatus() {
	allDazed = false
	numDazed = 0
	playerList.clone().each { if( it.isDazed() ) { numDazed ++ } }
	if( numDazed >= playerList.size() ) { //If all players in room are dazed, warp them out
		allDazed = true
		myRooms.VillageGeneral_1.getActorList().each{
			if( isPlayer( it ) ) {
				it.centerPrint( "The General's assistants drag you from his quarters." )
				it.warp( "VILLAGE_901", 650, 630, 6 )
			}
		}			
	} else { myManager.schedule(5) { checkDazedStatus() } }
}

def hateListCleanUp() {
	hateList = playerList.clone()
	hateList.clone().each() {
		if(it.isDazed()) { hateList.remove(it) }
	}
}