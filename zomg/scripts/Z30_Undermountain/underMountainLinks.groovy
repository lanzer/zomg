//Script created by gfern

import switches.switchActions;
import switches.DungeonGen

//-----------------------------------------------------
//Constants & Variable Init
//-----------------------------------------------------
roomLinkCounter = 0;
areaInit = false;

//-----------------------------------------------------
//Switches
//-----------------------------------------------------
w = new switchActions(scriptObj:this);

//Intra-room switches and exit
apron1A = w.simpleWarp("apron1A", myRooms.Under_1, 240, 220, "M1_303", 725, 325);
cross405A = w.simpleWarp("cross405A", myRooms.Under_405, 440, 270, "Under_405", 730, 270);
cross405B = w.simpleWarp("cross405B", myRooms.Under_405, 700, 190, "Under_405", 410, 360);
cross407A = w.simpleWarp("cross407A", myRooms.Under_407, 1530, 750, "Under_407", 1500, 340);
cross407B = w.simpleWarp("cross407B", myRooms.Under_407, 1530, 340, "Under_407", 1500, 750);
cross601A = w.simpleWarp("cross601A", myRooms.Under_601, 450, 250, "Under_601", 730, 270);
cross601B = w.simpleWarp("cross601B", myRooms.Under_601, 670, 170, "Under_601", 420, 350);
cross603A = w.simpleWarp("cross603A", myRooms.Under_603, 450, 250, "Under_603", 730, 270);
cross603B = w.simpleWarp("cross603B", myRooms.Under_603, 670, 170, "Under_603", 420, 350);
cross605A = w.simpleWarp("cross605A", myRooms.Under_605, 1110, 250, "Under_605", 830, 270);
cross605B = w.simpleWarp("cross605B", myRooms.Under_605, 860, 170, "Under_605", 1130, 350);
cross607A = w.simpleWarp("cross607A", myRooms.Under_607, 20, 730, "Under_607", 70, 370);
cross607B = w.simpleWarp("cross607B", myRooms.Under_607, 20, 370, "Under_607", 70, 730);
cross801A = w.simpleWarp("cross801A", myRooms.Under_801, 1110, 250, "Under_801", 830, 270);
cross801B = w.simpleWarp("cross801B", myRooms.Under_801, 860, 170, "Under_801", 1130, 350);


//-----------------------------------------------------
//Room Linking Logic
//-----------------------------------------------------

def synchronized initRooms() {
	if(areaInit) {
		return false;
	}
	areaInit = true;

	def dungeonGen = new DungeonGen(myRooms, w)
	
	// Save the bypass info and remove from map
	def byPass = dungeonGen.under1Map.Under_1[0]
	dungeonGen.prune(dungeonGen.under1Map, byPass)
	
	def u1Exit = dungeonGen.linkNodes(dungeonGen.under1Map, dungeonGen.under1Map.Under_2[0])
	def u2Enter = dungeonGen.randomJoinedNode(dungeonGen.under2Map)

	// [rc] Special case warp from the Under_1 ledge to Under2
	def byPassSwitch = makeGeneric(byPass.switchName, myRooms[byPass.room], byPass.clickX, byPass.clickY);
	byPassSwitch.setUsable(true);
	byPassSwitch.setRange(150);
	byPassSwitch.onUse { event ->
		if(event.player.isDoneQuest(366)) {
			event.player.warp(u2Enter.room, u2Enter.targetX, u2Enter.targetY);
		} else {
			event.player.centerPrint("You are not yet attuned to this passageway's energy")
		}
	}

	

	w.warpNoAggro(u1Exit, u2Enter)
	
	setScriptObject("under1LinkBack", [under1Exit:u1Exit, under2Entrance:u2Enter])
}

def grueCheck() {
	if( ( gst() > 0 && gst() < 50 ) || ( gst() > 2350 && gst() < 2400 ) ) { //Check time
		grueRoll = random(1, 100); //Roll between 1 and 100
		if(grueRoll >= 98) { //Check if roll higher than 98
			myArea.getAllPlayers().clone().each {
				if(isOnline(it) && isInZone(it)) { //For each person on grueList, check that they are still in M1
					if(it.sortedEquippedRings.toString().contains("Solar Rays")) {
						it.centerPrint("You have entered a dark place. Rays of sunlight protect you.");
					} else {
						it.centerPrint("Oh, no! You have walked into the slavering fangs of a lurking grue!");
						it.smite();
					}
				}
			}
		} else {
			myManager.schedule(60) { grueCheck() }
		}
	} else {
		myManager.schedule(60) { grueCheck() }
	}
}

//Startup functions
myManager.schedule(60) { grueCheck() }

onZoneIn() { event ->
	initRooms();
}
