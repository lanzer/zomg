//Script created by gfern

Alastor = spawnNPC("[NPC] Alastor", myRooms.Under_1, 1110, 460);
Alastor.setRotation(120);
Alastor.setDisplayName("Alastor");

Deva = spawnNPC("[NPC] Deva", myRooms.Under_1, 850, 580);
Deva.setRotation(90);
Deva.setDisplayName("Deva");

/*Servius = spawnNPC("[NPC] Servius", myRooms.Under_1, 1410, 560);
Servius.setRotation(120);
Servius.setDisplayName("Servius");

Secundus = spawnNPC("[NPC], Secundus", myRooms.Under_1, 140, 790);
Secundus.setRotation(90);
Secundus.setDisplayName("Secundus");*/

//--------------------------------------------
//Enter the Shadow
//--------------------------------------------
def enterShadowGrant = Deva.createConversation("enterShadowGrant", true, "!QuestStarted_363", "!QuestCompleted_363");

def enterShadowGrant1 = [id:1];
enterShadowGrant1.npctext = "Oh, thank goodness! Finally someone who looks competent. We need help and we're paying well. Have a moment to talk?";
enterShadowGrant1.options = [];
enterShadowGrant1.options << [text:"Yes.", result:2];
enterShadowGrant1.options << [text:"No.", result:6];
enterShadowGrant.addDialog(enterShadowGrant1, Deva);

def enterShadowGrant2 = [id:2];
enterShadowGrant2.npctext = "Excellent! You may recognize my employer, Alastor, as the owner of the Barton Coliseum. He runs many other businesses in addition to that venture.";
enterShadowGrant2.result = 3;
enterShadowGrant.addDialog(enterShadowGrant2, Deva);

def enterShadowGrant3 = [id:3];
enterShadowGrant3.npctext = "As you might imagine, a man with such varied interests employs people of various talents to protect those interests. That is what brings us here today.";
enterShadowGrant3.result = 4;
enterShadowGrant.addDialog(enterShadowGrant3, Deva);

def enterShadowGrant4 = [id:4];
enterShadowGrant4.npctext = "One of my employer's most trusted... er, associates has gone out of contact. We intend to find this associate and are recruiting the help of all capable Gaians. Interested?";
enterShadowGrant4.options = [];
enterShadowGrant4.options << [text:"Yes.", result:5];
enterShadowGrant4.options << [text:"No.", result:6];
enterShadowGrant.addDialog(enterShadowGrant4, Deva);

def enterShadowGrant5 = [id:5];
enterShadowGrant5.npctext = "I thought you might say that. These caves are inhabited by vampires and werewolves. Before we begin our search in earnest we need to determine their strength... and whether or not you can handle it.";
enterShadowGrant5.quest = 363;
enterShadowGrant5.result = DONE;
enterShadowGrant.addDialog(enterShadowGrant5, Deva);

def enterShadowGrant6 = [id:6];
enterShadowGrant6.npctext = "Very well, if that's your choice. I'm sure someone else will be interested";
enterShadowGrant6.result = DONE
enterShadowGrant.addDialog(enterShadowGrant6, Deva);

def enterShadowActive = Deva.createConversation("enterShadowActive", true, "QuestStarted_363:2");

def enterShadowActive1 = [id:1];
enterShadowActive1.npctext = "Remember, we need your help in judging the strength of the inhabitants of these caves.";
enterShadowActive1.result = DONE;
enterShadowActive.addDialog(enterShadowActive1, Deva);

def enterShadowComplete = Deva.createConversation("enterShadowComplete", true, "QuestStarted_363:3");

def enterShadowComplete1 = [id:1];
enterShadowComplete1.npctext = "I knew you could do it. They all said you'd get killed like all the others... but I could tell you were different.";
enterShadowComplete1.result = 2;
enterShadowComplete.addDialog(enterShadowComplete1, Deva);

def enterShadowComplete2 = [id:2];
enterShadowComplete2.npctext = "Now that you've proven yourself, we can start expanding our search. Right now, we'll start with the first cave section.";
enterShadowComplete2.result = 3;
enterShadowComplete.addDialog(enterShadowComplete2, Deva);

def enterShadowComplete3 = [id:3];
enterShadowComplete3.npctext = "I'd like you to fight your way through the first cave section and search for Kamila - our misssing associate. Are you ready to begin?";
enterShadowComplete3.options = [];
enterShadowComplete3.options << [text:"Yes.", result:4];
enterShadowComplete3.options << [text:"No.", result:5];
enterShadowComplete.addDialog(enterShadowComplete3, Deva);

def enterShadowComplete4 = [id:4];
enterShadowComplete4.npctext = "Great, get to work.";
enterShadowComplete4.quest = 363
enterShadowComplete4.exec = { event -> event.player.updateQuest(364, "[NPC] Deva"); };
enterShadowComplete4.result = DONE;
enterShadowComplete.addDialog(enterShadowComplete4, Deva);

def enterShadowComplete5 = [id:5];
enterShadowComplete5.npctext = "Very well, let me know when you are ready.";
enterShadowComplete5.quest = 363;
enterShadowComplete5.result = DONE;
enterShadowComplete.addDialog(enterShadowComplete5, Deva);

def searchingShadowBreak = Deva.createConversation("searchingShadowBreak", true, "!QuestStarted_364", "!QuestCompleted_364", "QuestCompleted_363");

def searchingShadowBreak1 = [id:1];
searchingShadowBreak1.npctext = "You've returned. Are you ready to begin your search of the first cave section?";
searchingShadowBreak1.options = [];
searchingShadowBreak1.options << [text:"Yes.", result:2];
searchingShadowBreak1.options << [text:"No.", result:3];
searchingShadowBreak.addDialog(searchingShadowBreak1, Deva);

def searchingShadowBreak2 = [id:2];
searchingShadowBreak2.npctext = "Then go ahead and get started."
searchingShadowBreak2.quest = 364;
searchingShadowBreak2.result = DONE;
searchingShadowBreak.addDialog(searchingShadowBreak2, Deva);

def searchingShadowBreak3 = [id:3];
searchingShadowBreak3.npctext = "I hope you change your mind soon.";
searchingShadowBreak3.result = DONE;
searchingShadowBreak.addDialog(searchingShadowBreak3, Deva);

def searchingShadowActiveA = Deva.createConversation("searchingShadowActiveA", true, "QuestStarted_364:2");

def searchingShadowActiveA1 = [id:1];
searchingShadowActiveA1.npctext = "Haven't you begun your search yet?";
searchingShadowActiveA1.result = DONE;
searchingShadowActiveA.addDialog(searchingShadowActiveA1, Deva);

def searchingShadowActiveB = Deva.createConversation("searchingShadowActiveB", true, "QuestStarted_364:3");

def searchingShadowActiveB1 = [id:1];
searchingShadowActiveB1.npctext = "Haven't you begun your search yet?";
searchingShadowActiveB1.result = DONE;
searchingShadowActiveB.addDialog(searchingShadowActiveB1, Deva);

def searchingShadowActiveC = Deva.createConversation("searchingShadowActiveC", true, "QuestStarted_364:4");

def searchingShadowActiveC1 = [id:1];
searchingShadowActiveC1.npctext = "Haven't you begun your search yet?";
searchingShadowActiveC1.result = DONE;
searchingShadowActiveC.addDialog(searchingShadowActiveC1, Deva);

def searchingShadowComplete = Deva.createConversation("searchingShadowComplete", true, "QuestStarted_364:5");

def searchingShadowComplete1 = [id:1];
searchingShadowComplete1.npctext = "You found her? Why didn't you bring her back here?!";
searchingShadowComplete1.playertext = "She wasn't lost. She's building an army of vampires and werewolves.";
searchingShadowComplete1.result = 2;
searchingShadowComplete.addDialog(searchingShadowComplete1, Deva);

def searchingShadowComplete2 = [id:2];
searchingShadowComplete2.npctext = "What?! An army of vampires and werewolves would wreak havoc on Gaia. I'm... I'm not sure what to do about this. I think it's time you talked to Alastor.";
searchingShadowComplete2.quest = 364;
searchingShadowComplete2.exec = { event -> event.player.updateQuest(365, "[NPC] Deva"); };
searchingShadowComplete2.result = DONE;
searchingShadowComplete.addDialog(searchingShadowComplete2, Deva);

def bigManComplete = Alastor.createConversation("bigManComplete", true, "QuestStarted_365:2");

def bigManComplete1 = [id:1];
bigManComplete1.npctext = "Deva tells me you've been tracking Kamila down. Why didn't you bring her back?";
bigManComplete1.playertext = "She didn't seem interested in being rescued.";
bigManComplete1.result = 2;
bigManComplete.addDialog(bigManComplete1, Alastor);

def bigManComplete2 = [id:2];
bigManComplete2.npctext = "That doesn't make any sense. She's been missing for days. What is she interested in, if not a rescue.";
bigManComplete2.playertext = "Revenge, on you and on Gaia.";
bigManComplete2.result = 3;
bigManComplete.addDialog(bigManComplete2, Alastor);

def bigManComplete3 = [id:3];
bigManComplete3.npctext = "On me?! Why that ungrateful little... she'd still be rolling bums in an alley for her next meal if it wasn't for me!";
bigManComplete3.result = 4;
bigManComplete.addDialog(bigManComplete3, Alastor);

def bigManComplete4 = [id:4];
bigManComplete4.npctext = "What could she possibly have against me? It was I who housed her, fed her, and trained her to be my most trusted and effective negotiator.";
bigManComplete4.result = 5;
bigManComplete.addDialog(bigManComplete4, Alastor);

def bigManComplete5 = [id:5];
bigManComplete5.npctext = "I suppose if that ingrate plans to seek her revenge I should oblige her with a reason. I want you to destroy her army! Head back to the caves and kill at least 100 over her minions.";
bigManComplete5.options = [];
bigManComplete5.options << [text:"Yes.", result:6];
bigManComplete5.options << [text:"No.", result:7];
bigManComplete.addDialog(bigManComplete5, Alastor);

def bigManComplete6 = [id:6];
bigManComplete6.npctext = "I'll pay you 25 gold for every one you kill!";
bigManComplete6.quest = 365;
bigManComplete6.exec = { event -> event.player.updateQuest(366, "[NPC] Alastor"); };
bigManComplete6.result = DONE;
bigManComplete.addDialog(bigManComplete6, Alastor);

def bigManComplete7 = [id:7];
bigManComplete7.npctext = "Need to resupply? Come back when you're ready.";
bigManComplete7.quest = 365;
bigManComplete7.result = DONE;
bigManComplete.addDialog(bigManComplete7, Alastor);

def deeperDarknessBreak = Alastor.createConversation("deeperDarknessBreak", true, "!QuestStarted_366", "!QuestCompleted_366", "QuestCompleted_365");

def deeperDarknessBreak1 = [id:1];
deeperDarknessBreak1.npctext = "Are you ready to get moving against Kamila's army?";
deeperDarknessBreak1.options = [];
deeperDarknessBreak1.options << [text:"Yes.", result:2];
deeperDarknessBreak1.options << [text:"No.", result:3];
deeperDarknessBreak.addDialog(deeperDarknessBreak1, Alastor);

def deeperDarknessBreak2 = [id:2];
deeperDarknessBreak2.npctext = "Okay, get to work.";
deeperDarknessBreak2.quest = 366;
deeperDarknessBreak2.result = DONE;
deeperDarknessBreak.addDialog(deeperDarknessBreak2, Alastor);

def deeperDarknessBreak3 = [id:3];
deeperDarknessBreak3.npctext = "Hurry up. I'm not a patient man.";
deeperDarknessBreak3.result = DONE;
deeperDarknessBreak.addDialog(deeperDarknessBreak3, Alastor);

def deeperDarknessActive = Alastor.createConversation("deeperDarknessActive", true, "QuestStarted_366:2");

def deeperDarknessActive1 = [id:1];
deeperDarknessActive1.npctext = "I don't think you've taken out quite enough of her minions yet.";
deeperDarknessActive1.result = DONE;
deeperDarknessActive.addDialog(deeperDarknessActive1, Alastor);

def deeperDarknessComplete = Alastor.createConversation("deeperDarknessComplete", true, "QuestStarted_366:3");

def deeperDarknessComplete1 = [id:1];
deeperDarknessComplete1.npctext = "Great work, I can almost see Kamila writhing in fury right now. HAHAHA!";
deeperDarknessComplete1.result = 2;
deeperDarknessComplete.addDialog(deeperDarknessComplete1, Alastor);

def deeperDarknessComplete2 = [id:2];
deeperDarknessComplete2.npctext = "I did some thinking while you were gone. I'm not going to just sit here and wait for Kamila to come kill me, and I'm not going to let her army have its way with Gaia. She is my responsibility."
deeperDarknessComplete2.result = 3;
deeperDarknessComplete.addDialog(deeperDarknessComplete2, Alastor);

def deeperDarknessComplete3 = [id:3];
deeperDarknessComplete3.npctext = "I have a plan. Kamila is a vampire, right? So is most of her crew. I bet if I get enough samples of their blood in my lab we can figure out something to stop them cold.";
deeperDarknessComplete3.result = 4;
deeperDarknessComplete.addDialog(deeperDarknessComplete3, Alastor);

def deeperDarknessComplete4 = [id:4];
deeperDarknessComplete4.npctext = "We're going to need a lot of tissue samples, though. I'm going to send you out to collect them, ready?";
deeperDarknessComplete4.options = [];
deeperDarknessComplete4.options << [text:"Yes.", result:5];
deeperDarknessComplete4.options << [text:"No.", result:6];
deeperDarknessComplete.addDialog(deeperDarknessComplete4, Alastor);

def deeperDarknessComplete5 = [id:5];
deeperDarknessComplete5.npctext = "Good. Get busy and don't come back until you have 150 samples.";
deeperDarknessComplete5.quest = 366;
deeperDarknessComplete5.exec = { event -> event.player.updateQuest(367, "[NPC] Alastor"); };
deeperDarknessComplete5.result = DONE;
deeperDarknessComplete.addDialog(deeperDarknessComplete5, Alastor);

def deeperDarknessComplete6 = [id:6];
deeperDarknessComplete6.npctext = "Then I'll find someone who is.";
deeperDarknessComplete6.quest = 366;
deeperDarknessComplete6.result = DONE;
deeperDarknessComplete.addDialog(deeperDarknessComplete6, Alastor);

def bloodyReadyBreak = Alastor.createConversation("bloodyReadyBreak", true, "!QuestStarted_367", "!QuestCompleted_367", "QuestCompleted_366");

def bloodyReadyBreak1 = [id:1];
bloodyReadyBreak1.npctext = "Thought twice, eh? Money always helps people come around. Ready to start collecting samples?";
bloodyReadyBreak1.options = [];
bloodyReadyBreak1.options << [text:"Yes.", result:2];
bloodyReadyBreak1.options << [text:"No.", result:3];
bloodyReadyBreak.addDialog(bloodyReadyBreak1, Alastor);

def bloodyReadyBreak2 = [id:2];
bloodyReadyBreak2.npctext = "Come back when you have 150 samples.";
bloodyReadyBreak2.quest = 366;
bloodyReadyBreak2.result = DONE;
bloodyReadyBreak.addDialog(bloodyReadyBreak2, Alastor);

def bloodyReadyBreak3 = [id:3];
bloodyReadyBreak3.npctext = "Stop wasting my time.";
bloodyReadyBreak3.result = DONE;
bloodyReadyBreak.addDialog(bloodyReadyBreak3, Alastor);

def bloodyReadyActive = Alastor.createConversation("bloodyReadyActive", true, "QuestStarted_367:2");

def bloodyReadyActive1 = [id:1];
bloodyReadyActive1.npctext = "You don't have my samples and I don't have time.";
bloodyReadyActive1.result = DONE;
bloodyReadyActive.addDialog(bloodyReadyActive1, Alastor);

def bloodyReadyComplete = Alastor.createConversation("bloodyReadyComplete", true, "QuestStarted_367:3");

def bloodyReadyComplete1 = [id:1];
bloodyReadyComplete1.npctext = "Excellent! You've collected the samples. I'll get the labmonkeys on them right away. This should at least get them started.";
bloodyReadyComplete1.result = 2;
bloodyReadyComplete.addDialog(bloodyReadyComplete1, Alastor);

def bloodyReadyComplete2 = [id:2];
bloodyReadyComplete2.npctext = "Most of these samples you collected are from Kamila's lesser minions, but to be thorough we need to research her tougher minions too.";
bloodyReadyComplete2.result = 3;
bloodyReadyComplete.addDialog(bloodyReadyComplete2, Alastor);

def bloodyReadyComplete3 = [id:3];
bloodyReadyComplete3.npctext = "I need you to collect samples from Night Frights, Big Dogs, and Counts. Let's get busy, shall we?";
bloodyReadyComplete3.options = [];
bloodyReadyComplete3.options << [text:"Yes.", result:4];
bloodyReadyComplete3.options << [text:"No.", result:5];
bloodyReadyComplete.addDialog(bloodyReadyComplete3, Alastor);

def bloodyReadyComplete4 = [id:4];
bloodyReadyComplete4.npctext = "Then you'd better get to work. Remember, we need at least 20 samples.";
bloodyReadyComplete4.quest = 367;
bloodyReadyComplete4.exec = { event -> event.player.updateQuest(368, "[NPC] Alastor"); };
bloodyReadyComplete4.result = DONE;
bloodyReadyComplete.addDialog(bloodyReadyComplete4, Alastor);

def bloodyReadyComplete5 = [id:5];
bloodyReadyComplete5.npctext = "Fine. Go have a tinkle and come back.";
bloodyReadyComplete5.quest = 367;
bloodyReadyComplete5.result = DONE;
bloodyReadyComplete.addDialog(bloodyReadyComplete5, Alastor);

def pitchBlackBreak = Alastor.createConversation("pitchBlackBreak", true, "!QuestStarted_368", "!QuestCompleted_368", "QuestCompleted_367");

def pitchBlackBreak1 = [id:1];
pitchBlackBreak1.npctext = "So, ready to go after the big baddies now?";
pitchBlackBreak1.options = [];
pitchBlackBreak1.options << [text:"Yes.", result:2];
pitchBlackBreak1.options << [text:"No.", result:3];
pitchBlackBreak.addDialog(pitchBlackBreak1, Alastor);

def pitchBlackBreak2 = [id:2];
pitchBlackBreak2.npctext = "Good. Remember, I need 20 samples from Night Frights, Big Dogs, or Counts.";
pitchBlackBreak2.quest = 368;
pitchBlackBreak2.result = DONE;
pitchBlackBreak.addDialog(pitchBlackBreak2, Alastor);

def pitchBlackBreak3 = [id:3];
pitchBlackBreak3.npctext = "Then stop bothering me.";
pitchBlackBreak3.result = DONE;
pitchBlackBreak.addDialog(pitchBlackBreak3, Alastor);

def pitchBlackActive = Alastor.createConversation("pitchBlackActive", true, "QuestStarted_368:2");

pitchBlackActive1 = [id:1];
pitchBlackActive1.npctext = "Remember, you need to collect samples from Kamila's more powerful minions.";
pitchBlackActive1.result = DONE;
pitchBlackActive.addDialog(pitchBlackActive1, Alastor);

def pitchBlackComplete = Alastor.createConversation("pitchBlackComplete", true, "QuestStarted_368:3");

def pitchBlackComplete1 = [id:1];
pitchBlackComplete1.npctext = "Perfect timing, the lab just sent over the results of their work on the samples you provided. We're in luck, they found the vulnerability we were looking for.";
pitchBlackComplete1.result = 2;
pitchBlackComplete.addDialog(pitchBlackComplete1, Alastor);

def pitchBlackComplete2 = [id:2];
pitchBlackComplete2.npctext = "It's time for us to put an end to Kamila. Understand that this will be very dangerous. If you need it, Deva has extra work to help you power up your rings.";
pitchBlackComplete2.result = 3;
pitchBlackComplete.addDialog(pitchBlackComplete2, Alastor);

def pitchBlackComplete3 = [id:3];
pitchBlackComplete3.npctext = "I'll pick up the formula. Meet me in Kamila's crypt room when you're ready to fight her, okay?";
pitchBlackComplete3.options = [];
pitchBlackComplete3.options << [text:"Yes.", result:4];
pitchBlackComplete3.options << [text:"No.", result:5];
pitchBlackComplete.addDialog(pitchBlackComplete3, Alastor);

def pitchBlackComplete4 = [id:4];
pitchBlackComplete4.npctext = "Fine, I'll see you there.";
pitchBlackComplete4.quest = 368;
pitchBlackComplete4.exec = { event -> event.player.updateQuest(370, "[NPC] Alastor") }
pitchBlackComplete4.result = DONE;
pitchBlackComplete.addDialog(pitchBlackComplete4, Alastor);

def pitchBlackComplete5 = [id:5];
pitchBlackComplete5.npctext = "Well, let me know when you are ready for Kamila.";
pitchBlackComplete5.quest = 368;
pitchBlackComplete5.result = DONE;
pitchBlackComplete.addDialog(pitchBlackComplete5, Alastor);

def bloodlustBreak = Alastor.createConversation("bloodlustBreak", true, "!QuestStarted_370", "!QuestCompleted_370", "QuestCompleted_368");

def bloodlustBreak1 = [id:1];
bloodlustBreak1.npctext = "Ready to take on Kamila now?";
bloodlustBreak1.options = [];
bloodlustBreak1.options << [text:"Yes.", result:2];
bloodlustBreak1.options << [text:"No.", result:3];
bloodlustBreak.addDialog(bloodlustBreak1, Alastor);

def bloodlustBreak2 = [id:2];
bloodlustBreak2.npctext = "Alright, I'll pick up the formula. Meet me in Kamila's crypt room.";
bloodlustBreak2.quest = 370;
bloodlustBreak2.result = DONE;
bloodlustBreak.addDialog(bloodlustBreak2, Alastor);

def bloodlustBreak3 = [id:3];
bloodlustBreak3.npctext = "Let me know when you pull some courage together.";
bloodlustBreak3.result = DONE;
bloodlustBreak.addDialog(bloodlustBreak3, Alastor);

def bloodlustActive = Alastor.createConversation("bloodlustActive", true, "QuestStarted_370:2");

def bloodlustActive1 = [id:1];
bloodlustActive1.npctext = "Remember, meet me in Kamila's crypt room and we'll take her out together.";
bloodlustActive1.result = DONE;
bloodlustActive.addDialog(bloodlustActive1, Alastor);

def bloodlustPostKam = Alastor.createConversation("bloodlustPostKam", true, "QuestStarted:370:3");

def bloodlustPostKam1 = [id:1];
bloodlustPostKam1.npctext = "You've foiled my plans. I have no desire to speak to you.";
bloodlustPostKam1.result = DONE;
bloodlustPostKam.addDialog(bloodlustPostKam1, Alastor);

def unquenchableGrant = Deva.createConversation("unquenchableGrant", true, "!QuestStarted_369", "!QuestCompleted_370", "QuestCompleted_368");

def unquenchableGrant1 = [id:1];
unquenchableGrant1.npctext = "I thought I might be seeing you again. We still need tissue samples from Kamila's minions. Are you up for collecting some more?";
unquenchableGrant1.options = [];
unquenchableGrant1.options << [text:"Yes.", result:2];
unquenchableGrant1.options << [text:"No.", result:3];
unquenchableGrant.addDialog(unquenchableGrant1, Deva);

def unquenchableGrant2 = [id:2];
unquenchableGrant2.npctext = "Good. Come back after you've got them.";
unquenchableGrant2.quest = 369;
unquenchableGrant2.result = DONE;
unquenchableGrant.addDialog(unquenchableGrant2, Deva);

def unquenchableGrant3 = [id:3];
unquenchableGrant3.npctext = "Come back when you are.";
unquenchableGrant3.result = DONE;
unquenchableGrant.addDialog(unquenchableGrant3, Deva);

def unquenchableActive = Deva.createConversation("unquenchableActive", true, "QuestStarted_369:2");

def unquenchableActive1 = [id:1];
unquenchableActive1.npctext = "Please work on those tissue samples.";
unquenchableActive1.result = DONE;
unquenchableActive.addDialog(unquenchableActive1, Deva);

def unquenchableComplete = Deva.createConversation("unquenchableComplete", true, "QuestStarted_369:3", "!QuestCompleted_370");

def unquenchableComplete1 = [id:1];
unquenchableComplete1.npctext = "Great! You got the samples. We could still use more, want to make another round?";
unquenchableComplete1.options = []
unquenchableComplete1.options << [text:"Yes.", result:2];
unquenchableComplete1.options << [text:"No.", result:3];
unquenchableComplete.addDialog(unquenchableComplete1, Deva);

def unquenchableComplete2 = [id:2];
unquenchableComplete2.npctext = "Good. Come back after you've got them.";
unquenchableComplete2.quest = 369;
unquenchableComplete2.exec = { event -> event.player.updateQuest(369, "[NPC] Deva"); };
unquenchableComplete2.result = DONE;
unquenchableComplete.addDialog(unquenchableComplete2, Deva);

def unquenchableComplete3 = [id:3];
unquenchableComplete3.npctext = "Come back when you are.";
unquenchableComplete3.quest = 369;
unquenchableComplete3.result = DONE;
unquenchableComplete.addDialog(unquenchableComplete3, Deva);

def unquenchableOver = Deva.createConversation("unquenchableOver", true, "QuestStarted_369:3", "QuestCompleted_370");

def unquenchableOver1 = [id:1];
unquenchableOver1.npctext = "Thank you for collecting these samples. Unfortunately, my employer has informed me that we no longer require your services.";
unquenchableOver1.quest = 369;
unquenchableOver1.result = DONE;
unquenchableOver.addDialog(unquenchableOver1, Deva);

def devaOver = Deva.createConversation("devaOver", true, "QuestCompleted_370", "!QuestStarted_369");

def devaOver1 = [id:1];
devaOver1.npctext = "Thank you for your help. Unfortunately, my employer has informed me that we no longer require your services.";
devaOver1.result = DONE
devaOver.addDialog(devaOver1, Deva);

def alastorOver = Alastor.createConversation("alastorOver", true, "QuestCompleted_370");

def alastorOver1 = [id:1];
alastorOver1.npctext = "You've foiled my plans. I have no desire to speak to you.";
alastorOver1.result = DONE;
alastorOver.addDialog(alastorOver1, Alastor);