import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
//ORB SPILL (OLD AQUEDUCT)                  
//------------------------------------------

//Make the orbs collectible only at the appropriate CL for the zone
def orbSpillSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= 6.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 6.0 or below to collect the orbs." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

orb = createStoppedEventSpawner( "Aqueduct_5", "orb", "orb_aqueduct", 100 )
orb.setPos( 350, 100 )
orb.setMonsterLevelForChildren( 5.6 )
orb.setHomeTetherForChildren( 6000 )
orb.setMiniEventSpec( orbSpillSpec )
orb.setEdgeHinting( false )
orb.setTargetCycle( false )

//------------------------------------------
//EVENT TIMER                               
//------------------------------------------

stopSpawning = false

def startEventTimer() {
	zoneBroadcast( "123-VQS", "Massive Ghi Overcharge!", "There is a vast overcharge of latent lifeforce energy in the area, manifesting as Charge Orbs. Fascinating!" )
	makeZoneTimer("Orb Spill Timer", "0:15:0", "0:0:0").onCompletion( { event ->
		removeZoneTimer( "Orb Spill Timer" )
		removeZoneBroadcast( "Massive Ghi Overcharge!" )
		stopSpawning = true
		myManager.schedule(2) {
			zoneBroadcast( "123-VQS", "'Normality' Achieved", "The Ghi readings have resumed what now passes for normality. The manifestation of Charge Orbs has ceased." )
			orbDisposal()
			myManager.schedule(15) {
				removeZoneBroadcast( "'Normality' Achieved" )
				eventDone()
			}
		}
	} ).start()
}

//------------------------------------------
//SPAWNING LOGIC                            
//------------------------------------------

orbEventPlayerList = []
numOrbPositionsPerRoom = 4
zonePlayerList = []

//Get the names of all the players in the zone
def makePlayerList(){
	//Empty the check list and then look into all rooms to find all players, whether in a Crew or not
	zonePlayerList.clear()
	myRooms.Aqueduct_5.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_6.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_7.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_102.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_103.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_104.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_105.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_106.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_107.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_202.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_203.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_204.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_205.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_206.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_207.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_303.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_304.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_305.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_306.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_307.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_403.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_404.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_405.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_406.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_407.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_504.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_505.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_506.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Aqueduct_507.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
}

//Dispose of the orbs after the countdown is complete
def orbDisposal(){
	myRooms.Aqueduct_5.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_6.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_7.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_102.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_103.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_104.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_105.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_106.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_107.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_202.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_203.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_204.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_205.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_206.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_207.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_303.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_304.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_305.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_306.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_307.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_403.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_404.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_405.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_406.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_407.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_504.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_505.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_506.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
	myRooms.Aqueduct_507.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_aqueduct" ) { it.dispose() } }
}

aqueduct5Map = [ 1:[100, 600, 0], 2:[390, 90, 0], 3:[600, 660, 0], 4:[890, 80, 0], 5:"Aqueduct_5" ]
aqueduct6Map = [ 1:[190, 500, 0], 2:[480, 80, 0], 3:[750, 490, 0], 4:[980, 40, 0], 5:"Aqueduct_6" ]
aqueduct7Map = [ 1:[110, 320, 0], 2:[430, 500, 0], 3:[760, 60, 0], 4:[950, 530, 0], 5:"Aqueduct_7" ]
aqueduct102Map = [ 1:[280, 570, 0], 2:[580, 560, 0], 3:[730, 160, 0], 4:[940, 550, 0], 5:"Aqueduct_102" ]
aqueduct103Map = [ 1:[250, 500, 0], 2:[490, 310, 0], 3:[670, 530, 0], 4:[1000, 560, 0], 5:"Aqueduct_103" ]
aqueduct104Map = [ 1:[110, 230, 0], 2:[290, 640, 0], 3:[540, 250, 0], 4:[900, 640, 0], 5:"Aqueduct_104" ]
aqueduct105Map = [ 1:[130, 440, 0], 2:[500, 590, 0], 3:[780, 540, 0], 4:[1010, 540, 0], 5:"Aqueduct_105" ]
aqueduct106Map = [ 1:[80, 580, 0], 2:[360, 600, 0], 3:[520, 600, 0], 4:[840, 630, 0], 5:"Aqueduct_106" ]
aqueduct107Map = [ 1:[250, 150, 0], 2:[370, 370, 0], 3:[80, 510, 0], 4:[860, 260, 0], 5:"Aqueduct_107" ]
aqueduct202Map = [ 1:[490, 240, 0], 2:[620, 490, 0], 3:[720, 570, 0], 4:[1025, 150, 0], 5:"Aqueduct_202" ]
aqueduct203Map = [ 1:[220, 300, 0], 2:[490, 240, 0], 3:[750, 620, 0], 4:[970, 40, 0], 5:"Aqueduct_203" ]
aqueduct204Map = [ 1:[160, 80, 0], 2:[420, 620, 0], 3:[650, 650, 0], 4:[960, 160, 0], 5:"Aqueduct_204" ]
aqueduct205Map = [ 1:[220, 220, 0], 2:[40, 580, 0], 3:[450, 190, 0], 4:[910, 250, 0], 5:"Aqueduct_205" ]
aqueduct206Map = [ 1:[150, 180, 0], 2:[420, 300, 0], 3:[320, 410, 0], 4:[990, 280, 0], 5:"Aqueduct_206" ]
aqueduct207Map = [ 1:[150, 350, 0], 2:[310, 650, 0], 3:[700, 160, 0], 4:[950, 460, 0], 5:"Aqueduct_207" ]
aqueduct303Map = [ 1:[50, 420, 0], 2:[330, 640, 0], 3:[630, 460, 0], 4:[1010, 90, 0], 5:"Aqueduct_303" ]
aqueduct304Map = [ 1:[180, 410, 0], 2:[400, 280, 0], 3:[730, 510, 0], 4:[900, 440, 0], 5:"Aqueduct_304" ]
aqueduct305Map = [ 1:[50, 20, 0], 2:[290, 100, 0], 3:[785, 275, 0], 4:[600, 40, 0], 5:"Aqueduct_305" ]
aqueduct306Map = [ 1:[420, 420, 0], 2:[660, 660, 0], 3:[810, 240, 0], 4:[950, 590, 0], 5:"Aqueduct_306" ]
aqueduct307Map = [ 1:[220, 500, 0], 2:[320, 180, 0], 3:[550, 590, 0], 4:[820, 130, 0], 5:"Aqueduct_307" ]
aqueduct403Map = [ 1:[180, 390, 0], 2:[310, 600, 0], 3:[690, 240, 0], 4:[920, 580, 0], 5:"Aqueduct_403" ]
aqueduct404Map = [ 1:[180, 30, 0], 2:[330, 580, 0], 3:[705, 620, 0], 4:[820, 450, 0], 5:"Aqueduct_404" ]
aqueduct405Map = [ 1:[100, 390, 0], 2:[275, 15, 0], 3:[390, 520, 0], 4:[740, 380, 0], 5:"Aqueduct_405" ]
aqueduct406Map = [ 1:[290, 650, 0], 2:[520, 580, 0], 3:[690, 80, 0], 4:[950, 300, 0], 5:"Aqueduct_406" ]
aqueduct407Map = [ 1:[170, 570, 0], 2:[310, 340, 0], 3:[560, 230, 0], 4:[940, 220, 0], 5:"Aqueduct_407" ]
aqueduct504Map = [ 1:[100, 460, 0], 2:[380, 270, 0], 3:[750, 580, 0], 4:[1000, 480, 0], 5:"Aqueduct_504" ]
aqueduct505Map = [ 1:[30, 630, 0], 2:[430, 310, 0], 3:[730, 300, 0], 4:[890, 500, 0], 5:"Aqueduct_505" ]
aqueduct506Map = [ 1:[100, 100, 0], 2:[320, 490, 0], 3:[380, 250, 0], 4:[800, 260, 0], 5:"Aqueduct_506" ]
aqueduct507Map = [ 1:[50, 270, 0], 2:[290, 90, 0], 3:[510, 150, 0], 4:[900, 90, 0], 5:"Aqueduct_507" ]

roomList = [ aqueduct5Map, aqueduct6Map, aqueduct7Map, aqueduct102Map, aqueduct103Map, aqueduct104Map, aqueduct105Map, aqueduct106Map, aqueduct107Map, aqueduct202Map, aqueduct203Map, aqueduct204Map, aqueduct205Map, aqueduct206Map, aqueduct207Map, aqueduct303Map, aqueduct304Map, aqueduct305Map, aqueduct306Map, aqueduct307Map, aqueduct403Map, aqueduct404Map, aqueduct405Map, aqueduct406Map, aqueduct407Map, aqueduct504Map, aqueduct505Map, aqueduct506Map, aqueduct507Map ]

def countOrbs() {
	orbCount = 0
	//count the number of orbs in the zone
	orbCount = orbCount + myRooms.Aqueduct_5.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_6.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_7.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_102.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_103.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_104.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_105.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_106.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_107.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_202.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_203.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_204.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_205.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_206.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_207.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_303.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_304.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_305.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_306.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_307.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_403.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_404.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_405.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_406.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_407.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_504.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_505.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_506.getSpawnTypeCount( "orb_aqueduct" )
	orbCount = orbCount + myRooms.Aqueduct_507.getSpawnTypeCount( "orb_aqueduct" )
}

//NOTE: The min limit for orbs in this zone is 10 instead of 20 because there are far fewer rooms in Barton Town than in other zones. Too many orbs with too few players is more like orb "harvesting" instead of orb "hunting".
def setMaxOrbs() {
	maxOrbs = areaPlayerCount()
	if( areaPlayerCount() < 20 ) {
		maxOrbs = 20
	}
}

//After the orbs have been spawned, keep checking until five or less of them exist...then spawn another wave of orbs
def checkForOrbRespawn() {
	if( stopSpawning == true ) return
	countOrbs()
	if( orbCount <= 5 ) {
		setMaxOrbs()
		spawnOrbs()
	} else {
		myManager.schedule(30) { checkForOrbRespawn() }
	}
}

//randomly spawn orbs
def spawnOrbs() {
	countOrbs()
	//if the current number of orbs in the zone is less than "maxOrbs", then spawn an orb
	if( orbCount < maxOrbs ) {
		//find the room in the zone to warp to
		room = random( roomList )
		//figure out the spawn position within that room
		position = random( numOrbPositionsPerRoom )
		positionInfo = room.get( position )
		//now look in position 2 of the "positionInfo" to see if the orb is spawned yet or not. (0 = not spawned; 1 = spawned)
		spawnHolder = positionInfo.get(2)
		if( spawnHolder == 0 ) {
			//change the "0" to a "1"
			positionInfo.remove( 2 ) ///remove the "placeholder" element in the current list
			positionInfo.add( 1 ) //put "1" at the end
			//now update the Map with the new list
			room.remove( position )
			room.put( position, positionInfo )
			roomName = room.get(5)
			X = positionInfo.get(0)
			Y = positionInfo.get(1)
			//orb.setHomeForChildren( roomName, X, Y )
			//now spawn the orb
			orb.warp( roomName.toString(), X, Y )
			orb.setHomeForChildren( "${roomName}", X, Y )
			currentOrb = orb.forceSpawnNow()
			//when the orb is destroyed, reset its record to 0 so the orb can spawn again later
			runOnDeath( currentOrb, { event ->
				event.killer.addPlayerVar( "Z01OrbCounter", 1 )
				deathRoomName = event.actor.getRoomName()
				deathX = event.actor.getX().intValue()
				deathY = event.actor.getY().intValue()
				//search through all the maps to find the map with the correct room name in it
				roomList.clone().each{
					if( it.get(5) == deathRoomName ) {
						deathRoom = it
					}
				}
				//once the correct map is found, use the death X to make a match for which position to read
				seed = 1
				deathPosition = 0
				findCorrectPosition()
				deathPositionInfo = deathRoom.get( deathPosition )
				//now that we have the correct position, change out the info properly so an orb can spawn there again
				deathPositionInfo.remove(2) //remove the placeholder at the end of the list
				deathPositionInfo.add(0)
				//now update the deathRoom Map with the updated list
				deathRoom.remove( deathPosition)
				deathRoom.put( deathPosition, deathPositionInfo )
			} )
		}
		//now spawn another orb a quarter-second from now
		myManager.schedule( 1 ) { spawnOrbs() }
	//otherwise, delay a few seconds and try again
	} else {
		myManager.schedule( 30 ) { checkForOrbRespawn() } 
	}
}

def findCorrectPosition() {
	if( seed <= 4 ) {
		deathPositionInfo = deathRoom.get(seed)
		//if the first element in this list is the same place the orb "died", then that's the correct position in the map
		if( deathPositionInfo.get(0) >= deathX - 40 && deathPositionInfo.get(0) <= deathX + 40 ) { //check a wide margin in case the exact position is slightly different than the spawner
			deathPosition = seed
		}
		seed ++
		findCorrectPosition()
	}
}


//start it all up
myManager.schedule(3) { startEventTimer() }
myManager.schedule(3) { checkForOrbRespawn() }


