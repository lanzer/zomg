//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//-----------------------------------------------------------------------------------
//Exit Trigger Zones                                                                 
//-----------------------------------------------------------------------------------
v2Zone = "v2Zone"
myRooms.ZENGARDEN_2.createTriggerZone(v2Zone, 240, 60, 340, 160)

v5Zone = "v5Zone"
myRooms.ZENGARDEN_5.createTriggerZone(v5Zone, 800, 330, 900, 430)

v101Zone = "v101Zone"
myRooms.ZENGARDEN_101.createTriggerZone(v101Zone, 50, 10, 150, 110)

v102Zone = "v102Zone"
myRooms.ZENGARDEN_102.createTriggerZone(v102Zone, 900, 510, 1000, 610)

v104Zone = "v104Zone"
myRooms.ZENGARDEN_104.createTriggerZone(v104Zone, 430, 160, 530, 260)

v201Zone = "v201Zone"
myRooms.ZENGARDEN_201.createTriggerZone(v201Zone, 10, 310, 110, 410)

v204Zone = "v204Zone"
myRooms.ZENGARDEN_204.createTriggerZone(v204Zone, 620, 570, 720, 670)

v302Zone = "v302Zone"
myRooms.ZENGARDEN_302.createTriggerZone(v302Zone, 10, 590, 110, 690)

v403Zone = "v403Zone"
myRooms.ZENGARDEN_403.createTriggerZone(v403Zone, 470, 590, 570, 690)

v405Zone = "v405Zone"
myRooms.ZENGARDEN_405.createTriggerZone(v405Zone, 850, 500, 950, 600)

v501Zone = "v501Zone"
myRooms.ZENGARDEN_501.createTriggerZone(v501Zone, 20, 240, 120, 340)

v504Zone = "v504Zone"
myRooms.ZENGARDEN_504.createTriggerZone(v504Zone, 10, 240, 100, 340)

v602Zone = "v602Zone"
myRooms.ZENGARDEN_602.createTriggerZone(v602Zone, 10, 570, 110, 670)

v605Zone = "v605Zone"
myRooms.ZENGARDEN_605.createTriggerZone(v605Zone, 850, 240, 900, 340)

v701Zone = "v701Zone"
myRooms.ZENGARDEN_701.createTriggerZone(v701Zone, 30, 230, 130, 330)

v703Zone = "v703Zone"
myRooms.ZENGARDEN_703.createTriggerZone(v703Zone, 100, 570, 200, 670)

//-----------------------------------------------------------------------------------
//Spy Trigger Zones                                                                  
//-----------------------------------------------------------------------------------
shrineZone = "shrineZone"
myRooms.ZENGARDEN_603.createTriggerZone(shrineZone, 320, 390, 420, 490)

dollLairZone = "dollLairZone"
myRooms.ZENGARDEN_704.createTriggerZone(dollLairZone, 660, 450, 760, 550)

bartonGateZone = "bartonGateZone"
myRooms.ZENGARDEN_304.createTriggerZone(bartonGateZone, 410, 350, 510, 450)

wishTreeZone = "wishTreeZone"
myRooms.ZENGARDEN_204.createTriggerZone(wishTreeZone, 75, 500, 175, 600)

basskenArchZone = "basskenArchZone"
myRooms.ZENGARDEN_103.createTriggerZone(basskenArchZone, 650, 110, 750, 210)

bridgeZone = "bridgeZone"
myRooms.ZENGARDEN_302.createTriggerZone(bridgeZone, 720, 295, 820, 395)

ninjaBellZone = "ninjaBellZone"
myRooms.ZENGARDEN_101.createTriggerZone(ninjaBellZone, 850, 420, 950, 520)

//-----------------------------------------------------------------------------------
//Spawners                                                                           
//-----------------------------------------------------------------------------------
//Make Clutches invulnerable to players over CL 4.5
def clutchEventSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() < 4.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 4.0 or below to repel the Clutch spies." ); }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

patrolLeader2 = createStoppedEventSpawner("ZENGARDEN_2", "patrolLeader2", "clutch_LT", 100)
patrolLeader2.setDispositionForChildren("normal")
patrolLeader2.setMiniEventSpec(clutchEventSpec)
patrolLeader2.setPos(290, 110)
patrolLeader2.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader2.setHomeTetherForChildren(1000)
patrolLeader2.setSpawnWhenPlayersAreInRoom(true)
patrolLeader2.setMonsterLevelForChildren(4.0)

patroller2 = createStoppedEventSpawner("ZENGARDEN_2", "patroller2", "clutch", 100)
patroller2.setDispositionForChildren("normal")
patroller2.setMiniEventSpec(clutchEventSpec)
patroller2.setPos(290, 110)
patroller2.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller2.setHomeTetherForChildren(1000)
patroller2.setSpawnWhenPlayersAreInRoom(true)
patroller2.setMonsterLevelForChildren(4.0)

patrolLeader5 = createStoppedEventSpawner("ZENGARDEN_5", "patrolLeader5", "clutch_LT", 100)
patrolLeader5.setDispositionForChildren("normal")
patrolLeader5.setMiniEventSpec(clutchEventSpec)
patrolLeader5.setPos(850, 380)
patrolLeader5.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader5.setHomeTetherForChildren(1000)
patrolLeader5.setSpawnWhenPlayersAreInRoom(true)
patrolLeader5.setMonsterLevelForChildren(4.0)

patroller5 = createStoppedEventSpawner("ZENGARDEN_5", "patroller5", "clutch", 100)
patroller5.setDispositionForChildren("normal")
patroller5.setMiniEventSpec(clutchEventSpec)
patroller5.setPos(850, 380)
patroller5.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller5.setHomeTetherForChildren(1000)
patroller5.setSpawnWhenPlayersAreInRoom(true)
patroller5.setMonsterLevelForChildren(4.0)

patrolLeader101 = createStoppedEventSpawner("ZENGARDEN_101", "patrolLeader101", "clutch_LT", 100)
patrolLeader101.setDispositionForChildren("normal")
patrolLeader101.setMiniEventSpec(clutchEventSpec)
patrolLeader101.setPos(100, 60)
patrolLeader101.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader101.setHomeTetherForChildren(1000)
patrolLeader101.setSpawnWhenPlayersAreInRoom(true)
patrolLeader101.setMonsterLevelForChildren(4.0)

patroller101 = createStoppedEventSpawner("ZENGARDEN_101", "patroller101", "clutch", 100)
patroller101.setDispositionForChildren("normal")
patroller101.setMiniEventSpec(clutchEventSpec)
patroller101.setPos(100, 60)
patroller101.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller101.setHomeTetherForChildren(1000)
patroller101.setSpawnWhenPlayersAreInRoom(true)
patroller101.setMonsterLevelForChildren(4.0)

patrolLeader102 = createStoppedEventSpawner("ZENGARDEN_102", "patrolLeader102", "clutch_LT", 100)
patrolLeader102.setDispositionForChildren("normal")
patrolLeader102.setMiniEventSpec(clutchEventSpec)
patrolLeader102.setPos(950, 560)
patrolLeader102.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader102.setHomeTetherForChildren(1000)
patrolLeader102.setSpawnWhenPlayersAreInRoom(true)
patrolLeader102.setMonsterLevelForChildren(4.0)

patroller102 = createStoppedEventSpawner("ZENGARDEN_102", "patroller102", "clutch", 100)
patroller102.setDispositionForChildren("normal")
patroller102.setMiniEventSpec(clutchEventSpec)
patroller102.setPos(950, 560)
patroller102.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller102.setHomeTetherForChildren(1000)
patroller102.setSpawnWhenPlayersAreInRoom(true)
patroller102.setMonsterLevelForChildren(4.0)

patrolLeader104 = createStoppedEventSpawner("ZENGARDEN_104", "patrolLeader104", "clutch_LT", 100)
patrolLeader104.setDispositionForChildren("normal")
patrolLeader104.setMiniEventSpec(clutchEventSpec)
patrolLeader104.setPos(480, 210)
patrolLeader104.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader104.setHomeTetherForChildren(1000)
patrolLeader104.setSpawnWhenPlayersAreInRoom(true)
patrolLeader104.setMonsterLevelForChildren(4.0)

patroller104 = createStoppedEventSpawner("ZENGARDEN_104", "patroller104", "clutch", 100)
patroller104.setDispositionForChildren("normal")
patroller104.setMiniEventSpec(clutchEventSpec)
patroller104.setPos(480, 210)
patroller104.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller104.setHomeTetherForChildren(1000)
patroller104.setSpawnWhenPlayersAreInRoom(true)
patroller104.setMonsterLevelForChildren(4.0)

patrolLeader201 = createStoppedEventSpawner("ZENGARDEN_201", "patrolLeader201", "clutch_LT", 100)
patrolLeader201.setDispositionForChildren("normal")
patrolLeader201.setMiniEventSpec(clutchEventSpec)
patrolLeader201.setPos(60, 360)
patrolLeader201.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader201.setHomeTetherForChildren(1000)
patrolLeader201.setSpawnWhenPlayersAreInRoom(true)
patrolLeader201.setMonsterLevelForChildren(4.0)

patroller201 = createStoppedEventSpawner("ZENGARDEN_201", "patroller201", "clutch", 100)
patroller201.setDispositionForChildren("normal")
patroller201.setMiniEventSpec(clutchEventSpec)
patroller201.setPos(60, 360)
patroller201.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller201.setHomeTetherForChildren(1000)
patroller201.setSpawnWhenPlayersAreInRoom(true)
patroller201.setMonsterLevelForChildren(4.0)

patrolLeader204 = createStoppedEventSpawner("ZENGARDEN_204", "patrolLeader204", "clutch_LT", 100)
patrolLeader204.setDispositionForChildren("normal")
patrolLeader204.setMiniEventSpec(clutchEventSpec)
patrolLeader204.setPos(670, 630)
patrolLeader204.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader204.setHomeTetherForChildren(1000)
patrolLeader204.setSpawnWhenPlayersAreInRoom(true)
patrolLeader204.setMonsterLevelForChildren(4.0)

patroller204 = createStoppedEventSpawner("ZENGARDEN_204", "patroller204", "clutch", 100)
patroller204.setDispositionForChildren("normal")
patroller204.setMiniEventSpec(clutchEventSpec)
patroller204.setPos(670, 630)
patroller204.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller204.setHomeTetherForChildren(1000)
patroller204.setSpawnWhenPlayersAreInRoom(true)
patroller204.setMonsterLevelForChildren(4.0)

patrolLeader302 = createStoppedEventSpawner("ZENGARDEN_302", "patrolLeader302", "clutch_LT", 100)
patrolLeader302.setDispositionForChildren("normal")
patrolLeader302.setMiniEventSpec(clutchEventSpec)
patrolLeader302.setPos(60, 640)
patrolLeader302.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader302.setHomeTetherForChildren(1000)
patrolLeader302.setSpawnWhenPlayersAreInRoom(true)
patrolLeader302.setMonsterLevelForChildren(4.0)

patroller302 = createStoppedEventSpawner("ZENGARDEN_302", "patroller302", "clutch", 100)
patroller302.setDispositionForChildren("normal")
patroller302.setMiniEventSpec(clutchEventSpec)
patroller302.setPos(60, 640)
patroller302.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller302.setHomeTetherForChildren(1000)
patroller302.setSpawnWhenPlayersAreInRoom(true)
patroller302.setMonsterLevelForChildren(4.0)

patrolLeader403 = createStoppedEventSpawner("ZENGARDEN_403", "patrolLeader403", "clutch_LT", 100)
patrolLeader403.setDispositionForChildren("normal")
patrolLeader403.setMiniEventSpec(clutchEventSpec)
patrolLeader403.setPos(520, 640)
patrolLeader403.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader403.setHomeTetherForChildren(1000)
patrolLeader403.setSpawnWhenPlayersAreInRoom(true)
patrolLeader403.setMonsterLevelForChildren(4.0)

patroller403 = createStoppedEventSpawner("ZENGARDEN_403", "patroller403", "clutch", 100)
patroller403.setDispositionForChildren("normal")
patroller403.setMiniEventSpec(clutchEventSpec)
patroller403.setPos(520, 640)
patroller403.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller403.setHomeTetherForChildren(1000)
patroller403.setSpawnWhenPlayersAreInRoom(true)
patroller403.setMonsterLevelForChildren(4.0)

patrolLeader405 = createStoppedEventSpawner("ZENGARDEN_405", "patrolLeader405", "clutch_LT", 100)
patrolLeader405.setDispositionForChildren("normal")
patrolLeader405.setMiniEventSpec(clutchEventSpec)
patrolLeader405.setPos(900, 550)
patrolLeader405.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader405.setHomeTetherForChildren(1000)
patrolLeader405.setSpawnWhenPlayersAreInRoom(true)
patrolLeader405.setMonsterLevelForChildren(4.0)

patroller405 = createStoppedEventSpawner("ZENGARDEN_405", "patroller405", "clutch", 100)
patroller405.setDispositionForChildren("normal")
patroller405.setMiniEventSpec(clutchEventSpec)
patroller405.setPos(900, 550)
patroller405.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller405.setHomeTetherForChildren(1000)
patroller405.setSpawnWhenPlayersAreInRoom(true)
patroller405.setMonsterLevelForChildren(4.0)

patrolLeader501 = createStoppedEventSpawner("ZENGARDEN_501", "patrolLeader501", "clutch_LT", 100)
patrolLeader501.setDispositionForChildren("normal")
patrolLeader501.setMiniEventSpec(clutchEventSpec)
patrolLeader501.setPos(70, 290)
patrolLeader501.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader501.setHomeTetherForChildren(1000)
patrolLeader501.setSpawnWhenPlayersAreInRoom(true)
patrolLeader501.setMonsterLevelForChildren(4.0)

patroller501 = createStoppedEventSpawner("ZENGARDEN_501", "patroller501", "clutch", 100)
patroller501.setDispositionForChildren("normal")
patroller501.setMiniEventSpec(clutchEventSpec)
patroller501.setPos(70, 290)
patroller501.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller501.setHomeTetherForChildren(1000)
patroller501.setSpawnWhenPlayersAreInRoom(true)
patroller501.setMonsterLevelForChildren(4.0)

patrolLeader504 = createStoppedEventSpawner("ZENGARDEN_504", "patrolLeader504", "clutch_LT", 100)
patrolLeader504.setDispositionForChildren("normal")
patrolLeader504.setMiniEventSpec(clutchEventSpec)
patrolLeader504.setPos(60, 210)
patrolLeader504.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader504.setHomeTetherForChildren(1000)
patrolLeader504.setSpawnWhenPlayersAreInRoom(true)
patrolLeader504.setMonsterLevelForChildren(4.0)

patroller504 = createStoppedEventSpawner("ZENGARDEN_504", "patroller504", "clutch", 100)
patroller504.setDispositionForChildren("normal")
patroller504.setMiniEventSpec(clutchEventSpec)
patroller504.setPos(60, 210)
patroller504.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller504.setHomeTetherForChildren(1000)
patroller504.setSpawnWhenPlayersAreInRoom(true)
patroller504.setMonsterLevelForChildren(4.0)

patrolLeader602 = createStoppedEventSpawner("ZENGARDEN_602", "patrolLeader602", "clutch_LT", 100)
patrolLeader602.setDispositionForChildren("normal")
patrolLeader602.setMiniEventSpec(clutchEventSpec)
patrolLeader602.setPos(60, 620)
patrolLeader602.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader602.setHomeTetherForChildren(1000)
patrolLeader602.setSpawnWhenPlayersAreInRoom(true)
patrolLeader602.setMonsterLevelForChildren(4.0)

patroller602 = createStoppedEventSpawner("ZENGARDEN_602", "patroller602", "clutch", 100)
patroller602.setDispositionForChildren("normal")
patroller602.setMiniEventSpec(clutchEventSpec)
patroller602.setPos(60, 620)
patroller602.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller602.setHomeTetherForChildren(1000)
patroller602.setSpawnWhenPlayersAreInRoom(true)
patroller602.setMonsterLevelForChildren(4.0)

patrolLeader605 = createStoppedEventSpawner("ZENGARDEN_605", "patrolLeader605", "clutch_LT", 100)
patrolLeader605.setDispositionForChildren("normal")
patrolLeader605.setMiniEventSpec(clutchEventSpec)
patrolLeader605.setPos(900, 290)
patrolLeader605.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader605.setHomeTetherForChildren(1000)
patrolLeader605.setSpawnWhenPlayersAreInRoom(true)
patrolLeader605.setMonsterLevelForChildren(4.0)

patroller605 = createStoppedEventSpawner("ZENGARDEN_605", "patroller605", "clutch", 100)
patroller605.setDispositionForChildren("normal")
patroller605.setMiniEventSpec(clutchEventSpec)
patroller605.setPos(900, 290)
patroller605.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller605.setHomeTetherForChildren(1000)
patroller605.setSpawnWhenPlayersAreInRoom(true)
patroller605.setMonsterLevelForChildren(4.0)

patrolLeader701 = createStoppedEventSpawner("ZENGARDEN_701", "patrolLeader701", "clutch_LT", 100)
patrolLeader701.setDispositionForChildren("normal")
patrolLeader701.setMiniEventSpec(clutchEventSpec)
patrolLeader701.setPos(80, 280)
patrolLeader701.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader701.setHomeTetherForChildren(1000)
patrolLeader701.setSpawnWhenPlayersAreInRoom(true)
patrolLeader701.setMonsterLevelForChildren(4.0)

patroller701 = createStoppedEventSpawner("ZENGARDEN_701", "patroller701", "clutch", 100)
patroller701.setDispositionForChildren("normal")
patroller701.setMiniEventSpec(clutchEventSpec)
patroller701.setPos(80, 280)
patroller701.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller701.setHomeTetherForChildren(1000)
patroller701.setSpawnWhenPlayersAreInRoom(true)
patroller701.setMonsterLevelForChildren(4.0)

patrolLeader703 = createStoppedEventSpawner("ZENGARDEN_703", "patrolLeader703", "clutch_LT", 100)
patrolLeader703.setDispositionForChildren("normal")
patrolLeader703.setMiniEventSpec(clutchEventSpec)
patrolLeader703.setPos(150, 620)
patrolLeader703.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeader703.setHomeTetherForChildren(1000)
patrolLeader703.setSpawnWhenPlayersAreInRoom(true)
patrolLeader703.setMonsterLevelForChildren(4.0)

patroller703 = createStoppedEventSpawner("ZENGARDEN_703", "patroller703", "clutch", 100)
patroller703.setDispositionForChildren("normal")
patroller703.setMiniEventSpec(clutchEventSpec)
patroller703.setPos(150, 620)
patroller703.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller703.setHomeTetherForChildren(1000)
patroller703.setSpawnWhenPlayersAreInRoom(true)
patroller703.setMonsterLevelForChildren(4.0)

//-----------------------------------------------------------------------------------
//Maps and Lists for Spawn Info                                                      
//-----------------------------------------------------------------------------------
v2Map = [1:"ZENGARDEN_2", 2:290, 3:110]
v5Map = [1:"ZENGARDEN_5", 2:850, 3:380]
v101Map = [1:"ZENGARDEN_101", 2:100, 3:60]
v102Map = [1:"ZENGARDEN_102", 2:950, 3:560]
v104Map = [1:"ZENGARDEN_104", 2:480, 3:210]
v201Map = [1:"ZENGARDEN_201", 2:60, 3:360]
v204Map = [1:"ZENGARDEN_204", 2:670, 3:630]
v302Map = [1:"ZENGARDEN_302", 2:60, 3:640]
v403Map = [1:"ZENGARDEN_403", 2:520, 3:640]
v405Map = [1:"ZENGARDEN_405", 2:900, 3:550]
v501Map = [1:"ZENGARDEN_501", 2:70, 3:290]
v504Map = [1:"ZENGARDEN_504", 2:60, 3:210]
v602Map = [1:"ZENGARDEN_602", 2:60, 3:620]
v605Map = [1:"ZENGARDEN_605", 2:900, 3:290]
v701Map = [1:"ZENGARDEN_701", 2:80, 3:280]
v703Map = [1:"ZENGARDEN_703", 2:150, 3:620]

shrineMap = [1:"ZENGARDEN_603", 2:370, 3:440]
dollLairMap = [1:"ZENGARDEN_704", 2:710, 3:500]
bartonGateMap = [1:"ZENGARDEN_304", 2:460, 3:400]
wishTreeMap = [1:"ZENGARDEN_204", 2:125, 3:550]
basskenArchMap = [1:"ZENGARDEN_103", 2:700, 3:160]
bridgeMap = [1:"ZENGARDEN_302", 2:770, 3:345]
ninjaBellMap = [1:"ZENGARDEN_101", 2:900, 3:470]

spawnLocations = [patrolLeader2, patrolLeader5, patrolLeader101, patrolLeader102, patrolLeader104, patrolLeader201, patrolLeader204, patrolLeader302, patrolLeader501, patrolLeader504, patrolLeader602, patrolLeader605, patrolLeader701, patrolLeader703]
spawnMap = [(patrolLeader2):patroller2, (patrolLeader5):patroller5, (patrolLeader101):patroller101, (patrolLeader102):patroller102, (patrolLeader104):patroller104, (patrolLeader201):patroller201, (patrolLeader204):patroller204, (patrolLeader302):patroller302, (patrolLeader403):patroller403, (patrolLeader501):patroller501, (patrolLeader504):patroller504, (patrolLeader602):patroller602, (patrolLeader605):patroller605, (patrolLeader701):patroller701, (patrolLeader703):patroller703]
spyLocations = [shrineMap, dollLairMap, bartonGateMap, wishTreeMap, basskenArchMap, bridgeMap, ninjaBellMap]
exitLocations = [v2Map, v5Map, v101Map, v102Map, v104Map, v201Map, v204Map, v302Map, v403Map, v405Map, v501Map, v504Map, v602Map, v605Map, v701Map, v703Map]
exitMap = [(patrolLeader2):v2Map, (patrolLeader5):v5Map, (patrolLeader101):v101Map, (patrolLeader102):v102Map, (patrolLeader104):v104Map, (patrolLeader201):v201Map, (patrolLeader204):v204Map, (patrolLeader302):v302Map, (patrolLeader403):v403Map, (patrolLeader405):v405Map, (patrolLeader501):v501Map, (patrolLeader504):v504Map, (patrolLeader602):v602Map, (patrolLeader605):v605Map, (patrolLeader701):v701Map, (patrolLeader703):v703Map]
spawnerList = [patrolLeader2, patrolLeader5, patrolLeader101, patrolLeader102, patrolLeader104, patrolLeader201, patrolLeader204, patrolLeader302, patrolLeader501, patrolLeader504, patrolLeader602, patrolLeader605, patrolLeader701, patrolLeader703]

clutchSpawnerMap = new HashMap()
clutchPatrolMap = new HashMap()
clutchMap = new HashMap()

patrollerList = []
clutchKillerList = []
clutchSpawnedList = []
spiedList = []

//-----------------------------------------------------------------------------------
//Variable Init                                                                      
//-----------------------------------------------------------------------------------
spawnTime = true
spawnRoll = null
spawnLocation = null
patrolLocation = null
clutchCounter = 0
numberOfPlayers = 0
clutchSpiesKilled = 0

//-----------------------------------------------------------------------------------
//Spy Trigger Logic                                                                  
//-----------------------------------------------------------------------------------
myManager.onTriggerIn(myRooms.ZENGARDEN_603, shrineZone) { event ->
	//println "#### ${event.actor} entered trigger zone ####"
	if(clutchSpawnerMap.containsKey(event.actor) && !spiedList.contains(event.actor)) {
		spiedList << event.actor //add event.actor to a list of leadres that have been to their spy location
		//println "#### ${event.actor} is present in clutchSpawnerMap ####"
		//println "#### Leader's spawner = ${clutchSpawnerMap.get(event.actor).get(0)} ####"
		//println "#### Follower's spawner = ${clutchSpawnerMap.get(event.actor).get(1)} ####"
		//println "#### Number of spy locations remaining ${clutchPatrolMap.get(event.actor).size()} they are ${clutchPatrolMap.get(event.actor)} ####"
		if(clutchPatrolMap.get(event.actor).size() > 0){
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					spiedList.remove(event.actor)
					def nextLocation = random(clutchPatrolMap.get(event.actor))
					
					def nextHome = nextLocation.get(1)
					def nextX = nextLocation.get(2)
					def nextY = nextLocation.get(3)
					
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					
					//remove that the most recent location from the map
					clutchPatrolMap.get(event.actor).remove(nextLocation)
					//println "#### removed ${nextLocation} left with ${clutchPatrolMap.get(event.actor)} ####"
					
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
				}
			}
		} else {
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					def exitLocation = exitMap.get(clutchSpawnerMap.get(event.actor).get(0))
			
					def exitHome = exitLocation.get(1)
					def exitX = exitLocation.get(2)
					def exitY = exitLocation.get(3)
			
					//println "#### exitHome = ${exitHome} ####"
					//println "#### exitX = ${exitX} ####"
					//println "#### exitY = ${exitY} ####"
			
					//Set the leader and patroller to return to despawn
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(exitHome.toString(), exitX, exitY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(exitHome.toString(), exitX, exitY)
		
					//event.actor.setHome( new com.gaiaonline.mmo.battle.map.RealDestinationImpl(myRooms.ZENGARDEN_701, new java.awt.Point(400, 400), "") 
		
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
				}
			}
		}
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_704, dollLairZone) { event ->
	//println "#### ${event.actor} entered trigger zone ####"
	if(clutchSpawnerMap.containsKey(event.actor) && !spiedList.contains(event.actor)) {
		spiedList << event.actor //add event.actor to a list of leadres that have been to their spy location
		//println "#### ${event.actor} is present in clutchSpawnerMap ####"
		//println "#### Leader's spawner = ${clutchSpawnerMap.get(event.actor).get(0)} ####"
		//println "#### Follower's spawner = ${clutchSpawnerMap.get(event.actor).get(1)} ####"
		//println "#### Number of spy locations remaining ${clutchPatrolMap.get(event.actor).size()} they are ${clutchPatrolMap.get(event.actor)} ####"
		if(clutchPatrolMap.get(event.actor).size() > 0){
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					spiedList.remove(event.actor)
					def nextLocation = random(clutchPatrolMap.get(event.actor))
					
					def nextHome = nextLocation.get(1)
					def nextX = nextLocation.get(2)
					def nextY = nextLocation.get(3)
					
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					
					//remove that the most recent location from the map
					clutchPatrolMap.get(event.actor).remove(nextLocation)
					//println "#### removed ${nextLocation} left with ${clutchPatrolMap.get(event.actor)} ####"
					
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
				}
			}
		} else {
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					def exitLocation = exitMap.get(clutchSpawnerMap.get(event.actor).get(0))
			
					def exitHome = exitLocation.get(1)
					def exitX = exitLocation.get(2)
					def exitY = exitLocation.get(3)
			
					//println "#### exitHome = ${exitHome} ####"
					//println "#### exitX = ${exitX} ####"
					//println "#### exitY = ${exitY} ####"
			
					//Set the leader and patroller to return to despawn
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(exitHome.toString(), exitX, exitY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(exitHome.toString(), exitX, exitY)
		
					//event.actor.setHome( new com.gaiaonline.mmo.battle.map.RealDestinationImpl(myRooms.ZENGARDEN_701, new java.awt.Point(400, 400), "") 
		
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
				}
			}
		}
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_304, bartonGateZone) { event ->
	//println "#### ${event.actor} entered trigger zone ####"
	if(clutchSpawnerMap.containsKey(event.actor) && !spiedList.contains(event.actor)) {
		spiedList << event.actor //add event.actor to a list of leadres that have been to their spy location
		//println "#### ${event.actor} is present in clutchSpawnerMap ####"
		//println "#### Leader's spawner = ${clutchSpawnerMap.get(event.actor).get(0)} ####"
		//println "#### Follower's spawner = ${clutchSpawnerMap.get(event.actor).get(1)} ####"
		//println "#### Number of spy locations remaining ${clutchPatrolMap.get(event.actor).size()} they are ${clutchPatrolMap.get(event.actor)} ####"
		if(clutchPatrolMap.get(event.actor).size() > 0){
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					spiedList.remove(event.actor)
					def nextLocation = random(clutchPatrolMap.get(event.actor))
					
					def nextHome = nextLocation.get(1)
					def nextX = nextLocation.get(2)
					def nextY = nextLocation.get(3)
					
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					
					//remove that the most recent location from the map
					clutchPatrolMap.get(event.actor).remove(nextLocation)
					//println "#### removed ${nextLocation} left with ${clutchPatrolMap.get(event.actor)} ####"
					
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
				}
			}
		} else {
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					def exitLocation = exitMap.get(clutchSpawnerMap.get(event.actor).get(0))
			
					def exitHome = exitLocation.get(1)
					def exitX = exitLocation.get(2)
					def exitY = exitLocation.get(3)
			
					//println "#### exitHome = ${exitHome} ####"
					//println "#### exitX = ${exitX} ####"
					//println "#### exitY = ${exitY} ####"
			
					//Set the leader and patroller to return to despawn
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(exitHome.toString(), exitX, exitY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(exitHome.toString(), exitX, exitY)
		
					//event.actor.setHome( new com.gaiaonline.mmo.battle.map.RealDestinationImpl(myRooms.ZENGARDEN_701, new java.awt.Point(400, 400), "") 
		
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
				}
			}
		}
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_204, wishTreeZone) { event ->
	//println "#### ${event.actor} entered trigger zone ####"
	if(clutchSpawnerMap.containsKey(event.actor) && !spiedList.contains(event.actor)) {
		spiedList << event.actor //add event.actor to a list of leadres that have been to their spy location
		//println "#### ${event.actor} is present in clutchSpawnerMap ####"
		//println "#### Leader's spawner = ${clutchSpawnerMap.get(event.actor).get(0)} ####"
		//println "#### Follower's spawner = ${clutchSpawnerMap.get(event.actor).get(1)} ####"
		//println "#### Number of spy locations remaining ${clutchPatrolMap.get(event.actor).size()} they are ${clutchPatrolMap.get(event.actor)} ####"
		if(clutchPatrolMap.get(event.actor).size() > 0){
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					spiedList.remove(event.actor)
					def nextLocation = random(clutchPatrolMap.get(event.actor))
					
					def nextHome = nextLocation.get(1)
					def nextX = nextLocation.get(2)
					def nextY = nextLocation.get(3)
					
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					
					//remove that the most recent location from the map
					clutchPatrolMap.get(event.actor).remove(nextLocation)
					//println "#### removed ${nextLocation} left with ${clutchPatrolMap.get(event.actor)} ####"
					
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
				}
			}
		} else {
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					def exitLocation = exitMap.get(clutchSpawnerMap.get(event.actor).get(0))
			
					def exitHome = exitLocation.get(1)
					def exitX = exitLocation.get(2)
					def exitY = exitLocation.get(3)
			
					//println "#### exitHome = ${exitHome} ####"
					//println "#### exitX = ${exitX} ####"
					//println "#### exitY = ${exitY} ####"
			
					//Set the leader and patroller to return to despawn
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(exitHome.toString(), exitX, exitY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(exitHome.toString(), exitX, exitY)
		
					//event.actor.setHome( new com.gaiaonline.mmo.battle.map.RealDestinationImpl(myRooms.ZENGARDEN_701, new java.awt.Point(400, 400), "") 
		
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
				}
			}
		}
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_103, basskenArchZone) { event ->
	//println "#### ${event.actor} entered trigger zone ####"
	if(clutchSpawnerMap.containsKey(event.actor) && !spiedList.contains(event.actor)) {
		spiedList << event.actor //add event.actor to a list of leadres that have been to their spy location
		//println "#### ${event.actor} is present in clutchSpawnerMap ####"
		//println "#### Leader's spawner = ${clutchSpawnerMap.get(event.actor).get(0)} ####"
		//println "#### Follower's spawner = ${clutchSpawnerMap.get(event.actor).get(1)} ####"
		//println "#### Number of spy locations remaining ${clutchPatrolMap.get(event.actor).size()} they are ${clutchPatrolMap.get(event.actor)} ####"
		if(clutchPatrolMap.get(event.actor).size() > 0){
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					spiedList.remove(event.actor)
					def nextLocation = random(clutchPatrolMap.get(event.actor))
					
					def nextHome = nextLocation.get(1)
					def nextX = nextLocation.get(2)
					def nextY = nextLocation.get(3)
					
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					
					//remove that the most recent location from the map
					clutchPatrolMap.get(event.actor).remove(nextLocation)
					//println "#### removed ${nextLocation} left with ${clutchPatrolMap.get(event.actor)} ####"
					
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
				}
			}
		} else {
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					def exitLocation = exitMap.get(clutchSpawnerMap.get(event.actor).get(0))
			
					def exitHome = exitLocation.get(1)
					def exitX = exitLocation.get(2)
					def exitY = exitLocation.get(3)
			
					//println "#### exitHome = ${exitHome} ####"
					//println "#### exitX = ${exitX} ####"
					//println "#### exitY = ${exitY} ####"
			
					//Set the leader and patroller to return to despawn
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(exitHome.toString(), exitX, exitY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(exitHome.toString(), exitX, exitY)
		
					//event.actor.setHome( new com.gaiaonline.mmo.battle.map.RealDestinationImpl(myRooms.ZENGARDEN_701, new java.awt.Point(400, 400), "") 
		
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
				}
			}
		}
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_302, bridgeZone) { event ->
	//println "#### ${event.actor} entered trigger zone ####"
	if(clutchSpawnerMap.containsKey(event.actor) && !spiedList.contains(event.actor)) {
		spiedList << event.actor //add event.actor to a list of leadres that have been to their spy location
		//println "#### ${event.actor} is present in clutchSpawnerMap ####"
		//println "#### Leader's spawner = ${clutchSpawnerMap.get(event.actor).get(0)} ####"
		//println "#### Follower's spawner = ${clutchSpawnerMap.get(event.actor).get(1)} ####"
		//println "#### Number of spy locations remaining ${clutchPatrolMap.get(event.actor).size()} they are ${clutchPatrolMap.get(event.actor)} ####"
		if(clutchPatrolMap.get(event.actor).size() > 0){
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					spiedList.remove(event.actor)
					def nextLocation = random(clutchPatrolMap.get(event.actor))
					
					def nextHome = nextLocation.get(1)
					def nextX = nextLocation.get(2)
					def nextY = nextLocation.get(3)
					
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					
					//remove that the most recent location from the map
					clutchPatrolMap.get(event.actor).remove(nextLocation)
					//println "#### removed ${nextLocation} left with ${clutchPatrolMap.get(event.actor)} ####"
					
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
				}
			}
		} else {
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					def exitLocation = exitMap.get(clutchSpawnerMap.get(event.actor).get(0))
			
					def exitHome = exitLocation.get(1)
					def exitX = exitLocation.get(2)
					def exitY = exitLocation.get(3)
			
					//println "#### exitHome = ${exitHome} ####"
					//println "#### exitX = ${exitX} ####"
					//println "#### exitY = ${exitY} ####"
			
					//Set the leader and patroller to return to despawn
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(exitHome.toString(), exitX, exitY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(exitHome.toString(), exitX, exitY)
		
					//event.actor.setHome( new com.gaiaonline.mmo.battle.map.RealDestinationImpl(myRooms.ZENGARDEN_701, new java.awt.Point(400, 400), "") 
		
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
				}
			}
		}
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_101, ninjaBellZone) { event ->
	//println "#### ${event.actor} entered trigger zone ####"
	if(clutchSpawnerMap.containsKey(event.actor) && !spiedList.contains(event.actor)) {
		spiedList << event.actor //add event.actor to a list of leadres that have been to their spy location
		//println "#### ${event.actor} is present in clutchSpawnerMap ####"
		//println "#### Leader's spawner = ${clutchSpawnerMap.get(event.actor).get(0)} ####"
		//println "#### Follower's spawner = ${clutchSpawnerMap.get(event.actor).get(1)} ####"
		//println "#### Number of spy locations remaining ${clutchPatrolMap.get(event.actor).size()} they are ${clutchPatrolMap.get(event.actor)} ####"
		if(clutchPatrolMap.get(event.actor).size() > 0){
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					spiedList.remove(event.actor)
					def nextLocation = random(clutchPatrolMap.get(event.actor))
					
					def nextHome = nextLocation.get(1)
					def nextX = nextLocation.get(2)
					def nextY = nextLocation.get(3)
					
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(nextHome.toString(), nextX, nextY)
					
					//remove that the most recent location from the map
					clutchPatrolMap.get(event.actor).remove(nextLocation)
					//println "#### removed ${nextLocation} left with ${clutchPatrolMap.get(event.actor)} ####"
					
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${nextHome.toString()}, ${nextX}, ${nextY} ####"
				}
			}
		} else {
			myManager.schedule(random(5, 10)) {
				if(clutchSpawnerMap.get(event.actor) != null) {
					def exitLocation = exitMap.get(clutchSpawnerMap.get(event.actor).get(0))
			
					def exitHome = exitLocation.get(1)
					def exitX = exitLocation.get(2)
					def exitY = exitLocation.get(3)
			
					//println "#### exitHome = ${exitHome} ####"
					//println "#### exitX = ${exitX} ####"
					//println "#### exitY = ${exitY} ####"
			
					//Set the leader and patroller to return to despawn
					clutchSpawnerMap.get(event.actor).get(0).setHomeForEveryone(exitHome.toString(), exitX, exitY)
					clutchSpawnerMap.get(event.actor).get(1).setHomeForEveryone(exitHome.toString(), exitX, exitY)
		
					//event.actor.setHome( new com.gaiaonline.mmo.battle.map.RealDestinationImpl(myRooms.ZENGARDEN_701, new java.awt.Point(400, 400), "") 
		
					//println "#### ${clutchSpawnerMap.get(event.actor).get(0)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
					//println "#### ${clutchSpawnerMap.get(event.actor).get(1)} now has home ${exitHome.toString()}, ${exitX}, ${exitY} ####"
				}
			}
		}
	}
}

//-----------------------------------------------------------------------------------
//Exit Trigger Logic                                                                 
//-----------------------------------------------------------------------------------
myManager.onTriggerIn(myRooms.ZENGARDEN_2, v2Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader2)) {
			spawnLocations << patrolLeader2
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_5, v5Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader5)) {
			spawnLocations << patrolLeader5
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_101, v101Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader101)) {
			spawnLocations << patrolLeader101
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_102, v102Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader102)) {
			spawnLocations << patrolLeader102
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_104, v104Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader104)) {
			spawnLocations << patrolLeader104
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_201, v201Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader201)) {
			spawnLocations << patrolLeader201
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_204, v204Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader204)) {
			spawnLocations << patrolLeader204
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_302, v302Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader302)) {
			spawnLocations << patrolLeader302
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_403, v403Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader403)) {
			spawnLocations << patrolLeader403
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_405, v405Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader405)) {
			spawnLocations << patrolLeader405
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_501, v501Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader501)) {
			spawnLocations << patrolLeader501
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_504, v504Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader504)) {
			spawnLocations << patrolLeader504
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_602, v602Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader602)) {
			spawnLocations << patrolLeader602
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_605, v605Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader605)) {
			spawnLocations << patrolLeader605
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_701, v701Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader701)) {
			spawnLocations << patrolLeader701
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

myManager.onTriggerIn(myRooms.ZENGARDEN_703, v703Zone) { event ->
	//println "#### spiedList = ${spiedList} ####"
	//println "#### event.actor = ${event.actor} ####"
	if(spiedList.contains(event.actor)) {
		//println "#### ${event.actor} entered trigger zone ####"
		
		clutchSpawnerMap.remove(event.actor)
		if(!spawnLocations.contains(patrolLeader703)) {
			spawnLocations << patrolLeader703
		}
		event.actor.dispose()
		clutchMap.get(event.actor).clone().each() { //dispose the leader's escort
			it.dispose()
			//println "#### Disposing ${it} ####"
		}
		clutchMap.remove(event.actor)
	}
}

//-----------------------------------------------------------------------------------
//Spawning Logic                                                                     
//-----------------------------------------------------------------------------------
def startEvent() {
	println "#### Starting Clutch Infiltration ####"
	sound("scoobyBats").toZone()
	myManager.schedule(2) {
		clutchCounter = 0
		makeZoneCounter("Clutch Spies Defeated", 0).watchForKill("clutch", "clutch_LT")
		zoneBroadcast("Katsumi-VQS", "Fly By", "Clutches have begun infiltrating Zen Garden. We must defend the area from these wretches. Defeat any you see, I will try to discern their purpose.")
		spawnCheck()
	}
}

def completeEvent() {
	myManager.schedule(1) {
		clutchSpawnedList.clone().each() {
			//println "##### disposing ${it} ####"
			it?.dispose()
		}
	
		rewardModifier = getZoneCounter("Clutch Spies Defeated").getValue() / 150 + 1
		rewardModifier = rewardModifier * 10
		rewardModifier = Math.round(rewardModifier)
		rewardModifier = rewardModifier / 10
		println "#### rewardModifier = ${rewardModifier} ####"
	
		removeZoneBroadcast("Fly By")
		zoneBroadcast("Katsumi-VQS", "Fly, Bye!", "Well done, friends. Next time, Zhivago and his spies will think twice before defiling the Garden.")
		myManager.schedule(15) { removeZoneCounter("Clutch Spies Defeated"); removeZoneBroadcast("Fly, Bye!") }
	
		println "#*#*#* BEGIN CLUTCH PRINTOUT *#*#*#"
		println "#*#*#* clutchKillerList = ${clutchKillerList} *#*#*#"
		clutchKillerList.clone().each() {
			if(it != null && isOnline(it)) {
				numberOfPlayers++
				clutchSpiesKilled = clutchSpiesKilled + it.getPlayerVar("Z05_ClutchSpiesCaught").toInteger()
				println "#*#*#* PlayerID = ${it} *#*#*#"
				println "#*#*#* Clutches killed this event = ${it.getPlayerVar("Z05_ClutchSpiesCaught")} *#*#*#"
				println "#*#*#* Clutches killed all time = ${it.getPlayerVar("Z05_ClutchSpiesCaughtTotal")} *#*#*#"
				if(it.getPlayerVar("Z05_ClutchSpiesCaught") > 0 && it.getPlayerVar("Z05_ClutchSpiesCaught") < 10) { 
					tinyReward = random(1, 2) * rewardModifier
					tinyReward = Math.round(tinyReward)
			
					goldReward = random(5, 10) * rewardModifier
					goldReward =  Math.round(goldReward)
			
					it.centerPrint("Event complete! ${getZoneCounter("Clutch Spies Defeated").getValue()} clutches were stopped. All participants earn a ${rewardModifier}X loot reward modifier!")
					it.centerPrint("You killed ${it.getPlayerVar("Z05_ClutchSpiesCaught").toInteger()} spies and earned a tiny reward.")
					it.centerPrint("You receive ${goldReward} gold and ${tinyReward} orbs.")
					it.grantQuantityItem(100257, tinyReward)
					it.grantCoins(goldReward)
					lootChance = random(100)
					/*if(lootChance > 99) {
						it.grantItem("100441")
						it.centerPrint("You have received a Zen Charm as an additional reward!")
					}*/
				}
				if(it.getPlayerVar("Z05_ClutchSpiesCaught") >= 10 && it.getPlayerVar("Z05_ClutchSpiesCaught") < 35) {
					moderateReward = random(3, 6) * rewardModifier
					moderateReward = Math.round(moderateReward)
			
					goldReward = random(10, 15) * rewardModifier
					goldReward =  Math.round(goldReward)
			
					it.centerPrint("Event complete! ${getZoneCounter("Clutch Spies Defeated").getValue()} clutches were stopped. All participants earn a ${rewardModifier}X loot reward modifier!")
					it.centerPrint("You killed ${it.getPlayerVar("Z05_ClutchSpiesCaught").toInteger()} spies and earned a moderate reward.")
					it.centerPrint("You receive ${goldReward} gold and ${moderateReward} orbs.")
					it.grantQuantityItem(100257, moderateReward)
					it.grantCoins(goldReward)
					lootChance = random(100)
					/*if(lootChance > 80) {
						it.grantItem("100441")
						it.centerPrint("You have received a Zen Charm as an additional reward!")
					}*/
				}
				if(it.getPlayerVar("Z05_ClutchSpiesCaught") >= 35 && it.getPlayerVar("Z05_ClutchSpiesCaught") < 60) {
					largeReward = random(8, 12) * rewardModifier
					largeReward = Math.round(largeReward)
			
					goldReward = random(15, 25) * rewardModifier
					goldReward =  Math.round(goldReward)
			
					it.centerPrint("Event complete! ${getZoneCounter("Clutch Spies Defeated").getValue()} clutches were stopped. All participants earn a ${rewardModifier}X loot reward modifier!")
					it.centerPrint("You killed ${it.getPlayerVar("Z05_ClutchSpiesCaught").toInteger()} spies and earned a large reward.")
					it.centerPrint("You receive ${goldReward} gold and ${largeReward} orbs.")
					it.grantQuantityItem(100257, largeReward)
					it.grantCoins(goldReward)
					lootChance = random(100)
					/*if(lootChance > 65) {
						it.grantItem("100441")
						it.centerPrint("You have received a Zen Charm as an additional reward!")
					}*/
				}
				if(it.getPlayerVar("Z05_ClutchSpiesCaught") >= 60) {
					massiveReward = random(15, 20) * rewardModifier
					massiveReward = Math.round(massiveReward)
			
					goldReward = random(50, 60) * rewardModifier
					goldReward =  Math.round(goldReward)
			
					it.centerPrint("Event complete! ${getZoneCounter("Clutch Spies Defeated").getValue()} clutches were stopped. All participants earn a ${rewardModifier}X loot reward modifier!")
					it.centerPrint("You killed ${it.getPlayerVar("Z05_ClutchSpiesCaught").toInteger()} spies and earned a massive reward.")
					it.centerPrint("You receive ${goldReward} gold and ${massiveReward} orbs.")
					it.grantQuantityItem(100257, massiveReward)
					it.grantCoins(goldReward)
					lootChance = random(100)
					/*if(lootChance > 0) {
						it.grantItem("100441")
						it.centerPrint("You have received a Zen Charm as an additional reward!")
					}*/
				}
				if(it.getPlayerVar("Z05_ClutchSpiesCaughtTotal") >= 100 && !it.isDoneQuest(324)) {
					it.centerPrint("You have been awarded the Fly By Intruder badge.")
					it.updateQuest(324, "Katsumi-VQS")
				}
				if(it.getPlayerVar("Z05_ClutchSpiesCaughtTotal") >= 1000 && !it.isDoneQuest(325)) {
					it.centerPrint("You have been awarded the Clutch Infiltrator Assassin badge.")
					it.updateQuest(325, "Katsumi-VQS")
				}
			}
		}
		if(numberOfPlayers > 0) { println "*#*#*# Average = ${clutchSpiesKilled / numberOfPlayers} *#*#*#" }
		println "*#*#*# END CLUTCH PRINTOUT *#*#*#"
		myManager.schedule(15) { eventDone() }
	}
}

def spawnCheck() {
	time = gst()
	//println "#### clutchCounter = ${clutchCounter} ####"
	//println "#### spawnLocations size = ${spawnLocations.size()} ####"
	if(spawnTime == true && clutchCounter <= 147) { //if it's time for an event and less than 150 clutches have been spawned, spawn
		if(spawnLocations.size() > 0) { 
			spawnRandom() 
			if(clutchCounter == 60) {
				zoneBroadcast("Katsumi-VQS", "Sacred Scouts", "They seem to be scouting the most sacred areas of the Garden, what could they be up to?")
				myManager.schedule(30) { removeZoneBroadcast("Sacred Scouts") }
			}
			if(clutchCounter == 90) {
				zoneBroadcast("Katsumi-VQS", "Cursed Vampire", "Zhivago! Of course, these are his minions! We cannot tolerate spies of the defiler in the Garden. Gaians, expell these Clutches!")
				myManager.schedule(30) { removeZoneBroadcast("Cursed Vampire") }
			}
			if(clutchCounter == 120) {
				zoneBroadcast("Katsumi-VQS", "Spy Awry", "Yes! They are beginning to tire. Hurry, friends, we must push the last few spies out of the Garden!")
				myManager.schedule(30) { removeZoneBroadcast("Spy Awry") }
			}
		} else {
			myManager.schedule(20) { spawnCheck() }
		}
	} else if(spawnTime == true && clutchCounter >= 150) {
		completeEvent()
	}
}

def spawnRandom() {
	//println "#### spawnLocations = ${spawnLocations} ####"
	randomizeML()
	clutchCounter = clutchCounter + 3
	myManager.schedule(20) { spawnCheck() }
	leaderSpawner = random(spawnLocations) //select a spawn location from the list of locations
	patrollerSpawner = spawnMap.get(leaderSpawner)

	if(null==patrollerSpawner) {
		throw new RuntimeException("clutchSpy event could not get spawner for ${leaderSpawner.toString()}");
	}

	spawnLocations.remove(leaderSpawner) //remove it from the allowed spawn locations
	spyLocation = random(spyLocations)//determine spy location
	
	//println "#### leaderSpawner = ${leaderSpawner} ####"
	
	myManager.schedule(3) {
		//Spawn the leader of the patrol and add it to list of clutches
		//println "#### SpawningClutch ${leaderSpawner} ####"
		leader = leaderSpawner.forceSpawnNow()
		leader.setDisplayName("Clutch Infiltrator")
		clutchSpawnerMap.put(leader, [leaderSpawner, patrollerSpawner])
		clutchSpawnedList << leader
		
		//println "#### clutchSpawnerMap keys = ${clutchSpawnerMap.keySet()} ####"
		//println "#### clutchSpawnerMap values = ${clutchSpawnerMap.values()} ####"
		
		//pull room and x/y locations for spy room
		spyRoom = spyLocation.get(1)
		spyX = spyLocation.get(2)
		spyY = spyLocation.get(3)

		
		runOnDeath(leader) { event ->
			if (!spawnLocations.contains(clutchSpawnerMap.get(event.actor).get(0))) {
				spawnLocations << clutchSpawnerMap.get(event.actor).get(0)
			}
			clutchSpawnedList.remove(event.actor)
			clutchSpawnerMap.remove(event.actor) //remove the dead clutch from the list of clutches
			event.actor.getHated().each() {
				if(!clutchKillerList.contains(it)) { clutchKillerList << it; it.setPlayerVar("Z05_ClutchSpiesCaught", 0) }
				if(it.getPlayerVar("Z05_ClutchSpiesCaught") == null && it.getPlayerVar("Z05_ClutchSpiesCaughtTotal") == null) { //if player doesn't have kill trackers, set them
					it.setPlayerVar("Z05_ClutchSpiesCaught", 1)
					it.setPlayerVar("Z05_ClutchSpiesCaughtTotal", 1)
				} else { //if player has kill trackers, increment them
					it.addToPlayerVar("Z05_ClutchSpiesCaught", 1)
					it.addToPlayerVar("Z05_ClutchSpiesCaughtTotal", 1)
				}
			}
		}

		//println "#### SpawningClutch ${patrollerSpawner} ####"
		patroller1 = patrollerSpawner.forceSpawnNow()
		patroller1.setDisplayName("Clutch Agent")
		patroller1.startFollow(leader)
		clutchSpawnedList << patroller1
		
		runOnDeath(patroller1) { event ->
			clutchSpawnedList.remove(event.actor)
			event.actor.getHated().each() {
				if(!clutchKillerList.contains(it)) { clutchKillerList << it; it.setPlayerVar("Z05_ClutchSpiesCaught", 0) }
				if(it.getPlayerVar("Z05_ClutchSpiesCaught") == null && it.getPlayerVar("Z05_ClutchSpiesCaughtTotal") == null) {
					it.setPlayerVar("Z05_ClutchSpiesCaught", 1)
					it.setPlayerVar("Z05_ClutchSpiesCaughtTotal", 1)
				} else {
					it.addToPlayerVar("Z05_ClutchSpiesCaught", 1)
					it.addToPlayerVar("Z05_ClutchSpiesCaughtTotal", 1)
				}
			}
		}
		
		//println "#### SpawningClutch ${patrollerSpawner} ####"
		patroller2 = patrollerSpawner.forceSpawnNow()
		patroller2.setDisplayName("Clutch Agent")
		patroller2.startFollow(leader)
		clutchSpawnedList << patroller2
		
		runOnDeath(patroller2) { event ->
			clutchSpawnedList.remove(event.actor)
			event.actor.getHated().each() {
				if(!clutchKillerList.contains(it)) { clutchKillerList << it; it.setPlayerVar("Z05_ClutchSpiesCaught", 0) }
				if(it.getPlayerVar("Z05_ClutchSpiesCaught") == null && it.getPlayerVar("Z05_ClutchSpiesCaughtTotal") == null) {
					if(!clutchKillerList.contains(it)) { clutchKillerList << it; it.setPlayerVar("Z05_ClutchSpiesCaught", 0) }
					it.setPlayerVar("Z05_ClutchSpiesCaught", 1)
					it.setPlayerVar("Z05_ClutchSpiesCaughtTotal", 1)
				} else {
					if(!clutchKillerList.contains(it)) { clutchKillerList << it; it.setPlayerVar("Z05_ClutchSpiesCaught", 0) }
					it.addToPlayerVar("Z05_ClutchSpiesCaught", 1)
					it.addToPlayerVar("Z05_ClutchSpiesCaughtTotal", 1)
				}
			}
		}
		clutchMap.put(leader, [patroller1, patroller2])
		clutchPatrolMap.put(leader, spyLocations.clone())
		clutchPatrolMap.get(leader).remove(spyLocation)
		
		leaderSpawner.setHomeForEveryone(spyRoom.toString(), spyX, spyY)
	}
}

def randomizeML() {
	spawnerList.clone().each() {
		randomVariance = random(0,5)/100
		//println "#### randomVariance = ${randomVariance} ####"
		plusOrMinus = random(0,1)
		if(plusOrMinus == 0) {
			monsterLevel = 4.0 + randomVariance
			//println "#### monsterLevel = ${monsterLevel} ####"
		} else {
			monsterLevel = 4.0 - randomVariance
			//println "#### monsterLevel = ${monsterLevel} ####"
		}
		it.setMonsterLevelForChildren(monsterLevel)
	}
}

startEvent()
