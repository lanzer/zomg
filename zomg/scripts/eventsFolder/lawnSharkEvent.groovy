import com.gaiaonline.mmo.battle.script.*;

//======================
//LAWNSHARK SPAWNER     
//======================

//Make LawnShark invulnerable to players over CL 2.0
def lawnSharkEventSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= 2.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 2.0 or lower to attack LawnShark." ); }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

lawnSharkOneSpawner = createStoppedEventSpawner( "VILLAGE_1002", "lawnSharkOneSpawner", "lawnshark", 1 )
lawnSharkOneSpawner.setMiniEventSpec( lawnSharkEventSpec )
lawnSharkOneSpawner.setHateRadiusForChildren( 20000 )
lawnSharkOneSpawner.setPos( 530, 580 )
lawnSharkOneSpawner.setForbiddenRoom( "VILLAGE_206" )
lawnSharkOneSpawner.setMonsterLevelForChildren( 1.5 )

lawnSharkTwoSpawner = createStoppedEventSpawner( "VILLAGE_1002", "lawnSharkTwoSpawner", "lawnshark", 1 )
lawnSharkTwoSpawner.setMiniEventSpec( lawnSharkEventSpec )
lawnSharkTwoSpawner.setHateRadiusForChildren( 20000 )
lawnSharkTwoSpawner.setPos( 530, 580 )
lawnSharkTwoSpawner.setForbiddenRoom( "VILLAGE_206" )
lawnSharkTwoSpawner.setMonsterLevelForChildren( 1.5 )

lawnSharkThreeSpawner = createStoppedEventSpawner( "VILLAGE_1002", "lawnSharkThreeSpawner", "lawnshark", 1 )
lawnSharkThreeSpawner.setMiniEventSpec( lawnSharkEventSpec )
lawnSharkThreeSpawner.setHateRadiusForChildren( 20000 )
lawnSharkThreeSpawner.setPos( 530, 580 )
lawnSharkThreeSpawner.setForbiddenRoom( "VILLAGE_206" )
lawnSharkThreeSpawner.setMonsterLevelForChildren( 1.5 )

//======================
//STARTUP & SELECT ROOM 
//======================
totalHateCollected = 0
eventStarted = false
lastCountDownExpired = false
lawnSharksDead = false
masterHateCollector = [] as Set
playerRoomList = []
playersThatDamagedLawnShark = [] as Set

myManager.schedule(3) { startUp() }

//Start the scenario
def startUp() {
	eventStarted = true
	sound( "lawnSharkEventStart" ).toZone()
	
	//spawn all three LandSharks
	lawnSharkOne = lawnSharkOneSpawner.forceSpawnNow()
	lawnSharkOne.setDisplayName( "Rollerblades" )
	lawnSharkOne.setMiniMapMarker("markerLawnShark", "LawnShark") //TODO: Set correct marker
	lawnSharkTwo = lawnSharkTwoSpawner.forceSpawnNow()
	lawnSharkTwo.setDisplayName( "Grass Master" )
	lawnSharkTwo.setMiniMapMarker("markerLawnShark", "LawnShark") //TODO: Set correct marker
	lawnSharkThree = lawnSharkThreeSpawner.forceSpawnNow()
	lawnSharkThree.setDisplayName( "Mower-nado" )
	lawnSharkThree.setMiniMapMarker("markerLawnShark", "LawnShark") //TODO: Set correct marker
	
	//set up the damage checkers to ensure that players get their "safety net" lists going (for rewards)
	myManager.onAreaCombat( myRooms.VILLAGE_3.getArea() ) { event ->
	        if( event.target == lawnSharkOne && event.effect.stat == com.gaiaonline.mmo.battle.ring.StatusEffect.Stat.DMG ) {
			//reset the "currentDamage" playerVar to zero if this is the first time the player has caused damage to any of the LawnSharks in this event
			if( !playersThatDamagedLawnShark.contains( event.cause ) ) {
				playersThatDamagedLawnShark << event.cause
				event.cause.getPlayerVar( "Z02LawnSharkCurrentDamage" ) == 0
			}
			event.cause.addPlayerVar( "Z02LawnSharkCurrentDamage", event.effect.amount )
	        }
	}

	myManager.onAreaCombat( myRooms.VILLAGE_3.getArea() ) { event ->
	        if( event.target == lawnSharkTwo && event.effect.stat == com.gaiaonline.mmo.battle.ring.StatusEffect.Stat.DMG ) {
			//reset the "currentDamage" playerVar to zero if this is the first time the player has caused damage to any of the LawnSharks in this event
			if( !playersThatDamagedLawnShark.contains( event.cause ) ) {
				playersThatDamagedLawnShark << event.cause
				event.cause.getPlayerVar( "Z02LawnSharkCurrentDamage" ) == 0
			}
			event.cause.addPlayerVar( "Z02LawnSharkCurrentDamage", event.effect.amount )
	        }
	}

	myManager.onAreaCombat( myRooms.VILLAGE_3.getArea() ) { event ->
	        if( event.target == lawnSharkThree && event.effect.stat == com.gaiaonline.mmo.battle.ring.StatusEffect.Stat.DMG ) {
			//reset the "currentDamage" playerVar to zero if this is the first time the player has caused damage to any of the LawnSharks in this event
			if( !playersThatDamagedLawnShark.contains( event.cause ) ) {
				playersThatDamagedLawnShark << event.cause
				event.cause.getPlayerVar( "Z02LawnSharkCurrentDamage" ) == 0
			}
			event.cause.addPlayerVar( "Z02LawnSharkCurrentDamage", event.effect.amount )
	        }
	}
	
	//now the runOnDeath statements for the LawnSharks
	runOnDeath( lawnSharkOne, { event ->
		lawnSharkOne.clearMiniMapMarker()
		//each time the event starts up, initialize the playerVar that holds Hate for that event
		event.actor.getHated().each{
			if( !masterHateCollector.contains( it ) ) {
				it.setPlayerVar( "Z02LawnSharkCurrentHate", 0 )
			}
		}
		//now make the master lists and currentEvent Hate totals
		event.actor.getHatedMap().each{ player, hateAmount ->
			if( hateAmount > 750 ) { hateAmount = 750 }

			totalHateCollected = totalHateCollected + hateAmount //For data mining only. Not used in logic of the script

			//add the hate accumulated into the current and all-time playerVars
			player.addPlayerVar( "Z02LawnSharkCurrentHate", hateAmount )

			//add the player to the master hate collector and then ensure there is only one instance of each player in that list
			masterHateCollector << player
		}
		
		checkForEventEnding()
	} )

	runOnDeath( lawnSharkTwo, { event ->
		lawnSharkTwo.clearMiniMapMarker()
		//each time the event starts up, initialize the playerVar that holds Hate for that event
		event.actor.getHated().each{
			if( !masterHateCollector.contains( it ) ) {
				it.setPlayerVar( "Z02LawnSharkCurrentHate", 0 )
			}
		}
		//now make the master lists and currentEvent Hate totals
		event.actor.getHatedMap().each{ player, hateAmount ->
			if( hateAmount > 750 ) { hateAmount = 750 }

			totalHateCollected = totalHateCollected + hateAmount //For data mining only. Not used in logic of the script

			//add the hate accumulated into the current and all-time playerVars
			player.addPlayerVar( "Z02LawnSharkCurrentHate", hateAmount )

			//add the player to the master hate collector and then ensure there is only one instance of each player in that list
			masterHateCollector << player
		}
		
		checkForEventEnding()
	} )

	runOnDeath( lawnSharkThree, { event ->
		lawnSharkThree.clearMiniMapMarker()
		//each time the event starts up, initialize the playerVar that holds Hate for that event
		event.actor.getHated().each{
			if( !masterHateCollector.contains( it ) ) {
				it.setPlayerVar( "Z02LawnSharkCurrentHate", 0 )
			}
		}
		//now make the master lists and currentEvent Hate totals
		event.actor.getHatedMap().each{ player, hateAmount ->
			if( hateAmount > 750 ) { hateAmount = 750 }

			totalHateCollected = totalHateCollected + hateAmount //For data mining only. Not used in logic of the script

			//add the hate accumulated into the current and all-time playerVars
			player.addPlayerVar( "Z02LawnSharkCurrentHate", hateAmount )

			//add the player to the master hate collector and then ensure there is only one instance of each player in that list
			masterHateCollector << player
		}
		
		checkForEventEnding()
	} )

	//send the LawnSharks on their way to other rooms
	pickNewHomes()
	
	myManager.schedule(2) {
		//announce the event to the zone
		zoneBroadcast( "Leon-VQS", "Mowing Frenzy!", "Citizens! The gnome pests have stolen and corrupted MacTaggart's mowers! Three of these 'lawnsharks' are rampagin' the Greens now! We must stop them!" )

		//start the counter to make the event end, even if the LawnSharks aren't destroyed
		lastFiveMinutesCountdown = myManager.schedule( 900 ) {
			makeZoneTimer( "Lawnshark Timer", "0:5:0", "0:0:0" ).onCompletion( { event ->
				if( lawnSharksDead == false ) {
					lastCountDownExpired = true
					if( !lawnSharkOne.isDead() ) { lawnSharkOne.dispose(); lawnSharkOne.clearMiniMapMarker() }
					if( !lawnSharkTwo.isDead() ) { lawnSharkTwo.dispose(); lawnSharkTwo.clearMiniMapMarker() }
					if( !lawnSharkThree.isDead() ) { lawnSharkThree.dispose(); lawnSharkThree.clearMiniMapMarker() }
					removeZoneTimer( "Lawnshark Timer" )
					removeZoneBroadcast( "Mowing Frenzy!" )
					rewardPlayers()
					myManager.schedule(2) { zoneBroadcast( "Leon-VQS", "Curses!", "We failed to destroy them all! At least they've left for now, but they'll be back!" ) }
					myManager.schedule(15) { removeZoneBroadcast( "Curses!" ); eventDone() }
				}
			} ).start()
		}
	}
}	

def checkForEventEnding() {
	if( lawnSharkOne.isDead() && lawnSharkTwo.isDead() && lawnSharkThree.isDead() && lastCountDownExpired == false ) {
		lawnSharksDead = true
		//remove the zoneBroadcast
		removeZoneBroadcast( "Mowing Frenzy!" )
		
		//cancel the lastFiveMinutesCountdown, and remove the zoneTimer if it's already started
		lastFiveMinutesCountdown.cancel()
		removeZoneTimer( "Lawnshark Timer" )

		myManager.schedule(2) {
			//Have Logan sound the all clear once LawnShark is dead
			zoneBroadcast( "Leon-VQS", "LawnSharks Defeated!", "A mighty blow for Barton, well done! You've sent Gnome's monster machines running!" )
			myManager.schedule( 15 ) {
				removeZoneBroadcast( "LawnSharks Defeated!" )
				eventDone()
			}
			if( !masterHateCollector.isEmpty() ) {
				//print out some data mining so we can look in logs during tests or live events
				println "************************************************"
				println "********** EVENT HATE SUMMARY ***************"
				println "**** The Player Hate Map for this event is: ****"
				println "**********       (VILLAGE)     ***************"
				println ""
				println "masterHateCollector = ${masterHateCollector}"
				println ""
				println "totalHateCollected = ${ totalHateCollected }"
				println ""
				println "numPeople in Event = ${masterHateCollector.size()} ****"
				println ""
				println "average Hate per person = ${ totalHateCollected / masterHateCollector.size() }"
				println ""
				println "********** EVENT HATE SUMMARY ***************"
				println "************************************************"

				rewardPlayers()
			}
		}

	}
}
	

//======================
//PICK NEW HOME         
//======================

//Reset the monster's home point every so often so that if nothing attacks it, it can keep moving to locations that have players in it

roomMap = [ 1: [ "VILLAGE_3", 600, 500], 2: [ "VILLAGE_4", 700, 500], 3: [ "VILLAGE_5", 420, 520], 4: [ "VILLAGE_103", 500, 350], 5: [ "VILLAGE_104", 500, 250], 6: [ "VILLAGE_201", 500, 350], 7: [ "VILLAGE_202", 500, 480], 8: [ "VILLAGE_203", 500, 350], 9: [ "VILLAGE_204", 650, 300], 10: [ "VILLAGE_205", 310, 300], 11: [ "VILLAGE_301", 500, 350], 12: [ "VILLAGE_302", 380, 350], 13: [ "VILLAGE_303", 500, 350], 14: [ "VILLAGE_304", 500, 350], 15: [ "VILLAGE_305", 500, 350], 16: [ "VILLAGE_306", 300, 350], 17: [ "VILLAGE_401", 500, 350], 18: [ "VILLAGE_402", 350, 350], 19: [ "VILLAGE_403", 500, 350], 20: [ "VILLAGE_404", 500, 350], 21: [ "VILLAGE_501", 500, 350], 22: [ "VILLAGE_502", 500, 350], 23: [ "VILLAGE_503", 500, 350], 24: [ "VILLAGE_504", 300, 350], 25: [ "VILLAGE_601", 840, 350], 26: [ "VILLAGE_602", 500, 150], 27: [ "VILLAGE_603", 500, 350], 28: [ "VILLAGE_604", 500, 350], 29: [ "VILLAGE_701", 400, 350], 30: [ "VILLAGE_702", 500, 480], 31: [ "VILLAGE_703", 500, 350], 32: [ "VILLAGE_801", 500, 350], 33: [ "VILLAGE_802", 500, 350], 34: [ "VILLAGE_803", 500, 350], 35: [ "VILLAGE_804", 500, 350], 36: [ "VILLAGE_902", 500, 350], 37: [ "VILLAGE_903", 500, 430], 38: [ "VILLAGE_904", 500, 350], 39: [ "VILLAGE_1001", 500, 440], 40: [ "VILLAGE_1002", 500, 350], 41: [ "VILLAGE_1003", 800, 300], 42: [ "VILLAGE_1004", 500, 350] ]

roomList = [ "VILLAGE_3", "VILLAGE_4", "VILLAGE_5", "VILLAGE_103", "VILLAGE_104", "VILLAGE_201", "VILLAGE_202", "VILLAGE_203", "VILLAGE_204", "VILLAGE_205", "VILLAGE_301", "VILLAGE_302", "VILLAGE_303", "VILLAGE_304", "VILLAGE_305", "VILLAGE_306", "VILLAGE_401", "VILLAGE_402", "VILLAGE_403", "VILLAGE_404", "VILLAGE_501", "VILLAGE_502", "VILLAGE_503", "VILLAGE_504", "VILLAGE_601", "VILLAGE_602", "VILLAGE_603", "VILLAGE_604", "VILLAGE_701", "VILLAGE_702", "VILLAGE_703", "VILLAGE_801", "VILLAGE_802", "VILLAGE_803", "VILLAGE_804", "VILLAGE_902", "VILLAGE_903", "VILLAGE_904", "VILLAGE_1001", "VILLAGE_1002", "VILLAGE_1003", "VILLAGE_1004" ]

def findPlayers() {
	playerRoomList.clear()
	myRooms.VILLAGE_3.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 1 } }
	myRooms.VILLAGE_4.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 2 } }
	myRooms.VILLAGE_5.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 3 } }
	myRooms.VILLAGE_103.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 4 } }
	myRooms.VILLAGE_104.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 5 } }
	myRooms.VILLAGE_201.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 6 } }
	myRooms.VILLAGE_202.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 7 } }
	myRooms.VILLAGE_203.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 8 } }
	myRooms.VILLAGE_204.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 9 } }
	myRooms.VILLAGE_205.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 10 } }
	myRooms.VILLAGE_301.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 11 } }
	myRooms.VILLAGE_302.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 12 } }
	myRooms.VILLAGE_303.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 13 } }
	myRooms.VILLAGE_304.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 14 } }
	myRooms.VILLAGE_305.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 15 } }
	myRooms.VILLAGE_306.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 16 } }
	myRooms.VILLAGE_401.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 17 } }
	myRooms.VILLAGE_402.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 18 } }
	myRooms.VILLAGE_403.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 19 } }
	myRooms.VILLAGE_404.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 20 } }
	myRooms.VILLAGE_501.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 21 } }
	myRooms.VILLAGE_502.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 22 } }
	myRooms.VILLAGE_503.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 23 } }
	myRooms.VILLAGE_504.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 24 } }
	myRooms.VILLAGE_601.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 25 } }
	myRooms.VILLAGE_602.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 26 } }
	myRooms.VILLAGE_603.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 27 } }
	myRooms.VILLAGE_604.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 28 } }
	myRooms.VILLAGE_701.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 29 } }
	myRooms.VILLAGE_702.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 30 } }
	myRooms.VILLAGE_703.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 31 } }
	myRooms.VILLAGE_801.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 32 } }
	myRooms.VILLAGE_802.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 33 } }
	myRooms.VILLAGE_803.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 34 } }
	myRooms.VILLAGE_804.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 35 } }
	myRooms.VILLAGE_902.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 36 } }
	myRooms.VILLAGE_903.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 37 } }
	myRooms.VILLAGE_904.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 38 } }
	myRooms.VILLAGE_1001.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 39 } }
	myRooms.VILLAGE_1002.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 40 } }
	myRooms.VILLAGE_1003.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 41 } }
	myRooms.VILLAGE_1004.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { playerRoomList << 42 } }
	playerRoomList.unique()
	//println "**** playerRoomList = ${playerRoomList} ****"
}	

//Every LawnShark picks a different room as "Home"
def pickNewHomes() {
	//reset the randomRoomList
	randomRoomList = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 ]
	//create the playerRoomList
	//findPlayers()
	//println "**** Initial playerRoomList = ${playerRoomList} ****"
	
	if( eventStarted == true && !lawnSharkOne.isDead() ) {
		if( !playerRoomList.isEmpty() ) {
			chosenRoom = random( playerRoomList )
			coords = roomMap.get( chosenRoom )
			//remove the room from both the playerRoomList and the randomRoomList to prevent it from being selected by another LawnShark
			playerRoomList.remove( playerRoomList.indexOf( chosenRoom ) )
			randomRoomList.remove( randomRoomList.indexOf( chosenRoom ) )
		} else {
			chosenRoom = random( randomRoomList )
			coords = roomMap.get( chosenRoom )
			randomRoomList.remove( randomRoomList.indexOf( chosenRoom ) )
		}
		lawnSharkOneRoom = coords.get(0)
		lawnSharkOneX = coords.get(1).intValue()
		lawnSharkOneY = coords.get(2).intValue()
		lawnSharkOneSpawner.setHomeForEveryone( lawnSharkOneRoom, lawnSharkOneX, lawnSharkOneY )
	}
	if( eventStarted == true && !lawnSharkTwo.isDead() ) {
		if( !playerRoomList.isEmpty() ) {
			chosenRoom = random( playerRoomList )
			coords = roomMap.get( chosenRoom )
			//remove the room from both the playerRoomList and the randomRoomList to prevent it from being selected by another LawnShark
			playerRoomList.remove( playerRoomList.indexOf( chosenRoom ) )
			randomRoomList.remove( randomRoomList.indexOf( chosenRoom ) )
		} else {
			chosenRoom = random( randomRoomList )
			coords = roomMap.get( chosenRoom )
			randomRoomList.remove( randomRoomList.indexOf( chosenRoom ) )
		}
		lawnSharkTwoRoom = coords.get(0)
		lawnSharkTwoX = coords.get(1).intValue()
		lawnSharkTwoY = coords.get(2).intValue()
		lawnSharkTwoSpawner.setHomeForEveryone( lawnSharkTwoRoom, lawnSharkTwoX, lawnSharkTwoY )
	}
	if( eventStarted == true && !lawnSharkThree.isDead() ) {
		if( !playerRoomList.isEmpty() ) {
			chosenRoom = random( playerRoomList )
			coords = roomMap.get( chosenRoom )
			//remove the room from both the playerRoomList and the randomRoomList to prevent it from being selected by another LawnShark
			playerRoomList.remove( playerRoomList.indexOf( chosenRoom ) )
			randomRoomList.remove( randomRoomList.indexOf( chosenRoom ) )
		} else {
			chosenRoom = random( randomRoomList )
			coords = roomMap.get( chosenRoom )
			randomRoomList.remove( randomRoomList.indexOf( chosenRoom ) )
		}
		lawnSharkThreeRoom = coords.get(0)
		lawnSharkThreeX = coords.get(1).intValue()
		lawnSharkThreeY = coords.get(2).intValue()
		lawnSharkThreeSpawner.setHomeForEveryone( lawnSharkThreeRoom, lawnSharkThreeX, lawnSharkThreeY )
	}
	
	//Start the countdown clock in that room to show how long LawnShark will stay in that room
	myManager.schedule( random( 90, 120 ) ) { pickNewHomes() }
}


//======================
//REWARD ROUTINE        
//======================

//LawnShark has about 2100 health
averageHatePerPlayer = 600
maxGoldGrant = 100
maxOrbGrant = 10
desiredChanceComponentReceived = 50

def rewardPlayers() {
	if( lastCountDownExpired == true ) { expiredMult = 0.5 } else { expiredMult = 1.0 }
	masterHateCollector.clone().each() {
		//make sure it's a player (pets are coming!) and make sure that player is still online
		if( isOnline( it ) && isPlayer( it ) && !isInOtherLayer( it ) ) {
			
			//safety net: The player should never have less Hate than the amount of damage that it has caused to LawnShark
			//If this is the case, then set the Hate playerVar to match the damage one.
			if( it.getPlayerVar( "Z02LawnSharkCurrentDamage" ) > it.getPlayerVar( "Z02LawnSharkCurrentHate" ) ) {
				newHate = it.getPlayerVar( "Z02LawnSharkCurrentDamage" )
				it.setPlayerVar( "Z02LawnSharkCurrentHate", newHate )
			}
						
			//All players recieve gold relating to the amount of Hate they generate during the event
			goldGrant = (it.getPlayerVar( "Z02LawnSharkCurrentHate" ) * (maxGoldGrant / averageHatePerPlayer) * expiredMult ).intValue()
			//cap the grant at 100 gold
			if( goldGrant > maxGoldGrant ) { goldGrant = maxGoldGrant }
			if( goldGrant < 1 ) { goldGrant = 1 }
			it.grantCoins( goldGrant )
			it.centerPrint( "You earned ${goldGrant} gold!" )
			
			//grant orbs for killing LawnShark
			orbGrant = (it.getPlayerVar( "Z02LawnSharkCurrentHate" ) * (maxOrbGrant / averageHatePerPlayer) * expiredMult ).intValue()
			if( orbGrant > maxOrbGrant ) { orbGrant = maxOrbGrant }
			if( orbGrant < 1 ) { orbGrant = 1 }
			it.grantQuantityItem( 100257, orbGrant )
			it.centerPrint( "You also earned ${orbGrant} Charge Orbs!")
			
			//Increment a playerVar (by one) that records the number of times the player has participated in this particular event
			it.addPlayerVar( "Z02LawnSharkTotalEventVisits", 1 )
			
			//Increment the All-Time Hate Collector with the amount of hate accumulated during this event
			thisEventHate = it.getPlayerVar( "Z02LawnSharkCurrentHate" )
			if( thisEventHate > 2000 ) { thisEventHate = 2000 } //cap at 2000 for a single event.
			it.addPlayerVar( "Z02LawnSharkAllTimeEventHate", thisEventHate )
			it.centerPrint( "And you earned ${thisEventHate.intValue()} points toward LawnShark Badges!" )
			
			//Give a badge if the player does about 1/10th the necessary Hate to kill LawnShark
			if( it.getPlayerVar( "Z02LawnSharkCurrentHate" ) > averageHatePerPlayer * 1.2 ) {
				it.updateQuest( 328, "MacTaggert-VQS" ) //complete the GREENSKEEPER OF THE PEACE badge
			}
			
			//Award badges for fighting LawnShark a LOT
			if( it.getPlayerVar( "Z02LawnSharkAllTimeEventHate" ) > averageHatePerPlayer * 10 ) { //10 players might earn about 700 hate each, so about 15 or so times fighting the LawnShark
				it.updateQuest( 329, "MacTaggert-VQS" ) //complete the KICKIN' GRASS AND TAKIN' NAMES badge
			}
			/*
			//Player receive a big monster-specific loot item *if* the roll under a certain percentage...which is based on the amount of hate accumulated in the fights.
			roll = random( 100 ) 
			eventHate = it.getPlayerVar( "Z02LawnSharkCurrentHate" )
			//this multiplier is just a scaling value to adjust hate points. Figure out the average hate accumulated during the event and adjust accordingly.
			multiplier = desiredChanceComponentReceived / averageHatePerPlayer

			if( roll < ( eventHate * multiplier * expiredMult ).intValue() ) { 
				//award the event item
				it.grantItem( "100439" )
				it.centerPrint( "You find one of LawnShark's blades!" )
			} else {
				it.centerPrint( "You find no specialty item this time. Try again when LawnShark returns!" )
			}
			*/
		}
	}
}

