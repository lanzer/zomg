import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
//ORB SPILL (VILLAGE GREENS)                
//------------------------------------------

//Make the orbs collectible only at the appropriate CL for the zone
def orbSpillSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= 2.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 2.0 or below to collect the orbs." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

orb = createStoppedEventSpawner( "VILLAGE_5", "orb", "orb_village", 100 )
orb.setPos( 850, 340 )
orb.setMonsterLevelForChildren( 1.6 )
orb.setHomeTetherForChildren( 6000 )
orb.setMiniEventSpec( orbSpillSpec )
orb.setEdgeHinting( false )
orb.setTargetCycle( false )

//------------------------------------------
//EVENT TIMER                               
//------------------------------------------

stopSpawning = false

def startEventTimer() {
	zoneBroadcast( "Leon-VQS", "Orbs Everywhere!", "The Ghi force is eddying all over the place. Charge Orbs are ripe for the picking! Get 'em while they're hot!" )
	makeZoneTimer("Orb Spill Timer", "0:15:0", "0:0:0").onCompletion( { event ->
		removeZoneTimer( "Orb Spill Timer" )
		removeZoneBroadcast( "Orbs Everywhere!" )
		stopSpawning = true
		myManager.schedule(2) {
			zoneBroadcast( "Leon-VQS", "The Party's Over", "Well, whatever was causing the Ghi overflow has stopped. I hope everyone got some Orbs while they lasted!" )
			orbDisposal()
			myManager.schedule(15) { removeZoneBroadcast( "The Party's Over" ); eventDone() }
		}
	} ).start()
}

//------------------------------------------
//SPAWNING LOGIC                            
//------------------------------------------

orbEventPlayerList = []
numOrbPositionsPerRoom = 4
zonePlayerList = []

//Get the names of all the players in the zone
def makePlayerList(){
	//Empty the check list and then look into all rooms to find all players, whether in a Crew or not
	zonePlayerList.clear()
	myRooms.VILLAGE_3.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_4.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_103.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_104.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_201.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_202.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_203.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_204.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_205.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_206.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_301.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_302.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_303.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_304.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_305.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_306.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_401.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_402.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_403.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_404.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_405.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_406.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_501.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_502.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_503.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_504.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_601.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_602.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_603.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_604.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_701.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_702.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_703.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_801.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_802.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_803.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_804.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_901.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_902.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_903.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_904.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_1001.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_1002.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_1003.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.VILLAGE_1004.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
}

//Dispose of the orbs after the countdown is complete
def orbDisposal(){
	myRooms.VILLAGE_3.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_4.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_103.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_104.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_201.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_202.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_203.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_204.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_205.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_206.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_301.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_302.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_303.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_304.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_305.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_306.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_401.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_402.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_403.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_404.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_405.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_406.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_501.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_502.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_503.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_504.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_601.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_602.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_603.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_604.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_701.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_702.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_703.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_801.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_802.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_803.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_804.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_901.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_902.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_903.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_904.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_1001.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_1002.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_1003.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
	myRooms.VILLAGE_1004.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_village" ) { it.dispose() } }
}

village3Map = [ 1:[200, 500, 0], 2:[320, 325, 0], 3:[920, 355, 0], 4:[510, 270, 0], 5:"VILLAGE_3" ]
village4Map = [ 1:[50, 340, 0], 2:[350, 550, 0], 3:[600, 360, 0], 4:[970, 330, 0], 5:"VILLAGE_4" ]
village103Map = [ 1:[60, 370, 0], 2:[530, 160, 0], 3:[855, 355, 0], 4:[315, 200, 0], 5:"VILLAGE_103" ]
village104Map = [ 1:[280, 400, 0], 2:[520, 400, 0], 3:[420, 570, 0], 4:[810, 280, 0], 5:"VILLAGE_104" ]
village201Map = [ 1:[115, 290, 0], 2:[405, 420, 0], 3:[790, 410, 0], 4:[1000, 115, 0], 5:"VILLAGE_201" ]
village202Map = [ 1:[40, 560, 0], 2:[170, 100, 0], 3:[680, 435, 0], 4:[940, 90, 0], 5:"VILLAGE_202" ]
village203Map = [ 1:[60, 25, 0], 2:[200, 150, 0], 3:[555, 500, 0], 4:[805, 370, 0], 5:"VILLAGE_203" ]
village204Map = [ 1:[165, 195, 0], 2:[275, 280, 0], 3:[500, 395, 0], 4:[830, 60, 0], 5:"VILLAGE_204" ]
village205Map = [ 1:[150, 420, 0], 2:[530, 480, 0], 3:[660, 290, 0], 4:[790, 150, 0], 5:"VILLAGE_205" ]
village206Map = [ 1:[265, 265, 0], 2:[450, 260, 0], 3:[900, 80, 0], 4:[860, 470, 0], 5:"VILLAGE_206" ]
village301Map = [ 1:[160, 630, 0], 2:[420, 250, 0], 3:[760, 395, 0], 4:[1005, 80, 0], 5:"VILLAGE_301" ]
village302Map = [ 1:[175, 100, 0], 2:[475, 465, 0], 3:[595, 320, 0], 4:[830, 95, 0], 5:"VILLAGE_302" ]
village303Map = [ 1:[110, 215, 0], 2:[460, 460, 0], 3:[620, 530, 0], 4:[280, 150, 0], 5:"VILLAGE_303" ]
village304Map = [ 1:[295, 600, 0], 2:[440, 600, 0], 3:[640, 190, 0], 4:[760, 190, 0], 5:"VILLAGE_304" ]
village305Map = [ 1:[290, 470, 0], 2:[385, 340, 0], 3:[855, 330, 0], 4:[510, 130, 0], 5:"VILLAGE_305" ]
village306Map = [ 1:[170, 560, 0], 2:[385, 430, 0], 3:[790, 610, 0], 4:[640, 110, 0], 5:"VILLAGE_306" ]
village401Map = [ 1:[215, 140, 0], 2:[470, 420, 0], 3:[750, 330, 0], 4:[970, 290, 0], 5:"VILLAGE_401" ]
village402Map = [ 1:[220, 250, 0], 2:[400, 180, 0], 3:[520, 405, 0], 4:[770, 440, 0], 5:"VILLAGE_402" ]
village403Map = [ 1:[210, 260, 0], 2:[310, 330, 0], 3:[680, 220, 0], 4:[930, 575, 0], 5:"VILLAGE_403" ]
village404Map = [ 1:[70, 655, 0], 2:[580, 440, 0], 3:[710, 400, 0], 4:[1000, 400, 0], 5:"VILLAGE_404" ]
village405Map = [ 1:[70, 400, 0], 2:[980, 330, 0], 3:[270, 260, 0], 4:[420, 400, 0], 5:"VILLAGE_405" ]
village406Map = [ 1:[290, 375, 0], 2:[450, 135, 0], 3:[735, 110, 0], 4:[1005, 165, 0], 5:"VILLAGE_406" ]
village501Map = [ 1:[45, 60, 0], 2:[720, 240, 0], 3:[170, 440, 0], 4:[620, 445, 0], 5:"VILLAGE_501" ]
village502Map = [ 1:[230, 480, 0], 2:[340, 360, 0], 3:[1000, 315, 0], 4:[710, 530, 0], 5:"VILLAGE_502" ]
village503Map = [ 1:[65, 290, 0], 2:[605, 100, 0], 3:[805, 185, 0], 4:[990, 50, 0], 5:"VILLAGE_503" ]
village504Map = [ 1:[420, 370, 0], 2:[560, 240, 0], 3:[740, 390, 0], 4:[940, 620, 0], 5:"VILLAGE_504" ]
village601Map = [ 1:[45, 125, 0], 2:[300, 550, 0], 3:[520, 460, 0], 4:[710, 200, 0], 5:"VILLAGE_601" ]
village602Map = [ 1:[180, 340, 0], 2:[380, 390, 0], 3:[800, 290, 0], 4:[660, 650, 0], 5:"VILLAGE_602" ]
village603Map = [ 1:[110, 200, 0], 2:[300, 430, 0], 3:[570, 130, 0], 4:[850, 270, 0], 5:"VILLAGE_603" ]
village604Map = [ 1:[20, 560, 0], 2:[380, 380, 0], 3:[500, 260, 0], 4:[880, 220, 0], 5:"VILLAGE_604" ]
village701Map = [ 1:[40, 620, 0], 2:[270, 320, 0], 3:[660, 310, 0], 4:[960, 410, 0], 5:"VILLAGE_701" ]
village702Map = [ 1:[80, 380, 0], 2:[280, 160, 0], 3:[600, 650, 0], 4:[840, 490, 0], 5:"VILLAGE_702" ]
village703Map = [ 1:[190, 560, 0], 2:[520, 380, 0], 3:[830, 100, 0], 4:[980, 500, 0], 5:"VILLAGE_703" ]
village801Map = [ 1:[40, 560, 0], 2:[320, 430, 0], 3:[600, 340, 0], 4:[960, 370, 0], 5:"VILLAGE_801" ]
village802Map = [ 1:[90, 420, 0], 2:[490, 510, 0], 3:[700, 160, 0], 4:[1000, 180, 0], 5:"VILLAGE_802" ]
village803Map = [ 1:[30, 180, 0], 2:[580, 510, 0], 3:[730, 380, 0], 4:[840, 540, 0], 5:"VILLAGE_803" ]
village804Map = [ 1:[190, 290, 0], 2:[380, 500, 0], 3:[630, 20, 0], 4:[810, 160, 0], 5:"VILLAGE_804" ]
village901Map = [ 1:[70, 150, 0], 2:[230, 360, 0], 3:[670, 560, 0], 4:[860, 20, 0], 5:"VILLAGE_901" ]
village902Map = [ 1:[40, 330, 0], 2:[390, 80, 0], 3:[650, 370, 0], 4:[840, 320, 0], 5:"VILLAGE_902" ]
village903Map = [ 1:[330, 130, 0], 2:[510, 320, 0], 3:[690, 320, 0], 4:[810, 660, 0], 5:"VILLAGE_903" ]
village904Map = [ 1:[200, 90, 0], 2:[360, 470, 0], 3:[520, 220, 0], 4:[740, 450, 0], 5:"VILLAGE_904" ]
village1001Map = [ 1:[110, 380, 0], 2:[420, 190, 0], 3:[670, 620, 0], 4:[900, 660, 0], 5:"VILLAGE_1001" ]
village1002Map = [ 1:[110, 620, 0], 2:[330, 420, 0], 3:[680, 480, 0], 4:[800, 620, 0], 5:"VILLAGE_1002" ]
village1003Map = [ 1:[240, 500, 0], 2:[390, 340, 0], 3:[600, 620, 0], 4:[1000, 420, 0], 5:"VILLAGE_1003" ]
village1004Map = [ 1:[60, 350, 0], 2:[350, 130, 0], 3:[450, 620, 0], 4:[750, 390, 0], 5:"VILLAGE_1004" ]

roomList = [ village3Map, village4Map, village103Map, village104Map, village201Map, village202Map, village203Map, village204Map, village205Map, village206Map, village301Map, village302Map, village303Map, village304Map, village305Map, village306Map, village401Map, village402Map, village403Map, village404Map, village405Map, village406Map, village501Map, village502Map, village503Map, village504Map, village601Map, village602Map, village603Map, village604Map, village701Map, village702Map, village703Map, village801Map, village802Map, village803Map, village804Map, village901Map, village902Map, village903Map, village904Map, village1001Map, village1002Map, village1003Map, village1004Map ]

def countOrbs() {
	orbCount = 0
	//count the number of orbs in the zone
	orbCount = orbCount + myRooms.VILLAGE_3.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_4.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_103.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_104.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_201.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_202.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_203.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_204.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_205.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_206.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_301.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_302.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_303.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_304.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_305.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_306.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_401.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_402.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_403.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_404.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_405.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_406.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_501.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_502.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_503.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_504.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_601.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_602.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_603.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_604.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_701.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_702.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_703.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_801.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_802.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_803.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_804.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_901.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_902.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_903.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_904.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_1001.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_1002.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_1003.getSpawnTypeCount( "orb_village" )
	orbCount = orbCount + myRooms.VILLAGE_1004.getSpawnTypeCount( "orb_village" )
}

//NOTE: The min limit for orbs in this zone is 10 instead of 20 because there are far fewer rooms in Barton Town than in other zones. Too many orbs with too few players is more like orb "harvesting" instead of orb "hunting".
def setMaxOrbs() {
	maxOrbs = areaPlayerCount()
	if( areaPlayerCount() < 20 ) {
		maxOrbs = 20
	}
}

//After the orbs have been spawned, keep checking until five or less of them exist...then spawn another wave of orbs
def checkForOrbRespawn() {
	if( stopSpawning == true ) return
	countOrbs()
	if( orbCount <= 5 ) {
		setMaxOrbs()
		spawnOrbs()
	} else {
		myManager.schedule(30) { checkForOrbRespawn() }
	}
}

//randomly spawn orbs
def spawnOrbs() {
	countOrbs()
	//if the current number of orbs in the zone is less than "maxOrbs", then spawn an orb
	if( orbCount < maxOrbs ) {
		//find the room in the zone to warp to
		room = random( roomList )
		//figure out the spawn position within that room
		position = random( numOrbPositionsPerRoom )
		positionInfo = room.get( position )
		//now look in position 2 of the "positionInfo" to see if the orb is spawned yet or not. (0 = not spawned; 1 = spawned)
		spawnHolder = positionInfo.get(2)
		if( spawnHolder == 0 ) {
			//change the "0" to a "1"
			positionInfo.remove( 2 ) ///remove the "placeholder" element in the current list
			positionInfo.add( 1 ) //put "1" at the end
			//now update the Map with the new list
			room.remove( position )
			room.put( position, positionInfo )
			roomName = room.get(5)
			X = positionInfo.get(0)
			Y = positionInfo.get(1)
			//orb.setHomeForChildren( roomName, X, Y )
			//now spawn the orb
			orb.warp( roomName.toString(), X, Y )
			orb.setHomeForChildren( "${roomName}", X, Y )
			currentOrb = orb.forceSpawnNow()
			//when the orb is destroyed, reset its record to 0 so the orb can spawn again later
			runOnDeath( currentOrb, { event ->
				event.killer.addPlayerVar( "Z01OrbCounter", 1 )
				deathRoomName = event.actor.getRoomName()
				deathX = event.actor.getX().intValue()
				deathY = event.actor.getY().intValue()
				//search through all the maps to find the map with the correct room name in it
				roomList.clone().each{
					if( it.get(5) == deathRoomName ) {
						deathRoom = it
					}
				}
				//once the correct map is found, use the death X to make a match for which position to read
				seed = 1
				deathPosition = 0
				findCorrectPosition()
				deathPositionInfo = deathRoom.get( deathPosition )
				//now that we have the correct position, change out the info properly so an orb can spawn there again
				deathPositionInfo.remove(2) //remove the placeholder at the end of the list
				deathPositionInfo.add(0)
				//now update the deathRoom Map with the updated list
				deathRoom.remove( deathPosition)
				deathRoom.put( deathPosition, deathPositionInfo )
			} )
		}
		//now spawn another orb a quarter-second from now
		myManager.schedule( 1 ) { spawnOrbs() }
	//otherwise, delay a few seconds and try again
	} else {
		myManager.schedule( 30 ) { checkForOrbRespawn() } 
	}
}

def findCorrectPosition() {
	if( seed <= 4 ) {
		deathPositionInfo = deathRoom.get(seed)
		//if the first element in this list is the same place the orb "died", then that's the correct position in the map
		if( deathPositionInfo.get(0) >= deathX - 40 && deathPositionInfo.get(0) <= deathX + 40 ) { //check a wide margin in case the exact position is slightly different than the spawner
			deathPosition = seed
		}
		seed ++
		findCorrectPosition()
	}
}


//start it all up
myManager.schedule(3) { startEventTimer() }
myManager.schedule(3) { checkForOrbRespawn() }


