import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
//ORB SPILL (OLD AQUEDUCT)                  
//------------------------------------------

//Make the orbs collectible only at the appropriate CL for the zone
def orbSpillSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= 7.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 7.0 or below to collect the orbs." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

orb = createStoppedEventSpawner( "Beach_205", "orb", "orb_beach", 100 )
orb.setPos( 350, 100 )
orb.setMonsterLevelForChildren( 6.6 )
orb.setHomeTetherForChildren( 6000 )
orb.setMiniEventSpec( orbSpillSpec )
orb.setEdgeHinting( false )
orb.setTargetCycle( false )

//------------------------------------------
//EVENT TIMER                               
//------------------------------------------

stopSpawning = false

def startEventTimer() {
	zoneBroadcast( "Jacques-VQS", "Thar Be Orbs!", "I spy wit' me goggle eyes, Charge Orbs poppin' out of the sand like fleas! Gather'm, me mates!" )
	makeZoneTimer("Orb Spill Timer", "0:15:0", "0:0:0").onCompletion( { event ->
		removeZoneTimer( "Orb Spill Timer" )
		removeZoneBroadcast( "Thar Be Orbs!" )
		stopSpawning = true
		myManager.schedule(2) {
			zoneBroadcast( "Jacques-VQS", "All Clear!", "Th' Orbs have ceased t'breed like overeager grunions. Get back to wot ye were doin' and trust that I'll keep a weather eye!" )
			orbDisposal()
			myManager.schedule(15) { removeZoneBroadcast( "All Clear!" ); eventDone() }
		}
	} ).start()
}

//------------------------------------------
//SPAWNING LOGIC                            
//------------------------------------------

orbEventPlayerList = []
numOrbPositionsPerRoom = 4
zonePlayerList = []

//Get the names of all the players in the zone
def makePlayerList(){
	//Empty the check list and then look into all rooms to find all players, whether in a Crew or not
	zonePlayerList.clear()
	myRooms.Beach_102.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_103.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_202.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_203.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_204.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_302.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_303.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_304.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_402.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_403.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_404.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_502.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_503.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_504.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_602.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_603.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_604.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_702.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_703.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_704.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_801.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_802.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_803.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_804.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_901.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_902.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_903.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_904.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_905.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_1001.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_1002.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_1003.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_1004.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_1005.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.Beach_1006.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
}

//Dispose of the orbs after the countdown is complete
def orbDisposal(){
	myRooms.Beach_102.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_103.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_202.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_203.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_204.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_302.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_303.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_304.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_402.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_403.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_404.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_502.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_503.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_504.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_602.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_603.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_604.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_702.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_703.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_704.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_801.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_802.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_803.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_804.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_901.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_902.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_903.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_904.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_905.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_1001.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_1002.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_1003.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_1004.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_1005.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
	myRooms.Beach_1006.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_beach" ) { it.dispose() } }
}

beach102Map = [ 1:[600, 180, 0], 2:[860, 210, 0], 3:[120, 420, 0], 4:[330, 510, 0], 5:"Beach_102" ]
beach103Map = [ 1:[780, 570, 0], 2:[250, 380, 0], 3:[600, 290, 0], 4:[70, 90, 0], 5:"Beach_103" ]
beach202Map = [ 1:[510, 110, 0], 2:[840, 490, 0], 3:[530, 640, 0], 4:[130, 165, 0], 5:"Beach_202" ]
beach203Map = [ 1:[380, 250, 0], 2:[790, 30, 0], 3:[480, 120, 0], 4:[250, 640, 0], 5:"Beach_203" ]
beach204Map = [ 1:[800, 350, 0], 2:[30, 60, 0], 3:[520, 620, 0], 4:[930, 210, 0], 5:"Beach_204" ]
beach302Map = [ 1:[550, 480, 0], 2:[60, 400, 0], 3:[640, 310, 0], 4:[90, 60, 0], 5:"Beach_302" ]
beach303Map = [ 1:[620, 340, 0], 2:[380, 560, 0], 3:[420, 620, 0], 4:[760, 180, 0], 5:"Beach_303" ]
beach304Map = [ 1:[710, 360, 0], 2:[400, 250, 0], 3:[950, 390, 0], 4:[170, 500, 0], 5:"Beach_304" ]
beach402Map = [ 1:[70, 370, 0], 2:[750, 470, 0], 3:[400, 150, 0], 4:[460, 570, 0], 5:"Beach_402" ]
beach403Map = [ 1:[250, 120, 0], 2:[880, 180, 0], 3:[630, 340, 0], 4:[670, 170, 0], 5:"Beach_403" ]
beach404Map = [ 1:[380, 160, 0], 2:[770, 640, 0], 3:[300, 440, 0], 4:[480, 640, 0], 5:"Beach_404" ]
beach502Map = [ 1:[610, 500, 0], 2:[1000, 360, 0], 3:[90, 360, 0], 4:[560, 230, 0], 5:"Beach_502" ]
beach503Map = [ 1:[240, 510, 0], 2:[60, 380, 0], 3:[300, 240, 0], 4:[910, 520, 0], 5:"Beach_503" ]
beach504Map = [ 1:[220, 190, 0], 2:[700, 300, 0], 3:[270, 520, 0], 4:[890, 40, 0], 5:"Beach_504" ]
beach602Map = [ 1:[510, 370, 0], 2:[980, 550, 0], 3:[40, 350, 0], 4:[750, 600, 0], 5:"Beach_602" ]
beach603Map = [ 1:[150, 560, 0], 2:[890, 90, 0], 3:[280, 180, 0], 4:[370, 430, 0], 5:"Beach_603" ]
beach604Map = [ 1:[590, 530, 0], 2:[320, 310, 0], 3:[870, 490, 0], 4:[920, 80, 0], 5:"Beach_604" ]
beach702Map = [ 1:[300, 570, 0], 2:[960, 420, 0], 3:[70, 490, 0], 4:[820, 30, 0], 5:"Beach_702" ]
beach703Map = [ 1:[260, 200, 0], 2:[620, 300, 0], 3:[610, 430, 0], 4:[50, 460, 0], 5:"Beach_703" ]
beach704Map = [ 1:[430, 490, 0], 2:[820, 290, 0], 3:[110, 250, 0], 4:[440, 50, 0], 5:"Beach_704" ]
beach801Map = [ 1:[440, 220, 0], 2:[700, 200, 0], 3:[160, 610, 0], 4:[1000, 410, 0], 5:"Beach_801" ]
beach802Map = [ 1:[160, 440, 0], 2:[990, 60, 0], 3:[630, 480, 0], 4:[170, 210, 0], 5:"Beach_802" ]
beach803Map = [ 1:[280, 60, 0], 2:[590, 370, 0], 3:[340, 470, 0], 4:[810, 240, 0], 5:"Beach_803" ]
beach804Map = [ 1:[280, 290, 0], 2:[870, 530, 0], 3:[860, 110, 0], 4:[300, 620, 0], 5:"Beach_804" ]
beach901Map = [ 1:[800, 440, 0], 2:[200, 480, 0], 3:[670, 550, 0], 4:[250, 100, 0], 5:"Beach_901" ]
beach902Map = [ 1:[850, 330, 0], 2:[190, 380, 0], 3:[260, 650, 0], 4:[990, 560, 0], 5:"Beach_902" ]
beach903Map = [ 1:[280, 480, 0], 2:[950, 170, 0], 3:[680, 100, 0], 4:[40, 620, 0], 5:"Beach_903" ]
beach904Map = [ 1:[540, 200, 0], 2:[940, 320, 0], 3:[870, 60, 0], 4:[170, 360, 0], 5:"Beach_904" ]
beach905Map = [ 1:[170, 590, 0], 2:[580, 620, 0], 3:[120, 130, 0], 4:[880, 100, 0], 5:"Beach_905" ]
beach1001Map = [ 1:[180, 350, 0], 2:[890, 120, 0], 3:[480, 580, 0], 4:[200, 60, 0], 5:"Beach_1001" ]
beach1002Map = [ 1:[380, 230, 0], 2:[880, 620, 0], 3:[940, 80, 0], 4:[360, 650, 0], 5:"Beach_1002" ]
beach1003Map = [ 1:[60, 60, 0], 2:[940, 530, 0], 3:[320, 610, 0], 4:[40, 240, 0], 5:"Beach_1003" ]
beach1004Map = [ 1:[230, 470, 0], 2:[620, 620, 0], 3:[800, 120, 0], 4:[940, 470, 0], 5:"Beach_1004" ]
beach1005Map = [ 1:[860, 600, 0], 2:[540, 340, 0], 3:[70, 590, 0], 4:[880, 440, 0], 5:"Beach_1005" ]
beach1006Map = [ 1:[590, 480, 0], 2:[800, 640, 0], 3:[130, 570, 0], 4:[190, 60, 0], 5:"Beach_1006" ]

roomList = [ beach102Map, beach103Map, beach202Map, beach203Map, beach204Map, beach302Map, beach303Map, beach304Map, beach402Map, beach403Map, beach404Map, beach503Map, beach503Map, beach504Map, beach602Map, beach603Map, beach604Map, beach702Map, beach703Map, beach704Map, beach801Map, beach802Map, beach803Map, beach804Map, beach901Map, beach902Map, beach903Map, beach904Map, beach905Map, beach1001Map, beach1002Map, beach1003Map, beach1004Map, beach1005Map, beach1006Map ]

def countOrbs() {
	orbCount = 0
	//count the number of orbs in the zone
	orbCount = orbCount + myRooms.Beach_102.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_103.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_202.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_203.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_204.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_302.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_303.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_304.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_402.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_403.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_404.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_502.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_503.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_504.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_602.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_603.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_604.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_702.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_703.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_704.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_801.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_802.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_803.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_804.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_901.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_902.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_903.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_904.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_905.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_1001.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_1002.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_1003.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_1004.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_1005.getSpawnTypeCount( "orb_beach" )
	orbCount = orbCount + myRooms.Beach_1006.getSpawnTypeCount( "orb_beach" )
}

//NOTE: The min limit for orbs in this zone is 10 instead of 20 because there are far fewer rooms in Barton Town than in other zones. Too many orbs with too few players is more like orb "harvesting" instead of orb "hunting".
def setMaxOrbs() {
	maxOrbs = areaPlayerCount()
	if( areaPlayerCount() < 20 ) {
		maxOrbs = 20
	}
}

//After the orbs have been spawned, keep checking until five or less of them exist...then spawn another wave of orbs
def checkForOrbRespawn() {
	if( stopSpawning == true ) return
	countOrbs()
	if( orbCount <= 5 ) {
		setMaxOrbs()
		spawnOrbs()
	} else {
		myManager.schedule(30) { checkForOrbRespawn() }
	}
}

//randomly spawn orbs
def spawnOrbs() {
	countOrbs()
	//if the current number of orbs in the zone is less than "maxOrbs", then spawn an orb
	if( orbCount < maxOrbs ) {
		//find the room in the zone to warp to
		room = random( roomList )
		//figure out the spawn position within that room
		position = random( numOrbPositionsPerRoom )
		positionInfo = room.get( position )
		//now look in position 2 of the "positionInfo" to see if the orb is spawned yet or not. (0 = not spawned; 1 = spawned)
		spawnHolder = positionInfo.get(2)
		if( spawnHolder == 0 ) {
			//change the "0" to a "1"
			positionInfo.remove( 2 ) ///remove the "placeholder" element in the current list
			positionInfo.add( 1 ) //put "1" at the end
			//now update the Map with the new list
			room.remove( position )
			room.put( position, positionInfo )
			roomName = room.get(5)
			X = positionInfo.get(0)
			Y = positionInfo.get(1)
			//orb.setHomeForChildren( roomName, X, Y )
			//now spawn the orb
			orb.warp( roomName.toString(), X, Y )
			orb.setHomeForChildren( "${roomName}", X, Y )
			currentOrb = orb.forceSpawnNow()
			//when the orb is destroyed, reset its record to 0 so the orb can spawn again later
			runOnDeath( currentOrb, { event ->
				event.killer.addPlayerVar( "Z01OrbCounter", 1 )
				deathRoomName = event.actor.getRoomName()
				deathX = event.actor.getX().intValue()
				deathY = event.actor.getY().intValue()
				//search through all the maps to find the map with the correct room name in it
				roomList.clone().each{
					if( it.get(5) == deathRoomName ) {
						deathRoom = it
					}
				}
				//once the correct map is found, use the death X to make a match for which position to read
				seed = 1
				deathPosition = 0
				findCorrectPosition()
				deathPositionInfo = deathRoom.get( deathPosition )
				//now that we have the correct position, change out the info properly so an orb can spawn there again
				deathPositionInfo.remove(2) //remove the placeholder at the end of the list
				deathPositionInfo.add(0)
				//now update the deathRoom Map with the updated list
				deathRoom.remove( deathPosition)
				deathRoom.put( deathPosition, deathPositionInfo )
			} )
		}
		//now spawn another orb a quarter-second from now
		myManager.schedule( 1 ) { spawnOrbs() }
	//otherwise, delay a few seconds and try again
	} else {
		myManager.schedule( 30 ) { checkForOrbRespawn() } 
	}
}

def findCorrectPosition() {
	if( seed <= 4 ) {
		deathPositionInfo = deathRoom.get(seed)
		//if the first element in this list is the same place the orb "died", then that's the correct position in the map
		if( deathPositionInfo.get(0) >= deathX - 40 && deathPositionInfo.get(0) <= deathX + 40 ) { //check a wide margin in case the exact position is slightly different than the spawner
			deathPosition = seed
		}
		seed ++
		findCorrectPosition()
	}
}


//start it all up
myManager.schedule(3) { startEventTimer() }
myManager.schedule(3) { checkForOrbRespawn() }


