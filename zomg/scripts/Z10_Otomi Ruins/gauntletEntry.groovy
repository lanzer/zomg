import com.gaiaonline.mmo.battle.script.*;

onClickEnterGauntlet = new Object()

gauntletEnter = makeSwitch( "Gauntlet_In", myRooms.OtRuins_304, 810, 480 )
gauntletEnter.unlock()
gauntletEnter.off()
gauntletEnter.setRange( 250 )
gauntletEnter.setMouseoverText("To the Gauntlet")

def enterGauntlet = { event ->
	synchronized( onClickEnterGauntlet ) {
		gauntletEnter.off()
		if( isPlayer( event.actor ) ) {
			currentPlayer = event.actor
			yourCrew = event.actor.getCrew()
			yourCrew.each { if( it.isOnQuest( 103, 2 ) || it.isOnQuest( 106, 2 ) ) { currentPlayer.setQuestFlag( GLOBAL, "Z10TempGauntletEntryAllowed" ) } }
			if( currentPlayer.hasQuestFlag( GLOBAL, "Z10TempGauntletEntryAllowed" ) ) {
				currentPlayer.centerPrint( "The vines move themselves aside to reveal a dark, humid, twisting stair to the cliffs above." ) 
				currentPlayer.warp( "OtCliffs_204", 640, 320 )
				currentPlayer.unsetQuestFlag( GLOBAL, "Z10TempGauntletEntryAllowed" )
				currentPlayer = null
			} else {
				event.actor.centerPrint( "Iron-hard vines block a passage that climbs up through the rock to the cliffs above." )
			}

		}
	}
}

gauntletEnter.whenOn( enterGauntlet )
