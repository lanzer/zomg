onClickEnterTwins = new Object()

twinsEnter = makeSwitch( "Twins_In", myRooms.OtRuins_701, 310, 500 )
twinsEnter.unlock()
twinsEnter.off()
twinsEnter.setRange( 250 )

def enterTwins = { event ->
	synchronized( onClickEnterTwins ) {
		twinsEnter.off()
		if( isPlayer( event.actor ) ) {
			event.actor.warp( "OtCliffs_501", 210, 490 )
		}
	}
}

twinsEnter.whenOn( enterTwins )