import com.gaiaonline.mmo.battle.script.*;

def stair804Trigger = "stair804Trigger"
myRooms.OtRuins_804.createTriggerZone(stair804Trigger, 390, 235, 620, 400)
//Trigger zone not currently used

//------------------------------------------
// Tiny Terror Wall Guards (Room 804)       
//------------------------------------------

def wallFacingInside01 = myRooms.OtRuins_804.spawnSpawner( "wallFacingInside01", "tiny_terror", 1)
wallFacingInside01.setPos( 215, 315 )
wallFacingInside01.setGuardPostForChildren( wallFacingInside01, 225 )
wallFacingInside01.setWaitTime( 50, 70 )
wallFacingInside01.setMonsterLevelForChildren( 7.2 )

def wallFacingInside02 = myRooms.OtRuins_804.spawnSpawner( "wallFacingInside02", "tiny_terror", 1)
wallFacingInside02.setPos( 585, 90 )
wallFacingInside02.setGuardPostForChildren( wallFacingInside02, 225 )
wallFacingInside02.setWaitTime( 50, 70 )
wallFacingInside02.setMonsterLevelForChildren( 7.2 )

def wallFacingOutside01 = myRooms.OtRuins_804.spawnSpawner( "wallFacingOutside01", "tiny_terror", 1)
wallFacingOutside01.setPos( 135, 360 )
wallFacingOutside01.setGuardPostForChildren( wallFacingOutside01, 45 )
wallFacingOutside01.setWaitTime( 50, 70 )
wallFacingOutside01.setMonsterLevelForChildren( 7.2 )

def wallFacingOutside02 = myRooms.OtRuins_804.spawnSpawner( "wallFacingOutside02", "tiny_terror", 1)
wallFacingOutside02.setPos( 650, 45 )
wallFacingOutside02.setGuardPostForChildren( wallFacingOutside02, 45 )
wallFacingOutside02.setWaitTime( 50, 70 )
wallFacingOutside02.setMonsterLevelForChildren( 7.2 )

//------------------------------------------
// Vase Stair Guards                        
//------------------------------------------

def rightStairGuard = myRooms.OtRuins_804.spawnSpawner( "rightStairGuard", "bladed_statue", 1 )
rightStairGuard.setPos( 735, 280 )
rightStairGuard.setGuardPostForChildren( rightStairGuard, 45 )
rightStairGuard.setWaitTime( 50, 70 )
rightStairGuard.setMonsterLevelForChildren( 7.2 )

def leftStairGuard = myRooms.OtRuins_804.spawnSpawner( "leftStairGuard", "bladed_statue", 1 )
leftStairGuard.setPos( 330, 520 )
leftStairGuard.setGuardPostForChildren( leftStairGuard, 45)
leftStairGuard.setWaitTime( 50, 70 )
leftStairGuard.setMonsterLevelForChildren( 7.2 )


//==========================
// ALLIANCES                
//==========================

wallFacingInside01.allyWithSpawner( wallFacingInside02 )
wallFacingInside01.allyWithSpawner( wallFacingOutside01 )
wallFacingInside01.allyWithSpawner( wallFacingOutside02 )
wallFacingInside02.allyWithSpawner( wallFacingOutside01 )
wallFacingInside02.allyWithSpawner( wallFacingOutside02 )
wallFacingOutside01.allyWithSpawner( wallFacingOutside02 )

//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

wallFacingInside01.forceSpawnNow()
wallFacingInside02.forceSpawnNow()
wallFacingOutside01.forceSpawnNow()
wallFacingOutside02.forceSpawnNow()

rightStairGuard.forceSpawnNow()
leftStairGuard.forceSpawnNow()
