import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Vases                                    
//------------------------------------------

def VaseA = myRooms.OtRuins_602.spawnSpawner( "VaseA", "bladed_statue", 1 )
VaseA.setPos( 465, 115 )
VaseA.setWaitTime( 50, 70 )
VaseA.setGuardPostForChildren( VaseA, 90 )
VaseA.addPatrolPointForChildren( "OtRuins_602", 110, 250, 2 )
VaseA.addPatrolPointForChildren( "OtRuins_602", 430, 380, 2 )
VaseA.setMonsterLevelForChildren( 7.6 )

def VaseB = myRooms.OtRuins_602.spawnSpawner( "VaseB", "bladed_statue", 1 )
VaseB.setPos( 700, 475 )
VaseB.setWaitTime( 50, 70 )
VaseB.setGuardPostForChildren( VaseB, 135 )
VaseB.addPatrolPointForChildren( "OtRuins_602", 530, 430, 2 )
VaseB.addPatrolPointForChildren( "OtRuins_602", 765, 520, 2 )
VaseB.setMonsterLevelForChildren( 7.6 )

//------------------------------------------
// Tiny Terror Wanderers                    
//------------------------------------------

def tinyWanderers602 = myRooms.OtRuins_602.spawnSpawner( "tinyWanderers602", "tiny_terror", 4 )
tinyWanderers602.setPos( 500, 250 )
tinyWanderers602.setWaitTime( 30, 60 )
tinyWanderers602.setWanderBehaviorForChildren( 50, 150, 3, 7, 200)
tinyWanderers602.setHomeTetherForChildren( 2000 )
tinyWanderers602.setHateRadiusForChildren( 2000 )
tinyWanderers602.setMonsterLevelForChildren( 7.6 )


//------------------------------------------
// Tiny Patroller                           
//------------------------------------------

def tinyPatroller602 = myRooms.OtRuins_602.spawnSpawner( "tinyPatroller602", "tiny_terror", 1 )
tinyPatroller602.setPos( 505, 250 )
tinyPatroller602.setWaitTime( 60, 90 )
tinyPatroller602.setHome( tinyWanderers602 )
tinyPatroller602.setRFH( true )
tinyPatroller602.setCFH( 300 )
tinyPatroller602.setHomeTetherForChildren( 2000 )
tinyPatroller602.setHateRadiusForChildren( 2000 )
tinyPatroller602.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
tinyPatroller602.setCowardLevelForChildren( 100 )
tinyPatroller602.addPatrolPointForChildren( "OtRuins_602", 510, 250, 10 )
tinyPatroller602.addPatrolPointForChildren( "OtRuins_603", 330, 190, 1 )
tinyPatroller602.setMonsterLevelForChildren( 7.6 )


//==========================
// ALLIANCES                
//==========================
tinyWanderers602.allyWithSpawner( VaseA )
tinyWanderers602.allyWithSpawner( VaseB )

tinyPatroller602.allyWithSpawner( tinyWanderers602 )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

tinyWanderers602.spawnAllNow()
tinyPatroller602.forceSpawnNow()
VaseA.forceSpawnNow()
VaseB.forceSpawnNow()