import com.gaiaonline.mmo.battle.script.*;

def ambush805Trigger = "ambush805Trigger"
myRooms.OtRuins_805.createTriggerZone(ambush805Trigger, 90, 350, 530, 630)

myManager.onTriggerIn(myRooms.OtRuins_805, ambush805Trigger) { event ->
	if( isPlayer(event.actor) && event.actor.getConLevel() <= 8.5 ) {
		triggerAmbush( event )
	}
}

//------------------------------------------
// Tiny Terror Ambush (Room 805)            
//------------------------------------------

baitSpawner = myRooms.OtRuins_805.spawnStoppedSpawner( "baitSpawner", "tiny_terror", 1 )
baitSpawner.setPos( 400, 468)
baitSpawner.setWanderBehaviorForChildren( 25, 50, 2, 5, 100)
baitSpawner.setMonsterLevelForChildren( 7.1 )

northSpawner = myRooms.OtRuins_805.spawnStoppedSpawner( "northSpawner", "tiny_terror_ambusher", 20)
northSpawner.setPos( 330, 270 )
northSpawner.setInitialMoveForChildren( "OtRuins_805", 300, 480 )
northSpawner.setHomeForChildren( "OtRuins_805", 300, 480 )
northSpawner.setWanderBehaviorForChildren( 25, 50, 2, 5, 100)
northSpawner.setMonsterLevelForChildren( 7.1 )

eastSpawner = myRooms.OtRuins_805.spawnStoppedSpawner( "eastSpawner", "tiny_terror_ambusher", 20)
eastSpawner.setPos( 680, 490 )
eastSpawner.setInitialMoveForChildren( "OtRuins_805", 300, 480 )
eastSpawner.setHomeForChildren( "OtRuins_805", 300, 480 )
eastSpawner.setWanderBehaviorForChildren( 25, 50, 2, 5, 100)
eastSpawner.setMonsterLevelForChildren( 7.1 )

southSpawner = myRooms.OtRuins_805.spawnStoppedSpawner( "southSpawner", "tiny_terror_ambusher", 20)
southSpawner.setPos( 360, 660 )
southSpawner.setInitialMoveForChildren( "OtRuins_805", 300, 480 )
southSpawner.setHomeForChildren( "OtRuins_805", 300, 480 )
southSpawner.setWanderBehaviorForChildren( 25, 50, 2, 5, 100)
southSpawner.setMonsterLevelForChildren( 7.1 )

westSpawner = myRooms.OtRuins_805.spawnStoppedSpawner( "westSpawner", "tiny_terror_ambusher", 20)
westSpawner.setPos( 50, 490 )
westSpawner.setInitialMoveForChildren( "OtRuins_805", 300, 480 )
westSpawner.setHomeForChildren( "OtRuins_805", 300, 480 )
westSpawner.setWanderBehaviorForChildren( 25, 50, 2, 5, 100)
westSpawner.setMonsterLevelForChildren( 7.1 )

numSpawned = 0
trapNotSprung = true
ambushSpawnerList = [ northSpawner, eastSpawner, southSpawner, westSpawner ]
ambushSaying = [ "yaoc!", "yaotlahueliloc!", "quinyaochihuaz!", "yaomiquico!", "yaotihua!"]
playerSet = [] as Set

myManager.onEnter( myRooms.OtRuins_805 ) { event ->
	if( isPlayer( event.actor ) && event.actor.getConLevel() <= 8.5 ) {
		playerSet << event.actor
	}
}

myManager.onExit( myRooms.OtRuins_805 ) { event ->
	if( isPlayer( event.actor ) && playerSet.contains( event.actor ) ) {
		playerSet.remove( event.actor )
	}
}

maxAmbush = 8

def triggerAmbush( event ) {
	if( trapNotSprung ) {
		trapNotSprung = false
		numSpawn = playerSet.size() + 1
		if( numSpawn > maxAmbush ) { numSpawn = maxAmbush }
		if( ambushMonsterList.size() > maxAmbush ) { numSpawn = 0 } //safety net so I don't have to dispose monsters if they are still aggro'd below
		ambushMonsterList.clear()
		populateAmbush( event )
	}
}	

ambushMonsterList = []
targetList = []

def populateAmbush( event ) {
	if( numSpawn > 0 ) {
		ambushTerror = random( ambushSpawnerList ).forceSpawnNow()
		ambushMonsterList << ambushTerror
		numSpawn --
		targetList.clear()
		event.actor.getCrew().each{ if( playerSet.contains( it ) ) { targetList << it } }
		if( !targetList.isEmpty() ) {
			ambushTerror.addHate( random( targetList ), 5000 )
		} else if( !playerSet.isEmpty() ) {
			ambushTerror.addHate( random( playerSet ), 50 )
		}
		ambushTerror.say( random( ambushSaying ) )
		myManager.schedule( 0.5 ) { populateAmbush( event ) }
	} else {
		//reset the trap one minute later for new unsuspecting players
		myManager.schedule( 60 ) { watchForReset() }
	}
}


def watchForReset() {
	if( !bait.isDead() && bait.isAggro() ) {
		myManager.schedule(2) { watchForReset() }
	} else {
		if( !bait.isDead() ) { bait.dispose() }
		ambushMonsterList.each{		
			if( !it.isDead() && !it.isAggro() ) { it.dispose() }
		}
		//re-bait the trap five seconds later
		myManager.schedule(5) {
			trapNotSprung = true
			bait = baitSpawner.forceSpawnNow()
		}
	}
}


//==========================
// ALLIANCES                
//==========================
northSpawner.allyWithSpawner( eastSpawner )
northSpawner.allyWithSpawner( southSpawner )
northSpawner.allyWithSpawner( westSpawner )
northSpawner.allyWithSpawner( baitSpawner )
eastSpawner.allyWithSpawner( southSpawner )
eastSpawner.allyWithSpawner( westSpawner )
eastSpawner.allyWithSpawner( baitSpawner )
southSpawner.allyWithSpawner( westSpawner )
southSpawner.allyWithSpawner( baitSpawner )
westSpawner.allyWithSpawner( baitSpawner )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

bait = baitSpawner.forceSpawnNow()

