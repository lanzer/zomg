import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Mask Spawner                             
//------------------------------------------

maskSpawner402 = myRooms.OtRuins_402.spawnStoppedSpawner( "maskSpawner402", "mask", 3 )
maskSpawner402.setPos( 420, 270)
maskSpawner402.setHome( "OtRuins_402", 525, 340 )
maskSpawner402.setWanderBehaviorForChildren( 50, 150, 4, 9, 300)
maskSpawner402.setMonsterLevelForChildren( 7.8 )

//------------------------------------------
// Feathered Coatl Spawner                  
//------------------------------------------

FCAmbushGuard = myRooms.OtRuins_402.spawnStoppedSpawner( "FCAmbushGuard", "feathered_coatl", 12 )
FCAmbushGuard.setPos( 420, 270 )
FCAmbushGuard.setHome( "OtRuins_402", 525, 340 )
FCAmbushGuard.setWanderBehaviorForChildren( 100, 250, 2, 5, 350)
FCAmbushGuard.setMonsterLevelForChildren( 7.8 )

playerSet = [] as Set

//=========================
// SPAWNCOUNT MANAGERS     
//=========================

myManager.onEnter(myRooms.OtRuins_402) { event ->
	if( isPlayer(event.actor) ) {
		playerSet << event.actor
	}
}

myManager.onExit(myRooms.OtRuins_402) { event ->
	if ( isPlayer(event.actor) ) {
		playerSet.remove( event.actor )
	}
}

//------------------------------------------
// Respawner Scripts (Ambush Strategies)    
//------------------------------------------

roomIsReset = true
stairFightInProgress = false

def ambushTrigger402A = "ambushTrigger402A"
myRooms.OtRuins_402.createTriggerZone( ambushTrigger402A, 340, 520, 530, 560 )

myManager.onTriggerIn(myRooms.OtRuins_402, ambushTrigger402A) { event ->
	if( stairFightInProgress == false && isPlayer( event.actor ) && roomIsReset == true && event.actor.getConLevel() <= 8.5 ) {
		stairFightInProgress = true
		roomIsReset = false
		playersIn402 = playerSet.size()
		
		//want twice as many FCs as players
		if( playersIn402 > 6 ) {
			playersIn402 = 6
		}
		numSpawnFC = (playersIn402 * 2)
		println "**** numSpawnFC = ${ numSpawnFC } ****"
		
		//one Mask for every three players, rounded up
		numSpawnMask = (playersIn402 / 2)
		if( ( playersIn402 % 2 ) != 0 ) { numSpawnMask ++ }
		println "**** numSpawnMask = ${ numSpawnMask } ****"
		
		spawnFCAmbushers()
		spawnMaskAmbushers()
		checkForReset()
	}
}
		
def ambushTrigger402B = "ambushTrigger402B"
myRooms.OtRuins_402.createTriggerZone( ambushTrigger402B, 715, 480, 910, 515 )

myManager.onTriggerIn(myRooms.OtRuins_402, ambushTrigger402B) { event ->
	if( stairFightInProgress == false && isPlayer( event.actor ) && roomIsReset == true && event.actor.getConLevel() <= 8.5 ) {
		stairFightInProgress = true
		roomIsReset = false
		playersIn402 = playerSet.size()
		
		//want twice as many FCs as players
		if( playersIn402 > 6 ) {
			playersIn402 = 6
		}
		numSpawnFC = (playersIn402 * 2)
		println "**** numSpawnFC = ${ numSpawnFC } ****"
		
		//one Mask for every three players, rounded up
		numSpawnMask = (playersIn402 / 2)
		if( ( playersIn402 % 2 ) != 0 ) { numSpawnMask ++ }
		println "**** numSpawnMask = ${ numSpawnMask } ****"
		
		spawnFCAmbushers()
		spawnMaskAmbushers()
		checkForReset()
	}
}
		
def spawnFCAmbushers() {
	if( numSpawnFC > 0 ) {
		fc1 = FCAmbushGuard.forceSpawnNow()
		if( !playerSet.isEmpty() ) { fc1.addHate( random( playerSet ), 1 ) }
		numSpawnFC --
		myManager.schedule(1) { spawnFCAmbushers() }
	}
}

def spawnMaskAmbushers() {
	if( numSpawnMask >= 1 ) {
		m1 = maskSpawner402.forceSpawnNow()
		if( !playerSet.isEmpty() ) { m1.addHate( random( playerSet ), 1 ) }
		numSpawnMask --
		myManager.schedule(2) { spawnMaskAmbushers() }
	}
}
		
def checkForReset() {
	//if all mobs are detroyed AND there are no players in the room then reset the flag for the ambush to occur again
	//if not, then check again in five seconds
	if( myRooms.OtRuins_402.getSpawnTypeCount( "mask" ) + myRooms.OtRuins_402.getSpawnTypeCount( "feathered_coatl" ) == 0 && fc1.isDead() && m1.isDead() ) {
		numSpawnFC = 0
		numSpawnMask = 0
		roomIsReset = true
		stairFightInProgress = false
	} else {
		myManager.schedule(30) { checkForReset() }
	}
}

//==========================
//ALLIANCES                 
//==========================
maskSpawner402.allyWithSpawner( FCAmbushGuard )


//------------------------------------------
// ENTRANCE TO THRONE ROOM SWITCH           
//------------------------------------------

timerMap = [:]
onClickThroneBlock = new Object()

throneDoor = makeSwitch( "throneRoomDoor", myRooms.OtRuins_402, 435, 280 )
throneDoor.unlock()
throneDoor.off()
throneDoor.setRange( 300 )
throneDoor.setMouseoverText("The Throne Room")

def enterThrone = { event ->
	synchronized( onClickThroneBlock ) {
		throneDoor.off()
		event.actor.getCrew().each {
			if( it.isOnQuest( 104 ) || it.isDoneQuest( 104 ) ) {
				event.actor.setQuestFlag( GLOBAL, "Z10TempThroneEntryAllowed" )
			}
		}
		if( event.actor.hasQuestFlag( GLOBAL, "Z10TempThroneEntryAllowed" ) ) {
			//********************* START DIFFICULTY SETTINGS ****************************
			if( event.actor.getTeam().hasAreaVar( "Z18_Otami_Throne_Room", "Z18ThroneRoomDifficulty" ) == false ) {
				if( event.actor.getCrewVar( "Z18ThroneRoomDifficultySelectionInProgress" ) == 0 ) {
					//Prevent multiple players from initiating the process of selecting difficulties
					event.actor.setCrewVar( "Z18ThroneRoomDifficultySelectionInProgress", 1 )

					//check to see if the Crew leader is in the zone
					if( isInZone( event.actor.getTeam().getLeader() ) ) {
						leader = event.actor.getTeam().getLeader()
						makeMenu()

						event.actor.getCrew().each{ if( it != event.actor.getTeam().getLeader() ) { it.centerPrint( "Your Crew Leader is choosing the challenge level for this encounter." ) } }

						//Everyone else gets the following message while the leader chooses a setting. If no choice is made after 10 seconds, then everyone gets a message that says:
						decisionTimer = myManager.schedule(30) {
							event.actor.getCrew().each{
								it.centerPrint( "No choice was made within 30 seconds. Click the stone block to try again." )
							}
							event.actor.setCrewVar( "Z18ThroneRoomDifficultySelectionInProgress", 0 )
							//if no selection was made, then remove the leader and timer from the timerMap
							timerMap.remove( event.actor.getTeam().getLeader() )
						}
						//add the leader and timer to a map so the timer can be canceled later, if necessary
						timerMap.put( event.actor.getTeam().getLeader(), decisionTimer )

					//If the leader is not in the zone then tell the crew the leader must be present
					} else {
						event.actor.getCrew().each{
							if( it != event.actor.getTeam().getLeader() ) {
								it.centerPrint( "Your Crew Leader must be in Otami Ruins before you can enter the Throne Room." )
							} else {
								it.centerPrint( "Your Crew is trying to enter the Throne Room. You must be in Otami Ruins for that to occur." )
							}
						}
						event.actor.setCrewVar( "Z18ThroneRoomDifficultySelectionInProgress", 0 )
					}
				} else if( event.actor.getCrewVar( "Z18ThroneRoomDifficultySelectionInProgress" ) == 1 ) {
					if( event.actor != event.actor.getTeam().getLeader() ) {
						event.actor.centerPrint( "Your Crew Leader is already making a challenge level choice. One moment, please..." )
					}
				}

			//If the Difficulty is not zero, then it has been set and the player can enter the Shrine
			} else if( event.actor.getTeam().hasAreaVar( "Z18_Otami_Throne_Room", "Z18ThroneRoomDifficulty" ) == true ) { 					
				event.actor.centerPrint( "The stone becomes like air and you pass through into the Throne Room beyond." )
				event.actor.warp( "Throne_1", 1430, 900 )	
			} 
			//********************* END DIFFICULTY SETTINGS ****************************

		} else {
			event.actor.centerPrint( "You push on the giant stone door, but to no avail. It's gigantic weight is beyond your strength." )
		}
	}
}

throneDoor.whenOn( enterThrone )

def synchronized makeMenu() {
	descripString = "You must choose a Challenge Level to enter the Throne Room. You have 30 seconds to decide."
	diffOptions = ["Easy (small rewards)", "Normal (moderate rewards)", "Hard (large rewards)", "Cancel"]
	
	uiButtonMenu( leader, "diffMenu", descripString, diffOptions, 300, 30 ) { event ->
		if( event.selection == "Easy (small rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Easy' challenge setting for this scenario. Click the stone block again to enter." )
				} else {
					it.centerPrint( "You chose the 'Easy' challenge setting for this scenario. Click the stone block again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z18_Otami_Throne_Room", "Z18ThroneRoomDifficulty", 1 )
			event.actor.setCrewVar( "Z18ThroneRoomDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Normal (moderate rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Normal' challenge setting for this scenario. Click the stone block again to enter." )
				} else {
					it.centerPrint( "You chose the 'Normal' challenge setting for this scenario. Click the stone block again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z18_Otami_Throne_Room", "Z18ThroneRoomDifficulty", 2 )
			event.actor.setCrewVar( "Z18ThroneRoomDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Hard (large rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Hard' challenge setting for this scenario. Click the stone block again to enter." )
				} else {
					it.centerPrint( "You chose the 'Hard' challenge setting for this scenario. Click the stone block again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z18_Otami_Throne_Room", "Z18ThroneRoomDifficulty", 3 )
			event.actor.setCrewVar( "Z18ThroneRoomDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Cancel" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose to not select a challenge setting at this time." )
				}
			}
			event.actor.setCrewVar( "Z18ThroneRoomDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
	}
}
