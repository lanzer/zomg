import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Tiny Terror Wall Guards (Room 803)       
//------------------------------------------

wallFacingInside01 = myRooms.OtRuins_803.spawnSpawner( "wallFacingInside01", "tiny_terror", 1)
wallFacingInside01.setPos( 90, 220 )
wallFacingInside01.setGuardPostForChildren( wallFacingInside01, 225 )
wallFacingInside01.setWaitTime( 1, 2 )
wallFacingInside01.setMonsterLevelForChildren( 7.3 )

wallFacingOutside01 = myRooms.OtRuins_803.spawnSpawner( "wallFacingOutside01", "tiny_terror", 1)
wallFacingOutside01.setPos( 170, 180 )
wallFacingOutside01.setGuardPostForChildren( wallFacingOutside01, 45 )
wallFacingOutside01.setWaitTime( 1, 2 )
wallFacingOutside01.setMonsterLevelForChildren( 7.3 )

//------------------------------------------
// Mask Spawner                             
//------------------------------------------

maskSpawner803 = myRooms.OtRuins_803.spawnSpawner( "maskSpawner803", "mask", 6 )
maskSpawner803.setPos( 730, 140 )
maskSpawner803.setCFH( 600 )
maskSpawner803.setMonsterLevelForChildren( 7.3 )

//=========================
// SPAWNCOUNT MANAGERS     
//=========================

myManager.onEnter(myRooms.OtRuins_803) { event ->
	if( isPlayer(event.actor) ) {
		playerList803 << event.actor
	}
}

myManager.onExit(myRooms.OtRuins_803) { event ->
	if ( isPlayer(event.actor) ) {
		playerList803.remove( event.actor )
	}
}

//------------------------------------------
// Respawner Scripts (Ambush Strategies)    
//------------------------------------------

//Objectives:
//	Once a Mask is spawned, all objects in the room should pick targets to hate.
//	Room should have to be cleared and empty before respawns should occur.
//		Trigger zone at base of stairs starts the ambush
// 		Number in ambush dependent on number of players in the room at the time

playerList803 = []

roomIsReset = true
respawnConditionMet = false

MaskList = []

def stair803Trigger = "stair803Trigger"
myRooms.OtRuins_803.createTriggerZone(stair803Trigger, 400, 145, 610, 270)

myManager.onTriggerIn(myRooms.OtRuins_803, stair803Trigger) { event ->
	if( isPlayer( event.actor ) && roomIsReset == true && event.actor.getConLevel() <= 8.5 ) {
		roomIsReset = false
		numSpawnMask = playerList803.size()
		
		if( numSpawnMask > 6 ) {
			numSpawnMask = 6
		}
		
		//Make the wall guards hate someone if they don't already
		if( !wall1.isDead() ) { //if NOT dead
			if( !playerList803.isEmpty() ) { wall1.addHate( random( playerList803 ), 1 ) }
		}
		if( !wall2.isDead() ) { //if NOT dead
			if( !playerList803.isEmpty() ) { wall2.addHate( random( playerList803 ), 1 ) }
		}
		
		//Now spawn the masks
		spawnMaskAmbushers()
		
	}
}
		
def spawnMaskAmbushers() {
	if( numSpawnMask > 0 ) {
		MaskList << maskSpawner803.forceSpawnNow() 
		MaskList.get( MaskList.size() - 1 ).addHate( random ( playerList803 ), 1 ) 
		runOnDeath( MaskList.get( MaskList.size() - 1 ) ) { MaskList.remove( MaskList.get( MaskList.size() - 1 ) ); checkForTrapReset() }
		numSpawnMask -- 
		myManager.schedule(1) { spawnMaskAmbushers() } 
	}
}

def synchronized checkForTrapReset() {
	//if all masks are detroyed then the ambush flag is reset 60 seconds thereafter
	if( respawnConditionMet == false && MaskList.isEmpty() ) {
		respawnConditionMet = true
		myManager.schedule(60) { spawnTTWallGuards(); MaskList = []; roomIsReset = true; respawnConditionMet = false }
	}
}

def spawnTTWallGuards() {
	if( wallFacingInside01.spawnsInUse() == 0 ) {
		wall1 = wallFacingInside01.forceSpawnNow()
	}
	if( wallFacingOutside01.spawnsInUse() == 0 ) {
		wall2 = wallFacingOutside01.forceSpawnNow()
	}
}

//==========================
// ALLIANCES                
//==========================

wallFacingInside01.allyWithSpawner( wallFacingOutside01 )
wallFacingInside01.allyWithSpawner( maskSpawner803 )
wallFacingOutside01.allyWithSpawner( maskSpawner803 )

//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

wall1 = wallFacingInside01.forceSpawnNow()
wall2 = wallFacingOutside01.forceSpawnNow()

wallFacingInside01.stopSpawning()
wallFacingOutside01.stopSpawning()

maskSpawner803.stopSpawning()