import com.gaiaonline.mmo.battle.script.*;


//------------------------------------------
// Tiny Terror Group                        
//------------------------------------------

def TTWanderers605 = myRooms.OtRuins_605.spawnSpawner( "TTWanderers605", "tiny_terror", 2 )
TTWanderers605.setPos( 650, 65 )
TTWanderers605.setWaitTime( 50, 70 )
TTWanderers605.setWanderBehaviorForChildren( 25, 100, 3, 7, 150)
TTWanderers605.setMonsterLevelForChildren( 7.3 )

def TTWanderers605b = myRooms.OtRuins_605.spawnSpawner( "TTWanderers605b", "tiny_terror", 2 )
TTWanderers605b.setPos( 240, 580 )
TTWanderers605b.setWaitTime( 50, 70 )
TTWanderers605b.setWanderBehaviorForChildren( 25, 100, 3, 7, 150)
TTWanderers605b.setMonsterLevelForChildren( 7.3 )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

TTWanderers605.spawnAllNow()
TTWanderers605b.spawnAllNow()