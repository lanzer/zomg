import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Stair Vases                              
//------------------------------------------

def leftStairVase = myRooms.OtRuins_802.spawnSpawner( "leftStairVase", "bladed_statue", 1)
leftStairVase.setPos( 450, 410 )
leftStairVase.setWaitTime( 60, 90 )
leftStairVase.setGuardPostForChildren( leftStairVase, 45 )
leftStairVase.setCFH( 600 )
leftStairVase.setMonsterLevelForChildren( 7.4 )

def rightStairVase = myRooms.OtRuins_802.spawnSpawner( "rightStairVase", "bladed_statue", 1)
rightStairVase.setPos( 750, 210 )
rightStairVase.setWaitTime( 60, 90 )
rightStairVase.setGuardPostForChildren( rightStairVase, 45 )
rightStairVase.setCFH( 600 )
rightStairVase.setMonsterLevelForChildren( 7.4 )

//------------------------------------------
// ROOM 702 VASE RESERVES                   
//------------------------------------------

def vaseReserveA = myRooms.OtRuins_702.spawnSpawner( "vaseReserveA", "bladed_statue", 2)
vaseReserveA.setPos( 320, 350 )
vaseReserveA.setWaitTime( 50, 70 )
vaseReserveA.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
vaseReserveA.setMonsterLevelForChildren( 7.4 )

def vaseReserveB = myRooms.OtRuins_702.spawnSpawner( "vaseReserveB", "bladed_statue", 2)
vaseReserveB.setPos( 470, 430 )
vaseReserveB.setWaitTime( 50, 70 )
vaseReserveB.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
vaseReserveB.setMonsterLevelForChildren( 7.4 )

def eastTTGuard = myRooms.OtRuins_702.spawnSpawner( "eastTTGuard", "tiny_terror", 1)
eastTTGuard.setPos( 825, 455 )
eastTTGuard.setGuardPostForChildren( eastTTGuard, 315 )
eastTTGuard.setHome( vaseReserveB )
eastTTGuard.setRFH( true )
eastTTGuard.setCFH( 200 )
eastTTGuard.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
eastTTGuard.setCowardLevelForChildren( 100 )
eastTTGuard.setWaitTime( 30, 60 )
eastTTGuard.setMonsterLevelForChildren( 7.4 )

//------------------------------------------
// Tiny Terror Guards                       
// These guards pull the vases out of       
// Room 702 if not stopped                  
//------------------------------------------

def leftGuard = myRooms.OtRuins_802.spawnSpawner( "leftGuard", "tiny_terror", 1)
leftGuard.setPos( 400, 145 )
leftGuard.setGuardPostForChildren( leftGuard, 45 )
leftGuard.setHome( vaseReserveA )
leftGuard.setRFH( true )
leftGuard.setCFH( 200 )
leftGuard.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
leftGuard.setCowardLevelForChildren( 100 )
leftGuard.setWaitTime( 30, 60 )
leftGuard.setMonsterLevelForChildren( 7.4 )

def rightGuard = myRooms.OtRuins_802.spawnSpawner( "rightGuard", "tiny_terror", 1)
rightGuard.setPos( 510, 80 )
rightGuard.setGuardPostForChildren( rightGuard, 45 )
rightGuard.setHome( vaseReserveB )
rightGuard.setRFH( true )
rightGuard.setCFH( 200 )
rightGuard.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
rightGuard.setCowardLevelForChildren( 100 )
rightGuard.setWaitTime( 30, 60 )
rightGuard.setMonsterLevelForChildren( 7.4 )

//==========================
// ALLIANCES                
//==========================
leftGuard.allyWithSpawner( vaseReserveA )
rightGuard.allyWithSpawner( vaseReserveB )
eastTTGuard.allyWithSpawner( vaseReserveB )
eastTTGuard.allyWithSpawner( vaseReserveA )

leftStairVase.allyWithSpawner( rightStairVase )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

leftStairVase.forceSpawnNow()
rightStairVase.forceSpawnNow()
eastTTGuard.forceSpawnNow()
leftGuard.forceSpawnNow()
rightGuard.forceSpawnNow()
vaseReserveA.spawnAllNow()
vaseReserveB.spawnAllNow()
