//EXPLOIT PREVENTIONS
// - Ensure that players that exit 304 are added back onto the warp list.

import com.gaiaonline.mmo.battle.script.*;

ML = 7.5 //this is the bottom threshhold for this scenario. Minimum of 7.5 and Max of 10.0

otCliffsRoomList = ["OtCliffs_1","OtCliffs_2","OtCliffs_3","OtCliffs_101","OtCliffs_102","OtCliffs_103","OtCliffs_104","OtCliffs_201","OtCliffs_202","OtCliffs_203","OtCliffs_204","OtCliffs_301","OtCliffs_303","OtCliffs_304","OtCliffs_401","OtCliffs_501","OtCliffs_503","OtCliffs_601"]

gauntletZoneCheckList = []

def calculateNewMonsterLevel() {
	//Empty the check list and then look into all rooms to find all players, whether in a Crew or not
	newOneSet = false
	playerCLCheckList = []
	myRooms.OtCliffs_1.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_2.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_3.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_101.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_102.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_103.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_104.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_202.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_203.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_204.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_303.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_304.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	
	//Now cycle through the playerCLCheckList and figure out the new ML level, ensuring that it never exceeds 10.0
	//Look through the list. If any of them are greater than the current ML, then reset the ML and we're done
	playerCLCheckList.each{ 
		if( it.getConLevel() >= ML && it.getConLevel() <= 10.0 ) {
			ML = it.getConLevel()
			newOneSet = true
		} else if ( it.getConLevel() > 10.0 ) {
			ML = 10.0
			newOneSet = true
		}
	}
	//If no one was greater than or equal to the current ML, then we need to lower the ML for folks to keep playing...but still no lower than CL 7.5
	if( newOneSet == false ) {
		ML = 7.5
		playerCLCheckList.each{
			if( it.getConLevel() > ML ) {
				ML = it.getConLevel()
			}
		}
	}
	//Make the return trip a bit easier ML, but increase the spawns and crowd control stuff
	if( gauntletStarted == true ) {
		ML = ML - 0.2 
	}
	
	//Now update all the spawners with the new ML so they start creating monsters at that new level
	mask103a.setMonsterLevelForChildren( ML )
	mask103b.setMonsterLevelForChildren( ML )
	mask103c.setMonsterLevelForChildren( ML )
	tiny103a.setMonsterLevelForChildren( ML )
	tiny103b.setMonsterLevelForChildren( ML )
	tiny103c.setMonsterLevelForChildren( ML )
	tiny103d.setMonsterLevelForChildren( ML )
	tiny103e.setMonsterLevelForChildren( ML )
	tiny103f.setMonsterLevelForChildren( ML )
	FC3a.setMonsterLevelForChildren( ML )
	FC3b.setMonsterLevelForChildren( ML )
	FC3c.setMonsterLevelForChildren( ML )
	FC2.setMonsterLevelForChildren( ML )
	tiny2.setMonsterLevelForChildren( ML )
	mask2.setMonsterLevelForChildren( ML )
	tiny101a.setMonsterLevelForChildren( ML )
	tiny101b.setMonsterLevelForChildren( ML )
	tiny101c.setMonsterLevelForChildren( ML )
	tiny101d.setMonsterLevelForChildren( ML )
	tiny101e.setMonsterLevelForChildren( ML )
	FC102.setMonsterLevelForChildren( ML )
	tiny102.setMonsterLevelForChildren( ML )
	mask102.setMonsterLevelForChildren( ML )
	tiny202.setMonsterLevelForChildren( ML )
	FC202a.setMonsterLevelForChildren( ML )
	FC202b.setMonsterLevelForChildren( ML )
	FCa203.setMonsterLevelForChildren( ML )
	FCb203.setMonsterLevelForChildren( ML )
	FCc203.setMonsterLevelForChildren( ML )
	harassSpawn.setMonsterLevelForChildren( ML )
}

def forceNewMonsterLevel() {
	//Empty the check list and then look into all rooms to find all players, whether in a Crew or not
	newOneSet = false
	playerCLCheckList = []
	myRooms.OtCliffs_1.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_2.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_3.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_101.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_102.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_103.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_104.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_202.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_203.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_204.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_303.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	myRooms.OtCliffs_304.getActorList().each { if( isPlayer( it ) ) { playerCLCheckList << it } }
	
	//Now cycle through the playerCLCheckList and figure out the new ML level, ensuring that it never exceeds 10.0
	//Look through the list. If any of them are greater than the current ML, then reset the ML and we're done
	playerCLCheckList.each{ 
		if( it.getConLevel() >= ML && it.getConLevel() <= 10.0 ) {
			ML = it.getConLevel()
			newOneSet = true
		} else if ( it.getConLevel() > 10.0 ) {
			ML = 10.0
			newOneSet = true
		}
	}
	//If no one was greater than or equal to the current ML, then we need to lower the ML for folks to keep playing...but still no lower than CL 7.5
	if( newOneSet == false ) {
		ML = 7.5
		playerCLCheckList.each{
			if( it.getConLevel() > ML ) {
				ML = it.getConLevel()
			}
		}
	}
	//Make the return trip a bit easier ML, but increase the spawns and crowd control stuff
	if( gauntletStarted == true ) {
		ML = ML - 0.2 
	}
	
	//Now update all the spawners with the new ML so they start creating monsters at that new level
	mask103a.setMonsterLevelForEveryone( ML )
	mask103b.setMonsterLevelForEveryone( ML )
	mask103c.setMonsterLevelForEveryone( ML )
	tiny103a.setMonsterLevelForEveryone( ML )
	tiny103b.setMonsterLevelForEveryone( ML )
	tiny103c.setMonsterLevelForEveryone( ML )
	tiny103d.setMonsterLevelForEveryone( ML )
	tiny103e.setMonsterLevelForEveryone( ML )
	tiny103f.setMonsterLevelForEveryone( ML )
	FC3a.setMonsterLevelForEveryone( ML )
	FC3b.setMonsterLevelForEveryone( ML )
	FC3c.setMonsterLevelForEveryone( ML )
	FC2.setMonsterLevelForEveryone( ML )
	tiny2.setMonsterLevelForEveryone( ML )
	mask2.setMonsterLevelForEveryone( ML )
	tiny101a.setMonsterLevelForEveryone( ML )
	tiny101b.setMonsterLevelForEveryone( ML )
	tiny101c.setMonsterLevelForEveryone( ML )
	tiny101d.setMonsterLevelForEveryone( ML )
	tiny101e.setMonsterLevelForEveryone( ML )
	FC102.setMonsterLevelForEveryone( ML )
	tiny102.setMonsterLevelForEveryone( ML )
	mask102.setMonsterLevelForEveryone( ML )
	tiny202.setMonsterLevelForEveryone( ML )
	FC202a.setMonsterLevelForEveryone( ML )
	FC202b.setMonsterLevelForEveryone( ML )
	FCa203.setMonsterLevelForEveryone( ML )
	FCb203.setMonsterLevelForEveryone( ML )
	FCc203.setMonsterLevelForEveryone( ML )
}

//Since the PlayerList is used throughout the scenario,
//it has to get weeded out occasionally so that if a player zones out (null chamber) or goes offline,
//then it causes invalid spawns or addHates
def weedOutPlayerList() {
	gauntletZoneCheckList = []
	myRooms.OtCliffs_1.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	myRooms.OtCliffs_2.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	myRooms.OtCliffs_3.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	myRooms.OtCliffs_101.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	myRooms.OtCliffs_102.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	myRooms.OtCliffs_103.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	myRooms.OtCliffs_104.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	myRooms.OtCliffs_202.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	myRooms.OtCliffs_203.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	myRooms.OtCliffs_204.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	myRooms.OtCliffs_303.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	myRooms.OtCliffs_304.getActorList().each { if( isPlayer( it ) ) { gauntletZoneCheckList << it } }
	
	//pare the playerList down to the players that are actually still in the Gauntlet area
	playerList = gauntletZoneCheckList.intersect( playerList )
	
	//now go through that pared-down list and remove all the offline players
	playerList.clone().each { 
		if( !isOnline( it ) ) {
			playerList.remove( it )
		}
	}
}

//------------------------------------------
// ROOM 103                                 
// (Last-ditch attempt to stop the players) 
//------------------------------------------

mask103a = myRooms.OtCliffs_103.spawnSpawner( "mask103a", "mask_gauntlet", 1)
mask103a.setPos( 920, 510 )
mask103a.setWaitTime( 1, 2 )
mask103a.setGuardPostForChildren( "OtCliffs_103", 920, 510, 225 )
mask103a.setMonsterLevelForChildren( ML )

mask103b = myRooms.OtCliffs_103.spawnSpawner( "mask103b", "mask_gauntlet", 1)
mask103b.setPos( 930, 395 )
mask103b.setWaitTime( 1, 2 )
mask103b.setGuardPostForChildren( "OtCliffs_103", 930, 395, 225 )
mask103b.setMonsterLevelForChildren( ML )

mask103c = myRooms.OtCliffs_103.spawnSpawner( "mask103c", "mask_gauntlet", 1)
mask103c.setPos( 730, 500 )
mask103c.setWaitTime( 1, 2 )
mask103c.setGuardPostForChildren( "OtCliffs_103", 730, 500, 225 )
mask103c.setMonsterLevelForChildren( ML )

tiny103a = myRooms.OtCliffs_103.spawnSpawner( "tiny103a", "feathered_coatl", 1)
tiny103a.setPos( 575, 405 )
tiny103a.setWaitTime( 1, 2 )
tiny103a.setGuardPostForChildren( "OtCliffs_103", 575, 405, 315 )
tiny103a.setMonsterLevelForChildren( ML )

tiny103b = myRooms.OtCliffs_103.spawnSpawner( "tiny103b", "feathered_coatl", 1)
tiny103b.setPos( 870, 250 )
tiny103b.setWaitTime( 1, 2 )
tiny103b.setGuardPostForChildren( "OtCliffs_103", 870, 250, 225 )
tiny103b.setMonsterLevelForChildren( ML )

tiny103c = myRooms.OtCliffs_103.spawnSpawner( "tiny103c", "feathered_coatl", 1)
tiny103c.setPos( 450, 320 )
tiny103c.setWaitTime( 1, 2 )
tiny103c.setGuardPostForChildren( "OtCliffs_103", 450, 320, 315 )
tiny103c.setMonsterLevelForChildren( ML )

tiny103d = myRooms.OtCliffs_103.spawnSpawner( "tiny103d", "feathered_coatl", 1)
tiny103d.setPos( 380, 65 )
tiny103d.setWaitTime( 1, 2 )
tiny103d.setGuardPostForChildren( "OtCliffs_103", 380, 65, 45 )
tiny103d.setMonsterLevelForChildren( ML )

tiny103e = myRooms.OtCliffs_103.spawnSpawner( "tiny103e", "tiny_terror_LT", 1)
tiny103e.setPos( 860, 110 )
tiny103e.setWaitTime( 1, 2 )
tiny103e.setGuardPostForChildren( "OtCliffs_103", 860, 110, 225 )
tiny103e.setMonsterLevelForChildren( ML )

tiny103f = myRooms.OtCliffs_103.spawnSpawner( "tiny103f", "tiny_terror_LT", 1)
tiny103f.setPos( 320, 220 )
tiny103f.setWaitTime( 1, 2 )
tiny103f.setGuardPostForChildren( "OtCliffs_103", 320, 220, 315 )
tiny103f.setMonsterLevelForChildren( ML )

playerList103 = []
spawned103 = false

myManager.onEnter( myRooms.OtCliffs_103 ) { event ->
	if( isPlayer( event.actor ) ) {
		weedOutPlayerList()
		playerList103 << event.actor
		if( gauntletStarted == true && spawned103 == false ) {
			spawned103 = true
			spawnAmbush103()
		}
	}
}

myManager.onExit( myRooms.OtCliffs_103 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList103.remove( event.actor )
	}
}	

//use each of the spawners depending on the size of the number of players in the zone
def spawnAmbush103() {
	calculateNewMonsterLevel()
	if( playerList.size() == 1 ) {
		mask103a.forceSpawnNow()
		tiny103a.forceSpawnNow()
	}
	if( playerList.size() > 1 ) {
		tiny103b.forceSpawnNow()
	}
	if( playerList.size() > 2 ) {
		mask103b.forceSpawnNow()
		tiny103c.forceSpawnNow()
	}
	if( playerList.size() > 3 ) {
		tiny103d.forceSpawnNow()
	}
	if( playerList.size() > 4 ) {
		mask103c.forceSpawnNow()
		tiny103e.forceSpawnNow()
	}
	if( playerList.size() > 5 ) {
		tiny103f.forceSpawnNow()
	}		
}

mask103a.stopSpawning()
mask103b.stopSpawning()
mask103c.stopSpawning()
tiny103a.stopSpawning()
tiny103b.stopSpawning()
tiny103c.stopSpawning()
tiny103d.stopSpawning()
tiny103e.stopSpawning()
tiny103f.stopSpawning()

//------------------------------------------
// ROOM 3                                   
// (More tree droppers!)                    
//------------------------------------------

FC3a = myRooms.OtCliffs_3.spawnSpawner( "FC3a", "feathered_coatl", 12)
FC3a.setPos( 440, 160 )
FC3a.setWaitTime( 1, 2 )
FC3a.setWanderBehaviorForChildren( 50, 150, 2, 6, 350)
FC3a.setMonsterLevelForChildren( ML )

FC3b = myRooms.OtCliffs_3.spawnSpawner( "FC3b", "feathered_coatl", 12)
FC3b.setPos( 330, 130 )
FC3b.setWaitTime( 1, 2 )
FC3b.setWanderBehaviorForChildren( 50, 200, 2, 5, 250)
FC3b.setMonsterLevelForChildren( ML )

FC3c = myRooms.OtCliffs_3.spawnSpawner( "FC3c", "feathered_coatl", 12)
FC3c.setPos( 185, 335 )
FC3c.setWaitTime( 1, 2 )
FC3c.setWanderBehaviorForChildren( 100, 200, 3, 7, 300)
FC3c.setMonsterLevelForChildren( ML )

playerList3 = []
spawned3 = false

myManager.onEnter( myRooms.OtCliffs_3 ) { event ->
	if( isPlayer( event.actor ) ) {
		weedOutPlayerList()
		playerList3 << event.actor
		if( gauntletStarted == true && spawned3 == false ) {
			spawned3 = true
			calculateNewMonsterLevel()
			numWaves3 = ( playerList.size / 2 ).intValue() + ( playerList.size % 2 ).intValue()
			spawnAmbush3()
		}
	}
}

myManager.onExit( myRooms.OtCliffs_3 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList3.remove( event.actor )
	}
}

def spawnAmbush3() {
	if( numWaves3 > 0 ) {
		weedOutPlayerList()
		current = FC3a.forceSpawnNow(); if( !playerList3.isEmpty() ) { current.addHate( random( playerList3 ), 1 ) } else { current.addHate( random( playerList ), 1 ) }
		current = FC3b.forceSpawnNow(); if( !playerList3.isEmpty() ) { current.addHate( random( playerList3 ), 1 ) } else { current.addHate( random( playerList ), 1 ) }
		current = FC3c.forceSpawnNow(); if( !playerList3.isEmpty() ) { current.addHate( random( playerList3 ), 1 ) } else { current.addHate( random( playerList ), 1 ) }
		numWaves3 --
		myManager.schedule(3) { spawnAmbush3() }
	}
}

FC3a.forceSpawnNow()
FC3b.forceSpawnNow()
FC3c.forceSpawnNow()

FC3a.stopSpawning()
FC3b.stopSpawning()
FC3c.stopSpawning()


//------------------------------------------
// ROOM 2                                   
//------------------------------------------

FC2 = myRooms.OtCliffs_2.spawnSpawner( "FC2", "feathered_coatl", 6)
FC2.setPos( 445, 40 )
FC2.setWaitTime( 1, 2 )
FC2.setWanderBehaviorForChildren( 50, 150, 2, 6, 350)
FC2.setMonsterLevelForChildren( ML )

tiny2 = myRooms.OtCliffs_2.spawnSpawner( "tiny2", "tiny_terror_LT", 6)
tiny2.setPos( 680, 200 )
tiny2.setWaitTime( 1, 2 )
tiny2.setWanderBehaviorForChildren( 50, 200, 2, 5, 250)
tiny2.setMonsterLevelForChildren( ML )

mask2 = myRooms.OtCliffs_2.spawnSpawner( "mask2", "mask_gauntlet", 1)
mask2.setPos( 445, 340 )
mask2.setWaitTime( 1, 2 )
mask2.setWanderBehaviorForChildren( 100, 200, 3, 7, 300)
mask2.setMonsterLevelForChildren( ML )

playerList2 = []
spawned2 = false

myManager.onEnter( myRooms.OtCliffs_2 ) { event ->
	if( isPlayer( event.actor ) ) {
		weedOutPlayerList()
		playerList2 << event.actor
		if( gauntletStarted == true && spawned2 == false ) {
			spawned2 = true
			calculateNewMonsterLevel()
			numSpawn2 = playerList.size()
			if( playerList.size() > 3 ) {
				current = mask2.forceSpawnNow()
				if( !playerList2.isEmpty() ) { current.addHate( random( playerList2 ), 1 ) } else { current.addHate( random( playerList ), 1 ) }
			}
			spawnAmbush2()
		}
	}
}

myManager.onExit( myRooms.OtCliffs_2 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList2.remove( event.actor )
	}
}	

def spawnAmbush2() {
	if( numSpawn2 > 0 ) {
		weedOutPlayerList()
		roll = random( 100 )
		if( roll <= 50 ) {
			current = tiny2.forceSpawnNow(); if( !playerList2.isEmpty() ) { current.addHate( random( playerList2 ), 1 ) } else { current.addHate( random( playerList ), 1 ) }
		} else {
			current = FC2.forceSpawnNow(); if( !playerList2.isEmpty() ) { current.addHate( random( playerList2 ), 1 ) } else { current.addHate( random( playerList ), 1 ) }
		}
		numSpawn2 --
		myManager.schedule(1) { spawnAmbush2() }
	}
}

FC2.forceSpawnNow()

FC2.stopSpawning()
tiny2.stopSpawning()
mask2.stopSpawning()

//------------------------------------------
// ROOM 101                                 
//------------------------------------------

tiny101a = myRooms.OtCliffs_101.spawnSpawner( "tiny101a", "tiny_terror_LT", 1)
tiny101a.setPos( 735, 75 )
tiny101a.setWaitTime( 1, 2 )
tiny101a.setGuardPostForChildren( "OtCliffs_101", 735, 75, 135 )
tiny101a.setMonsterLevelForChildren( ML )

tiny101b = myRooms.OtCliffs_101.spawnSpawner( "tiny101b", "tiny_terror_LT", 1)
tiny101b.setPos( 950, 205 )
tiny101b.setWaitTime( 1, 2 )
tiny101b.setGuardPostForChildren( "OtCliffs_101", 950, 205, 135 )
tiny101b.setMonsterLevelForChildren( ML )

tiny101c = myRooms.OtCliffs_101.spawnSpawner( "tiny101c", "tiny_terror_LT", 1)
tiny101c.setPos( 390, 165 )
tiny101c.setWaitTime( 1, 2 )
tiny101c.setGuardPostForChildren( "OtCliffs_101", 390, 165, 315 )
tiny101c.setMonsterLevelForChildren( ML )

tiny101d = myRooms.OtCliffs_101.spawnSpawner( "tiny101d", "tiny_terror_LT", 1)
tiny101d.setPos( 630, 290 )
tiny101d.setWaitTime( 1, 2 )
tiny101d.setGuardPostForChildren( "OtCliffs_101", 630, 290, 315 )
tiny101d.setMonsterLevelForChildren( ML )

tiny101e = myRooms.OtCliffs_101.spawnSpawner( "tiny101e", "tiny_terror_LT", 1)
tiny101e.setPos( 835, 460 )
tiny101e.setWaitTime( 1, 2 )
tiny101e.setGuardPostForChildren( "OtCliffs_101", 835, 460, 315 )
tiny101e.setMonsterLevelForChildren( ML )

def spawnTT101Ambush() {
	weedOutPlayerList()
	calculateNewMonsterLevel()
	if( playerList.size() == 1 ) {
		tiny101a.forceSpawnNow()
		tiny101b.forceSpawnNow()
	}
	if( playerList.size() > 2 ) {
		tiny101c.forceSpawnNow()
	}
	if( playerList.size() > 3 ) {
		tiny101d.forceSpawnNow()
	}
	if( playerList.size() > 4 ) {
		tiny101e.forceSpawnNow()
	}
}

myManager.onEnter( myRooms.OtCliffs_101 ) { event ->
	if( isPlayer( event.actor ) ) {
		weedOutPlayerList()
	}
}

tiny101a.stopSpawning()
tiny101b.stopSpawning()
tiny101c.stopSpawning()
tiny101d.stopSpawning()
tiny101e.stopSpawning()

//------------------------------------------
// ROOM 102                                 
//------------------------------------------

FC102 = myRooms.OtCliffs_102.spawnSpawner( "FC102", "feathered_coatl", 6)
FC102.setPos( 900, 500 )
FC102.setWaitTime( 1, 2 )
FC102.setWanderBehaviorForChildren( 50, 150, 2, 6, 350)
FC102.setMonsterLevelForChildren( ML )

tiny102 = myRooms.OtCliffs_102.spawnSpawner( "tiny102", "tiny_terror", 6)
tiny102.setPos( 45, 250 )
tiny102.setWaitTime( 1, 2 )
tiny102.setWanderBehaviorForChildren( 50, 200, 2, 5, 250)
tiny102.setMonsterLevelForChildren( ML )

mask102 = myRooms.OtCliffs_102.spawnSpawner( "mask102", "mask_gauntlet", 1)
mask102.setPos( 210, 600 )
mask102.setWaitTime( 1, 2 )
mask102.setWanderBehaviorForChildren( 100, 200, 3, 7, 300)
mask102.setMonsterLevelForChildren( ML )

playerList102 = []
spawned102 = false

myManager.onEnter( myRooms.OtCliffs_102 ) { event ->
	if( isPlayer( event.actor ) ) {
		weedOutPlayerList()
		playerList102 << event.actor
		if( gauntletStarted == true && spawned102 == false ) {
			spawned102 = true
			calculateNewMonsterLevel()
			numSpawn102 = playerList.size() * 2
			if( playerList.size() > 3 ) {
				current = mask102.forceSpawnNow()
				if( !playerList102.isEmpty() ) { current.addHate( random( playerList102 ), 1 ) } else { current.addHate( random( playerList ), 1 ) }
			}
			spawnAmbush102()
		}
	}
}

myManager.onExit( myRooms.OtCliffs_102 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList102.remove( event.actor )
	}
}	

def spawnAmbush102() {
	if( numSpawn102 > 0 ) {
		weedOutPlayerList()
		roll = random( 100 )
		if( roll <= 50 ) {
			current = tiny102.forceSpawnNow(); if( !playerList102.isEmpty() ) { current.addHate( random( playerList102 ), 1 ) } else { current.addHate( random( playerList ), 1 ) }
		} else {
			current = FC102.forceSpawnNow(); if( !playerList102.isEmpty() ) { current.addHate( random( playerList102 ), 1 ) } else { current.addHate( random( playerList ), 1 ) }
		}
		numSpawn102 --
		myManager.schedule(1) { spawnAmbush102() }
	}
}

FC102.forceSpawnNow()

FC102.stopSpawning()
tiny102.stopSpawning()
mask102.stopSpawning()

//------------------------------------------
// ROOM 202                                 
// FCs from the chasm, and a Tiny Witch Doc 
//------------------------------------------

tiny202 = myRooms.OtCliffs_202.spawnSpawner( "tiny202", "tiny_terror_LT", 1)
tiny202.setPos( 570, 180 )
tiny202.setWaitTime( 1, 2 )
tiny202.setWanderBehaviorForChildren( 50, 150, 2, 6, 350)
tiny202.setMonsterLevelForChildren( ML )

FC202a = myRooms.OtCliffs_202.spawnSpawner( "FC202a", "feathered_coatl", 6)
FC202a.setPos( 70, 180 )
FC202a.setWaitTime( 1, 2 )
FC202a.setWanderBehaviorForChildren( 50, 200, 2, 5, 250)
FC202a.setMonsterLevelForChildren( ML )

FC202b = myRooms.OtCliffs_202.spawnSpawner( "FC202b", "feathered_coatl", 6)
FC202b.setPos( 580, 630 )
FC202b.setWaitTime( 1, 2 )
FC202b.setWanderBehaviorForChildren( 100, 200, 3, 7, 300)
FC202b.setMonsterLevelForChildren( ML )

playerList202 = []
spawned202 = false

myManager.onEnter( myRooms.OtCliffs_202 ) { event ->
	if( isPlayer( event.actor ) ) {
		weedOutPlayerList()
		playerList202 << event.actor
		if( gauntletStarted == true && spawned202 == false ) {
			spawned202 = true
			calculateNewMonsterLevel()
			tt202 = tiny202.forceSpawnNow()
			if( !playerList202.isEmpty() ) { tt202.addHate( random( playerList202 ), 1 ) } else { current.addHate( random( playerList ), 1 ) }
			numSpawn202 = playerList.size()
			spawnAmbush202()
		}
	}
}

myManager.onExit( myRooms.OtCliffs_202 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList202.remove( event.actor )
	}
}	

ambush202List = [ FC202a, FC202b ]

def spawnAmbush202() {
	if( numSpawn202 > 0 ) {
		weedOutPlayerList()
		current = random( ambush202List ).forceSpawnNow()
		if( !playerList202.isEmpty() ) {
			current.addHate( random( playerList202 ), 1 )
		} else {
			current.addHate( random( playerList ), 1 )
		}
		numSpawn202 --
		myManager.schedule(1) { spawnAmbush202() }
	}
}

FC202a.forceSpawnNow()
FC202b.forceSpawnNow()

tiny202.stopSpawning()
FC202a.stopSpawning()
FC202b.stopSpawning()

//------------------------------------------
// ROOM 203                                 
// Feathered Coatls out of the trees!       
//------------------------------------------

FCa203 = myRooms.OtCliffs_203.spawnSpawner( "FCa203", "feathered_coatl", 12)
FCa203.setPos( 800, 490 )
FCa203.setWaitTime( 1, 2 )
FCa203.setWanderBehaviorForChildren( 50, 150, 2, 6, 350)
FCa203.setMonsterLevelForChildren( ML )

FCb203 = myRooms.OtCliffs_203.spawnSpawner( "FCb203", "feathered_coatl", 12)
FCb203.setPos( 220, 75 )
FCb203.setWaitTime( 1, 2 )
FCb203.setWanderBehaviorForChildren( 50, 200, 1, 4, 250)
FCb203.setMonsterLevelForChildren( ML )

FCc203 = myRooms.OtCliffs_203.spawnSpawner( "FCc203", "feathered_coatl", 12)
FCc203.setPos( 60, 630 )
FCc203.setWaitTime( 1, 2 )
FCc203.setWanderBehaviorForChildren( 100, 200, 3, 7, 300)
FCc203.setMonsterLevelForChildren( ML )

playerList203 = []
spawned203 = false

myManager.onEnter( myRooms.OtCliffs_203 ) { event ->
	if( isPlayer( event.actor ) ) {
		weedOutPlayerList()
		playerList203 << event.actor
		if( gauntletStarted == true && spawned203 == false ) {
			calculateNewMonsterLevel()
			spawned203 = true
			numSpawn203 = playerList.size()
			spawnAmbush203()
		}
	}
}

myManager.onExit( myRooms.OtCliffs_203 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList203.remove( event.actor )
	}
}	

ambush203List = [ FCa203, FCb203, FCc203 ]

def spawnAmbush203() {
	if( numSpawn203 > 0 ) {
		weedOutPlayerList()
		current = random( ambush203List ).forceSpawnNow()
		if( !playerList203.isEmpty() ) {
			current.addHate( random( playerList203 ), 1 )
		} else {
			current.addHate( random( playerList ), 1 )
		}
		numSpawn203 --
		myManager.schedule(1) { spawnAmbush203() }
	}
}

FCa203.forceSpawnNow()
FCb203.forceSpawnNow()
FCc203.forceSpawnNow()

FCa203.stopSpawning()
FCb203.stopSpawning()
FCc203.stopSpawning()

//------------------------------------------
// The Three Celestial Spirits              
// (Thunder, Storm, and Lightning)          
//------------------------------------------

thunder = myRooms.OtCliffs_304.spawnSpawner( "thunder", "bladed_statue_gauntlet", 1)
thunder.setPos( 80, 370 )
thunder.setHomeTetherForChildren( 6000 )
thunder.setHateRadiusForChildren( 6000 )
thunder.setMonsterLevelForChildren( ML + 2.0 )

thunder.stopSpawning()

lightning = myRooms.OtCliffs_304.spawnSpawner( "lightning", "bladed_statue_gauntlet", 1)
lightning.setPos( 210, 440 )
lightning.setHomeTetherForChildren( 6000 )
lightning.setHateRadiusForChildren( 6000 )
lightning.setMonsterLevelForChildren( ML + 2.0 )

lightning.stopSpawning()

storm = myRooms.OtCliffs_304.spawnSpawner( "storm", "bladed_statue_gauntlet", 1)
storm.setPos( 290, 335 )
storm.setHomeTetherForChildren( 6000 )
storm.setHateRadiusForChildren( 6000 )
storm.setMonsterLevelForChildren( ML + 2.0 )

storm.stopSpawning()

//GAME THEORY:
// - Feathered Coatls are just nuisances on the way in...but are additional trouble if the players don't clean them out on the way to the idol.
// - Once the player touches the idol, all the team members that aren't there already are teleported to the idol (nobody gets to hang out at the exit...everyone plays!)
// - Once everyone is warped to the idol, start the gauntlet.
// - (SIDE NOTE: There should be no "zone" to the Shallow Sea. It should be a trigger zone that looks for this global flag instead.)
// - Players fight their way back to the temple at the start of the mission instance.
// - Once they get to that temple, they are granted the reward flag if they exit through a trigger zone at that location. (They don't get the flag if they escape to null chamber.)

//--------------------------------------------
// TOUCHING THE STATUE AND GATHERING THE TEAM 
//--------------------------------------------

playerList = []
playerList304 = []
legitList = []
exitList = []
gauntletStarted = false
firstPlayerIntoGauntlet = false

//create a list of all players that have entered the mission instance
myManager.onEnter(myRooms.OtCliffs_204) { event ->
	if( isPlayer(event.actor) && gauntletStarted == false ) { //stop adding players after the Gauntlet has started (exploit prevention)
		if( !playerList.contains( event.actor ) ) {
			playerList << event.actor
			if( firstPlayerIntoGauntlet == false ) {
				firstPlayerIntoGauntlet = true
				//even though calculateMonsterLevel() sets the levels, this is the first player into the scenario, so we need to set via his crew levels
				event.actor.getCrew().each() {
					if( it.getConLevel() >= ML && it.getConLevel() < 10.0 ) { //Scenario is a minimum of CL 7.5 and a max of CL 10.0
						ML = it.getConLevel()
					} else if( it.getConLevel() > 10.0 ) {
						ML = 10.0
					}
				}
				//force all existing monsters to adapt to the player CLs
				forceNewMonsterLevel()
			}
		}

	}
}

//LIST OF PLAYERS THAT CAME TO OtCliffs_304 "legitimately" (in other words, they didn't just dip down from 204 to get fake credit)
myManager.onEnter(myRooms.OtCliffs_303) { event ->
	if( isPlayer(event.actor) && !legitList.contains( event.actor ) ) {
		legitList << event.actor
	}
}

//LIST OF PLAYERS IN THE CHALOC STATUE ROOM
myManager.onEnter(myRooms.OtCliffs_304) { event ->
	if( isPlayer(event.actor) ) {
		playerList304 << event.actor
	}
}

myManager.onExit(myRooms.OtCliffs_304) { event ->
	if( isPlayer(event.actor) ) {
		playerList304.remove( event.actor )
	}
}

statue = makeSwitch( "chalocStatue", myRooms.OtCliffs_304, 190, 330 )

statue.unlock()

// This warps the players to the same location when the statue is touched
def gatherTeam = { event ->
	//statue locked until the mission is reset
	statue.lock() 
	event.actor.say( "Ummmm...that statue...it moved!" )
	//warp anyone not already in 304 into 304 so the gauntlet can begin
	weedOutPlayerList()
	playerList.clone().each {
		if( !legitList.contains( it ) || !playerList304.contains( it ) ) {
			it.centerPrint( "A power beyond your understanding moves you through space..." )
			it.warp( "OtCliffs_304", 130, 285 )
		}
	}
	//remember the list of players that were in the instance when the idol was touched!
	exitList = playerList304.clone()
	myManager.schedule(1) { playerList.each{ controlPlayer( it ) } }
	weedOutPlayerList()
	myManager.schedule(2) { playerList.clone().each { it.centerPrint( "Tremble before Chaloc, Master of the Rain and Wind!" ) } } 
	weedOutPlayerList()
	myManager.schedule(5) { playerList.clone().each { it.centerPrint( "Flee before my coming Storms, and fulfill that which is fated!" ) } }
	weedOutPlayerList()
	myManager.schedule(8) { playerList.clone().each { it.centerPrint( "Your gauntlet begins..." ) } }
	weedOutPlayerList()
	myManager.schedule(9) { playerList.clone().each { it.centerPrint( "...NOW!" ) }; playerList.each{ freePlayer( it ) } }
	myManager.schedule(11) { startGauntlet(); gauntletStarted = true }
}

statue.whenOn( gatherTeam )

//------------------------------------------
// THE GAUNTLET                             
//------------------------------------------

// The "_gauntlet" monsters drop ZERO loot or gold to prevent this zone from being a gigantic farm.
// The "theStorms" are designed to be basically invulnerable, moving after the players in a steady, unstoppable form.

def startGauntlet() {
	spawnGauntlet()
	
	//set the ML
	calculateNewMonsterLevel()
	
	chaser1 = thunder.forceSpawnNow()
	chaser2 = storm.forceSpawnNow()
	chaser3 = lightning.forceSpawnNow()
	chaser1.setDisplayName( "Thunder" )
	chaser2.setDisplayName( "Storm" )
	chaser3.setDisplayName( "Lightning" )
	
	//make the STORMS hate three different players, if there are three players to hate.
	
	//Make Thunder hate someone
	weedOutPlayerList()
	hateListHolder = playerList.clone()
	hated1 = random( hateListHolder )
	hateListHolder.remove( hated1 )
	chaser1.addHate( hated1, 1 )
	
	//Make Storm hate someone else
	if( !hateListHolder.isEmpty() ) {
		hated2 = random( hateListHolder )
		hateListHolder.remove( hated2 )
		chaser2.addHate( hated2, 1 )
	} else {
		chaser2.addHate( hated1, 1 )
	}
	
	//Make Lightning hate yet another person
	if( !hateListHolder.isEmpty() ) {
		hated3 = random( hateListHolder )
		chaser3.addHate( hated3, 1 )
	} else {
		chaser3.addHate( hated1 , 1 )
	}
	
	checkForEmptyHate()
	
	chaser1.say( "Fear the booming Thunder!" )
	myManager.schedule(1) { if( !chaser2.isDead() ) { chaser2.say( "Fall before relentless Storm!" ) } }
	myManager.schedule(2) { if( !chaser3.isDead() ) { chaser3.say( "Feel the crash of Lightning!" ) } }
	myManager.schedule(3) { if( !chaser1.isDead() ) { chaser1.say( "OUR TOUCH IS DOOM!" ) }; if( !chaser2.isDead() ) { chaser2.say( "OUR TOUCH IS DOOM!" ) }; if( !chaser3.isDead() ) { chaser3.say( "OUR TOUCH IS DOOM!" ) } }
	myManager.schedule(8) { chaserChant() }
	
}

def checkForEmptyHate() {
	if( !chaser1.isDead() && chaser1.getHated().isEmpty() && !playerList.isEmpty() ) {
		chaser1.addHate( random( playerList ), 100 )
	}
	if( !chaser2.isDead() && chaser2.getHated().isEmpty() && !playerList.isEmpty() ) {
		chaser2.addHate( random( playerList ), 100 )
	}
	if( !chaser3.isDead() && chaser3.getHated().isEmpty() && !playerList.isEmpty() ) {
		chaser3.addHate( random( playerList ), 100 )
	}
	myManager.schedule(5) { checkForEmptyHate() }
}


def chaserChant() {
	chaser1Sayings = [ "OUR TOUCH IS DOOM!", "I am for you, ${chaser1.getMostHated()}!", "Fear the Thunder!" ]
	chaser2Sayings = [ "OUR TOUCH IS DOOM!", "I am for you, ${chaser2.getMostHated()}!", "The Storm smothers all!" ]
	chaser3Sayings = [ "OUR TOUCH IS DOOM!", "I am for you, ${chaser3.getMostHated()}!", "The Lightning strikes!" ]
	if( !chaser1.isDead() ) {
		if( !chaser1.getHated().isEmpty() ) {
			chaser1.say( random( chaser1Sayings ) )
		}
	}
	myManager.schedule(1) { 
		if( !chaser2.isDead() ) {
			if( !chaser2.getHated().isEmpty() ) {
				chaser2.say( random( chaser2Sayings ) )
			}
		}
	}
	myManager.schedule(2) {
		if( !chaser3.isDead() ) {
			if( !chaser3.getHated().isEmpty() ) {
				chaser3.say( random( chaser3Sayings ) )
			}
		}
	}
	myManager.schedule(8) { chaserChant() }
}


def spawnGauntlet() {
	
	calculateNewMonsterLevel()

	//First eliminate all current spawns from OtCliffs
	myRooms.OtCliffs_204.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	myRooms.OtCliffs_104.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	myRooms.OtCliffs_103.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	myRooms.OtCliffs_3.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	myRooms.OtCliffs_2.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	myRooms.OtCliffs_1.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	myRooms.OtCliffs_101.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	myRooms.OtCliffs_102.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	myRooms.OtCliffs_202.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	myRooms.OtCliffs_203.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	myRooms.OtCliffs_303.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	myRooms.OtCliffs_304.getActorList().each { 
		if( isMonster( it ) ) {
			it.dispose()
		}
	}
	
	//spawn the tiny terror gauntlet in room 101 so it's ready for the players when they arrive.
	calculateNewMonsterLevel()
	spawnTT101Ambush() 
	
	//spawn the Harasser mobs
	startHarassers()
}

//=========================================================
// HARASSER MOBS                                           
//=========================================================
harassSpawn = myRooms.OtCliffs_103.spawnSpawner( "harassSpawn", "tiny_terror_LT", 10)
harassSpawn.setPos( 680, 300 )
harassSpawn.setHomeTetherForChildren( 6000 )
harassSpawn.setHateRadiusForChildren( 6000 )
harassSpawn.setMonsterLevelForChildren( ML )

harassSpawn.stopSpawning()

harasserHateListHolder = []

def startHarassers() {
	harasserHateListHolder = playerList.clone()
	numHarassers = playerList.size()
	spawnHarassers()
	myManager.schedule(180) { startHarassers() }
}
	

def spawnHarassers() {
	println "**** harasserHateListHolder = ${harasserHateListHolder} ****"
	println "**** numHarassers = ${numHarassers} ****"
	if( numHarassers > 0 && harassSpawn.spawnsInUse() < 6 ) {
		calculateNewMonsterLevel()
		weedOutPlayerList()
		println "**** the list after weed out = ${harasserHateListHolder} ****"
		tinyChaser1 = harassSpawn.forceSpawnNow()
		hhated1 = random( harasserHateListHolder )
		harasserHateListHolder.remove( hhated1 )
		tinyChaser1.addHate( hhated1, 140 )
		tinyChaser1.setDisplayName( "Tiny Seeker" )
		println "**** chaser1 hates ${ hhated1 } ****"
		numHarassers --
		myManager.schedule(1) { spawnHarassers() }
	}
}
	

//------------------------------------------
// EXIT SWITCH LOGIC                        
//------------------------------------------

onClickExitGauntlet = new Object()

gauntletExit = makeSwitch( "Gauntlet_Out", myRooms.OtCliffs_204, 570, 200 )
gauntletExit.unlock()
gauntletExit.off()
gauntletExit.setRange( 250 )

def exitGauntlet = { event ->
	synchronized( onClickExitGauntlet ) {
		gauntletExit.off()
		if( isPlayer( event.actor ) && exitList.contains( event.actor ) ) { //if a player AND the player was present when the statue was touched THEN
			if( event.actor.isOnQuest( 103, 2 ) || event.actor.isOnQuest( 106, 2 ) ) {
				event.actor.centerPrint( "You feel the blessing of Chaloc permeate your being." )
				if( event.actor.isOnQuest( 103, 2 ) ) {
					event.actor.updateQuest( 103, "Otomi Rin-VQS" ) //push the completion of step 2 so the quest can progress when players return to Lin and Rin.
				}
				if( event.actor.isOnQuest( 106, 2 ) ) {
					event.actor.updateQuest( 106, "Otomi Rin-VQS" ) //push the completion of step 2 and trigger the player to go to the Throne Room (only when replaying the Gauntlet/Throne sequence)
				}
			}
		}
		event.actor.warp( "OtRuins_304", 890, 590 )
	}
}

gauntletExit.whenOn( exitGauntlet )


def exitListCleanup() {
	myManager.schedule(30) { exitListCleanup() }
	exitList.clone().each() {
		if(!otCliffsRoomList.contains(it?.getRoomName())) {
			exitList.remove(it)
		}
	}
}

exitListCleanup()