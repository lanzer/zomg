import com.gaiaonline.mmo.battle.script.*;

Lin = spawnNPC( "Otomi Lin-VQS", myRooms.OtCliffs_301, 215, 320 )
Lin.setRotation( 90 )
Lin.setDisplayName( "Lin" )

Rin = spawnNPC( "Otomi Rin-VQS", myRooms.OtCliffs_301, 680, 360 )
Rin.setRotation( 135 )
Rin.setDisplayName( "Rin" )

onQuestStep( 102, 5 ) { event -> event.player.addMiniMapQuestActorName( "Otomi Rin-VQS" ) }
onQuestStep( 103, 2 ) { event -> event.player.addMiniMapQuestActorName( "Otomi Rin-VQS" ) }
onQuestStep( 104, 2 ) { event -> event.player.addMiniMapQuestActorName( "Otomi Rin-VQS" ) }

//---------------------------------------------------------
// ENTRANCE LOGIC                                          
//---------------------------------------------------------

playerList301 = []

myManager.onEnter( myRooms.OtCliffs_301) { event ->
	if( isPlayer( event.actor ) ) {
		playerList301 << event.actor
		
		//Added script fix in case player data gets lost between end of MERIT IN THE EYES and the start of THE GAUNTLET
		if( event.actor.isDoneQuest( 102 ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z19MeritProven" )
		}
	}
}

myManager.onExit( myRooms.OtCliffs_301 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList301.remove( event.actor )
	}
}

//---------------------------------------------------------
// INTRODUCTORY CONVERSATION                               
//---------------------------------------------------------

def rinLinIntro = Rin.createConversation( "rinLinIntro", true, "QuestStarted_107:2", "!Z19ReadyToStart", "!QuestStarted_102", "!QuestCompleted_102" )
rinLinIntro.setUrgent( true )

def rlIntro1 = [id:1]
rlIntro1.npctext = "Halt! How dare you intrude upon the high grove of the Otami spirits?"
rlIntro1.playertext = "I was sent here. To speak with you, I think."
rlIntro1.result = 2
rinLinIntro.addDialog( rlIntro1, Rin )

def rlIntro2 = [id:2]
rlIntro2.npctext = "How could anyone know we are here? We speak to no one and reveal ourselves even more rarely."
rlIntro2.playertext = "More rarely than never speaking to anyone?"
rlIntro2.result = 3
rinLinIntro.addDialog( rlIntro2, Lin )

def rlIntro3 = [id:3]
rlIntro3.npctext = "...what?"
rlIntro3.playertext = "She said that you reveal yourselves even more rarely than you speak, and you never speak. Yet, here you are, and you've already spoken to me. So I'm asking...how rare do you mean, really?"
rlIntro3.result = 4
rinLinIntro.addDialog( rlIntro3, Rin )

def rlIntro4 = [id:4]
rlIntro4.npctext = "Oh, Rin...I told you we shouldn't say anything. Now what are we going to do?"
rlIntro4.result = 5
rinLinIntro.addDialog( rlIntro4, Lin )

def rlIntro5 = [id:5]
rlIntro5.npctext = "Shhhh, Lin. You know that we're the chosen ones. We can DO this."
rlIntro5.playertext = "You can do what?"
rlIntro5.result = 6
rinLinIntro.addDialog( rlIntro5, Rin ) 

def rlIntro6 = [id:6]
rlIntro6.npctext = "Lin is sensitive to the thoughts of the spirits around us. She sees flashes of things that have not yet come to pass."
rlIntro6.playertext = "She sees the future?"
rlIntro6.result = 7
rinLinIntro.addDialog( rlIntro6, Rin )

def rlIntro7 = [id:7]
rlIntro7.npctext = "It's not always the future. It's just one of the futures that *may* happen. Not necessarily what *will* happen."
rlIntro7.playertext = "That's what Blaze was talking about when she sent me looking for you!"
rlIntro7.result = 8
rinLinIntro.addDialog( rlIntro7, Lin )

def rlIntro8 = [id:8]
rlIntro8.npctext = "Yes...speaking to you is why we spoke to Blaze originally. I saw that revealing ourselves to her would eventually result in meeting you...and that is necessary if certain other things are to come to pass."
rlIntro8.playertext = "You mean...*I'm* important?"
rlIntro8.result = 9
rinLinIntro.addDialog( rlIntro8, Lin )

def rlIntro9 = [id:9]
rlIntro9.npctext = "Yes, yes...we're all important. Lin...we mustn't talk too much about what we hope for, or it shall not come to pass."
rlIntro9.result = 10
rinLinIntro.addDialog( rlIntro9, Rin )

def rlIntro10 = [id:10]
rlIntro10.npctext = "Yes, of course, brother. It's just that there's so much turmoil in the future I see. It seems unkind to hide it."
rlIntro10.result = 11
rinLinIntro.addDialog( rlIntro10, Lin )

def rlIntro11 = [id:11]
rlIntro11.npctext = "We must."
rlIntro11.playertext = "Wait a minute...don't I get a vote here?"
rlIntro11.result = 12
rinLinIntro.addDialog( rlIntro11, Rin )

def rlIntro12 = [id:12]
rlIntro12.npctext = "No. For you, there is only the two possible outcomes: your own death, deep within the world, or the salvation of all that is Gaia."
rlIntro12.playertext = "What?!?"
rlIntro12.result = 13
rinLinIntro.addDialog( rlIntro12, Rin )

def rlIntro13 = [id:13]
rlIntro13.npctext = "Our people have hidden, deep in the jungles, since the ancient days that war tore the fabric of our once great society apart. We had not emerged from that darkness until the fateful day that Lin began having her visions. Visions of you, %p."
rlIntro13.playertext = "You know my name?"
rlIntro13.result = 14
rinLinIntro.addDialog( rlIntro13, Rin )

def rlIntro14 = [id:14]
rlIntro14.npctext = "It is through you that we hope the people of Gaia will one day understand what occurs now in the world. It is either that...or you will destroy us all along with yourself."
rlIntro14.playertext = "Now, wait a minute..."
rlIntro14.result = 15
rinLinIntro.addDialog( rlIntro14, Lin )

def rlIntro15 = [id:15]
rlIntro15.npctext = "We understand that this is overwhelming to you, but you are here for a reason. You wish to find the one that Blaze calls 'Marshall'. But to do that, you must be willing to prove yourself to the spirits of the Otami. Are you so willing?"
rlIntro15.options = []
rlIntro15.options << [text: "Wow. Kinda put me on the spot there. Yeah...I guess I should find out more.", result: 17]
rlIntro15.options << [text: "No thanks. That kind of woogie-woogie future vision stuff is too New Age for me.", result: 16]
rinLinIntro.addDialog( rlIntro15, Lin )

def rlIntro16 = [id:16]
rlIntro16.npctext = "So be it. It is our hope that we may find some other way that the world may be saved. We bid you farewell."
rlIntro16.result = DONE
rinLinIntro.addDialog( rlIntro16, Rin )

def rlIntro17 = [id:17]
rlIntro17.npctext = "Good."
rlIntro17.flag = "Z19ReadyToStart"
rlIntro17.exec = { event ->
	Rin.pushDialog( event.player, "rinLinMeritStart" )
}
rinLinIntro.addDialog( rlIntro17, Rin )

//---------------------------------------------------------
// PROVE YOUR MERIT IN THE EYES OF THE SPIRITS (Start)     
//---------------------------------------------------------

def rinLinMeritStart = Rin.createConversation( "rinLinMeritStart", true, "Z19ReadyToStart", "!QuestStarted_102", "!QuestCompleted_102" )

def rlMerit1 = [id:1]
rlMerit1.npctext = "To begin, the Spirits are impressed by your prowess. They have a deep hatred of the Unliving that defile our ancient city and shrines. The mocking appearance they take of the statues and idols throughout these sacred ruins cannot be tolerated."
rlMerit1.playertext = "I assume 'Unliving' refers to what we call the 'Animated', but there are hundreds of them out there. What do you expect me to do?"
rlMerit1.result = 2
rinLinMeritStart.addDialog( rlMerit1, Rin )

def rlMerit2 = [id:2]
rlMerit2.npctext = "The day has not yet come for the Unliving to be entirely eliminated. But you must show now that you are capable of eliminating the enemies of the Spirits."
rlMerit2.playertext = "How do I do that?"
rlMerit2.result = 3
rinLinMeritStart.addDialog( rlMerit2, Lin )

def rlMerit3 = [id:3]
rlMerit3.npctext = "There is a totem that each of the Unliving carries with it. You must destroy each of the various Unliving tribes until you find one of their totems. Bring back all four totems to Rin and he can then channel the Spirits to speak with you."
rlMerit3.playertext = "But...what totems am I looking for?"
rlMerit3.result = 4
rinLinMeritStart.addDialog( rlMerit3, Lin )

def rlMerit4 = [id:4]
rlMerit4.npctext = "You must find the tail plume from the Feathered Coatl, the sharpened spear point used by the Tiny Terrors, a mace head from the Bladed Vases, and a jewel eye of the Masks of Death and Rebirth."
rlMerit4.playertext = "Whoa. That's a lot of stuff."
rlMerit4.result = 5
rinLinMeritStart.addDialog( rlMerit4, Lin )

def rlMerit5 = [id:5]
rlMerit5.npctext = "It is by such extreme acts of bravery that the Spirits are impressed."
rlMerit5.playertext = "And that's a good thing, right?"
rlMerit5.result = 7
rinLinMeritStart.addDialog( rlMerit5, Lin )

def rlMerit7 = [id:7]
rlMerit7.npctext = "Yes it is. Go now. Your challenges have begun."
rlMerit7.quest = 102 //push the start of the MERIT IN THE EYES OF THE SPIRITS quest
rlMerit7.result = DONE
rinLinMeritStart.addDialog( rlMerit7, Rin )

//---------------------------------------------------------
// PROVE YOUR MERIT IN THE EYES OF THE SPIRITS (Interim)   
//---------------------------------------------------------

def rinLinMeritInterim = Rin.createConversation( "rinLinMeritInterim", true, "QuestStarted_102", "!QuestStarted_102:6" )

def rlMeritInt1 = [id:1]
rlMeritInt1.npctext = "You are not yet done with your quest, %p. Why have you returned to us?"
rlMeritInt1.playertext = "I'm not sure what I'm supposed to do next!"
rlMeritInt1.exec = { event ->
	if( event.player.isOnQuest( 102, 2 ) ) { event.player.setQuestFlag( GLOBAL, "Z19InterimFC" ); Rin.pushDialog( event.player, "interimFC" ) }
	if( event.player.isOnQuest( 102, 3 ) ) { event.player.setQuestFlag( GLOBAL, "Z19InterimTT" ); Rin.pushDialog( event.player, "interimTT" ) }
	if( event.player.isOnQuest( 102, 4 ) ) { event.player.setQuestFlag( GLOBAL, "Z19InterimBV" ); Rin.pushDialog( event.player, "interimBV" ) }
	if( event.player.isOnQuest( 102, 5 ) ) { event.player.setQuestFlag( GLOBAL, "Z19InterimMask" ); Rin.pushDialog( event.player, "interimMask" ) }
}
rlMeritInt1.result = DONE
rinLinMeritInterim.addDialog( rlMeritInt1, Rin )

def interimFC = Lin.createConversation( "interimFC", true, "Z19InterimFC", "QuestStarted_102:2" )

def intFC1 = [id:1]
intFC1.npctext = "You are to seek out the tail plume of the Feathered Coatl. The Spirits care not where you find it...just that you gather it while on the quest."
intFC1.playertext = "Okay. I'm on it!"
intFC1.result = DONE
interimFC.addDialog( intFC1, Lin )

def interimTT = Lin.createConversation( "interimTT", true, "Z19InterimTT", "QuestStarted_102:3" )

def intTT1 = [id:1]
intTT1.npctext = "Destroy the Unliving known as the Tiny Terrors until you are able to gain one of their sharpened spear points as their totem."
intTT1.playertext = "Thanks. I'll make it happen!"
intTT1.result = DONE
interimTT.addDialog( intTT1, Lin )

def interimBV = Lin.createConversation( "interimBV", true, "Z19InterimBV", "QuestStarted_102:4" )

def intBV1 = [id:1]
intBV1.npctext = "The Bladed Vases carry a symbol of their warlike spirit within them. Find one of the mystic Mace Heads they carry and bring it to us as their totem."
intBV1.playertext = "All right. That's clear. Thanks!"
intBV1.result = DONE
interimBV.addDialog( intBV1, Lin )

def interimMask = Lin.createConversation( "interimMask", true, "Z19InterimMask", "QuestStarted_102:5" )

def intMask1 = [id:1]
intMask1.npctext = "The most coveted of the Unliving totems is the jewel eye of the Mask of Death and Rebirth. Retrieve one such stone and bring it to us with the others you have gathered."
intMask1.playertext = "Got it, Lin. Thank you."
intMask1.result = DONE
interimMask.addDialog( intMask1, Lin )

//---------------------------------------------------------
// PROVE YOUR MERIT IN THE EYES OF THE SPIRITS (Success)   
//---------------------------------------------------------

def rinLinMeritSuccess = Rin.createConversation( "rinLinMeritSuccess", true, "!Z19AllMeritItemsRemoved", "!Z19PreviouslyRefused", "QuestStarted_102:6", "!QuestCompleted_102" )
rinLinMeritSuccess.setUrgent( true )

def rlMeritSucc1 = [id:1]
rlMeritSucc1.npctext = "You have done well, %p. You have gathered the totems of our mutual enemies and this will please the Spirits greatly. Now pass those totems to me and I will use them to speak to the Spirits."
rlMeritSucc1.options = []
rlMeritSucc1.options << [text: "Sure. Here they are.", result: 4]
rlMeritSucc1.options << [text: "No way! I gathered these things through a lot of effort. I passed your tests, but these 'totems' are mine!", result: 2]
rinLinMeritSuccess.addDialog( rlMeritSucc1, Rin )

def rlMeritSucc2 = [id:2]
rlMeritSucc2.npctext = "That is your choice. But if you do not pass over those totems, then the Spirits will not speak to you and you will have made the choice of which of your fates will befall you."
rlMeritSucc2.options = []
rlMeritSucc2.options << [text: "So fork it over, or all bets are off, eh? Okay fine. Take 'em.", result: 4]
rlMeritSucc2.options << [text: "That's blackmail and I'm not having any of it. These totems are MINE.", result: 3]
rinLinMeritSuccess.addDialog( rlMeritSucc2, Rin )

def rlMeritSucc3 = [id:3]
rlMeritSucc3.npctext = "So be it. It is sad to see someone with so much promise choose to go down a road of darkness. Perhaps we will find another savior elsewhere. Go now to your doom, %p. To all of our dooms."
rlMeritSucc3.flag = "Z19PreviouslyRefused"
rlMeritSucc3.result = DONE
rinLinMeritSuccess.addDialog( rlMeritSucc3, Rin )

def rlMeritSucc4 = [id:4]
rlMeritSucc4.npctext = "You have chosen wisely, %p."
rlMeritSucc4.exec = { event ->
	//remove the four items from the player's inventory
	runOnDeduct( event.player, 100407, 1, checkSpearHead, badResponse ) //Tropical Bird Feather
}
rlMeritSucc4.result = 5
rinLinMeritSuccess.addDialog( rlMeritSucc4, Rin )

//More Removal Checks
checkSpearHead = { event ->
	println "***** Tropical Bird Feather removed successfully. *****"
	runOnDeduct( event.player, 100395, 1, checkMaceHead, badResponse ) //Spear Head
}

checkMaceHead = { event ->
	println "***** Spear Head removed successfully. *****"
	runOnDeduct( event.player, 100413, 1, checkJewelEye, badResponse ) //Mace Head
}

checkJewelEye = { event ->
	println "***** Mace Head removed successfully. *****"
	runOnDeduct( event.player, 100266, 1, continueConvo, badResponse ) //Jewel Eye
}

continueConvo = { event ->
	println "***** Jewel Eye removed successfully. *****"
	event.player.setQuestFlag( GLOBAL, "Z19AllMeritItemsRemoved" )
	Rin.pushDialog( event.player, "rinLinContinueMerit" )
}

badResponse = { event ->
	event.player.setQuestFlag( GLOBAL, "Z19AtLeastOneMeritItemMissing" )
	Rin.pushDialog( event.player, "rinLinWhereAreThey" )
}

//---------------------------------------------------------
// MERIT IN THE EYES OF THE SPIRIT (alt Success)           
//---------------------------------------------------------

def rlGauntletAltSuccess = Lin.createConversation( "rlGauntletAltSuccess", true, "Z19PreviouslyRefused", "QuestStarted_102:6", "!QuestCompleted_102" )

def rlAltSuccess1 = [id:1]
rlAltSuccess1.npctext = "You have returned. Why?"
rlAltSuccess1.options = []
rlAltSuccess1.options << [text: "I had second thoughts. I don't want to be the cause of more destruction. What can I do to help?", result: 3]
rlAltSuccess1.options << [text: "I don't know. I think maybe it was a bad idea. Nevermind.", result: 2]
rlGauntletAltSuccess.addDialog( rlAltSuccess1, Lin )

def rlAltSuccess2 = [id:2]
rlAltSuccess2.npctext = "Oh. I thought that perhaps you had changed your mind...but no, I see that is not the case now. Goodbye."
rlAltSuccess2.result = DONE
rlGauntletAltSuccess.addDialog( rlAltSuccess2, Lin )

def rlAltSuccess3 = [id:3]
rlAltSuccess3.npctext = "Good. Then hand me the totems."
rlAltSuccess3.playertext = "Okay, here you go."
rlAltSuccess3.flag = "!Z19PreviouslyRefused"
rlAltSuccess3.exec = { event ->
	//remove the four items from the player's inventory
	runOnDeduct( event.player, 100407, 1, checkSpearHead, badResponse ) //Tropical Bird Feather
}	
rlAltSuccess3.result = DONE
rlGauntletAltSuccess.addDialog( rlAltSuccess3, Lin )


//---------------------------------------------------------
// RESUMING MERIT CONVO AFTER ITEM REMOVAL                 
//---------------------------------------------------------

def rinLinContinueMerit = Rin.createConversation( "rinLinContinueMerit", true, "Z19AllMeritItemsRemoved", "!QuestCompleted_102", "!Z19MeritProven" )

def rlContMerit1 = [id:1]
rlContMerit1.npctext = "Thank you."
rlContMerit1.playertext = "Wait a minute! Did you just put those things in your belt pouch?"
rlContMerit1.result = 2
rinLinContinueMerit.addDialog( rlContMerit1, Rin )

def rlContMerit2 = [id:2]
rlContMerit2.npctext = "Yes. Why do you ask?"
rlContMerit2.playertext = "But you said...weren't those 'totems' supposed to be used to summon the Spirits or something?"
rlContMerit2.result = 3
rinLinContinueMerit.addDialog( rlContMerit2, Rin )

def rlContMerit3 = [id:3]
rlContMerit3.npctext = "No. The totems were only to prove that you were who we thought you were. Now that you've gathered them and proven your value as a warrior, I may channel the Spirits to speak with you with a pure heart that you are the One."
rlContMerit3.playertext = "What? The One? Wait..."
rlContMerit3.result = 4
rinLinContinueMerit.addDialog( rlContMerit3, Rin )

def rlContMerit4 = [id:4]
rlContMerit4.npctext = "Rin is now gathering his energy to speak with the Spirits. He will not speak directly to you again until they have passed beyond again. If you wish to halt this process, you must speak now."
rlContMerit4.options = []
rlContMerit4.options << [text: "I'm ready now, I suppose. Let's do this.", result: 6]
rlContMerit4.options << [text: "Wait! Let's do this later if there's an option!", result: 5]
rinLinContinueMerit.addDialog( rlContMerit4, Lin )

def rlContMerit5 = [id:5]
rlContMerit5.npctext = "So be it. Rin will be ready another time when you return to this sacred clearing. Until then, be safe. You are the world's hope."
rlContMerit5.flag = "Z19MeritProven"
rlContMerit5.exec = { event ->
	event.player.updateQuest( 102, "Otomi Rin-VQS" ) //push the completion of MERIT IN THE EYES OF THE SPIRITS quest
	event.player.removeMiniMapQuestActorName( "Otomi Rin-VQS" )
}
rlContMerit5.result = DONE
rinLinContinueMerit.addDialog( rlContMerit5, Lin )

def rlContMerit6 = [id:6]
rlContMerit6.npctext = "So be it. Your world-changing adventures are about to begin, %p. Be strong and true for all of us, and do not fail."
rlContMerit6.quest = 102 //push the completion of MERIT IN THE EYES OF THE SPIRITS quest
rlContMerit6.flag = "Z19MeritProven"
rlContMerit6.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Otomi Rin-VQS" )
	Rin.pushDialog( event.player, "rinLinGauntletStart" )
}
rlContMerit6.result = DONE
rinLinContinueMerit.addDialog( rlContMerit6, Rin )


//---------------------------------------------------------
// BAD RESPONSE IF MERIT ITEMS ARE NOT IN INVENTORY        
//---------------------------------------------------------

def rinLinWhereAreThey = Rin.createConversation( "rinLinWhereAreThey", true, "Z19AtLeastOneMeritItemMissing", "!QuestCompleted_102" )

def rlWhere1 = [id:1]
rlWhere1.npctext = "But wait...the Spirits say that you have gathered the items needed, and yet, you do not have them in your possession. You must find them and bring them to us before we can continue."
rlWhere1.playertext = "That's odd...I could have sworn I had them. Okay. I'll make sure I have all four items next time when I come back."
rlWhere1.flag = "!Z19AtLeastOneMeritItemMissing"
rlWhere1.result = DONE
rinLinWhereAreThey.addDialog( rlWhere1, Rin )

//---------------------------------------------------------
// THE GAUNTLET (Start)                                    
//---------------------------------------------------------

def rinLinGauntletStart = Rin.createConversation( "rinLinGauntletStart", true, "Z19MeritProven", "!QuestStarted_103", "!QuestCompleted_103" )

def rlGaunt1 = [id:1]
rlGaunt1.npctext = "Spirits of the world, hear your mortal servant and vessel! These totems of the Unliving have been gathered at hazard to prove the valor of those beseeching your aid."
rlGaunt1.result = 2
rinLinGauntletStart.addDialog( rlGaunt1, Rin )

def rlGaunt2 = [id:2]
rlGaunt2.npctext = "Hear me O Spirits!"
rlGaunt2.playertext = "Ummm...is something supposed to be happening?"
rlGaunt2.exec = { event ->
	myManager.schedule(1) { spiritSpeakOne( event ) }
}
rlGaunt2.result = DONE
rinLinGauntletStart.addDialog( rlGaunt2, Rin )

def spiritSpeakOne( event ) {
	event.player.centerPrint( "YOU ARE HEARD, CHOSEN ONE. SPEAK THAT WE MAY KNOW YOUR DESIRES!" )
	myManager.schedule(3) { event.player.setQuestFlag( GLOBAL, "Z19SpiritOne" ); Rin.pushDialog( event.player, "spiritOne" )  }
}

//Next section of dialog (Spirit One) 
def spiritOne = Rin.createConversation( "spiritOne", true, "Z19SpiritOne", "!Z19SpiritTwo" )

def spiritOne1 = [id:1]
spiritOne1.playertext = "Whoa!"
spiritOne1.exec = { event ->
	myManager.schedule(1) { spiritSpeakTwo( event ) }
}
spiritOne1.result = DONE
spiritOne.addDialog( spiritOne1, Rin )

def spiritSpeakTwo( event ) {
	event.player.centerPrint( "WHAT BOON WOULD YOU EARN FROM US?" )
	myManager.schedule(3) { event.player.setQuestFlag( GLOBAL, "Z19SpiritTwo" ); Rin.pushDialog( event.player, "spiritTwo" ) }
}

//Next section of dialog (Spirit Two) 
def spiritTwo = Rin.createConversation( "spiritTwo", true, "Z19SpiritTwo", "!Z19SpiritThree" )

def spiritTwo1 = [id:1]
spiritTwo1.npctext = "Hush! The Spirits give me visions, but they *speak* through Rin. You must be reverent!"
spiritTwo1.playertext = "I didn't know the Spirits were *real*!"
spiritTwo1.result = 2
spiritTwo.addDialog( spiritTwo1, Lin )

def spiritTwo2 = [id:2]
spiritTwo2.npctext = "The one that comes before me now does not know it, but he wishes a boon from you to breath water as if it were air."
spiritTwo2.result = 3
spiritTwo.addDialog( spiritTwo2, Rin )

def spiritTwo3 = [id:3]
spiritTwo3.playertext = "I want...what?!?"
spiritTwo3.exec = { event ->
	myManager.schedule(1) { spiritSpeakThree( event ) }
}
spiritTwo3.result = DONE
spiritTwo.addDialog( spiritTwo3, Rin )

def spiritSpeakThree( event ) {
	event.player.centerPrint( "WE SEE THE FUTURE YOU SPEAK OF AND APPROVE OF YOUR INTENTION." )
	myManager.schedule(3) { event.player.setQuestFlag( GLOBAL, "Z19SpiritThree" ); Rin.pushDialog( event.player, "spiritThree" ) }
}

//Next section of dialog (Spirit Three) 
def spiritThree = Rin.createConversation( "spiritThree", true, "Z19SpiritThree", "!Z19SpiritFour" )

def spiritThree1 = [id:1]
spiritThree1.npctext = "%p! Do you not know how to hush?"
spiritThree1.exec = { event ->
	myManager.schedule(1) { spiritSpeakFour( event ) }
}
spiritThree1.result = DONE
spiritThree.addDialog( spiritThree1, Lin )

def spiritSpeakFour( event ) {
	event.player.centerPrint( "BUT SUCH BOONS MAY NOT BE GRANTED WITHOUT CHALLENGE." )
	myManager.schedule(3) { event.player.setQuestFlag( GLOBAL, "Z19SpiritFour" ); Rin.pushDialog( event.player, "spiritFour" ) }
}

//Next section of dialog (Spirit Four) 
def spiritFour = Rin.createConversation( "spiritFour", true, "Z19SpiritFour", "!Z19SpiritFive" )

def spiritFour1 = [id:1]
spiritFour1.npctext = "Of course, Great Spirits. What challenges will this one face for such a reward?"
spiritFour1.playertext = "Again...no choice! What is with you people?"
spiritFour1.exec = { event ->
	myManager.schedule(1) { spiritSpeakFive( event ) }
}
spiritFour1.result = DONE
spiritFour.addDialog( spiritFour1, Rin )

def spiritSpeakFive( event ) {
	event.player.centerPrint( "THE TESTS OF CHALOC WILL BE YOUR CHALLENGES! LET THEM BEGIN!" )
	myManager.schedule(3) { event.player.setQuestFlag( GLOBAL, "Z19SpiritFive" ); Rin.pushDialog( event.player, "spiritFive" ) }
}

//Next section of dialog (Spirit Four) 
def spiritFive = Rin.createConversation( "spiritFive", true, "Z19SpiritFive", "!Z19FirstGauntletSpiritTalkDone" )

//def endSpirit1 = [id:1]
//endSpirit1.npctext = "..."
//endSpirit1.result = 2
//spiritFive.addDialog( endSpirit1, Rin )

def endSpirit2 = [id:2]
endSpirit2.npctext = "It is done. The Spirits have departed. Rin is taking a moment to come back to himself now."
endSpirit2.playertext = "Now wait just a minute! You two are completely for real? I mean...I thought all this Spirit stuff was just mumbo-jumbo to impress the locals!"
endSpirit2.result = 3
spiritFive.addDialog( endSpirit2, Lin )

def endSpirit3 = [id:3]
endSpirit3.npctext = "No. They are very real, at least here in the lands of The People. I think they have less power farther away from the Shallow Sea, but here, they are mighty, and in recent years, they have been very active in the lives of The People."
endSpirit3.playertext = "So you two just signed me up for some sort of quest to face the challenges of spirits that I never knew existed, for a 'boon' that I never asked for, and now if I don't face that challenge, I'm going to have a GOD angry at me?"
endSpirit3.result = 4
spiritFive.addDialog( endSpirit3, Lin )

def endSpirit4 = [id:4]
endSpirit4.npctext = "Yes. Although they are merely Spirits of Gaia's elements made manifest...not exactly 'gods' as you barbarians usually refer to them. But that is essentially correct."
endSpirit4.playertext = "Well...isn't that just fine?!?"
endSpirit4.result = 5
spiritFive.addDialog( endSpirit4, Rin )

def endSpirit5 = [id:5]
endSpirit5.npctext = "You must face the two challenges set for you. The first is the Challenge of Chaloc, the Spirit of Rain whose most sacred grounds are the headwaters of the river which cascades down the steps to the ponds below."
endSpirit5.playertext = "The river that feeds the waterfall? Okay...what's there?"
endSpirit5.result = 6
spiritFive.addDialog( endSpirit5, Lin )

def endSpirit6 = [id:6]
endSpirit6.npctext = "Next to the waterfall, you will find a vine-enshrouded doorway leading to a winding stair that goes to the top of the cliffs. You must journey to the bridge near the headwaters, cross it and then find the shrine of Chaloc."
endSpirit6.playertext = "Okay...that sounds easy. What then?"
endSpirit6.result = 7
spiritFive.addDialog( endSpirit6, Lin )

def endSpirit7 = [id:7]
endSpirit7.npctext = "Nothing. Just touch the statue, and then survive Chaloc's Gauntlet"
endSpirit7.playertext = "Why is it that I feel like you're not telling me everything?"
endSpirit7.result = 8
spiritFive.addDialog( endSpirit7, Rin )

def endSpirit8 = [id:8]
endSpirit8.npctext = "We have told you all that we may. May the Spirits smile upon you."
endSpirit8.playertext = "Well...they'd better! I'm doing this for them, after all! Dang!"
endSpirit8.flag = "Z19FirstGauntletSpiritTalkDone"
endSpirit8.result = DONE
endSpirit8.quest = 103 //push the start of the GAUNTLET quest
spiritFive.addDialog( endSpirit8, Rin )


//---------------------------------------------------------
// THE GAUNTLET (Interim)                                  
//---------------------------------------------------------

def rinLinGauntletInterim = Lin.createConversation( "rinLinGauntletInterim", true, "QuestStarted_103", "!QuestStarted_103:3" )

def rlGauntInt1 = [id:1]
rlGauntInt1.npctext = "I know not why you have returned to us when your challenge from the Spirits has not yet been fulfilled. Is there something further you wish to learn from us?"
rlGauntInt1.options = []
rlGauntInt1.options << [text: "Can you tell me more about the Otami society?", result: 2]
rlGauntInt1.options << [text: "Most Gaians think your people are extinct. How many of you are there still in the jungles?", result: 2]
rlGauntInt1.options << [text: "Who are these Spirits that you keep talking about?", result: 2]
rinLinGauntletInterim.addDialog( rlGauntInt1, Lin )

def rlGauntInt2 = [id:2]
rlGauntInt2.npctext = "Your question has no bearing on the success of your challenge. I suggest that you focus on that test exclusively. The Spirits will not be otherwise pleased."
rlGauntInt2.playertext = "So you're just not going to answer my questions?"
rlGauntInt2.result = 3
rinLinGauntletInterim.addDialog( rlGauntInt2, Lin )

def rlGauntInt3 = [id:3]
rlGauntInt3.npctext = "She has given you your answer already, %p. Go now, while you still can."
rlGauntInt3.result = DONE
rinLinGauntletInterim.addDialog( rlGauntInt3, Rin )

//---------------------------------------------------------
// THE GAUNTLET (Success)                                  
//---------------------------------------------------------

def rinLinGauntletSuccess = Rin.createConversation( "rinLinGauntletSuccess", true, "QuestStarted_103:3", "!QuestCompleted_103" )
rinLinGauntletSuccess.setUrgent( true )

def rlGauntSucc1 = [id:1]
rlGauntSucc1.playertext = "That was insane! That Gauntlet just about killed me!"
rlGauntSucc1.result = 2
rinLinGauntletSuccess.addDialog( rlGauntSucc1, Rin )

def rlGauntSucc2 = [id:2]
rlGauntSucc2.npctext = "That is as it was supposed to be. Anything less would not have been a test worthy of the Spirits."
rlGauntSucc2.playertext = "Not worthy, eh? Your Spirits are a tough bunch of task masters. But okay...I passed the test. Where's that 'boon' they mentioned?"
rlGauntSucc2.result = 3
rinLinGauntletSuccess.addDialog( rlGauntSucc2, Rin )

def rlGauntSucc3 = [id:3]
rlGauntSucc3.npctext = "That is but the first, and least, of the tests the two Spirits have given you, %p."
rlGauntSucc3.playertext = "What? Two Spirits? Chaloc *and*...that other one..."
rlGauntSucc3.result = 4
rinLinGauntletSuccess.addDialog( rlGauntSucc3, Rin )

def rlGauntSucc4 = [id:4]
rlGauntSucc4.npctext = "Huitzotl, Spirit of the Air and Stone, yes. The Spirits ask that you defeat a particularly offensive sacrilege they suffer within the very Throne Room of the Otami."
rlGauntSucc4.options = []
rlGauntSucc4.options << [text: "All right. I'm up to the challenge. What's the deal?", result: 5]
rlGauntSucc4.options << [text: "I need a rest before I take on the next challenge. I'll be back later.", result: 6]
rinLinGauntletSuccess.addDialog( rlGauntSucc4, Lin )

def rlGauntSucc5 = [id:5]
rlGauntSucc5.npctext = "Good. I am glad that you are so eager."
rlGauntSucc5.flag = "Z19GauntletRewardGiven"
rlGauntSucc5.quest = 103 //push the completion of THE GAUNTLET quest
rlGauntSucc5.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Otomi Rin-VQS" )
	Rin.pushDialog( event.player, "rinLinThroneStart" )
}
rlGauntSucc5.result = DONE
rinLinGauntletSuccess.addDialog( rlGauntSucc5, Rin )

def rlGauntSucc6 = [id:6]
rlGauntSucc6.npctext = "So be it. When you return to us, we will lead you to the next challenge."
rlGauntSucc6.playertext = "Okay...see you then!"
rlGauntSucc6.flag = "Z19GauntletConvContinues"
rlGauntSucc6.quest = 103 //push the completion of THE GAUNTLET quest
rlGauntSucc6.result = DONE
rinLinGauntletSuccess.addDialog( rlGauntSucc6, Rin )

//---------------------------------------------------------
// THE GAUNTLET (Success)                                  
//---------------------------------------------------------

def rinLinBridgeToThrone = Rin.createConversation( "rinLinBridgeToThrone", true, "QuestCompleted_103", "Z19GauntletConvContinues" )

def rlBridgeThrone1 = [id:1]
rlBridgeThrone1.npctext = "Welcome back, %p. You have completed the first, and least, of the two tests the Spirits have given you, %p. You must now prepare yourself for the next task."
rlBridgeThrone1.playertext = "What? Two Spirits? Oh right. Chaloc *and*...that other one..."
rlBridgeThrone1.result = 2
rinLinBridgeToThrone.addDialog( rlBridgeThrone1, Rin )

def rlBridgeThrone2 = [id:2]
rlBridgeThrone2.npctext = "Huitzotl, Spirit of the Air and Stone, yes. The Spirits ask that you defeat a particularly offensive sacrilege they suffer within the very Throne Room of the Otami."
rlBridgeThrone2.playertext = "Sacrilege, eh?"
rlBridgeThrone2.result = 3
rinLinBridgeToThrone.addDialog( rlBridgeThrone2, Lin )

def rlBridgeThrone3 = [id:3]
rlBridgeThrone3.npctext = "They desecrate the greatest of our Spirits. It cannot be tolerated."
rlBridgeThrone3.flag = [ "Z19GauntletRewardGiven", "!Z19GauntletConvContinues" ]
rlBridgeThrone3.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Otomi Rin-VQS" )
	Rin.pushDialog( event.player, "rinLinThroneStart" )
}
rlBridgeThrone3.result = DONE
rinLinBridgeToThrone.addDialog( rlBridgeThrone3, Rin )


//---------------------------------------------------------
// THE THRONE ROOM (Start)                                 
//---------------------------------------------------------

def rinLinThroneStart = Rin.createConversation( "rinLinThroneStart", true, "Z19GauntletRewardGiven", "!QuestStarted_104", "!QuestCompleted_104", "!Z19GauntletConvContinues" )

def rlThrone1 = [id:1]
rlThrone1.npctext = "To brave this challenge, you must fight your way through the Unliving hordes to the entrance to the Throne Room of our People."
rlThrone1.playertext = "Sounds like quite the challenge!"
rlThrone1.result = 2
rinLinThroneStart.addDialog( rlThrone1, Rin )

def rlThrone2 = [id:2]
rlThrone2.npctext = "That is *not* the challenge."
rlThrone2.playertext = "Oh. There's more. Okay...what next then?"
rlThrone2.result = 3
rinLinThroneStart.addDialog( rlThrone2, Rin )

def rlThrone3 = [id:3]
rlThrone3.npctext = "Once you reach the Throne Room entrance, you will pass within and face the abomination that inhabits that room now."
rlThrone3.playertext = "What sort of 'abomination' would that be?"
rlThrone3.result = 4
rinLinThroneStart.addDialog( rlThrone3, Rin )

def rlThrone4 = [id:4]
rlThrone4.npctext = "The Unliving have inhabited the statue of Chaloc himself. It has made the Spirit *very* angry."
rlThrone4.playertext = "So one of the Animated brought the Spirit's statue to life? Yeah...I can see why that ticked it off."
rlThrone4.result = 5
rinLinThroneStart.addDialog( rlThrone4, Lin )

def rlThrone5 = [id:5]
rlThrone5.npctext = "The Spirit wishes you to destroy the Unliving and return its Throne Room to peace. You will know which of the foes to vanquish by sight. Chaloc is a great stone serpent that controls the lightning and sky."
rlThrone5.playertext = "Hoo boy. So...fight my way to the Throne Room, and then beat down an Animated in the form of one of your Spirits. Got it."
rlThrone5.result = 6
rinLinThroneStart.addDialog( rlThrone5, Lin )

def rlThrone6 = [id:6]
rlThrone6.npctext = "Yes. That will suffice. We do hope you return. There are other hopes in the world, but none as strong as you."
rlThrone6.playertext = "Wow. You guys really know how to lay it on thick. Okay...I'll do my best."
rlThrone6.quest = 104 //push the start of THE STONE COATL quest
rlThrone6.result = DONE
rinLinThroneStart.addDialog( rlThrone6, Lin )

//---------------------------------------------------------
// THE THRONE ROOM (Interim)                               
//---------------------------------------------------------

def rinLinThroneInterim = Rin.createConversation( "rinLinThroneInterim", true, "QuestStarted_104:2", "!QuestStarted_104:3" )

def rlThroneInt1 = [id:1]
rlThroneInt1.npctext = "Why do you return to us again? How could your objective have not been clear?"
rlThroneInt1.playertext = "I...ummm...just thought you might have something else to share with me to make the task easier..."
rlThroneInt1.result = 2
rinLinThroneInterim.addDialog( rlThroneInt1, Rin )

def rlThroneInt2 = [id:2]
rlThroneInt2.npctext = "No. We do not. You know your task. Now do it."
rlThroneInt2.playertext = "Sheesh. Don't get so uptight, Rin. I'm on it!"
rlThroneInt2.result = DONE
rinLinThroneInterim.addDialog( rlThroneInt2, Rin )


//---------------------------------------------------------
// THE THRONE ROOM (Success)                               
//---------------------------------------------------------

def rinLinThroneSuccess = Lin.createConversation( "rinLinThroneSuccess", true, "QuestStarted_104:3", "!QuestCompleted_104", "!Z19ThroneOne" )
rinLinThroneSuccess.setUrgent( true )

def rlThroneSucc1 = [id:1]
rlThroneSucc1.npctext = "Congratulations, %p! We did not wish to dissuade you from trying, but most of the possible futures we saw ended with your body crushed to the floor beneath the Stone Coatl. We are well pleased that you survived!"
rlThroneSucc1.playertext = "Ummm...wow. Okay...well...I'm glad I survived, too!"
rlThroneSucc1.result = 2
rinLinThroneSuccess.addDialog( rlThroneSucc1, Lin )

def rlThroneSucc2 = [id:2]
rlThroneSucc2.npctext = "You have done very well, %p. We believe now that the world has hope for the first time since the Animated appeared."
rlThroneSucc2.playertext = "Okay...so are you going to explain about this 'boon' you mentioned before?"
rlThroneSucc2.exec  = { event ->
	myManager.schedule(1) { spiritThroneOne( event ) }
}
rlThroneSucc2.result = DONE
rinLinThroneSuccess.addDialog( rlThroneSucc2, Lin )

def spiritThroneOne( event ) {
	event.player.centerPrint( "YOU HAVE DONE WELL, MORTAL!" )
	myManager.schedule(3) { event.player.centerPrint( "YOU HAVE PASSED THE TESTS OF CHALOC. WE ARE PLEASED." ) }
	myManager.schedule(6) { event.player.setQuestFlag( GLOBAL, "Z19ThroneOne" ); Rin.pushDialog( event.player, "throneOne" )  }
}


//Continuing the Throne Room Success dialog (throneOne)

def throneOne = Rin.createConversation( "throneOne", true, "Z19ThroneOne", "!Z19ThroneTwo" )

def throneOne1 = [id:3]
throneOne1.playertext = "Oh. I guess I'm about to find out then..."
throneOne1.exec = { event ->
	myManager.schedule(1) { spiritThroneTwo( event ) }
}
throneOne1.result = DONE
throneOne.addDialog( throneOne1, Lin )

def spiritThroneTwo( event ) {
	event.player.centerPrint( "THE BOON OF CHALOC IS BESTOWED UPON YOU NOW. USE IT WISELY FOR THE BENEFIT OF GAIA." )
	myManager.schedule(3) { event.player.setQuestFlag( GLOBAL, "Z19ThroneTwo" ); Rin.pushDialog( event.player, "throneTwo" )  }
}


//Continuing the Throne Room Success dialog (throneTwo)

def throneTwo = Rin.createConversation( "throneTwo", true, "Z19ThroneTwo", "!Z19ThroneSpiritDone" )

def throneTwo1 = [id:1]
throneTwo1.playertext = "How am I supposed to know what is for the 'benefit of Gaia'?"
throneTwo1.result = 2
throneTwo.addDialog( throneTwo1, Lin )

def throneTwo2 = [id:2]
throneTwo2.npctext = "The Spirits have left Rin once again."
throneTwo2.playertext = "Oh great. Now how am I gonna get my questions answered?"
throneTwo2.result = 3
throneTwo.addDialog( throneTwo2, Lin )

def throneTwo3 = [id:3]
throneTwo3.npctext = "What questions would those be, %p?"
throneTwo3.options = []
throneTwo3.options << [text: "So, just what is this 'Boon of Chaloc'?", result: 4]
throneTwo3.options << [text: "What am I supposed to use the boon for?", result: 5]
throneTwo3.options << [text: "How am *I* supposed to 'save the world' someday?", result: 7]
throneTwo3.options << [text: "Just where ARE the rest of your people?", result: 10]
throneTwo3.options << [text: "Oh, nevermind. I'll just find out any other answers myself.", result: 13]
throneTwo.addDialog( throneTwo3, Lin )

def throneTwo4 = [id:4]
throneTwo4.npctext = "Chaloc is the Rain Spirit and his boon allows you to breath water as if it were air. This boon extends to all the water within the realm of the Otami...including the Shallow Sea."
throneTwo4.playertext = "So I can breath underwater? Cool! But that still doesn't answer my questions."
throneTwo4.result = 3
throneTwo.addDialog( throneTwo4, Lin )

def throneTwo5 = [id:5]
throneTwo5.npctext = "There is a man you must save. A man that survives beneath the water, captured by more of the Unliving. More than that we may not say. But if that man dies, then your future also dies."
throneTwo5.playertext = "Wait! What kind of answer is that? How do I find the guy? Who *is* the guy?"
throneTwo5.result = 6
throneTwo.addDialog( throneTwo5, Lin )

def throneTwo6 = [id:6]
throneTwo6.npctext = "Be still. More than this we may not say or your path will be altered away from the desired future as if you had failed already."
throneTwo6.playertext = "But...ah, drat. You two stink at answering questions."
throneTwo6.result = 3
throneTwo.addDialog( throneTwo6, Rin )

def throneTwo7 = [id:7]
throneTwo7.npctext = "There are thousands and thousands of possibilities with which to thread the loom of our future. From those threads, there are several that result in the world not being destroyed. But only a few."
throneTwo7.result = 8
throneTwo.addDialog( throneTwo7, Lin )

def throneTwo8 = [id:8]
throneTwo8.npctext = "And despite the fact that seem an improbable savior, those threads where you make the best decisions are the strongest of possibilities."
throneTwo8.playertext = "And...?"
throneTwo8.result = 9
throneTwo.addDialog( throneTwo8, Rin )

def throneTwo9 = [id:9]
throneTwo9.npctext = "And what? We may not reveal your possible futures, or only those that end in your immediate death could come true. Since none of us desires that outcome, these answers must suffice."
throneTwo9.playertext = "Suffice? Are you kidding? That kind of answer just brings up more questions!"
throneTwo9.result = 3
throneTwo.addDialog( throneTwo9, Rin )

def throneTwo10 = [id:10]
throneTwo10.npctext = "The number of our people is small, but intentionally so. Our society tore itself apart with constant warfare, and we made the decision to abandon the model of the city-state in favor of more sustainable, peaceful village enclaves."
throneTwo10.playertext = "So, there are villages of you out there in the jungle?"
throneTwo10.result = 11
throneTwo.addDialog( throneTwo10, Lin )

def throneTwo11 = [id:11]
throneTwo11.npctext = "Possibly. But more we cannot say without the permission of the Elders. But now that you have proven yourself through the challenges of our Spirits, there is no village of ours that would deny you succor."
throneTwo11.playertext = "Oh great. A group of people I've never met, in hidden villages, will give me any aid I want. Perfect. Thanks."
throneTwo11.result = 12
throneTwo.addDialog( throneTwo11, Rin )

def throneTwo12 = [id:12]
throneTwo12.npctext = "You are a funny kind of hero, %p. But our hopes lay with you."
throneTwo12.playertext = "It seems like the more I talk to you, the more questions I have to answer."
throneTwo12.result = 3
throneTwo.addDialog( throneTwo12, Lin )

def throneTwo13 = [id:13]
throneTwo13.npctext = "That is truly the only path you may travel anyway. Each of us must find our own answers in life."
throneTwo13.playertext = "Sure, sure. Okay. I'll go find my way then. See ya."
throneTwo13.flag = "Z19ThroneSpiritDone"
throneTwo13.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Otomi Rin-VQS" )
	event.player.updateQuest( 104, "Otomi Lin-VQS" ) //push the completion of THRONE ROOM quest
	event.player.updateQuest( 107, "Otomi Rin-VQS" ) //push the completion of Blaze's "wrapper" quest so the player can return to her and finish.
}
throneTwo13.result = 14
throneTwo.addDialog( throneTwo13, Lin )