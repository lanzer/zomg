import com.gaiaonline.mmo.battle.script.*;

def Ryan = spawnNPC("Ryan-VQS", myRooms.BASSKEN_607, 100, 250)
Ryan.setRotation( 45 )
Ryan.setDisplayName( "Ryan" )

onQuestStep( 282, 2 ) { event -> event.player.addMiniMapQuestActorName( "Ryan-VQS" ) }
onQuestStep(331, 3) { event -> event.player.addMiniMapQuestActorName("Ryan-VQS") }

//---------------------------------------------------------
// Default Conversation - North Gate Guard                 
//---------------------------------------------------------

def RyanGuard = Ryan.createConversation("RyanGuard", true, "!QuestStarted_79", "!QuestCompleted_79", "!Z06AfterGustav")

def Ryan1 = [id:1]
Ryan1.npctext = "Greetings, Citizen. Watch yourself out there. The Outlaw Wolves may *look* cuddly, but their bite is worse than their bark."
Ryan1.playertext = "hehehe. Funny!"
Ryan1.options = []
Ryan1.options << [text:"Do you need a hand with anything out here?", result: 2]
Ryan1.options << [text:"Thanks! See ya!", result: DONE]
RyanGuard.addDialog(Ryan1, Ryan)

def Ryan2 = [id:2]
Ryan2.npctext = "Actually, I do! We haven't seen hide nor hair of Gustav, the old trapper and tree cutter that lives out in the woods near the Lake. You wanna see if you can find him and tell him to check in with us here at the Gate?"
Ryan2.options = []
Ryan2.options << [text:"I can take a look. Is there any place special he likes to hang out?", result: 4]
Ryan2.options << [text:"I'm doing other things right now. Maybe later.", result: 3]
RyanGuard.addDialog(Ryan2, Ryan)

def Ryan3 = [id:3]
Ryan3.npctext = "There's plenty that needs doing. That's sure enough. Best of luck to you, citizen."
Ryan3.playertext = "Back at ya!"
Ryan3.result = DONE
RyanGuard.addDialog(Ryan3, Ryan)

def Ryan4 = [id:4]
Ryan4.npctext = "Sometimes he's up by the old abandoned lumber mill, and sometimes he hangs out in the forest south of the Ole Fishing Hole. Most times, anyway."
Ryan4.playertext = "Hmmm...okay. I'll watch for him."
Ryan4.result = 5
RyanGuard.addDialog(Ryan4, Ryan)

def Ryan5 = [id:5]
Ryan5.npctext = "Much appreciated. Good luck to you!"
Ryan5.quest = 79 //Start looking for Gustav
Ryan5.result = DONE
RyanGuard.addDialog(Ryan5, Ryan)

//---------------------------------------------------------
// Interim Conversation - Looking for Gustav               
//---------------------------------------------------------
def GustavInterim = Ryan.createConversation("GustavInterim", true, "QuestStarted_79:2", "!QuestStarted_79:3")

def Interim1 = [id:1]
Interim1.npctext = "Hey! You made it back! Did you find Gustav yet?"
Interim1.playertext = "Nope. Not yet."
Interim1.result = 2
GustavInterim.addDialog(Interim1, Ryan)

def Interim2 = [id:2]
Interim2.npctext = "You will...eventually. He always shows up."
Interim2.result = DONE
GustavInterim.addDialog(Interim2, Ryan)

//---------------------------------------------------------
// Success Convo - Looking for Gustav                      
//---------------------------------------------------------
def GustavSuccess = Ryan.createConversation("GustavSuccess", true, "QuestStarted_79:3")

def Succ1 = [id:1]
Succ1.playertext = "Gustav says that he's just fine but that he will 'stay and play with ze doggeez for a while'."
Succ1.result = 2
GustavSuccess.addDialog(Succ1, Ryan)

def Succ2 = [id:2]
Succ2.npctext = "lol. That sounds just like Gustav. I'm glad he's okay then. Great job on finding him!"
Succ2.playertext = "It wasn't too much trouble. Anytime."
Succ2.quest = 79 //Complete the "Looking for Gustav" quest.
Succ2.flag = "Z06AfterGustav"
Succ2.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Ryan-VQS" )
	myManager.schedule(1) { event.player.removeMiniMapQuestActorName( "Ryan-VQS" ); Ryan.pushDialog(event.player, "AfterGustav") }
}
GustavSuccess.addDialog(Succ2, Ryan)

//---------------------------------------------------------
// After Gustav - Post-quest Default Dialog                
//---------------------------------------------------------
def AfterGustav = Ryan.createConversation("AfterGustav", true, "Z06AfterGustav", "!QuestStarted_282", "!Z5_Ryan_RabidFluff_Allowed", "!Z5_Ryan_RabidFluff_Denied", "!QuestStarted_331:4")

def After1 = [id:1]
After1.npctext = "Well, hello again %p. I've got something strange for you, if you're interested."
After1.playertext = "Oh yeah? I always enjoy a bit of the weird. Tell me!"
After1.result = 2
AfterGustav.addDialog(After1, Ryan)

def After2 = [id:2]
After2.npctext = "You've probably seen the town's 'beautification cans' they put along the roadside, right?"
After2.playertext = "Of course. How could you miss them?"
After2.result = 3
AfterGustav.addDialog(After2, Ryan)

def After3 = [id:3]
After3.npctext = "Well...if you bang those cans real hard, it makes those Fluff things appear out of the trees!"
After3.playertext = "Really?"
After3.result = 4
AfterGustav.addDialog( After3, Ryan )

def After4 = [id:4]
After4.npctext = "I kid you not! And if you don't mind, I'd like you to help me test a pet theory of mine."
After4.options = []
After4.options << [text:"Oh yeah? How can I help?", exec: { event ->
	if(event.player.getConLevel() < 5.1) {
		event.player.setQuestFlag(GLOBAL, "Z5_Ryan_RabidFluff_Allowed")
		Ryan.pushDialog(event.player, "rabidFluffAllowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z5_Ryan_RabidFluff_Denied")
		Ryan.pushDialog(event.player, "rabidFluffDenied")
	}	
}, result: DONE]
After4.options << [text:"That doesn't sound very interesting to me. Have anything else you need done?", exec: { event ->
	if(!event.player.isOnQuest(331) && !event.player.isDoneQuest(331) && !event.player.hasQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_Break") && !event.player.hasQuestFlag(GLOBAL, "Z07RoderickDefaultDone")) {
		event.player.setQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_GrantAllowed")
		Ryan.pushDialog(event.player, "toBelievers")
	}
	if(!event.player.isOnQuest(331) && !event.player.isDoneQuest(331) && event.player.hasQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_Break") && !event.player.hasQuestFlag(GLOBAL, "Z07RoderickDefaultDone")) {
		event.player.setQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_BreakAllowed")
		Ryan.pushDialog(event.player, "toBelieversBreak")
	}
	if(event.player.isOnQuest(331) || event.player.isDoneQuest(331) || event.player.hasQuestFlag(GLOBAL, "Z07RoderickDefaultDone")) {
		event.player.setQuestFlag(GLOBAL, "Z06_Ryan_NoOthers")
		Ryan.pushDialog(event.player, "noOthers")
	}
}, result: DONE]
After4.options << [text:"Maybe later, Ryan. I'll be back.", result: 5]
AfterGustav.addDialog(After4, Ryan)

def After5 = [id:5]
After5.npctext = "Okay. There's no rush. See you soon."
After5.result = DONE
AfterGustav.addDialog(After5, Ryan)

//---------------------------------------------------------
// ACTIVE - Rabid Fluffs                                   
//---------------------------------------------------------
def rabidFluffActive = Ryan.createConversation("rabidFluffActive", true, "QuestStarted_282:2", "!QuestStarted_331:4")

def rabidFluffActive1 = [id:1]
rabidFluffActive1.npctext = "Don't forget to bang the trash cans to attract the fluffs out of the trees and bushes."
rabidFluffActive1.options = []
rabidFluffActive1.options << [text:"Right, trash cans. Noisy things.", result: DONE]
rabidFluffActive1.options << [text:"I'm kinda tired of those. Is there anything else I can do?", exec: { event ->
	if(!event.player.isOnQuest(331) && !event.player.isDoneQuest(331) && !event.player.hasQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_Break") && !event.player.hasQuestFlag(GLOBAL, "Z07RoderickDefaultDone")) {
		event.player.setQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_GrantAllowed")
		Ryan.pushDialog(event.player, "toBelievers")
	}
	if(!event.player.isOnQuest(331) && !event.player.isDoneQuest(331) && event.player.hasQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_Break") && !event.player.hasQuestFlag(GLOBAL, "Z07RoderickDefaultDone")) {
		event.player.setQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_BreakAllowed")
		Ryan.pushDialog(event.player, "toBelieversBreak")
	}
	if(event.player.isOnQuest(331) || event.player.isDoneQuest(331) || event.player.hasQuestFlag(GLOBAL, "Z07RoderickDefaultDone")) {
		event.player.setQuestFlag(GLOBAL, "Z06_Ryan_NoOthers")
		Ryan.pushDialog(event.player, "noOthers")
	}
}, result: DONE]
rabidFluffActive.addDialog(rabidFluffActive1, Ryan)

//---------------------------------------------------------
// COMPLETE - Rabid Fluffs                                 
//---------------------------------------------------------
def rabidFluffComplete = Ryan.createConversation("rabidFluffComplete", true, "QuestStarted_282:3", "!QuestStarted_331:4")

def rabidFluffComplete1 = [id:1]
rabidFluffComplete1.npctext = "I heard you banging the cans around out there. Any luck?"
rabidFluffComplete1.playertext = "Some luck. I whacked a bunch of Carrion Flowers, like you asked, but it didn't seem to thin their ranks much."
rabidFluffComplete1.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Ryan-VQS" )
}
rabidFluffComplete1.result = 2
rabidFluffComplete.addDialog(rabidFluffComplete1, Ryan)

def rabidFluffComplete2 = [id:2]
rabidFluffComplete2.npctext = "Hmmmm...maybe we just need to keep knocking them down for a while. It's hard to tell."
rabidFluffComplete2.options = []
rabidFluffComplete2.options << [text:"I guess I could go out there again.", quest:282, exec: { event ->
	if(event.player.getConLevel() < 5.1) {
		event.player.setQuestFlag(GLOBAL, "Z5_Ryan_RabidFluff_AllowedAgain")
		Ryan.pushDialog(event.player, "rabidFluffAllowedAgain")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z5_Ryan_RabidFluff_Denied")
		Ryan.pushDialog(event.player, "rabidFluffDenied")
	}
}, result: DONE]
rabidFluffComplete2.options << [text:"I dunno, killing fluffs and all is fun but do you have anything else I could do?", quest:282, exec: { event ->
	if(!event.player.isOnQuest(331) && !event.player.isDoneQuest(331) && !event.player.hasQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_Break") && !event.player.hasQuestFlag(GLOBAL, "Z07RoderickDefaultDone")) {
		event.player.setQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_GrantAllowed")
		Ryan.pushDialog(event.player, "toBelievers")
	}
	if(!event.player.isOnQuest(331) && !event.player.isDoneQuest(331) && event.player.hasQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_Break") && !event.player.hasQuestFlag(GLOBAL, "Z07RoderickDefaultDone")) {
		event.player.setQuestFlag(GLOBAL, "Z06_Ryan_ToBelievers_BreakAllowed")
		Ryan.pushDialog(event.player, "toBelieversBreak")
	}
	if(event.player.isOnQuest(331) || event.player.isDoneQuest(331) || event.player.hasQuestFlag(GLOBAL, "Z07RoderickDefaultDone")) {
		event.player.setQuestFlag(GLOBAL, "Z06_Ryan_NoOthers")
		Ryan.pushDialog(event.player, "noOthers")
	}
}, result: DONE]
rabidFluffComplete2.options << [text:"Well maybe I'll come back later and help you out some more.", quest:282, result: 3]
rabidFluffComplete.addDialog(rabidFluffComplete2, Ryan)

def rabidFluffComplete3 = [id:3]
rabidFluffComplete3.npctext = "Okay, %p. Be well."
rabidFluffComplete3.result = DONE
rabidFluffComplete.addDialog(rabidFluffComplete3, Ryan)

//---------------------------------------------------------
// ALLOWED AGAIN - Rabid Fluffs                            
//---------------------------------------------------------
def rabidFluffAllowedAgain = Ryan.createConversation("rabidFluffAllowedAgain", true, "Z5_Ryan_RabidFluff_AllowedAgain")

def rabidFluffAllowedAgain1 = [id:1]
rabidFluffAllowedAgain1.npctext = "That's the spirit, %p!"
rabidFluffAllowedAgain1.exec = { event ->
	event.player.updateQuest(282, "Ryan-VQS")
}
rabidFluffAllowedAgain1.flag = "!Z5_Ryan_RabidFluff_AllowedAgain"
rabidFluffAllowedAgain1.result = DONE
rabidFluffAllowedAgain.addDialog(rabidFluffAllowedAgain1, Ryan)

//---------------------------------------------------------
// ALLOWED - Rabid Fluffs                                  
//---------------------------------------------------------
def rabidFluffAllowed = Ryan.createConversation("rabidFluffAllowed", true, "Z5_Ryan_RabidFluff_Allowed")

def rabidFluffAllowed1 = [id:1]
rabidFluffAllowed1.npctext = "I've been thinking that if we make lots of those Fluffs appear and keep beating them down, then maybe we can drain whatever makes them and get rid of them permanently!"
rabidFluffAllowed1.playertext = "How's that going?"
rabidFluffAllowed1.result = 2
rabidFluffAllowed.addDialog( rabidFluffAllowed1, Ryan )

def rabidFluffAllowed2 = [id:2]
rabidFluffAllowed2.npctext = "Well...so far, no change. But maybe we're close now! If you could go trash 20 of those Carrion Flower Fluffs, maybe it'll make a difference!"
rabidFluffAllowed2.playertext = "Why just the Carrion Flower Fluffs?"
rabidFluffAllowed2.result = 3
rabidFluffAllowed.addDialog( rabidFluffAllowed2, Ryan )

def rabidFluffAllowed3 = [id:3]
rabidFluffAllowed3.npctext = "They smell. Really bad. It seems as good a reason as any."
rabidFluffAllowed3.playertext = "Heh. Okay. Fair enough."
rabidFluffAllowed3.quest = 282
rabidFluffAllowed3.flag = "!Z5_Ryan_RabidFluff_Allowed"
rabidFluffAllowed3.result = DONE
rabidFluffAllowed.addDialog(rabidFluffAllowed3, Ryan)

//---------------------------------------------------------
// DENIED - Rabid Fluffs                                   
//---------------------------------------------------------
def rabidFluffDenied = Ryan.createConversation("rabidFluffDenied", true, "Z5_Ryan_RabidFluff_Denied")

def rabidFluffDenied1 = [id:1]
rabidFluffDenied1.npctext = "Oh. Heh. Nevermind. You're way too tough for a task like this. I'll ask someone else."
rabidFluffDenied1.flag = "!Z5_Ryan_RabidFluff_Denied"
rabidFluffDenied1.exec = { event ->
	event.player.centerPrint( "You must be level 5.0 or lower for this task." ) 
	event.player.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
}
rabidFluffDenied1.result = DONE
rabidFluffDenied.addDialog(rabidFluffDenied1, Ryan)

//---------------------------------------------------------
//To Believers - Connector                                 
//---------------------------------------------------------
def toBelievers = Ryan.createConversation("toBelievers", true, "!QuestStarted_331", "!QuestCompleted_331", "Z06_Ryan_ToBelievers_GrantAllowed")

def toBelievers1 = [id:1]
toBelievers1.npctext = "I do have another task for you. It requires a little more cunning than the tasks I've put to you before. Think you can manage it?"
toBelievers1.options = []
toBelievers1.options << [text:"I'm more than capable of subtlety and cunning.", result: 2]
toBelievers1.options << [text:"Nah, I'd much rather smash things!", result: 7]
toBelievers.addDialog(toBelievers1, Ryan)

def toBelievers2 = [id:2]
toBelievers2.npctext = "I knew that you were ready for this."
toBelievers2.playertext = "Thanks, so what's the problem?"
toBelievers2.result = 3
toBelievers.addDialog(toBelievers2, Ryan)

def toBelievers3 = [id:3]
toBelievers3.npctext = "The Barton Regulars have been receiving reports about these True Believers near the Old Aqueduct. Reports of citizens disappearing and being held against their will."
toBelievers3.playertext = "Really? That sounds bad. Let's takem out!"
toBelievers3.result = 4
toBelievers.addDialog(toBelievers3, Ryan)

def toBelievers4 = [id:4]
toBelievers4.npctext = "Not so fast, like I said, this will require some cunning. We can't just barge in there without proof!"
toBelievers4.playertext = "Ahh, so finding the proof is where you need my help, then?"
toBelievers4.result = 5
toBelievers.addDialog(toBelievers4, Ryan)

def toBelievers5 = [id:5]
toBelievers5.npctext = "You catch on quick. Yes, we can't move against them until we have confirmation of these rumors. Of course, they'd never talk to a Regular so I need someone to get inside, check them out, and let me know what's going on."
toBelievers5.options = []
toBelievers5.options << [text:"You can count on me!", result: 6]
toBelievers5.options << [text:"Let me think about it.", result: 7]
toBelievers.addDialog(toBelievers5, Ryan)

def toBelievers6 = [id:6]
toBelievers6.npctext = "I knew that I could."
toBelievers6.quest = 331
toBelievers6.flag = "!Z06_Ryan_ToBelievers_GrantAllowed"
toBelievers6.exec = { event ->
	event.player.addMiniMapQuestActorName("Tourist George-VQS")
}
toBelievers6.result = DONE
toBelievers.addDialog(toBelievers6, Ryan)

def toBelievers7 = [id:7]
toBelievers7.npctext = "If you change your mind..."
toBelievers7.playertext = "I know where to find you."
toBelievers7.flag = ["Z06_Ryan_ToBelievers_Break", "!Z06_Ryan_ToBelievers_GrantAllowed"]
toBelievers7.result = DONE
toBelievers.addDialog(toBelievers7, Ryan)

//---------------------------------------------------------
//To Believers - Break                                     
//---------------------------------------------------------
def toBelieversBreak = Ryan.createConversation("toBelieversBreak", true, "Z06_Ryan_ToBelievers_Break", "Z06_Ryan_ToBelievers_BreakAllowed")

def toBelieversBreak1 = [id:1]
toBelieversBreak1.npctext = "I do still need someone to take a look into the True Believers. Interested now?"
toBelieversBreak1.options = []
toBelieversBreak1.options << [text:"Yeah, I think I have time now.", result: 2]
toBelieversBreak1.options << [text:"Nah, not really.", result: 3]
toBelieversBreak.addDialog(toBelieversBreak1, Ryan)

def toBelieversBreak2 = [id:2]
toBelieversBreak2.npctext = "Excellent. Remember, we need to find out if there is any truth to the rumors regarding their connection to the disappearance of Bartonian citizens. Check them out in Old Aqueduct and let me know what you discover."
toBelieversBreak2.quest = 331
toBelieversBreak2.flag = ["!Z06_Ryan_ToBelievers_Break", "!Z06_Ryan_ToBelievers_BreakAllowed"]
toBelieversBreak2.exec = { event ->
	event.player.addMiniMapQuestActorName("Tourist George-VQS")
}
toBelieversBreak2.result = DONE
toBelieversBreak.addDialog(toBelieversBreak2, Ryan)

def toBelieversBreak3 = [id:3]
toBelieversBreak3.npctext = "Very well, then I have nothing for you."
toBelieversBreak3.flag = "!Z06_Ryan_ToBelievers_BreakAllowed"
toBelieversBreak3.result = DONE
toBelieversBreak.addDialog(toBelieversBreak3, Ryan)

//---------------------------------------------------------
//To Believers - Complete                                  
//---------------------------------------------------------
def toBelieversComplete = Ryan.createConversation("toBelieversComplete", true, "QuestStarted_331:4")

def toBelieversComplete1 = [id:1]
toBelieversComplete1.npctext = "Hello, %p. Have you learned anything?"
toBelieversComplete1.playertext = "Only that those Believers are nuts! Those crazy fools are just standing around waiting for the Zurg to swoop down and take them into outer-space!"
toBelieversComplete1.result = 2
toBelieversComplete.addDialog(toBelieversComplete1, Ryan)

def toBelieversComplete2 = [id:2]
toBelieversComplete2.npctext = "We don't need to worry about them then?"
toBelieversComplete2.playertext = "I don't think so. It seems like everyone who is there wants to be."
toBelieversComplete2.result = 3
toBelieversComplete.addDialog(toBelieversComplete2, Ryan)

def toBelieversComplete3 = [id:3]
toBelieversComplete3.npctext = "Hmm, I guess they were just vicious rumors then. Thanks."
toBelieversComplete3.quest = 331
toBelieversComplete3.exec = { event ->
	event.player.removeMiniMapQuestActorName("Ryan-VQS")
}
toBelieversComplete3.result = DONE
toBelieversComplete.addDialog(toBelieversComplete3, Ryan)

//---------------------------------------------------------
//To Believers - Denied                                    
//---------------------------------------------------------
def noOthers = Ryan.createConversation("noOthers", true, "Z06_Ryan_NoOthers")

def noOthers1 = [id:1]
noOthers1.npctext = "Sorry, I have nothing else for you."
noOthers1.flag = "!Z06_Ryan_NoOthers"
noOthers1.result = DONE
noOthers.addDialog(noOthers1, Ryan)