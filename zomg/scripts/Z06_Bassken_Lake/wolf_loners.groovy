import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Lone Wanderers                           
//------------------------------------------

def wanderOne = myRooms.BASSKEN_603.spawnSpawner( "wanderOne", "outlaw_pup", 1)
wanderOne.setPos( 300, 170 )
wanderOne.setWanderBehaviorForChildren( 50, 200, 4, 9, 250)
wanderOne.setCFH( 0 )
wanderOne.setWaitTime( 50, 70 )
wanderOne.setMonsterLevelForChildren( 4.2 )

def wanderTwo = myRooms.BASSKEN_604.spawnSpawner( "wanderTwo", "outlaw_pup", 1)
wanderTwo.setPos( 325, 150 )
wanderTwo.setWanderBehaviorForChildren( 50, 150, 4, 9, 200)
wanderTwo.setCFH( 0 )
wanderTwo.setWaitTime( 50, 70 )
wanderTwo.setMonsterLevelForChildren( 4.3 )

def wanderThree = myRooms.BASSKEN_504.spawnSpawner( "wanderThree", "outlaw_pup", 1)
wanderThree.setPos( 820, 275 )
wanderThree.setWanderBehaviorForChildren( 50, 200, 4, 9, 250)
wanderThree.setCFH( 0 )
wanderThree.setWaitTime( 50, 70 )
wanderThree.setMonsterLevelForChildren( 4.4 )

def wanderFour = myRooms.BASSKEN_505.spawnSpawner( "wanderFour", "outlaw_pup", 1)
wanderFour.setPos( 430, 130 )
wanderFour.setWanderBehaviorForChildren( 50, 200, 4, 9, 250)
wanderFour.setCFH( 0 )
wanderFour.setWaitTime( 50, 70 )
wanderFour.setMonsterLevelForChildren( 4.3 )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

wanderOne.forceSpawnNow()
wanderTwo.forceSpawnNow()
wanderThree.forceSpawnNow()
wanderFour.forceSpawnNow()
