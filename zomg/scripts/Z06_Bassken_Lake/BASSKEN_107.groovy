import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.BASSKEN_107.spawnSpawner("spawner1", "grass_fluff", 2) 
spawner1.setPos(370, 300)
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 5, 300)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 4.5 )

//STARTUP LOGIC
spawner1.spawnAllNow()