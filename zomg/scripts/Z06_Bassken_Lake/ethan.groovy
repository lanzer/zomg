import com.gaiaonline.mmo.battle.script.*;

def Ethan = spawnNPC("Ethan-VQS", myRooms.BASSKEN_6, 850, 470)
Ethan.setDisplayName( "Ethan" )

//---------------------------------------------------------
// Ethan is on Beach Patrol. (Default Convo)               
//---------------------------------------------------------

def EthanDefault = Ethan.createConversation("EthanDefault", true, "!Z6EthanPissed")

def Default1 = [id:1]
Default1.npctext = "You headed out to the beach?"
Default1.options = []
Default1.options << [text:"I sure am. Who's asking?", result: 2]
Default1.options << [text:"That's none of your business!", result: 8]
Default1.options << [text:"Nope. Just investigating the area.", result: 6]
EthanDefault.addDialog(Default1, Ethan)

def Default2 = [id:2]
Default2.npctext = "Oh! I'm Ethan. Sorry. I just didn't want you heading out there without warning. It's pretty wild out there now."
Default2.playertext = "You mean...even more than around here?"
Default2.result = 3
EthanDefault.addDialog(Default2, Ethan)

def Default3 = [id:3]
Default3.npctext = "Yeah. Believe it or not, it's *crazy* out there and it gets worse the farther north and west you work your way up the coast."
Default3.playertext = "What kinds of craziness should I expect?"
Default3.result = 4
EthanDefault.addDialog(Default3, Ethan)

def Default4 = [id:4]
Default4.npctext = "Well...this is about as far as Leon lets us guards go right now, so I'm getting most of this info second-hand from the construction workers up by the boardwalk...but they say the ocean itself is coming alive and the sand is crawling with danger."
Default4.playertext = "Hmmm...seems far-fetched. But what doesn't sound like that these days?"
Default4.result = 5
EthanDefault.addDialog(Default4, Ethan)

def Default5 = [id:5]
Default5.npctext = "heh. You got that right! Well...if you're heading out there...watch yourself. Even the Pirates have holed up in their ships. No one's going out there unless they have to do so."
Default5.playertext = "Okay. Gotcha. It's dangerous. See ya."
Default5.result = DONE
EthanDefault.addDialog(Default5, Ethan)

def Default6 = [id:6]
Default6.npctext = "Ah, gotcha. Well...if you go any further than this, you'll be headed to the old Boardwalk the pirates bought from Gambino. So you might want to turn around if you wanted to stay at Bass'ken."
Default6.playertext = "Okay. Thanks! I was just about to overshoot then. Much appreciated!"
Default6.result = 7
EthanDefault.addDialog(Default6, Ethan)

def Default7 = [id:7]
Default7.npctext = "No problem. See ya. Hey...have you heard about the Lake Monster?"
Default7.playertext = "Lake Monster? What?!?"
Default7.result = 9
EthanDefault.addDialog(Default7, Ethan)

def Default8 = [id:8]
Default8.npctext = "It's not, is it? Okay, fine. You're on your own...citizen. See if you get any help from me!"
Default8.flag = "Z6EthanPissed"
Default8.result = DONE
EthanDefault.addDialog(Default8, Ethan)

def Default9 = [id:9]
Default9.npctext = "We keep hearing reports of some huge sea monster that must have swum up into the lake through tunnels connected to the sea!"
Default9.playertext = "Have you seen it?"
Default9.result = 10
EthanDefault.addDialog(Default9, Ethan)

def Default10 = [id:10]
Default10.npctext = "Nope. I don't know anyone that has. But there are several people that swear they have good friends that have seen it!"
Default10.playertext = "Sounds pretty fishy to me."
Default10.result = 11
EthanDefault.addDialog(Default10, Ethan)

def Default11 = [id:11]
Default11.npctext = "...is that a pun?"
Default11.playertext = "hehehe. No. I just meant it didn't seem very believable."
Default11.result = 12
EthanDefault.addDialog(Default11, Ethan)

def Default12 = [id:12]
Default12.npctext = "Well, I'm one of the people that knows someone who saw it. My brother swears he saw it and that's good enough for me!"
Default12.playertext = "Oh yeah? What did your brother see, exactly?"
Default12.result = 13
EthanDefault.addDialog(Default12, Ethan)

def Default13 = [id:13]
Default13.npctext = "He says a giant shadowy form came out of the depths, broke through the water's surface, blasted water and air into the sky with a huge trumpeting noise, and then vanished into the depths again."
Default13.playertext = "Giant, you say? How giant?"
Default13.result = 14
EthanDefault.addDialog(Default13, Ethan)

def Default14 = [id:14]
Default14.npctext = "As long as one of the piers and as broad as a house!"
Default14.playertext = "Well...sure. Why not? Everything else seems believable today."
Default14.result = 15
EthanDefault.addDialog(Default14, Ethan)

def Default15 = [id:15]
Default15.npctext = "No! Bessie's been around for a long time!"
Default15.playertext = "Bessie? You *named* the lake monster?"
Default15.result = 16
EthanDefault.addDialog(Default15, Ethan)

def Default16 = [id:16]
Default16.npctext = "Well...I...errr...yes?"
Default16.playertext = "Wait a minute...you don't even *have* a brother, do you? YOU were the one that saw the creature!"
Default16.result = 17
EthanDefault.addDialog(Default16, Ethan)

def Default17 = [id:17]
Default17.npctext = "No! I mean...NO! I can't have seen it. Leon will rake me over the coals if he...I mean...NO...I didn't see it and you can't prove I said anything. Go away!"
Default17.flag = "Z6EthanPissed"
Default17.result = DONE
EthanDefault.addDialog(Default17, Ethan)



def EthanPissed = Ethan.createConversation("EthanPissed", true, "Z6EthanPissed")

def Pissed1 = [id:1]
Pissed1.npctext = "...*scowl*..."
Pissed1.playertext = "Hey, Ethan..."
Pissed1.options = []
Pissed1.options << [text:"...I'm sorry, man. I didn't mean to be so hasty and curt.", result: 2]
Pissed1.options << [text:"...you sure know how to look ticked off!", result: 4]
EthanPissed.addDialog(Pissed1, Ethan)

def Pissed2 = [id:2]
Pissed2.npctext = "Yeah?"
Pissed2.playertext = "Yeah."
Pissed2.result = 3
EthanPissed.addDialog(Pissed2, Ethan)

def Pissed3 = [id:3]
Pissed3.npctext = "Well...all right. I guess that's all right then."
Pissed3.flag = "!Z6EthanPissed"
Pissed3.result = DONE
EthanPissed.addDialog(Pissed3, Ethan)

def Pissed4 = [id:4]
Pissed4.npctext = "What?!? That's...that's...*splutter*...fine. Go have fun on the beach. Don't cry for your mommy when you get your butt kicked. We're done here!"
Pissed4.result = DONE
EthanPissed.addDialog(Pissed4, Ethan)