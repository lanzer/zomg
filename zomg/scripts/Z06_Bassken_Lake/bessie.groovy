import com.gaiaonline.mmo.battle.script.*;

//=============================================
// BESSIE TRIGGER ZONES                        
//=============================================

def bassieOneTrigger = "bassieOneTrigger"
myRooms.BASSKEN_205.createTriggerZone(bassieOneTrigger, 300, 230, 900, 410)

def bassieTwoTrigger = "bassieTwoTrigger"
myRooms.BASSKEN_204.createTriggerZone(bassieTwoTrigger, 70, 10, 330, 240)

bassie1 = makeSwitch( "bessie1", myRooms.BASSKEN_205, 300, 525 )
bassie2 = makeSwitch( "bessie2", myRooms.BASSKEN_204, 655, 285 )

bassie1.off()
bassie2.off()

bassie1Ready = true
bassie2Ready = true

delay = 30

//==============================================
// PLAYER LISTS                                 
//==============================================

playerList205 = []
playerList204 = []

//Room 205 (Bassie 1)
myManager.onEnter( myRooms.BASSKEN_205 ) { event ->
	if( isPlayer(event.actor) ) {
		playerList205 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_205 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList205.remove( event.actor )
	}
}

//Room 204 (Bassie 2)
myManager.onEnter( myRooms.BASSKEN_204 ) { event ->
	if( isPlayer(event.actor) ) {
		playerList204 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_204 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList204.remove( event.actor )
	}
}

//==============================================
// BESSIE 1 LOGIC (ROOM 205)                    
//==============================================

myManager.onTriggerIn( myRooms.BASSKEN_205, bassieOneTrigger ) { event ->
	if( isPlayer(event.actor) && bassie1Ready ) {
		bassie1Ready = false
		myManager.schedule( delay ) { resetBassie1() }
		tryToSeeBassie1( event )
	}
}

def tryToSeeBassie1( event ) {
	roll = random( 1, 100 )
	if( roll <= 1 ) {
		bassie1.on()
		sound( "bessie1Sound" ).toZone()
		event.actor.setQuestFlag( GLOBAL, "triedToSeeBassie1")
		myManager.schedule(25) { bassie1.off() }
	}
}

def resetBassie1() {
	if( playerList205.isEmpty() ) {
		bassie1Ready = true
	} else {
		myManager.schedule( delay ) { resetBassie1() }
	}
}

//==============================================
// BESSIE 2 LOGIC (ROOM 204)                    
//==============================================

myManager.onTriggerIn( myRooms.BASSKEN_204, bassieTwoTrigger ) { event ->
	if( isPlayer(event.actor) && bassie2Ready ) {
		bassie2Ready = false
		myManager.schedule( delay ) { resetBassie2() }
		tryToSeeBassie2( event )
	}
}

def tryToSeeBassie2( event ) {
	roll = random( 1, 100 )
	if( roll <= 1 ) {
		bassie2.on()
		sound( "bessie2Sound" ).toZone()
		event.actor.setQuestFlag( GLOBAL, "triedToSeeBassie2")
		myManager.schedule(25) { bassie2.off() }
	}
}

def resetBassie2() {
	if( playerList204.isEmpty() ) {
		bassie2Ready = true
	} else {
		myManager.schedule( delay ) { resetBassie2() }
	}
}

