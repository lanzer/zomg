//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def dateRockUpdater = "dateRockUpdater"
myRooms.BASSKEN_402.createTriggerZone(dateRockUpdater, 320, 80, 620, 325)
myManager.onTriggerIn(myRooms.BASSKEN_402, dateRockUpdater) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(225)) {
		event.actor.updateQuest(225, "Story-VQS")
	}
}