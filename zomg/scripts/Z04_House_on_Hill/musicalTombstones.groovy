import com.gaiaonline.mmo.battle.script.*;

//Song list
maryHadALittleLamb = [ "B", "A", "G", "A", "B", "B", "B", "A", "A", "A", "B", "D", "D", "B", "A", "G", "A", "B", "B", "B", "B", "A", "A", "B", "A", "G"]
camptownRaces = ["C", "C", "A", "C", "D", "C", "A", "A", "G", "A", "G", "C", "C", "A", "C", "D", "C", "A", "G", "A", "G", "F"]
whenTheSaintsGoMarchingIn = ["G", "B", "C", "D", "G", "B", "C", "D", "G", "B", "C", "D", "B", "G", "B", "A", "B", "A", "G", "G", "B", "D", "D", "C", "B", "C", "D", "B", "G", "A", "G"]
twinkleTwinkleLittleStar = ["G", "G", "D", "D", "E", "E", "D", "C", "C", "B", "B", "A", "A", "G", "D", "D", "C", "C", "B", "B", "A", "D", "D", "C", "C", "B", "B", "A", "G", "G", "D", "D", "E", "E", "D", "C", "C", "B", "B", "A", "A", "G"]
scale = ["C", "D", "E", "F", "G", "A", "B", "C2"]
funeralMarch = ["C", "C", "C", "C", "E", "D", "D", "C", "C", "C", "C"]
beethovens5 = ["G", "G", "G", "E", "F", "F", "F", "D", "G", "G", "G", "E", "B", "B", "B", "A", "C2", "C2", "C2", "B"]
theWorms = ["D", "E", "D", "E", "D", "E", "D", "E", "D", "E", "G", "G", "F", "E", "D", "E", "G"]
playerSong = []

//Define switches
tombstoneC = makeSwitch( "tombstoneC", myRooms.M1_305, 160, 425 )
tombstoneD = makeSwitch( "tombstoneD", myRooms.M1_305, 280, 470 )
tombstoneE = makeSwitch( "tombstoneE", myRooms.M1_305, 350, 360 )
tombstoneF = makeSwitch( "tombstoneF", myRooms.M1_305, 455, 425 )
tombstoneG = makeSwitch( "tombstoneG", myRooms.M1_305, 850, 520 )
tombstoneA = makeSwitch( "tombstoneA", myRooms.M1_305, 885, 640 )
tombstoneB = makeSwitch( "tombstoneB", myRooms.M1_305, 930, 375 )
tombstoneC2 = makeSwitch( "tombstoneC2", myRooms.M1_305, 990, 570 )

//Unlock switches
tombstoneC.unlock()
tombstoneD.unlock()
tombstoneE.unlock()
tombstoneF.unlock()
tombstoneG.unlock()
tombstoneA.unlock()
tombstoneB.unlock()
tombstoneC2.unlock()

//Set range for switches
tombstoneC.setRange( 250 )
tombstoneD.setRange( 250 )
tombstoneE.setRange( 250 )
tombstoneF.setRange( 250 )
tombstoneG.setRange( 250 )
tombstoneA.setRange( 250 )
tombstoneB.setRange( 250 )
tombstoneC2.setRange( 250 )

//Note definitions
//C Note
def shriekC = { event ->
	sound("shriekC").toZone()
	tombstoneC.lock()
	myManager.schedule(1) { 
		tombstoneC.unlock()
		tombstoneC.off() 
	}
	playerSong << "C"
	songCheck(event)
}

//D Note
def shriekD = { event ->
	sound("shriekD").toZone()
	tombstoneD.lock()
	myManager.schedule(1) { 
		tombstoneD.unlock()
		tombstoneD.off() 
	}
	playerSong << "D"
	songCheck(event)
}

//E Note
def shriekE = { event ->
	sound("shriekE").toZone()
	tombstoneE.lock()
	myManager.schedule(1) { 
		tombstoneE.unlock()
		tombstoneE.off() 
	}
	playerSong << "E"
	songCheck(event)
}

//F Note
def shriekF = { event ->
	sound("shriekF").toZone()
	tombstoneF.lock()
	myManager.schedule(1) { 
		tombstoneF.unlock()
		tombstoneF.off() 
	}
	playerSong << "F"
	songCheck(event)
}

//G Note
def shriekG = { event ->
	sound("shriekG").toZone()
	tombstoneG.lock()
	myManager.schedule(1) { 
		tombstoneG.unlock()
		tombstoneG.off() 
	}
	playerSong << "G"
	songCheck(event)
}

//A Note
def shriekA = { event ->
	sound("shriekA").toZone()
	tombstoneA.lock()
	myManager.schedule(1) { 
		tombstoneA.unlock()
		tombstoneA.off() 
	}
	playerSong << "A"
	songCheck(event)
}

//B Note
def shriekB = { event ->
	sound("shriekB").toZone()
	tombstoneB.lock()
	myManager.schedule(1) { 
		tombstoneB.unlock()
		tombstoneB.off() 
	}
	playerSong << "B"
	songCheck(event)
}

//C2 Note
def shriekC2 = { event ->
	sound("shriekC2").toZone()
	tombstoneC2.lock()
	myManager.schedule(1) { 
		tombstoneC2.unlock()
		tombstoneC2.off() 
	}
	playerSong << "C2"
	songCheck(event)
}


//This is an updater for the Hallowed Ground badge (quest 246)
def hallowedUpdater = "hallowedUpdater"
myRooms.M1_305.createTriggerZone(hallowedUpdater, 185, 540, 1010, 640)
myManager.onEnter(myRooms.M1_305) { event ->
	if(isPlayer(event.actor)) {
		if(!event.actor.isDoneQuest(246)) {
			event.actor.updateQuest(246, "Story-VQS")
			if(!event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Scale") && !event.actor.hasQuestFlag(GLOBAL, "Z4_Song_FuneralMarch") && !event.actor.hasQuestFlag(GLOBAL, "Z4_Song_TheWorms") && !event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Beethoven5") && !event.actor.hasQuestFlag(GLOBAL, "Z4_Song_CamptownRaces") && !event.actor.hasQuestFlag(GLOBAL, "Z4_Song_LittleLamb") && !event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Saints") && !event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Twinkle")) {
				event.actor.setQuestFlag(GLOBAL, "Z4_Song_None")
				tutorialNPC.pushDialog( event.actor, "tombstones" )
			}
		}
	}
}

//Conversation for Musical Tombstone placard
tombstones = tutorialNPC.createConversation("tombstones", true, "Z4_Song_None")

tombstones1 = [id:1]
tombstones1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/tombstonegame.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
tombstones1.flag = "!Z4_Song_None"
tombstones1.result = DONE
tombstones.addDialog(tombstones1, tutorialNPC)

//This is an updater for the Virtuoso badge (quest 248) - it runs whenever a player successfully players a song so long as they have not completed the Virtuoso badge quest.
//It updates the quest if they have been flagged as playing all 7 songs.
def checkForVirtuoso( event ) {
	/*if(event.actor.hasQuestFlag(GLOBAL, "Z4_Song_None")) {
		
	}*/
	if(!event.actor.isDoneQuest(248) && event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Scale") && event.actor.hasQuestFlag(GLOBAL, "Z4_Song_FuneralMarch") && event.actor.hasQuestFlag(GLOBAL, "Z4_Song_TheWorms") && event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Beethoven5") && event.actor.hasQuestFlag(GLOBAL, "Z4_Song_CamptownRaces") && event.actor.hasQuestFlag(GLOBAL, "Z4_Song_LittleLamb") && event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Saints") && event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Twinkle")) {
		event.actor.updateQuest(248, "Story-VQS")
	}
}

//This checks that the note the player just played is the correct note for a song, otherwise is returns failure.
//It also handles rewards for the first time the player plays a song.
def songCheck(event) { 
	noteCount = playerSong.size - 1
	//println "*** DEBUG*** noteCount == ${noteCount}"
	if(noteCount < 8 && playerSong.get(noteCount) == scale.get(noteCount)) {
		//event.actor.centerPrint( "You got it!" ) //debug message
		if(playerSong == scale) {
			event.actor.centerPrint( "Congratulations, you've successfully played a scale! Now try something more complex." )
			playerSong.clear()
			if(!event.actor.isDoneQuest(247)) {
				event.actor.updateQuest(247, "Story-VQS")
			}
			if(!event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Scale")) {
				event.actor.setQuestFlag(GLOBAL, "Z4_Song_Scale")
			}
			checkForVirtuoso( event )
		} 
	} else if(noteCount < 11 && playerSong.get(noteCount) == funeralMarch.get(noteCount)) {
		//event.actor.centerPrint( "You got it!" ) //debug message
		if(playerSong == funeralMarch) {
			event.actor.centerPrint( "Congratulations, you've successfully played Funeral March!" )
			playerSong.clear()
			if(!event.actor.isDoneQuest(247)) {
				event.actor.updateQuest(247, "Story-VQS")
			}
			if(!event.actor.hasQuestFlag(GLOBAL, "Z4_Song_FuneralMarch")) {
				event.actor.setQuestFlag(GLOBAL, "Z4_Song_FuneralMarch")
			}
			checkForVirtuoso( event )
		} 
	} else if(noteCount < 17 && playerSong.get(noteCount) == theWorms.get(noteCount)) {
		//event.actor.centerPrint( "You got it!" ) //debug message
		if(playerSong == theWorms) {
			event.actor.centerPrint( "Congratulations, you've successfully played The Worms!" )
			playerSong.clear()
			if(!event.actor.isDoneQuest(247)) {
				event.actor.updateQuest(247, "Story-VQS")
			}
			if(!event.actor.hasQuestFlag(GLOBAL, "Z4_Song_TheWorms")) {
				event.actor.setQuestFlag(GLOBAL, "Z4_Song_TheWorms")
			}
			checkForVirtuoso( event )
		} 
	} else if(noteCount < 20 && playerSong.get(noteCount) == beethovens5.get(noteCount)) {
		//event.actor.centerPrint( "You got it!" ) //debug message
		if(playerSong == beethovens5) {
			event.actor.centerPrint( "Congratulations, you've successfully played Beethoven's 5th Symphony!" )
			playerSong.clear()
			if(!event.actor.isDoneQuest(247)) {
				event.actor.updateQuest(247, "Story-VQS")
			}
			if(!event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Beethoven5")) {
				event.actor.setQuestFlag(GLOBAL, "Z4_Song_Beethoven5")
			}
			checkForVirtuoso( event )
		} 
	} else if(noteCount < 22 && playerSong.get(noteCount) == camptownRaces.get(noteCount)) {
		//event.actor.centerPrint( "You got it!" ) //debug message
		if(playerSong == camptownRaces) {
			event.actor.centerPrint( "Congratulations, you've successfully played Camptown Races!" )
			playerSong.clear()
			if(!event.actor.isDoneQuest(247)) {
				event.actor.updateQuest(247, "Story-VQS")
			}
			if(!event.actor.hasQuestFlag(GLOBAL, "Z4_Song_CamptownRaces")) {
				event.actor.setQuestFlag(GLOBAL, "Z4_Song_CamptownRaces")
			}
			checkForVirtuoso( event )
		} 
	} else if(noteCount < 26 && playerSong.get(noteCount) == maryHadALittleLamb.get(noteCount)) {
		//event.actor.centerPrint( "You got it!" ) //debug message
		if(playerSong == maryHadALittleLamb) {
			event.actor.centerPrint( "Congratulations, you've successfully played Mary Had A Little Lamb!" )
			playerSong.clear()
			if(!event.actor.isDoneQuest(247)) {
				event.actor.updateQuest(247, "Story-VQS")
			}
			if(!event.actor.hasQuestFlag(GLOBAL, "Z4_Song_LittleLamb")) {
				event.actor.setQuestFlag(GLOBAL, "Z4_Song_LittleLamb")
			}
			checkForVirtuoso( event )
		}
	} else if(noteCount < 31 && playerSong.get(noteCount) == whenTheSaintsGoMarchingIn.get(noteCount)) {
		//event.actor.centerPrint( "You got it!" ) //debug message
		if(playerSong == whenTheSaintsGoMarchingIn) {
			event.actor.centerPrint( "Congratulations, you've successfully played When The Saints Go Marching In!" )
			playerSong.clear()
			if(!event.actor.isDoneQuest(247)) {
				event.actor.updateQuest(247, "Story-VQS")
			}
			if(!event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Saints")) {
				event.actor.setQuestFlag(GLOBAL, "Z4_Song_Saints")
			}
			checkForVirtuoso( event )
		} 
	} else if(noteCount < 42 && playerSong.get(noteCount) == twinkleTwinkleLittleStar.get(noteCount)) {
		//event.actor.centerPrint( "You got it!" ) //debug message
		if(playerSong == twinkleTwinkleLittleStar) {
			event.actor.centerPrint( "Congratulations, you've successfully played Twinkle, Twinkle Little Star!" )
			playerSong.clear()
			if(!event.actor.isDoneQuest(247)) {
				event.actor.updateQuest(247, "Story-VQS")
			}
			if(!event.actor.hasQuestFlag(GLOBAL, "Z4_Song_Twinkle")) {
				event.actor.setQuestFlag(GLOBAL, "Z4_Song_Twinkle")
			}
			checkForVirtuoso( event )
		} 
	} else {
		event.actor.centerPrint( "That doesn't sound right..." ) //Oops you suck at musics :(
		playerSong.clear()
	}
}

//Switch activation
tombstoneC.whenOn( shriekC )
tombstoneD.whenOn( shriekD )
tombstoneE.whenOn( shriekE )
tombstoneF.whenOn( shriekF )
tombstoneG.whenOn( shriekG )
tombstoneA.whenOn( shriekA )
tombstoneB.whenOn( shriekB )
tombstoneC2.whenOn( shriekC2 )