//Script created by gfern

import spawner.createSpawner;
import difficulty.setLevel;

//-----------------------------------------------------
//Constants & Variable Init
//-----------------------------------------------------
level = 10;
underDifficulty = 1;
ltCounter = 0;
areaInit = false

HATE_RADIUS = 1000;
MIN_LEVEL = 12;
MAX_LEVEL = 12;
MIN_LEVEL_LT = 11.5;
MAX_LEVEL_LT = 11.5;
LT_MAX = 10;

//-----------------------------------------------------
//Maps and lists
//-----------------------------------------------------
underSpawnerList = [] as Set;
ltSpawnerRoomList = [] as Set;
underSpawnerListLT = [] as Set;
underMountainMonsters = [
	[name:"vampire_male_um",    level:12, spawner_list:underSpawnerList,	quantity:1],
	[name:"vampire_female_um",  level:12, spawner_list:underSpawnerList,	quantity:1],
	[name:"werewolf_um",        level:12, spawner_list:underSpawnerList,	quantity:1],
	[name:"blood_bat", 	        level:12, spawner_list:underSpawnerList,	quantity:4],
];
underMountainLTMonsters = [
	[name:"werewolf_LT_um",         level:12, spawner_list:underSpawnerListLT,	quantity:1],
	[name:"vampire_batmonster_um",  level:12, spawner_list:underSpawnerListLT,	quantity:1]
];

difficultyMap = [1:0, 2:0.5, 3:1.0];

doNotSpawnLTsInRooms = [];

//-----------------------------------------------------
//Level adjusting
//-----------------------------------------------------
l = new setLevel();

def checkLevel() {
	if(myArea.getAllPlayers().size() > 0) {
		player = random(myArea.getAllPlayers());
		underDifficulty = player.getTeam().getAreaVar("Z30_Undermountain", "Z30DeadmansDifficulty");
		if(!underDifficulty) { underDifficulty = 2 }

		player.getCrew().each() {
			if(it.getConLevel() + difficultyMap.get(underDifficulty) > level) {
				levelAdjust();
			}
		}
	}

	myManager.schedule(30) { checkLevel(); }
}

def levelAdjust() {
	level = l.setSpawnerLevel(MIN_LEVEL_LT, MAX_LEVEL_LT, underDifficulty, difficultyMap, player, underSpawnerListLT);
	level = l.setSpawnerLevel(MIN_LEVEL, MAX_LEVEL, underDifficulty, difficultyMap, player, underSpawnerList);
}

//---------------------------------------------------
// Room activator
//---------------------------------------------------

activatedRooms = [] as Set
activationTasks = [:]
def activateRoom(room) {
	if(! activatedRooms.contains(room)) {
		log("Activating {}", room)
		activatedRooms << room
		myManager.stopListenForEnterExit(room) // [rc] Manage this separately if we ever add another onEnter to this room
		if(activationTasks.containsKey(room)) {
			activationTasks[room].cancel()
		}
		room.getActorList().each {
			if(isMonster(it)) {
				it.setAwarenessArc(360)
				it.getAI().setMiniEventSpec(null)
			}
		}
	}
	else {
		log("Room {} already activated", room)
	}
}

attackActivator = [
isAllowed : { attacker, target ->
	if(isMonster(target)) {
		activateRoom(target.getRoom())
	} else {
		log("ERROR! Got mini event spec check for {} by {}", target, attacker)
	}
	return true;
}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec;


//-----------------------------------------------------
//Spawners
//-----------------------------------------------------

creatorOfSpawners = new createSpawner();

//Under 2
//Guards 5
spawner5_1 = randomStoppedGuardSpawner(myRooms.Under2_5, "spawner5_1", 1, 390, 370, "Under2_5", 90);
spawner5_2 = randomStoppedGuardSpawner(myRooms.Under2_5, "spawner5_2", 1, 440, 620, "Under2_5", 240);
spawner5_3 = randomStoppedGuardSpawner(myRooms.Under2_5, "spawner5_3", 1, 820, 770, "Under2_5", 90);
spawner5_4 = randomStoppedGuardSpawner(myRooms.Under2_5, "spawner5_4", 1, 1090, 350, "Under2_5", 120);
spawner5_5 = randomStoppedGuardSpawner(myRooms.Under2_5, "spawner5_5", 1, 870, 330, "Under2_5", 90);

//Guards 7
spawner7_1 = randomStoppedGuardSpawner(myRooms.Under2_7, "spawner7_1", 1, 800, 380, "Under2_7", 90);
spawner7_2 = randomStoppedGuardSpawner(myRooms.Under2_7, "spawner7_2", 1, 620, 700, "Under2_7", 90);
spawner7_3 = randomStoppedGuardSpawner(myRooms.Under2_7, "spawner7_3", 1, 1100, 600, "Under2_7", 120);

//Patrollers 7
spawner7_4 = randomStoppedSpawner(myRooms.Under2_7, "spawner7_4", 1, 1090, 320, "Under2_7");
spawner7_4.addPatrolPointForChildren("Under2_7", 980, 400, 0);
spawner7_4.addPatrolPointForChildren("Under2_7", 820, 530, 10);
spawner7_4.addPatrolPointForChildren("Under2_7", 980, 400, 0);
spawner7_4.addPatrolPointForChildren("Under2_7", 1090, 320, 10);

spawner7_5 = randomStoppedSpawner(myRooms.Under2_7, "spawner7_5", 1, 570, 780, "Under2_7");
spawner7_5.addPatrolPointForChildren("Under2_7", 880, 740, 0);
spawner7_5.addPatrolPointForChildren("Under2_7", 1010, 670, 10);
spawner7_5.addPatrolPointForChildren("Under2_7", 880, 740, 0);
spawner7_5.addPatrolPointForChildren("Under2_7", 570, 780, 10);

//Guards 203
spawner203_1 = randomStoppedGuardSpawner(myRooms.Under2_203, "spawner203_1", 1, 1230, 450, "Under2_203", 120);
spawner203_2 = randomStoppedGuardSpawner(myRooms.Under2_203, "spawner203_2", 1, 940, 760, "Under2_203", 120);
spawner203_3 = randomStoppedGuardSpawner(myRooms.Under2_203, "spawner203_3", 1, 560, 690, "Under2_203", 350);
spawner203_4 = randomStoppedGuardSpawner(myRooms.Under2_203, "spawner203_4", 1, 1020, 270, "Under2_203", 90);

//Roamers 203
spawner203_5 = randomStoppedSpawner(myRooms.Under2_203, "spawner203_5", 1, 570, 350, "Under2_203");
spawner203_5.addPatrolPointForChildren("Under2_203", 900, 360, 0);
spawner203_5.addPatrolPointForChildren("Under2_203", 1100, 480, 10);
spawner203_5.addPatrolPointForChildren("Under2_203", 900, 360, 0);
spawner203_5.addPatrolPointForChildren("Under2_203", 570, 350, 10);

//Guards 205
spawner205_1 = randomStoppedGuardSpawner(myRooms.Under2_205, "spawner205_1", 1, 300, 500, "Under2_205", 90);
spawner205_2 = randomStoppedGuardSpawner(myRooms.Under2_205, "spawner205_2", 1, 840, 360, "Under2_205", 350);
spawner205_3 = randomStoppedGuardSpawner(myRooms.Under2_205, "spawner205_3", 1, 1110, 700, "Under2_205", 90);
spawner205_4 = randomStoppedGuardSpawner(myRooms.Under2_205, "spawner205_4", 1, 710, 860, "Under2_205", 350);
spawner205_5 = randomStoppedGuardSpawner(myRooms.Under2_205, "spawner205_5", 1, 760, 570, "Under2_205", 90);
spawner205_6 = randomStoppedGuardSpawner(myRooms.Under2_205, "spawner205_6", 1, 1140, 320, "Under2_205", 120);

//Guards 207
spawner207_1 = randomStoppedGuardSpawner(myRooms.Under2_207, "spawner207_1", 1, 620, 320, "Under2_207", 90);
spawner207_2 = randomStoppedGuardSpawner(myRooms.Under2_207, "spawner207_2", 1, 1100, 540, "Under2_207", 120);
spawner207_3 = randomStoppedGuardSpawner(myRooms.Under2_207, "spawner207_3", 1, 680, 840, "Under2_207", 350);
spawner207_4 = randomStoppedGuardSpawner(myRooms.Under2_207, "spawner207_4", 1, 360, 530, "Under2_207", 90);

//Roamers 207
spawner207_5 = randomStoppedSpawner(myRooms.Under2_207, "spawner207_5", 1, 420, 610, "Under2_207");
spawner207_5.addPatrolPointForChildren("Under2_207", 590, 430, 0);
spawner207_5.addPatrolPointForChildren("Under2_207", 790, 390, 10);
spawner207_5.addPatrolPointForChildren("Under2_207", 590, 430, 0);
spawner207_5.addPatrolPointForChildren("Under2_207", 420, 610, 10);

spawner207_6 = randomStoppedSpawner(myRooms.Under2_207, "spawner207_6", 1, 1190, 690, "Under2_207");
spawner207_6.addPatrolPointForChildren("Under2_207", 1020, 790, 0);
spawner207_6.addPatrolPointForChildren("Under2_207", 770, 920, 10);
spawner207_6.addPatrolPointForChildren("Under2_207", 1020, 790, 0);
spawner207_6.addPatrolPointForChildren("Under2_207", 1190, 690, 10);

//Guards 401
spawner401_1 = randomStoppedGuardSpawner(myRooms.Under2_401, "spawner401_1", 1, 320, 450, "Under2_401", 350);
spawner401_2 = randomStoppedGuardSpawner(myRooms.Under2_401, "spawner401_2", 1, 970, 440, "Under2_401", 120);
spawner401_3 = randomStoppedGuardSpawner(myRooms.Under2_401, "spawner401_3", 1, 1070, 730, "Under2_401", 240);
spawner401_4 = randomStoppedGuardSpawner(myRooms.Under2_401, "spawner401_4", 1, 820, 670, "Under2_401", 90);
spawner401_5 = randomStoppedGuardSpawner(myRooms.Under2_401, "spawner401_5", 1, 410, 750, "Under2_401", 350);

//Roamers 401
spawner401_6 = randomStoppedSpawner(myRooms.Under2_401, "spawner401_6", 1, 450, 300, "Under2_401");
spawner401_6.addPatrolPointForChildren("Under2_401", 590, 420, 0);
spawner401_6.addPatrolPointForChildren("Under2_401", 440, 590, 10);
spawner401_6.addPatrolPointForChildren("Under2_401", 590, 420, 0);
spawner401_6.addPatrolPointForChildren("Under2_401", 450, 300, 10);

//Guards 403
spawner403_1 = randomStoppedGuardSpawner(myRooms.Under2_403, "spawner403_1", 1, 370, 670, "Under2_403", 350);
spawner403_2 = randomStoppedGuardSpawner(myRooms.Under2_403, "spawner403_2", 1, 1210, 520, "Under2_403", 120);
spawner403_3 = randomStoppedGuardSpawner(myRooms.Under2_403, "spawner403_3", 1, 590, 300, "Under2_403", 240);
spawner403_4 = randomStoppedGuardSpawner(myRooms.Under2_403, "spawner403_4", 1, 1060, 690, "Under2_403", 90);

//Roamers 403
spawner403_5 = randomStoppedSpawner(myRooms.Under2_403, "spawner403_5", 1, 400, 460, "Under2_403");
spawner403_5.addPatrolPointForChildren("Under2_403", 540, 780, 0);
spawner403_5.addPatrolPointForChildren("Under2_403", 1110, 720, 10);
spawner403_5.addPatrolPointForChildren("Under2_403", 540, 780, 0);
spawner403_5.addPatrolPointForChildren("Under2_403", 400, 460, 10);

//Guards 405
spawner405_1 = randomStoppedGuardSpawner(myRooms.Under2_405, "spawner405_1", 1, 960, 220, "Under2_405", 90);
spawner405_2 = randomStoppedGuardSpawner(myRooms.Under2_405, "spawner405_2", 1, 1480, 330, "Under2_405", 120);
spawner405_3 = randomStoppedGuardSpawner(myRooms.Under2_405, "spawner405_3", 1, 840, 630, "Under2_405", 90);
spawner405_4 = randomStoppedGuardSpawner(myRooms.Under2_405, "spawner405_4", 1, 580, 810, "Under2_405", 350);

//Patrollers 405
spawner405_5 = randomStoppedSpawner(myRooms.Under2_405, "spawner405_5", 1, 1210, 790, "Under2_405");
spawner405_5.addPatrolPointForChildren("Under2_405", 880, 690, 0);
spawner405_5.addPatrolPointForChildren("Under2_405", 510, 700, 10);
spawner405_5.addPatrolPointForChildren("Under2_405", 880, 690, 0);
spawner405_5.addPatrolPointForChildren("Under2_405", 1210, 790, 10);

spawner405_6 = randomStoppedSpawner(myRooms.Under2_405, "spawner405_6", 1, 300, 580, "Under2_405");
spawner405_6.addPatrolPointForChildren("Under2_405", 340, 470, 0);
spawner405_6.addPatrolPointForChildren("Under2_405", 220, 350, 10);
spawner405_6.addPatrolPointForChildren("Under2_405", 340, 470, 0);
spawner405_6.addPatrolPointForChildren("Under2_405", 300, 580, 10);

//Guards 407
spawner407_1 = randomStoppedGuardSpawner(myRooms.Under2_407, "spawner407_1", 1, 1050, 200, "Under2_407", 350);
spawner407_2 = randomStoppedGuardSpawner(myRooms.Under2_407, "spawner407_2", 1, 1250, 330, "Under2_407", 90);
spawner407_3 = randomStoppedGuardSpawner(myRooms.Under2_407, "spawner407_3", 1, 270, 450, "Under2_407", 90);
spawner407_4 = randomStoppedGuardSpawner(myRooms.Under2_407, "spawner407_4", 1, 160, 760, "Under2_407", 90);

//Roamers 407
spawner407_5 = randomStoppedSpawner(myRooms.Under2_407, "spawner407_5", 1, 330, 550, "Under2_407");
spawner407_5.addPatrolPointForChildren("Under2_407", 690, 630, 0);
spawner407_5.addPatrolPointForChildren("Under2_407", 940, 750, 10);
spawner407_5.addPatrolPointForChildren("Under2_407", 690, 630, 0);
spawner407_5.addPatrolPointForChildren("Under2_407", 330, 550, 10);

//Guards 601
spawner601_1 = randomStoppedGuardSpawner(myRooms.Under2_601, "spawner601_1", 1, 940, 220, "Under2_601", 90);
spawner601_2 = randomStoppedGuardSpawner(myRooms.Under2_601, "spawner601_2", 1, 810, 680, "Under2_601", 90);
spawner601_3 = randomStoppedGuardSpawner(myRooms.Under2_601, "spawner601_3", 1, 400, 800, "Under2_601", 350);
spawner601_4 = randomStoppedGuardSpawner(myRooms.Under2_601, "spawner601_4", 1, 430, 460, "Under2_601", 120);
spawner601_5 = randomStoppedGuardSpawner(myRooms.Under2_601, "spawner601_5", 1, 1140, 810, "Under2_601", 240);

//Roamers 601
spawner601_6 = randomStoppedSpawner(myRooms.Under2_601, "spawner601_6", 1, 1210, 350, "Under2_601");
spawner601_6.addPatrolPointForChildren("Under2_601", 1110, 270, 0);
spawner601_6.addPatrolPointForChildren("Under2_601", 930, 340, 10);
spawner601_6.addPatrolPointForChildren("Under2_601", 1110, 270, 0);
spawner601_6.addPatrolPointForChildren("Under2_601", 1210, 350, 10);

//Guards 603
spawner603_1 = randomStoppedGuardSpawner(myRooms.Under2_603, "spawner603_1", 1, 1220, 320, "Under2_603", 350);
spawner603_2 = randomStoppedGuardSpawner(myRooms.Under2_603, "spawner603_2", 1, 850, 330, "Under2_603", 240);
spawner603_3 = randomStoppedGuardSpawner(myRooms.Under2_603, "spawner603_3", 1, 1180, 800, "Under2_603", 240);
spawner603_4 = randomStoppedGuardSpawner(myRooms.Under2_603, "spawner603_4", 1, 640, 770, "Under2_603", 350);

//Roamers 603
spawner603_5 = randomStoppedSpawner(myRooms.Under2_603, "spawner603_5", 1, 400, 430, "Under2_603");
spawner603_5.addPatrolPointForChildren("Under2_603", 520, 510, 0);
spawner603_5.addPatrolPointForChildren("Under2_603", 540, 590, 10);
spawner603_5.addPatrolPointForChildren("Under2_603", 520, 510, 0);
spawner603_5.addPatrolPointForChildren("Under2_603", 400, 430, 10);

//Guards 605
spawner605_1 = randomStoppedGuardSpawner(myRooms.Under2_605, "spawner605_1", 1, 1360, 440, "Under2_605", 240);
spawner605_2 = randomStoppedGuardSpawner(myRooms.Under2_605, "spawner605_2", 1, 630, 250, "Under2_605", 90);
spawner605_3 = randomStoppedGuardSpawner(myRooms.Under2_605, "spawner605_3", 1, 210, 430, "Under2_605", 90);
spawner605_4 = randomStoppedGuardSpawner(myRooms.Under2_605, "spawner605_4", 1, 410, 830, "Under2_605", 350);
spawner605_5 = randomStoppedGuardSpawner(myRooms.Under2_605, "spawner605_5", 1, 1150, 810, "Under2_605", 350);

//Roamers 605
spawner605_6 = randomStoppedSpawner(myRooms.Under2_605, "spawner605_6", 1, 1270, 380, "Under2_605");
spawner605_6.addPatrolPointForChildren("Under2_605", 890, 610, 0);
spawner605_6.addPatrolPointForChildren("Under2_605", 1260, 740, 10);
spawner605_6.addPatrolPointForChildren("Under2_605", 890, 610, 0);
spawner605_6.addPatrolPointForChildren("Under2_605", 1270, 380, 10);

//Guards 607
spawner607_1 = randomStoppedGuardSpawner(myRooms.Under2_607, "spawner607_1", 1, 130, 320, "Under2_607", 90);
spawner607_2 = randomStoppedGuardSpawner(myRooms.Under2_607, "spawner607_2", 1, 630, 230, "Under2_607", 240);
spawner607_3 = randomStoppedGuardSpawner(myRooms.Under2_607, "spawner607_3", 1, 1030, 520, "Under2_607", 90);
spawner607_4 = randomStoppedGuardSpawner(myRooms.Under2_607, "spawner607_4", 1, 220, 800, "Under2_607", 90);
spawner607_5 = randomStoppedGuardSpawner(myRooms.Under2_607, "spawner607_5", 1, 1390, 750, "Under2_607", 120);
spawner607_6 = randomStoppedGuardSpawner(myRooms.Under2_607, "spawner607_6", 1, 950, 780, "Under2_607", 350);

//Guards 801
spawner801_1 = randomStoppedGuardSpawner(myRooms.Under2_801, "spawner801_1", 1, 200, 320, "Under2_801", 120);
spawner801_2 = randomStoppedGuardSpawner(myRooms.Under2_801, "spawner801_2", 1, 480, 370, "Under2_801", 90);
spawner801_3 = randomStoppedGuardSpawner(myRooms.Under2_801, "spawner801_3", 1, 380, 800, "Under2_801", 350);
spawner801_4 = randomStoppedGuardSpawner(myRooms.Under2_801, "spawner801_4", 1, 1120, 820, "Under2_801", 350);
spawner801_5 = randomStoppedGuardSpawner(myRooms.Under2_801, "spawner801_5", 1, 1330, 500, "Under2_801", 240);

//Guards 803
spawner803_1 = randomStoppedGuardSpawner(myRooms.Under2_803, "spawner803_1", 1, 1250, 250, "Under2_803", 120);
spawner803_2 = randomStoppedGuardSpawner(myRooms.Under2_803, "spawner803_2", 1, 810, 390, "Under2_803", 120);
spawner803_3 = randomStoppedGuardSpawner(myRooms.Under2_803, "spawner803_3", 1, 300, 300, "Under2_803", 90);
spawner803_4 = randomStoppedGuardSpawner(myRooms.Under2_803, "spawner803_4", 1, 170, 540, "Under2_803", 350);
spawner803_5 = randomStoppedGuardSpawner(myRooms.Under2_803, "spawner803_5", 1, 320, 840, "Under2_803", 350);
spawner803_6 = randomStoppedGuardSpawner(myRooms.Under2_803, "spawner803_6", 1, 950, 830, "Under2_803", 240);
spawner803_7 = randomStoppedGuardSpawner(myRooms.Under2_803, "spawner803_7", 1, 1150, 510, "Under2_803", 120);

//Guards 805
spawner805_1 = randomStoppedGuardSpawner(myRooms.Under2_805, "spawner805_1", 1, 930, 700, "Under2_805", 240);
spawner805_2 = randomStoppedGuardSpawner(myRooms.Under2_805, "spawner805_2", 1, 320, 530, "Under2_805", 350);
spawner805_3 = randomStoppedGuardSpawner(myRooms.Under2_805, "spawner805_3", 1, 550, 880, "Under2_805", 350);
spawner805_4 = randomStoppedGuardSpawner(myRooms.Under2_805, "spawner805_4", 1, 550, 450, "Under2_805", 240);

//Roamers 805
spawner805_5 = randomStoppedSpawner(myRooms.Under2_805, "spawner805_5", 1, 430, 320, "Under2_805");
spawner805_5.addPatrolPointForChildren("Under2_805", 440, 600, 0);
spawner805_5.addPatrolPointForChildren("Under2_805", 740, 690, 10);
spawner805_5.addPatrolPointForChildren("Under2_805", 440, 600, 0);
spawner805_5.addPatrolPointForChildren("Under2_805", 430, 320, 10);

spawner805_6 = randomStoppedSpawner(myRooms.Under2_805, "spawner805_6", 1, 1270, 650, "Under2_805");
spawner805_6.addPatrolPointForChildren("Under2_805", 990, 580, 0);
spawner805_6.addPatrolPointForChildren("Under2_805", 1060, 380, 10);
spawner805_6.addPatrolPointForChildren("Under2_805", 990, 580, 0);
spawner805_6.addPatrolPointForChildren("Under2_805", 1270, 650, 10);

//Guards 807
spawner807_1 = randomStoppedGuardSpawner(myRooms.Under2_807, "spawner807_1", 1, 1250, 290, "Under2_807", 240);
spawner807_2 = randomStoppedGuardSpawner(myRooms.Under2_807, "spawner807_2", 1, 1150, 610, "Under2_807", 120);
spawner807_3 = randomStoppedGuardSpawner(myRooms.Under2_807, "spawner807_3", 1, 350, 840, "Under2_807", 350);
spawner807_4 = randomStoppedGuardSpawner(myRooms.Under2_807, "spawner807_4", 1, 350, 180, "Under2_807", 90);
spawner807_5 = randomStoppedGuardSpawner(myRooms.Under2_807, "spawner807_5", 1, 570, 530, "Under2_807", 90);
spawner807_6 = randomStoppedGuardSpawner(myRooms.Under2_807, "spawner807_6", 1, 140, 490, "Under2_807", 90);

//Guards 1001
spawner1001_1 = randomStoppedGuardSpawner(myRooms.Under2_1001, "spawner1001_1", 1, 220, 440, "Under2_1001", 90);
spawner1001_2 = randomStoppedGuardSpawner(myRooms.Under2_1001, "spawner1001_2", 1, 750, 480, "Under2_1001", 90);
spawner1001_3 = randomStoppedGuardSpawner(myRooms.Under2_1001, "spawner1001_3", 1, 960, 730, "Under2_1001", 120);
spawner1001_4 = randomStoppedGuardSpawner(myRooms.Under2_1001, "spawner1001_4", 1, 700, 830, "Under2_1001", 350);
spawner1001_5 = randomStoppedGuardSpawner(myRooms.Under2_1001, "spawner1001_5", 1, 120, 690, "Under2_1001", 350);
spawner1001_6 = randomStoppedGuardSpawner(myRooms.Under2_1001, "spawner1001_6", 1, 1250, 610, "Under2_1001", 90);

//Guards 1003
spawner1003_1 = randomStoppedGuardSpawner(myRooms.Under2_1003, "spawner1003_1", 1, 560, 800, "Under2_1003", 90);
spawner1003_2 = randomStoppedGuardSpawner(myRooms.Under2_1003, "spawner1003_2", 1, 1310, 500, "Under2_1003", 90);
spawner1003_3 = randomStoppedGuardSpawner(myRooms.Under2_1003, "spawner1003_3", 1, 300, 210, "Under2_1003", 90);
spawner1003_4 = randomStoppedGuardSpawner(myRooms.Under2_1003, "spawner1003_4", 1, 740, 380, "Under2_1003", 90);

//Roamers 1003
spawner1003_5 = randomStoppedSpawner(myRooms.Under2_1003, "spawner1003_5", 1, 1390, 570, "Under2_1003");
spawner1003_5.addPatrolPointForChildren("Under2_1003", 1140, 760, 0);
spawner1003_5.addPatrolPointForChildren("Under2_1003", 750, 820, 10);
spawner1003_5.addPatrolPointForChildren("Under2_1003", 1140, 760, 0);
spawner1003_5.addPatrolPointForChildren("Under2_1003", 1390, 570, 10);

spawner1003_6 = randomStoppedSpawner(myRooms.Under2_1003, "spawner1003_6", 1, 900, 390, "Under2_1003");
spawner1003_6.addPatrolPointForChildren("Under2_1003", 710, 460, 0);
spawner1003_6.addPatrolPointForChildren("Under2_1003", 420, 280, 10);
spawner1003_6.addPatrolPointForChildren("Under2_1003", 710, 460, 0);
spawner1003_6.addPatrolPointForChildren("Under2_1003", 900, 390, 10);

//Guards 1005
spawner1005_1 = randomStoppedGuardSpawner(myRooms.Under2_1005, "spawner1005_1", 1, 420, 170, "Under2_1005", 120);
spawner1005_2 = randomStoppedGuardSpawner(myRooms.Under2_1005, "spawner1005_2", 1, 340, 740, "Under2_1005", 240);
spawner1005_3 = randomStoppedGuardSpawner(myRooms.Under2_1005, "spawner1005_3", 1, 770, 880, "Under2_1005", 90);
spawner1005_4 = randomStoppedGuardSpawner(myRooms.Under2_1005, "spawner1005_4", 1, 1150, 580, "Under2_1005", 120);

//Roamers 1005
spawner1005_5 = randomStoppedSpawner(myRooms.Under2_1005, "spawner1005_5", 1, 420, 280, "Under2_1005");
spawner1005_5.addPatrolPointForChildren("Under2_1005", 500, 370, 0);
spawner1005_5.addPatrolPointForChildren("Under2_1005", 380, 550, 10);
spawner1005_5.addPatrolPointForChildren("Under2_1005", 500, 370, 0);
spawner1005_5.addPatrolPointForChildren("Under2_1005", 420, 280, 10);

spawner1005_6 = randomStoppedSpawner(myRooms.Under2_1005, "spawner1005_6", 1, 1030, 860, "Under2_1005");
spawner1005_6.addPatrolPointForChildren("Under2_1005", 1210, 700, 0);
spawner1005_6.addPatrolPointForChildren("Under2_1005", 910, 580, 10);
spawner1005_6.addPatrolPointForChildren("Under2_1005", 1210, 700, 0);
spawner1005_6.addPatrolPointForChildren("Under2_1005", 1030, 860, 10);

//Guards 1007
spawner1007_1 = randomStoppedGuardSpawner(myRooms.Under2_1007, "spawner1007_1", 1, 400, 320, "Under2_1007", 350);
spawner1007_2 = randomStoppedGuardSpawner(myRooms.Under2_1007, "spawner1007_2", 1, 560, 730, "Under2_1007", 90);
spawner1007_3 = randomStoppedGuardSpawner(myRooms.Under2_1007, "spawner1007_3", 1, 1340, 410, "Under2_1007", 120);
spawner1007_4 = randomStoppedGuardSpawner(myRooms.Under2_1007, "spawner1007_4", 1, 1080, 860, "Under2_1007", 240);
spawner1007_5 = randomStoppedGuardSpawner(myRooms.Under2_1007, "spawner1007_5", 1, 610, 470, "Under2_1007", 120);

//Roamers 1007
spawner1007_6 = randomStoppedSpawner(myRooms.Under2_1007, "spawner1007_6", 1, 950, 370, "Under2_1007");
spawner1007_6.addPatrolPointForChildren("Under2_1007", 800, 590, 0);
spawner1007_6.addPatrolPointForChildren("Under2_1007", 810, 770, 10);
spawner1007_6.addPatrolPointForChildren("Under2_1007", 800, 590, 0);
spawner1007_6.addPatrolPointForChildren("Under2_1007", 950, 370, 10);

//Spawn everything
underSpawnerList.each() {
	it.spawnAllNow();
	it.getChildren().each() { monster ->
		monster.setAwarenessArc(0);
		monster.getAI().overrideCanAttackForMiniEvent(Boolean.TRUE)
	}
}
underSpawnerListLT.each() {
	it.spawnAllNow();
	it.getChildren().each() { monster ->
		monster.setAwarenessArc(0);
		monster.getAI().overrideCanAttackForMiniEvent(Boolean.TRUE)
	}
}

// [rc] Set up all rooms as listeners for on enter to activate the room
myRooms.each { roomName, room ->
	myManager.onEnter(room) { event ->
		if( isPlayer( event.actor ) ) {
			def task = myManager.schedule(10) {
				activateRoom(room)
			}

			activationTasks[room] = task
			// [rc] It's possible that we overwrite an existing one if two onEnter events were queued up before we could stopListenForEnterExit.  And that's totally fine.  I'd rather the method be called twice than risk not calling stopListenForEnterExit by checking for the key in activationTasks
		}
	}
}




def randomStoppedGuardSpawner(room, name, maxSpawn, posX, posY, homeRoom, angle) {
	def randomMonster = ltMonsterRandomizer(room);
	def randomSpawner = creatorOfSpawners.createStoppedGuardSpawner(room, name, randomMonster.name, maxSpawn, posX, posY, homeRoom, randomMonster.level, HATE_RADIUS, angle);
	randomSpawner.setMaxNumberOfMonsters(randomMonster.quantity);
	randomSpawner.setMiniEventSpec(attackActivator)
	randomMonster.spawner_list << randomSpawner;
	return randomSpawner;
}

def randomStoppedSpawner(room, name, maxSpawn, posX, posY, homeRoom) {
	def randomMonster = ltMonsterRandomizer(room);
	def randomSpawner = creatorOfSpawners.createStoppedSpawner(room, name, randomMonster.name, maxSpawn, posX, posY, homeRoom, randomMonster.level, HATE_RADIUS);
	randomSpawner.setMaxNumberOfMonsters(randomMonster.quantity);
	randomSpawner.setMiniEventSpec(attackActivator)
	randomMonster.spawner_list << randomSpawner;
	return randomSpawner;
}

def ltMonsterRandomizer(room) {
	def randomMonster = null;
	if( ltCounter < LT_MAX && !ltSpawnerRoomList.contains(room) && !doNotSpawnLTsInRooms.contains(room) ) {
		ltRoll = random(1,10);
		if(ltRoll > 9) {
			ltCounter++;
			randomMonster = random(underMountainLTMonsters);
			ltSpawnerRoomList << room;
		} else {
			randomMonster = random(underMountainMonsters);
		}
	} else {
		randomMonster = random(underMountainMonsters);
	}
	return randomMonster;
}

def init() {
	if(areaInit) {
		return
	}

	areaInit = true
	checkLevel()
}

onZoneIn() {
	init()
}

