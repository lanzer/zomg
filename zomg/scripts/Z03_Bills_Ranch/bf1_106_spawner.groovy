//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner One - Cleared for take-off
/*def spawner1 = myRooms.BF1_106.spawnSpawner( "bf1_106_spawner1", "garlic", 1)
spawner1.setPos( 50, 410 )
spawner1.setInitialMoveForChildren( "BF1_106", 210, 560 )
spawner1.setHome( "BF1_106", 210, 560 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 2.2 )

// Spawner Two - Cleared for take-off
def spawner2 = myRooms.BF1_106.spawnSpawner( "bf1_106_spawner2", "garlic", 1)
spawner2.setPos( 50, 420 )
spawner2.setInitialMoveForChildren( "BF1_106", 175, 225 )
spawner2.setHome( "BF1_106", 175, 225 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50, 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 2.2 )

// Spawner Three - Cleared for take-off
def spawner3 = myRooms.BF1_106.spawnSpawner( "bf1_106_spawner3", "garlic", 1)
spawner3.setPos( 50, 430 )
spawner3.setInitialMoveForChildren( "BF1_106", 760, 200 )
spawner3.setHome( "BF1_106", 760, 200 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 2.2 )*/

// Spawner Three - Cleared for take-off
def spawner4 = myRooms.BF1_106.spawnSpawner( "bf1_106_spawner4", "fluff", 1)
spawner4.setPos( 50, 440 )
spawner4.setInitialMoveForChildren( "BF1_106", 760, 200 )
spawner4.setHome( "BF1_106", 760, 200 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 50 , 70 )
spawner4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 2.2 )

// Spawner Three - Cleared for take-off
def spawner5 = myRooms.BF1_106.spawnSpawner( "bf1_106_spawner5", "fluff", 1)
spawner5.setPos( 50, 450 )
spawner5.setInitialMoveForChildren( "BF1_106", 500, 380 )
spawner5.setHome( "BF1_106", 500, 380 )
spawner5.setCFH( 100 )
spawner5.setSpawnWhenPlayersAreInRoom( true )
spawner5.setWaitTime( 50 , 70 )
spawner5.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner5.childrenWander( true )
spawner5.setMonsterLevelForChildren( 2.2 )

//Alliances
spawner4.allyWithSpawner( spawner5 )

//Spawning
spawner4.forceSpawnNow()
spawner5.forceSpawnNow()