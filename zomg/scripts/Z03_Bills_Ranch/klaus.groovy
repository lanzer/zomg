//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def Klaus = spawnNPC("Klaus Klokenmeyer", myRooms.BF1_706, 290, 340)
Klaus.setRotation( 45 )
Klaus.setDisplayName( "Klaus Klokenmeyer" )

angrySkeeter1Spawned = false
angrySkeeter2Spawned = false
angrySkeeter3Spawned = false
angrySkeeter4Spawned = false
angrySkeeter5Spawned = false
angrySkeeter6Spawned = false

onQuestStep( 26, 2 ) { event -> event.player.addMiniMapQuestActorName( "Klaus Klokenmeyer" ) }

onQuestStep( 24, 2 ) { event -> event.player.addMiniMapQuestActorName( "Klaus Klokenmeyer" ); event.player.removeMiniMapQuestLocation( "Null Crystal" ) } 

onQuestStep(278,2) { event -> event.player.addMiniMapQuestActorName("Klaus Klokenmeyer")}

//---------------------------------------------------------------------------------------------
//Trigger zone for fix clock quest                                                             
//---------------------------------------------------------------------------------------------
def angrySkeeterDispose1() { 
	if(angrySkeeter1Spawned == true && angrySkeeter1.getHated().size() == 0) { 
		angrySkeeter1.dispose() 
		angrySkeeter1Spawned = false
	}
	if(angrySkeeter1Spawned == true && angrySkeeter1.getHated().size() > 0) {
		myManager.schedule(30) { angrySkeeter1Dispose() }
	}
}
def angrySkeeterDispose2() { 
	if(angrySkeeter2Spawned == true && angrySkeeter2.getHated().size() == 0) { 
		angrySkeeter2.dispose() 
		angrySkeeter2Spawned = false
	}
	if(angrySkeeter2Spawned == true && angrySkeeter2.getHated().size() > 0) {
		myManager.schedule(30) { angrySkeeter2Dispose() }
	}
}
def angrySkeeterDispose3() { 
	if(angrySkeeter3Spawned == true && angrySkeeter3.getHated().size() == 0) { 
		angrySkeeter3.dispose() 
		angrySkeeter3Spawned = false
	}
	if(angrySkeeter3Spawned == true && angrySkeeter3.getHated().size() > 0) {
		myManager.schedule(30) { angrySkeeter3Dispose() }
	}
}
def angrySkeeterDispose4() { 
	if(angrySkeeter4Spawned == true && angrySkeeter4.getHated().size() == 0) { 
		angrySkeeter4.dispose() 
		angrySkeeter3Spawned = false
	}
	if(angrySkeeter4Spawned == true && angrySkeeter4.getHated().size() > 0) {
		myManager.schedule(30) { angrySkeeter4Dispose() }
	}
}
def angrySkeeterDispose5() { 
	if(angrySkeeter5Spawned == true && angrySkeeter5.getHated().size() == 0) { 
		angrySkeeter5.dispose() 
		angrySkeeter5Spawned = false
	}
	if(angrySkeeter5Spawned == true && angrySkeeter5.getHated().size() > 0) {
		myManager.schedule(30) { angrySkeeter5Dispose() }
	}
}
def angrySkeeterDispose6() { 
	if(angrySkeeter6Spawned == true && angrySkeeter6.getHated().size() == 0) { 
		angrySkeeter6.dispose() 
		angrySkeeter6Spawned = false
	}
	if(angrySkeeter6Spawned == true && angrySkeeter6.getHated().size() > 0) {
		myManager.schedule(30) { angrySkeeter6Dispose() }
	}
}

//Spawners
skeeterSpawner1 = myRooms.BF1_805.spawnSpawner("skeeterSpawner1", "mosquito", 1)
skeeterSpawner1.setPos(520, 85)
skeeterSpawner1.setSpawnWhenPlayersAreInRoom(true)
skeeterSpawner1.setMonsterLevelForChildren(2.0)
skeeterSpawner1.stopSpawning()

skeeterSpawner2 = myRooms.BF1_805.spawnSpawner("skeeterSpawner2", "mosquito", 1)
skeeterSpawner2.setPos(980, 190)
skeeterSpawner2.setSpawnWhenPlayersAreInRoom(true)
skeeterSpawner2.setMonsterLevelForChildren(2.0)
skeeterSpawner2.stopSpawning()

skeeterSpawner3 = myRooms.BF1_806.spawnSpawner("skeeterSpawner3", "mosquito", 1)
skeeterSpawner3.setPos(890, 210)
skeeterSpawner3.setSpawnWhenPlayersAreInRoom(true)
skeeterSpawner3.setMonsterLevelForChildren(2.0)
skeeterSpawner3.stopSpawning()

skeeterSpawner4 = myRooms.BF1_806.spawnSpawner("skeeterSpawner4", "mosquito", 1)
skeeterSpawner4.setPos(865, 160)
skeeterSpawner4.setSpawnWhenPlayersAreInRoom(true)
skeeterSpawner4.setMonsterLevelForChildren(2.0)
skeeterSpawner4.stopSpawning()

skeeterSpawner5 = myRooms.BF1_705.spawnSpawner("skeeterSpawner5", "mosquito", 1)
skeeterSpawner5.setPos(540, 75)
skeeterSpawner5.setSpawnWhenPlayersAreInRoom(true)
skeeterSpawner5.setMonsterLevelForChildren(2.0)
skeeterSpawner5.stopSpawning()

skeeterSpawner6 = myRooms.BF1_705.spawnSpawner("skeeterSpawner6", "mosquito", 1)
skeeterSpawner6.setPos(850, 75)
skeeterSpawner6.setSpawnWhenPlayersAreInRoom(true)
skeeterSpawner6.setMonsterLevelForChildren(2.0)
skeeterSpawner6.stopSpawning()

klausKlocken = "klausKlocken"
myRooms.BF1_404.createTriggerZone(klausKlocken, 250, 200, 500, 400)
myManager.onTriggerIn(myRooms.BF1_404, klausKlocken) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(24, 2)) {
		event.actor.updateQuest(24, "Klaus Klokenmeyer")
		event.actor.centerPrint("The effects of Animation no longer control Klaus' clocken.")
	}
}

//SUGGESTION : Use the onEnter to say "You see the clock on the ground toward the cliff" and then set up a trigger zone
//toward the western edge of the room to actually award the quest update.                                              
//Makes the challenge a bit higher and the message not so abrupt as you enter the room. -- DG                          

myManager.onEnter(myRooms.BF1_804) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(26, 2)) {
		event.actor.updateQuest(26, "Klaus Klokenmeyer")
		myManager.schedule(3) { event.actor.centerPrint("You have retrieved Klaus Klokenmeyer's clocken. It was on the ground!") }
	}
}

block805 = new Object()

myManager.onEnter(myRooms.BF1_805) { event ->
	synchronized( block805 ) {
		if(isPlayer(event.actor) && event.actor.isOnQuest(26, 3) && !event.actor.hasQuestFlag(GLOBAL, "Z3_Klaus_805_gank") && angrySkeeter1Spawned == false && angrySkeeter2Spawned == false && skeeterSpawner1.spawnsInUse() == 0 && skeeterSpawner2.spawnsInUse() == 0) {
			angrySkeeter1Spawned = true
			angrySkeeter1 = skeeterSpawner1.forceSpawnNow() 
			angrySkeeter1.goAggroOn(event.actor)
			myManager.schedule(120) { angrySkeeterDispose1() }
			runOnDeath(angrySkeeter1){ angrySkeeter1Spawned = false }
		
			angrySkeeter2Spawned = true
			angrySkeeter2 = skeeterSpawner2.forceSpawnNow() 
			angrySkeeter2.goAggroOn(event.actor)
			myManager.schedule(120) { angrySkeeterDispose2() }
			runOnDeath(angrySkeeter2) { angrySkeeter2Spawned = false }
		
			event.actor.setQuestFlag(GLOBAL, "Z3_Klaus_805_gank")
		}
	}
}

block806 = new Object()
		
myManager.onEnter(myRooms.BF1_806) { event ->
	synchronized( block806 ) {
		if(isPlayer(event.actor) && event.actor.isOnQuest(26, 3) && !event.actor.hasQuestFlag(GLOBAL, "Z3_Klaus_806_gank")&& angrySkeeter3Spawned == false && angrySkeeter4Spawned == false && skeeterSpawner3.spawnsInUse() == 0 && skeeterSpawner4.spawnsInUse() == 0) {
			angrySkeeter3Spawned = true
			angrySkeeter3 = skeeterSpawner3.forceSpawnNow()
			angrySkeeter3.goAggroOn(event.actor)
			myManager.schedule(120) { angrySkeeterDispose3() }
			runOnDeath(angrySkeeter3) { angrySkeeter2Spawned = false }
	
			angrySkeeter4Spawned = true
			angrySkeeter4 = skeeterSpawner4.forceSpawnNow()
			angrySkeeter4.goAggroOn(event.actor)
			myManager.schedule(120) { angrySkeeterDispose4() }
			runOnDeath(angrySkeeter4) { angrySkeeter2Spawned = false }
		
			event.actor.setQuestFlag(GLOBAL, "Z3_Klaus_806_gank")
		}
	}
}

block705 = new Object()

myManager.onEnter(myRooms.BF1_705) { event ->
	synchronized( block705 ) {
		if(isPlayer(event.actor) && event.actor.isOnQuest(26, 3) && !event.actor.hasQuestFlag(GLOBAL, "Z3_Klaus_705_gank") && angrySkeeter5Spawned == false && angrySkeeter6Spawned == false && skeeterSpawner5.spawnsInUse() == 0 && skeeterSpawner6.spawnsInUse() == 0) {
			angrySkeeter5Spawned = true
			angrySkeeter5 = skeeterSpawner5.forceSpawnNow()
			angrySkeeter5.goAggroOn(event.actor)
			myManager.schedule(120) { angrySkeeterDispose5() }
			runOnDeath(angrySkeeter5) { angrySkeeter2Spawned = false }
	
			angrySkeeter6Spawned = true
			angrySkeeter6 = skeeterSpawner6.forceSpawnNow()
			angrySkeeter6.goAggroOn(event.actor)
			myManager.schedule(120) { angrySkeeterDispose6() }
			runOnDeath(angrySkeeter6) { angrySkeeter2Spawned = false }
		
			event.actor.setQuestFlag(GLOBAL, "Z3_Klaus_705_gank")
		}
	}
}

//---------------------------------------------------------------------------------------------
//Default conversation for Klaus - Mine Clocken                                                
//---------------------------------------------------------------------------------------------
def klausDefault = Klaus.createConversation("klausDefault", true, "!QuestStarted_26", "!QuestCompleted_26")

def default1 = [id:1]
default1.npctext = "Gut-mornink! Erm. It iz mornink, ya? I cannot be telling ze time since I losen mine clocken."
default1.options = []
default1.options << [text:"Yeah, it's morning.", result: 2]
default1.options << [text:"No, it's daytime.", result: 9]
default1.options << [text:"No, it's night.", result: 10]
klausDefault.addDialog(default1, Klaus)

def default2 = [id:2]
default2.npctext = "Oh, thank gutness! I am being lost vithout ze clocken."
default2.result = 3
klausDefault.addDialog(default2, Klaus)

def default3 = [id:3]
default3.playertext = "What happened to your clock?"
default3.result = 4
klausDefault.addDialog(default3, Klaus)

def default4 = [id:4]
default4.npctext = "Ach! Mine clocken! One minute it vas tick-tick-ticking avay ze next it vas running down ze road. Ze Animated has infected mine clocken!"
default4.result = 5
klausDefault.addDialog(default4, Klaus)

def default5 = [id:5]
default5.playertext = "Can't you just track it down and grab it?"
default5.result = 6
klausDefault.addDialog(default5, Klaus)

def default6 = [id:6]
default6.npctext = "Nein! I tried but ze skeeters, zer are too many. Zey are fine until I grab ze clock, but zen zey all attacks me! Maybe you can fight zem off?"
default6.options = []
default6.options << [text:"Okay. I'll try to get your clock back for you.", result: 7]
default6.options << [text:"I really don't have time to go track down a silly clock.", result: 8]
klausDefault.addDialog(default6, Klaus)

def default7 = [id:7]
default7.npctext = "Guten! You vill save mine clocken? I followed mine clocken all ze vay from Aekea to ze skeeter hive to the southvest. It seems all the Animated clocks are gathering there and zey are very protective of ze hive! Get mine clocken, but be careful. Ze skeeters vill attacken vhen you take it."
default7.quest = 26
default7.result = DONE
klausDefault.addDialog(default7, Klaus)

def default8 = [id:8]
default8.npctext = "Mine clocken! Ze skeeters, ach!"
default8.result = DONE
klausDefault.addDialog(default8, Klaus)

def default9 = [id:9]
default9.npctext = "Ach! Daytime? Well zen, gut-afternoon...k! I am being lost vithout ze clocken."
default9.result = 3
klausDefault.addDialog(default9, Klaus)

def default10 = [id:10]
default10.npctext = "Ach! Night? Zen, gut-evenink! I am being lost vithout ze clocken."
default10.result = 3
klausDefault.addDialog(default10, Klaus)

//---------------------------------------------------------------------------------------------
//Conversation when player is on the Mine Clocken quest                                        
//---------------------------------------------------------------------------------------------
def klausClockenActive = Klaus.createConversation("klausClockenActive", true, "QuestStarted_26", "!QuestStarted_26:3")

def clockenActive1 = [id:1]
clockenActive1.npctext = "Mine clocken! Please bring it quickly, before ze skeeters near ze hive in ze southvest break it!"
clockenActive1.result = DONE
klausClockenActive.addDialog(clockenActive1, Klaus)

//---------------------------------------------------------------------------------------------
//Conversation when player has brought Klaus' clock back                                       
//---------------------------------------------------------------------------------------------
def klausClockenEnd = Klaus.createConversation("klausClockenEnd", true, "QuestStarted_26:3")

def clockenEnd1 = [id:1]
clockenEnd1.npctext = "Mine clocken! You saved it from ze skeeters!"
clockenEnd1.exec = { event -> event.player.removeMiniMapQuestActorName( "Klaus Klokenmeyer" ) }
clockenEnd1.result = 2
klausClockenEnd.addDialog(clockenEnd1, Klaus)

def clockenEnd2 = [id:2]
clockenEnd2.playertext = "I sure did. Anything else I can help with?"
clockenEnd2.result = 3
klausClockenEnd.addDialog(clockenEnd2, Klaus)

def clockenEnd3 = [id:3]
clockenEnd3.npctext = "Since you offered, mine clocken iz schtill Animated! I have been noticing zat ze crystal near ze traktor reverses ze animation. Would you take mine clocken to the nearby field, northvest of here, and reverse ze animation?"
clockenEnd3.options = []
clockenEnd3.options << [text:"I can do that!", result: 4]
clockenEnd3.options << [text:"No way, do it yourself you lazy old man.", result: 5]
klausClockenEnd.addDialog(clockenEnd3, Klaus)

def clockenEnd4 = [id:4]
clockenEnd4.npctext = "Excellent! Just bring mine clocken near ze crystal and it should reverse ze animation."
clockenEnd4.result = DONE
clockenEnd4.exec = { event -> 
	event.player.updateQuest(26, "Klaus Klokenmeyer")
	event.player.updateQuest(24, "Klaus Klokenmeyer")
	event.player.addMiniMapQuestLocation( "Null Crystal", "BF1_404", 375, 300, "Null Crystal" )
}
klausClockenEnd.addDialog(clockenEnd4, Klaus)

def clockenEnd5 = [id:5]
clockenEnd5.npctext = "Ach! Mine clocken!"
clockenEnd5.quest = 26
clockenEnd5.result = DONE
klausClockenEnd.addDialog(clockenEnd5, Klaus)

//---------------------------------------------------------------------------------------------
//Conversation when player is on Fix Mine Clocken quest                                        
//---------------------------------------------------------------------------------------------
def klausFixClockenActive = Klaus.createConversation("klausFixClockenActive", true, "QuestStarted_24", "!QuestStarted_24:3")

def fixClockenActive1 = [id:1]
fixClockenActive1.npctext = "Mine clocken is still ze Animated!"
fixClockenActive1.result = DONE
klausFixClockenActive.addDialog(fixClockenActive1, Klaus)

//---------------------------------------------------------------------------------------------
//Conversation when player has fixed Klaus' clock                                              
//---------------------------------------------------------------------------------------------
def klausFixClockenEnd = Klaus.createConversation("klausFixClockenEnd", true, "QuestStarted_24:3")

def fixClockenEnd1 = [id:1]
fixClockenEnd1.npctext = "Mine clocken is no longer ze Animated! Oh zank gutness, I can tell ze time again!"
fixClockenEnd1.quest = 24
fixClockenEnd1.exec = { event -> event.player.removeMiniMapQuestActorName( "Klaus Klokenmeyer" ) }
fixClockenEnd1.result = DONE
klausFixClockenEnd.addDialog(fixClockenEnd1, Klaus)

//---------------------------------------------------------------------------------------------
//GRANT - Repeatable Skeeter kill quest                                                        
//---------------------------------------------------------------------------------------------
def klokedOutGive = Klaus.createConversation("klokedOutGive", true, "QuestCompleted_26", "!QuestStarted_24", "!QuestStarted_278", "!Z3_Klaus_KlokedOut_Allowed", "!Z3_Klaus_KlokedOut_Denied")

def klokedOutGive1 = [id:1]
klokedOutGive1.npctext = "Ze skeeters, zey must pay for ze attacks zey are makings! You must make zem pay!"
klokedOutGive1.options = []
klokedOutGive1.options << [text:"Definitely! The pests just won't leave well enough alone. I'll take them out.", exec: { event ->
	if(event.player.getConLevel() < 3.1) {
		event.player.setQuestFlag(GLOBAL, "Z3_Klaus_KlokedOut_Allowed")
		Klaus.pushDialog(event.player, "klokedOutAllowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z3_Klaus_KlokedOut_Denied")
		Klaus.pushDialog(event.player, "klokedOutDenied")
	}
}, result: DONE]
klokedOutGive1.options << [text:"I've got more important things to deal with than a few pests, Klaus. Get some bug spray and move on with your life.", result: 2]
klokedOutGive.addDialog(klokedOutGive1, Klaus)

def klokedOutGive2 = [id:2]
klokedOutGive2.npctext = "Bah! You vill be back!"
klokedOutGive2.result = DONE
klokedOutGive.addDialog(klokedOutGive2, Klaus)

//---------------------------------------------------------------------------------------------
//ALLOWED - Repeatable Skeeter kill quest                                                      
//---------------------------------------------------------------------------------------------
def klokedOutAllowed = Klaus.createConversation("klokedOutAllowed", true, "Z3_Klaus_KlokedOut_Allowed")

def klokedOutAllowed1 = [id:1]
klokedOutAllowed1.npctext = "If you defeat zem, I vill give you ze revards!"
klokedOutAllowed1.playertext = "How many did you have in mind?"
klokedOutAllowed1.result = 2
klokedOutAllowed.addDialog(klokedOutAllowed1, Klaus)

def klokedOutAllowed2 = [id:2]
klokedOutAllowed2.npctext = "Oh, I think perhaps tventy."
klokedOutAllowed2.quest = 278
klokedOutAllowed2.flag = "!Z3_Klaus_KlokedOut_Allowed"
klokedOutAllowed2.result = DONE
klokedOutAllowed.addDialog(klokedOutAllowed2, Klaus)

//---------------------------------------------------------------------------------------------
//DENIED - Repeatable Skeeter kill quest                                                       
//---------------------------------------------------------------------------------------------
def klokedOutDenied = Klaus.createConversation("klokedOutDenied", true, "Z3_Klaus_KlokedOut_Denied")

def klokedOutDenied1 = [id:1]
klokedOutDenied1.npctext = "Ach! I vouldn't dream of vasting the time of someone as schtrong as you."
klokedOutDenied1.flag = "!Z3_Klaus_KlokedOut_Denied"
klokedOutDenied1.exec = { event ->
	event.player.centerPrint( "You must be level 3.0 or lower for this task." ) 
	event.player.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
}
klokedOutDenied1.result = DONE
klokedOutDenied.addDialog(klokedOutDenied1, Klaus)

//---------------------------------------------------------------------------------------------
//ACTIVE - Repeatable Skeeter kill quest                                                       
//---------------------------------------------------------------------------------------------
def klokedOutActive = Klaus.createConversation("klokedOutActive", true, "QuestStarted_278:2")

def klokedOutActive1 = [id:1]
klokedOutActive1.npctext = "You have not defeated ze skeeters yet! Vill you do it zoon?"
klokedOutActive1.playertext = "Yes, but I'm having trouble finding them."
klokedOutActive1.result = 2
klokedOutActive.addDialog(klokedOutActive1, Klaus)

def klokedOutActive2 = [id:2]
klokedOutActive2.npctext = "Vat?! Zey are crawling all over zeir hive to ze zouthvest!"
klokedOutActive2.result = DONE
klokedOutActive.addDialog(klokedOutActive2, Klaus)

//---------------------------------------------------------------------------------------------
//COMPLETE - Repeatable Skeeter kill quest                                                     
//---------------------------------------------------------------------------------------------
def klokedOutComplete = Klaus.createConversation("klokedOutComplete", true, "QuestStarted_278:3")

def klokedOutComplete1 = [id:1]
klokedOutComplete1.npctext = "You did it! You defeated ze skeeters."
klokedOutComplete1.playertext = "Yes... but those things must breed fast. They just keep pouring out of the dang hive."
klokedOutComplete1.result = 2
klokedOutComplete.addDialog(klokedOutComplete1, Klaus)

def klokedOutComplete2 = [id:2]
klokedOutComplete2.npctext = "Zen defeat zome more? We will beat zem yet."
klokedOutComplete2.options = []
klokedOutComplete2.options << [text:"Yes, I will not give up this easily.", exec: { event ->
	event.player.removeMiniMapQuestActorName("Klaus Klokenmeyer")
	event.player.updateQuest(278, "Klaus Klokenmeyer")
	if(event.player.getConLevel() < 3.1) {
		event.player.setQuestFlag(GLOBAL, "Z3_Klaus_KlokedOut_AllowedAgain")
		Klaus.pushDialog(event.player, "klokedOutAllowedAgain")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z3_Klaus_KlokedOut_Denied")
		Klaus.pushDialog(event.player, "klokedOutDenied")
	}
}, result: DONE]
klokedOutComplete2.options << [text:"Not right now, Klaus. The buzzing has given me a headache.", result: 3]
klokedOutComplete.addDialog(klokedOutComplete2, Klaus)

def klokedOutComplete3 = [id:3]
klokedOutComplete3.npctext = "Ech!"
klokedOutComplete3.quest = 278
klokedOutComplete3.exec = { event -> event.player.removeMiniMapQuestActorName("Klaus Klokenmeyer") }
klokedOutComplete3.result = DONE
klokedOutComplete.addDialog(klokedOutComplete3, Klaus)

//---------------------------------------------------------------------------------------------
//ALLOWED AGAIN - Repeatable Skeeter kill quest                                                     
//---------------------------------------------------------------------------------------------
def klokedOutAllowedAgain = Klaus.createConversation("klokedOutAllowedAgain", true, "Z3_Klaus_KlokedOut_AllowedAgain")

def klokedOutAllowedAgain1 = [id:1]
klokedOutAllowedAgain1.npctext = "Exzellent!"
klokedOutAllowedAgain1.exec = { event ->
	event.player.updateQuest(278, "Klaus Klokenmeyer")
	//println "##### RAN ALLOWED AGAIN EXEC #####"
}
klokedOutAllowedAgain1.flag = "!Z3_Klaus_KlokedOut_AllowedAgain"
klokedOutAllowedAgain1.result = DONE
klokedOutAllowedAgain.addDialog(klokedOutAllowedAgain1, Klaus)
