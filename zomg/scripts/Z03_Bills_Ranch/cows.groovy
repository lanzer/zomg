//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

//List creation
tipMap = new HashMap()

tipList = [] as Set
milkList = [] as Set
allowMilkList = [] as Set
recentlyMilkedList = [] as Set

cow1Location = [myRooms.BF1_103, 920, 165]
cow2Location = [myRooms.BF1_103, 800, 500]
cow3Location = [myRooms.BF1_103, 545, 590]
cow4Location = [myRooms.BF1_104, 105, 430]
cow5Location = [myRooms.BF1_104, 160, 80]
cow6Location = [myRooms.BF1_202, 875, 280]
cow7Location = [myRooms.BF1_102, 730, 465]
cow8Location = [myRooms.BF1_202, 585, 425]
cow9Location = [myRooms.BF1_203, 880, 150]
cow10Location = [myRooms.BF1_203, 830, 545]
cow11Location = [myRooms.BF1_203, 540, 350]
cow12Location = [myRooms.BF1_203, 170, 515]
cow13Location = [myRooms.BF1_203, 140, 120]
cow14Location = [myRooms.BF1_302, 850, 200]
cow15Location = [myRooms.BF1_203, 915, 460]
cow16Location = [myRooms.BF1_302, 550, 360]
cow17Location = [myRooms.BF1_303, 880, 75]
cow18Location = [myRooms.BF1_303, 800, 345]
cow19Location = [myRooms.BF1_303, 135, 585]
cow20Location = [myRooms.BF1_303, 180, 335]
cow21Location = [myRooms.BF1_303, 140, 120]
cow22Location = [myRooms.BF1_404, 325, 115]
cow23Location = [myRooms.BF1_404, 160, 280]

cowLocations = [cow1Location, cow2Location, cow3Location, cow4Location, cow5Location, cow6Location, cow7Location, cow8Location, cow9Location, cow10Location, cow11Location, cow12Location, cow13Location, cow14Location, cow15Location, cow16Location, cow17Location, cow18Location, cow19Location, cow20Location, cow21Location, cow22Location, cow23Location]

cowToDispose = null
MIN_MILK_COWS = 8

//Quest update logic
def synchronized checkCowUpdate(event) {
	event.player.centerPrint("You kneel and attempt to milk the cow.")
	event.player.addToPlayerVar("mooersMilked", 1)
	event.critter.pause()
	controlPlayer(event.player)
	myManager.schedule(3) {
		event.player.centerPrint("Mooers Milked ${event.player.getPlayerVar("mooersMilked").intValue()}/10")
		event.critter.pauseResume()
		freePlayer(event.player)
		allowMilkList << event.critter
		event.player.unsetQuestFlag(GLOBAL, "Z3_Purvis_Milking")
		myManager.schedule(1) {
			if(event.player.getPlayerVar("mooersMilked") >= 10 && event.player.isOnQuest(66, 2)) {
				event.player.centerPrint("Take the milk back to Purvis.")
				event.player.updateQuest(66, "Purvis-VQS")
			}
		}
	}
}

def spawnRandomCow() {
	if(cowLocations.size() > 0) {
		myManager.schedule(1) { spawnRandomCow() }
		
		cowType = random(1, 5)
		cowLocation = random(cowLocations)
		cowLocations.remove(cowLocation)
	
		cowSpawnRoom = cowLocation.get(0)
		cowSpawnX = cowLocation.get(1)
		cowSpawnY = cowLocation.get(2)
	
		if(cowType >= 4) {
			cow = makeCritter("cow", cowSpawnRoom, cowSpawnX, cowSpawnY)
			cow.setDisplayName("Cow")
			cow.setUsable(true)
		
			milkList << cow
			allowMilkList << cow
		
			cow.onUse { event ->
				if(event.player.isOnQuest(66, 2) && allowMilkList.contains(event.critter) && !event.player.hasQuestFlag(GLOBAL, "Z3_Purvis_Milking")) {
					event.player.setQuestFlag(GLOBAL, "Z3_Purvis_Milking")
					allowMilkList.remove(event.critter)
					if(!recentlyMilkedList.contains(event.critter)) {
						recentlyMilkedList << event.critter
						checkCowUpdate(event)
						myManager.schedule(3) { allowMilkList << event.critter }
						myManager.schedule(60) { recentlyMilkedList.remove(event.critter) }
					} else {
						event.player.centerPrint("You kneel and attempt to milk the cow.")
						event.critter.pause()
						controlPlayer(event.player)
						myManager.schedule(3) {
							event.player.centerPrint("That cow has been milked too recently.")
							event.critter.pauseResume()
							freePlayer(event.player)
							allowMilkList << event.critter
							event.player.unsetQuestFlag(GLOBAL, "Z3_Purvis_Milking")
						}
					}
				} else { event.critter.say("Mooo") }
			}
		} else { cow = makeCow(cowSpawnRoom, cowSpawnX, cowSpawnY); cow.setDisplayName("Cow"); tipList << cow; tipMap.put(cow, cowLocation) }
	} else if(milkList.size() < MIN_MILK_COWS) {
		myManager.schedule(1) { spawnRandomCow() }
		
		cowToDispose = random(tipList)
		cowLocation = tipMap.get(cowToDispose)
		
		cowSpawnRoom = cowLocation.get(0)
		cowSpawnX = cowLocation.get(1)
		cowSpawnY = cowLocation.get(2)
		
		tipList.remove(cowToDispose)
		tipMap.remove(cowToDispose)
		cowToDispose.dispose()
		
		cow = makeCritter("cow", cowSpawnRoom, cowSpawnX, cowSpawnY)
		cow.setDisplayName("Cow")
		cow.setUsable(true)
		
		milkList << cow
		allowMilkList << cow
		
		cow.onUse { event ->
			if(event.player.isOnQuest(66, 2) && allowMilkList.contains(event.critter) && !event.player.hasQuestFlag(GLOBAL, "Z3_Purvis_Milking")) {
				event.player.setQuestFlag(GLOBAL, "Z3_Purvis_Milking")
				allowMilkList.remove(event.critter)
				if(!recentlyMilkedList.contains(event.critter)) {
					recentlyMilkedList << event.critter
					checkCowUpdate(event)
					myManager.schedule(3) { allowMilkList << event.critter }
					myManager.schedule(60) { recentlyMilkedList.remove(event.critter) }
				} else {
					event.player.centerPrint("You kneel and attempt to milk the cow.")
					event.critter.pause()
					controlPlayer(event.player)
					myManager.schedule(3) {
						event.player.centerPrint("That cow has been milked too recently.")
						event.critter.pauseResume()
						freePlayer(event.player)
						allowMilkList << event.critter
						event.player.unsetQuestFlag(GLOBAL, "Z3_Purvis_Milking")
					}
				}
			} else { event.critter.say("Mooo") }
		}
	}
}

spawnRandomCow()