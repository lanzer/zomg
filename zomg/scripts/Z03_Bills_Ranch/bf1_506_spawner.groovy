import com.gaiaonline.mmo.battle.script.*;

//----------------------------------------------------
// SPAWNERS AND PATROL PATHS                          
//----------------------------------------------------

def spawner1 = myRooms.BF1_506.spawnSpawner( "BF1_506_Spawner", "fluff", 1)
spawner1.setPos( 200, 480 )
spawner1.setInitialMoveForChildren( "BF1_506", 180, 140 )
spawner1.setHome( "BF1_506", 180, 140 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 2.1 )

def spawner2 = myRooms.BF1_506.spawnSpawner( "BF1_506_fluffr2", "fluff", 1)
spawner2.setPos( 200, 470 )
spawner2.setInitialMoveForChildren( "BF1_506", 775, 100 )
spawner2.setHome( "BF1_506", 775, 100 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander (true)
spawner2.setMonsterLevelForChildren( 2.1 )

/*def spawner3 = myRooms.BF1_506.spawnSpawner( "BF1_506_Spawner3", "garlic", 1)
spawner3.setPos( 200, 460 )
spawner3.setInitialMoveForChildren( "BF1_506", 120, 575 )
spawner3.setHome( "BF1_506", 120, 575 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander (true)
spawner3.setMonsterLevelForChildren( 2.1 )

def spawner4 = myRooms.BF1_506.spawnSpawner( "BF1_506_Spawner4", "garlic", 1)
spawner4.setPos( 200, 450 )
spawner4.setInitialMoveForChildren( "BF1_506", 770, 400 )
spawner4.setHome( "BF1_506", 770, 400 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 50 , 70 )
spawner4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner4.childrenWander (true)
spawner4.setMonsterLevelForChildren( 2.1 )*/

def spawner5 = myRooms.BF1_506.spawnSpawner( "BF1_506_Spawner5", "fluff", 1)
spawner5.setPos( 200, 440 )
spawner5.setInitialMoveForChildren( "BF1_506", 505, 345 )
spawner5.setHome( "BF1_506", 505, 345 )
spawner5.setSpawnWhenPlayersAreInRoom( true )
spawner5.setWaitTime( 50 , 70 )
spawner5.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner5.childrenWander (true)
spawner5.setMonsterLevelForChildren( 2.1 )

//Alliances
spawner1.allyWithSpawner( spawner2 )
spawner1.allyWithSpawner( spawner5 )
spawner2.allyWithSpawner( spawner5 )

//Spawning
spawner1.forceSpawnNow()
spawner2.forceSpawnNow()
spawner5.forceSpawnNow()