//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def Rubella = spawnNPC("Rubella-VQS", myRooms.BF1_2, 700, 585)
Rubella.setDisplayName( "Rubella" )

//============================================================================
//Patrols                                                                     
//============================================================================
chicken1Run = makeNewPatrol()
chicken1Run.addPatrolPoint( myRooms.BF1_201, 955, 420, 0 )
chicken1Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken1Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken2Run = makeNewPatrol()
chicken2Run.addPatrolPoint( myRooms.BF1_201, 955, 420, 0 )
chicken2Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken2Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken3Run = makeNewPatrol()
chicken3Run.addPatrolPoint( myRooms.BF1_201, 955, 420, 0 )
chicken3Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken3Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken4Run = makeNewPatrol()
chicken4Run.addPatrolPoint( myRooms.BF1_201, 955, 420, 0 )
chicken4Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken4Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken5Run = makeNewPatrol()
chicken5Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken5Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken6Run = makeNewPatrol()
chicken6Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken6Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken7Run = makeNewPatrol()
chicken7Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken7Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken8Run = makeNewPatrol()
chicken8Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken8Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken9Run = makeNewPatrol()
chicken9Run.addPatrolPoint( myRooms.BF1_201, 955, 420, 0 )
chicken9Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken9Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken10Run = makeNewPatrol()
chicken10Run.addPatrolPoint( myRooms.BF1_201, 955, 420, 0 )
chicken10Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken10Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken11Run = makeNewPatrol()
chicken11Run.addPatrolPoint( myRooms.BF1_103, 90, 80, 0 )
chicken11Run.addPatrolPoint( myRooms.BF1_102, 885, 270, 0 )
chicken11Run.addPatrolPoint( myRooms.BF1_101, 950, 580, 0 )
chicken11Run.addPatrolPoint( myRooms.BF1_201, 955, 420, 0 )
chicken11Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken11Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken12Run = makeNewPatrol()
chicken12Run.addPatrolPoint( myRooms.BF1_103, 90, 80, 0 )
chicken12Run.addPatrolPoint( myRooms.BF1_102, 885, 270, 0 )
chicken12Run.addPatrolPoint( myRooms.BF1_101, 950, 580, 0 )
chicken12Run.addPatrolPoint( myRooms.BF1_201, 955, 420, 0 )
chicken12Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken12Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken13Run = makeNewPatrol()
chicken13Run.addPatrolPoint( myRooms.BF1_103, 90, 80, 0 )
chicken13Run.addPatrolPoint( myRooms.BF1_102, 885, 270, 0 )
chicken13Run.addPatrolPoint( myRooms.BF1_101, 950, 580, 0 )
chicken13Run.addPatrolPoint( myRooms.BF1_201, 955, 420, 0 )
chicken13Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken13Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken14Run = makeNewPatrol()
chicken14Run.addPatrolPoint( myRooms.BF1_103, 90, 80, 0 )
chicken14Run.addPatrolPoint( myRooms.BF1_102, 885, 270, 0 )
chicken14Run.addPatrolPoint( myRooms.BF1_101, 950, 580, 0 )
chicken14Run.addPatrolPoint( myRooms.BF1_201, 955, 420, 0 )
chicken14Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken14Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken15Run = makeNewPatrol()
chicken15Run.addPatrolPoint( myRooms.BF1_301, 690, 530, 0 )
chicken15Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken16Run = makeNewPatrol()
chicken16Run.addPatrolPoint( myRooms.BF1_301, 690, 530, 0 )
chicken16Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken17Run = makeNewPatrol()
chicken17Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken17Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken18Run = makeNewPatrol()
chicken18Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken18Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken19Run = makeNewPatrol()
chicken19Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken19Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken20Run = makeNewPatrol()
chicken20Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken20Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken21Run = makeNewPatrol()
chicken21Run.addPatrolPoint( myRooms.BF1_401, 840, 70, 0 )
chicken21Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken21Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken22Run = makeNewPatrol()
chicken22Run.addPatrolPoint( myRooms.BF1_401, 840, 70, 0 )
chicken22Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken22Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

chicken23Run = makeNewPatrol()
chicken23Run.addPatrolPoint( myRooms.BF1_401, 840, 70, 0 )
chicken23Run.addPatrolPoint( myRooms.BF1_301, 850, 530, 0 )
chicken23Run.addPatrolPoint( myRooms.BF1_301, 535, 465, 10 )

//============================================================================
//Creates dispose trigger                                                     
//============================================================================
chickenTrigger = "chickenTrigger"
myRooms.BF1_301.createTriggerZone( chickenTrigger, 435, 350, 635, 550 )

def rubellaChickens = "rubellaChickens"

myRooms.BF1_2.createTriggerZone(rubellaChickens, 610, 515, 805, 630)

//============================================================================
//Dispose logic                                                               
//============================================================================
myManager.onTriggerIn(myRooms.BF1_301, chickenTrigger) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(223)) {
		event.actor.updateQuest(223, "Story-VQS")
	}
	if( isPlayer(event.actor) && event.actor.isOnQuest(67, 1)) {
			event.actor.updateQuest(67, "Rubella-VQS")
	}
	if(chickenFleeList.contains(event.actor)) {
		spawnLocations << chickenSpawnMap.get(event.actor)
		event.actor.dispose()
	}
}

//============================================================================
//Chicken behavior, player can grab or chicken runs                           
//============================================================================
chickenBehaviorList = [ "wrangles", "wrangles", "runs" ]

chicken1Location = [myRooms.BF1_101, 810, 550, chicken1Run]
chicken2Location = [myRooms.BF1_102, 350, 505, chicken2Run]
chicken3Location = [myRooms.BF1_102, 780, 465, chicken3Run]
chicken4Location = [myRooms.BF1_102, 545, 125, chicken4Run]
chicken5Location = [myRooms.BF1_201, 490, 575, chicken5Run]
chicken6Location = [myRooms.BF1_201, 880, 560, chicken6Run]
chicken7Location = [myRooms.BF1_201, 940, 175, chicken7Run]
chicken8Location = [myRooms.BF1_201, 945, 325, chicken8Run]
chicken9Location = [myRooms.BF1_202, 105, 500, chicken9Run]
chicken10Location = [myRooms.BF1_202, 95, 220, chicken10Run]
chicken11Location = [myRooms.BF1_3, 570, 330, chicken11Run]
chicken12Location = [myRooms.BF1_3, 700, 530, chicken12Run]
chicken13Location = [myRooms.BF1_3, 925, 435, chicken13Run]
chicken14Location = [myRooms.BF1_301, 850, 145, chicken14Run]
chicken15Location = [myRooms.BF1_301, 870, 450, chicken15Run]
chicken16Location = [myRooms.BF1_301, 750, 590, chicken16Run]
chicken17Location = [myRooms.BF1_302, 125, 550, chicken17Run]
chicken18Location = [myRooms.BF1_302, 150, 135, chicken18Run]
chicken19Location = [myRooms.BF1_401, 850, 150, chicken19Run]
chicken20Location = [myRooms.BF1_401, 370, 85, chicken20Run]
chicken21Location = [myRooms.BF1_402, 150, 400, chicken21Run]
chicken22Location = [myRooms.BF1_402, 150, 115, chicken22Run]
chicken23Location = [myRooms.BF1_402, 425, 310, chicken23Run]

spawnLocations = [chicken1Location, chicken2Location, chicken3Location, chicken4Location, chicken5Location, chicken6Location, chicken7Location, chicken8Location, chicken9Location, chicken10Location, chicken11Location, chicken12Location, chicken13Location, chicken14Location, chicken15Location, chicken16Location, chicken17Location, chicken18Location, chicken19Location, chicken20Location, chicken21Location, chicken22Location, chicken23Location]
chickenFleeList = []
chickenSpawnMap = new HashMap()
chickenBehaviorMap = new HashMap()
chickenCarryMap = new HashMap()

chickenStartupCounter = 0

//============================================================================
//Chicken spawn logic                                                         
//============================================================================
def spawnCheck() {
	if(spawnLocations.size() > 0) {
		if(chickenStartupCounter < 23) {
			myManager.schedule(1) { chickenSpawn() }
		}
		if(chickenStartupCounter >= 23) {
			myManager.schedule(random(30, 45)) { chickenSpawn() }
		}
	} else {
		myManager.schedule(15) { spawnCheck() }
	}
}

def chickenSpawn() {
	myManager.schedule(1) { spawnCheck() }
	chickenBehavior = random(chickenBehaviorList)
	spawnLocation = random(spawnLocations)
	
	spawnRoom = spawnLocation.get(0)
	spawnX = spawnLocation.get(1)
	spawnY = spawnLocation.get(2)
	spawnPatrol = spawnLocation.get(3)
	
	chicken = makeCritter("chicken", spawnRoom, spawnX, spawnY)
	chicken.setDisplayName("Chicken")
	chicken.setUsable(true)
	chicken.requireFlags("QuestStarted_67:2")
	chicken.setPatrol(spawnPatrol)
	
	//println "#### Spawned ${chicken} in ${spawnRoom}, ${spawnX}, ${spawnY} and behavior is ${chickenBehavior} ####"
	
	chickenBehaviorMap.put(chicken, chickenBehavior)
	chickenSpawnMap.put(chicken, spawnLocation)
	
	spawnLocations.remove(spawnLocation)
	
	chicken.onUse( { event ->
		if(whoIsLatchedTo(event.player).size() > 0) {
			whoIsLatchedTo(event.player).each{
				if(chickenCarryMap.containsKey(event.player)) {
					return
				} else {
					it.unLatch()
				}
			}
		}
		if(chickenBehaviorMap.get(event.critter) == "runs" && !chickenFleeList.contains(event.critter) && !chickenCarryMap.containsKey(event.player)) {
			event.critter.setBaseSpeed(450)
			event.critter.startPatrol() 
			chickenFleeList << event.critter
			event.player.centerPrint("The chicken clucks away as you try to pick it up!")
		}
		if (chickenBehaviorMap.get(event.critter) == "wrangles" && !event.critter.isLatched() && !chickenCarryMap.containsKey(event.player)) {
			event.critter.latchOnTo(event.player)
			chickenCarryMap.put(event.player, event.critter)
			event.player.centerPrint("You picked up the chicken, now take it to Rubella.")
		
			myManager.schedule(120) {
				if(event.critter.isLatched()) {
					event.critter.unLatch()
					//event.critter.dispose()
					//chickenCarryMap.remove(event.player)
				}
			}
		
			event.critter.onDrop { dropEvent ->
				if(dropEvent.reason == "dazed" || dropEvent.reason == "zoned" || dropEvent.reason =="script") {
					dropEvent.actor.startWander(true)
					chickenCarryMap.remove(dropEvent.dropper)
					dropEvent.actor?.dispose()
					spawnLocations << chickenSpawnMap.get(dropEvent.actor)
					//println "#### chickenCarryMap = ${chickenCarryMap} ####"
				}
			}
		}
	} )
}

spawnCheck()

//-----------------------------------------------------------------------------------------------------------
//Default conversation when player visits Rubella before earning Bill's trust                                
//-----------------------------------------------------------------------------------------------------------
def rubellaDefault = Rubella.createConversation("rubellaDefault", true, "!QuestStarted_75", "!QuestStarted_67", "!QuestCompleted_67", "!Z3_RancherBill_HelpingLarry_Active")

def default1 = [id:1]
default1.npctext = "Howdeee sweetheart. I'm sorry, I'd love to chat but Bill won't let me talk to anyone he doesn't trust. Ya better hurry up and get off the farm before he catches you wanderin' around."
default1.options = []
default1.options << [text:"I'm just looking for something to do.", result: 3]
default1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 224, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 224)
			Rubella.say("'Course I have a freebie fer you sug. Jus' don't tell Bill. He'd be powerful mad if he heard I was givin' ya stuff fer nothing.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Rubella today. Try again tomorrow!" )
		}
	})
}, result: DONE]
rubellaDefault.addDialog(default1, Rubella)

def default2 = [id:2]
default2.playertext = "I'm just looking for something to do. What am I supposed to do if people won't even talk to me?"
default2.result = 3
rubellaDefault.addDialog(default2, Rubella)

def default3 = [id:3]
default3.npctext = "Hmm... maybe you should try talking to Larry, sugar. Bill seems to trust him. I'm sure if Larry likes you Bill will trust you."
default3.flag = "Z3_RancherBill_HelpingLarry_Active"
rubellaDefault.addDialog(default3, Rubella)

//-----------------------------------------------------------------------------------------------------------
//Default conversation when player is on Helping Larry quest                                                 
//-----------------------------------------------------------------------------------------------------------
def rubellaLarry = Rubella.createConversation("rubellaLarry", true, "Z3_RancherBill_HelpingLarry_Active", "!QuestCompleted_76")

def larry1 = [id:1]
larry1.npctext = "Sugar, you'd better go talk to Larry and get off the ranch before Bill notices..."
larry1.playertext = "Do you have any freebies available today?"
larry1.exec = { event ->
	checkDailyChance(event.player, 224, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 224)
			Rubella.say("'Course I have a freebie fer you sug. Jus' don't tell Bill. He'd be powerful mad if he heard I was givin' ya stuff fer nothing.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Rubella today. Try again tomorrow!" )
		}
	})
}
larry1.result = DONE
rubellaLarry.addDialog(larry1, Rubella)

//-----------------------------------------------------------------------------------------------------------
//Conversation after player has been given quest to tell Rubella about the shipment                          
//-----------------------------------------------------------------------------------------------------------
def rubellaShipment = Rubella.createConversation("rubellaShipment", true, "QuestStarted_75:3", "!QuestStarted_67", "!QuestCompleted_67")

def shipment1 = [id:1]
shipment1.npctext = "Howdeee sweetheart... does Bill know yer here?"
shipment1.options = []
shipment1.options << [text:"Of course, he sent me over here to find out why you haven't sent the chickens for the shipment.", result: 3]
shipment1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 224, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 224)
			Rubella.say("'Course I have a freebie fer you sug. Jus' don't tell Bill. He'd be powerful mad if he heard I was givin' ya stuff fer nothing.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Rubella today. Try again tomorrow!" )
		}
	})
}, result: DONE]
rubellaShipment.addDialog(shipment1, Rubella)

/*def shipment2 = [id:2]
shipment2.playertext = "Of course, he sent me over here to find out why you don't have the shipment ready. He needs to send it out soon and wants you to get it ready as soon as possible."
shipment2.result = 3
rubellaShipment.addDialog(shipment2, Rubella)*/

def shipment3 = [id:3]
shipment3.npctext = "Oh no, I don't have them all! I still need ten more chickens! I've been tryin' to catch 'em but every time I get near they run away. Bill's gonna be soooo angry with me."
shipment3.options = []
shipment3.options << [text:"Maybe I can help you, Rubella.", result: 4]
shipment3.options << [text:"Sounds like you've got your hands full! I'll get out of your way.", result: 5]
rubellaShipment.addDialog(shipment3, Rubella)

def shipment4 = [id:4]
shipment4.npctext = "Really? Thanks, sweetheart! I have always depended on the kindness of strangers. I think you'll find most of the chickens all along the western end of the farm, between the barn and here."
shipment4.quest = 67
shipment4.exec = { event ->
	event.player.setPlayerVar("chickensCaught", 0)
}
shipment4.result = DONE
rubellaShipment.addDialog(shipment4, Rubella)

def shipment5 = [id:5]
shipment5.npctext = "I don't suppose you'd be willing to help little ol' me out?"
shipment5.options = []
shipment5.options << [text:"Sure, I can help you catch a chicken.", result: 4]
shipment5.options << [text:"No way, Rubella.", result: DONE]
rubellaShipment.addDialog(shipment5, Rubella)

//-----------------------------------------------------------------------------------------------------------
//Conversation while player is on Chicken Wranglin' quest                                                    
//-----------------------------------------------------------------------------------------------------------
def rubellaWranglinActive = Rubella.createConversation("rubellaWranglinActive", true, "QuestStarted_67:2")

def wranglinActive1 = [id:1]
wranglinActive1.npctext = "Howdeee sweetheart!"
wranglinActive1.exec = { event -> 
	if(chickenCarryMap.containsKey(event.player)) {
		if(event.player.getPlayerVar("chickensCaught") >= 9) {
			chickenCarryMap.get(event.player).latchOnTo(Rubella)
			myManager.schedule(3) {
				chickenCarryMap.get(event.player).unLatch()
				//chickenCarryMap.get(event.player).dispose()
				//chickenCarryMap.remove(event.player)
			}
			event.player.setQuestFlag(GLOBAL, "Z3_Rubella_ChickenEnd")
			Rubella.pushDialog(event.player, "rubellaWranglinEnd")
		}
		if(event.player.getPlayerVar("chickensCaught") < 9) {
			chickenCarryMap.get(event.player).latchOnTo(Rubella)
			myManager.schedule(3) {
				chickenCarryMap.get(event.player).dispose()
				chickenCarryMap.remove(event.player)
			}
			event.player.setQuestFlag(GLOBAL, "Z3_Rubella_ChickenAdvance")
			Rubella.pushDialog(event.player, "rubellaWranglinAdvance")
		}
	} else {
		event.player.setQuestFlag(GLOBAL, "Z3_Rubella_ChickenContinue")
		Rubella.pushDialog(event.player, "rubellaWranglinContinue")
	}
}
rubellaWranglinActive.addDialog(wranglinActive1, Rubella)

//-----------------------------------------------------------------------------------------------------------
//Conversation after player hasn't wrangled chickens for Rubella                                             
//-----------------------------------------------------------------------------------------------------------
def rubellaWranglinContinue = Rubella.createConversation("rubellaWranglinContinue", true, "QuestStarted_67:2", "Z3_Rubella_ChickenContinue")

def wranglinContinue1 = [id:1]
wranglinContinue1.npctext = "Do you think, maybe, you could hurry up with that chicken? I don't want Bill to be mad at me. Just track 'em down near the hay bales to the south."
wranglinContinue1.flag = "!Z3_Rubella_ChickenContinue"
wranglinContinue1.result = DONE
rubellaWranglinContinue.addDialog(wranglinContinue1, Rubella)

//-----------------------------------------------------------------------------------------------------------
//Conversation after player hasn't wrangled chickens for Rubella                                             
//-----------------------------------------------------------------------------------------------------------
def rubellaWranglinAdvance = Rubella.createConversation("rubellaWranglinAdvance", true, "QuestStarted_67:2", "Z3_Rubella_ChickenAdvance")

def wranglinAdvance1 = [id:1]
wranglinAdvance1.npctext = "Omigod! You brung me a chicken. Thanks, sugar!"
wranglinAdvance1.flag = "!Z3_Rubella_ChickenAdvance"
wranglinAdvance1.exec = { event ->
	event.player.addToPlayerVar("chickensCaught", 1)
	event.player.centerPrint("Chickens Wrangled ${event.player.getPlayerVar("chickensCaught").intValue()}/10")
}
wranglinAdvance1.result = DONE
rubellaWranglinAdvance.addDialog(wranglinAdvance1, Rubella)

//-----------------------------------------------------------------------------------------------------------
//Conversation after player has wrangled chickens for Rubella                                                
//-----------------------------------------------------------------------------------------------------------
def rubellaWranglinEnd = Rubella.createConversation("rubellaWranglinEnd", true, "QuestStarted_67:2", "Z3_Rubella_ChickenEnd")

def wranglinEnd1 = [id:1]
wranglinEnd1.npctext = "The last chicken! Omigod thanks, sugar! I'll send these right over to Bill."
wranglinEnd1.quest = 67
wranglinEnd1.flag = "!Z3_Rubella_ChickenEnd"
wranglinEnd1.exec = { event -> 
	event.player.addToPlayerVar("chickensCaught", 1)
	event.player.centerPrint("Chickens Wrangled ${event.player.getPlayerVar("chickensCaught").intValue()}/10")
	event.player.updateQuest(75, "Rubella-VQS")
	event.player.removeMiniMapQuestActorName("Rubella-VQS")
	event.player.deletePlayerVar("chickensCaught")
}
wranglinEnd1.result = DONE
rubellaWranglinEnd.addDialog(wranglinEnd1, Rubella)

//SAFETY NET CODE TO FIX PLAYER DATA SCREW-UPS *IF* THEY GOT MESSED UP ON THE "KATSUMI" QUEST
myManager.onEnter( myRooms.BF1_2 ) { event ->
	//If the user is on the KATSUMI quest, and has already talked to Katsumi, (which shouldn't be possible), then complete the quest
	// THIS IS SAFETY NET TO FIX ERRORS ON PLAYERS THAT HAVE SCREWED-UP DATA
	if( isPlayer( event.actor ) && event.actor.isOnQuest( 68, 2 ) && event.actor.hasQuestFlag( GLOBAL, "Z05KatsumiAlreadyMet" ) ) {
		event.actor.updateQuest( 68, "Katsumi-VQS" )
		event.actor.centerPrint( "You complete the KATSUMI quest." )
	}	
	if(isPlayer(event.actor) && event.actor.isDoneQuest(68) && event.actor.hasQuestFlag(GLOBAL, "Z3_Bill_Katsumi_Redirect")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z3_Bill_Katsumi_Redirect")
	}
}


//-----------------------------------------------------------------------------------------------------------
//Conversation after Chicken Wranglin' is complete                                                           
//-----------------------------------------------------------------------------------------------------------
def rubellaWranglinComplete = Rubella.createConversation("rubellaWranglinComplete", true, "QuestCompleted_67", "!QuestStarted_65", "!Z3_Bill_Katsumi_Redirect", "!QuestStarted_68", "!QuestCompleted_68")

def wranglinComplete1 = [id:1]
wranglinComplete1.npctext = "Howdeee sweetheart! Thanks again fer helpin' with the chicken. Is there anything I can do fer ya?"
wranglinComplete1.options = []
wranglinComplete1.options << [text:"Where can I find Purvis?", result: 2]
wranglinComplete1.options << [text:"No, nothing for now. Thanks.", result: 3]
wranglinComplete1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 224, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 224)
			Rubella.say("'Course I have a freebie fer you sug. Jus' don't tell Bill. He'd be powerful mad if he heard I was givin' ya stuff fer nothing.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Rubella today. Try again tomorrow!" )
		}
	})
}, result: DONE]
rubellaWranglinComplete.addDialog(wranglinComplete1, Rubella)

def wranglinComplete2 = [id:2]
wranglinComplete2.npctext = "Why on earth would you be looking fer Purvis, sugar? He's such a dimwit. I just avoid him, but if you really need to find him he'll be wanderin' the field lookin' with the cows..."
wranglinComplete2.result = DONE
rubellaWranglinComplete.addDialog(wranglinComplete2, Rubella)

def wranglinComplete3 = [id:3]
wranglinComplete3.npctext = "Okay, sugar. Talk to you again soon!"
wranglinComplete3.result = DONE
rubellaWranglinComplete.addDialog(wranglinComplete3, Rubella)

//-----------------------------------------------------------------------------------------------------------
//Conversation when player is on Purvis Likes Rubella quest                                                  
//-----------------------------------------------------------------------------------------------------------
def rubellaPurvisLikes = Rubella.createConversation("rubellaPurvisLikes", true, "QuestStarted_65:2")

def purvisLikes1 = [id:1]
purvisLikes1.npctext = "Hey again, sugar. What can I do fer ya?"
purvisLikes1.options = []
purvisLikes1.options << [text:"Rubella, Purvis wrote a poem for you. Can I read it to you?", result: 2]
purvisLikes1.options << [text:"What do you think of Purvis?", result: 7]
rubellaPurvisLikes.addDialog(purvisLikes1, Rubella)

def purvisLikes2 = [id:2]
purvisLikes2.npctext = "Purvis? A poem? Uhg! That boy is a little slow on the uptake. I've tried subtly letting him know that I'm not interested but he just doesn't seem to get the message."
purvisLikes2.result = 3
rubellaPurvisLikes.addDialog(purvisLikes2, Rubella)

def purvisLikes3 = [id:3]
purvisLikes3.playertext = "Oh, come on, he's a nice guy. Can't you give him a chance?"
purvisLikes3.result = 4
rubellaPurvisLikes.addDialog(purvisLikes3, Rubella)

def purvisLikes4 = [id:4]
purvisLikes4.npctext = "Well... he is nice. Okay, read me his poem."
purvisLikes4.options = []
purvisLikes4.options << [text:"Roses is red, mooers go MOO! Purvis is a fella and he likes Rubella.", result: 5]
purvisLikes4.options << [text:"You are my shining star, you are my heart and soul. Without you I am nothing, but with you I am whole.", result: 6]
rubellaPurvisLikes.addDialog(purvisLikes4, Rubella)

def purvisLikes5 = [id:5]
purvisLikes5.npctext = "You see what I mean? Tell Purvis I'm flattered, but no thanks!"
purvisLikes5.flag = "Z3_Purvis_PurvisLikesRubella_BadPoem"
purvisLikes5.quest = 65
purvisLikes5.exec = { event ->
	event.player.removeMiniMapQuestActorName("Rubella-VQS")
	event.player.addMiniMapQuestActorName("Purvis-VQS")
}
purvisLikes5.result = DONE
rubellaPurvisLikes.addDialog(purvisLikes5, Rubella)

def purvisLikes6 = [id:6]
purvisLikes6.npctext = "Purvis wrote that? About me? Maybe I was wrong about him. Tell him I think his poem is great. He should write another."
purvisLikes6.flag = "Z3_Purvis_PurvisLikesRubella_GoodPoem"
purvisLikes6.quest = 65
purvisLikes6.exec = { event ->
	event.player.removeMiniMapQuestActorName("Rubella-VQS")
	event.player.addMiniMapQuestActorName("Purvis-VQS")
}
purvisLikes6.result = DONE
rubellaPurvisLikes.addDialog(purvisLikes6, Rubella)

def purvisLikes7 = [id:7]
purvisLikes7.npctext = "Honestly, I try not to. I think the poor boy has a little thing fer me but I need someone a bit more... intellectual."
purvisLikes7.result = 8
rubellaPurvisLikes.addDialog(purvisLikes7, Rubella)

def purvisLikes8 = [id:8]
purvisLikes8.playertext = "Intellectual? Like a poet?"
purvisLikes8.result = 9
rubellaPurvisLikes.addDialog(purvisLikes8, Rubella)

def purvisLikes9 = [id:9]
purvisLikes9.npctext = "Yes! Oooh! A poet would be great!"
purvisLikes9.result = 10
rubellaPurvisLikes.addDialog(purvisLikes9, Rubella)

def purvisLikes10 = [id:10]
purvisLikes10.playertext = "It just so happens that Purvis wrote a poem for you. Would you like to hear it?"
purvisLikes10.result = 2
rubellaPurvisLikes.addDialog(purvisLikes10, Rubella)

//-----------------------------------------------------------------------------------------------------------
//Conversation to give Katsumi quest                                                                         
//-----------------------------------------------------------------------------------------------------------
def rubellaKatsumiGive = Rubella.createConversation("rubellaKatsumiGive", true, "Z3_Bill_Katsumi_Redirect", "!QuestStarted_68", "!QuestCompleted_68" )

def katsumiGive1 = [id:1]
katsumiGive1.npctext = "Howdeee sweetheart! Say, did Bill mention I was lookin' fer you? I really could use another favor, if'n you have time."
katsumiGive1.options = []
katsumiGive1.options << [text:"Of course I'll help you, Rubella.", result: 2]
katsumiGive1.options << [text:"Ask me later. I'm too busy right now.", result: 4]
katsumiGive1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 224, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 224)
			Rubella.say("'Course I have a freebie fer you sug. Jus' don't tell Bill. He'd be powerful mad if he heard I was givin' ya stuff fer nothing.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Rubella today. Try again tomorrow!" )
		}
	})
}, result: DONE]
rubellaKatsumiGive.addDialog(katsumiGive1, Rubella)

def katsumiGive2 = [id:2]
katsumiGive2.npctext = "Thanks again, sugar. I'd be lost without you. Anyway, with all the Animated running wild around here, and the men-folk running even wilder with panic, I haven't had a chance to check up on Katsumi! We write each all the time but I haven't heard hide nor hair from her since the Animated started showin' up. I sure am worried 'bout her. Please go track her down in Zen Gardens to the north... she's usually in the shrine."
katsumiGive2.result = 3
rubellaKatsumiGive.addDialog(katsumiGive2, Rubella)

def katsumiGive3 = [id:3]
katsumiGive3.playertext = "I'll go check up on Katsumi immediately."
katsumiGive3.flag = "!Z3_Bill_Katsumi_Redirect"
katsumiGive3.quest = 68
katsumiGive3.exec = { event ->
	event.player.removeMiniMapQuestActorName("Rubella-VQS")
	event.player.addMiniMapQuestActorName("Katsumi-VQS")
}
katsumiGive3.result = DONE
rubellaKatsumiGive.addDialog(katsumiGive3, Rubella)

def katsumiGive4 = [id:4]
katsumiGive4.npctext = "Awww, sure I can't convince you, sugar?"
katsumiGive4.result = DONE
rubellaKatsumiGive.addDialog(katsumiGive4, Rubella)

//-----------------------------------------------------------------------------------------------------------
//Conversation when player is on Katsumi quest                                                               
//-----------------------------------------------------------------------------------------------------------
def rubellaKatsumiActive = Rubella.createConversation("rubellaKatsumiActive", true, "QuestStarted_68:2")

def katsumiActive1 = [id:1]
katsumiActive1.npctext = "Howdeee sweatheart! Please check on Katsumi soon, I'm powerful worried about her."
katsumiActive1.playertext = "Do you have any freebies available today?"
katsumiActive1.exec = { event ->
	checkDailyChance(event.player, 224, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 224)
			Rubella.say("'Course I have a freebie fer you sug. Jus' don't tell Bill. He'd be powerful mad if he heard I was givin' ya stuff fer nothing.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Rubella today. Try again tomorrow!" )
		}
	})
}
katsumiActive1.result = DONE
rubellaKatsumiActive.addDialog(katsumiActive1, Rubella)

//-----------------------------------------------------------------------------------------------------------
//Conversation when player has verified that Katsumi is ok                                                   
//-----------------------------------------------------------------------------------------------------------
def rubellaKatsumiEnd = Rubella.createConversation("rubellaKatsumiEnd", true, "QuestStarted_68:3")

def katsumiEnd1 = [id:1]
katsumiEnd1.npctext = "Hey sugar, did you get a chance to check up on Katsumi yet?"
katsumiEnd1.result = 2
rubellaKatsumiEnd.addDialog(katsumiEnd1, Rubella)

def katsumiEnd2 = [id:2]
katsumiEnd2.playertext = "I sure did. She seems to be doing just fine."
katsumiEnd2.result = 3
rubellaKatsumiEnd.addDialog(katsumiEnd2, Rubella)

def katsumiEnd3 = [id:3]
katsumiEnd3.npctext = "That's a mighty relief! Did her uncle, Kin, come protect her? Must be nice havin' a ninja lookin' after her."
katsumiEnd3.result = 4
rubellaKatsumiEnd.addDialog(katsumiEnd3, Rubella)

def katsumiEnd4 = [id:4]
katsumiEnd4.playertext = "No, actually there seems to be something about the shrine that just repells the Animated altogether. She's as safe as can be as long as she stays in the shrine."
katsumiEnd4.result = 5
rubellaKatsumiEnd.addDialog(katsumiEnd4, Rubella)

def katsumiEnd5 = [id:5]
katsumiEnd5.npctext = "Imagine that! Here I am frettin' and fussin' over the girl and she's safe as safe can be. I shoulda guessed, that Katsumi is too resourceful to let herself fall into trouble. Well, thanks fer checkin' up on her just the same."
katsumiEnd5.quest = 68
katsumiEnd5.exec = { event -> event.player.removeMiniMapQuestActorName("Rubella-VQS") }
katsumiEnd5.result = DONE
rubellaKatsumiEnd.addDialog(katsumiEnd5, Rubella)

//-----------------------------------------------------------------------------------------------------------
//Final conversation for Rubella                                                                             
//-----------------------------------------------------------------------------------------------------------
def rubellaFinal = Rubella.createConversation("rubellaFinal", true, "QuestCompleted_65", "!QuestStarted_68", "!Z3_Bill_Katsumi_Redirect")

def final1 = [id:1]
final1.npctext = "Hey sugar! Thanks again fer all your help."
final1.playertext = "Do you have any freebies available today?"
final1.exec = { event ->
	checkDailyChance(event.player, 224, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 224)
			Rubella.say("'Course I have a freebie fer you sug. Jus' don't tell Bill. He'd be powerful mad if he heard I was givin' ya stuff fer nothing.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Rubella today. Try again tomorrow!" )
		}
	})
}
final1.result = DONE
rubellaFinal.addDialog(final1, Rubella)