//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner One - Cleared for take-off
def spawner1 = myRooms.BF1_204.spawnSpawner( "bf1_204_Spawner1", "fluff", 1)
spawner1.setPos( 800, 80 )
spawner1.setHome( "BF1_204", 800, 80 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 2.2 )

// Spawner One - Cleared for take-off
/*def spawner2 = myRooms.BF1_204.spawnSpawner( "bf1_204_spawner2", "garlic", 1)
spawner2.setPos( 900, 365 )
spawner2.setHome( "BF1_204", 900, 365 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 2.2 )

// Spawner One - Cleared for take-off
def spawner3 = myRooms.BF1_204.spawnSpawner( "bf1_204_spawner3", "garlic", 1)
spawner3.setPos( 880, 595 )
spawner3.setHome( "BF1_204", 880, 595 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 2.2 )*/

// Spawner One - Cleared for take-off
def spawner4 = myRooms.BF1_204.spawnSpawner( "bf1_204_spawner4", "fluff", 1)
spawner4.setPos( 725, 200 )
spawner4.setHome( "BF1_204", 725, 200 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 50 , 70 )
spawner4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 2.2 )

//Alliances
spawner1.allyWithSpawner( spawner4 )

//Spawning
spawner1.forceSpawnNow()
spawner4.forceSpawnNow()