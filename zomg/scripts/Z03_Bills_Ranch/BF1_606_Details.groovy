import com.gaiaonline.mmo.battle.script.*;

//----------------------------------------------------
// SPAWNERS AND PATROL PATHS                          
//----------------------------------------------------

def spawner = myRooms.BF1_606.spawnSpawner( "BF1_606_Spawner", "fluff", 1)
spawner.setPos( 400, 105 )
spawner.setInitialMoveForChildren( "BF1_606", 785, 120 )
spawner.setHome( "BF1_606", 785, 120 )
spawner.setSpawnWhenPlayersAreInRoom( true )
spawner.setWaitTime( 50 , 70 )
spawner.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner.childrenWander( true )
spawner.setMonsterLevelForChildren( 1.7 )

def spawner2 = myRooms.BF1_606.spawnSpawner( "BF1_606_Spawner2", "fluff", 1)
spawner2.setPos( 400, 95 )
spawner2.setInitialMoveForChildren( "BF1_606", 150, 150 )
spawner2.setHome( "BF1_606", 150, 150 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 1.7 )

def spawner3 = myRooms.BF1_606.spawnSpawner( "BF1_606_Spawner3", "fluff", 1)
spawner3.setPos( 400, 85 )
spawner3.setInitialMoveForChildren( "BF1_606", 440, 340 )
spawner3.setHome( "BF1_606", 440, 340 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 1.7 )

//Alliances
spawner.allyWithSpawner( spawner2 )
spawner.allyWithSpawner( spawner3 )
spawner2.allyWithSpawner( spawner3 )

//Spawning
spawner.forceSpawnNow()
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()