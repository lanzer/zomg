//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner One - Cleared for take-off
/*def spawner1 = myRooms.BF1_206.spawnSpawner( "bf1_206_spawner1", "garlic", 1)
spawner1.setPos( 100, 570 )
spawner1.setHome( "BF1_206", 100, 570 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren(50, 150, 2, 8, 250 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 1.6 )*/

// Spawner Three - Cleared for take-off
def spawner2 = myRooms.BF1_206.spawnSpawner( "bf1_206_spawner2", "fluff", 1)
spawner2.setPos( 340, 410 )
spawner2.setHome( "BF1_206", 340, 410 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 1.6 )

// Spawner Three - Cleared for take-off
/*def spawner3 = myRooms.BF1_206.spawnSpawner( "bf1_206_spawner3", "garlic", 1)
spawner3.setPos( 290, 280 )
spawner3.setHome( "BF1_206", 190, 280 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 250 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 1.6 )

// Spawner Three - Cleared for take-off
def spawner4 = myRooms.BF1_206.spawnSpawner( "bf1_206_spawner4", "garlic", 1)
spawner4.setPos( 170, 105 )
spawner4.setHome( "BF1_206", 170, 105 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 50 , 70 )
spawner4.setWanderBehaviorForChildren( 50, 150, 2, 8, 250 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 1.6 )*/

// Spawner Three - Cleared for take-off
def spawner5 = myRooms.BF1_206.spawnSpawner( "bf1_206_spawner5", "fluff", 1)
spawner5.setPos( 480, 180 )
spawner5.setHome( "BF1_206", 480, 180 )
spawner5.setSpawnWhenPlayersAreInRoom( true )
spawner5.setWaitTime( 50 , 70 )
spawner5.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner5.childrenWander( true )
spawner5.setMonsterLevelForChildren( 1.6 )

//Alliances
spawner2.allyWithSpawner( spawner5 )

//Spawning
spawner2.forceSpawnNow()
spawner5.forceSpawnNow()