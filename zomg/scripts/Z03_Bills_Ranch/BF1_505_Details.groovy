import com.gaiaonline.mmo.battle.script.*;

//----------------------------------------------------
// SPAWNERS AND PATROL PATHS                          
//----------------------------------------------------
def spawner2 = myRooms.BF1_505.spawnSpawner( "BF1_505_Spawner2", "fluff", 1)
spawner2.setPos( 580, 310 )
spawner2.setInitialMoveForChildren( "BF1_505", 750, 570 )
spawner2.setHome( "BF1_505", 750, 570 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 1.7 )

def spawner3 = myRooms.BF1_505.spawnSpawner( "BF1_505_Spawner3", "fluff", 1)
spawner3.setPos( 580, 320 )
spawner3.setInitialMoveForChildren( "BF1_505", 930, 330 )
spawner3.setHome( "BF1_505", 930, 330 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 1.7 )

//Alliances
spawner2.allyWithSpawner( spawner3 )

//Spawning 
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()