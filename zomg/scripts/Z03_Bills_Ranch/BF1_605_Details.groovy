import com.gaiaonline.mmo.battle.script.*;

//----------------------------------------------------
// SPAWNERS AND PATROL PATHS                          
//----------------------------------------------------

def spawner1 = myRooms.BF1_605.spawnSpawner( "BF1_605_Fluff1", "fluff", 1)
spawner1.setPos( 260, 565 )
spawner1.setInitialMoveForChildren( "BF1_605", 150, 300 )
spawner1.setHome( "BF1_605", 150, 300 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 1.8 )

def spawner3 = myRooms.BF1_605.spawnSpawner( "BF1_605_Spawner3", "mosquito", 1)
spawner3.setPos( 260, 545 )
spawner3.setInitialMoveForChildren( "BF1_605", 440, 440 )
spawner3.setHome( "BF1_605", 440, 440 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 1.8 )

//Spawning
spawner1.forceSpawnNow()
spawner3.forceSpawnNow()