//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//Purvis Patrol Route
def purvisPatrol = makeNewPatrol()
purvisPatrol.addPatrolPoint( "BF1_403", 850, 215, 30 )
purvisPatrol.addPatrolPoint( "BF1_404", 285, 160, 30 )
purvisPatrol.addPatrolPoint( "BF1_303", 765, 175, 30 )
purvisPatrol.addPatrolPoint( "BF1_103", 700, 500, 30 )
purvisPatrol.addPatrolPoint( "BF1_202", 780, 430, 30 )
purvisPatrol.addPatrolPoint( "BF1_302", 930, 330, 30 )
purvisPatrol.addPatrolPoint( "BF1_303", 190, 575, 30 )

//Purvis
def Purvis = spawnNPC("Purvis-VQS", myRooms.BF1_303, 530, 550)
Purvis.setDisplayName( "Purvis" )
//Purvis.setPatrol( purvisPatrol )
//Purvis.startPatrol()
//Purvis.stopPatrol()

onQuestStep(66, 2) { event -> event.player.addMiniMapQuestActorName("Purvis-VQS") }
onQuestStep(280, 2) { event -> event.player.addMiniMapQuestActorName("Purvis-VQS") }

myManager.onExit(myRooms.BF1_303) { event ->
	if(isPlayer(event.actor) && event.actor.isDoneQuest(65)) { event.actor.removeMiniMapQuestActorName("Purvis-VQS") }
}

//-----------------------------------------------------------------------------------------------------------
//Default conversation when player visits Purvis before earning Bill's trust                                 
//-----------------------------------------------------------------------------------------------------------
def purvisDefault = Purvis.createConversation("purvisDefault", true, "!QuestStarted_66", "!QuestCompleted_66", "!QuestStarted_75", "!Z3_RancherBill_HelpingLarry_Active")

def default1 = [id:1]
default1.npctext = "Buh-duh... Garsh, I dun' think pa would want me talkin' to ya."
default1.result = 2
purvisDefault.addDialog(default1, Purvis)

def default2 = [id:2]
default2.playertext = "I'm just looking for something to do. What am I supposed to do if people won't even talk to me?"
default2.result = 3
purvisDefault.addDialog(default2, Purvis)

def default3 = [id:3]
default3.npctext = "Duh... Go talk to Larry at the station! Pa trusts 'im. If Larry'll vou.. speak for ya, pa'll trust ya."
default3.flag = "Z3_RancherBill_HelpingLarry_Active"
purvisDefault.addDialog(default3, Purvis)

//-----------------------------------------------------------------------------------------------------------
//Default conversation when player is on Helping Larry quest                                                 
//-----------------------------------------------------------------------------------------------------------
def purvisLarry = Purvis.createConversation("purvisLarry", true, "Z3_RancherBill_HelpingLarry_Active", "!QuestCompleted_76")

def larry1 = [id:1]
larry1.npctext = "Duh... you better go talk to Larry before pa catches ya on the farm."
larry1.result = DONE
purvisLarry.addDialog(larry1, Purvis)

//-----------------------------------------------------------------------------------------------------------
//Conversation after player has been given quest to tell Purvis about the shipment                           
//-----------------------------------------------------------------------------------------------------------
def purvisShipment = Purvis.createConversation("purvisShipment", true, "QuestStarted_75:3", "!QuestStarted_66", "!QuestCompleted_66")

def shipment1 = [id:1]
shipment1.npctext = "Dur, pa's ready fer the shipment? Garsh... I been so busy rightin' them silly mooers I can't find no time to milk 'em. Buh... d'ya think ya could 'elp me out?"
shipment1.options = []
shipment1.options << [text:"Mooers? Do you mean cows? I guess I could milk them, but where are the cows?", result: 2]
shipment1.options << [text:"I'll think about it and get back to you, Purvis.", result: 3]
purvisShipment.addDialog(shipment1, Purvis)

def shipment2 = [id:2]
shipment2.npctext = "Oh, they is all over the farm... most of 'em are in the field though. If'n ya find one whats been tipped ya can't milk it. Don't worry, I'll be rightin' any I see and then ya can milk 'em."
shipment2.quest = 66
shipment2.exec = { event -> 
	event.player.removeMiniMapQuestActorName("Purvis-VQS") 
	event.player.setPlayerVar("mooersMilked", 0)
}
shipment2.result = DONE
purvisShipment.addDialog(shipment2, Purvis)

def shipment3 = [id:3]
shipment3.npctext = "Buh duh... Garsh."
shipment3.result = DONE
purvisShipment.addDialog(shipment3, Purvis)

//-----------------------------------------------------------------------------------------------------------
//Conversation while player is on Milkin' Mooers quest                                                       
//-----------------------------------------------------------------------------------------------------------
def purvisMooersActive = Purvis.createConversation("purvisMooersActive", true, "QuestStarted_66:2")

def mooersActive1 = [id:1]
mooersActive1.npctext = "Dur, did ya finish milkin' them mooers?"
mooersActive1.result = DONE
purvisMooersActive.addDialog(mooersActive1, Purvis)

//-----------------------------------------------------------------------------------------------------------
//Conversation when player finishes Milkin' Mooers quest                                                     
//-----------------------------------------------------------------------------------------------------------
def purvisMooersEnd = Purvis.createConversation("purvisMooersEnd", true, "QuestStarted_66:3", "!QuestCompleted_67")

def mooersEnd1 = [id:1]
mooersEnd1.npctext = "Them mooers is milked! Buh dur... thanks!"
mooersEnd1.quest = 66
mooersEnd1.exec = { event -> 
	event.player.removeMiniMapQuestActorName("Purvis-VQS")
	event.player.updateQuest(75, "Purvis-VQS") 
	event.player.deletePlayerVar("mooersMilked")
}
mooersEnd1.result = DONE
purvisMooersEnd.addDialog(mooersEnd1, Purvis)

//-----------------------------------------------------------------------------------------------------------
//Milkin' Mooers - Complete                                                                                  
//-----------------------------------------------------------------------------------------------------------
def purvisMooersComplete = Purvis.createConversation("purvisMooersComplete", true, "QuestCompleted_66", "!QuestStarted_67", "!QuestCompleted_67", "!QuestStarted_65", "!QuestCompleted_65")

def mooersComplete1 = [id:1]
mooersComplete1.npctext = "Gawrsh, I sure owe ya fer 'elpin' with the mooers. Is there anythin' I can do fer ya?"
mooersComplete1.options = []
mooersComplete1.options << [text:"Any idea where I can find Rubella?", result: 2]
mooersComplete1.options << [text:"Nothing for now.", result: 3]
purvisMooersComplete.addDialog(mooersComplete1, Purvis)

def mooersComplete2 = [id:2]
mooersComplete2.npctext = "Dur... Rubella! She's purty. You can find 'er by the farm'ouse, just nor'west of 'ere. Tell 'er I said hullo!"
mooersComplete2.result = DONE
purvisMooersComplete.addDialog(mooersComplete2, Purvis)

def mooersComplete3 = [id:3]
mooersComplete3.npctext = "Dur, take care then."
mooersComplete3.result = DONE
purvisMooersComplete.addDialog(mooersComplete3, Purvis)

//-----------------------------------------------------------------------------------------------------------
//Purvis Likes Rubella - Get when Wranglin completed before Mooers                                           
//-----------------------------------------------------------------------------------------------------------
def purvisMooersLove = Purvis.createConversation("purvisMooersLove", true, "QuestStarted_66:3", "QuestCompleted_67", "!QuestStarted_65", "!QuestCompleted_65")

def mooersLove1 = [id:1]
mooersLove1.npctext = "Them mooers is milked! Buh dur... thanks! Say... yer pretty helpful. Maybe ya could help me wit' somethin' else?"
mooersLove1.result = 2
purvisMooersLove.addDialog(mooersLove1, Purvis)

def mooersLove2 = [id:2]
mooersLove2.playertext = "That depends. What do you need help with, Purvis?"
mooersLove2.result = 3
purvisMooersLove.addDialog(mooersLove2, Purvis)

def mooersLove3 = [id:3]
mooersLove3.npctext = "Garsh, have ya... have ya met Rubella? She's so purty. Every time I talk to 'er I get so ner... nerv... I can't talk good. I wrote 'er a poem... but I'm too skeered to read it to 'er."
mooersLove3.options = []
mooersLove3.options << [text:"I could tell it to her for you.", result: 4]
mooersLove3.options << [text:"I really don't feel like being in a love triangle right now.", result: 7]
purvisMooersLove.addDialog(mooersLove3, Purvis)

def mooersLove4 = [id:4]
mooersLove4.npctext = "Dur... okay. Roses is red, mooers go MOO! Purvis is a fella and he likes Rubella."
mooersLove4.quest = 65
mooersLove4.exec = { event -> 
	event.player.updateQuest(66, "Purvis-VQS")
	event.player.updateQuest(75, "Purvis-VQS")
	event.player.addMiniMapQuestActorName("Rubella-VQS")
}
mooersLove4.result = 5
purvisMooersLove.addDialog(mooersLove4, Purvis)

def mooersLove5 = [id:5]
mooersLove5.playertext = "That's... a nice poem. I'll go tell it to Rubella immediately."
mooersLove5.result = 6
purvisMooersLove.addDialog(mooersLove5, Purvis)

def mooersLove6 = [id:6]
mooersLove6.npctext = "Garsh, thanks a lot, %p! I hopen she likes it."
mooersLove6.result = DONE
purvisMooersLove.addDialog(mooersLove6, Purvis)

def mooersLove7 = [id:7]
mooersLove7.npctext = "Awww, shucks. How's she ever gonna know I like 'er?"
mooersLove7.flag = "Z3_Purvis_PurvisLikesRubella_Break"
mooersLove7.quest = 66
mooersLove7.exec = { event -> event.player.updateQuest(75, "Purvis-VQS") }
mooersLove7.result = DONE
purvisMooersLove.addDialog(mooersLove7, Purvis)

//-----------------------------------------------------------------------------------------------------------
//Purvis Likes Rubella - Get when Mooers completed before Wranglin                                           
//-----------------------------------------------------------------------------------------------------------
def purvisRubellaGet = Purvis.createConversation("purvisRubellaGet", true, "QuestCompleted_66", "QuestCompleted_67", "!Z3_Purvis_PurvisLikesRubella_Break", "!QuestStarted_65", "!QuestCompleted_65")

def rubellaGet1 = [id:1]
rubellaGet1.npctext = "Buh, 'ey there, %p. Think ya could help me wit' somethin' else?"
rubellaGet1.result = 2
purvisRubellaGet.addDialog(rubellaGet1, Purvis)

def rubellaGet2 = [id:2]
rubellaGet2.playertext = "That depends. What do you need help with, Purvis?"
rubellaGet2.result = 3
purvisRubellaGet.addDialog(rubellaGet2, Purvis)

def rubellaGet3 = [id:3]
rubellaGet3.npctext = "Garsh, have ya... have ya met Rubella? She's so purty. Every time I talk to 'er I get so ner... nerv... I can't talk good. I wrote 'er a poem... but I'm too skeered to read it to 'er."
rubellaGet3.options = []
rubellaGet3.options << [text:"I could tell it to her for you.", result: 4]
rubellaGet3.options << [text:"I really don't feel like being in a love triangle right now.", result: 7]
purvisRubellaGet.addDialog(rubellaGet3, Purvis)

def rubellaGet4 = [id:4]
rubellaGet4.npctext = "Dur... okay. Roses is red, mooers go MOO! Puvis is a fella and he likes Rubella."
rubellaGet4.result = 5
purvisRubellaGet.addDialog(rubellaGet4, Purvis)

def rubellaGet5 = [id:5]
rubellaGet5.playertext = "That's... a nice poem. I'll go tell it to Rubella immediately."
rubellaGet5.result = 6
purvisRubellaGet.addDialog(rubellaGet5, Purvis)

def rubellaGet6 = [id:6]
rubellaGet6.npctext = "Garsh, thanks a lot, %p! I hopen she likes it."
rubellaGet6.quest = 65
rubellaGet6.exec = { event -> event.player.addMiniMapQuestActorName("Rubella-VQS") }
rubellaGet6.result = DONE
purvisRubellaGet.addDialog(rubellaGet6, Purvis)

def rubellaGet7 = [id:7]
rubellaGet7.npctext = "Awww, shucks. How's she ever gonna know I like 'er?"
rubellaGet7.result = DONE
purvisRubellaGet.addDialog(rubellaGet7, Purvis)

//-----------------------------------------------------------------------------------------------------------
//Conversation if player initially declines Purvis Likes Rubella                                             
//-----------------------------------------------------------------------------------------------------------
def purvisRubellaBreak = Purvis.createConversation("purvisRubellaBreak", true, "Z3_Purvis_PurvisLikesRubella_Break", "!QuestCompleted_65")

def rubellaBreak1 = [id:1]
rubellaBreak1.npctext = "Rubella, my love!"
rubellaBreak1.options = []
rubellaBreak1.options << [text:"Oh fine! I'll read her the poem. Tell it to me.", result: 2]
rubellaBreak1.options << [text:"Stop trying to guilt me into reading Rubella your poem.", result: 4]
purvisRubellaBreak.addDialog(rubellaBreak1, Purvis)

def rubellaBreak2 = [id:2]
rubellaBreak2.npctext = "Dur... okay. Roses is red, mooers go MOO! Purvis is a fella and he likes Rubella."
rubellaBreak2.result = 3
purvisRubellaBreak.addDialog(rubellaBreak2, Purvis)

def rubellaBreak3 = [id:3]
rubellaBreak3.playertext = "That's... a nice poem. I'll go tell it to Rubella immediately."
rubellaBreak3.flag = "!Z3_Purvis_PurvisLikesRubella_Break"
rubellaBreak3.quest = 65
rubellaBreak3.exec = { event -> event.player.addMiniMapQuestActorName("Rubella-VQS") }
purvisRubellaBreak.addDialog(rubellaBreak3, Purvis)

def rubellaBreak4 = [id:4]
rubellaBreak4.npctext = "Awww, shucks. How's she ever gonna know I like 'er?"
rubellaBreak4.result = DONE
purvisRubellaBreak.addDialog(rubellaBreak4, Purvis)

//-----------------------------------------------------------------------------------------------------------
//Conversation while player is on Purvis Likes Rubella                                                       
//-----------------------------------------------------------------------------------------------------------
def purvisRubellaActive = Purvis.createConversation("purvisRubellaActive", true, "QuestStarted_65", "!QuestStarted_65:3", , "!QuestStarted_65:4", "!QuestCompleted_65")

def rubellaActive1 = [id:1]
rubellaActive1.npctext = "Garsh, I'm so nervous! I hope Rubella likes my poem."
rubellaActive1.result = DONE
purvisRubellaActive.addDialog(rubellaActive1, Purvis)

//-----------------------------------------------------------------------------------------------------------
//Good conversation when player returns after talking to Rubella                                             
//-----------------------------------------------------------------------------------------------------------
def purvisRubellaGood = Purvis.createConversation("purvisRubellaGood", true, "QuestStarted_65:3", "Z3_Purvis_PurvisLikesRubella_GoodPoem", "!QuestCompleted_65")

def rubellaGood1 = [id:1]
rubellaGood1.npctext = "Dur... did ya talk to Rubella? Did she like my poems?"
rubellaGood1.result = 2
purvisRubellaGood.addDialog(rubellaGood1, Purvis)

def rubellaGood2 = [id:2]
rubellaGood2.playertext = "Er... yeah. I read her a poem. She loved it! She wants you to write her another."
rubellaGood2.result = 3
purvisRubellaGood.addDialog(rubellaGood2, Purvis)

def rubellaGood3 = [id:3]
rubellaGood3.npctext = "Garsh! Poems is hard to write. Pa's good at thinkin'... mebbe he knows a poem fer Rubella?"
rubellaGood3.result = 4
purvisRubellaGood.addDialog(rubellaGood3, Purvis)

def rubellaGood4 = [id:4]
rubellaGood4.playertext = "I can go ask him if you like."
rubellaGood4.result = 5
purvisRubellaGood.addDialog(rubellaGood4, Purvis)

def rubellaGood5 = [id:5]
rubellaGood5.npctext = "Buh duh... yeah! Pa'll help!"
rubellaGood5.quest = 65
rubellaGood5.exec = { event -> 
	event.player.unsetQuestFlag(GLOBAL, "Z3_PurvisLiesRubella_GoodPoem")
	event.player.removeMiniMapQuestActorName("Purvis-VQS")
	event.player.addMiniMapQuestActorName("Rancher Bill-VQS")
}
rubellaGood5.result = DONE
purvisRubellaGood.addDialog(rubellaGood5, Purvis)

//-----------------------------------------------------------------------------------------------------------
//Bad conversation when player returns after talking to Rubella                                              
//-----------------------------------------------------------------------------------------------------------
def purvisRubellaBad = Purvis.createConversation("purvisRubellaBad", true, "QuestStarted_65:3", "Z3_Purvis_PurvisLikesRubella_BadPoem", "!QuestCompleted_65")

def rubellaBad1 = [id:1]
rubellaBad1.npctext = "Dur... did ya talk to Rubella? Did she like my poems?"
rubellaBad1.result = 2
purvisRubellaBad.addDialog(rubellaBad1, Purvis)

def rubellaBad2 = [id:2]
rubellaBad2.playertext = "No. She didn't care for it, Purvis."
rubellaBad2.result = 3
purvisRubellaBad.addDialog(rubellaBad2, Purvis)

def rubellaBad3 = [id:3]
rubellaBad3.npctext = "Dar! Noooooooooo! Rubella!"
rubellaBad3.result = 4
purvisRubellaBad.addDialog(rubellaBad3, Purvis)

def rubellaBad4 = [id:4]
rubellaBad4.playertext = "I'm sorry Purvis. Maybe you should give up."
rubellaBad4.result = 7
purvisRubellaBad.addDialog(rubellaBad4, Purvis)

/*def rubellaBad5 = [id:5]
rubellaBad5.npctext = "But I love 'er!"
rubellaBad5.result = 6
purvisRubellaBad.addDialog(rubellaBad5, Purvis)

def rubellaBad6 = [id:6]
rubellaBad6.playertext = "Well... what do you want to do?"
rubellaBad6.result = 7
purvisRubellaBad.addDialog(rubellaBad6, Purvis)*/

def rubellaBad7 = [id:7]
rubellaBad7.npctext = "Dur... I dunno what to do. Buh duh... wait! Pa'll know! He always knows! Go ask pa what I should do!"
rubellaBad7.quest = 65
rubellaBad7.exec = { event -> 
	event.player.unsetQuestFlag(GLOBAL, "Z3_PurvisLiesRubella_BadPoem")
	event.player.removeMiniMapQuestActorName("Purvis-VQS")
	event.player.addMiniMapQuestActorName("Rancher Bill-VQS")
}
rubellaBad7.result = DONE
purvisRubellaBad.addDialog(rubellaBad7, Purvis)

//-----------------------------------------------------------------------------------------------------------
//GRANT - Buh-Durn Garlic                                                                                      
//-----------------------------------------------------------------------------------------------------------
def buhdurnGarlicGive = Purvis.createConversation("buhdurnGarlicGive", true, "QuestCompleted_65", "!QuestStarted_280", "!Z3_Purvis_BuhDurnGarlic_Allowed", "!Z3_Purvis_BuhDurnGarlic_Denied")
buhdurnGarlicGive.setUrgent(true)

def buhdurnGarlicGive1 = [id:1]
buhdurnGarlicGive1.npctext = "Gawrsh, hey there %p. Guda see ya agin!"
buhdurnGarlicGive1.playertext = "Hey Purvis, how's it going?"
buhdurnGarlicGive1.result = 2
buhdurnGarlicGive.addDialog(buhdurnGarlicGive1, Purvis)

def buhdurnGarlicGive2 = [id:2]
buhdurnGarlicGive2.npctext = "Urg, not so good. Y'see Rubella was complainin' 'bout the smell of them Garlics, so I tole 'er I'd get rid of 'em, but I can't!"
buhdurnGarlicGive2.playertext = "Why not? Just bang them over the head with a few rings and they'll drop easy enough."
buhdurnGarlicGive2.result = 3
buhdurnGarlicGive.addDialog(buhdurnGarlicGive2, Purvis)

def buhdurnGarlicGive3 = [id:3]
buhdurnGarlicGive3.npctext = "Buh duh... I don't got no rings. Not much a fighter anyway. I jus' wish someone'd take out the buh-durn Garlic so Rubella'd be 'appy."
buhdurnGarlicGive3.options = []
buhdurnGarlicGive3.options << [text:"That's a noble cause. I'll take care of your Garlic problem, Purvis.", exec: { event ->
	if(event.player.getConLevel() < 3.1) {
		event.player.setQuestFlag(GLOBAL, "Z3_Purvis_BuhDurnGarlic_Allowed")
		Purvis.pushDialog(event.player, "buhdurnGarlicAllowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z3_Purvis_BuhDurnGarlic_Denied")
		Purvis.pushDialog(event.player, "buhdurnGarlicDenied")
	}
}, result: DONE]
buhdurnGarlicGive3.options << [text:"You'd better start looking for some rings, then.", result: 4]
buhdurnGarlicGive.addDialog(buhdurnGarlicGive3, Purvis)

def buhdurnGarlicGive4 = [id:4]
buhdurnGarlicGive4.npctext = "Buh-duh. I'll letcha go then."
buhdurnGarlicGive4.result = DONE
buhdurnGarlicGive.addDialog(buhdurnGarlicGive4, Purvis)

//-----------------------------------------------------------------------------------------------------------
//ALLOWED - Buh-Durn Garlic                                                                                  
//-----------------------------------------------------------------------------------------------------------
def buhdurnGarlicAllowed = Purvis.createConversation("buhdurnGarlicAllowed", true, "Z3_Purvis_BuhDurnGarlic_Allowed")

def buhdurnGarlicAllowed1 = [id:1]
buhdurnGarlicAllowed1.npctext = "%p! Ya jus' made me 'appier than a fly in cow-dung!"
buhdurnGarlicAllowed1.playertext = "Er, I guess flies enjoy that sort of thing?"
buhdurnGarlicAllowed1.result = 2
buhdurnGarlicAllowed.addDialog(buhdurnGarlicAllowed1, Purvis)

def buhdurnGarlicAllowed2 = [id:2]
buhdurnGarlicAllowed2.npctext = "Durn tootin'! Anyway, I figger about twenty oughta bring the smell down ta a ree... reaso... oughta stop the stink enough."
buhdurnGarlicAllowed2.quest = 280
buhdurnGarlicAllowed2.flag = "!Z3_Purvis_BuhDurnGarlic_Allowed"
buhdurnGarlicAllowed2.result = DONE
buhdurnGarlicAllowed.addDialog(buhdurnGarlicAllowed2, Purvis)

//-----------------------------------------------------------------------------------------------------------
//DENIED - Buh-Durn Garlic                                                                                   
//-----------------------------------------------------------------------------------------------------------
def buhdurnGarlicDenied = Purvis.createConversation("buhdurnGarlicDenied", true, "Z3_Purvis_BuhDurnGarlic_Denied")

def buhdurnGarlicDenied1 = [id:1]
buhdurnGarlicDenied1.npctext = "Pa always tole me, 'Purvis don't be wastin' the time of those more importan' 'an ya.' Sorry, but I thinks yer too importan' fer this task."
buhdurnGarlicDenied1.flag = "!Z3_Purvis_BuhDurnGarlic_Denied"
buhdurnGarlicDenied1.exec = { event ->
	event.player.centerPrint( "You must be level 3.0 or lower for this task." ) 
	event.player.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
}
buhdurnGarlicDenied1.result = DONE
buhdurnGarlicDenied.addDialog(buhdurnGarlicDenied1, Purvis)

//-----------------------------------------------------------------------------------------------------------
//ACTIVE - Buh-Durn Garlic                                                                                   
//-----------------------------------------------------------------------------------------------------------
def buhdurnGarlicActive = Purvis.createConversation("buhdurnGarlicActive", true, "QuestStarted_280:2")

def buhdurnGarlicActive1 = [id:1]
buhdurnGarlicActive1.npctext = "I still smell them Garlics powerful strong. Ya 'avin' trouble?"
buhdurnGarlicActive1.options = []
buhdurnGarlicActive1.options << [text:"Nope, I'll get right on it.", result: DONE]
buhdurnGarlicActive1.options << [text:"Yeah, I can't find any Garlic. Where should I look?", result: 2]
buhdurnGarlicActive.addDialog(buhdurnGarlicActive1, Purvis)

def buhdurnGarlicActive2 = [id:2]
buhdurnGarlicActive2.npctext = "Buh dur, that's easy. Ya kin findem anywheres in the woods easta the fence!"
buhdurnGarlicActive2.playertext = "Okay, I'll look there. Thanks."
buhdurnGarlicActive2.result = DONE
buhdurnGarlicActive.addDialog(buhdurnGarlicActive2, Purvis)

//-----------------------------------------------------------------------------------------------------------
//COMPLETE - Buh-Durn Garlic                                                                                 
//-----------------------------------------------------------------------------------------------------------
def buhdurnGarlicComplete = Purvis.createConversation("buhdurnGarlicComplete", true, "QuestStarted_280:3")

buhdurnGarlicComplete1 = [id:1]
buhdurnGarlicComplete1.npctext = "Buh dur, I know I said I wanted ya to knock off twenty Garlic, and I know ya did, but... well'n I can still smellem!"
buhdurnGarlicComplete1.playertext = "Of course you can! They just keep popping back up every time I whack them!"
buhdurnGarlicComplete1.result = 2
buhdurnGarlicComplete.addDialog(buhdurnGarlicComplete1, Purvis)

def buhdurnGarlicComplete2 = [id:2]
buhdurnGarlicComplete2.npctext = "Well'n, think ya could whack some more? They can't come at ya ferever!"
buhdurnGarlicComplete2.options = []
buhdurnGarlicComplete2.options << [text:"I suppose they can't. I'll give it another shot, Purvis", exec: { event ->
	event.player.updateQuest(280, "Purvis-VQS")
	event.player.removeMiniMapQuestActorName("Purvis-VQS")
	if(event.player.getConLevel() < 3.1) {
		event.player.setQuestFlag(GLOBAL, "Z3_Purvis_BuhDurnGarlic_AllowedAgain")
		Purvis.pushDialog(event.player, "buhdurnGarlicAllowedAgain")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z3_Purvis_BuhDurnGarlic_Denied")
		Purvis.pushDialog(event.player, "buhdurnGarlicDenied")
	}
}, result: DONE]
buhdurnGarlicComplete2.options << [text:"Not right now, Purvis. I need a break from the smell of Garlic.", result: 4]
buhdurnGarlicComplete.addDialog(buhdurnGarlicComplete2, Purvis)

def buhdurnGarlicComplete4 = [id:4]
buhdurnGarlicComplete4.npctext = "Durnit, maybe someone else'll 'elp."
buhdurnGarlicComplete4.quest = 280
buhdurnGarlicComplete4.exec = { event -> event.player.removeMiniMapQuestActorName("Purvis-VQS") }
buhdurnGarlicComplete.addDialog(buhdurnGarlicComplete4, Purvis)

//-----------------------------------------------------------------------------------------------------------
//ALLOWED AGAIN - Buh-Durn Garlic                                                                            
//-----------------------------------------------------------------------------------------------------------
def buhdurnGarlicAllowedAgain = Purvis.createConversation("buhdurnGarlicAllowedAgain", true, "Z3_Purvis_BuhDurnGarlic_AllowedAgain")

def buhdurnGarlicAllowedAgain1 = [id:1]
buhdurnGarlicAllowedAgain1.npctext = "Gawrsh, thanks %p!"
buhdurnGarlicAllowedAgain1.exec = { event ->
	event.player.updateQuest(280, "Purvis-VQS")
}
buhdurnGarlicAllowedAgain1.flag = "!Z3_Purvis_BuhDurnGarlic_AllowedAgain"
buhdurnGarlicAllowedAgain1.result = DONE
buhdurnGarlicAllowedAgain.addDialog(buhdurnGarlicAllowedAgain1, Purvis)