import com.gaiaonline.mmo.battle.script.*;

MinML = 7.5
ML = 7.5 //this is the bottom threshhold for this scenario. Minimum of 7.5 and Max of 10.0
MaxML = 10.1

//==========================================
// COATL GUARDIANS                          
//==========================================

// The type of guard the Coatl has is based entirely upon the size of the crew entering.
// It gets checked upon death to route to the correct type.
// (If players start the scenario with fewer players and then add after the start to try and get a weaker type of guardian.)

stoneCoatlCL = 100
maskGuard1CL = 100
maskGuard2CL = 100
maskGuard3CL = 100

//Routine for making the Coatl invulnerable
def coatlSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() > stoneCoatlCL
		if( player == attacker && !allowed ) { player.centerPrint( "The Coatl's Mystic Shield repels your attack." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

//Routine for making the first Mask Guardian invulnerable
def maskGuardian1Spec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() > maskGuard1CL
		if( player == attacker && !allowed ) { player.centerPrint( "The Coatl's Mystic Shield extends to protect this Mask while it stays near its master." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

//Routine for making the second Mask Guardian invulnerable
def maskGuardian2Spec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() > maskGuard2CL
		if( player == attacker && !allowed ) { player.centerPrint( "The Coatl's Mystic Shield extends to protect this Mask while it stays near its master." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

//Routine for making the third Mask Guardian invulnerable
def maskGuardian3Spec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() > maskGuard3CL
		if( player == attacker && !allowed ) { player.centerPrint( "The Coatl's Mystic Shield extends to protect this Mask while it stays near its master." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

//------------------------------------------
// GIANT STONE COATL SPAWNER                
//------------------------------------------
stoneCoatl = myRooms.Throne_1.spawnStoppedSpawner( "stoneCoatl", "stone_coatl", 1)
stoneCoatl.setMiniEventSpec( coatlSpec )
stoneCoatl.setRotationForChildren( 5 )
stoneCoatl.setPos( 260, 720 )
stoneCoatl.setMonsterLevelForChildren( ML )
stoneCoatl.setGuardPostForChildren( "Throne_1", 380, 680, 10 )

//------------------------------------------
// Mask Guardians                           
//------------------------------------------
maskGuard1 = myRooms.Throne_1.spawnStoppedSpawner( "maskGuard1", "mask_throne_room", 1)
maskGuard1.setMiniEventSpec( maskGuardian1Spec )
maskGuard1.setPos( 300, 805 )
maskGuard1.setRotationForChildren( 5 )
maskGuard1.setHateRadiusForChildren( 50 )
maskGuard1.setMonsterLevelForChildren( ML )
maskGuard1.setGuardPostForChildren( maskGuard1, 5 )

maskGuard2 = myRooms.Throne_1.spawnStoppedSpawner( "maskGuard2", "mask_throne_room", 1)
maskGuard2.setMiniEventSpec( maskGuardian2Spec )
maskGuard2.setPos( 410, 635 )
maskGuard2.setRotationForChildren( 5 )
maskGuard2.setHateRadiusForChildren( 50 )
maskGuard2.setMonsterLevelForChildren( ML )
maskGuard2.setGuardPostForChildren( maskGuard2, 5 )

maskGuard3 = myRooms.Throne_1.spawnStoppedSpawner( "maskGuard3", "mask_throne_room", 1)
maskGuard3.setMiniEventSpec( maskGuardian3Spec )
maskGuard3.setPos( 420, 720 )
maskGuard3.setRotationForChildren( 5 )
maskGuard3.setHateRadiusForChildren( 50 )
maskGuard3.setMonsterLevelForChildren( ML )
maskGuard3.setGuardPostForChildren( maskGuard3, 5 )

//===========================================
// STONE COATL MINION SPAWNERS               
//===========================================

//FCs
featheredCoatl = myRooms.Throne_1.spawnStoppedSpawner( "featheredCoatl", "feathered_coatl_throne_room", 10)
featheredCoatl.setPos( 380, 680 )
featheredCoatl.setMonsterLevelForChildren( ML )

//TTs
tinyTerror = myRooms.Throne_1.spawnStoppedSpawner( "tinyTerror", "tiny_terror_throne_room", 10)
tinyTerror.setPos( 380, 680 )
tinyTerror.setMonsterLevelForChildren( ML )

//Vases
bladedVase = myRooms.Throne_1.spawnStoppedSpawner( "bladedVase", "bladed_statue_throne_room", 10)
bladedVase.setPos( 380, 680 )
bladedVase.setMonsterLevelForChildren( ML )

//Vases
tinyWitchDoctor = myRooms.Throne_1.spawnStoppedSpawner( "tinyWitchDoctor", "tiny_terror_LT_throne_room", 10)
tinyWitchDoctor.setPos( 380, 680 )
tinyWitchDoctor.setMonsterLevelForChildren( ML )

//------------------------------------------
// FLAME PYRES and the COATL SHIELD         
//------------------------------------------
pyre1 = makeSwitch( "Pyre1", myRooms.Throne_1, 250, 340 )
pyre1.on()
pyre1.lock()
pyre1.setUsable( false )

pyre2 = makeSwitch( "Pyre2", myRooms.Throne_1, 250, 340 )
pyre2.on()
pyre2.lock()
pyre2.setUsable( false )

pyre3 = makeSwitch( "Pyre3", myRooms.Throne_1, 250, 340 )
pyre3.on()
pyre3.lock()
pyre3.setUsable( false )

pyre4 = makeSwitch( "Pyre4", myRooms.Throne_1, 250, 340 )
pyre4.on()
pyre4.lock()
pyre4.setUsable( false )

coatlShield = makeSwitch( "coatlShield", myRooms.Throne_1, 250, 340 )
coatlShield.on()
coatlShield.lock()
coatlShield.setUsable( false )

//------------------------------------------
// VARIABLES                                
//------------------------------------------

initialSetupCompleted = false
allPlayersDead = false
gameStarted = false
FCSpawnReplaced = false
TTSpawnReplaced = false
VaseSpawnReplaced = false
difficulty = null
mask1Released = false
mask2Released = false
mask3Released = false
bossDead = false

oldSpawn = null
numBirdDead = 0
numTTDead = 0
numVaseDead = 0
numAdjusts = 0
totalPlayersPresentAtSpawns = 0

safeZoneSet = [] as Set
nonDazedSet = [] as Set
playerSet = [] as Set
unWarpSet = [] as Set

playerRecord = [:]

onEnterBlock = new Object()

spawnPositionList = [ 1: [680, 650], 2: [940, 630], 3: [1110, 620], 4:[1160, 960], 5: [940, 980], 6: [620, 970] ]

randomSpawnList = [ featheredCoatl, featheredCoatl, tinyTerror, tinyTerror, bladedVase, bladedVase, tinyWitchDoctor ]

//------------------------------------------
// onEnter / onExit LOGIC                   
//------------------------------------------

myManager.onEnter(myRooms.Throne_1) { event ->
	synchronized( onEnterBlock ) {
		if( isPlayer(event.actor) ) {
			if( bossDead == true ) return //this prevents the messages from appearing if someone enters the instance after the coatl is already killed
			playerSet << event.actor
			event.actor.centerPrint( "The giant stone door becomes solid behind you again." )
			if( initialSetupCompleted == false ) {
				initialSetupCompleted = true
				
				//The first player into the scenario sets the difficulty for everyone else
				if( difficulty == null ) {
					difficulty = event.actor.getTeam().getAreaVar( "Z18_Otami_Throne_Room", "Z18ThroneRoomDifficulty" )
					if( difficulty == 0 ) { difficulty = 1 }
				}			
			
				//Adjust the ML before spawning anything
				adjustML()
				
				//SPAWN GIANT STONE COATL AND START HEALTH MONITORS
				boss = stoneCoatl.forceSpawnNow()
				
				guard1 = maskGuard1.forceSpawnNow()
				guard1.setDisplayName( "Ixtli" ) // Means "face" in Nahuatl
				
				guard2 = maskGuard2.forceSpawnNow()
				guard2.setDisplayName( "Necalli" ) //Means "battle" in Nahuatl
				
				guard3 = maskGuard3.forceSpawnNow()
				guard3.setDisplayName( "Eztli" ) //Means "blood" in Nahuatl
			}
			myManager.schedule(3) {
				if( gameStarted == false ) {
					event.actor.centerPrint( "The Giant Stone Coatl glares down at you." )
				} else {
					event.actor.centerPrint( "The fight is already in-progress. Help your crewmates!" )
				}
			}
		}
	}
}

//Players can't normally leave once they enter, but if they exit to nullChamber, then remove them from playerSet
myManager.onExit(myRooms.Throne_1) { event ->
	if( isPlayer(event.actor) ) {
		playerSet.remove( event.actor )
	}
}

//------------------------------------------
// ADJUST MONSTER LEVEL                     
//------------------------------------------
def adjustML() {
	numAdjusts ++
	
	//this is used for orb reward size
	playerSet.each{ if( !it.isDazed() ) { totalPlayersPresentAtSpawns ++ } }
	
	//keep track of the players that start this wave, and also increment the number of spawn waves they have encountered so far	
	//this is used for badge rewards
	playerSet.each{
		if( !it.isDazed() ) { 
			if( !playerRecord.containsKey( it ) ) {
				playerRecord.put( it, 1 )
			} else {
				curAdjusts = playerRecord.get( it ) + 1
				playerRecord.put( it, curAdjusts )
			}
		}
	}

	ML = MinML

	//kick players out if they are cheating
	playerSet.clone().each{
		if( it.getConLevel() >= MaxML ) {
			it.centerPrint( "Ah, ah, ah! Your level is too high for this scenario! Out you go!" )
			it.warp( "OtRuins_402", 465, 310 )
		}
	}

	random( playerSet ).getCrew().each() {
		if( it.getConLevel() >= ML && it.getConLevel() < MaxML ) { 
			ML = it.getConLevel()
		} else if( it.getConLevel() >= MaxML ) {
			ML = MaxML
		}
	}
	
	//DIFFICULTY
	if( difficulty == 1 ) { ML = ML - 0.6 }
	if( difficulty == 3 ) { ML = ML + 0.6 }

	
	//Now update all the spawners with the new ML so they start creating monsters at that new level
	featheredCoatl.setMonsterLevelForChildren( ML )
	tinyTerror.setMonsterLevelForChildren( ML )
	bladedVase.setMonsterLevelForChildren( ML )
	tinyWitchDoctor.setMonsterLevelForChildren( ML )
	stoneCoatl.setMonsterLevelForChildren( ML )
	maskGuard1.setMonsterLevelForChildren( ML )
	maskGuard2.setMonsterLevelForChildren( ML )
	maskGuard3.setMonsterLevelForChildren( ML )
}

//------------------------------------------
// THROW THE BUMS OUT IF THEY GET WIPED     
//------------------------------------------

def checkDazedStatus() {
	//stop checking this routine if coatl is dead and no monsters are present in the room
	stillMonstersPresent = false
	myRooms.Throne_1.getActorList().each{ if( isMonster( it ) ) { stillMonstersPresent = true } }
	if( stillMonstersPresent == false && bossDead == true ) return
	
	//now check to see if all players are dazed
	allDazed = false
	numDazed = 0
	myRooms.Throne_1.getActorList().each { if( isPlayer( it ) && it.isDazed() ) { numDazed ++ } }
	if( numDazed >= playerSet.size() ) {
		allDazed = true
		myRooms.Throne_1.getActorList().each{
			if( isPlayer( it ) ) {
				it.centerPrint( "The Coatl's minions drag you from the chamber, tossing you unceremoniously into the jungle." )
				it.warp( "OtRuins_402", 480, 320 )
			}
		}
	} else {
		myManager.schedule(2) { checkDazedStatus() }
	}
}

//------------------------------------------
// MAKE NON-DAZED SET (for monster attacks) 
//------------------------------------------
def makeNonDazedSet() {
	nonDazedSet.clear()
	myRooms.Throne_1.getActorList().each { if( isPlayer( it ) && !it.isDazed() ) { nonDazedSet << it } }
}

//------------------------------------------
// START THE SCENARIO                       
//------------------------------------------

def startGame() {
	checkDazedStatus()
	boss.say( "Pathetic mortals. With the combined lifeforces of my countless minions, we cannot be harmed." )
	myManager.schedule(3){ guard3.say( "Prepare to..." ) }
	myManager.schedule(4){ guard2.say( "...die for..." ) }
	myManager.schedule(5){ guard1.say( "... your trespass!" ) }
	myManager.schedule(7){ boss.say( "Wings from the Shadows! Destroy the intruders!" ); replenishFeatheredCoatls() }
}

def replenishFeatheredCoatls() {
	if( featheredCoatl.spawnsInUse() < playerSet.size() && numBirdDead < ( 10 * (playerSet.size()/2) ) ) {
		spawnFeatheredCoatl()
		replenishFeatheredCoatls()
	} else if( numBirdDead >= ( 10 * (playerSet.size()/2) ) ) {
		if( mask1Released == false ) {
			mask1Released = true
			//set the guard so he's vulnerable and then send him after the players
			adjustML()
			maskGuard1.setMonsterLevelForEveryone( ML )
			maskGuard1CL = 1
			guard1.setHateRadius( 2000 )
			guard1.addHate( random( playerSet ), 400 )
			guard1.say( "I will attend to these mortals, master!" )
			runOnDeath( guard1 ) {
				playerSet.clone().each{ it.centerPrint( "One of the mystic flames suddenly snuffs out with the destruction of the Mask." ) }
				sound( "flameOff" ).toZone()
				pyre4.off()
				myManager.schedule( 20 - (difficulty * 2) ) { replenishTinyTerrors() }
			}
		}
	}
}

def synchronized spawnFeatheredCoatl() {
	adjustML()
	findRandomSpawn()
	featheredCoatl.warp( "Throne_1", X, Y )
	FC = featheredCoatl.forceSpawnNow()
	makeNonDazedSet()
	FC.addHate( random( nonDazedSet ), 50 )
	runOnDeath( FC ) { numBirdDead ++; myManager.schedule( 8 - (difficulty * 2) ) { replenishFeatheredCoatls() } }
}

def replenishTinyTerrors() {
	if( tinyTerror.spawnsInUse() + tinyWitchDoctor.spawnsInUse() < playerSet.size() && numTTDead < ( 10 * (playerSet.size()/2) ) ) {
		spawnTinyTerror()
		replenishTinyTerrors()
	} else if( numTTDead >= ( 10 * (playerSet.size()/2) ) ) {
		if( mask2Released == false ) {
			mask2Released = true
			//set the guard so he's vulnerable and then send him after the players
			adjustML()
			maskGuard2.setMonsterLevelForEveryone( ML )
			maskGuard2CL = 1
			guard2.setHateRadius( 2000 )
			guard2.addHate( random( playerSet ), 400 )
			guard2.say( "They shall be destroyed in a moment, o Great Coatl!" )
			runOnDeath( guard2 ) {
				playerSet.clone().each{ it.centerPrint( "Another of the flames is extinguished as you destroy the Mask." ) }
				sound( "flameOff" ).toZone()
				pyre3.off()
				myManager.schedule( 20 - (difficulty * 2) ) { replenishBladedVases() }
			}
		}
	}
}

def synchronized spawnTinyTerror() {
	adjustML()
	findRandomSpawn()
	roll = random(100)
	if( roll < 100 - (difficulty * 15) ) { spawner = tinyTerror } else { spawner = tinyWitchDoctor }
	spawner.warp( "Throne_1", X, Y )
	TT = spawner.forceSpawnNow()
	makeNonDazedSet()
	TT.addHate( random( nonDazedSet ), 50 )
	runOnDeath( TT ) { numTTDead ++; myManager.schedule( 8 - (difficulty * 2) ) { replenishTinyTerrors() } }
}

def replenishBladedVases() {
	if( bladedVase.spawnsInUse() < playerSet.size() && numVaseDead < ( 10 * (playerSet.size()/2) ) ) {
		spawnBladedVase()
		replenishBladedVases()
	} else if( numVaseDead >= ( 10 * (playerSet.size()/2) ) ) {
		if( mask3Released == false ) {
			mask3Released = true
			//set the guard so he's vulnerable and then send him after the players
			adjustML()
			maskGuard3.setMonsterLevelForEveryone( ML )
			maskGuard3CL = 1
			guard3.setHateRadius( 2000 )
			guard3.addHate( random( playerSet ), 400 )
			guard3.say( "Fear not, Great One. Your shield weakens, but I shall not let it fail!" )
			runOnDeath( guard3 ) {
				playerSet.clone().each{ it.centerPrint( "A third flame snuffs out as the mask is destroyed." ) }
				sound( "flameOff" ).toZone()
				pyre2.off()
				myManager.schedule( 20 - (difficulty * 2) ) { releaseTheCoatl() }
			}
		}
	}
}
	
def synchronized spawnBladedVase() {
	adjustML()
	findRandomSpawn()
	bladedVase.warp( "Throne_1", X, Y )
	vase = bladedVase.forceSpawnNow()
	makeNonDazedSet()
	vase.addHate( random( nonDazedSet ), 50 )
	runOnDeath( vase ) { numVaseDead ++; myManager.schedule( 8 - (difficulty * 1.5) ) { replenishBladedVases() } }
}

def releaseTheCoatl() {
	boss.say( "ENOUGH! Even without the power of the pyres, I need no protection to deal with you...insects!" )
	myManager.schedule(3) { boss.say( "The might of the Otami Spirits flows through me and you cannot..." ) }
	myManager.schedule(4) { boss.say( "...WILL NOT..." ) }
	myManager.schedule(5) {
		boss.say( "...survive!" )
		adjustML()
		stoneCoatl.setMonsterLevelForEveryone( ML )
		boss.setMostHatedRadius( 300 )
		boss.getAI().setAttackIndex( 0 )
		myManager.onHealth( boss ) { event ->
			if( event.didTransition( 66 ) && event.isDecrease() ) {
				boss.say( "I am like unto a GOD. How dare you struggle against me!!!" )
				boss.getAI().setAttackIndex( 1 )
			} else if( event.didTransition( 33 ) && event.isDecrease() ) {
				boss.say( "No...the power cannot be ebbing. I will not allow it!" )
				boss.getAI().setAttackIndex( 2 )
			}
		}
		runOnDeath( boss ) {
			bossDead = true
			playerSet.clone().each{ it.centerPrint( "The Giant Stone Coatl falls to pieces before your eyes!" ) }; 
			myManager.schedule(3) {
				playerSet.clone().each{
					it.centerPrint( "Without the Coatl to sustain it, the last remaining flame is extinguished." ) 
				}
				myManager.schedule(1) { sound( "flameOff" ).toZone(); pyre1.off() }
			}
			myManager.schedule(6) {
				playerSet.clone().each{
					it.centerPrint( "And the Coatl's minions also fall apart." )
				}
				myRooms.Throne_1.getActorList().each { if( isMonster( it ) ) { it.instantPercentDamage(100) } }
				myManager.schedule(3) { checkForRewardsAndQuestUpdates() }
			} 
		}
		coatlShield.off()
		stoneCoatlCL = 1
		sound( "coatlAggro" ).toZone()
		warpSetup()
		myManager.schedule( 8 - ( difficulty * 2 ) ) { spawnInitialRandomHarassers() }		
	}
	
}

def spawnInitialRandomHarassers() {
	if( featheredCoatl.spawnsInUse() + tinyTerror.spawnsInUse() + tinyWitchDoctor.spawnsInUse() + bladedVase.spawnsInUse() < playerSet.size() ) {
		spawnRandomHarasser()
		myManager.schedule( 8 - ( difficulty * 2 ) ) { spawnInitialRandomHarassers() }
	}
}	

def synchronized spawnRandomHarasser() {
	if( !boss.isDead() ) {
		adjustML()
		findRandomSpawn()
		spawner = random( randomSpawnList)
		spawner.warp( "Throne_1", X, Y )
		harasser = spawner.forceSpawnNow()
		makeNonDazedSet()
		harasser.addHate( random( nonDazedSet ), 50 )
		runOnDeath( harasser ) { myManager.schedule( 8 - (difficulty * 2) ) { spawnRandomHarasser() } }
	}
}

def findRandomSpawn() {
	newSpawn = spawnPositionList.get( random( 6 ) )
	if( newSpawn != oldSpawn ) {
		oldSpawn = newSpawn
		X = newSpawn.get(0).intValue()
		Y = newSpawn.get(1).intValue()
	} else {
		findRandomSpawn()
	}
}

recipeList = [ "17842", "17840", "17843", "17841", "17838", "17837", "17839" ]
		//Greaves, Helmet, Shield, Breastplate, Spear, Maqhuitl, Club

//For those users on the quest, update the quest step
def checkForRewardsAndQuestUpdates() {
	if( !playerSet.isEmpty() ) {
		playerSet.clone().each {
			//REWARDS
			if( it.getConLevel() <= MaxML ) {
				//DIFFICULTY: set multipliers. They are set so that the min of one difficulty is higher than the max of the difficulty below it
				if( difficulty == 1 ) { goldMult = 0.75; orbMult = 0.75; recipeChance = 5 }
				else if( difficulty == 2 ) { goldMult = 3; orbMult = 2; recipeChance = 20 }
				else if( difficulty == 3 ) { goldMult = 8; orbMult = 4; recipeChance = 40 }

				//gold reward
				goldGrant = ( random( 10, 25 ) * goldMult ).intValue()
				it.centerPrint( "You receive ${goldGrant} gold!" )
				it.grantCoins( goldGrant )

				//minAdjusts is the minimum number of spawns that a player must be at in order to qualify for orb and badge rewards.
				minAdjusts = numAdjusts / 2

				//orb reward
				if( playerRecord.get( it ) < minAdjusts ) {
					it.centerPrint( "You must be present and awake for at least half the fight to earn Orbs and Recipes." )
				} else {
					orbGrant = ( random( 3, 5 ) * orbMult ).intValue()
					it.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
					it.grantQuantityItem( 100257, orbGrant )
				
					//recipe reward
					roll = random(100)
					if( roll <= recipeChance && difficulty > 1 ) { //no recipes if on Easy difficulty
						it.grantItem( random( recipeList ) )
					}
				}

				//check to see if anyone in the room needs a Throne Room quest update
				if( it.isOnQuest( 104, 2 ) ) {
					if( playerRecord.get( it ) < minAdjusts ) {
						it.centerPrint( "You must be present and awake for at least half the fight to advance your Stone Coatl task." )
					} else {															
						it.updateQuest( 104, "Otomi Lin-VQS" )
					}
				}
			} else {
				it.centerPrint( "Your level is too high for this scenario. No reward for you!" )
			}
		}
	}
}


//------------------------------------------
// STONE COATL "WARP TO ME" LOGIC           
//------------------------------------------
// - The Coatl attacks once every five seconds
// - Every 10 seconds, the Coatl pulls in 1 - ( playerSet.size() - 1 ) (but not less than 1)
// - Warp each player to each of one random spots on the stairs, but no more than one player per spot

warpLocList = [ "one", "two", "three", "four", "five" ]

def warpSetup() {
	if( !boss.isDead() && !playerSet.isEmpty() ) {
		warpLocList = [ "one", "two", "three", "four", "five" ] //reset the WarpLocList for use in comeToMe
		makeNonDazedSet()
		if( nonDazedSet.size() > 1 ) {
			maxWarp = nonDazedSet.size() - 1
		} else {
			maxWarp = 1
		}
		numToWarp = random( maxWarp )
		warpList = nonDazedSet - unWarpSet
		if( numToWarp > warpList.size() ) {
			numToWarp = warpList.size()
		}
		comeToMe()
	}
}

//MAKING THE "UNWARP" SET
def unWarpTrigger = "unWarpTrigger"
myRooms.Throne_1.createTriggerZone( unWarpTrigger, 170, 520, 700, 865 )

myManager.onTriggerIn(myRooms.Throne_1, unWarpTrigger) { event ->
	if( isPlayer( event.actor ) ) {
		unWarpSet << event.actor
	}
}

myManager.onTriggerOut( myRooms.Throne_1, unWarpTrigger ) { event ->
	if( isPlayer( event.actor ) ) {
		unWarpSet.remove( event.actor )
	}
}

//WARP THE PLAYER TO THE COATL
def comeToMe() {
	if( boss.isDead() ) return
	if( numToWarp > 0 ) {
		if( !warpList.isEmpty() ) {
			victim = random( warpList )
			if( !victim.isDazed() ) {
				position = random( warpLocList )
				victim.centerPrint( "The world turns inside-out as you are drawn inward to the Giant Stone Coatl!" )
				if( position == "one" ) {
					victim.warp( "Throne_1", 570, 670, 7)
				} else if( position == "two" ) {
					victim.warp( "Throne_1", 620, 760, 7 )
				} else if( position == "three" ) {
					victim.warp( "Throne_1", 600, 870, 7)
				} else if( position == "four" ) {
					victim.warp( "Throne_1", 535, 820, 7)
				} else if( position == "five" ) {
					victim.warp( "Throne_1", 425, 800, 7)
				}
				warpLocList.remove( position )
			}
			warpList.remove( victim )
		}
		numToWarp -- 
		comeToMe()
	} else {
		myManager.schedule( random( 45, 60 ) ) { warpSetup() }
	} 
}
	
//------------------------------------------
// START GAME TRIGGER                       
//------------------------------------------

def startGameTrigger = "startGameTrigger"
myRooms.Throne_1.createTriggerZone( startGameTrigger, 1190, 825, 1555, 1005 )

onStartGameBlock = new Object()

myManager.onTriggerOut(myRooms.Throne_1, startGameTrigger) { event ->
	synchronized( onStartGameBlock ) {
		if( isPlayer( event.actor ) && gameStarted == false ) {
			gameStarted = true
			startGame()
		}
	}
}

//=====================================
// EXIT THRONE ROOM SWITCH             
//=====================================
onClickThroneExit = new Object()

throneExit = makeSwitch( "ThroneDoor", myRooms.Throne_1, 1500, 700 )
throneExit.unlock()
throneExit.off()
throneExit.setRange( 250 )

def exitThrone = { event ->
	synchronized( onClickThroneExit ) {
		throneExit.off()
		if( isPlayer( event.actor ) ) {
			if( boss.isDead() ) {
				event.actor.setQuestFlag( GLOBAL, "Z18ThroneRoomCompleted" )
				event.actor.centerPrint( "The stone block becomes ghostlike now that the Coatl is destroyed." )
				myManager.schedule(2) {
					event.actor.centerPrint( "You leave the Throne Room, happy to have survived." )
					event.actor.warp( "OtRuins_402", 465, 310 )
				}
				myManager.schedule(4) { event.actor.centerPrint( "The stone becomes solid again behind you." ) }
			} else {
				event.actor.centerPrint( "The stone blocking the exit is solid once again. You cannot move it." )
			}
			//DEBUG!!!!
			event.actor.setCrewVar( "Z18ThroneRoomDifficultySelectionInProgress", 0 )
			//DEBUG!!!!
		}
	}
}

throneExit.whenOn( exitThrone )

