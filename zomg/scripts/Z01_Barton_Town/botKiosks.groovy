import com.gaiaonline.mmo.battle.script.*;

//Add a map marker when the script starts up and never turn it off
/*addMiniMapMarker("kioskPosition3", "markerOther", "BARTON_3", 530, 290, "HyperNet Access")
addMiniMapMarker("kioskPosition303", "markerOther", "BARTON_303", 280, 270, "HyperNet Access")
addMiniMapMarker("kioskPosition201", "markerOther", "BARTON_201", 410, 200, "HyperNet Access")

//BARTON_3
kiosk3 = makeSwitch( "basskenBot", myRooms.BARTON_3, 530, 290 )
kiosk3.unlock()
kiosk3.off()
kiosk3.setRange( 200 )
kiosk3.setMouseoverText("Access the HyperNet")

def clickKiosk3 = { event ->
	kiosk3.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z02UsingHyperNet" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		player = event.actor
		makeKioskMenu( player )
	}
}

kiosk3.whenOn( clickKiosk3 )

//BARTON_303
kiosk303 = makeSwitch( "villageBot", myRooms.BARTON_303, 280, 270 )
kiosk303.unlock()
kiosk303.off()
kiosk303.setRange( 200 )
kiosk303.setMouseoverText("Access the HyperNet")

def clickKiosk303 = { event ->
	kiosk303.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z02UsingHyperNet" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		player = event.actor
		makeKioskMenu( player )
	}
}

kiosk303.whenOn( clickKiosk303 )

//BARTON_201
kiosk201 = makeSwitch( "zenBot", myRooms.BARTON_201, 410, 200 )
kiosk201.unlock()
kiosk201.off()
kiosk201.setRange( 200 )
kiosk201.setMouseoverText("Access the HyperNet")

def clickKiosk201 = { event ->
	kiosk201.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z02UsingHyperNet" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		player = event.actor
		makeKioskMenu( player )
	}
}

kiosk201.whenOn( clickKiosk201 )


//====================================================
// TUTORIAL TREE                                      
//====================================================

kioskWidth = 300

def giveReward( event ) {
	//gold reward
	goldGrant = random( 50, 150 )
	event.player.centerPrint( "You receive ${goldGrant} gold!" )
	event.player.grantCoins( goldGrant )

	//orb reward
	orbGrant = 2
	event.player.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
	event.player.grantQuantityItem( 100257, orbGrant )
}
	
def synchronized makeKioskMenu( player ) {
	titleString = "The HyperNet"
	descripString = "Choose Game Help to access loads of information about the game, or one of the other categories for easy downloads of useful tidbits."
	diffOptions = ["Game Help", "Tutorials", "Area Info", "Exit HyperNet"]
	
	uiButtonMenu( player, "kioskMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Game Help" ) {
			if( !event.actor.hasQuestFlag( GLOBAL, "ZXXGameHelpSelected" ) ) {
				event.actor.setQuestFlag( GLOBAL, "ZXXGameHelpSelected" )
				//gold reward
				goldGrant = random( 50, 150 )
				event.actor.centerPrint( "You receive ${goldGrant} gold!" )
				event.actor.grantCoins( goldGrant )

				//orb reward
				orbGrant = 2
				event.actor.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
				event.actor.grantQuantityItem( 100257, orbGrant )
			}
			showWidget( event.actor, "GameHelp", true, true )
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
		if( event.selection == "Tutorials" ) {
			makeTutorialMenu( player )
		}
		if( event.selection == "Area Info" ) {
			makeAreaMenu( player )
		}
		if( event.selection == "Exit HyperNet" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
	}
}

def makeAreaMenu( player ) {
	titleString = "Area Info"
	descripString = "A small archive of stories and tidbits of the area around you."
	diffOptions = ["The HyperNet", "Barton Town", "The Barton Regulars", "Main Menu"]
	
	uiButtonMenu( player, "fictionMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "The HyperNet" ) {
			tutorialNPC.pushDialog( event.actor, "hyperNet" )
		}
		if( event.selection == "Barton Town" ) {
			tutorialNPC.pushDialog( event.actor, "bartonTown" )
		}
		if( event.selection == "The Barton Regulars" ) {
			tutorialNPC.pushDialog( event.actor, "bartonRegulars" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}
	


def makeTutorialMenu( player ) {
	titleString = "Tutorials"
	descripString = "These tutorials give you quick overviews on various features in the game."
	diffOptions = ["Doing Tasks", "Chatting", "PowerUps", "The Fountain Game", "Main Menu"]
	
	uiButtonMenu( player, "tutorialMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Doing Tasks" ) {
			tutorialNPC.pushDialog( event.actor, "doingTasks" )
		}
		if( event.selection == "Chatting" ) {
			tutorialNPC.pushDialog( event.actor, "chatPane" )
		}
		if( event.selection == "PowerUps" ) {
			tutorialNPC.pushDialog( event.actor, "powerups" )
		}
		if( event.selection == "The Fountain Game" ) {
			tutorialNPC.pushDialog( event.actor, "fountainGame" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}

//======================================================================================
//======================================================================================
// TUTORIAL MENU                                                                        
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//DOING TASKS TUTORIAL                                                                  
//--------------------------------------------------------------------------------------
doingTasks = tutorialNPC.createConversation( "doingTasks", true )

def task1 = [id:1]
task1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Doing Tasks</font></b></h1><br><font face='Arial' size ='12'>Various characters in the world would like you to do tasks for them.<br><br>Characters that have tasks for you have a '!!!' word balloon over their head.<br><br>You always get rewards for doing Tasks, including gold, charge orbs, recipes, and sometimes even rings!<br><br>]]></zOMG>"
task1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BARTON_201 || event.actor.getRoom() == myRooms.BARTON_303 || event.actor.getRoom() == myRooms.BARTON_3 ) {
	player = event.player
		makeTutorialMenu( player )
	if( !event.player.hasQuestFlag( GLOBAL, "ZXXDoingTasks" ) ) {
		event.player.setQuestFlag( GLOBAL, "ZXXDoingTasks" )
		giveReward( event )
	}
}
}
task1.result = DONE
doingTasks.addDialog( task1, tutorialNPC )

//--------------------------------------------------------------------------------------
//CHAT PANE TUTORIAL                                                                    
//--------------------------------------------------------------------------------------
chatPane = tutorialNPC.createConversation( "chatPane", true )

def chat1 = [id:1]
chat1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Chatting</font></b></h1><br><font face='Arial' size ='12'>To chat with other players, just click in the text entry area of the Chat Pane, and hit ENTER when you're done.<br><br>To control which groups of players see what you type, click on the button to the left of the text entry area and select which channel you desire.<br><br>By default, you see all the chat channels, but you can reduce how much you see by clicking the 'View' button and then unclicking the chat you don't want to see.<br><br>The various chat channels are:<br><br><b>Talk</b> : This is seen only by the players in the immediate area around you.<br><b>Shout</b> : This is heard by everyone in the same zone as you (for example, Barton Town or Village Greens).<br><b>Crew</b> : Only players in your Crew see your chat messages.<br><b>Clan</b> : Only players in your Clan see your chat messages.<br><b>Whisper</b> : Only the player you specify can see your message.<br><br>]]></zOMG>"
chat1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BARTON_201 || event.actor.getRoom() == myRooms.BARTON_303 || event.actor.getRoom() == myRooms.BARTON_3 ) {
	player = event.player
		makeTutorialMenu( player )
	if( !event.player.hasQuestFlag( GLOBAL, "ZXXChatPane" ) ) {
		event.player.setQuestFlag( GLOBAL, "ZXXChatPane" )
		giveReward( event )
	}
}
}
chat1.result = DONE
chatPane.addDialog( chat1, tutorialNPC )

//--------------------------------------------------------------------------------------
//POWERUPS TUTORIAL                                                                     
//--------------------------------------------------------------------------------------
powerups = tutorialNPC.createConversation( "powerups", true )

def power1 = [id:1]
power1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Using PowerUps</font></b></h1><br><font face='Arial' size ='12'>There are a number of PowerUps for sale that can help you out when you get into trouble.<br><br>These PowerUps are found by clicking the SHOP button in the upper, right of your screen, or by visiting the Back Alley Bargains shop on the main site.<br><br>PowerUps available to you include things like being able to wake yourself up after being Dazed, using Superchargers to restore health and stamina, using Ghi Amps to increase the damage you do, and more!<br><br>Click on the SHOP button to see what's available!<br><br>]]></zOMG>"
power1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BARTON_201 || event.actor.getRoom() == myRooms.BARTON_303 || event.actor.getRoom() == myRooms.BARTON_3 ) {
	player = event.player
		makeTutorialMenu( player )
	if( !event.player.hasQuestFlag( GLOBAL, "ZXXPowerUps" ) ) {
		event.player.setQuestFlag( GLOBAL, "ZXXPowerUps" )
		giveReward( event )
	}
}
}
power1.result = DONE
powerups.addDialog( power1, tutorialNPC )

//--------------------------------------------------------------------------------------
//THE FOUNTAIN GAME                                                                     
//--------------------------------------------------------------------------------------
fountainGame = tutorialNPC.createConversation( "fountainGame", true )

def fountain1 = [id:1]
fountain1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>The Fountain Game</font></b></h1><br><font face='Arial' size ='12'>In the center of Barton Town, there is a water fountain. If you click on this fountain, you can sign up to play the Fountain Game!<br><br>This game requires a Crew of four people.<br><br>This is a sort of 'Simon Says' game, where each of your Crew is responsible for a different colored button.<br><br>The lights blink in a pattern, and when the pattern has been shown to you, then your Crew repeats the pattern.<br><br>Prizes are based on how far you get into the game and include things like gold, charge orbs, and more!<br><br>]]></zOMG>"
fountain1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BARTON_201 || event.actor.getRoom() == myRooms.BARTON_303 || event.actor.getRoom() == myRooms.BARTON_3 ) {
	player = event.player
		makeTutorialMenu( player )
	if( !event.player.hasQuestFlag( GLOBAL, "ZXXFountainGame" ) ) {
		event.player.setQuestFlag( GLOBAL, "ZXXFountainGame" )
		giveReward( event )
	}
}
}
fountain1.result = DONE
fountainGame.addDialog( fountain1, tutorialNPC )



//======================================================================================
//======================================================================================
// AREA MENU                                                                            
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//HYPERNET BACKGROUND                                                                   
//--------------------------------------------------------------------------------------
def hyperNet = tutorialNPC.createConversation( "hyperNet", true )

def hyper1 = [id:1]
hyper1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Origins of the HyperNet</font></b></h1><br><font face='Arial' size ='12'>The town of Aekea is well-known for its AI (artificial intelligence) personalities.<br><br>Those AIs move around in robotic bodies, but they aren't constrained to those mere metal forms.<br><br>The vast intellects of the AIs dwell within a cyberspace area that they call the 'HyperNet'.<br><br>The AIs decided long ago to let Gaians access that thoughtsphere and tap it for data when needed.<br><br>Try not to think too hard about the fact that you're rummaging around in someone's mind for this information.<br><br>]]></zOMG>"
hyper1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BARTON_201 || event.actor.getRoom() == myRooms.BARTON_303 || event.actor.getRoom() == myRooms.BARTON_3 ) {
	player = event.player
		makeAreaMenu( player )
	if( !event.player.hasQuestFlag( GLOBAL, "ZXXHyperNet" ) ) {
		event.player.setQuestFlag( GLOBAL, "ZXXHyperNet" )
		giveReward( event )
	}
}
}
hyper1.result = DONE
hyperNet.addDialog( hyper1, tutorialNPC )

//--------------------------------------------------------------------------------------
//BARTON TOWN BACKGROUND                                                                
//--------------------------------------------------------------------------------------
def bartonTown = tutorialNPC.createConversation( "bartonTown", true )

def town1 = [id:1]
town1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Barton Town</font></b></h1><br><font face='Arial' size ='12'>Barton Town is the oldest of the towns in this region, and has something of a quirky attitude about itself.<br><br>The citizens of Barton Town like the quiet, backwater kind of lifestyle, and their architecture, the guards' antiquated armor, and the laws against vehicles keep the town pedestrian and serene.<br><br>That is, until the Animated arrived anyway.<br><br>]]></zOMG>"
town1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BARTON_201 || event.actor.getRoom() == myRooms.BARTON_303 || event.actor.getRoom() == myRooms.BARTON_3 ) {
	player = event.player
		makeAreaMenu( player )
	if( !event.player.hasQuestFlag( GLOBAL, "ZXXBartonTown" ) ) {
		event.player.setQuestFlag( GLOBAL, "ZXXBartonTown" )
		giveReward( event )
	}
}
}
town1.result = DONE
bartonTown.addDialog( town1, tutorialNPC )

//--------------------------------------------------------------------------------------
//BARTON REGULARS BACKGROUND                                                            
//--------------------------------------------------------------------------------------
def bartonRegulars = tutorialNPC.createConversation( "bartonRegulars", true )

def regular1 = [id:1]
regular1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>The Barton Regulars</font></b></h1><br><font face='Arial' size ='12'>The Barton Regulars are the law enforcement squad for Barton Town. Their commander, Leon, is a huge fan of swords & sorcery novels. He figures that his forces will be better protected while wearing plate armor...and it makes his guards less intimidating to other citizens.<br><br>Plus, it never ceases to make him grin when they get together for photos.<br><br>]]></zOMG>"
regular1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BARTON_201 || event.actor.getRoom() == myRooms.BARTON_303 || event.actor.getRoom() == myRooms.BARTON_3 ) {
	player = event.player
		makeAreaMenu( player )
	if( !event.player.hasQuestFlag( GLOBAL, "ZXXBartonRegulars" ) ) {
		event.player.setQuestFlag( GLOBAL, "ZXXBartonRegulars" )
		giveReward( event )
	}
}
}
regular1.result = DONE
bartonRegulars.addDialog( regular1, tutorialNPC )*/

