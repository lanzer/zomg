import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}
//------------------------------------------
//BARTON JEWELERS (Agatha)                  
//------------------------------------------
def Agatha = spawnNPC("Agatha-VQS", myRooms.BARTON_101, 1345, 740)
Agatha.setRotation( 45 )
Agatha.setDisplayName( "Agatha" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Agatha.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/agatha_strip.png")
}
//------------------------------------------
//Agatha's Default Conversation             
// (Doesn't occur if on Ellie's Friends or  
// Kin's Travels quests)                     
//------------------------------------------

def AgathaDefault = Agatha.createConversation("AgathaDefault", true, "!QuestStarted_31:2", "!QuestStarted_61:3") //31 = Ellie's Friends; 61 = Kin's Travel Quest
AgathaDefault.setUrgent( false )

def Agatha8 = [id:8]
Agatha8.npctext = "Welcome to Barton Jewelers, %p! What may I help you with?"
Agatha8.options = []
Agatha8.options << [text:"What sorts of things does your shop sell?", result :9]
Agatha8.options << [text:"Have you heard anything about what's going on outside the Town?", result: 10]
Agatha8.options << [text:"No thanks. Just saying hello.", result: 16]
AgathaDefault.addDialog(Agatha8, Agatha)

def Agatha9 = [id:9]
Agatha9.npctext = "I sell jewelry, both affordable and expensive, depending on your own personal budget. All of it is beautiful, but a wide variety of tastes are accommodated."
Agatha9.playertext = "Sounds nice. Thanks."
Agatha9.result = DONE
AgathaDefault.addDialog(Agatha9, Agatha)

def Agatha10 = [id:10]
Agatha10.npctext = "Only a tiny bit, luv, and that only second-hand from a few of the guards here in Town."
Agatha10.playertext = "What have you heard?"
Agatha10.result = 11
AgathaDefault.addDialog(Agatha10, Agatha)

//Agatha 11 is the conversation hub for a lot of what follows
def Agatha11 = [id:11]
Agatha11.npctext = "We've held the walls secure against the Animated, and Leon has led a contingent of guards down south to protect the villages. But, Durem and Aekea are cut off by Animated incursions even more ferocious than those we face here at Barton Town."
Agatha11.options = []
Agatha11.options << [text:"What do you mean by 'secure against the Animated'?", result: 12]
Agatha11.options << [text:"The villages?", result: 17]
Agatha11.options << [text:"Durem and Aekea are cut off? What's going on?", result: 14]
AgathaDefault.addDialog(Agatha11, Agatha)

def Agatha12 = [id:12]
Agatha12.npctext = "We don't know why or how, but items that once lay inanimate upon the ground are now picking themselves up and moving of their own volition. We've also found that many of the Animated seem to attack us on sight and that the wave of Animations is happening almost everywhere."
Agatha12.playertext = "Everywhere?"
Agatha12.result = 13
AgathaDefault.addDialog(Agatha12, Agatha)

def Agatha13 = [id:13]
Agatha13.npctext = "As far as we can tell, yes. Leon has sent patrols out to examine things further. If you want to find out more, I recommend that you speak with Leon directly. He's on the road south from Barton, toward the Village enclave. Would you like to speak with him?"
Agatha13.options = []
Agatha13.options << [text:"That seems like a good idea! I'll head down to see Leon right away!", result: DONE, flag: "Z1FindLeonQuest"]
Agatha13.options << [text:"First, I'd like to talk to you a bit more. Can you remind me about what you've heard?", result: 11]
Agatha13.options << [text:"Not at the moment. I still have business in Town and I need to get back to it. Thanks!", result: DONE]
AgathaDefault.addDialog(Agatha13, Agatha)

def Agatha14 = [id:14]
Agatha14.npctext = "I'm not the one with the details. I can't even get through to Edmund on the phone. The lines are all jammed! It seems like everyone, everywhere is busy dealing with all the craziness that's happening. Be careful out there, if you go outside the walls. The world has changed!"
Agatha14.playertext = "There must be some explanation for it all. We just need to find that explanation and make things better again!"
Agatha14.result = 15
AgathaDefault.addDialog(Agatha14, Agatha)

def Agatha15 = [id:15]
Agatha15.npctext = "Ah, the exuberance and naivete of youth. I wish you luck, hun, but that's about all I know about that. Is there anything else I can help you with?"
Agatha15.options = []
Agatha15.options << [text:"Can you tell me again about the things that you've heard?", result: 11]
Agatha15.options << [text:"No thanks! Good luck to you, Agatha!", result: DONE]
AgathaDefault.addDialog(Agatha15, Agatha)

def Agatha16 = [id:16]
Agatha16.npctext = "All right then. Be careful, but enjoy your day!"
Agatha16.result = DONE
AgathaDefault.addDialog(Agatha16, Agatha)

def Agatha17 = [id:17]
Agatha17.npctext = "The sprawling area to the south where the Barton suburbs lie. It is surrounded by a long, low wall which Leon and his guards struggle to hold secure against the Animated."
Agatha17.options = []
Agatha17.options << [text:"Are the villages still secure?", result: 18]
Agatha17.options << [text:"The villages lie outside the South Gate then?", result: 19]
Agatha17.options << [text:"What other things can you tell me about?", result: 11]
AgathaDefault.addDialog(Agatha17, Agatha)

def Agatha18 = [id:18]
Agatha18.npctext = "So far. The struggles of the guards have sometimes been a near thing, but the villages remain secure."
Agatha18.playertext = "That's good."
Agatha18.options = []
Agatha18.options << [text: "Thanks for the 411, Agatha. See you later!", result: DONE]
Agatha18.options << [text: "Can you tell me more about what you know?", result: 11]
AgathaDefault.addDialog(Agatha18, Agatha)

def Agatha19 = [id:19]
Agatha19.npctext = "Yes, exactly. The Zen Gardens lie to the west, Bass'ken Lake is out the North Gate, and the villages are through the South Gate."
Agatha19.playertext = "Thanks!"
Agatha19.options = []
Agatha19.options << [text: "I appreciate the roadmap. Adios for now!", result: DONE]
Agatha19.options << [text: "But can you tell me anything else about what's going on?", result: 11]
AgathaDefault.addDialog(Agatha19, Agatha)

//----------------------------------------------
// ELLIE'S FRIENDS QUEST (Agatha's leg)         
//----------------------------------------------

def EllieQuestPartThree = Agatha.createConversation("EllieQuestPartThree", true, "QuestStarted_31:2", "!Z1AgathaGoodbye" )
EllieQuestPartThree.setUrgent( true )

def Agatha1 = [id:1]
Agatha1.npctext = "Hello there, hun. What may I do for you?"
Agatha1.playertext = "Ellie asked me to come by and see you."
Agatha1.result = 2
EllieQuestPartThree.addDialog(Agatha1, Agatha)

def Agatha2 = [id:2]
Agatha2.npctext = "Hmmm, we haven't met before, so let me do a bit of guessing, you stopped to see Miss Ellie while she was painting and she asked you for a favor out of the blue."
Agatha2.playertext = "Well, yes. I suppose it was something like that."
Agatha2.result = 3
EllieQuestPartThree.addDialog(Agatha2, Agatha)

def Agatha3 = [id:3]
Agatha3.npctext = "And I suppose she wants you to tell me she can't come along right now because her paint is wet or the light is just right or she just HAS to draw out a sudden inspiration. Yes?"
Agatha3.playertext = "Yes! That's it exactly! About the lighting part anyway. How did you know?"
Agatha3.result = 4
EllieQuestPartThree.addDialog(Agatha3, Agatha)

def Agatha4 = [id:4]
Agatha4.npctext = "To Ellie, painting is more important than breathing. She has missed more appointments because of reasons like that than we can count. But we all love her. She's incredibly talented."
Agatha4.playertext = "Well, she's missing her date with you today, so she's lucky you think so highly of her."
Agatha4.result = 5
EllieQuestPartThree.addDialog(Agatha4, Agatha)

def Agatha5 = [id:5]
Agatha5.npctext = "Hmmm. Yes, she is. Thank you for coming by and letting me know, dear."
Agatha5.result = DONE
Agatha5.exec = { event ->
	if(!event.player.hasQuestFlag(GLOBAL, "Z1MaestroGoodbye") ) {
		event.player.setQuestFlag( GLOBAL, "Z1AgathaGoodbye" )
		Agatha.pushDialog(event.player, "AgathaGoodbye")
	} else {
		event.player.updateQuest( 31, "Agatha-VQS" ) //complete Agatha's leg of the quest
		event.player.removeMiniMapQuestActorName( "Agatha-VQS" )
		event.player.addMiniMapQuestActorName( "Ellie-VQS" )
	}
}
EllieQuestPartThree.addDialog(Agatha5, Agatha)

//---------------------------------------------
// Agatha's First Goodbye                      
// (if the player hasn't spoken to Maestro yet)
//---------------------------------------------
def AgathaGoodbye = Agatha.createConversation("AgathaGoodbye", true, "Z1AgathaGoodbye", "!Z1AgathaGoodbye2" )
AgathaGoodbye.setUrgent( false )

def Agatha6 = [id:6]
Agatha6.npctext = "Would you mind stopping by the Music Box to let Maestro know about Ellie's delay?"
Agatha6.playertext = "No problem. I was headed over there already to do that very thing."
Agatha6.result = 7
AgathaGoodbye.addDialog(Agatha6, Agatha)

def Agatha7 = [id:7]
Agatha7.npctext = "Very nice. Thank you, luv. Enjoy your day!"
Agatha7.quest = 31 //complete Agatha's leg of the quest
Agatha7.flag = "Z1AgathaGoodbye2"
Agatha7.exec = { event ->
	event.player.updateQuest( 31, "Agatha-VQS" )
	event.player.removeMiniMapQuestActorName( "Agatha-VQS" )
}	
Agatha7.result = DONE
AgathaGoodbye.addDialog(Agatha7, Agatha)

//------------------------------------------
// TRAVEL QUEST - Agatha's Leg              
// The player comes here after speaking to  
// Logan at the Ole Fishing Hole            
//------------------------------------------

def TravelQuestAgatha = Agatha.createConversation("TravelQuestAgatha", true, "QuestStarted_61:3", "!QuestStarted_61:4")
TravelQuestAgatha.setUrgent( true )

def AgathaTrav1 = [id:1]
AgathaTrav1.npctext = "Hello there, hon. How can I help you?"
AgathaTrav1.options = []
AgathaTrav1.options << [text:"Old Man Logan sent me to you, Agatha.", result: 2]
AgathaTrav1.options << [text:"Nothing right now. Thanks though!", result: DONE]
TravelQuestAgatha.addDialog(AgathaTrav1, Agatha)

def AgathaTrav2 = [id:2]
AgathaTrav2.npctext = "Logan? Well bless his heart. What does that old scrapper want with me?"
AgathaTrav2.playertext = "Mostly, he wants to share information. We've been finding quite a lot out about the Animated and their rings and we wanted to pass it on."
AgathaTrav2.result = 3
TravelQuestAgatha.addDialog(AgathaTrav2, Agatha)

def AgathaTrav3 = [id:3]
AgathaTrav3.npctext = "Oh, *I* see. Logan doesn't want to tell ME that information. He just can't get to Edmund and wants me to run and fetch for him, doesn't he?"
AgathaTrav3.playertext = "Ummm...errr, is there maybe a little history between you two?"
AgathaTrav3.result = 4
TravelQuestAgatha.addDialog(AgathaTrav3, Agatha)

def AgathaTrav4 = [id:4]
AgathaTrav4.npctext = "History. Yes, I suppose that's how you might describe Logan, Edmund, Johnny and I, but I prefer to think of us as still vibrant and alive!"
AgathaTrav4.playertext = "No! That's not what I meant..."
AgathaTrav4.result = 5
TravelQuestAgatha.addDialog(AgathaTrav4, Agatha)

def AgathaTrav5 = [id:5]
AgathaTrav5.npctext = "I know, dearie. It's just been a long day and I'm having a bit of fun with you. What is it that Logan wants to tell me?"
AgathaTrav5.playertext = "He thinks that the Animated might have some root cause that has something to do with Johnny Gambino."
AgathaTrav5.result = 6
TravelQuestAgatha.addDialog(AgathaTrav5, Agatha)

def AgathaTrav6 = [id:6]
AgathaTrav6.npctext = "What? Why?!?"
AgathaTrav6.playertext = "Logan says that the way the Animated seem to pop out of nowhere, immediately starting to attack things around them, it reminds him a lot of when the zombies overran the Gambino mansion."
AgathaTrav6.result = 7
TravelQuestAgatha.addDialog(AgathaTrav6, Agatha)

def AgathaTrav7 = [id:7]
AgathaTrav7.npctext = "That's...very interesting. But, as I recall, the zombie outbreak had very specific origins in the G-Virus experiments that were occurring back then. The outbreak spread through direct contact between a zombie and another dead thing. I don't see how that applies here since none of the Animated were ever really 'dead' since they'd never been 'alive' previously. Besides, we're not seeing Animated touch anything to bring it to life...they just seem to occur spontaneously!"
AgathaTrav7.playertext = "I guess Logan was just guessing about it all."
AgathaTrav7.result = 8
TravelQuestAgatha.addDialog(AgathaTrav7, Agatha)

def AgathaTrav8 = [id:8]
AgathaTrav8.npctext = "hehehe. More than likely. Still, he might be right. Just because I can't see the connection, doesn't mean that there isn't one. Edmund's mind for analysis would be better suited to this issue. I'll contact him and let him know. But to me, this has about as much validity as the 'return of the Zurg' theories I keep hearing."
AgathaTrav8.playertext = "I guess we need more information before we can really decide."
AgathaTrav8.result = 9
TravelQuestAgatha.addDialog(AgathaTrav8, Agatha)

def AgathaTrav9 = [id:9]
AgathaTrav9.npctext = "Well, then what we need to do is contact an organization that CAN gather that info for us. How would you feel about heading across the Gardens to try contacting the Chyaku Norisu ninja clan?"
AgathaTrav9.playertext = "You must be kidding! I started this whole journey by talking to those ninjas."
AgathaTrav9.result = 10
TravelQuestAgatha.addDialog(AgathaTrav9, Agatha)

def AgathaTrav10 = [id:10]
AgathaTrav10.npctext = "Really?!? *You* spoke to a ninja? Who?"
AgathaTrav10.playertext = "His name is Kin."
AgathaTrav10.result = 11
TravelQuestAgatha.addDialog(AgathaTrav10, Agatha)

def AgathaTrav11 = [id:11]
AgathaTrav11.npctext = "Their clan's second-in-command? That's amazing! Well, go back to him and tell him about Logan's ring theory. The ninja are masters of piecing together bits of information into pictures that others can't see."
AgathaTrav11.playertext = "Is there anything else I should tell him?"
AgathaTrav11.result = 12
TravelQuestAgatha.addDialog(AgathaTrav11, Agatha)

def AgathaTrav12 = [id:12]
AgathaTrav12.npctext = "Yes. We're getting reports of things roaming the areas north of the Gardens that are definitely *not* Animated. But they're also not natural."
AgathaTrav12.playertext = "What sorts of things?"
AgathaTrav12.result = 13
TravelQuestAgatha.addDialog(AgathaTrav12, Agatha)

def AgathaTrav13 = [id:13]
AgathaTrav13.npctext = "The reports are very hard to believe, but so is everything else that's happening now. I've heard rumors of a return of the Zurg, sea creatures roaming around on land, and some even crazier things. It's hard to tell what's truth and what's being exaggerated due to panic."
AgathaTrav13.playertext = "The Animated show up, and then at the same time, other enemies start appearing at the same time? This is sounding less and less like a coincidence..."
AgathaTrav13.result = 14
TravelQuestAgatha.addDialog(AgathaTrav13, Agatha)

def AgathaTrav14 = [id:14]
AgathaTrav14.npctext = "Perhaps. It could also be something more fundamental, like an alteration in the physics of the universe for some reason, or even something as simple as that coincidence you mentioned."
AgathaTrav14.playertext = "Hmmm..."
AgathaTrav14.result = 15
TravelQuestAgatha.addDialog(AgathaTrav14, Agatha)

def AgathaTrav15 = [id:15]
AgathaTrav15.npctext = "Yes, it does make you think, doesn't it? Well, do be a dear and go tell Kin what we've spoken about. I'll try to somehow get word of all this to Edmund and perhaps we will make sense of it soon."
AgathaTrav15.playertext = "All right. I'll head back to Kin then and see what he says about getting more info for you."
AgathaTrav15.result = 16
TravelQuestAgatha.addDialog(AgathaTrav15, Agatha)

def AgathaTrav16 = [id:16]
AgathaTrav16.npctext = "Thanks, hun. Good luck to you!"
AgathaTrav16.result = DONE
AgathaTrav16.quest = 61 //push the completion flag for Agatha's leg of "Kin's Trials (part 3)" quest
AgathaTrav16.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Agatha-VQS" )
	event.player.addMiniMapQuestActorName( "Kin-VQS" )
}
TravelQuestAgatha.addDialog(AgathaTrav16, Agatha)


