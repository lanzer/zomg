import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

Terry = spawnNPC("Terry-VQS", myRooms.BARTON_204, 800, 400)
Perry = spawnNPC("Perry-VQS", myRooms.BARTON_204, 990, 520)

Terry.setRotation( 135 )
Perry.setRotation( 135 )

Terry.setDisplayName( "Terry" )
Perry.setDisplayName( "Perry" )

//--------------------------------------------------------------------------------------------------------------
//Default conversation for Terry and Perry                                                                      
//--------------------------------------------------------------------------------------------------------------
def TerryPerry = Terry.createConversation("TerryPerry", true, "!Z01DefendPathChosen" )

def TerryPerry1 = [id:1]
TerryPerry1.npctext = "Hello %p, Nice to..."
TerryPerry1.result = 2
TerryPerry.addDialog(TerryPerry1, Terry)

def TerryPerry2 = [id:2]
TerryPerry2.npctext = "...meet you. We're..."
TerryPerry2.result = 3
TerryPerry.addDialog(TerryPerry2, Perry)

def TerryPerry3 = [id:3]
TerryPerry3.npctext = "...Terry and Perry."
TerryPerry3.options = []
TerryPerry3.options << [text:"Wait, which of you is Terry and which is Perry?", result:4]
TerryPerry3.options << [text:"Do you two always talk that way?", result:5]
TerryPerry3.options << [text:"OMG! Do that again!", result:6]
TerryPerry.addDialog(TerryPerry3, Terry)

def TerryPerry4 = [id:4]
TerryPerry4.npctext = "Allow us..."
TerryPerry4.result = 7
TerryPerry.addDialog(TerryPerry4, Terry)

def TerryPerry5 = [id:5]
TerryPerry5.npctext = "Of course..."
TerryPerry5.result = 12
TerryPerry.addDialog(TerryPerry5, Terry)

def TerryPerry6 = [id:6]
TerryPerry6.npctext = "Do what..."
TerryPerry6.result = 16
TerryPerry.addDialog(TerryPerry6, Terry)

//--------------------------------------------------------------------------------------------------------------
//Dialog Option 1                                                                                               
//--------------------------------------------------------------------------------------------------------------
def TerryPerry7 = [id:7]
TerryPerry7.npctext = "...to introduce..."
TerryPerry7.result = 8
TerryPerry.addDialog(TerryPerry7, Perry)

def TerryPerry8 = [id:8]
TerryPerry8.npctext = "...ourselves. Meet my brother..."
TerryPerry8.result = 9
TerryPerry.addDialog(TerryPerry8, Terry)

def TerryPerry9 = [id:9]
TerryPerry9.npctext = "...Perry. And my brother..."
TerryPerry9.result = 10
TerryPerry.addDialog(TerryPerry9, Perry)

def TerryPerry10 = [id:10]
TerryPerry10.npctext = "...Terry."
TerryPerry10.result = 11
TerryPerry.addDialog(TerryPerry10, Terry)

def TerryPerry11 = [id:11]
TerryPerry11.npctext = "THE CONDUCTOR TWINS!!!"
TerryPerry11.result = 20
TerryPerry.addDialog(TerryPerry11, Perry)

//----------------------------------------------------------------------------------------------------------------
//Dialog Option 2                                                                                                 
//----------------------------------------------------------------------------------------------------------------
def TerryPerry12 = [id:12]
TerryPerry12.npctext = "...how else should..."
TerryPerry12.result = 13
TerryPerry.addDialog(TerryPerry12, Perry)

def TerryPerry13 = [id:13]
TerryPerry13.npctext = "...we talk?"
TerryPerry13.result = 14
TerryPerry.addDialog(TerryPerry13, Terry)

def TerryPerry14 = [id:14]
TerryPerry14.npctext = "At the same time?!"
TerryPerry14.result = 21
TerryPerry.addDialog(TerryPerry14, Perry)

//15 was removed

//-----------------------------------------------------------------------------------------------------------------
//Dialog Option 3                                                                                                  
//-----------------------------------------------------------------------------------------------------------------
def TerryPerry16 = [id:16]
TerryPerry16.npctext = "Oh! You mean..."
TerryPerry16.result = 17
TerryPerry.addDialog(TerryPerry16, Perry)

def TerryPerry17 = [id:17]
TerryPerry17.npctext = "...talk..."
TerryPerry17.result = 18
TerryPerry.addDialog(TerryPerry17, Terry)

def TerryPerry18 = [id:18]
TerryPerry18.npctext = "...like..."
TerryPerry18.result = 19
TerryPerry.addDialog(TerryPerry18, Perry)

def TerryPerry19 = [id:19]
TerryPerry19.npctext = "...THIS?!"
TerryPerry19.result = 22
TerryPerry.addDialog(TerryPerry19, Terry)

//----------------------------------------------------------------------------------------------------------------
//Looper dialog (Option 1)                                                                                        
//----------------------------------------------------------------------------------------------------------------
def TerryPerry20 = [id:20]
TerryPerry20.playertext = "Hi Terry. Hi Perry."
TerryPerry20.options = []
TerryPerry20.options << [text:"Do you two always talk that way?", result:5]
TerryPerry20.options << [text:"OMG! Do that again!", result:6]
TerryPerry20.options << [text:"Okay. I'm leaving before you two give me a headache.", result: DONE]
TerryPerry.addDialog(TerryPerry20, Terry)

//----------------------------------------------------------------------------------------------------------------
//Looper dialog (Option 2)                                                                                        
//----------------------------------------------------------------------------------------------------------------
def TerryPerry21 = [id:21]
TerryPerry21.playertext = "Hi Terry. Hi Perry."
TerryPerry21.options = []
TerryPerry21.options << [text:"Wait, which of you is Terry and which is Perry?", result:4]
TerryPerry21.options << [text:"OMG! Do that again!", result:6]
TerryPerry21.options << [text:"Okay. I'm leaving before you two give me a headache.", result: DONE]
TerryPerry.addDialog(TerryPerry21, Terry)

//---------------------------------------------------------------------------------------------------------------
//Looper dialog (Option 3)                                                                                       
//---------------------------------------------------------------------------------------------------------------
def TerryPerry22 = [id:22]
TerryPerry22.playertext = "Hi Terry. Hi Perry."
TerryPerry22.options = []
TerryPerry22.options << [text:"Wait, which of you is Terry and which is Perry?", result:4]
TerryPerry22.options << [text:"Do you two always talk that way?", result:5]
TerryPerry22.options << [text:"Okay. I'm leaving before you two give me a headache.", result: DONE]
TerryPerry.addDialog(TerryPerry22, Terry)


//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Terry.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/terry_strip.png")
	Perry.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/perry_strip.png")
}

if (eventStart != 'HALLOWEEN') return

//======================================
// HALLOWEEN TRICK OR TREATING SCRIPT   
//======================================

playerList = []
fluffList = []

myManager.onEnter( myRooms.BARTON_204 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList << event.actor 
		event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" )
	}
}

myManager.onExit( myRooms.BARTON_204 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList.remove( event.actor )
	}
}

//Pumpkin Fluff Spawner
pumpFluff204 = myRooms.BARTON_204.spawnSpawner( "pumpFluff204", "pumpkin_fluff", 6 )
pumpFluff204.setPos( 830, 375 )
pumpFluff204.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
pumpFluff204.setMonsterLevelForChildren( 1.0 )

pumpFluff204.stopSpawning()

//=========================================
// JACK-O-LANTERN SWITCH                   
//=========================================

//TODO : Set things up so that trick or treating only occurs at night (when the pumpkins light up)
// GST test : Turns ON pumpkins at night, turns them OFF during day.
// Pumpkins are locked during the day.
// Pumpkins turn off when activated at night, but immediately turn back ON for the next player. (Light flickers when used.)

jack204 = makeSwitch( "jack204", myRooms.BARTON_204, 840, 475 )
jack204.setRange( 200 )

def startTrickOrTreating = { event ->
	jack204.on()
	if( !event.actor.hasQuestFlag( GLOBAL, "TrickOrTreatActive" ) ) {
		//set a preventative flag that expires in 5 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "TrickOrTreatActive" ) 
		myManager.schedule(5) { event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" ) }
		//now push the trick or treat dialog
		event.actor.setQuestFlag( GLOBAL, "Z01TrickOrTreat204A" )
		Perry.pushDialog( event.actor, "trickOrTreat" )
	}
}
	
jack204.whenOff( startTrickOrTreating )

//Variable Initialization & Master Timer
fluffsAlreadySpawned = false
hourCounter = System.currentTimeMillis()
trickTreatTimer()
jackOLanternTimer()

//Every hour, update the hourCounter variable
def trickTreatTimer() {
	myManager.schedule(4200){ hourCounter = System.currentTimeMillis(); trickTreatTimer() }
	
}

//Turn on and unlock Jack O'Lanterns at night. Turn them off during day and lock them.
def jackOLanternTimer() {
//	println "**** GST = ${gst()} ****"
	if( ( gst() > 1800 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 600 ) ) { //trick or treating allowed between 6pm and 6am.
		jack204.unlock()
		jack204.on()
	} else {
		jack204.lock()
		jack204.off()
	}
	myManager.schedule(120) { jackOLanternTimer() } //check time of day again in five minutes
}
	
//=========================================
// DIALOG AND SCRIPT LOGIC                 
//=========================================

//determine how to respond to the player
def trickOrTreat = Perry.createConversation( "trickOrTreat", true, "Z01TrickOrTreat204A" )

TT1 = [id:1]
TT1.npctext = "Happy Halloween!"
TT1.flag = "!Z01TrickOrTreat204A"
TT1.exec = { event ->
	if( event.player.getPlayerVar( "Z01VisitedPerry" ).longValue() == null || event.player.getPlayerVar( "Z01VisitedPerry" ).longValue() < hourCounter ) {
		def awardList = playerList.clone().intersect( event.player.getCrew() )
		awardList.each{
			//don't allow crewmembers that already got treats from this pumpkin to get them again just because they're in a different crew.
			if( it.getPlayerVar( "Z01VisitedPerry" ).longValue() >= hourCounter ) {
				it.setQuestFlag( GLOBAL, "Z01NotYet" )
				Perry.pushDialog( it, "notYet" )
			}
		} 
		event.player.setQuestFlag( GLOBAL, "Z01TrickOrTreat204B" )
		Perry.pushDialog( event.player, "trickOrTreat2" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z01NotYet" )
		Perry.pushDialog( event.player, "notYet" )
	}
}
trickOrTreat.addDialog( TT1, Perry )

//If the player has received a treat too recently then don't give him a treat
def notYet = Perry.createConversation( "notYet", true, "Z01NotYet" )

nYet1 = [id:1]
nYet1.npctext = "Wait a sec! I talked to you already tonight! Try again tomorrow night."
nYet1.flag = "!Z01NotYet"
nYet1.result = DONE
notYet.addDialog( nYet1, Perry )

//if the player qualifies for a treat or trick, then get this conversation instead
def trickOrTreat2 = Perry.createConversation( "trickOrTreat2", true, "Z01TrickOrTreat204B" )

TT2 = [id:1]
TT2.playertext = "Trick or Treat?"
TT2.flag = "!Z01TrickOrTreat204B"
TT2.result = DONE
TT2.exec = { event ->
	def awardList = playerList.clone().intersect( event.player.getCrew() )
	awardList.clone().each{ if( it.getPlayerVar( "Z01VisitedPerry" ).longValue() >= hourCounter ) { awardList.remove( it ) } }
	awardList.each{
		//reset the playerVar hourCounter in case people try to exploit by joining the Crew after the first conversation dialog.
		it.setPlayerVar( "Z01VisitedPerry", hourCounter )

		//increment the number of pumpkins clicked counter
		it.addToPlayerVar( "Z01JackOLanternsClicked", 1 )

		//check for Badge Updates
		if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 50 && it.getPlayerVar( "Z01JackOLanternsClicked" ) < 200 && !it.isDoneQuest(276) ) {
			it.updateQuest( 276, "Story-VQS" ) //Update the "Jack O'Smasher" badge
		} else if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 200 && !it.isDoneQuest(277) ) {
			it.updateQuest( 277, "Story-VQS" ) //Update the "Pumpkin King" badge
		}

		//determine if it's a trick or treat.
		roll = random( 100 )
		def player = it
		if( roll <= 40 ) { //This value is the chance of a trick. Remainder is the treat percentage.
			player.centerPrint( "You get TRICKED!" )
			trick( player, awardList )
		} else {
			player.centerPrint( "You get a TREAT!" )
			treat( player, awardList )
		}
	}
}
trickOrTreat2.addDialog( TT2, Perry )

//=========================================
// TRICKS!!!                               
//=========================================
def synchronized trick( player, awardList ) {
	roll =  random( 100 )
	if( fluffsAlreadySpawned == true && roll <= 45 ) {
		roll = random( 46, 100 )
	}
	if( roll <= 45 ) {
		//pumpkin fluff spawn
		player.centerPrint( "From out of the pumpkin come pumpkin-costumed fluffs to attack you!" )
		fluffsAlreadySpawned = true
		fluffSpawnNum = awardList.size()
		spawnPumpkinFluffs()
	} else if( roll > 45 && roll <= 90 ) {
		//give the player a dentist gift
		dentistGift = random( 100 )
		if( dentistGift <= 50 ) {
			dentistGift = "100479"
			player.centerPrint( "Oh man, are you kidding? Dental Floss?!? Who gives dental floss instead of candy?" )
		} else { 
			dentistGift = "100481"
			player.centerPrint( "No way! A toothbrush? WTF?!? It's Halloween, people!" )
		}
		player.grantItem( dentistGift )
	} else if( roll > 90 ) {
		//warp the player
		player.centerPrint( "You feel a strange energy pulling you away to...somewhere else!" )
		roll = random( 100 )
		if( roll <= 50 ) {
			//somewhere in Barton (one of three places)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "BARTON_301", 580, 200 ) //behind statue
			} else if( roll == 2 ) {
				player.warp( "BARTON_4", 1400, 700 ) //near Dusty in corner
			} else if( roll == 3 ) {
				player.warp( "BARTON_1", 1360, 290 ) //in trees above Maestro
			} 
		} else if( roll > 50 && roll <= 80 ) {
			//somewhere in Village Greens (three places, one is nasty)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "VILLAGE_3", 310, 380 ) //in quiet corner at top of map
			} else if( roll == 2 ) {
				player.warp( "VILLAGE_201", 155, 375 ) //Way over by the goof marker at top of west side of map
			} else if( roll == 3 ) {
				player.warp( "VILLAGE_1004", 900, 590 ) //Down by the flamingo spiral at the bottom, right of the map. DANGER!!!
			} 
		} else if( roll > 80 && roll <= 95 ) {
			//danger spots in other zones near Crystals (pull a random one from a map with coords for warping)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "ZENGARDEN_502", 500, 625 ) //in Katsumi's shrine
			} else if( roll == 2 ) {
				player.warp( "BASSKEN_403", 350, 330 ) //By Logan
			} else if( roll == 3 ) {
				player.warp( "Aqueduct_602", 1330, 530 ) //Up by the True Believers
			} 
		} else if( roll > 95 ) {
			player.warp( "Hive_1", 735, 530 ) //YIKES! THE HIVE!!
		}
	}
}

def spawnPumpkinFluffs() {
	if( fluffSpawnNum > 0 ) {
		fluffList << pumpFluff204.forceSpawnNow() //This list starts at position 0, so subtract one from the list size to get the correct list position
		fluffList.get( fluffList.size() - 1 ).addHate( random ( playerList ), 1 ) 
		runOnDeath( fluffList.get( fluffList.size() - 1 ) ) { event -> fluffList.remove( fluffList.get( fluffList.size() - 1 ) ); checkForLoot( event ) }
		fluffSpawnNum -- 
		myManager.schedule(1) { spawnPumpkinFluffs() } 
	} else {
		myManager.schedule(30) { disposalTimer() }
	}
}

def synchronized checkForLoot( event ) {
	def awardList = playerList.clone().intersect( event.killer.getCrew() )
	awardList.each {
		roll = random( 100 )
		if( !it.hasQuestFlag( GLOBAL, "Z01GotOhMyGumball10" ) ) {
			if( roll <= 25 ) { //trick is 50% and pumpkin fluffs are 50% of that and this is 20% of that, so 2.5% chance of getting the item for each fluff killed.
				it.centerPrint( "Happy Halloween! You found the 'Oh My Gumball'!" )
				it.grantItem( "28768" ) //TODO: Fill in with the correct item ID
				it.setQuestFlag( GLOBAL, "Z01GotOhMyGumball10" )
			}
		} else { //if player has already received the 'Oh My Gumball' item
			if( roll <= 25 ) { //only tell them about the 'Oh My Gumball' if they would have normally received it
				it.centerPrint( "You've already found the 'Oh My Gumball', so all you find this time is pumpkin guts." )
			}
		}			
	}
}

def disposalTimer() {
	if( !fluffList.isEmpty() ) {
		fluffList.each{ it.dispose() }
	}
	fluffsAlreadySpawned = false
}

//=========================================
// TREATS!!!                               
//=========================================

commonList = [ 100272, 100289, 100385, 100297, 100289, 100397, 100291, 100262, 100263, 100298, 100388, 100278, 100275, 100283, 100281, 100367, 100411, 100394, 100284, 100405, 100267, 100265, 100285, 100398, 100397, 100376, 100296 ]

uncommonList = [ 100280, 100279, 100270, 100380, 100279, 100384, 100378, 100261, 100271, 100276, 100258, 100268, 100381, 100382, 100282, 100383, 100390, 100299, 100286, 100369, 100400, 100387 ]

recipeList = [ "17766", "17764", "17772", "17758", "17756", "17861", "17857", "17755", "17848", "17849", "17852", "17851", "17850", "17753", "17833", "17831", "17754", "17836", "17835", "17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779" ]

ringList = [ "17716", "17722", "17723", "17737", "17738", "17741", "17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "20253" ]

def synchronized treat( player, awardList ) {
	//scale the returned results by relative con levels for each Crew member
	roll = random( 100 )

	lootMultiplier = player.getConLevel() / 10 //the lower the overall CL of a player in relation to player max CL (currently 10), the lower the reward so the trick or treating can be meaningful to all CLs
	//grant gold
	if( roll <= 50 ) {
		goldGrant = ( random( 100000, 300000 ) * lootMultiplier ).intValue()
		player.grantCoins( goldGrant ) 
		player.centerPrint( "It's raining gold!" )
	//grant common item
	} else if( roll > 50 && roll <= 65 ) {
		player.grantQuantityItem( random( commonList ), random(2,5) )
		player.centerPrint( "All right! A loot item!" )
	//grant uncommon item
	} else if( roll > 65 && roll <= 75 ) {
		player.grantQuantityItem( random( uncommonList ), random(1,3) )
		player.centerPrint( "An uncommon loot item!" )
	//grant recipes
	} else if( roll > 75 && roll <= 85 ) {
		player.grantItem( random( recipeList ) )
		player.centerPrint( "Awesome! A recipe!" )
	//grant orbs
	} else if( roll > 85 && roll <= 95 ) {
		orbGrant = ( 10 * lootMultiplier ).intValue()
		player.grantQuantityItem( 100257, orbGrant )
		player.centerPrint( "Look! Charge Orbs!!" )
	//rings
	} else if( roll > 95 ) {
		player.grantRing( random( ringList ), true )
		player.centerPrint( "Woohoo! A Ring!" )
	}
}
