import com.gaiaonline.mmo.battle.script.*;

//Add a map marker when the script starts up and never turn it off
/*addMiniMapMarker( "fountainGame", "markerOther", "BARTON_103", 520, 640, "Play the Fountain Game" )

//---------------------------------------------------------
// THE QUEUE                                               
//---------------------------------------------------------

//set of unique players that are present currently in BARTON_103
playerSet103 = [] as Set 

//list of all players that have signed up in the Queue. (Must be a List so it can be referenced by index.)
notificationList = [] 

//Create and Maintain the Player List for the room
myManager.onEnter( myRooms.BARTON_103 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet103 << event.actor
		event.actor.unsetQuestFlag( GLOBAL, "Z01ViewingFountainMenus" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01NotRegisteredErrorMessageStarted" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01FountainIntroActivated" ) //unsets the flag in case the user managed to log out with the flag still set, thus preventing the water fountain game from starting
		event.actor.unsetQuestFlag( GLOBAL, "Z01SignedUpConvoOkay" ) //unset this flag in case they have signed up before so they can see that dialog again

		//this is for Aquatic Pete's random conversation
		if(!event.actor.hasQuestFlag(GLOBAL, "Z0AquaticPeteR1") || !event.actor.hasQuestFlag(GLOBAL, "Z0AquaticPeteR2") || !event.actor.hasQuestFlag(GLOBAL, "Z0AquaticPeteR3") || !event.actor.hasQuestFlag(GLOBAL, "Z0AquaticPeteR4")) {
			convoFlag = random( randomFlag )
			event.actor.setQuestFlag( GLOBAL, convoFlag )
		}
	}
}

myManager.onExit( myRooms.BARTON_103 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet103.remove( event.actor )

		//this is for Aquatic Pete's random conversation
		event.actor.unsetQuestFlag( GLOBAL, convoFlag )
	}
}

// The Queue Switch
queue = makeSwitch( "waterFountainQueue", myRooms.BARTON_103, 520, 640 )
queue.off()
queue.unlock()
queue.setRange( 1000 )
queue.setMouseoverText("Click to sign up for Fountain Game")

//Show the introductory dialog to the player signing up so they understand what the Water Fountain Game does
def startQueueCheck = { event ->
	myManager.schedule(1) { queue.off() }
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01ViewingFountainMenus" ) ) {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z01FountainIntroActivated" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z01FountainIntroActivated" )
			tutorialNPC.pushDialog( event.actor, "fountainIntro" )
		} else {
			player = event.player
			makeFountainMenu()
		}
	}
}

queue.whenOn( startQueueCheck )

//The tutorialNPC description of the water fountain game.
def fountainIntro = tutorialNPC.createConversation( "fountainIntro", true )

def intro1 = [id:1]
intro1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/waterFountain.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
intro1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BARTON_103 ) {
		player = event.player
		makeFountainMenu()
	}
}
intro1.result = DONE
fountainIntro.addDialog( intro1, tutorialNPC )


def makeFountainMenu() {
	titleString = "The Fountain Game"
	descripString = "Sign up to play, or check the Tips and Hints for help."
	diffOptions = [ "Sign Up!", "Tips and Hints", , "High Scores!", "Cancel" ]
	
	uiButtonMenu( player, "fountainMenu", titleString, descripString, diffOptions, 300 ) { event ->
		event.actor.setQuestFlag( GLOBAL, "Z01ViewingFountainMenus" )
		if( event.selection == "Sign Up!" ) {
			player = event.actor
			makeNumPlayersMenu()
		}
		if( event.selection == "Tips and Hints" ) {
			closeUiButtonMenu(event.actor, "fountainMenu")
			tutorialNPC.pushDialog( event.actor, "tipsHints" )
		}
		if( event.selection == "High Scores!" ) {
			closeUiButtonMenu(event.actor, "fountainMenu")
			player = event.actor
			makeHighScoresMenu()
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z01ViewingFountainMenus" )
		}
	}
}

def makeNumPlayersMenu() {
	titleString = "How Many Players?"
	descripString = "You can play the Fountain Game with up to four players. How many are playing this time?"
	diffOptions = [ "One Player", "Two Players", "Three Players", "Four Players", "Main Menu" ]
	
	uiButtonMenu( player, "numPlayersMenu", titleString, descripString, diffOptions, 300 ) { event ->
		if( event.selection == "One Player" ) {
			event.actor.setPlayerVar( "Z01FountainGameNumPlayers", 1 )
			event.actor.unsetQuestFlag( GLOBAL, "Z01ViewingFountainMenus" )
			checkCrewSize( event )
		}
		if( event.selection == "Two Players" ) {
			event.actor.setPlayerVar( "Z01FountainGameNumPlayers", 2 )
			event.actor.unsetQuestFlag( GLOBAL, "Z01ViewingFountainMenus" )
			checkCrewSize( event )
		}
		if( event.selection == "Three Players" ) {
			event.actor.setPlayerVar( "Z01FountainGameNumPlayers", 3 )
			event.actor.unsetQuestFlag( GLOBAL, "Z01ViewingFountainMenus" )
			checkCrewSize( event )
		}
		if( event.selection == "Four Players" ) {
			event.actor.setPlayerVar( "Z01FountainGameNumPlayers", 4 )
			event.actor.unsetQuestFlag( GLOBAL, "Z01ViewingFountainMenus" )
			checkCrewSize( event )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			event.actor.unsetQuestFlag( GLOBAL, "Z01ViewingFountainMenus" )
			makeFountainMenu()
		}
	}
}

def makeHighScoresMenu() {
	titleString = "High Score Lists"
	descripString = "See the High Scores for each of the different game sizes!"
	diffOptions = [ "One Player High Scores", "Two Player High Scores", "Three Player High Scores", "Four Player High Scores", "Main Menu" ]
	
	uiButtonMenu( player, "highScoresMenu", titleString, descripString, diffOptions, 300 ) { event ->
		//set the flag to prevent the player from clicking the Fountain while viewing scores (prevents duplication of menus due to confusion)
		
		if( event.selection == "One Player High Scores" ) {
			if( fountainOnePlayerHighScore > 0 ) {
				event.actor.centerPrint( "${fountainOnePlayerHighScore} Rounds!" )
				output = fountainOnePlayerHighScoreNames.join(",")
				event.actor.centerPrint( "Set by: ${output}" )
			} else {
				event.actor.centerPrint( "High score not yet set! Get it!" )
			}
			myManager.schedule(2) { player = event.actor; makeHighScoresMenu() }
		}
		if( event.selection == "Two Player High Scores" ) {
			if( fountainTwoPlayerHighScore > 0 ) {
				event.actor.centerPrint( "${fountainTwoPlayerHighScore} Rounds!" )
				output = fountainTwoPlayerHighScoreNames.join(",")
				event.actor.centerPrint( "$Set by: {output}" )
			} else {
				event.actor.centerPrint( "High score not yet set! Get it!" )
			}
			myManager.schedule(2) { player = event.actor; makeHighScoresMenu() }
		}
		if( event.selection == "Three Player High Scores" ) {
			if( fountainThreePlayerHighScore > 0 ) {
				event.actor.centerPrint( "${fountainThreePlayerHighScore} Rounds!" )
				output = fountainThreePlayerHighScoreNames.join(",")
				event.actor.centerPrint( "$Set by: {output}" )
			} else {
				event.actor.centerPrint( "High score not yet set! Get it!" )
			}
			myManager.schedule(2) { player = event.actor; makeHighScoresMenu() }
		}
		if( event.selection == "Four Player High Scores" ) {
			if( fountainFourPlayerHighScore > 0 ) {
				event.actor.centerPrint( "${fountainFourPlayerHighScore} Rounds!" )
				output = fountainFourPlayerHighScoreNames.join(",")
				event.actor.centerPrint( "$Set by: {output}" )
			} else {
				event.actor.centerPrint( "High score not yet set! Get it!" )
			}
			myManager.schedule(2) { player = event.actor; makeHighScoresMenu() }
		}
		if( event.selection == "Main Menu" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z01ViewingHighScores" )
			player = event.actor
			makeFountainMenu()
		}
		
	}
}

//Tips and Hints for the First Tee menus (below)
def tipsHints = tutorialNPC.createConversation( "tipsHints", true )

def hint1 = [id:1]
hint1.npctext = "<zOMG dialogWidth='300'><![CDATA[<h1><b><font face='Arial' size='14'>Tips and Hints</font></b></h1><font face='Arial' size ='12'><br>The Fountain Game can be challenging. Here are some hints and tips to help you out:<br><br>* Don't forget that you can use Coyote Spirit, Fleet Feet, ring set bonuses, etc., to increase your footspeed and make it easier to get from spot to spot.<br><br>* When playing two-player games, try assigning the two left buttons to one player and the two right ones to the other player.<br><br>* Four-player games generally require that each player be reponsible for a single button.<br><br>]]></zOMG>"
hint1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BARTON_103 ) {
		player = event.actor
		makeFountainMenu()
	}
}
hint1.result = DONE
tipsHints.addDialog( hint1, tutorialNPC )


def checkCrewSize( event ) {
	if( event.actor.getPlayerVar( "Z01FountainGameNumPlayers" ) > event.actor.getCrew().size() ) {
		event.actor.centerPrint( "You don't have that many players in your Crew. Try again." )
		myManager.schedule(2) { player = event.actor; makeNumPlayersMenu() }
	} else {
		//register the Crew and put them in the queue
		//check to see if anyone on your Crew is also on the Crew of someone already on the notificationList
		playerAuthorized = true
		checkListPosition = notificationList.size() - 1
		println "**** completeTheQueueCheck : notificationList = ${notificationList} and checkListPosition = ${checkListPosition} ****"
		checkAllCrewsForDupes( event )	
	}
}

//check to see if the registrering player is already on any other registered Crews
def checkAllCrewsForDupes( event ) {
	println "**** entering checkAllCrewsForDupes ****"
	if( checkListPosition >= 0 ) {
		println "**** checkListPosition = ${ checkListPosition } ****"
		nextRegisteredPlayer = notificationList.get( checkListPosition ) //find the next registered player in the list
		if( nextRegisteredPlayer.getCrew().contains( event.actor ) ) { //if that other Crew contains the same actor trying to sign up now, then tell him he can't sign up.
			event.actor.centerPrint( "You can't register now because you're a member of a Crew that is already signed up." )
			playerAuthorized = false
			queue.unlock()
		}
		checkListPosition --
		checkAllCrewsForDupes( event )
	} else { //if all checks have been made
		if( playerAuthorized == true ) {
			signThemUp( event ) //use "checkToWarp( event )" here instead once it's ready for publication
		}
	}
}

//If they pass all qualifications, then sign them up!
def signThemUp( event ) {
	println "**** signing the crew up now ****"
	queue.off()
	notificationList << event.actor //put this person on the list for notifications
	println "**** notificationList = ${notificationList} ****"
	queue.unlock()
	signUpPlayer = event.actor
	event.actor.getCrew().each{ 
		it.unsetQuestFlag( GLOBAL, "Z01SignedUpConvoOkay" )
		tutorialNPC.pushDialog( it, "youAreSignedUp" )
	}
}

//Water Fountain Notification Dialog
def youAreSignedUp = tutorialNPC.createConversation( "youAreSignedUp", true, "!Z01SignedUpConvoOkay" )

def signed1 = [id:1]
signed1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/thirtySeconds.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
signed1.flag = "Z01SignedUpConvoOkay"
signed1.exec = { event ->
	println "**** event.player = ${ event.player } and signUpPlayer = ${ signUpPlayer } ****"
	if( event.player == signUpPlayer ) {
		myManager.schedule(2) { notifyNewCrewOfQueuePosition( event ); signUpPlayer = null }
	}
}
signed1.result = DONE
youAreSignedUp.addDialog( signed1, tutorialNPC )


//Tell the Crew that just signed up what position they are in on the Queue.
def notifyNewCrewOfQueuePosition( event ) {
	println "**** entering notifyNewCrewOfQueuePosition ****"
	println "**** notificationList.size = ${notificationList.size()} ****"
	if( !notificationList.isEmpty() ) { //don't execute this stuff if the list is empty
		if( event.player == notificationList.get( 0 ) ) { //if they're the only ones on the notificationList, then move them to the start-up process immediately
			nextInQueue = notificationList.get( 0 )
			checkIfStillValid()
		} else { //if not, then tell them where they stand on the queue
			println "****Player's position in queue is ${ notificationList.indexOf( event.player ) } plus one. ****"
			event.player.getCrew().each{
				it.centerPrint( "Your Crew is number ${notificationList.indexOf( event.player ) + 1} out of ${notificationList.size()} Crews registered for the Fountain Game!" )
			}
		}
	}
}

//When a team is moved off the queue, update the other teams as to where their place in the Queue is at this time.
def notifyALLCrewsOfQueuePosition() {
	println "**** entering notifyALLCrewsOfQueuePosition ****"
	println "**** Current listPostion = ${listPosition} out of ${notificationList.size()} in list ****"
	if( listPosition > 0 ) {
		tellThisCrew = notificationList.get( listPosition ) 
		if( isOnline( tellThisCrew ) ) {
			tellThisCrew.getCrew().each{
				it.centerPrint( "Your Crew is number ${listPosition} out of ${notificationList.size()} Crews." )
			}
			listPosition --
		} else {
			notificationList.remove( tellThisCrew ) //weed out the off-line folks and continue the process
		}
		notifyALLCrewsOfQueuePosition()
	} else {
		if( listPosition == 0 ) { //it's this Crew's turn now
			println "**** The next team in queue is found. Checking now to see if they're still valid ****"
			nextInQueue = notificationList.get( 0 )
			checkIfStillValid()
		}
	}
}

//Called when either a) a team no longer qualifies and the next Crew is called instead, or b) when the previously competing Crew finishes their game.
def moveToNextCrewInQueue() {
	println "**** entering moveToNextCrewInQueue ****"
	if( notificationList.size() > 0 ) {
		println "**** MOVING TO NEXT IN QUEUE. notificationList = ${notificationList} *****"
		notificationList.remove( notificationList.get( 0 ) ) //remove the current player's name from the notificationList
		println "**** AFTER REMOVAL. notificationList = ${notificationList} ****"
		if( notificationList.size() > 0 ) {
			if( isOnline( notificationList.get( 0 ) ) ) {
				nextInQueue = notificationList.get( 0 ) //get the player's name that's at the top of the notificationList
				listPosition = notificationList.size() - 1
				notifyALLCrewsOfQueuePosition()
			} else {
				moveToNextCrewInQueue() //if the next in queue is off-line, then move to next one down the line
			}
		}
	} else {
		println "**** notificationList is empty ****"
	}
}

//Ensure that when a Crew gets its turn that they are still valid as a team
def checkIfStillValid() {
	println "**** entering checkIfStillValid ****"
	if( playerSet103.contains( nextInQueue ) ) { //the registered player must be in BARTON_103 or the Crew is bumped off the list and must re-register
	
		//if the crew size of the player has reduced to less the number of players previously selected
		if( nextInQueue.getCrew().size() < nextInQueue.getPlayerVar( "Z01FountainGameNumPlayers" ) ) { 
			println "**** crew size too small. Disqualify this crew. ****"
			nextInQueue.getCrew().each{ it.centerPrint( "You need more Crewmates! You signed up for ${nextInQueue.getPlayerVar( "Z01FountainGameNumPlayers" )} players. Register again!" ) }
			moveToNextCrewInQueue()

		} else { //If everything's good, then notify that it's their turn and start the game timer!
			println "**** crew is authorized! ****"
			registrant = nextInQueue
			authorizedCrew = nextInQueue.getCrew()
			println "**** authorizedCrew = ${ authorizedCrew } ****"
			authorizedCrew.each{ tutorialNPC.pushDialog( it, "thirtySecondPartTwo" ) }
			myManager.schedule(2) {
				//start the timer
				timeExpired = false
				myManager.schedule(30) {
					timeExpired = true
					if( alreadyStarted == false ) { //if the game hasn't started, tell them that they didn't get there in time
						authorizedCrew.clone().each { it.centerPrint( "You have to start within 30 seconds of being notified it's your turn! Re-register your Crew to start again." ) }
						authorizedCrew = [] //reset the "authorizedCrew" list back to null so the player can't keep trying to trigger the buttons
						//reset the buttons states
						yellowButton.off()
						violetButton.off()
						pinkButton.off()
						goldButton.off()
						yellowOccupied = false
						violetOccupied = false
						pinkOccupied = false
						goldOccupied = false
						awardSet.clear()
						moveToNextCrewInQueue()		
					}
				}
			}
		}
	} else { //if the player wasn't in BARTON_103, then do the following
		println "**** the registering player was not present...disqualify this crew. ****"
		nextInQueue.getCrew().each{ it.centerPrint( "The registering player was not in the fountain area when your Crew's turn arrived." ) }
		myManager.schedule(3) { nextInQueue.getCrew().each{ it.centerPrint( "So the next team gets to go now. Re-register to try again!" ) } }
		moveToNextCrewInQueue()
	}
}

//Water Fountain Notification Dialog
def thirtySecondPartTwo = tutorialNPC.createConversation( "thirtySecondPartTwo", true )

def partTwo1 = [id:1]
partTwo1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/thirtySecondsPartTwo.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
partTwo1.result = DONE
thirtySecondPartTwo.addDialog( partTwo1, tutorialNPC )


//Ensure that when a Crew gets its turn that they are still valid as a team
def checkIfTeamIsStillValid( event ) {
	println "**** entering checkIfTeamIsStillValid ****"
	//check if all members of the awardSet are in the player's crew. If not, then disqualify the team because someone quit.
	awardSet.each{
		if( !event.actor.getCrew().contains( it ) ) { //if the actor's crew list does NOT contain the award list member being checked, then disqualify the team
			println "**** one of the team members is missing...disqualify the team ****"
			disqualifyTeam()
		}		
	}
}

def disqualifyTeam() {
	playerTurn = false
	sound("ERROR").toZone()
	myManager.schedule(0.5){ sound("ERROR").toZone() }
	myManager.schedule(1.0){ sound("ERROR").toZone() }
	awardSet.each{ it.centerPrint( "One of your team is no longer in your Crew. You'll need to restart." ) }
	myManager.schedule(3) { awardSet.clone().each{ it.centerPrint( "You made it to round ${round}! Congrats!" ) } }
	myManager.schedule(6) { checkForHighScore() }
}
	

//---------------------------------------------------------
// TRIGGER DEFINITIONS                                     
//---------------------------------------------------------

def yellowTrigger = "yellowTrigger"
myRooms.BARTON_103.createTriggerZone( yellowTrigger, 840, 525, 950, 605 )

def violetTrigger = "violetTrigger"
myRooms.BARTON_103.createTriggerZone( violetTrigger, 710, 835, 820, 900 )

def pinkTrigger = "pinkTrigger"
myRooms.BARTON_103.createTriggerZone( pinkTrigger, 120, 765, 220, 845 )

def goldTrigger = "goldTrigger"
myRooms.BARTON_103.createTriggerZone( goldTrigger, 240, 470, 350, 545 )


//---------------------------------------------------------
// GAME STARTING CONDITIONS & TRIGGER LOGIC                
//---------------------------------------------------------

authorizedCrew = [] //the next Crew in the Queue that has a turn
awardSet = [] as Set //the list of four players that start the game. Any badges earned are awarded to these people only and others don't, even if they are 4 out of 6 in a Crew.
bounceSet = [] as Set
checkMe = 0

alreadyStarted = false
yellowOccupied = false
violetOccupied = false
pinkOccupied = false
goldOccupied = false

yellowSet = [] as Set
violetSet = [] as Set
pinkSet = [] as Set
goldSet = [] as Set

//YELLOW TRIGGERS
myManager.onTriggerIn(myRooms.BARTON_103, yellowTrigger) { event ->
//	println "**** YELLOW BUTTON : authorizedCrew = ${ authorizedCrew } and awardSet = ${ awardSet } and checkMe = ${ checkMe } ****"

	if( isPlayer( event.actor ) ) {
		yellowSet << event.actor
	}

	//if the player on the button isn't authorized...then...
	if( isPlayer( event.actor ) && !authorizedCrew.isEmpty() && !authorizedCrew.contains( event.actor ) ) { 
		bounceButton = "yellow"
		bounceOffButtons()
	}
	
	//if the game hasn't started and the player on the button IS authorized...then...
	if( isPlayer( event.actor) && authorizedCrew.contains( event.actor ) && alreadyStarted == false ) {  
		yellowOccupied = true
		yellowButton.on()
		sound("ZING").toPlayer( event.actor )
		if( awardSet.isEmpty() || !awardSet.contains( event.actor ) ) {
			awardSet << event.actor //add the name of the authorized player onto the "award list"
			println "**** Adding ${event.actor} to awardSet ****"
		}
		checkForStart() //check if enough players are on the spots
	}
	//if the game has started and this actor is authorized to play now, then...
	if( isPlayer( event.actor ) && playerTurn == true && awardSet.contains( event.actor ) ) { 
		//check if all team members are still present in the actor's crew list
		checkIfTeamIsStillValid( event )
		println "**** ${event.actor} stepping on button during player turn ****"
		if( tonePlayback.get( checkMe) == "ZING!" ) {
			println "****CORRECT BUTTON****"
			yellowButton.on()
			sound("ZING").toZone()
			myManager.schedule(0.5) { yellowButton.off() }
			checkMe ++
			checkWin()
		} else {
			println "****WRONG BUTTON****"
			sequenceTimer.cancel() //stop the round timer so the users don't also fail due to taking too much time
			playerTurn = false
			sequenceEnded = true
			yellowButton.on()
			sound("ERROR").toZone()
			myManager.schedule(0.5) { yellowButton.off() }
			authorizedCrew.clone().each{ it.centerPrint( "Wrong Button! Game Over!" ) }
			myManager.schedule(3) { authorizedCrew.clone().each{ it.centerPrint( "You made it to round ${round.intValue()}! Congrats!" ) } }
			myManager.schedule(6) { checkForHighScore() }
		}
	}
}

myManager.onTriggerOut( myRooms.BARTON_103, yellowTrigger ) { event ->
	if( isPlayer( event.actor ) ) {
		yellowSet.remove( event.actor )
	}
	if( isPlayer( event.actor ) && authorizedCrew.contains( event.actor ) && alreadyStarted == false ) { //if an authorized person leaves a spot before the game begins, then remove them from the award list and don't start without them
		println "**** Player left YELLOW BUTTON ****"
		yellowOccupied = false
		yellowButton.off()
		awardSet.remove( event.actor )
	}
}

//VIOLET TRIGGERS
myManager.onTriggerIn(myRooms.BARTON_103, violetTrigger) { event ->
//	println "**** VIOLET BUTTON : authorizedCrew = ${ authorizedCrew } and awardSet = ${ awardSet } and checkMe = ${ checkMe } ****"
	if( isPlayer( event.actor ) ) {
		violetSet << event.actor
	}
	if( isPlayer( event.actor ) && !authorizedCrew.isEmpty() && !authorizedCrew.contains( event.actor ) ) { 
		bounceButton = "violet"
		bounceOffButtons()
	}
	if( isPlayer( event.actor) && authorizedCrew.contains( event.actor ) && alreadyStarted == false ) {  //if the game hasn't started and the player on the button IS authorized...then...
		violetOccupied = true
		violetButton.on()
		sound("RING").toZone()
		if( awardSet.isEmpty() || !awardSet.contains( event.actor ) ) {
			awardSet << event.actor //add the name of the authorized player onto the "award list"
			println "**** Adding ${event.actor} to awardSet ****"
		}
		checkForStart() //check if enough players are on the spots
	}
	if( isPlayer( event.actor ) && playerTurn == true && awardSet.contains( event.actor ) ) {
		//check if all team members are still present in the actor's crew list
		checkIfTeamIsStillValid( event )
		println "**** ${event.actor} stepping on button during player turn ****"
		if( tonePlayback.get( checkMe) == "RING!" ) {
			println "****CORRECT BUTTON****"
			violetButton.on()
			sound("RING").toZone()
			myManager.schedule(0.5) { violetButton.off() }
			checkMe ++
			checkWin()
		} else {
			println "****WRONG BUTTON****"
			sequenceTimer.cancel() //stop the round timer so the users don't also fail due to taking too much time
			playerTurn = false
			sequenceEnded = true
			violetButton.on()
			sound("ERROR").toZone()
			myManager.schedule(0.5) { violetButton.off() }
			authorizedCrew.clone().each{ it.centerPrint( "Wrong Button! Game Over!" ) }
			myManager.schedule(3) { authorizedCrew.clone().each{ it.centerPrint( "You made it to round ${round.intValue()}! Congrats!" ) } }
			myManager.schedule(6) { checkForHighScore() }
		}
	}
}

myManager.onTriggerOut( myRooms.BARTON_103, violetTrigger ) { event ->
	if( isPlayer( event.actor ) ) {
		violetSet.remove( event.actor )
	}
	if( isPlayer( event.actor ) && authorizedCrew.contains( event.actor ) && alreadyStarted == false ) { //if an authorized person leaves a spot before the game begins, then remove them from the award list and don't start without them
		println "**** Player left VIOLET BUTTON ****"
		violetOccupied = false
		violetButton.off()
		awardSet.remove( event.actor )
	}
}


//PINK TRIGGERS
myManager.onTriggerIn(myRooms.BARTON_103, pinkTrigger) { event ->
//	println "**** PINK BUTTON : authorizedCrew = ${ authorizedCrew } and awardSet = ${ awardSet } and checkMe = ${ checkMe } ****"
	if( isPlayer( event.actor ) ) {
		pinkSet << event.actor
	}
	if( isPlayer( event.actor ) && !authorizedCrew.isEmpty() && !authorizedCrew.contains( event.actor ) ) { 
		bounceButton = "pink"
		bounceOffButtons()
	}
	if( isPlayer( event.actor) && authorizedCrew.contains( event.actor ) && alreadyStarted == false ) {  //if the game hasn't started and the player on the button IS authorized...then...
		pinkOccupied = true
		pinkButton.on()
		sound("PLING").toZone()
		if( awardSet.isEmpty() || !awardSet.contains( event.actor ) ) {
			awardSet << event.actor //add the name of the authorized player onto the "award list"
			println "**** Adding ${event.actor} to awardSet ****"
		}
		checkForStart() //check if enough players are on the spots
	}
	if( isPlayer( event.actor ) && playerTurn == true && awardSet.contains( event.actor ) ) {
		//check if all team members are still present in the actor's crew list
		checkIfTeamIsStillValid( event )
		println "**** ${event.actor} stepping on button during player turn ****"
		if( tonePlayback.get( checkMe) == "PLING!" ) {
			println "****CORRECT BUTTON****"
			pinkButton.on()
			sound("PLING").toZone()
			myManager.schedule(0.5) { pinkButton.off() }
			checkMe ++
			checkWin()
		} else {
			println "****WRONG BUTTON****"
			sequenceTimer.cancel() //stop the round timer so the users don't also fail due to taking too much time
			sequenceEnded = true
			playerTurn = false
			pinkButton.on()
			sound("ERROR").toZone()
			myManager.schedule(0.5) { pinkButton.off() }
			authorizedCrew.clone().each{ it.centerPrint( "Wrong Button! Game Over!" ) }
			myManager.schedule(3) { authorizedCrew.clone().each{ it.centerPrint( "You made it to round ${round.intValue()}! Congrats!" ) } }
			myManager.schedule(6) { checkForHighScore() }
		}
	}
}

myManager.onTriggerOut( myRooms.BARTON_103, pinkTrigger ) { event ->
	if( isPlayer( event.actor ) ) {
		pinkSet.remove( event.actor )
	}
	if( isPlayer( event.actor ) && authorizedCrew.contains( event.actor ) && alreadyStarted == false ) { //if an authorized person leaves a spot before the game begins, then remove them from the award list and don't start without them
		println "**** Player left PINK BUTTON ****"
		pinkOccupied = false
		pinkButton.off()
		awardSet.remove( event.actor )
	}
}


//GOLD TRIGGERS
myManager.onTriggerIn(myRooms.BARTON_103, goldTrigger) { event ->
//	println "**** GOLD BUTTON : authorizedCrew = ${ authorizedCrew } and awardSet = ${ awardSet } and checkMe = ${ checkMe } ****"
	if( isPlayer( event.actor ) ) {
		goldSet << event.actor
	}
	if( isPlayer( event.actor ) && !authorizedCrew.isEmpty() && !authorizedCrew.contains( event.actor ) ) { 
		bounceButton = "gold"
		bounceOffButtons()
	}
	if( isPlayer( event.actor) && authorizedCrew.contains( event.actor ) && alreadyStarted == false ) {  //if the game hasn't started and the player on the button IS authorized...then...
		goldOccupied = true
		goldButton.on()
		sound("CLANK").toZone()
		if( awardSet.isEmpty() || !awardSet.contains( event.actor ) ) {
			awardSet << event.actor //add the name of the authorized player onto the "award list"
			println "**** Adding ${event.actor} to awardSet ****"
		}
		checkForStart() //check if enough players are on the spots
	}
	if( isPlayer( event.actor ) && playerTurn == true && awardSet.contains( event.actor ) ) {
		//check if all team members are still present in the actor's crew list
		checkIfTeamIsStillValid( event )
		println "**** ${event.actor} stepping on button during player turn ****"
		if( tonePlayback.get( checkMe) == "CLANK!" ) {
			println "****CORRECT BUTTON****"
			goldButton.on()
			sound("CLANK").toZone()
			myManager.schedule(0.5) { goldButton.off() }
			checkMe ++
			checkWin()
		} else {
			println "****WRONG BUTTON****"
			sequenceTimer.cancel() //stop the round timer so the users don't also fail due to taking too much time
			sequenceEnded = true
			playerTurn = false
			goldButton.on()
			sound("ERROR").toZone()
			myManager.schedule(0.5) { goldButton.off() }
			authorizedCrew.clone().each{ it.centerPrint( "Wrong Button! Game Over!" ) }
			myManager.schedule(3) { authorizedCrew.clone().each{ it.centerPrint( "You made it to round ${round.intValue()}! Congrats!" ) } }
			myManager.schedule(6) { checkForHighScore() }
		}
	}
}

myManager.onTriggerOut( myRooms.BARTON_103, goldTrigger ) { event ->
	if( isPlayer( event.actor ) ) {
		goldSet.remove( event.actor )
	}
	if( isPlayer( event.actor ) && authorizedCrew.contains( event.actor ) && alreadyStarted == false ) { //if an authorized person leaves a spot before the game begins, then remove them from the award list and don't start without them
		println "**** Player left GOLD BUTTON ****"
		goldOccupied = false
		goldButton.off()
		awardSet.remove( event.actor )
	}
}



//IF all four spots are occupied with authorized players, then start the game
def checkForStart() {
//	println "**** entering checkForStart ****"
//	println "**** ${yellowOccupied}, ${violetOccupied}, ${pinkOccupied}, ${goldOccupied} ****"
//	println "**** awardSet = ${awardSet} ****"
	numOccupied = 0
	if( yellowOccupied == true ) { numOccupied ++ }
	if( violetOccupied == true ) { numOccupied ++ }
	if( pinkOccupied == true ) { numOccupied ++ }
	if( goldOccupied == true ) { numOccupied ++ }
	
	println "**** REGISTRANT = ${registrant} ****"
	if( numOccupied >= registrant.getPlayerVar( "Z01FountainGameNumPlayers" ) ) {
		yellowOccupied = false
		violetOccupied = false
		pinkOccupied = false
		goldOccupied = false
		startGame()
	}
}

//================================
// If the player isn't authorized,
// then bounce them off the button
//================================
def bounceOffButtons() {
	bounceSet.clear()
	
	println "**** entering bounceOffButtons ****"
	//knock them off the button
	yellowSet.each{
		if( !authorizedCrew.contains( it ) ) {
			knockBack( it, yellowButton, 600 )
			bounceSet << it
			sound("ERROR").toPlayer( it )
		}
	}
	violetSet.each{
		if( !authorizedCrew.contains( it ) ) {
			knockBack( it, violetButton, 600 )
			bounceSet << it
			sound("ERROR").toPlayer( it )
		}
	}
	pinkSet.each{
		if( !authorizedCrew.contains( it ) ) {
			knockBack( it, pinkButton, 600 )
			bounceSet << it
			sound("ERROR").toPlayer( it )
		}
	}
	goldSet.each{
		if( !authorizedCrew.contains( it ) ) {
			knockBack( it, goldButton, 600 )
			bounceSet << it
			sound("ERROR").toPlayer( it )
		}
	}
	warnThem()
}

def warnThem() {
	bounceSet.each{
		//now warn them not to do it again
		if( it.getPlayerVar( "Z01ButtonWarningStage" ) == 0 ) {
			it.centerPrint( "A game is in progress. Please stay off the buttons until it's your turn." )
			it.addToPlayerVar( "Z01ButtonWarningStage", 1 )
			myManager.schedule(120){ it.addToPlayerVar( "Z01ButtonWarningStage", -1 ) } //decrement the counter by one after 120 seconds
		} else if( it.getPlayerVar( "Z01ButtonWarningStage" ) == 1 ) {
			it.centerPrint( "A game is in progress. Wait your turn, please. Really!" )
			it.instantPercentDamage( random( 25, 50 ) ) 
			it.addToPlayerVar( "Z01ButtonWarningStage", 1 )
			myManager.schedule(240){ it.addToPlayerVar( "Z01ButtonWarningStage", -1 ) } //decrement the counter by one after 240 seconds
		} else if( it.getPlayerVar( "Z01ButtonWarningStage" ) >= 2 ) {
			it.centerPrint( "You've been sent far away since you seem obsessively attracted to the buttons." )
			it.warp( "nullChamber1_1", 1030, 655 )
			it.addToPlayerVar( "Z01ButtonWarningStage", 1 )
			myManager.schedule(480){ it.addToPlayerVar( "Z01ButtonWarningStage", -1 ) } //decrement the counter by one after 480 seconds
		}
	}
}

//---------------------------------------------------------
// FOUNTAIN BUTTON SWITCHES                                
// These switches are lit by script only. No player        
// interaction occurs. (Trigger zones do all the work.)    
//---------------------------------------------------------

//YELLOW BUTTON
yellowButton = makeSwitch( "yellowButton", myRooms.BARTON_103, 730, 515 )
yellowButton.lock()
yellowButton.setUsable( false )

//VIOLET BUTTON
violetButton = makeSwitch( "violetButton", myRooms.BARTON_103, 850, 800 )
violetButton.lock()
violetButton.setUsable( false )

//PINK BUTTON
pinkButton = makeSwitch( "pinkButton", myRooms.BARTON_103, 305, 875 )
pinkButton.lock()
pinkButton.setUsable( false )

//GOLD BUTTON
goldButton = makeSwitch( "goldButton", myRooms.BARTON_103, 205, 580 )
goldButton.lock()
goldButton.setUsable( false )

//---------------------------------------------------------
// FOUNTAIN LOGIC                                          
//---------------------------------------------------------

// The must be maximum intervals between the inputs on the buttons.    
// If that interval is exceeded, then game is lost.                    

// Start with a two-sequence, then three-sequence, up to a 10-sequence.
// Then reduce the interval and start again                            
// Once down to 0.5 second interval, start randomizing the interval for
// increased difficulty and enforce a minimum threshold also.          

playerTurn = false

toneList = []
toneList << "ZING!" //yellow
toneList << "RING!" //violet
toneList << "PLING!" //pink
toneList << "CLANK!" //gold

def startGame() {
	println "**** entering startGame ****"
	alreadyStarted = true //sets a flag to to avoid the 30-second timer from expiring and stopping the game. Also for use on the triggerIn zones to create the awardSet.
	randomSequences = false
	roundFailed = false
	round = 0
	lap = 0
	seqCount = round
	tonePlayback = []
	authorizedCrew.clone().each{ it.setPlayerVar( "Z01AlreadySignedUp", 0 ) } //set the "alreadySignedUp" flag to "false" (0) so the warnings work correctly in the next game.
	
	playerSet103.clone().each{ it.centerPrint( "Clear the BUTTONS! The Fountain Game is Starting!!!" ) }  //broadcast the centerPrints to the entire room
	
	myManager.schedule(0.5) { bounceOffButtons() }
	
	myManager.schedule(1.0) { yellowButton.on(); violetButton.on(); pinkButton.on(); goldButton.on(); sound("ZING").toZone(); sound("RING").toZone(); sound("PLING").toZone(); sound("CLANK").toZone() }
	
	myManager.schedule(1.5) { yellowButton.off(); violetButton.off(); pinkButton.off(); goldButton.off() }
	
	myManager.schedule(2.0) { yellowButton.on(); violetButton.on(); pinkButton.on(); goldButton.on(); sound("ZING").toZone(); sound("RING").toZone(); sound("PLING").toZone(); sound("CLANK").toZone() }
	
	myManager.schedule(2.5) { yellowButton.off(); violetButton.off(); pinkButton.off(); goldButton.off() }
	
	myManager.schedule(3.0) { yellowButton.on(); violetButton.on(); pinkButton.on(); goldButton.on(); sound("ZING").toZone(); sound("RING").toZone(); sound("PLING").toZone(); sound("CLANK").toZone() }
	
	myManager.schedule(3.5) { yellowButton.off(); violetButton.off(); pinkButton.off(); goldButton.off() }

	numPlayers = registrant.getPlayerVar( "Z01FountainGameNumPlayers" )
	
	//adjust the base interval
	if( numPlayers == 1 ) { interval = 4.0 }
	else if( numPlayers == 2 ) { interval = 2.5 }
	else if( numPlayers == 3 ) { interval = 2.0 }
	else if( numPlayers == 4 ) { interval = 1.0 }
	
	myManager.schedule(4.0) { nextRound() }
}

def nextRound() {
	sequenceEnded = false

	println "**** entering nextRound ****"
	round ++
	//time to complete the next button press dimishes each and every round
	if( round > 1 ) {
		interval = interval * 0.98
	}
	
	intervalSelector = round - ( lap * 4 )
	
	if( round-1 != 0 && (round-1) % 4 == 0 ) { //everytime you get to the first lap on a round, increment lap by one.
		lap ++
	}	
	seqCount = 2 + lap + ( (round-1) % 4 ) //min length raises each lap, and goes to base + 4 by end of round
	seqPlaybackCount = seqCount
	tonePlayback.clear()

	println "**** round = ${round} ****"
	println "**** lap = ${lap} ****"
	println "**** seqCount = ${seqCount} ****"
	
	myManager.schedule(2) { authorizedCrew.clone().each{ it.centerPrint( "Watch the pattern! Repeat the pattern as fast as you can!" ) }; myManager.schedule(1) { playSequence() } }
}
		
def playSequence() {
	if( seqPlaybackCount >= 1 ) {
		tone = random( toneList )
		if( tone == "ZING!" ) {
			yellowButton.on()
			sound("ZING").toZone()
			myManager.schedule(0.75) { yellowButton.off() }
		}
		if( tone == "RING!" ) {
			violetButton.on()
			sound("RING").toZone()
			myManager.schedule(0.75) { violetButton.off() }
		}
		if( tone == "PLING!" ) {
			sound("PLING").toZone()
			pinkButton.on()
			myManager.schedule(0.75) { pinkButton.off() }
		}
		if( tone == "CLANK!" ) {
			sound("CLANK").toZone()
			goldButton.on()
			myManager.schedule(0.75) { goldButton.off() }
		}
		seqPlaybackCount -- 
		tonePlayback << tone
		myManager.schedule(1) { playSequence() }
	} else {
		println "**** Sequence = ${tonePlayback} ****"
		seqTimer = interval * seqCount
		authorizedCrew.clone().each{ it.centerPrint( "Get Ready!" ) }
		authorizedCrew.clone().each{ it.centerPrint( "Next sequence length: ${seqCount.intValue()}!" ) }
		authorizedCrew.clone().each{ it.centerPrint( "Time to complete: ${seqTimer.intValue()} seconds" ) }
		myManager.schedule(1) { startPlayerTurn() }
	}
}

def startPlayerTurn() {
	println "**** startPlayerTurn entered ****"
	playerTurn = true
	checkMe = 0
	countdown = seqTimer
	countdownClock()
	sequenceTimer = myManager.schedule( seqTimer ) {
		roundFailed = true
		yellowButton.on(); violetButton.on(); pinkButton.on(); goldButton.on()
		sound("ERROR").toZone()
		authorizedCrew.clone().each{ it.centerPrint( "Too bad! You took too long to complete the pattern!" ) }
		myManager.schedule(0.5) { yellowButton.off(); violetButton.off(); pinkButton.off(); goldButton.off() }
		myManager.schedule(3) { authorizedCrew.clone().each{ it.centerPrint( "You made it to round ${round.intValue()}! Congrats!" ) } }
		myManager.schedule(6) { checkForHighScore() }
	}			
}

def countdownClock() {
	if( countdown > 0 && sequenceEnded == false ) {
		authorizedCrew.clone().each{ it.centerPrint( "${countdown.intValue()}" ) }
		countdown --
		myManager.schedule(1) { countdownClock() }
	}
}

woohooList = [ "Woohoo!", "Oh yeah!", "Awesome!", "On fire!", "Bring it on!", "Fantastic!", "Superb!", "Fantabulous!", "Next!", "Tight!", "r0xx0r!", "Ready for more!", "Success!", "Not bad!", "All RIGHT!", "Can't touch this!" ]

totalGold = 0
totalOrb = 0
goldGrant = 0
orbGrant = 0

def checkWin() {
	println "**** checkWin entered ****"
	if( checkMe == seqCount ) {
		sequenceEnded = true
		if( roundFailed == false ) { //if the round timer hasn't expired, then keep going!
			sequenceTimer.cancel() //stop the round timer so the users don't fail due to taking too much time
			playerTurn = false
			
			//figure out gold rewards for the round
			if( round <= lapThreshold ) {
				goldGrant = 10
			} else if( round > lapThreshold && round <= lapThreshold * 3 ) {
				goldGrant = 20
			} else if( round > lapThreshold * 3 && round <= lapThreshold * 6 ) {
				goldGrant = 30
			} else if( round > lapThreshold * 6 ) {
				goldGrant = 40
			}
			
			totalGold = totalGold + goldGrant
			
			//figure out orb rewards for the round
			if( round == lapThreshold ) {
				orbGrant = 1
			} else if( round > lapThreshold && round <= lapThreshold * 3 ) {
				if( round % 3 == 0 ) { orbGrant = 1 } else { orbGrant = 0 }
			} else if( round > lapThreshold * 3 && round <= lapThreshold * 6 ) {
				if( round % 2 == 0 ) { orbGrant = 1 } else { orbGrant = 0 }
			} else if( round > lapThreshold * 6 ) {
				orbGrant = 1
			}
			
			totalOrb = totalOrb + orbGrant
			
			awardSet.clone().each {
				it.grantCoins( goldGrant )
				if( orbGrant > 0 ) {
					it.grantQuantityItem( 100257, orbGrant )
				}
				it.say( random( woohooList ) )				
			}
			
			myManager.schedule(2) { authorizedCrew.clone().each{ it.centerPrint( "Round Number ${(round + 1).intValue()}!" ) } }
			myManager.schedule(3) { nextRound() }
		}
	}
}

//REWARDS CHECKING (After the Game is Over)
// (Failure condition comes from triggers )

highScoreHolder = []
fountainOnePlayerHighScoreNames = []
fountainTwoPlayerHighScoreNames = []
fountainThreePlayerHighScoreNames = []
fountainFourPlayerHighScoreNames = []

highestRoundCount = 0
fountainOnePlayerHighScore = 0
fountainTwoPlayerHighScore = 0
fountainThreePlayerHighScore = 0
fountainFourPlayerHighScore = 0

def checkForHighScore() {
	//reset variables
	highestRoundCount = 0
	highScoreHolder.clear()
	
	//determine current high scores and holders
	if( numPlayers == 1 ) { highestRoundCount = fountainOnePlayerHighScore; highScoreHolder = fountainOnePlayerHighScoreNames.clone() }
	if( numPlayers == 2 ) { highestRoundCount = fountainTwoPlayerHighScore; highScoreHolder = fountainTwoPlayerHighScoreNames.clone() }
	if( numPlayers == 3 ) { highestRoundCount = fountainThreePlayerHighScore; highScoreHolder = fountainThreePlayerHighScoreNames.clone() }
	if( numPlayers == 4 ) { highestRoundCount = fountainFourPlayerHighScore; highScoreHolder = fountainFourPlayerHighScoreNames.clone() }
	
	//High Score Announcements
	if( round < highestRoundCount) {
		awardSet.each{ it.centerPrint( "The highest round reached is ${highestRoundCount.intValue()} rounds. " ) }
		awardSet.each{ it.centerPrint( "Set by: ${highScoreHolder}" ) }
		myManager.schedule(3) { checkForRewards() }
	}
	if( round == highestRoundCount) {
		awardSet.each{ it.centerPrint( "Oh! So close!" ) }
		awardSet.each{ it.centerPrint( "You tied the record round of ${highestRoundCount.intValue()}" ) }
		awardSet.each{ it.centerPrint( "Set by: ${highScoreHolder}" ) }
		myManager.schedule(3) { checkForRewards() }
	}
	if( round > highestRoundCount ) {
		if( round == 1 ) {
			awardSet.each{ it.centerPrint( "You set the current High Score of ${round.intValue()} round completed!" ) }
		} else {
			awardSet.each{ it.centerPrint( "Congratulations! You set the current High Score of ${round.intValue()} rounds completed!" ) }
			println "**** numPlayers = ${numPlayers} ****"
			if( numPlayers == 1 ) { fountainOnePlayerHighScore = round; fountainOnePlayerHighScoreNames.clear(); fountainOnePlayerHighScoreNames = authorizedCrew.clone() }
			if( numPlayers == 2 ) { fountainTwoPlayerHighScore = round; fountainTwoHighScoreNames.clear(); fountainTwoPlayerHighScoreNames = authorizedCrew.clone() }
			if( numPlayers == 3 ) { fountainThreePlayerHighScore = round; fountainThreePlayerHighScoreNames.clear(); fountainThreePlayerHighScoreNames = authorizedCrew.clone() }
			if( numPlayers == 4 ) { fountainFourPlayerHighScore = round; fountainFourPlayerHighScoreNames.clear(); fountainFourPlayerHighScoreNames = authorizedCrew.clone() }
		}

		myManager.schedule(3) {
			checkForRewards()
		}
	}
}

lapThreshold = 4

def checkForRewards() {
	println "**** checkForRewards entered ****"
	
	//Reward with Gold and Orbs and Badges
	awardSet.each{	
		it.centerPrint( "You earned a total of ${totalGold} gold!" )

		if( totalOrb > 0 ) {
			if( totalOrb == 1 ) {
				it.centerPrint( "And ${totalOrb} charge orb for playing!" )
			} else {
				it.centerPrint( "And ${totalOrb} charge orbs for playing!" )
			}
		}

		//Check for Badges (NOTE: Badges are gained independently of how many players are playing the game.)
		if( round > lapThreshold && !it.isDoneQuest( 112 ) ) {
			it.grantQuantityItem( 100257, orbGrant )
			it.centerPrint( "You also get the Fountain Dancer Badge for exceeding ${lapThreshold.intValue()} rounds!" )
			it.updateQuest( 112, "Story-VQS" ) //push completion on the Team Dancer Badge
			
		} else if( round > lapThreshold * 3 && !it.isDoneQuest( 113 ) ) {
			it.grantQuantityItem( 100257, orbGrant )
			it.centerPrint( "You also get the Triumphant Dancer Badge for exceeding ${(lapThreshold * 3).intValue()} rounds!" )
			it.updateQuest( 113, "Story-VQS" ) //push completion on the Triumphant Dancer Badge
			
		} else if( round > lapThreshold * 6 && !it.isDoneQuest( 237 ) ) {
			it.grantQuantityItem( 100257, orbGrant )
			it.centerPrint( "You also get the Superlative Dancer Badge for exceeding ${(lapThreshold * 6).intValue()} rounds!" )
			it.updateQuest( 237, "Story-VQS" ) //push completion on the Triumphant Dancer Badge
		}
	}
	myManager.schedule(3) { awardSet.each{ it.centerPrint( "Try again and beat your score!" ) }; resetGameStates() }
}

def resetGameStates() {	
	println "**** resetGameStates entered ****"
	//after checking for all possible rewards, empty the awardSet and get other lists and variables prepped for the next round also	
	awardSet.clear()
	authorizedCrew.clear()
	tonePlayback.clear()
	checkMe = 0
	alreadyStarted = false
	moveToNextCrewInQueue()
	yellowButton.off()
	violetButton.off()
	pinkButton.off()
	goldButton.off()
}

//================================================
// AQUATIC PETE (sharing the onEnter/onExit stuff 
//================================================

def AquaticPete = spawnNPC("Aquatic Pete", myRooms.BARTON_103, 1350, 530)
AquaticPete.setRotation( 135 )
AquaticPete.setDisplayName( "Aquatic Pete" )


//======================================
// RANDOM CONVERSATIONS                 
//======================================

def Random1 = AquaticPete.createConversation("Random1", true, "Z0AquaticPeteR1" )

def rand1 = [id:1]
rand1.npctext = "Man, I was gonna go to the beach, but now there are monsters everywhere! I guess I'll swim in the moat instead."
rand1.flag = "!Z0AquaticPeteR1"
rand1.result = DONE
Random1.addDialog(rand1, AquaticPete)
//--------------------
def Random2 = AquaticPete.createConversation("Random2", true, "Z0AquaticPeteR2" )

def rand2 = [id:1]
rand2.npctext = "Gold Beach is my favorite hangout. Where's that? It's up by the new Bucanneer Boardwalk, of course!"
rand2.flag = "!Z0AquaticPeteR2"
rand2.result = DONE
Random2.addDialog(rand2, AquaticPete)
//--------------------
def Random3 = AquaticPete.createConversation("Random3", true, "Z0AquaticPeteR3" )

def rand3 = [id:1]
rand3.npctext = "I know, I know, I look silly in my diving gear. But I swear that I laid my clothes around here SOMEplace!"
rand3.flag = "!Z0AquaticPeteR3"
rand3.result = DONE
Random3.addDialog(rand3, AquaticPete)
//--------------------
def Random4 = AquaticPete.createConversation("Random4", true, "Z0AquaticPeteR4" )

def rand4 = [id:1]
rand4.npctext = "So if I promise to scrape the barnacles off their ship, do you think the Pirates of the Shallow Sea would take me onto their crew?"
rand4.flag = "!Z0AquaticPeteR4"
rand4.result = DONE
Random4.addDialog(rand4, AquaticPete)

//======================================
// LOGIC FLOW FOR RANDOM LIST           
//======================================

randomFlag = []
randomFlag << "Z0AquaticPeteR1"
randomFlag << "Z0AquaticPeteR2"
randomFlag << "Z0AquaticPeteR3"
randomFlag << "Z0AquaticPeteR4"


def selectRandomFlag( event ) {
	lastFlag = convoFlag
	convoFlag = random( randomFlag )
	if( lastFlag == convoFlag ) {
		selectRandomFlag( event )
	}
	event.player.setQuestFlag( GLOBAL, convoFlag )
}*/
