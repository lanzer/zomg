import com.gaiaonline.mmo.battle.actor.Player;
import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

Maestro = spawnNPC("The Maestro-VQS", myRooms.BARTON_1, 690, 925)
Maestro.setRotation( 45 )
Maestro.setDisplayName( "The Maestro" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Maestro.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/maestro_strip.png")
}
onQuestStep( 111, 2 ) { event -> event.player.addMiniMapQuestActorName( "The Maestro-VQS" ) }

playerList = []
fluffList = []

myManager.onEnter( myRooms.BARTON_1 ) { event ->
	//Beatbox Convo stuff
	if( isPlayer( event.actor) ) {
		convoFlag = random( brandomFlag )
		event.actor.setQuestFlag( GLOBAL, convoFlag )
		playerList << event.actor 
		event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" )

	}
	//***** BUG FIX FOR SCREWED-UP PLAYER DATA that didn't result in the badge being awarded *****
	if( isPlayer( event.actor ) && !event.actor.isDoneQuest( 262 ) && event.actor.hasQuestFlag( GLOBAL, "Z01MarysLamb") && event.actor.hasQuestFlag( GLOBAL, "Z01Camptown") && event.actor.hasQuestFlag( GLOBAL, "Z01Saints") && event.actor.hasQuestFlag( GLOBAL, "Z01Twinkle")  && event.actor.hasQuestFlag( GLOBAL, "Z01Funeral") && event.actor.hasQuestFlag( GLOBAL, "Z01Beethoven5") && event.actor.hasQuestFlag( GLOBAL, "Z01Worms") ) {
		event.actor.updateQuest( 262, "Story-VQS" )
		event.actor.centerPrint( "You've completed all the songs for the Maestro and are now awarded a new badge!" )
	}
}

myManager.onExit( myRooms.BARTON_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, convoFlag )
		playerList.remove( event.actor )
	}
}


randomConvo = [ "Random1", "Random2", "Random3", "Random4", "Random5", "Random6", "Random7", "Random8", "Random9", "Random10", "Random11", "Random12", "Random13" ]

//--------------------------------------
//DEFAULT CONVERSATION (when no quest)  
//--------------------------------------

def DefaultConvo = Maestro.createConversation("DefaultConvo", true, "!QuestStarted_31:2", "!QuestStarted_111")

def Default1 = [id:1]
Default1.npctext = "Hello, lover of melodies and harmonies, to the one and only Music Box! What'll it be? Would you like to hear about the Music Box? Or would you rather hear a really bad music pun?"
Default1.options = []
Default1.options << [text:"What is the Music Box?", result: 2]
Default1.options << [text:"Lay the puns on me, Maestro. I need cheering up, and I need it right now!", result: 4]
Default1.options << [text:"Thanks, but I've got to run. See ya.", result: 5]
DefaultConvo.addDialog(Default1, Maestro)

def Default2 = [id:2]
Default2.npctext = "Ah, the Music Box. I run a special service here, collecting snippets of tunes and songs that people have heard in the world and assembling them into original scores."
Default2.playertext = "That's really impressive! When I hear snippets of song, I'll bring them to you and see what you can fashion!"
Default2.result = 3
DefaultConvo.addDialog(Default2, Maestro)

def Default3 = [id:3]
Default3.npctext = "Would you like to take out a contract to find music notes for me?"
Default3.options = []
Default3.options << [text:"Sure! But how do I do that?", result: 6]
Default3.options << [text:"Thanks, but I'd rather hear some of those puns you mentioned earlier instead.", result: 4]
Default3.options << [text:"Nah, I'm going to take off now. See you later!", result: 5]
Default3.result = DONE
DefaultConvo.addDialog(Default3, Maestro)

def Default4 = [id:4]
Default4.npctext = "Okay! You asked for it!"
Default4.exec = { event ->
	//println "**** entering the exec ****"
	event.player.setQuestFlag( GLOBAL, "Z1MaestroTalk" )
	//println "**** global flag is set ****"
	maestroSaying = random( randomConvo ) //choose a random conversation
	//println "**** maestroSaying = ${maestroSaying} ****"
	Maestro.pushDialog( event.player, maestroSaying ) //push the randomly-chosen dialog
	if( event.player.hasQuestFlag( GLOBAL, "Z01MaestroTaskAuthorized" ) && !event.player.isOnQuest( 111 ) ) {
		//println "**** adding the task now ****"
		event.player.updateQuest( 111, "The Maestro-VQS" ) //start the RECOMPOSING task
	}
}
Default4.result = DONE
DefaultConvo.addDialog(Default4, Maestro)

def Default5 = [id:5]
Default5.npctext = "Okay. Stay sharp!"
Default5.playertext = "...groan..."
Default5.quest = 111 //start the RECOMPOSING task
Default5.result = DONE
DefaultConvo.addDialog(Default5, Maestro)

def Default6 = [id:6]
Default6.npctext = "Just keep your eye peeled while you're out in the world. Some of the Animated seem musically-oriented. If you find bits of song or notes, bring them back to me and I'll string them together for you."
Default6.playertext = "Okay! Sort of an open-ended contract then. I'll come back after I find enough notes to give you some ideas."
Default6.flag = "Z01MaestroTaskAuthorized"
Default6.result = 7
DefaultConvo.addDialog( Default6, Maestro )

def Default7 = [id:7]
Default7.npctext = "That's music to my ears!"
Default7.playertext = "Oh yeah. Music puns. How could I forget?"
Default7.options = []
Default7.options << [text: "Why don't you rattle off one of those puns for me?", result: 4]
Default7.options << [text: "Thanks! I'll see you later, Maestro!", result: 5]
DefaultConvo.addDialog( Default7, Maestro )

//--------------------------------------
//RE-COMPOSING (Interim)                
//--------------------------------------

def ReComposingInterim = Maestro.createConversation("ReComposingInterim", true, "!QuestStarted_31:2", "QuestStarted_111", "!QuestStarted_111:3")

def composeInt1 = [id:1]
composeInt1.npctext = "How goes the hunt for the music of the world, my friend?"
composeInt1.playertext = "I'm still looking for the notes. Any ideas on where the notes might be found?"
composeInt1.result = 2
ReComposingInterim.addDialog( composeInt1, Maestro )

def composeInt2 = [id:2]
composeInt2.npctext = "No. Not really. I mean, I assume that Animated that look like they might have something to do with music or musical instruments would be the likeliest candidates, but I'm relying on you to find out, honestly."
composeInt2.playertext = "Okay, fair enough."
composeInt2.options = []
composeInt2.options << [text: "Hey, the hunt has been long. How about one or two of those puns you like to lay on people?", result: 3]
composeInt2.options << [text: "I'll see you later, Maestro.", result: 4]
ReComposingInterim.addDialog( composeInt2, Maestro )

def composeInt3 = [id:3]
composeInt3.npctext = "Heck yeah! Here it goes!"
composeInt3.exec = {event ->
	event.player.setQuestFlag( GLOBAL, "Z1MaestroTalk" )
	maestroSaying = random( randomConvo ) //choose a random conversation
	Maestro.pushDialog( event.player, maestroSaying ) //push the randomly-chosen dialog
}
composeInt3.result = DONE
ReComposingInterim.addDialog( composeInt3, Maestro )

def composeInt4 = [id:4]
composeInt4.npctext = "Okay, %p! Good luck on the hunt!"
composeInt4.result = DONE
ReComposingInterim.addDialog( composeInt4, Maestro )

//--------------------------------------
//RE-COMPOSING (Success)                
//--------------------------------------

def RecomposingSuccess = Maestro.createConversation("ReComposingSuccess", true, "QuestStarted_111:3")

def composeSucc1 = [id:1]
composeSucc1.playertext = "I found them!"
composeSucc1.result = 2
RecomposingSuccess.addDialog( composeSucc1, Maestro )

def composeSucc2 = [id:2]
composeSucc2.npctext = "The music notes? Fantastic! Let me see 'em!"
composeSucc2.playertext = "Okay, here you go!"
composeSucc2.result = DONE
composeSucc2.exec = { event ->
	runOnDeduct( event.player, 100284, 6, notehappy, notesad ) // Removes six musical notes from player's inventory
}
RecomposingSuccess.addDialog( composeSucc2, Maestro )

//Closures to check to ensure that the player actually still has five music notes at the time of the transaction
notehappy = { event ->
	event.player.setQuestFlag( GLOBAL, "Z01MusicNotesPresent" )
	Maestro.pushDialog( event.player, "happyNotes" )
}

notesad = { event ->
	event.player.setQuestFlag( GLOBAL, "Z01MusicNotesNotPresent" )
	Maestro.pushDialog( event.player, "sadNotes" )
}

//Happy about the Music Notes
def happyNotes = Maestro.createConversation( "happyNotes", true, "QuestStarted_111:3", "Z01MusicNotesPresent" )

def notes1 = [id:1]
notes1.npctext = "Hey! These combine into a song that I recognize!"
notes1.playertext = "Really? What song is that?"
notes1.result = DONE
notes1.exec = { event ->
	checkSong( event )
	event.player.removeMiniMapQuestActorName( "The Maestro-VQS" )
}
happyNotes.addDialog(notes1, Maestro)

//Sad about the Music Notes
def sadNotes = Maestro.createConversation( "sadNotes", true, "QuestStarted_111:3", "Z01MusicNotesNotPresent" )

def notesad1 = [id:1]
notesad1.npctext = "You're just having fun with the ol' Maestro, aren't you, %p? Where's the notes?"
notesad1.playertext = "Oh drat. I must have given some of them away or sold them or something."
notesad1.result = 2
sadNotes.addDialog( notesad1, Maestro )

def notesad2 = [id:2]
notesad2.npctext = "That's okay. I'm sure there are more where that came from. Just go find more and come on back!"
notesad2.playertext = "Okay. Thanks for not being upset."
notesad2.result = 3
sadNotes.addDialog( notesad2, Maestro )

def notesad3 = [id:3]
notesad3.npctext = "What's to be upset about? You're trying to bring some music back into the world!"
notesad3.playertext = "Heh. Okay, I guess so. Thanks. See you soon."
notesad3.flag = "!Z01MusicNotesNotPresent" //unset the flag so it defaults back to before the music note check occurs
notesad3.result = DONE
sadNotes.addDialog( notesad3, Maestro )

//Determine which song to reveal to the player
songList = [ "Z01MarysLamb", "Z01Camptown", "Z01Saints", "Z01Twinkle", "Z01Funeral", "Z01Beethoven5", "Z01Worms"]

def checkSong( event ) {
	song = random( songList )
	if( event.player.hasQuestFlag( GLOBAL, "Z01MarysLamb") && event.player.hasQuestFlag( GLOBAL, "Z01Camptown") && event.player.hasQuestFlag( GLOBAL, "Z01Saints") && event.player.hasQuestFlag( GLOBAL, "Z01Twinkle")  && event.player.hasQuestFlag( GLOBAL, "Z01Funeral") && event.player.hasQuestFlag( GLOBAL, "Z01Beethoven5") && event.player.hasQuestFlag( GLOBAL, "Z01Worms") ) {
		event.player.setQuestFlag( GLOBAL, "Z01GotAllSongs" )
		Maestro.pushDialog( event.player, "gotEmAll" )
	} else {
		//if the player doesn't have all songs, then push the appropriate dialog
		if( event.player.hasQuestFlag( GLOBAL, song ) ) {
			checkSong( event )
		} else {
			if( song == "Z01MarysLamb" ) {
				event.player.setQuestFlag( GLOBAL, "Z01MarysLamb" )
				event.player.setQuestFlag( GLOBAL, "Z01SongChosen" )
				myManager.schedule(1) { Maestro.pushDialog( event.player, "marysLamb" ) }
			}
			if( song == "Z01Camptown" ) {
				event.player.setQuestFlag( GLOBAL, "Z01Camptown" )
				event.player.setQuestFlag( GLOBAL, "Z01SongChosen" )
				myManager.schedule(1) { Maestro.pushDialog( event.player, "campTown" ) }
			}
			if( song == "Z01Saints" ) {
				event.player.setQuestFlag( GLOBAL, "Z01Saints" )
				event.player.setQuestFlag( GLOBAL, "Z01SongChosen" )
				myManager.schedule(1) { Maestro.pushDialog( event.player, "saints" ) }
			}
			if( song == "Z01Twinkle" ) {
				event.player.setQuestFlag( GLOBAL, "Z01Twinkle" )
				event.player.setQuestFlag( GLOBAL, "Z01SongChosen" )
				myManager.schedule(1) { Maestro.pushDialog( event.player, "twinkle" ) }
			}
			if( song == "Z01Funeral" ) {
				event.player.setQuestFlag( GLOBAL, "Z01Funeral" )
				event.player.setQuestFlag( GLOBAL, "Z01SongChosen" )
				myManager.schedule(1) { Maestro.pushDialog( event.player, "funeral" ) }
			}
			if( song == "Z01Beethoven5" ) {
				event.player.setQuestFlag( GLOBAL, "Z01Beethoven5" )
				event.player.setQuestFlag( GLOBAL, "Z01SongChosen" )
				myManager.schedule(1) { Maestro.pushDialog( event.player, "beethoven" ) }
			}
			if( song == "Z01Worms" ) {
				event.player.setQuestFlag( GLOBAL, "Z01Worms" )
				event.player.setQuestFlag( GLOBAL, "Z01SongChosen" )
				myManager.schedule(1) { Maestro.pushDialog( event.player, "worms" ) }
			}
		}
	}
}	

//Mary Had a Little Lamb
def marysLamb = Maestro.createConversation( "marysLamb", true, "QuestStarted_111:3", "Z01MarysLamb", "Z01SongChosen" )

def mary1 = [id:1]
mary1.npctext = "Yeah, that's 'Mary Had a Little Lamb'! Trivia point: Did you know those lyrics were added to a tune written originally by Mozart?"
mary1.playertext = "Seriously?"
mary1.result = 2
marysLamb.addDialog( mary1, Maestro )

def mary2 = [id:2]
mary2.npctext = "Seriously. I'll tell ya what, I'll give you 50 gold for letting me bore you with trivia and for bringing me the notes. Thanks!"
mary2.playertext = "No problem. I don't know what I would have done with them otherwise. Glad to give you something to think about."
mary2.quest = 111 //push the completion of the RE-COMPOSING quest
mary2.flag = ["!Z01MusicNotesPresent", "!Z01SongChosen", "!Z01MaestroTaskAuthorized"]
mary2.result = DONE
marysLamb.addDialog( mary2, Maestro )

//Camp Town Races
def campTown = Maestro.createConversation( "campTown", true, "QuestStarted_111:3", "Z01Camptown", "Z01SongChosen" )

def camptown1 = [id:1]
camptown1.npctext = "What is 'five miles long' and has ladies that 'sing dis song'?"
camptown1.playertext = "Ummmm...'Camptown Races'?"
camptown1.result = 2
campTown.addDialog( camptown1, Maestro )

def camptown2 = [id:2]
camptown2.npctext = "Nice! You got it on the first guess. Stephen Foster wrote that one. Well done. Here's 50 gold for figuring it out and bringing me the tunes."
camptown2.playertext = "Cool, Maestro. Thanks!"
camptown2.quest = 111 //push the completion of the RE-COMPOSING quest
camptown2.flag = ["!Z01MusicNotesPresent", "!Z01SongChosen", "!Z01MaestroTaskAuthorized"]
camptown2.result = DONE
campTown.addDialog( camptown2, Maestro )

//When the Saints Go Marching
def saints = Maestro.createConversation( "saints", true, "QuestStarted_111:3", "Z01Saints", "Z01SongChosen" )

def saintsGo1 = [id:1]
saintsGo1.npctext = "I know this one! 'The Saints Go Marching In!' This has to be the most popular funeral dirges ever."
saintsGo1.playertext = "You're kidding! A funeral dirge?"
saintsGo1.result = 2
saints.addDialog( saintsGo1, Maestro )

def saintsGo2 = [id:2]
saintsGo2.npctext = "Originally, yes. It was originally used for funerals in New Orleans, but Louis Armstrong ended up making it quite popular. Thanks for the tune! Here's 50 gold for your efforts!"
saintsGo2.playertext = "Fantastic. And thanks for the story!"
saintsGo2.quest = 111 //push the completion of the RE-COMPOSING quest
saintsGo2.flag = ["!Z01MusicNotesPresent", "!Z01SongChosen", "!Z01MaestroTaskAuthorized"]
saintsGo2.result = DONE
saints.addDialog( saintsGo2, Maestro )

//Twinkle, Twinkle Little Star
def twinkle = Maestro.createConversation( "twinkle", true, "QuestStarted_111:3", "Z01Twinkle", "Z01SongChosen" )

def twinkleTwinkle1 = [id:1]
twinkleTwinkle1.npctext = "Hey! 'Twinkle, Twinkle'! I remember when my mother used to sing that to me as a kid. Maybe that's why I grew up to be such a 'little star'!"
twinkleTwinkle1.playertext = "heh. Is that so?"
twinkleTwinkle1.result = 2
twinkle.addDialog( twinkleTwinkle1, Maestro )

def twinkleTwinkle2 = [id:2]
twinkleTwinkle2.npctext = "Yeah, that song is from back in the mid-1700s, believe it or not. Let me give you 50 gold as a reward for finding these notes. Thanks!"
twinkleTwinkle2.playertext = "No problem. Glad to give you a trip down memory lane."
twinkleTwinkle2.quest = 111 //push the completion of the RE-COMPOSING quest
twinkleTwinkle2.flag = ["!Z01MusicNotesPresent", "!Z01SongChosen", "!Z01MaestroTaskAuthorized"]
twinkleTwinkle2.result = DONE
twinkle.addDialog( twinkleTwinkle2, Maestro )

//The Funeral March
def funeral = Maestro.createConversation( "funeral", true, "QuestStarted_111:3", "Z01Funeral", "Z01SongChosen" )

def funeralMarch1 = [id:1]
funeralMarch1.npctext = "That's a somber one. Ever hear of the 'Funeral March' by Chopin?"
funeralMarch1.playertext = "I *think* so..."
funeralMarch1.result = 2
funeral.addDialog( funeralMarch1, Maestro )

def funeralMarch2 = [id:2]
funeralMarch2.npctext = "Look it up on the net then. You'll probably recognize it right away. It gets used by cartoons, movies, and tons of stuff. Thanks. Here's 50 gold for the effort of bringing me the tune!"
funeralMarch2.playertext = "Cool! Thanks, Maestro!"
funeralMarch2.quest = 111 //push the completion of the RE-COMPOSING quest
funeralMarch2.flag = ["!Z01MusicNotesPresent", "!Z01SongChosen", "!Z01MaestroTaskAuthorized"]
funeralMarch2.result = DONE
funeral.addDialog( funeralMarch2, Maestro )

//Beethoven's Fifth Symphony
def beethoven = Maestro.createConversation( "beethoven", true, "QuestStarted_111:3", "Z01Beethoven5", "Z01SongChosen" )

def beethovenFifth1 = [id:1]
beethovenFifth1.npctext = "Ah, what a classic. Beethoven, even! This is a segment of his 'Symphony No. 5 in C minor', arguably one of his most famous works ever."
beethovenFifth1.playertext = "Isn't that the one that was the 'V for Victory' theme they used in WWII?"
beethovenFifth1.result = 2
beethoven.addDialog( beethovenFifth1, Maestro )

def beethovenFifth2 = [id:2]
beethovenFifth2.npctext = "You bet it is! I've no idea why the *Animated* care about it, of course, but yes, it's a great song. Here's 50 gold for bringing the tune to me, %p."
beethovenFifth2.playertext = "Excellent. Glad to be of assistance!"
beethovenFifth2.quest = 111 //push the completion of the RE-COMPOSING quest
beethovenFifth2.flag = ["!Z01MusicNotesPresent", "!Z01SongChosen", "!Z01MaestroTaskAuthorized"]
beethovenFifth2.result = DONE
beethoven.addDialog( beethovenFifth2, Maestro )

//The Worms Crawl In (Johnny Come Marching Home)
def worms = Maestro.createConversation( "worms", true, "QuestStarted_111:3", "Z01Worms", "Z01SongChosen" )

def wormsCrawl1 = [id:1]
wormsCrawl1.npctext = "heh. This song's been around for a long time. Some folks call it the 'Hearse Song', but we sing it now as 'The Worms Crawl In, the Worms Crawl Out'."
wormsCrawl1.playertext = "The one where the 'worms play pinochle on my snout'?"
wormsCrawl1.result = 2
worms.addDialog( wormsCrawl1, Maestro )

def wormsCrawl2 = [id:2]
wormsCrawl2.npctext = "That's the one! I've always liked that song. Not sure why. I'll give you 50 gold for doing the legwork to bring this to me. Thanks, %p!"
wormsCrawl2.playertext = "No problem. I don't know what I would have done with them otherwise. Glad to give you something to think about."
wormsCrawl2.quest = 111 //push the completion of the RE-COMPOSING quest
wormsCrawl2.flag = ["!Z01MusicNotesPresent", "!Z01SongChosen", "!Z01MaestroTaskAuthorized"]
wormsCrawl2.result = DONE
worms.addDialog( wormsCrawl2, Maestro )

//Player has all songs already
def gotEmAll = Maestro.createConversation( "gotEmAll", true, "QuestStarted_111:3", "Z01GotAllSongs" )

def gotem1 = [id:1]
gotem1.npctext = "I think I've got all the songs I need right now, %p. Thanks anyway. Here's 25 gold for gathering the notes for me anyway. Maybe I'll store them up for an original composition someday."
gotem1.playertext = "That's okay, Maestro. It was fun gathering the notes for you!"
gotem1.quest = 111 //push the completion of the RE-COMPOSING quest
gotem1.flag = "!Z01MusicNotesPresent"
gotem1.exec = { event ->
	if( !event.player.isDoneQuest( 262 ) ) {
		event.player.updateQuest( 262, "Story-VQS" ) //reward the player with the Re-Composer badge if they don't have it already.
	}
}
gotem1.result = DONE
gotEmAll.addDialog( gotem1, Maestro )

//------------------------------------------
// ELLIE'S FRIENDS (The Maestro's part)     
//------------------------------------------
def DefaultConvoPartTwo = Maestro.createConversation("DefaultConvoPartTwo", true, "QuestStarted_31:2", "!Z1MaestroGoodbye" )
DefaultConvoPartTwo.setUrgent( true )

def Maestro1 = [id:1]
Maestro1.npctext = "Hi there. Welcome to the Music Box! Do you know what Mozart is doing right now?"
Maestro1.playertext = "Ummm, no...what?"
Maestro1.result = 2
DefaultConvoPartTwo.addDialog(Maestro1, Maestro)

def Maestro2 = [id:2]
Maestro2.npctext = "He's de-composing! Ha!"
Maestro2.playertext = "Ack! Puns! Ellie is actually a friend of yours?"
Maestro2.result = 3
DefaultConvoPartTwo.addDialog(Maestro2, Maestro)

def Maestro3 = [id:3]
Maestro3.npctext = "Oh ho! So you know Ellie?"
Maestro3.playertext = "Well, actually, I just met her, but she asked me to come tell you something."
Maestro3.result = 4
DefaultConvoPartTwo.addDialog(Maestro3, Maestro)

def Maestro4 = [id:4]
Maestro4.npctext = "What a coincidence. I'm supposed to be meeting her and Agatha in just a few minutes."
Maestro4.playertext = "Right. Except that's what I'm supposed to tell you...she's not coming. Something about the light being perfect right now..."
Maestro4.result = 5
DefaultConvoPartTwo.addDialog(Maestro4, Maestro)

def Maestro5 = [id:5]
Maestro5.npctext = "Of course. I should have known. She's painting. Oh well, I'll stop by later and see how it comes out."
Maestro5.playertext = "All right then. See ya."
Maestro5.exec = { event ->
	if(!event.player.hasQuestFlag(GLOBAL, "Z1AgathaGoodbye") ) {
		event.player.setQuestFlag( GLOBAL, "Z1MaestroGoodbye" )
		Maestro.pushDialog(event.player, "MaestroGoodbyeTwo")
		
	} else {
		event.player.updateQuest( 31, "The Maestro-VQS" )
		event.player.removeMiniMapQuestActorName( "The Maestro-VQS" )
		event.player.addMiniMapQuestActorName( "Ellie-VQS" )
	}
}

Maestro5.result = DONE
DefaultConvoPartTwo.addDialog(Maestro5, Maestro)

//------------------------------------------
//MAESTRO'S GOODBYE                         
//(if you have NOT met Agatha already)      
//------------------------------------------

def MaestroGoodbyeTwo = Maestro.createConversation("MaestroGoodbyeTwo", true, "Z1MaestroGoodbye", "!Z1MaestroGoodbye2" )

def Maestro10 = [id:10]
Maestro10.npctext = "Hey, just a second! Have you stopped in to see Agatha yet?"
Maestro10.playertext = "No, not yet. Why?"
Maestro10.result = 11
MaestroGoodbyeTwo.addDialog(Maestro10, Maestro)

def Maestro11 = [id:11]
Maestro11.npctext = "I wasn't sure if she was in her shop today, with all the craziness going on. I've been meaning to check up on her."
Maestro11.playertext = "Okay. I'll stop by and see her soon."
Maestro11.result = 12
MaestroGoodbyeTwo.addDialog(Maestro11, Maestro)

def Maestro12 = [id:12]
Maestro12.npctext = "Thanks! And always remember, you can tune a piano, but you can't tuna fish!"
Maestro12.playertext = "...argh. Stop. Now. Before you pun again."
Maestro12.result = 13
MaestroGoodbyeTwo.addDialog(Maestro12, Maestro)

def Maestro13 = [id:13]
Maestro13.npctext = "Party pooper."
Maestro13.flag = "Z1MaestroGoodbye2"
Maestro13.exec = { event ->
	event.player.updateQuest( 31, "The Maestro-VQS" )
	event.player.removeMiniMapQuestActorName( "The Maestro-VQS" )
}
Maestro13.result = DONE
MaestroGoodbyeTwo.addDialog(Maestro13, Maestro)

//------------------------------------------
//MAESTRO'S GOODBYE                         
//(if you HAVE met Agatha already, but the  
//quest has not yet been completed by       
//visiting Ellie again.                     
//------------------------------------------

def MaestroGoodbye = Maestro.createConversation("MaestroGoodbye", true, "QuestStarted_31:3")

def Maestro6 = [id:6]
Maestro6.npctext = "Hi again, %p! I've got another one for you!"
Maestro6.options = []
Maestro6.options << [text:"Must you?", result: 7]
Maestro6.options << [text:"Oh no! No more puns! This conversation is DONE!", result: 13]
MaestroGoodbye.addDialog(Maestro6, Maestro)

def Maestro7 = [id:7]
Maestro7.npctext = "So...a 'C', an 'E-flat', and a 'G' go into a bar..."
Maestro7.playertext = "This is going to hurt..."
Maestro7.result = 8
MaestroGoodbye.addDialog(Maestro7, Maestro)

def Maestro8 = [id:8]
Maestro8.npctext = "...and they walk up to the bar, but the bartender says..."
Maestro8.playertext = "You're really milking this, aren't you?"
Maestro8.result = 9
MaestroGoodbye.addDialog(Maestro8, Maestro)

def Maestro9 = [id:9]
Maestro9.npctext = "...he says...'I'm sorry. You'll have to leave. We don't serve Minors here!'"
Maestro9.playertext = "Oh, ouch. No wonder there's no other customers here."
Maestro9.result = DONE
MaestroGoodbye.addDialog(Maestro9, Maestro)

//======================================
// RANDOM CONVERSATIONS                 
//======================================


def Random1 = Maestro.createConversation("Random1", true, "Z1MaestroTalk" )

def rand1 = [id:1]
rand1.npctext = "I often break into song because I can't find the key!"
rand1.flag = "!Z1MaestroTalk"
rand1.result = DONE
Random1.addDialog(rand1, Maestro)
//--------------------
def Random2 = Maestro.createConversation("Random2", true, "Z1MaestroTalk" )

def rand2 = [id:1]
rand2.npctext = "You CAN tuna fish. You just need to adjust its scales!"
rand2.flag = "!Z1MaestroTalk"
rand2.result = DONE
Random2.addDialog(rand2, Maestro)
//--------------------
def Random3 = Maestro.createConversation("Random3", true, "Z1MaestroTalk" )

def rand3 = [id:1]
rand3.npctext = "Y'know, some musicians are really sharp. And that's just not natural!"
rand3.flag = "!Z1MaestroTalk"
rand3.result = DONE
Random3.addDialog(rand3, Maestro)
//--------------------
def Random4 = Maestro.createConversation("Random4", true, "Z1MaestroTalk" )

def rand4 = [id:1]
rand4.npctext = "I robbed a medieval music store once, but all I got was a lot of lute."
rand4.flag = "!Z1MaestroTalk"
rand4.result = DONE
Random4.addDialog(rand4, Maestro)
//--------------------
def Random5 = Maestro.createConversation("Random5", true, "Z1MaestroTalk" )

def rand5 = [id:1]
rand5.npctext = "If you can't find anyone to sing with, then duet yourself!"
rand5.flag = "!Z1MaestroTalk"
rand5.result = DONE
Random5.addDialog(rand5, Maestro)
//--------------------
def Random6 = Maestro.createConversation("Random6", true, "Z1MaestroTalk" )

def rand6 = [id:1]
rand6.npctext = "I buy violins! No strings attached!"
rand6.flag = "!Z1MaestroTalk"
rand6.result = DONE
Random6.addDialog(rand6, Maestro)
//--------------------
def Random7 = Maestro.createConversation("Random7", true, "Z1MaestroTalk" )

def rand7 = [id:1]
rand7.npctext = "The Easter Bunny is really into hip hop!"
rand7.flag = "!Z1MaestroTalk"
rand7.result = DONE
Random7.addDialog(rand7, Maestro)
//--------------------
def Random8 = Maestro.createConversation("Random8", true, "Z1MaestroTalk" )

def rand8 = [id:1]
rand8.npctext = "I tried to learn the violin, but I was really just fiddling around."
rand8.flag = "!Z1MaestroTalk"
rand8.result = DONE
Random8.addDialog(rand8, Maestro)
//--------------------
def Random9 = Maestro.createConversation("Random9", true, "Z1MaestroTalk" )

def rand9 = [id:1]
rand9.npctext = "I tried to play the drums, but there were too many re-percussions!"
rand9.flag = "!Z1MaestroTalk"
rand9.result = DONE
Random9.addDialog(rand9, Maestro)
//--------------------
def Random10 = Maestro.createConversation("Random10", true, "Z1MaestroTalk" )

def rand10 = [id:1]
rand10.npctext = "Did you hear about the glass drum? It can't be beat!"
rand10.flag = "!Z1MaestroTalk"
rand10.result = DONE
Random10.addDialog(rand10, Maestro)
//--------------------
def Random11 = Maestro.createConversation("Random11", true, "Z1MaestroTalk" )

def rand11 = [id:1]
rand11.npctext = "Musicians who play in bed use sheet music!"
rand11.flag = "!Z1MaestroTalk"
rand11.result = DONE
Random11.addDialog(rand11, Maestro)
//--------------------
def Random12 = Maestro.createConversation("Random12", true, "Z1MaestroTalk" )

def rand12 = [id:1]
rand12.npctext = "If you chop up an old piano, you'll get a chord of wood!"
rand12.flag = "!Z1MaestroTalk"
rand12.result = DONE
Random12.addDialog(rand12, Maestro)
//--------------------
def Random13 = Maestro.createConversation("Random13", true, "Z1MaestroTalk" )

def rand13 = [id:1]
rand13.npctext = "A novel about the murder of a musician was a real Clef-hanger!"
rand13.flag = "!Z1MaestroTalk"
rand13.result = DONE
Random13.addDialog(rand13, Maestro)


//======================================
// LOGIC FLOW FOR TRIGGER ZONE AND      
// RANDOM LIST                          
//======================================

//=============================================================================
//BEATBOX CONVOS
//=============================================================================

def beatBox = spawnNPC("Beatbox-VQS", myRooms.BARTON_1, 830, 665)
beatBox.setRotation( 10 )
beatBox.setDisplayName( "Beat Box" )

//HALLOWEEN COSTUME
//beatBox.setURL("http://test10cdn.gaiaonline.com/images/zomg/halloween2k8/beatbox_strip.png")

//======================================
// RANDOM CONVERSATIONS                 
//======================================

def BRandom1 = beatBox.createConversation("BRandom1", true, "Z0BeatBoxR1" )
BRandom1.setUrgent( false )

def brand1 = [id:1]
brand1.npctext = "ba-ba-pa-pow. tick, tick, tick, ba-pisssssh!"
brand1.flag = "!Z0BeatBoxR1"
brand1.exec = { event ->
	selectBRandomFlag( event )
}
brand1.result = DONE
BRandom1.addDialog(brand1, beatBox)
//--------------------
def BRandom2 = beatBox.createConversation("BRandom2", true, "Z0BeatBoxR2" )
BRandom2.setUrgent( false )

def brand2 = [id:1]
brand2.npctext = "tick, ba-pish, ba-pish, tick, tick, scritcha, scritcha, ka-pah!"
brand2.flag = "!Z0BeatBoxR2"
brand2.exec = { event ->
	selectBRandomFlag( event )
}
brand2.result = DONE
BRandom2.addDialog(brand2, beatBox)
//--------------------
def BRandom3 = beatBox.createConversation("BRandom3", true, "Z0BeatBoxR3" )
BRandom3.setUrgent( false )

def brand3 = [id:1]
brand3.npctext = "tick, tick, tick, pa-kow!"
brand3.flag = "!Z0BeatBoxR3"
brand3.exec = { event ->
	selectBRandomFlag( event )
}
brand3.result = DONE
BRandom3.addDialog(brand3, beatBox)
//--------------------
def BRandom4 = beatBox.createConversation("BRandom4", true, "Z0BeatBoxR4" )
BRandom4.setUrgent( false )

def brand4 = [id:1]
brand4.npctext = "ba-pssh-tick...psshhhh! Hi there! pa-KOW! pish, pish, pish...tick."
brand4.flag = "!Z0BeatBoxR4"
brand4.exec = { event ->
	selectBRandomFlag( event )
}
brand4.result = DONE
BRandom4.addDialog(brand4, beatBox)

//======================================
// LOGIC FLOW FOR RANDOM LIST           
//======================================

brandomFlag = []
brandomFlag << "Z0BeatBoxR1"
brandomFlag << "Z0BeatBoxR2"
brandomFlag << "Z0BeatBoxR3"
brandomFlag << "Z0BeatBoxR4"


def selectBRandomFlag( event ) {
	lastFlag = convoFlag
	convoFlag = random( brandomFlag )
	if( lastFlag == convoFlag ) {
		selectBRandomFlag( event )
	}
	event.player.setQuestFlag( GLOBAL, convoFlag )
}

if (eventStart != 'HALLOWEEN') return


//======================================
// HALLOWEEN TRICK OR TREATING SCRIPT   
//======================================

//Pumpkin Fluff Spawner
pumpFluff1 = myRooms.BARTON_1.spawnSpawner( "pumpFluff1", "pumpkin_fluff", 6 )
pumpFluff1.setPos( 1015, 610 )
pumpFluff1.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
pumpFluff1.setMonsterLevelForChildren( 1.0 )

pumpFluff1.stopSpawning()

//=========================================
// JACK-O-LANTERN SWITCH                   
//=========================================

//TODO : Set things up so that trick or treating only occurs at night (when the pumpkins light up)
// GST test : Turns ON pumpkins at night, turns them OFF during day.
// Pumpkins are locked during the day.
// Pumpkins turn off when activated at night, but immediately turn back ON for the next player. (Light flickers when used.)

jack1 = makeSwitch( "jack1", myRooms.BARTON_1, 930, 660 )
jack1.setRange( 200 )

def startTrickOrTreating = { event ->
	jack1.on()
	if( !event.actor.hasQuestFlag( GLOBAL, "TrickOrTreatActive" ) ) {
		//set a preventative flag that expires in 5 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "TrickOrTreatActive" ) 
		myManager.schedule(5) { event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" ) }
		//now push the trick or treat dialog
		event.actor.setQuestFlag( GLOBAL, "Z01TrickOrTreat1A" )
		beatBox.pushDialog( event.actor, "trickOrTreat" )
	}
}
	
jack1.whenOff( startTrickOrTreating )

//Variable Initialization & Master Timer
fluffsAlreadySpawned = false
hourCounter = System.currentTimeMillis()
trickTreatTimer()
jackOLanternTimer()

//Every hour, update the hourCounter variable
def trickTreatTimer() {
	myManager.schedule(4200){ hourCounter = System.currentTimeMillis(); trickTreatTimer() }
	
}

//Turn on and unlock Jack O'Lanterns at night. Turn them off during day and lock them.
def jackOLanternTimer() {
//	println "**** GST = ${gst()} ****"
	if( ( gst() > 1800 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 600 ) ) { //trick or treating allowed between 6pm and 6am.
		jack1.unlock()
		jack1.on()
	} else {
		jack1.lock()
		jack1.off()
	}
	myManager.schedule(120) { jackOLanternTimer() } //check time of day again in five minutes
}
	
//=========================================
// DIALOG AND SCRIPT LOGIC                 
//=========================================

//determine how to respond to the player
def trickOrTreat = beatBox.createConversation( "trickOrTreat", true, "Z01TrickOrTreat1A" )
trickOrTreat.setUrgent( false )

TT1 = [id:1]
TT1.npctext = "Happy Halloween!"
TT1.flag = "!Z01TrickOrTreat1A"
TT1.exec = { event ->
	if( event.player.getPlayerVar( "Z01VisitedbeatBox" ).longValue() == null || event.player.getPlayerVar( "Z01VisitedbeatBox" ).longValue() < hourCounter ) {
		def awardList = playerList.clone().intersect( event.player.getCrew() )
		awardList.each{
			//don't allow crewmembers that already got treats from this pumpkin to get them again just because they're in a different crew.
			if( it.getPlayerVar( "Z01VisitedbeatBox" ).longValue() >= hourCounter ) {
				it.setQuestFlag( GLOBAL, "Z01NotYet" )
				beatBox.pushDialog( it, "notYet" )
			}
		} 
		event.player.setQuestFlag( GLOBAL, "Z01TrickOrTreat1B" )
		beatBox.pushDialog( event.player, "trickOrTreat2" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z01NotYet" )
		beatBox.pushDialog( event.player, "notYet" )
	}
}
trickOrTreat.addDialog( TT1, beatBox )

//If the player has received a treat too recently then don't give him a treat
def notYet = beatBox.createConversation( "notYet", true, "Z01NotYet" )
notYet.setUrgent( false )

nYet1 = [id:1]
nYet1.npctext = "Wait a sec! I talked to you already tonight! Try again tomorrow night."
nYet1.flag = "!Z01NotYet"
nYet1.result = DONE
notYet.addDialog( nYet1, beatBox )

//if the player qualifies for a treat or trick, then get this conversation instead
def trickOrTreat2 = beatBox.createConversation( "trickOrTreat2", true, "Z01TrickOrTreat1B" )
trickOrTreat2.setUrgent( false )

TT2 = [id:1]
TT2.playertext = "Trick or Treat?"
TT2.flag = "!Z01TrickOrTreat1B"
TT2.result = DONE
TT2.exec = { event ->
	def awardList = playerList.clone().intersect( event.player.getCrew() )
	awardList.clone().each{ if( it.getPlayerVar( "Z01VisitedbeatBox" ).longValue() >= hourCounter ) { awardList.remove( it ) } }
	awardList.each{
		//reset the playerVar hourCounter in case people try to exploit by joining the Crew after the first conversation dialog.
		it.setPlayerVar( "Z01VisitedbeatBox", hourCounter )

		//increment the number of pumpkins clicked counter
		it.addToPlayerVar( "Z01JackOLanternsClicked", 1 )

		//check for Badge Updates
		if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 50 && it.getPlayerVar( "Z01JackOLanternsClicked" ) < 200 && !it.isDoneQuest(276) ) {
			it.updateQuest( 276, "Story-VQS" ) //Update the "Jack O'Smasher" badge
		} else if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 200 && !it.isDoneQuest(277) ) {
			it.updateQuest( 277, "Story-VQS" ) //Update the "Pumpkin King" badge
		}

		//determine if it's a trick or treat.
		roll = random( 100 )
		def player = it
		if( roll <= 40 ) { //This value is the chance of a trick. Remainder is the treat percentage.
			player.centerPrint( "You get TRICKED!" )
			trick( player, awardList )
		} else {
			player.centerPrint( "You get a TREAT!" )
			treat( player, awardList )
		}
	}
}
trickOrTreat2.addDialog( TT2, beatBox )

//=========================================
// TRICKS!!!                               
//=========================================
def synchronized trick( player, awardList ) {
	roll =  random( 100 )
	if( fluffsAlreadySpawned == true && roll <= 45 ) {
		roll = random( 46, 100 )
	}
	if( roll <= 45 ) {
		//pumpkin fluff spawn
		player.centerPrint( "From out of the pumpkin come pumpkin-costumed fluffs to attack you!" )
		fluffsAlreadySpawned = true
		fluffSpawnNum = awardList.size()
		spawnPumpkinFluffs()
	} else if( roll > 45 && roll <= 90 ) {
		//give the player a dentist gift
		dentistGift = random( 100 )
		if( dentistGift <= 50 ) {
			dentistGift = "100479"
			player.centerPrint( "Oh man, are you kidding? Dental Floss?!? Who gives dental floss instead of candy?" )
		} else { 
			dentistGift = "100481"
			player.centerPrint( "No way! A toothbrush? WTF?!? It's Halloween, people!" )
		}
		player.grantItem( dentistGift )
	} else if( roll > 90 ) {
		//warp the player
		player.centerPrint( "You feel a strange energy pulling you away to...somewhere else!" )
		roll = random( 100 )
		if( roll <= 50 ) {
			//somewhere in Barton (one of three places)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "BARTON_301", 580, 200 ) //behind statue
			} else if( roll == 2 ) {
				player.warp( "BARTON_4", 1400, 700 ) //near Dusty in corner
			} else if( roll == 3 ) {
				player.warp( "BARTON_1", 1360, 290 ) //in trees above Maestro
			} 
		} else if( roll > 50 && roll <= 80 ) {
			//somewhere in Village Greens (three places, one is nasty)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "VILLAGE_3", 310, 380 ) //in quiet corner at top of map
			} else if( roll == 2 ) {
				player.warp( "VILLAGE_201", 155, 375 ) //Way over by the goof marker at top of west side of map
			} else if( roll == 3 ) {
				player.warp( "VILLAGE_1004", 900, 590 ) //Down by the flamingo spiral at the bottom, right of the map. DANGER!!!
			} 
		} else if( roll > 80 && roll <= 95 ) {
			//danger spots in other zones near Crystals (pull a random one from a map with coords for warping)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "ZENGARDEN_502", 500, 625 ) //in Katsumi's shrine
			} else if( roll == 2 ) {
				player.warp( "BASSKEN_403", 350, 330 ) //By Logan
			} else if( roll == 3 ) {
				player.warp( "Aqueduct_602", 1330, 530 ) //Up by the True Believers
			} 
		} else if( roll > 95 ) {
			player.warp( "Hive_1", 735, 530 ) //YIKES! THE HIVE!!
		}
	}
}

def spawnPumpkinFluffs() {
	if( fluffSpawnNum > 0 ) {
		fluffList << pumpFluff1.forceSpawnNow() //This list starts at position 0, so subtract one from the list size to get the correct list position
		fluffList.get( fluffList.size() - 1 ).addHate( random ( playerList ), 1 ) 
		runOnDeath( fluffList.get( fluffList.size() - 1 ) ) { event -> fluffList.remove( fluffList.get( fluffList.size() - 1 ) ); checkForLoot( event ) }
		fluffSpawnNum -- 
		myManager.schedule(1) { spawnPumpkinFluffs() } 
	} else {
		myManager.schedule(30) { disposalTimer() }
	}
}

def synchronized checkForLoot( event ) {
	def awardList = playerList.clone().intersect( event.killer.getCrew() )
	awardList.each {
		roll = random( 100 )
		if( !it.hasQuestFlag( GLOBAL, "Z01GotOhMyGumball10" ) ) {
			if( roll <= 25 ) { //trick is 50% and pumpkin fluffs are 50% of that and this is 20% of that, so 2.5% chance of getting the item for each fluff killed.
				it.centerPrint( "Happy Halloween! You found the 'Oh My Gumball'!" )
				it.grantItem( "28768" ) //TODO: Fill in with the correct item ID
				it.setQuestFlag( GLOBAL, "Z01GotOhMyGumball10" )
			}
		} else { //if player has already received the 'Oh My Gumball' item
			if( roll <= 25 ) { //only tell them about the 'Oh My Gumball' if they would have normally received it
				it.centerPrint( "You've already found the 'Oh My Gumball', so all you find this time is pumpkin guts." )
			}
		}			
	}
}

def disposalTimer() {
	if( !fluffList.isEmpty() ) {
		fluffList.each{ it.dispose() }
	}
	fluffsAlreadySpawned = false
}

//=========================================
// TREATS!!!                               
//=========================================

commonList = [ 100272, 100289, 100385, 100297, 100289, 100397, 100291, 100262, 100263, 100298, 100388, 100278, 100275, 100283, 100281, 100367, 100411, 100394, 100284, 100405, 100267, 100265, 100285, 100398, 100397, 100376, 100296 ]

uncommonList = [ 100280, 100279, 100270, 100380, 100279, 100384, 100378, 100261, 100271, 100276, 100258, 100268, 100381, 100382, 100282, 100383, 100390, 100299, 100286, 100369, 100400, 100387 ]

recipeList = [ "17766", "17764", "17772", "17758", "17756", "17861", "17857", "17755", "17848", "17849", "17852", "17851", "17850", "17753", "17833", "17831", "17754", "17836", "17835", "17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779" ]

ringList = [ "17716", "17722", "17723", "17737", "17738", "17741", "17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "20253" ]

def synchronized treat( player, awardList ) {
	//scale the returned results by relative con levels for each Crew member
	roll = random( 100 )

	lootMultiplier = player.getConLevel() / 10 //the lower the overall CL of a player in relation to player max CL (currently 10), the lower the reward so the trick or treating can be meaningful to all CLs
	//grant gold
	if( roll <= 50 ) {
		goldGrant = ( random( 100000, 300000 ) * lootMultiplier ).intValue()
		player.grantCoins( goldGrant ) 
		player.centerPrint( "It's raining gold!" )
	//grant common item
	} else if( roll > 50 && roll <= 65 ) {
		player.grantQuantityItem( random( commonList ), random(2,5) )
		player.centerPrint( "All right! A loot item!" )
	//grant uncommon item
	} else if( roll > 65 && roll <= 75 ) {
		player.grantQuantityItem( random( uncommonList ), random(1,3) )
		player.centerPrint( "An uncommon loot item!" )
	//grant recipes
	} else if( roll > 75 && roll <= 85 ) {
		player.grantItem( random( recipeList ) )
		player.centerPrint( "Awesome! A recipe!" )
	//grant orbs
	} else if( roll > 85 && roll <= 95 ) {
		orbGrant = ( 10 * lootMultiplier ).intValue()
		player.grantQuantityItem( 100257, orbGrant )
		player.centerPrint( "Look! Charge Orbs!!" )
	//rings
	} else if( roll > 95 ) {
		player.grantRing( random( ringList ), true )
		player.centerPrint( "Woohoo! A Ring!" )
	}
}
