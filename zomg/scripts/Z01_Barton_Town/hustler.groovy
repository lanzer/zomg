import com.gaiaonline.mmo.battle.script.*;


Hustler = spawnNPC("The Penultimate Hustler", myRooms.BARTON_103, 1075, 400)

Hustler.setWanderBehavior( 50, 150, 3, 7, 250 )
Hustler.startWander(true)
Hustler.setDisplayName( "The Penultimate Hustler" )

//STOP MOVING WHEN TALKING
def stopMoving(event) {
	Hustler.pause()
}

//START MOVING AFTER DONE TALKING
def startMoving(event) {
	Hustler.pauseResume()
}

//Default speech loop
def HustlerConvo = Hustler.createConversation("HustlerConvo", true )

def Default1 = [id:1]
Default1.npctext = "Heyyyy there!"
Default1.playertext = "Ummm...hi?"
Default1.exec = { event ->
	stopMoving()
}
Default1.result = 2
HustlerConvo.addDialog(Default1, Hustler)

def Default2 = [id:2]
Default2.npctext = "How YOU doin'?"
Default2.options = []
Default2.options << [text:"Fired up and rarin' to go!", result: 3]
Default2.options << [text:"Sweet! How YOU doin'?", result: 4]
Default2.options << [text:"Just fine, and moving on. See ya!", result: DONE, exec:{ event -> startMoving() } ]
HustlerConvo.addDialog(Default2, Hustler)

def Default3 = [id:3]
Default3.npctext = "Then you GO!"
Default3.playertext = "Uh...thanks. I will then."
Default3.exec = { event ->
	startMoving()
}
Default3.result = DONE
HustlerConvo.addDialog(Default3, Hustler)

def Default4 = [id:4]
Default4.npctext = "Dude! I am presto-fabulous-o! I'm smooth movin' like chocolate over nougat, like silk on a baby's butt, like teflon on a new pan, like..."
Default4.playertext = "I get it. I get it. You're smooooth."
Default4.result = 5
HustlerConvo.addDialog(Default4, Hustler)

def Default5 = [id:5]
Default5.npctext = "Oh yeah. You know it. Smoothilicious. That's me."
Default5.playertext = "I'm just going to smoothly slide out of here right now. See ya, Hustler."
Default5.result = 6
HustlerConvo.addDialog(Default5, Hustler)

def Default6 = [id:6]
Default6.npctext = "See ya! Be cool!"
Default6.exec = { event ->
	startMoving()
}
Default6.result = DONE
HustlerConvo.addDialog(Default6, Hustler)