//Script created by gfern

import com.gaiaonline.mmo.battle.actor.Player;
import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

rufus = spawnNPC("Rufus-VQS", myRooms.BARTON_104, 80, 865)
rufus.setDisplayName( "Rufus" )
rufus.setRotation(90)

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	rufus.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/rufus_strip.png")
}
//Trigger zone definition
rufusDiary = "rufusDiary"
myRooms.BARTON_2.createTriggerZone(rufusDiary, 475, 215, 1040, 340)
myManager.onTriggerIn(myRooms.BARTON_2, rufusDiary) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(37, 2)) {	
		event.actor.updateQuest( 37, "Rufus-VQS" )
		event.actor.centerPrint("You have located Rufus the Cat's diary!")
	}
}

onQuestStep(37, 2) { event -> event.player.addMiniMapQuestActorName("Rufus-VQS") }


//--------------------------------------------------------------------------------------------------
// Convo From TrainStation                                                                          
//--------------------------------------------------------------------------------------------------
def rufusFromTraining = rufus.createConversation( "rufusFromTraining", true, "Z22GoTalkToRufus" )
rufusFromTraining.setUrgent( true )

def train1 = [id:1]
train1.playertext = "So, you must be Rufus the cat."
train1.result = 2
rufusFromTraining.addDialog( train1, rufus )

def train2 = [id:2]
train2.npctext = "We usually drop 'the cat' from my name. I mean, it's rather obvious, don't you think?"
train2.playertext = "Ummm, yes, I guess so. Are you *running* this shop?"
train2.result = 3
rufusFromTraining.addDialog( train2, rufus )

def train3 = [id:3]
train3.npctext = "Not anymore! Ian runs it again and I'm super-happy-dancey about that."
train3.playertext = "hehehe. Okay. Well, I'm here to tell you that Barry sent me. He wanted you to know the sewers below town are infested with a weird kind of hamster and that it might be happy hunting for you."
train3.result = 4
rufusFromTraining.addDialog( train3, rufus )

def train4 = [id:4]
train4.npctext = "Well, the hamsters sound yummy, but I don't know about hunting in the sewers. Things I find there always leave such a nasty aftertaste in my mouth. But, we'll see."
train4.playertext = "Okay. But, seriously, did you *really* run this shop?"
train4.result = 5
rufusFromTraining.addDialog( train4, rufus )

def train5 = [id:5]
train5.npctext = "Yes, I did! Would you like to hear the official snappy patter I used all the time?"
train5.playertext = "Sure! Lay it on me!"
train5.result = DONE
train5.flag = "!Z22GoTalkToRufus"
train5.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Rufus-VQS" )
	if(!event.player.isOnQuest(39)) { rufus.pushDialog( event.player, "rufusConversationA" ) }
	if(event.player.isOnQuest(39)) { rufus.pushDialog(event.player, "rufusIntroPants")}
}
rufusFromTraining.addDialog( train5, rufus )

//--------------------------------------------------------------------------------------------------
//This conversation is given the first time a player speaks to Rufus                                
//--------------------------------------------------------------------------------------------------
def rufusConversationA = rufus.createConversation("rufusConversationA", true, "!QuestCompleted_37", "!Z22GoTalkToRufus", "!Z1_Rufus_TeasingRufus_Teased", "!Z1_Rufus_TeasingRufus_AngryRufus", "!Z1_Rufus_TeasingRufus_NotTeased", "!QuestStarted_39")
rufusConversationA.setUrgent( true )

def dialogA1 = [id:1]
dialogA1.npctext = "Welcome to Barton Boutique! My name is Rufus."
dialogA1.options = []
dialogA1.options << [text:"Nice to meet you, Rufus. What's going on?", result:2]
dialogA1.options << [text:"What's with the clothes? Don't you know you're just a cat?", result:3]
dialogA1.options << [text:"Tell me about Barton Boutique.", result:4]
rufusConversationA.addDialog(dialogA1, rufus)

def dialogA2 = [id:2]
dialogA2.npctext = "Oh, I'm just waiting for a friend. He should have been here a while ago. I wonder where he is."
dialogA2.options = []
dialogA2.options << [text:"I could look for him if you like.", result:6]
dialogA2.options << [text:"Tell me about Barton Boutique", result:4]
rufusConversationA.addDialog(dialogA2, rufus)

def dialogA3 = [id:3]
dialogA3.npctext = "Just a cat?! How many cats do you know that can talk? Or own a store? Not very many, I'd wager."
dialogA3.options = []
dialogA3.options << [text:"That's a good point, but you still look a little silly with a coat, hat, and no pants", flag: "Z1_Rufus_TeasingRufus_Teased", result:5]
dialogA3.options << [text:"Sorry Rufus, tell me about Barton Boutique.", result:4]
rufusConversationA.addDialog(dialogA3, rufus)

def dialogA4 = [id:4]
dialogA4.npctext = "At Barton Boutique you can buy all sorts of clothing, but we specialize in animal accessories. Ever wanted a tail? Paws? Wings? If so, you've come to the right place."
dialogA4.options = []
dialogA4.options << [text:"I'll keep that in mind. So, what's going on with you?", result: 8]
dialogA4.options << [text:"And here I thought you were just a cat.", result:3]
dialogA4.options << [text:"Thanks, Rufus. Goodbye, for now.", flag: "Z1_Rufus_TeasingRufus_NotTeased", result: DONE]
rufusConversationA.addDialog(dialogA4, rufus)

def dialogA5 = [id:5]
dialogA5.npctext = "What? Should I wear them backwards with my tail through the zipper? Now that would be truly ridiculous."
dialogA5.playertext = "Good point. That would look terrible!"
dialogA5.result = 7
rufusConversationA.addDialog(dialogA5, rufus)

def dialogA6 = [id:6]
dialogA6.npctext = "Hmmm, no, that's okay. I'm sure he'll be along soon."
dialogA6.options = []
dialogA6.options << [text:"So, why are you wearing clothes? You do know you're just a cat, right?", result:3]
dialogA6.options << [text:"Tell me about Barton Boutique.", result:4]
rufusConversationA.addDialog(dialogA6, rufus)

def dialogA7 = [id:7, npctext: "Besides, the pants that came with this suit didn't have a zipper anyway. Now, I'm tired of being insulted. Come back when you've learned some manners.", result: DONE]
rufusConversationA.addDialog(dialogA7, rufus)

def dialogA8 = [id:8]
dialogA8.npctext = "I can't find my diary! I'm so worried that someone will find it and read it, but I have no idea where it is and I can't leave the boutique to look for it."
dialogA8.playertext = "Where did you last see it? Just retrace your steps and I'm sure you'll remember where you left it."
dialogA8.result = 9
rufusConversationA.addDialog(dialogA8, rufus)

def dialogA9 = [id:9]
dialogA9.npctext = "I was writing in it behind the well... it must be somewhere between here and there. Do you think you could look? I trust you not to read it."
dialogA9.options = []
dialogA9.options << [text:"Sure, Rufus. I'll go look immediately.", result: 10]
dialogA9.options << [text:"Not yet, Rufus. I'm busy.", flag: "Z1_Rufus_TeasingRufus_NotTeased", result: DONE]
rufusConversationA.addDialog(dialogA9, rufus)

def dialogA10 = [id:10]
dialogA10.npctext = "Thanks!"
dialogA10.flag = "Z1_Rufus_TeasingRufus_NotTeased"
dialogA10.quest = 37
dialogA10.result = DONE
rufusConversationA.addDialog(dialogA10, rufus)

//--------------------------------------------------------------------------------------------------
//This conversation is given the first time a player speaks to Rufus                                
//--------------------------------------------------------------------------------------------------
def rufusIntroPants = rufus.createConversation("rufusIntroPants", true, "!QuestCompleted_37", "!Z22GoTalkToRufus", "!Z1_Rufus_TeasingRufus_Teased", "!Z1_Rufus_TeasingRufus_AngryRufus", "!Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39")

def rufusIntroPants1 = [id:1]
rufusIntroPants1.npctext = "Welcome to Barton Boutique! My name is Rufus."
rufusIntroPants1.options = []
rufusIntroPants1.options << [text:"Nice to meet you, Rufus. What's going on?", result:2]
rufusIntroPants1.options << [text:"What's with the clothes? Don't you know you're just a cat?", result:3]
rufusIntroPants1.options << [text:"Tell me about Barton Boutique.", result:4]
rufusIntroPants.addDialog(rufusIntroPants1, rufus)

def rufusIntroPants2 = [id:2]
rufusIntroPants2.npctext = "Oh, I'm just waiting for a friend. He should have been here a while ago. I wonder where he is."
rufusIntroPants2.options = []
rufusIntroPants2.options << [text:"I could look for him if you like.", result:6]
rufusIntroPants2.options << [text:"Tell me about Barton Boutique", result:4]
rufusIntroPants.addDialog(rufusIntroPants2, rufus)

def rufusIntroPants3 = [id:3]
rufusIntroPants3.npctext = "Just a cat?! How many cats do you know that can talk? Or own a store? Not very many, I'd wager."
rufusIntroPants3.options = []
rufusIntroPants3.options << [text:"That's a good point, but you still look a little silly with a coat, hat, and no pants", flag: "Z1_Rufus_TeasingRufus_Teased", result:5]
rufusIntroPants3.options << [text:"Sorry Rufus, tell me about Barton Boutique.", result:4]
rufusIntroPants.addDialog(rufusIntroPants3, rufus)

def rufusIntroPants4 = [id:4]
rufusIntroPants4.npctext = "At Barton Boutique you can buy all sorts of clothing, but we specialize in animal accessories. Ever wanted a tail? Paws? Wings? If so, you've come to the right place."
rufusIntroPants4.options = []
rufusIntroPants4.options << [text:"I'll keep that in mind. So, I have to ask. No pants?", result: 11]
rufusIntroPants4.options << [text:"And here I thought you were just a cat.", result:3]
rufusIntroPants4.options << [text:"Thanks, Rufus. Goodbye, for now.", flag: "Z1_Rufus_TeasingRufus_NotTeased", result: DONE]
rufusIntroPants.addDialog(rufusIntroPants4, rufus)

def rufusIntroPants5 = [id:5]
rufusIntroPants5.npctext = "What? Should I wear them backwards with my tail through the zipper? Now that would be truly ridiculous."
rufusIntroPants5.playertext = "Good point. That would look terrible!"
rufusIntroPants5.result = 7
rufusIntroPants.addDialog(rufusIntroPants5, rufus)

def rufusIntroPants6 = [id:6]
rufusIntroPants6.npctext = "Hmmm, no, that's okay. I'm sure he'll be along soon."
rufusIntroPants6.options = []
rufusIntroPants6.options << [text:"So, why are you wearing clothes? You do know you're just a cat, right?", result:3]
rufusIntroPants6.options << [text:"Tell me about Barton Boutique.", result:4]
rufusIntroPants.addDialog(rufusIntroPants6, rufus)

def rufusIntroPants7 = [id:7, npctext: "Besides, the pants that came with this suit didn't have a zipper anyway. Now, I'm tired of being insulted. Come back when you've learned some manners.", result: DONE]
rufusIntroPants.addDialog(rufusIntroPants7, rufus)

def rufusIntroPants8 = [id:8]
rufusIntroPants8.npctext = "Well, maybe if you help me with something. I lost my Diary. If you help me find it, I'll give you my pants."
rufusIntroPants8.playertext = "Where did you last see it? Just retrace your steps and I'm sure you'll remember where you left it."
rufusIntroPants8.result = 9
rufusIntroPants.addDialog(rufusIntroPants8, rufus)

def rufusIntroPants9 = [id:9]
rufusIntroPants9.npctext = "I was writing in it behind the well... it must be somewhere between here and there. Do you think you could look? I trust you not to read it."
rufusIntroPants9.options = []
rufusIntroPants9.options << [text:"Sure, Rufus. I'll go look immediately.", result: 10]
rufusIntroPants9.options << [text:"Not yet, Rufus. I'm busy.", flag: "Z1_Rufus_TeasingRufus_NotTeased", result: DONE]
rufusIntroPants.addDialog(rufusIntroPants9, rufus)

def rufusIntroPants10 = [id:10]
rufusIntroPants10.npctext = "Thanks!"
rufusIntroPants10.flag = "Z1_Rufus_TeasingRufus_NotTeased"
rufusIntroPants10.quest = 37
rufusIntroPants10.result = DONE
rufusIntroPants.addDialog(rufusIntroPants10, rufus)

def rufusIntroPants11 = [id:11]
rufusIntroPants11.npctext = "Oh, heh, that... Well, the pants that came with this outfit didn't come with a hole for my tail. Or a zipper, for that matter. So I just don't wear them. It's not like anybody would complain about a cat not wearing pants anyway."
rufusIntroPants11.options = []
rufusIntroPants11.options << [text:"That's a good point. So, since you don't need them, do you think I could have them?", result: 8]
rufusIntroPants11.options << [text:"It does seem an odd complaint. Goodbye for now, Rufus.", flag: "Z1_Rufus_TeasingRufus_NotTeased", result: DONE]
rufusIntroPants.addDialog(rufusIntroPants11, rufus)

//---------------------------------------------------------------------------------------------------------------
//This conversation is given if the player has teased Rufus                                                      
//Does it make sense to put this conversation in a separate file, put it in a {} closure, or leave as is?        
//---------------------------------------------------------------------------------------------------------------
def rufusConversationB = rufus.createConversation("rufusConversationB", true, "Z1_Rufus_TeasingRufus_Teased", "!QuestStarted_37", "!Z1_Rufus_TeasingRufus_NotTeased", "!Z1_Rufus_TeasingRufus_AngryRufus", "!QuestCompleted_37")

def dialogB1 = [id:1]
dialogB1.npctext = "Back to make fun of my clothing again?"
dialogB1.playertext = "Looks like I hurt your feelings."
dialogB1.options = []
dialogB1.options << [text:"I'm sorry Rufus, I really didn't mean to upset you.", result:2]
dialogB1.options << [text:"Lighten up, it was just a joke.", result:3]
dialogB1.options << [text:"I just think a talking cat wearing a coat and hat without pants is ridiculous.", flag:["!Z1_Rufus_TeasingRufus_Teased", "Z1_Rufus_TeasingRufus_AngryRufus"], result:4]
rufusConversationB.addDialog(dialogB1, rufus)

def dialogB2 = [id:2]
dialogB2.npctext = "Well, that's okay. I suppose I was being a little too sensitive anyway. Apology accepted, %p."
dialogB2.options = []
dialogB2.options << [text:"So, what's going on with you, Rufus?", result: 5]
dialogB2.options << [text:"Thanks. See you around.", flag: ["!Z1_Rufus_TeasingRufus_Teased", "Z1_Rufus_TeasingRufus_NotTeased"], result: DONE]
dialogB2.flag = ["Z1_Rufus_TeasingRufus_NotTeased", "!Z1_Rufus_TeasingRufus_Teased"]
dialogB2.result = DONE
rufusConversationB.addDialog(dialogB2, rufus)

def dialogB3 = [id:3]
dialogB3.npctext = "Making fun of someone you've just met is not funny."
dialogB3.options = []
dialogB3.options << [text:"You're right. It was rude of me. I'm sorry, Rufus.", flag: ["Z1_Rufus_TeasingRufus_NotTeased", "!Z1_Rufus_TeasingRufus_Teased"], result:2]
dialogB3.options << [text:"But you're just a cat! You have no business talking or wearing clothes!", result:4]
rufusConversationB.addDialog(dialogB3, rufus)

def dialogB4 = [id:4, npctext: "LEAVE ME ALONE!", result:DONE]
rufusConversationB.addDialog(dialogB4, rufus)

def dialogB5 = [id:5]
dialogB5.npctext = "I can't find my diary! I'm so worried that someone will find it and read it, but I have no idea where it is and I can't leave the boutique to look for it."
dialogB5.playertext = "Where did you last see it? Just retrace your steps and I'm sure you'll remember where you left it."
dialogB5.result = 6
rufusConversationB.addDialog(dialogB5, rufus)

def dialogB6 = [id:6]
dialogB6.npctext = "I was writing in it behind the well... it must be somewhere between here and there. Do you think you could look? I trust you not to read it."
dialogB6.options = []
dialogB6.options << [text:"Sure, Rufus. I'll go look immediately.", result: 7]
dialogB6.options << [text:"Not yet, Rufus. I'm busy.", result: DONE]
rufusConversationB.addDialog(dialogB6, rufus)

def dialogB7 = [id:7]
dialogB7.npctext = "Thanks!"
dialogB7.flag = ["!Z1_Rufus_TeasingRufus_Teased", "Z1_Rufus_TeasingRufus_NotTeased"]
dialogB7.quest = 37
dialogB7.result = DONE
rufusConversationB.addDialog(dialogB7, rufus)

//---------------------------------------------------------------------------------------------------------------------
//Rufus is angry now                                                                                                   
//---------------------------------------------------------------------------------------------------------------------
def rufusConversationG = rufus.createConversation("rufusConversationG", true, "Z1_Rufus_TeasingRufus_AngryRufus", "!QuestStarted_37", "!QuestCompleted_37")

def dialogG1 = [id:1]
dialogG1.npctext = "You've got a lot of nerve showing your face around here after the way you've been acting."
dialogG1.options = []
dialogG1.options << [text:"I know. I came back to apologize for making fun of you earlier. I'm sorry, Rufus.", result:2]
dialogG1.options << [text:"Uhg! Stop whining!", result:3]
rufusConversationG.addDialog(dialogG1, rufus)

def dialogG2 = [id:2]
dialogG2.npctext = "Well, I suppose it's alright. Just don't let it happen again."
dialogG2.playertext = "I'll be nice to you from now on."
dialogG2.flag = ["Z1_Rufus_TeasingRufus_NotTeased", "!Z1_Rufus_TeasingRufus_AngryRufus"]
dialogG2.result = 4
rufusConversationG.addDialog(dialogG2, rufus)

def dialogG3 = [id:3, npctext:"GET OUT OF HERE!", result:DONE]
rufusConversationG.addDialog(dialogG3, rufus)

def dialogG4 = [id:4]
dialogG4.npctext = "I'd appreciate it, thanks."
dialogG4.options = []
dialogG4.options << [text:"Sure thing. So, what's going on with you, Rufus?", result: 5]
dialogG4.options << [text:"You're welcome. Listen, Rufus, I have to run. See you around.", result: 8]
rufusConversationG.addDialog(dialogG4, rufus)

def dialogG5 = [id:5]
dialogG5.npctext = "I can't find my diary! I'm so worried that someone will find it and read it, but I have no idea where it is and I can't leave the boutique to look for it."
dialogG5.playertext = "Where did you last see it? Just retrace your steps and I'm sure you'll remember where you left it."
dialogG5.result = 6
rufusConversationG.addDialog(dialogG5, rufus)

def dialogG6 = [id:6]
dialogG6.npctext = "I was writing in it behind the well... it must be somewhere between here and there. Do you think you could look? I trust you not to read it."
dialogG6.options = []
dialogG6.options << [text:"Sure, Rufus. I'll go look immediately.", result: 7]
dialogG6.options << [text:"Not yet, Rufus. I'm busy.", result: 8]
rufusConversationG.addDialog(dialogG6, rufus)

def dialogG7 = [id:7]
dialogG7.npctext = "Thanks!"
dialogG7.flag = ["!Z1_Rufus_TeasingRufus_AngryRufus", "Z1_Rufus_TeasingRufus_NotTeased"]
dialogG7.quest = 37
dialogG7.result = DONE
rufusConversationG.addDialog(dialogG7, rufus)

def dialogG8 = [id:8]
dialogG8.npctext = "Oh, alright. Take care, %p."
dialogG8.result = DONE
rufusConversationG.addDialog(dialogG8, rufus)

//------------------------------------------------------------------------------------------------------------------
//Player was either nice to Rufus initially and has apologizied - can get quest down this path                      
//------------------------------------------------------------------------------------------------------------------
rufusConversationC1 = rufus.createConversation("rufusConversationC1", true, "Z1_Rufus_TeasingRufus_NotTeased", "!QuestStarted_37", "!QuestStarted_39", "!QuestCompleted_37")

def dialogC1 = [id:1]
dialogC1.npctext = "%p! Good to see you again."
dialogC1.options = []
dialogC1.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListC)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
dialogC1.options << [text:"Hey Rufus. Whatcha up to?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationC3)
}, result:DONE]
rufusConversationC1.addDialog(dialogC1, rufus)

//Dialog comes back in from random
rufusConversationC3 = rufus.createConversation("rufusConversationC3", true, "Z1_Rufus_TeasingRufus_NotTeased", "!QuestStarted_37", "!QuestStarted_39", "!QuestCompleted_37", "Z1_Rufus_Random_Out")

def dialogC3 = [id:1]
dialogC3.npctext = "I've lost my diary! I'm so worried that someone will find it and read it, but I can't leave the shop to look for it. I trust you not to read it. Do you think you could go look for it and bring it back to me?"
dialogC3.options = []
dialogC3.options << [text:"Sure, I'd be glad to look for it. Where did you last see it?", result:2]
dialogC3.options << [text:"Sorry, Rufus, I'm a little too busy right now.", flag:"!Z1_Rufus_Random_Out", result:DONE]
rufusConversationC3.addDialog(dialogC3, rufus)

def dialogC4 = [id:2]
dialogC4.npctext = "Great, I really appreciate it. I always write in my diary in the copse of trees near the well. It's a relatively empty part of town so it provides ample privacy. You could start looking there."
dialogC4.quest = 37
dialogC4.flag = "!Z1_Rufus_Random_Out"
dialogC4.result = DONE
rufusConversationC3.addDialog(dialogC4, rufus)

//----------------------------------------------------
//Random response C                                   
//----------------------------------------------------
rufusRandomCa = rufus.createConversation("rufusRandomCa", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "!QuestStarted_37", "!QuestStarted_39", "!QuestCompleted_37")

def randomCa = [id:1]
randomCa.npctext = "Purr~"
randomCa.options = []
randomCa.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListC1)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomCa.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationC3)
}, result:DONE]
rufusRandomCa.addDialog(randomCa, rufus)

rufusRandomCb = rufus.createConversation("rufusRandomCb", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "!QuestStarted_37", "!QuestStarted_39", "!QuestCompleted_37")

def randomCb = [id:1]
randomCb.npctext = "Don't even think about pulling my tail."
randomCb.options = []
randomCb.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListC2)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomCb.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationC3)
}, result:DONE]
rufusRandomCb.addDialog(randomCb, rufus)

rufusRandomCc = rufus.createConversation("rufusRandomCc", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "!QuestStarted_37", "!QuestStarted_39", "!QuestCompleted_37")

def randomCc = [id:1]
randomCc.npctext = "*yawn*"
randomCc.options = []
randomCc.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListC3)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomCc.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationC3)
}, result:DONE]
rufusRandomCc.addDialog(randomCc, rufus)

rufusRandomCd = rufus.createConversation("rufusRandomCd", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "!QuestStarted_37", "!QuestStarted_39", "!QuestCompleted_37")

def randomCd = [id:1]
randomCd.npctext = "Meow~"
randomCd.options = []
randomCd.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListC4)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomCd.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationC3)
}, result:DONE]
rufusRandomCd.addDialog(randomCd, rufus)

rufusRandomCe = rufus.createConversation("rufusRandomCe", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "!QuestStarted_37", "!QuestStarted_39", "!QuestCompleted_37")

def randomCe = [id:1]
randomCe.npctext = "*woof* *woof*"
randomCe.options = []
randomCe.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListC5)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomCe.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationC3)
}, result:DONE]
rufusRandomCe.addDialog(randomCe, rufus)

rufusRandomCf = rufus.createConversation("rufusRandomCf", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "!QuestStarted_37", "!QuestStarted_39", "!QuestCompleted_37")

def randomCf = [id:1]
randomCf.npctext = "Ooo! Ooo! I love being scratched behind the ears."
randomCf.options = []
randomCf.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListC6)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomCf.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationC3)
}, result:DONE]
rufusRandomCf.addDialog(randomCf, rufus)

rufusRandomCg = rufus.createConversation("rufusRandomCg", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "!QuestStarted_37", "!QuestStarted_39", "!QuestCompleted_37")

def randomCg = [id:1]
randomCg.npctext = "I'm not moving, no matter how many times you nudge me."
randomCg.options = []
randomCg.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListC7)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomCg.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationC3)
}, result:DONE]
rufusRandomCg.addDialog(randomCg, rufus)

//--------------------------------------------------------------------------------------------------------------------
//Player returns to Rufus after accepting quest but before locating diary                                             
//--------------------------------------------------------------------------------------------------------------------
def rufusConversationD = rufus.createConversation("rufusConversationD", true, "QuestStarted_37:2", "!QuestStarted_39", "!QuestCompleted_39")

def dialogD1 = [id:1, npctext:"Have you found my diary?", result:2]
rufusConversationD.addDialog(dialogD1, rufus)

def dialogD2 = [id:2, playertext:"Not yet, Rufus.", result:3]
rufusConversationD.addDialog(dialogD2, rufus)

def dialogD3 = [id:3, npctext:"It has to be somewhere between here and there. Just head to the northwest section of town and you're sure to come across it.", result:4]
rufusConversationD.addDialog(dialogD3, rufus)

def dialogD4 = [id:4, playertext:"I'll look over that way, then.", result:DONE]
rufusConversationD.addDialog(dialogD4, rufus)

//--------------------------------------------------------------------------------------------------------------------
//Player has found Diary, Rufus thanks player                                                                         
//--------------------------------------------------------------------------------------------------------------------
def rufusConversationE = rufus.createConversation("rufusConversationE", true, "QuestStarted_37:3", "!QuestStarted_39", "!QuestCompleted_39")

def dialogE1 = [id:1] 
dialogE1.npctext = "My diary! I'm so relieved. Thank you, %p."
dialogE1.playertext = "I was happy to help." 
dialogE1.quest = 37
dialogE1.exec = { event -> event.player.removeMiniMapQuestActorName("Rufus-VQS") } 
dialogE1.result = DONE
rufusConversationE.addDialog(dialogE1, rufus)

//--------------------------------------------------------------------------------------------------------------------
//Player has found diary and been thanked by Rufus - standard conversation after quest completion                     
//--------------------------------------------------------------------------------------------------------------------
rufusConversationF1 = rufus.createConversation("rufusConversationF1", true, "QuestCompleted_37", "!QuestStarted_39", "!QuestCompleted_39")

def dialogF1 = [id:1]
dialogF1.npctext = "Hey, %p! I'm so glad to see you again!"
dialogF1.playertext = "Hey, Rufus!"
dialogF1.options = []
dialogF1.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListF)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
dialogF1.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationF3)
}, result:DONE]
rufusConversationF1.addDialog(dialogF1, rufus)

rufusConversationF3 = rufus.createConversation("rufusConversationF3", true, "QuestCompleted_37", "!QuestStarted_39", "Z1_Rufus_Random_Out")

def dialogF3 = [id:3]
dialogF3.npctext = "At Barton Boutique you can buy all sorts of clothing, but we specialize in animal accessories. Ever wanted a tail? Paws? Wings? If so, you've coming to the right place."
dialogF3.options = []
dialogF3.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListF)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
dialogF3.options << [text:"Thanks Rufus, goodbye for now.", flag: "!Z1_Rufus_Random_Out", result:DONE]
rufusConversationF3.addDialog(dialogF3, rufus)

//----------------------------------------------------
//Random response F                                   
//----------------------------------------------------
rufusRandomFa = rufus.createConversation("rufusRandomFa", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "!QuestStarted_39")

def randomFa = [id:1]
randomFa.npctext = "Purr~"
randomFa.options = []
randomFa.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListF1)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomFa.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationF3)
}, result:DONE]
rufusRandomFa.addDialog(randomFa, rufus)

rufusRandomFb = rufus.createConversation("rufusRandomFb", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "!QuestStarted_39")

def randomFb = [id:1]
randomFb.npctext = "Don't even think about pulling my tail."
randomFb.options = []
randomFb.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListF2)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomFb.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationF3)
}, result:DONE]
rufusRandomFb.addDialog(randomFb, rufus)

rufusRandomFc = rufus.createConversation("rufusRandomFc", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "!QuestStarted_39")

def randomFc = [id:1]
randomFc.npctext = "*yawn*"
randomFc.options = []
randomFc.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListF3)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomFc.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationF3)
}, result:DONE]
rufusRandomFc.addDialog(randomFc, rufus)

rufusRandomFd = rufus.createConversation("rufusRandomFd", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "!QuestStarted_39")

def randomFd = [id:1]
randomFd.npctext = "Meow~"
randomFd.options = []
randomFd.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListF4)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomFd.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationF3)
}, result:DONE]
rufusRandomFd.addDialog(randomFd, rufus)

rufusRandomFe = rufus.createConversation("rufusRandomFe", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "!QuestStarted_39")

def randomFe = [id:1]
randomFe.npctext = "*woof* *woof*"
randomFe.options = []
randomFe.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListF5)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomFe.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationF3)
}, result:DONE]
rufusRandomFe.addDialog(randomFe, rufus)

rufusRandomFf = rufus.createConversation("rufusRandomFf", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "!QuestStarted_39")

def randomFf = [id:1]
randomFf.npctext = "Ooo! Ooo! I love being scratched behind the ears."
randomFf.options = []
randomFf.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListF6)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomFf.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationF3)
}, result:DONE]
rufusRandomFf.addDialog(randomFf, rufus)

rufusRandomFg = rufus.createConversation("rufusRandomFg", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "!QuestStarted_39")

def randomFg = [id:1]
randomFg.npctext = "I'm not moving, no matter how many times you nudge me."
randomFg.options = []
randomFg.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListF7)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomFg.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationF3)
}, result:DONE]
rufusRandomFg.addDialog(randomFg, rufus)

//-----------------------------------------------------------------------------------------------------------------------
//This is a replacement for rufusConversationC if player has visited Fernando already                                    
//-----------------------------------------------------------------------------------------------------------------------
rufusConversationH1 = rufus.createConversation("rufusConversationH1", true, "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39", "!QuestCompleted_37")

def dialogH1 = [id:1]
dialogH1.npctext = "%p! Good to see you again."
dialogH1.options = []
dialogH1.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListH)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
dialogH1.options << [text:"Hey Rufus. Whatcha up to?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out3"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH3)
}, result:DONE]
dialogH1.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out5"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH5)
}, result:DONE]
rufusConversationH1.addDialog(dialogH1, rufus)

rufusConversationH3 = rufus.createConversation("rufusConversationH3", true, "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39", "Z1_Rufus_Random_Out3")

def dialogH3 = [id:1]
dialogH3.npctext = "I've lost my diary! I'm so worried that someone will find it and read it, but I can't leave the shop to look for it. I trust you not to read it. Do you think you could go look for it and bring it back to me?"
dialogH3.options = []
dialogH3.options << [text:"Sure, I'd be glad to look for it. Where did you last see it?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out4", "!Z1_Rufus_Random_Out3"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH4)
}, result:DONE]
dialogH3.options << [text:"Sorry, Rufus, I'm a little too busy right now.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out8", "!Z1_Rufus_Random_Out3"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH8)
}, result:DONE]
rufusConversationH3.addDialog(dialogH3, rufus)

rufusConversationH4 = rufus.createConversation("rufusConversationH4", true, "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39", "Z1_Rufus_Random_Out4")

def dialogH4 = [id:1]
dialogH4.npctext = "Great, I really appreciate it. I always write in my diary in the copse of trees near the well. It's a relatively empty part of town so it provides ample privacy. You could start looking there."
dialogH4.quest = 37
dialogH4.flag = "!Z1_Rufus_Random_Out4"
dialogH4.result = DONE
rufusConversationH4.addDialog(dialogH4, rufus)

rufusConversationH5 = rufus.createConversation("rufusConversationH5", true, "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39", "Z1_Rufus_Random_Out5")

def dialogH5 = [id:1, npctext:"I think I have them around here somewhere. I never wear them. They don't even have a zipper. Where would I put my tail?", result:2]
rufusConversationH5.addDialog(dialogH5, rufus)

def dialogH6 = [id:2, playertext:"Do you think I could have them?", result:3]
rufusConversationH5.addDialog(dialogH6, rufus)

def dialogH7 = [id:3]
dialogH7.npctext = "Well, I suppose I could part with them if you help me with something..."
dialogH7.options = []
dialogH7.options << [text:"What do you need help with?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out3", "!Z1_Rufus_Random_Out5"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH3)
}, result:DONE]
dialogH7.options << [text:"Let me think about it and get back to you.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out8", "!Z1_Rufus_Random_Out5"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH8)
}, result:DONE]
rufusConversationH5.addDialog(dialogH7, rufus)

def rufusConversationH8 = rufus.createConversation("rufusConversationH8", true, "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39", "Z1_Rufus_Random_Out8")

def dialogH8 = [id:1, npctext:"Okay, let me know if you change your mind", flag:"!Z1_Rufus_Random_Out8", result:DONE]
rufusConversationH8.addDialog(dialogH8, rufus)

//----------------------------------------------------
//Random response H                                   
//----------------------------------------------------
rufusRandomHa = rufus.createConversation("rufusRandomHa", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39")

def randomHa = [id:1]
randomHa.npctext = "Purr~"
randomHa.options = []
randomHa.options << [text:"Pet Rufus.", exec: {event -> 
	rufusRandom = random(rufusPetListH1)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomHa.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out5"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH5)
}, result:DONE]
randomHa.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out3"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH3)
}, result:DONE]
rufusRandomHa.addDialog(randomHa, rufus)

rufusRandomHb = rufus.createConversation("rufusRandomHb", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39")

def randomHb = [id:1]
randomHb.npctext = "Don't even think about pulling my tail."
randomHb.options = []
randomHb.options << [text:"Pet Rufus.", exec: {event -> 
	rufusRandom = random(rufusPetListH2)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomHb.options << [text:"What did you do with your pants, Rufus?", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out5"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH5)
}, result:DONE]
randomHb.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out3"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH3)
}, result:DONE]
rufusRandomHb.addDialog(randomHb, rufus)

rufusRandomHc = rufus.createConversation("rufusRandomHc", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39")

def randomHc = [id:1]
randomHc.npctext = "*yawn*"
randomHc.options = []
randomHc.options << [text:"Pet Rufus.", exec: {event -> 
	rufusRandom = random(rufusPetListH3)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomHc.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out5"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH5)
}, result:DONE]
randomHc.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out3"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH3)
}, result:DONE]
rufusRandomHc.addDialog(randomHc, rufus)

rufusRandomHd = rufus.createConversation("rufusRandomHd", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39")

def randomHd = [id:1]
randomHd.npctext = "Meow~"
randomHd.options = []
randomHd.options << [text:"Pet Rufus.", exec: {event -> 
	rufusRandom = random(rufusPetListH4)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomHd.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out5"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH5)
}, result:DONE]
randomHd.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out3"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH3)
}, result:DONE]
rufusRandomHd.addDialog(randomHd, rufus)

rufusRandomHe = rufus.createConversation("rufusRandomHe", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39")

def randomHe = [id:1]
randomHe.npctext = "*woof* *woof*"
randomHe.options = []
randomHe.options << [text:"Pet Rufus.", exec: {event -> 
	rufusRandom = random(rufusPetListH5)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomHe.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out5"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH5)
}, result:DONE]
randomHe.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out3"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH3)
}, result:DONE]
rufusRandomHe.addDialog(randomHe, rufus)

rufusRandomHf = rufus.createConversation("rufusRandomHf", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39")

def randomHf = [id:1]
randomHf.npctext = "Ooo! Ooo! I love being scratched behind the ears."
randomHf.options = []
randomHf.options << [text:"Pet Rufus.", exec: {event -> 
	rufusRandom = random(rufusPetListH6)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomHf.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out5"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH5)
}, result:DONE]
randomHf.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out3"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH3)
}, result:DONE]
rufusRandomHf.addDialog(randomHf, rufus)

rufusRandomHg = rufus.createConversation("rufusRandomHg", true, "Z1_Rufus_Random_In", "Z1_Rufus_TeasingRufus_NotTeased", "QuestStarted_39", "!QuestStarted_37", "!QuestCompleted_39")

def randomHg = [id:1]
randomHg.npctext = "I'm not moving, no matter how many times you nudge me."
randomHg.options = []
randomHg.options << [text:"Pet Rufus.", exec: {event -> 
	rufusRandom = random(rufusPetListH7)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomHg.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out5"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH5)
}, result:DONE]
randomHg.options << [text:"So, anything going on?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out3"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationH3)
}, result:DONE]
rufusRandomHg.addDialog(randomHg, rufus)

//------------------------------------------------------------------------------------------------------------------
//This is a replacment for rufusConversationF if the player has visited Fernando                                    
//------------------------------------------------------------------------------------------------------------------
rufusConversationI1 = rufus.createConversation("rufusConversationI1", true, "QuestCompleted_37", "QuestStarted_39", "!QuestStarted_39:3")

def dialogI1 = [id:1]
dialogI1.npctext = "Hey, %p! I'm so glad to see you again!"
dialogI1.playertext = "Hey, Rufus!"
dialogI1.options = []
dialogI1.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListI)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
dialogI1.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI4)
}, result:DONE]
dialogI1.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI3)
}, result:DONE]
rufusConversationI1.addDialog(dialogI1, rufus)

rufusConversationI3 = rufus.createConversation("rufusConversationI3", true, "QuestCompleted_37", "QuestStarted_39", "!QuestStarted_39:3", "Z1_Rufus_Random_Out")

def dialogI3 = [id:1]
dialogI3.npctext = "At Barton Boutique you can buy all sorts of clothing, but we specialize in animal accessories. Ever wanted a tail? Paws? Wings? If so, you've coming to the right place."
dialogI3.options = []
dialogI3.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListI)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
dialogI3.options << [text:"Thanks, Rufus. Goodbye, for now.", flag:["Z1_Rufus_TeasingRufus_NotTeased", "!Z1_Rufus_Random_Out"], result:DONE]
rufusConversationI3.addDialog(dialogI3, rufus)

rufusConversationI4 = rufus.createConversation("rufusConversationI4", true, "QuestCompleted_37", "QuestStarted_39", "!QuestStarted_39:3", "Z1_Rufus_Random_Out")

def dialogI4 = [id:1, npctext:"I think I have them around here somewhere. I never wear them. They don't even have a zipper. Where would I put my tail?", result:2]
rufusConversationI4.addDialog(dialogI4, rufus)

def dialogI5 = [id:2, playertext:"Do you think I could have them?", result:3]
rufusConversationI4.addDialog(dialogI5, rufus)

def dialogI6 = [id:3, npctext:"Uhm, I suppose I owe you. What do you need them for?", quest:39, result:4]
rufusConversationI4.addDialog(dialogI6, rufus)

def dialogI7 = [id:4]
dialogI7.playertext ="I'm giving them to Fernando so he'll dance again."
dialogI7.flag = "!Z1_Rufus_Random_Out"
dialogI7.quest = 39
dialogI7.result = DONE
rufusConversationI4.addDialog(dialogI7, rufus)

//----------------------------------------------------
//Random response I                                   
//----------------------------------------------------
rufusRandomIa = rufus.createConversation("rufusRandomIa", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "QuestStarted_39", "!QuestStarted_39:3")

def randomIa = [id:1]
randomIa.npctext = "Purr~"
randomIa.options = []
randomIa.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListI)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomIa.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI4)
}, result:DONE]
randomIa.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI3)
}, result:DONE]
rufusRandomIa.addDialog(randomIa, rufus)

rufusRandomIb = rufus.createConversation("rufusRandomIb", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "QuestStarted_39", "!QuestStarted_39:3")

def randomIb = [id:1]
randomIb.npctext = "Don't even think about pulling my tail."
randomIb.options = []
randomIb.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListI)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomIb.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI4)
}, result:DONE]
randomIb.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI3)
}, result:DONE]
rufusRandomIb.addDialog(randomIb, rufus)

rufusRandomIc = rufus.createConversation("rufusRandomIc", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "QuestStarted_39", "!QuestStarted_39:3")

def randomIc = [id:1]
randomIc.npctext = "*yawn*"
randomIc.options = []
randomIc.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListI)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomIc.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI4)
}, result:DONE]
randomIc.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI3)
}, result:DONE]
rufusRandomIc.addDialog(randomIc, rufus)

rufusRandomId = rufus.createConversation("rufusRandomId", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "QuestStarted_39", "!QuestStarted_39:3")

def randomId = [id:1]
randomId.npctext = "Meow~"
randomId.options = []
randomId.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListI)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomId.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI4)
}, result:DONE]
randomId.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI3)
}, result:DONE]
rufusRandomId.addDialog(randomId, rufus)

rufusRandomIe = rufus.createConversation("rufusRandomIe", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "QuestStarted_39", "!QuestStarted_39:3")

def randomIe = [id:1]
randomIe.npctext = "*woof* *woof*"
randomIe.options = []
randomIe.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListI)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomIe.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI4)
}, result:DONE]
randomIe.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI3)
}, result:DONE]
rufusRandomIe.addDialog(randomIe, rufus)

rufusRandomIf = rufus.createConversation("rufusRandomIf", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "QuestStarted_39", "!QuestStarted_39:3")

def randomIf = [id:1]
randomIf.npctext = "Ooo! Ooo! I love being scratched behind the ears."
randomIf.options = []
randomIf.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListI)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomIf.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI4)
}, result:DONE]
randomIf.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI3)
}, result:DONE]
rufusRandomIf.addDialog(randomIf, rufus)

rufusRandomIg = rufus.createConversation("rufusRandomIg", true, "Z1_Rufus_Random_In", "QuestCompleted_37", "QuestStarted_39", "!QuestStarted_39:3")

def randomIg = [id:1]
randomIg.npctext = "I'm not moving, no matter how many times you nudge me."
randomIg.options = []
randomIg.options << [text:"Pet Rufus.", flag:["Z1_Rufus_Random_In", "!Z1_Rufus_Random_Out"], exec: {event -> 
	rufusRandom = random(rufusPetListI)
	rufus.pushDialog(event.player, rufusRandom)
}, result:DONE]
randomIg.options << [text:"What did you do with your pants, Rufus?", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI4)
}, result:DONE]
randomIg.options << [text:"Tell me about Barton Boutique.", flag:["!Z1_Rufus_Random_In", "Z1_Rufus_Random_Out"], exec: {event -> 
	rufus.pushDialog(event.player, rufusConversationI3)
}, result:DONE]
rufusRandomIg.addDialog(randomIg, rufus)

//------------------------------------------------------------------------------------------------------------------
//Replacement for rufusConversationE if player has Fernando quest                                                   
//------------------------------------------------------------------------------------------------------------------
def rufusConversationJ = rufus.createConversation("rufusConversationJ", true, "QuestStarted_37:3", "QuestStarted_39", "QuestStarted_39", "!QuestStarted_39:3")
rufusConversationJ.setUrgent(true)

def dialogJ1 = [id:1]
dialogJ1.npctext = "My diary! I'm so relieved. Thank you, %p."
dialogJ1.result = 2
rufusConversationJ.addDialog(dialogJ1, rufus)

def dialogJ2 = [id:2, playertext:"You're welcome. How about those pants?", result:3]
rufusConversationJ.addDialog(dialogJ2, rufus)

def dialogJ3 = [id:3]
dialogJ3.npctext = "Okay, %p, a deal is a deal."
dialogJ3.exec = { event ->
	event.player.updateQuest(37, "Rufus-VQS")
	event.player.updateQuest(39, "Rufus-VQS")
	event.player.removeMiniMapQuestActorName("Rufus-VQS")
}
dialogJ3.result = DONE
rufusConversationJ.addDialog(dialogJ3, rufus)
	
//----------------------------------------------------
//Random array C                                      
//----------------------------------------------------
rufusPetListC = []
rufusPetListC << "rufusRandomCa"
rufusPetListC << "rufusRandomCb"
rufusPetListC << "rufusRandomCc"
rufusPetListC << "rufusRandomCd"
rufusPetListC << "rufusRandomCe"
rufusPetListC << "rufusRandomCf"
rufusPetListC << "rufusRandomCg"

rufusPetListC1 = []
rufusPetListC1 << "rufusRandomCb"
rufusPetListC1 << "rufusRandomCc"
rufusPetListC1 << "rufusRandomCd"
rufusPetListC1 << "rufusRandomCe"
rufusPetListC1 << "rufusRandomCf"
rufusPetListC1 << "rufusRandomCg"

rufusPetListC2 = []
rufusPetListC2 << "rufusRandomCa"
rufusPetListC2 << "rufusRandomCc"
rufusPetListC2 << "rufusRandomCd"
rufusPetListC2 << "rufusRandomCe"
rufusPetListC2 << "rufusRandomCf"
rufusPetListC2 << "rufusRandomCg"

rufusPetListC3 = []
rufusPetListC3 << "rufusRandomCa"
rufusPetListC3 << "rufusRandomCb"
rufusPetListC3 << "rufusRandomCd"
rufusPetListC3 << "rufusRandomCe"
rufusPetListC3 << "rufusRandomCf"
rufusPetListC3 << "rufusRandomCg"

rufusPetListC4 = []
rufusPetListC4 << "rufusRandomCa"
rufusPetListC4 << "rufusRandomCb"
rufusPetListC4 << "rufusRandomCc"
rufusPetListC4 << "rufusRandomCe"
rufusPetListC4 << "rufusRandomCf"
rufusPetListC4 << "rufusRandomCg"

rufusPetListC5 = []
rufusPetListC5 << "rufusRandomCa"
rufusPetListC5 << "rufusRandomCb"
rufusPetListC5 << "rufusRandomCc"
rufusPetListC5 << "rufusRandomCd"
rufusPetListC5 << "rufusRandomCf"
rufusPetListC5 << "rufusRandomCg"

rufusPetListC6 = []
rufusPetListC6 << "rufusRandomCa"
rufusPetListC6 << "rufusRandomCb"
rufusPetListC6 << "rufusRandomCc"
rufusPetListC6 << "rufusRandomCd"
rufusPetListC6 << "rufusRandomCe"
rufusPetListC6 << "rufusRandomCg"

rufusPetListC7 = []
rufusPetListC7 << "rufusRandomCa"
rufusPetListC7 << "rufusRandomCb"
rufusPetListC7 << "rufusRandomCc"
rufusPetListC7 << "rufusRandomCd"
rufusPetListC7 << "rufusRandomCe"
rufusPetListC7 << "rufusRandomCf"

//----------------------------------------------------
//Random array F                                      
//----------------------------------------------------
rufusPetListF = []
rufusPetListF << "rufusRandomFa"
rufusPetListF << "rufusRandomFb"
rufusPetListF << "rufusRandomFc"
rufusPetListF << "rufusRandomFd"
rufusPetListF << "rufusRandomFe"
rufusPetListF << "rufusRandomFf"
rufusPetListF << "rufusRandomFg"

rufusPetListF1 = []
rufusPetListF1 << "rufusRandomFb"
rufusPetListF1 << "rufusRandomFc"
rufusPetListF1 << "rufusRandomFd"
rufusPetListF1 << "rufusRandomFe"
rufusPetListF1 << "rufusRandomFf"
rufusPetListF1 << "rufusRandomFg"

rufusPetListF2 = []
rufusPetListF2 << "rufusRandomFa"
rufusPetListF2 << "rufusRandomFc"
rufusPetListF2 << "rufusRandomFd"
rufusPetListF2 << "rufusRandomFe"
rufusPetListF2 << "rufusRandomFf"
rufusPetListF2 << "rufusRandomFg"

rufusPetListF3 = []
rufusPetListF3 << "rufusRandomFa"
rufusPetListF3 << "rufusRandomFb"
rufusPetListF3 << "rufusRandomFd"
rufusPetListF3 << "rufusRandomFe"
rufusPetListF3 << "rufusRandomFf"
rufusPetListF3 << "rufusRandomFg"

rufusPetListF4 = []
rufusPetListF4 << "rufusRandomFa"
rufusPetListF4 << "rufusRandomFb"
rufusPetListF4 << "rufusRandomFc"
rufusPetListF4 << "rufusRandomFe"
rufusPetListF4 << "rufusRandomFf"
rufusPetListF4 << "rufusRandomFg"

rufusPetListF5 = []
rufusPetListF5 << "rufusRandomFa"
rufusPetListF5 << "rufusRandomFb"
rufusPetListF5 << "rufusRandomFc"
rufusPetListF5 << "rufusRandomFd"
rufusPetListF5 << "rufusRandomFf"
rufusPetListF5 << "rufusRandomFg"

rufusPetListF6 = []
rufusPetListF6 << "rufusRandomFa"
rufusPetListF6 << "rufusRandomFb"
rufusPetListF6 << "rufusRandomFc"
rufusPetListF6 << "rufusRandomFd"
rufusPetListF6 << "rufusRandomFe"
rufusPetListF6 << "rufusRandomFg"

rufusPetListF7 = []
rufusPetListF7 << "rufusRandomFa"
rufusPetListF7 << "rufusRandomFb"
rufusPetListF7 << "rufusRandomFc"
rufusPetListF7 << "rufusRandomFd"
rufusPetListF7 << "rufusRandomFe"
rufusPetListF7 << "rufusRandomFf"

//----------------------------------------------------
//Random array H                                      
//----------------------------------------------------
rufusPetListH = []
rufusPetListH << "rufusRandomHa"
rufusPetListH << "rufusRandomHb"
rufusPetListH << "rufusRandomHc"
rufusPetListH << "rufusRandomHd"
rufusPetListH << "rufusRandomHe"
rufusPetListH << "rufusRandomHf"
rufusPetListH << "rufusRandomHg"

rufusPetListH1 = []
rufusPetListH1 << "rufusRandomHb"
rufusPetListH1 << "rufusRandomHc"
rufusPetListH1 << "rufusRandomHd"
rufusPetListH1 << "rufusRandomHe"
rufusPetListH1 << "rufusRandomHf"
rufusPetListH1 << "rufusRandomHg"

rufusPetListH2 = []
rufusPetListH2 << "rufusRandomHa"
rufusPetListH2 << "rufusRandomHc"
rufusPetListH2 << "rufusRandomHd"
rufusPetListH2 << "rufusRandomHe"
rufusPetListH2 << "rufusRandomHf"
rufusPetListH2 << "rufusRandomHg"

rufusPetListH3 = []
rufusPetListH3 << "rufusRandomHa"
rufusPetListH3 << "rufusRandomHb"
rufusPetListH3 << "rufusRandomHd"
rufusPetListH3 << "rufusRandomHe"
rufusPetListH3 << "rufusRandomHf"
rufusPetListH3 << "rufusRandomHg"

rufusPetListH4 = []
rufusPetListH4 << "rufusRandomHa"
rufusPetListH4 << "rufusRandomHb"
rufusPetListH4 << "rufusRandomHc"
rufusPetListH4 << "rufusRandomHe"
rufusPetListH4 << "rufusRandomHf"
rufusPetListH4 << "rufusRandomHg"

rufusPetListH5 = []
rufusPetListH5 << "rufusRandomHa"
rufusPetListH5 << "rufusRandomHb"
rufusPetListH5 << "rufusRandomHc"
rufusPetListH5 << "rufusRandomHd"
rufusPetListH5 << "rufusRandomHf"
rufusPetListH5 << "rufusRandomHg"

rufusPetListH6 = []
rufusPetListH6 << "rufusRandomHa"
rufusPetListH6 << "rufusRandomHb"
rufusPetListH6 << "rufusRandomHc"
rufusPetListH6 << "rufusRandomHd"
rufusPetListH6 << "rufusRandomHe"
rufusPetListH6 << "rufusRandomHg"

rufusPetListH7 = []
rufusPetListH7 << "rufusRandomHa"
rufusPetListH7 << "rufusRandomHb"
rufusPetListH7 << "rufusRandomHc"
rufusPetListH7 << "rufusRandomHd"
rufusPetListH7 << "rufusRandomHe"
rufusPetListH7 << "rufusRandomHf"

//----------------------------------------------------
//Random array I                                      
//----------------------------------------------------
rufusPetListI = []
rufusPetListI << "rufusRandomIa"
rufusPetListI << "rufusRandomIb"
rufusPetListI << "rufusRandomIc"
rufusPetListI << "rufusRandomId"
rufusPetListI << "rufusRandomIe"
rufusPetListI << "rufusRandomIf"
rufusPetListI << "rufusRandomIg"

rufusPetListI1 = []
rufusPetListI1 << "rufusRandomIb"
rufusPetListI1 << "rufusRandomIc"
rufusPetListI1 << "rufusRandomId"
rufusPetListI1 << "rufusRandomIe"
rufusPetListI1 << "rufusRandomIf"
rufusPetListI1 << "rufusRandomIg"

rufusPetListI2 = []
rufusPetListI2 << "rufusRandomIa"
rufusPetListI2 << "rufusRandomIc"
rufusPetListI2 << "rufusRandomId"
rufusPetListI2 << "rufusRandomIe"
rufusPetListI2 << "rufusRandomIf"
rufusPetListI2 << "rufusRandomIg"

rufusPetListI3 = []
rufusPetListI3 << "rufusRandomIa"
rufusPetListI3 << "rufusRandomIb"
rufusPetListI3 << "rufusRandomId"
rufusPetListI3 << "rufusRandomIe"
rufusPetListI3 << "rufusRandomIf"
rufusPetListI3 << "rufusRandomIg"

rufusPetListI4 = []
rufusPetListI4 << "rufusRandomIa"
rufusPetListI4 << "rufusRandomIb"
rufusPetListI4 << "rufusRandomIc"
rufusPetListI4 << "rufusRandomIe"
rufusPetListI4 << "rufusRandomIf"
rufusPetListI4 << "rufusRandomIg"

rufusPetListI5 = []
rufusPetListI5 << "rufusRandomIa"
rufusPetListI5 << "rufusRandomIb"
rufusPetListI5 << "rufusRandomIc"
rufusPetListI5 << "rufusRandomId"
rufusPetListI5 << "rufusRandomIf"
rufusPetListI5 << "rufusRandomIg"

rufusPetListI6 = []
rufusPetListI6 << "rufusRandomIa"
rufusPetListI6 << "rufusRandomIb"
rufusPetListI6 << "rufusRandomIc"
rufusPetListI6 << "rufusRandomId"
rufusPetListI6 << "rufusRandomIe"
rufusPetListI6 << "rufusRandomIg"

rufusPetListI7 = []
rufusPetListI7 << "rufusRandomIa"
rufusPetListI7 << "rufusRandomIb"
rufusPetListI7 << "rufusRandomIc"
rufusPetListI7 << "rufusRandomId"
rufusPetListI7 << "rufusRandomIe"
rufusPetListI7 << "rufusRandomIf"

//----------------------------------------------------
//Random array J                                      
//----------------------------------------------------
rufusPetListJ = []
rufusPetListJ << "rufusRandomJa"
rufusPetListJ << "rufusRandomJb"
rufusPetListJ << "rufusRandomJc"
rufusPetListJ << "rufusRandomJd"
rufusPetListJ << "rufusRandomJe"
rufusPetListJ << "rufusRandomJf"
rufusPetListJ << "rufusRandomJg"

rufusPetListJ1 = []
rufusPetListJ1 << "rufusRandomJb"
rufusPetListJ1 << "rufusRandomJc"
rufusPetListJ1 << "rufusRandomJd"
rufusPetListJ1 << "rufusRandomJe"
rufusPetListJ1 << "rufusRandomJf"
rufusPetListJ1 << "rufusRandomJg"

rufusPetListJ2 = []
rufusPetListJ2 << "rufusRandomJa"
rufusPetListJ2 << "rufusRandomJc"
rufusPetListJ2 << "rufusRandomJd"
rufusPetListJ2 << "rufusRandomJe"
rufusPetListJ2 << "rufusRandomJf"
rufusPetListJ2 << "rufusRandomJg"

rufusPetListJ3 = []
rufusPetListJ3 << "rufusRandomJa"
rufusPetListJ3 << "rufusRandomJb"
rufusPetListJ3 << "rufusRandomJd"
rufusPetListJ3 << "rufusRandomJe"
rufusPetListJ3 << "rufusRandomJf"
rufusPetListJ3 << "rufusRandomJg"

rufusPetListJ4 = []
rufusPetListJ4 << "rufusRandomJa"
rufusPetListJ4 << "rufusRandomJb"
rufusPetListJ4 << "rufusRandomJc"
rufusPetListJ4 << "rufusRandomJe"
rufusPetListJ4 << "rufusRandomJf"
rufusPetListJ4 << "rufusRandomJg"

rufusPetListJ5 = []
rufusPetListJ5 << "rufusRandomJa"
rufusPetListJ5 << "rufusRandomJb"
rufusPetListJ5 << "rufusRandomJc"
rufusPetListJ5 << "rufusRandomJd"
rufusPetListJ5 << "rufusRandomJf"
rufusPetListJ5 << "rufusRandomJg"

rufusPetListJ6 = []
rufusPetListJ6 << "rufusRandomJa"
rufusPetListJ6 << "rufusRandomJb"
rufusPetListJ6 << "rufusRandomJc"
rufusPetListJ6 << "rufusRandomJd"
rufusPetListJ6 << "rufusRandomJe"
rufusPetListJ6 << "rufusRandomJg"

rufusPetListJ7 = []
rufusPetListJ7 << "rufusRandomJa"
rufusPetListJ7 << "rufusRandomJb"
rufusPetListJ7 << "rufusRandomJc"
rufusPetListJ7 << "rufusRandomJd"
rufusPetListJ7 << "rufusRandomJe"
rufusPetListJ7 << "rufusRandomJf"

//-----------------------------------------------------------------------------------------------------------------------
//Flag cleanup - if player exits room flags for random convo are stripped                                                
//-----------------------------------------------------------------------------------------------------------------------
myManager.onEnter(myRooms.BARTON_104) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z1_Rufus_Random_In")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z1_Rufus_Random_In")
	}
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z1_Rufus_Random_Out")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z1_Rufus_Random_Out")
	}
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z1_Rufus_Random_Out3")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z1_Rufus_Random_Out3")
	}
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z1_Rufus_Random_Out4")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z1_Rufus_Random_Out4")
	}
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z1_Rufus_Random_Out5")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z1_Rufus_Random_Out5")
	}
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z1_Rufus_Random_Out8")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z1_Rufus_Random_Out8")
	}
}