/*

onClickEnterBar = new Object()

barEnter = makeSwitch( "Bar_In", myRooms.BARTON_102, 1000, 500 )
barEnter.unlock()
barEnter.off()
barEnter.setRange( 250 )

def enterBar = { event ->
	synchronized( onClickEnterBar ) {
		barEnter.off()
		if( isPlayer( event.actor ) ) {
			event.actor.warp( "Bar_1", 980, 790 )
		}
	}
}

barEnter.whenOn( enterBar )
*/