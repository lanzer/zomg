import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}
def Candice = spawnNPC("Candice-VQS", myRooms.BARTON_203, 620, 780)
Candice.setRotation( 45 )
Candice.setDisplayName( "Candice" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Candice.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/candice_strip.png")
}
//---------------------------------------------------------
// I HATE RINA! This conversation occurs only if the player
// has accepted Rina as a friend in their first conver-    
// sation with Rina.                                       
//---------------------------------------------------------

def RinaHate = Candice.createConversation("RinaHate", true, "Z1RinaFriend")
RinaHate.setUrgent( true )

def Hate1 = [id:1]
Hate1.npctext = "Hi there! Isn't it a simply fabulous day?"
Hate1.playertext = "It is! You seem really cheery. Is there something in the water around here?"
Hate1.result = 2
RinaHate.addDialog(Hate1, Candice)

def Hate2 = [id:2]
Hate2.npctext = "No, silly. Barton Town's water is purified in the Reclamation Center toward Durem. It's tasty and sparkilicious!"
Hate2.playertext = "Sparkilicious, eh? You remind me a lot of Rina, the girl across the street."
Hate2.result = 3
RinaHate.addDialog(Hate2, Candice)

def Hate3 = [id:3]
Hate3.npctext = "..."
Hate3.playertext = "Was it something I said?"
Hate3.result = 4
RinaHate.addDialog(Hate3, Candice)

def Hate4 = [id:4]
Hate4.npctext = "That girl always imitates everything I do. I can't *stand* her!"
Hate4.playertext = "'She' imitates 'you'?"
Hate4.result = 5
RinaHate.addDialog(Hate4, Candice)

def Hate5 = [id:5]
Hate5.npctext = "Yes! And it drives me CRAZY! I was always the bubbliest and ditziest of us all, and now that red-haired squirrel-brain steals all my glory!"
Hate5.options = []
Hate5.options << [text:"Can't you try being even...ditzier? To sort of steal back your lost glory?", result: 6]
Hate5.options << [text:"Maybe you should dye your hair more blonde?", result: 9]
Hate5.options << [text:"Rina is my friend and I won't hear you speak badly of her!", result: 16]
Hate5.options << [text:"Oops! Wrong place, wrong time! Enjoy your feud. I'm out of here!", result: 25]
RinaHate.addDialog(Hate5, Candice)

def Hate6 = [id:6]
Hate6.npctext = "Ooooo! I never *thought* of that! Hee hee HEEE! I love it! You're such a wonderful smarty person! Thanks! I'll start right away!"
Hate6.playertext = "Okay. Lay it on me. Be ditzy!"
Hate6.result = 7
RinaHate.addDialog(Hate6, Candice)

def Hate7 = [id:7]
Hate7.npctext = "Hi! I'm Candice! Err, no...Candy! No, wait! Candi! Or better yet...KANDI!"
Hate7.playertext = "It sounds like you're on the right track. Nice job, Kandi!"
Hate7.result = 8
RinaHate.addDialog(Hate7, Candice)

def Hate8 = [id:8]
Hate8.npctext = "You're MY friend now, %p! Thanks so much for my new name!"
Hate8.playertext = "Okay. See ya for now!"
Hate8.flag = ["Z1CandiceFriend", "!Z1RinaFriend"]
Hate8.result = DONE
RinaHate.addDialog(Hate8, Candice)

def Hate9 = [id:9]
Hate9.npctext = "Oh, no way! My hair gets all frizzy when I go blonde!"
Hate9.playertext = "Y'know, I should have known you would have tried that already."
Hate9.result = 10
RinaHate.addDialog(Hate9, Candice)

def Hate10 = [id:10]
Hate10.npctext = "Of course I have! Blondes may have more fun, but my hair looked like I had just stuck a fork in a toaster!"
Hate10.playertext = "It sounds like you had some basis for comparison there, Kandi."
Hate10.result = 11
RinaHate.addDialog(Hate10, Candice)

def Hate11 = [id:11]
Hate11.npctext = "Well, that silly toast was all jammed down in that little slot. What I was *supposed* to use? A knife?"
Hate11.playertext = "..."
Hate11.result = 12
RinaHate.addDialog(Hate11, Candice)

def Hate12 = [id:12]
Hate12.npctext = "Oh. You're right. I should have used a rubber fork!"
Hate12.playertext = "Errr, right. A rubber fork. Perfect. You're brilliant."
Hate12.result = 13
RinaHate.addDialog(Hate12, Candice)

def Hate13 = [id:13]
Hate13.npctext = "Thanks, %p! You're such a good listener! Would you like to be my friend?"
Hate13.options = []
Hate13.options << [text:"Ooooo, I don't know, Candice. Rina is already my friend and I don't think you two get along very well.", result: 14]
Hate13.options << [text:"Sure! I can always use more friends!", result: 15]
RinaHate.addDialog(Hate13, Candice)

def Hate14 = [id:14]
Hate14.npctext = "Fine! If you want to be friends with that Rina...creature...then that's...just...FINE."
Hate14.playertext = "Well, at least you aren't bothered by it. Talk to you later."
Hate14.result = DONE
RinaHate.addDialog(Hate14, Candice)

def Hate15 = [id:15]
Hate15.npctext = "hee hee HEE! That makes me so happy, %p! Thanks! Come by and say 'hi' anytime!"
Hate15.flag = [ "Z1CandiceFriend", "!Z1RinaFriend" ]
Hate15.exec = { event -> 
	def hasFlag = event.player.hasQuestFlag(GLOBAL,"Z1CandiceFriend")
	println "TESTING Z1CandiceFriend = ${hasFlag}"
}
Hate15.result = DONE
RinaHate.addDialog(Hate15, Candice)

def Hate16 = [id:16]
Hate16.npctext = "Ohhhhh! You just don't understand! I can't STAND how she's the one that makes all the town announcements and SHE'S the one that gets to run the pretty flower shop. Argh!"
Hate16.playertext = "Hmmm. Candice, I just had a thought..."
Hate16.result = 17
RinaHate.addDialog(Hate16, Candice)

def Hate17 = [id:17]
Hate17.npctext = "A thought? What's that?"
Hate17.playertext = "Wha...? Oh. You mean 'what was my thought'?"
Hate17.result = 18
RinaHate.addDialog(Hate17, Candice)

def Hate18 = [id:18]
Hate18.npctext = "Oh! THOUGHT! I thought you meant THOT, and I didn't know what that was..."
Hate18.playertext = "...anyway...oh, nevermind."
Hate18.result = 19
RinaHate.addDialog(Hate18, Candice)

def Hate19 = [id:19]
Hate19.npctext = "Please tell me!"
Hate19.playertext = "All right. Look, your name is Candice. You're standing next to a big shop with a candy heart on top of it. Does that give you any ideas?"
Hate19.result = 20
RinaHate.addDialog(Hate19, Candice)

def Hate20 = [id:20]
Hate20.npctext = "YES!"
Hate20.playertext = "Yes? It does?!? What does it make you think of, Candice?"
Hate20.result = 21
RinaHate.addDialog(Hate20, Candice)

def Hate21 = [id:21]
Hate21.npctext = "...ummm...pink?"
Hate21.playertext = "Pink, yes. But even better, it's a pink shop with a candy heart on it...your name is Candy...how about you buy the store and open a Candy Shop?"
Hate21.result = 22
RinaHate.addDialog(Hate21, Candice)

def Hate22 = [id:22]
Hate22.npctext = "You're so silly! Me? Buy a shop? Well, maybe. You're nice to think I could do it. I like you!! I'm going to call you my friend!"
Hate22.options = []
Hate22.options << [text:"Well, don't tell Rina. I was her friend first and I want to stay that way.", result: 23]
Hate22.options << [text:"Sure, Candy! I like having friends as sweet as you!", result: 24]
RinaHate.addDialog(Hate22, Candice)

def Hate23 = [id:23]
Hate23.npctext = "Oh, poo! Fine. Rina can have you, but when you get mad at her, you come say 'hi'. I'm sure I like you better than she does anyway."
Hate23.playertext = "Will do, Candice! Good luck with the shop idea!"
Hate23.result = DONE
RinaHate.addDialog(Hate23, Candice)

def Hate24 = [id:24]
Hate24.npctext = "hee hee HEE! Rina will be so jealous! I mean, thanks, %p! We'll be GREAT friends!"
Hate24.playertext = "Okay, Candice! Talk to you soon!"
Hate24.flag = ["!Z1RinaFriend", "Z1CandiceFriend"]
RinaHate.addDialog(Hate24, Candice)

def Hate25 = [id:25]
Hate25.npctext = "Humph. Okay. Bye now."
Hate25.result = DONE
RinaHate.addDialog(Hate25, Candice)

//---------------------------------------------------------
// AFTER REJECTING CANDACE AS A FRIEND                     
// (You accepted Rina as your BFF earlier)                 
//---------------------------------------------------------

def RinaBFF = Candice.createConversation("RinaBFF", true, "Z1RinaFriend2", "!Z1_Candice_NeverFriends")
RinaBFF.setUrgent( false )

def MoreHate1 = [id:1]
MoreHate1.npctext = "Go away!"
MoreHate1.playertext = "Ah, c'mon, Candice! I know you don't like Rina, but can't we be friends still?"
MoreHate1.result = 2
RinaBFF.addDialog(MoreHate1, Candice)

def MoreHate2 = [id:2]
MoreHate2.npctext = "You fibbed to me once about us being friends, %p. I won't be fooled again. Now go away. I don't want to talk to you anymore."
MoreHate2.flag = "Z1_Candice_NeverFriends"
MoreHate2.result = DONE // CANDICE WILL NEVER BE YOUR FRIEND AGAIN
RinaBFF.addDialog(MoreHate2, Candice)

//---------------------------------------------------------
// AFTER ACCEPTING "BURY THE HATCHET" FROM RINA            
// (Interim script until that quest is done with Rina)     
//---------------------------------------------------------

def NeutralCandice = Candice.createConversation("NeutralCandice", true, "Z1BothFriends", "!QuestStarted_45", "!QuestCompleted_45")
NeutralCandice.setUrgent( false )

def NeutralLoop1 = [id:1]
NeutralLoop1.npctext = "Hi there, %p!"
NeutralLoop1.playertext = "Hi, Candice! How's my good friend doing today?"
NeutralLoop1.result = 2
NeutralCandice.addDialog(NeutralLoop1, Candice)

def NeutralLoop2 = [id:2]
NeutralLoop2.npctext = "Splendifabulouserrific!"
NeutralLoop2.playertext = "You don't say? I should have guessed. Well, good for you!"
NeutralLoop2.result = 3
NeutralCandice.addDialog(NeutralLoop2, Candice)

def NeutralLoop3 = [id:3]
NeutralLoop3.npctext = "Thanks! Thanks for stopping by, %p!"
NeutralLoop3.result = DONE
NeutralCandice.addDialog(NeutralLoop3, Candice)

//---------------------------------------------------------
// AFTER REJECTING RINA AS A FRIEND                        
// (Candice is your only friend now! Rina hates you!!)     
// (PLASTIC BOUQUET Quest START)                        
//---------------------------------------------------------

def CandiceBFF = Candice.createConversation("CandiceBFF", true, "Z1RinaEnemy", "!QuestStarted_43", "!QuestCompleted_43" )
CandiceBFF.setUrgent( true )

def BFF1 = [id:1]
BFF1.npctext = "So what did Rina say? I saw you go over there..."
BFF1.playertext = "Let's just say that I chose to be YOUR friend, not hers."
BFF1.result = 2
CandiceBFF.addDialog(BFF1, Candice)

def BFF2 = [id:2]
BFF2.npctext = "You did?!? REALLY????"
BFF2.playertext = "I sure did."
BFF2.result = 3
CandiceBFF.addDialog(BFF2, Candice)

def BFF3 = [id:3]
BFF3.npctext = "YAY! %p is MY friend now! Yay us!"
BFF3.playertext = "hehehe. I'm glad you're happy, Candice. Really glad."
BFF3.result = 4
CandiceBFF.addDialog(BFF3, Candice)

def BFF4 = [id:4]
BFF4.npctext = "Wow. Just wow. I'm soooo used to her stealing things that I like. We should commem...commens...comemurate..."
BFF4.playertext = "Commemorate?"
BFF4.result = 5
CandiceBFF.addDialog(BFF4, Candice)

def BFF5 = [id:5]
BFF5.npctext = "Yes!!! We should com-mem-or-ate our friendship! Would you do me a teensy, weensy favor?"
BFF5.options = []
BFF5.options << [text:"Sure, Candice! Why not? What do you need?", result: 6]
BFF5.options << [text:"Not right now, Candice. Maybe later, okay?", result: DONE]
CandiceBFF.addDialog(BFF5, Candice)

def BFF6 = [id:6]
BFF6.npctext = "What I'd *really* like is to collect enough little pieces of brightly colored plastic that I could assemble a bouquet of fake flowers, so that I never have to go see that vixen over there again!"
BFF6.playertext = "Ooookay. Sure! See you soon!"
BFF6.result = DONE
BFF6.quest = 43  //This is the start of the PLASTIC BOUQUET quest
CandiceBFF.addDialog(BFF6, Candice)

//---------------------------------------------------------
// INTERIM FOR PLASTIC BOUQUET                             
//---------------------------------------------------------

def PaperInterim = Candice.createConversation("PaperInterim", true, "QuestStarted_43:2" )
PaperInterim.setUrgent( false )

def Paper1 = [id:1]
Paper1.npctext = "Hi there, %p! Have you found neat little plastic pieces for me yet?"
Paper1.playertext = "I don't think there's quite enough of the colors you need yet. I'm still searching."
Paper1.result = 2
PaperInterim.addDialog(Paper1, Candice)

def Paper2 = [id:2]
Paper2.npctext = "Thats okay, %p. There's no deadline. Take your time!"
Paper2.playertext = "Thanks, Candice. See you soon!"
Paper2.result = DONE
PaperInterim.addDialog(Paper2, Candice)

//---------------------------------------------------------
// SUCCESS FOR PLASTIC BOUQUET QUEST                       
//---------------------------------------------------------

def PaperSuccess = Candice.createConversation("PaperSuccess", true, "QuestStarted_43:3" )
PaperSuccess.setUrgent( true )

def PaperSucc1 = [id:1]
PaperSucc1.npctext = "Did you find some? Did you?"
PaperSucc1.playertext = "I sure did! A whole assortment of stuff."
PaperSucc1.exec = { event ->
	runOnDeduct( event.player, 100385, 10, paperhappy, papersad ) // Removes two plastic from player's inventory
}
PaperSuccess.addDialog(PaperSucc1, Candice)

//Happy about the Plastic Pieces
def happyPaper = Candice.createConversation( "happyPaper", true, "QuestStarted_43:3", "Z1PlasticPiecesPresent" )
happyPaper.setUrgent( true )

def happyPaper1 = [id:1]
happyPaper1.npctext = "Ooo! This is all *perfect*! Thank you ever so much, %p! This is perfect! My new friend is helping me avoid my old enemy! I'm so happy!"
happyPaper1.playertext = "You're welcome, Candice. Have fun!"
happyPaper1.result = 2
happyPaper.addDialog( happyPaper1, Candice )

def happyPaper2 = [id:2]
happyPaper2.npctext = "I will! Come back to talk ANYtime, %p!"
happyPaper2.flag = "Z01CandiceBFF"
happyPaper2.quest = 43 // Send the completion for Plastic Bouquet quest
happyPaper2.exec = { event ->
	event.player.centerPrint( "You are awarded a Cherry Ring Pop!" )
	event.player.removeMiniMapQuestActorName( "Candice-VQS" )
}
happyPaper2.result = DONE
happyPaper.addDialog(happyPaper2, Candice)

//Sad about the Plastic Pieces
def sadPaper = Candice.createConversation( "sadPaper", true, "QuestStarted_43:3", "Z1PlasticPiecesNotPresent", "!QuestCompleted_43" )
sadPaper.setUrgent( true )

def sadPaper1 = [id:1]
sadPaper1.npctext = "%p, where's the plastic pieces you promised me?"
sadPaper1.playertext = "Oh, drat. I must have gotten rid of them somehow!"
sadPaper1.result = 2
sadPaper.addDialog( sadPaper1, Candice )

def sadPaper2 = [id:2]
sadPaper2.npctext = "I can't hardly make plastic bouquets without any pieces. Go find me the brightly colored plastic I need, okay?"
sadPaper2.playertext = "You bet, Candice. I'm on my way!"
sadPaper2.flag = "!Z1PlasticPiecesNotPresent" //unset the flag so it defaults back to before the feather check occurs
sadPaper2.result = DONE
sadPaper.addDialog( sadPaper2, Candice )

//Closures to check to ensure that the player actually still has five pink flamingo feathers at the time of the transaction
paperhappy = { event ->
	event.player.setQuestFlag( GLOBAL, "Z1PlasticPiecesPresent" )
	Candice.pushDialog( event.player, "happyPaper" )
}

papersad = { event ->
	event.player.setQuestFlag( GLOBAL, "Z1PlasticPiecesNotPresent" )
	Candice.pushDialog( event.player, "sadPaper" )
}

//---------------------------------------------------------
// CANDY'S BOA QUEST (Step 2)                              
//---------------------------------------------------------

def RinaBoa = Candice.createConversation("RinaBoa", true, "QuestStarted_46:2") //Candy's Boa quest
RinaBoa.setUrgent( true )

def Boa1 = [id:1]
Boa1.npctext = "Hello, %p! How have you been?"
Boa1.playertext = "Great! Hey! A friend of mine asked me to bring you a present!"
Boa1.result = 2
RinaBoa.addDialog(Boa1, Candice)

def Boa2 = [id:2]
Boa2.npctext = "A present? For me?!?"
Boa2.playertext = "Yup. It's a handmade boa, woven from pink flamingo feathers!"
Boa2.result = 3
RinaBoa.addDialog(Boa2, Candice)

def Boa3 = [id:3]
Boa3.npctext = "Flamingo feathers! Pink! It's BEAUTIFUL!!"
Boa3.playertext = "My friend will be very happy that you like it. She wove it together herself just to make you happy."
Boa3.result = 4
RinaBoa.addDialog(Boa3, Candice)

def Boa4 = [id:4]
Boa4.npctext = "She did? Who is she? Tell me!"
Boa4.playertext = "Okay, but brace yourself!"
Boa4.result = 5
RinaBoa.addDialog(Boa4, Candice)

def Boa5 = [id:5]
Boa5.npctext = "...why?"
Boa5.playertext = "Because the person that made this for you, the person that wants to be your friend and make you happy, is...Rina."
Boa5.result = 6
RinaBoa.addDialog(Boa5, Candice)

def Boa6 = [id:6]
Boa6.npctext = "..."
Boa6.playertext = "Candice? Are you okay?"
Boa6.result = 7
RinaBoa.addDialog(Boa6, Candice)

def Boa7 = [id:7]
Boa7.npctext = "Yesss. I'm just really, really surprised. Rina? Really?!?"
Boa7.playertext = "Really. She thought better about your feud and asked me to bring this boa over to you as a peace present."
Boa7.result = 8
RinaBoa.addDialog(Boa7, Candice)

def Boa8 = [id:8]
Boa8.npctext = "It's really, really nice. Oh. Rina's actually pretty nice, isn't she?"
Boa8.playertext = "Yeah. I think you two just didn't mix well at first because you're so much alike. In fact, I think you'd probably be best friends if you gave her a chance."
Boa8.result = 9
RinaBoa.addDialog(Boa8, Candice)

def Boa9 = [id:9]
Boa9.npctext = "Oh, %p! This is the best present ever. A feather boa AND a new friend. Thank you, thank you, thank YOU!"
Boa9.playertext = "I'm glad to help bring you two together. I can just imagine the conversations you're going to have now."
Boa9.result = 10
RinaBoa.addDialog(Boa9, Candice)

def Boa10 = [id:10]
Boa10.npctext = "Yes! They'll be splendifertastic!"
Boa10.playertext = "Hehehe. I'm sure they will be. Have fun, Candice!"
Boa10.result = 11
RinaBoa.addDialog(Boa10, Candice)

def Boa11 = [id:11]
Boa11.npctext = "I will! You too, %p!"
Boa11.flag = "Z01CandiceBFF"
Boa11.quest = 46 //Update that Candice has received the boa (step 2 completed)
Boa11.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Candice-VQS" )
	event.player.addMiniMapQuestActorName( "Rina-VQS" )
}
Boa11.result = DONE
RinaBoa.addDialog(Boa11, Candice)

//---------------------------------------------------------
// CANDICE'S AFTERMATH CONVERSATION                        
// (Occurs after the Plastic Bouquet quest or after giving 
// her the feather boa from Rina)                          
//---------------------------------------------------------

def CandiceAftermath = Candice.createConversation( "CandiceAftermath", true, "Z01CandiceBFF" )
CandiceAftermath.setUrgent( false )

def After1 = [id:1]
After1.npctext = "Hi, %p! Are you having a great day?"
After1.playertext = "I sure am, Candice! How's your day going?"
After1.result = 2
CandiceAftermath.addDialog( After1, Candice )

def After2 = [id:2]
After2.npctext = "Light and fluffy and pink. Well, at least it's pink in my mind!"
After2.playertext = "I'm sure it is, Candice. I'm sure it is."
After2.result = 3
CandiceAftermath.addDialog( After2, Candice )

def After3 = [id:3]
After3.npctext = "Stay happy, %p! And be safe out there!"
After3.playertext = "Will do, Candice. Thanks!"
After3.result = DONE
CandiceAftermath.addDialog( After3, Candice )
