import com.gaiaonline.mmo.battle.script.*;


//---------------------------------------------------------
// Switch for the wheel                                    
//---------------------------------------------------------
dailyChanceState = 0
dailyChanceSet = [] as Set

dailyChanceWheel = makeGeneric("dailyChanceWheel", myRooms.BARTON_303, 120, 330)
dailyChanceWheel.setStateful()
dailyChanceWheel.setUsable(true)
dailyChanceWheel.setRange(200)
dailyChanceWheel.setMouseoverText("Daily Chance Wheel")

dailyChanceWheel.onUse { event ->
	//println "^^^ Player using switch = ${event.player} ^^^^"
	//dailyChanceWheel.off()
	if(dailyChanceWheel.getActorState() == 0) {
		dailyChanceWheel.setActorState(1)
		
		dailyChanceSet.clear()
		dailyChanceSet << event.player
		
		myManager.schedule(2) { dailyChanceReward(); dailyChanceWheel.setActorState(0) }
	} 
	if(dailyChanceWheel.getActorState() == 1) {
		dailyChanceSet << event.player
	}
}

def dailyChanceReward() {
	//println "^^^^ Running Daily Chance Reward for ${dailyChanceSet} ^^^^"
	dailyChanceState = 0
	dailyChanceSet.clone().each {
		runDailyChanceCheck(it)
	}
}

def runDailyChanceCheck(player) {
	checkDailyChance(player, 220, { result -> 
		if( result == false ) {
			player.centerPrint("The Wheel of Chance has given you a reward!")
			runDailyChance(player, 220)
		} else {
			player.centerPrint( "You have already claimed your reward today." )
		}
	})
}

//dailyChanceWheel.whenOn(dailyChanceOn)
