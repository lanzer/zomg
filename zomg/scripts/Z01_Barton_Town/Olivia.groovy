import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

def Olivia = spawnNPC("Olivia-VQS", myRooms.BARTON_302, 100, 550)
Olivia.setRotation( 45 )
Olivia.setDisplayName( "Olivia" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Olivia.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/olivia_strip.png")
}
//Z01BridgeToOlivia

//---------------------------------------------------------
// Convo From Clara (from TrainStation)                    
//---------------------------------------------------------

def fromClara = Olivia.createConversation( "fromClara", true, "Z01BridgeToOlivia" )

def train1 = [id:1]
train1.npctext = "Good day!"
train1.playertext = "Hi there. Clara sent me over here to tell you..."
train1.result = DONE
train1.flag = "!Z01BridgeToOlivia"
train1.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Olivia-VQS" )
	Olivia.pushDialog( event.player, "MomConvo" )
}	
fromClara.addDialog( train1, Olivia ) 

//---------------------------------------------------------
// COOKIE QUEST START (Initial Conversation)               
//---------------------------------------------------------
def MomConvo = Olivia.createConversation("MomConvo", true, "!Z01BridgeToOlivia", "!QuestStarted_34", "!QuestCompleted_34")

def dialog1 = [id:1]
dialog1.npctext = "Things are great! Well, other than the Animated, of course. Have you come by to play with Leon?"
dialog1.playertext = "What? No. I..."
dialog1.result = 2
MomConvo.addDialog(dialog1, Olivia)

def dialog2 = [id:2]
dialog2.npctext = "That boy and his guard friends have gone to 'save the entire town', as they put it. It's so cute when they get excited about something."
dialog2.playertext = "I was just wondering if..."
dialog2.result = 3
MomConvo.addDialog(dialog2, Olivia)

def dialog3 = [id:3]
dialog3.npctext = "Personally, I don't see what all the fuss is about. He puts on his big suit and goes trodding off under the sun every day and then comes home and complains that it's too hot in there."
dialog3.playertext = "Is there someone else I can talk to...?"
dialog3.result = 4
MomConvo.addDialog(dialog3, Olivia)

def dialog4 = [id:4]
dialog4.npctext = "He's a good boy though. I told him 'Young man, if that suit is so hot, why don't you use it as a smoker and smoke me up some sausages?' and wouldn't you know it, he went and did it! Started bringing me big bags of smoked sausages every week. Though sometimes they would have strange cat hair all over them..."
dialog4.playertext = "HELLO?!?"
dialog4.result = 5
MomConvo.addDialog(dialog4, Olivia)

def dialog5 = [id:5]
dialog5.npctext = "I'm sorry, sweetie. What do you need?"
dialog5.options = []
dialog5.options << [text: "Do I smell...cookies?", result: 6]
dialog5.options << [text: "Nothing. Thanks.", result: 8]
MomConvo.addDialog(dialog5, Olivia)

def dialog6 = [id:6]
dialog6.npctext = "Why yes you do! Have one! They're shaped like little swords. I was baking them mostly for Leon and the boys, because the guild hall smells like a locker room. I was hoping the smell of freshly baked cookies would overpower the musk."
dialog6.playertext = "Did it work?"
dialog6.result = 7
MomConvo.addDialog(dialog6, Olivia)

def dialog7 = [id:7]
dialog7.npctext = "...No. Not really. But the cookies did come out nice and substantial. Would you like to help me take them to the guards?"
dialog7.options = []
dialog7.options << [text: "Sure. I'd be glad to help. Which guards do you want me to take cookies to?", result: 9]
dialog7.options << [text: "I'll take a few for myself, thank you, but I really have to run right now. Sorry.", result: DONE]
MomConvo.addDialog(dialog7, Olivia)

def dialog8 = [id:8]
dialog8.npctext = "Okay, dear. Have a nice day!"
dialog8.result = DONE
MomConvo.addDialog(dialog8, Olivia)

def dialog9 = [id:9]
dialog9.npctext = "Well, I haven't finished enough for ALL the guards just yet, so perhaps you could just take these to John, Martha, and William for now?"
dialog9.playertext = "Whoa. These things are heavy! A few of these cookies are going to go a long way..."
dialog9.result = 10
MomConvo.addDialog(dialog9, Olivia)

def dialog10 = [id:10]
dialog10.npctext = "That's a nice thought, dearie. Now hurry, while the cookies are still warm, they seem to get much heavier when cold. John is at North Gate, while Martha is at West Gate and William is down by the South Gate with Clara."
dialog10.playertext = "Got it! John at North Gate, Martha at West, and William at the South Gate. See you later!"
dialog10.quest = 34 // Start the cookie quest
dialog10.exec = { event ->
	event.player.addMiniMapQuestActorName( "BFG-William" )
	event.player.addMiniMapQuestActorName( "BFG-John" )
	event.player.addMiniMapQuestActorName( "BFG-Martha" )
	if( event.player.isOnQuest( 287 ) ) {
		event.player.removeMiniMapQuestActorName( "Olivia-VQS" )
	}
}
dialog10.result = DONE
MomConvo.addDialog(dialog10, Olivia)

//---------------------------------------------------------
// COOKIE QUEST INTERIM                                                 
//---------------------------------------------------------
//Now a version AFTER the quest is accepted but BEFORE all the cookies are delivered.
def MomConvoInterim = Olivia.createConversation("MomConvoInterim", true, "QuestStarted_34", "!QuestStarted_34:3")

def CookieInterim1 = [id:1]
CookieInterim1.npctext = "That was fast! Have you delivered the cookies already?"
CookieInterim1.playertext = "No...not yet. Which guards were they supposed to go to again?"
CookieInterim1.result = 2
MomConvoInterim.addDialog(CookieInterim1, Olivia)

def CookieInterim2 = [id:2]
CookieInterim2.npctext = "John is at the North Gate, Martha is at the West Gate, and William is down at the South Gate working with Clara. Hurry along now!"
CookieInterim2.playertext = "Gotcha! Thanks!"
CookieInterim2.result = DONE
MomConvoInterim.addDialog(CookieInterim2, Olivia)

//---------------------------------------------------------
// COOKIE QUEST THANK YOU (Completion)                     
//                                                         
// NOTE: This quest does NOT reward the player through the 
// Quest Admin panel because it varies the rewards based   
// on what the player said when the quest is completed.    
//---------------------------------------------------------
//A version to set flags when all cookies are delivered
def MomQuestCompleted = Olivia.createConversation("MomQuestCompleted", true, "QuestStarted_34:3", "!Z01CookieQuestCompleted")

def dialog11 = [id:11]
dialog11.npctext = "Oh good! You've delivered all the cookies! Thank you so much! Did the guards enjoy their cookies?"
dialog11.options = []
dialog11.options << [text: "Good lord, no! Were you trying to poison them?!?", result: 12]
dialog11.options << [text: "Errr...yes. They seemed very impressed at the...solidity of them all.", result: 13]
dialog11.options << [text: "Absolutely! They can't wait for more!", result: 15]
MomQuestCompleted.addDialog(dialog11, Olivia)

def dialog12 = [id:12]
dialog12.npctext = "What? Poison? Why, I never! I mean, okay, maybe I shouldn't have added the silicone filling so they kept their shape, but POISON? That implies I wanted to HURT someone!"
dialog12.playertext = "Oops. Maybe I shouldn't have been so blunt..."
//Player receives no reward for being rude.
dialog12.result = DONE
dialog12.quest = 34 //push completion step for Olivia's Cookie Quest
dialog12.flag = ["Z01CookieQuestCompleted"]
dialog12.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Olivia-VQS" )
	if( !event.player.isOnQuest( 82 ) ) {
		Olivia.pushDialog( event.player, "LunchStart" )
	}
}
MomQuestCompleted.addDialog(dialog12, Olivia)

def dialog13 = [id:13]
dialog13.npctext = "As well they should be. It's the trick of double-boiling the molasses until it has the consistency of a fine tar that really does the trick. That and adding those wonderfully flavorful chunks of pork lard. There's nothing that doesn't taste better once it has that hint of bacon in it."
dialog13.playertext = "Oh...my. That sounds...filling. I'm sorry I didn't snag a few for myself."
dialog13.result = 14
MomQuestCompleted.addDialog(dialog13, Olivia)

def dialog14 = [id:14]
dialog14.npctext = "Well, we can solve that for you tomorrow, but not today. Come back sometime and I'll save a few out for you! Meanwhile, here's something for your trouble since you hauled those heavy cookies all over Town for me."
dialog14.playertext = "Lucky me!"
dialog14.result = DONE
dialog14.quest = 34 //push completion step for Olivia's Cookie Quest
dialog14.flag = ["Z01CookieQuestCompleted"]
dialog14.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Olivia-VQS" )
	if( !event.player.isOnQuest( 82 ) ) {
		Olivia.pushDialog( event.player, "LunchStart" )
	}
}
MomQuestCompleted.addDialog(dialog14, Olivia)

def dialog15 = [id:15]
dialog15.npctext = "Oh! Oh! OH! The guards don't usually like my cooking! Marvelous! I've finally found a recipe that I can bake!"
dialog15.playertext = "Yes. That is...err...wow. Maybe you should sell them?"
dialog15.result = 16
MomQuestCompleted.addDialog(dialog15, Olivia)

def dialog16 = [id:16]
dialog16.npctext = "What a wonderful idea! I'll talk to Leon as soon as he gets home tonight. Maybe we can start 'Olivia's Sweet Shoppe' soon. It'll be a sensation!"
dialog16.playertext = "It'll definitely be talked about. Definitely. You go!"
dialog16.result = 17
MomQuestCompleted.addDialog(dialog16, Olivia)

def dialog17 = [id:17]
dialog17.npctext = "I'm just all a'flutter! Let me get you something for your trouble."
dialog17.playertext = "Oh no. You shouldn't. Or rather...thanks. I don't mind if you do."
dialog17.result = DONE
dialog17.flag = ["Z01CookieQuestCompleted"]
dialog17.quest = 34 //push completion step for Olivia's Cookie Quest
dialog17.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Olivia-VQS" )
	if( !event.player.isOnQuest( 82 ) ) {
		Olivia.pushDialog( event.player, "LunchStart" )
	}
}
MomQuestCompleted.addDialog(dialog17, Olivia)

//---------------------------------------------------------
// LUNCH QUEST (Start!)                                       
//---------------------------------------------------------
//This becomes available after the player has completed the Cookie Quest

def LunchStart = Olivia.createConversation("LunchStart", true, "Z01CookieQuestCompleted", "!QuestStarted_54", "!QuestCompleted_54", "!QuestStarted_82")

def LunchStart1 = [id:1]
LunchStart1.npctext = "I've got something else you could help me with...if you're interested..."
LunchStart1.options = []
LunchStart1.options << [text:"Sure! What do you need?", result: 2]
LunchStart1.options << [text:"I've got a lot going on right now. Will you be okay without me?", result: 5]
LunchStart.addDialog(LunchStart1, Olivia)

def LunchStart2 = [id:2]
LunchStart2.npctext = "Oh, thank you, dear! I packed Leon's lunch today, but with all the craziness going on, he headed down south without taking his lunch pail. Would you mind running it down to him?"
LunchStart2.playertext = "To Leon? Isn't he outside of Barton Town right now?"
LunchStart2.result = 3
LunchStart.addDialog(LunchStart2, Olivia)

def LunchStart3 = [id:3]
LunchStart3.npctext = "Yes, that's right! He tends to wander around, so I recommend that you ask his second-in-command, Clara, about where he's patrolling. Clara should be over at the South Gate."
LunchStart3.playertext = "Okay. I'll go talk to Clara then. Thanks!"
LunchStart3.result = 4
LunchStart.addDialog(LunchStart3, Olivia)

def LunchStart4 = [id:4]
LunchStart4.npctext = "Thank you so much. You're a doll."
LunchStart4.quest = 54 //Start Leon's Lunch Pail quest
LunchStart4.result = DONE
LunchStart4.exec = { event ->
	event.player.addMiniMapQuestActorName( "BFG-Clara" )
}
LunchStart.addDialog(LunchStart4, Olivia)

def LunchStart5 = [id:5]
LunchStart5.npctext = "Oh, I've muddled on by myself for years. It won't be a problem. Thanks anyway!"
LunchStart5.result = DONE
LunchStart.addDialog(LunchStart5, Olivia)

//---------------------------------------------------------
// LUNCH QUEST INTERIM                                     
//---------------------------------------------------------
// After the lunch pail quest has started, but before Leon actually has it in his hands.

def LunchInterim = Olivia.createConversation("LunchInterim", true, "QuestStarted_54", "!QuestStarted_54:4")

def Interim1 = [id:1]
Interim1.npctext = "Hi there, sweetie. Did you take the lunch pail to Leon yet?"
Interim1.playertext = "Nope. Not yet. But I'm on my way!"
Interim1.result = 2
LunchInterim.addDialog(Interim1, Olivia)

def Interim2 = [id:2]
Interim2.npctext = "That's good, dear. Leon's a big boy and he needs his strength. Thank you so much!"
Interim2.result = DONE
LunchInterim.addDialog(Interim2, Olivia)


//---------------------------------------------------------
// LUNCH QUEST COMPLETION                                  
//---------------------------------------------------------
//Upon visiting Olivia and returning the empty lunch pail.

def LunchSuccess = Olivia.createConversation("LunchSuccess", true, "QuestStarted_54:4")

def Success1 = [id:1]
Success1.npctext = "Oh! You're back already, %p! Did you find Leon?"
Success1.playertext = "I sure did! He really enjoyed the...food...you sent him."
Success1.result = 2
LunchSuccess.addDialog(Success1, Olivia)

def Success2 = [id:2]
Success2.npctext = "I'm so glad! How is he doing?"
Success2.playertext = "Your son is strong and able. It's dangerous out there, but he's doing a fantastic job of protecting us all against the Animated."
Success2.result = 3
LunchSuccess.addDialog(Success2, Olivia)

def Success3 = [id:3]
Success3.npctext = "Oh, I'm SO glad! Thank you, %p! I knew that you were the right kind of person to trust! Let me see here, I have something here for you..."
Success3.playertext = "Thank you, Olivia! It was nothing! Enjoy your day, and I'll see you again soon!"
Success3.quest = 54 //push the completion step for Leon's Lunch Pail quest
Success3.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Olivia-VQS" )
}
Success3.result = DONE
LunchSuccess.addDialog(Success3, Olivia)

//---------------------------------------------------------
// LOOPING AFTERMATH CONVO                                 
//---------------------------------------------------------

def OliviaAfterLoop = Olivia.createConversation("OliviaAfterLoop", true, "QuestCompleted_54", "!QuestStarted_84")

def After1 = [id:1]
After1.npctext = "Hello again, %p! Would you like me to cook you up something special after all the good deeds you've done for me?"
After1.playertext = "Ack! err, urk. Umm, I mean...oh no. Please don't put yourself out on my account, Olivia. I'm not the slightest bit hungry at the moment."
After1.result = 2
OliviaAfterLoop.addDialog(After1, Olivia)

def After2 = [id:2]
After2.npctext = "Oh it would *never* be a problem for you, dearie. Just let me know when you want something and I'll cook it right up!"
After2.playertext = "Oooookay. Thanks! I'm not sure I'll ever be hungry again, but if that happens, I'll come right back to you."
After2.result = 3
OliviaAfterLoop.addDialog(After2, Olivia)

def After3 = [id:3]
After3.npctext = "'Never hungry again'. My word, you do have a talent for exaggeration. Well, take care then, %p. See you soon!"
After3.playertext = "You too, Olivia. 'Bye for now!"
After3.result = DONE
OliviaAfterLoop.addDialog(After3, Olivia)