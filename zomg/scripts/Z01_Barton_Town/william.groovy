import com.gaiaonline.mmo.battle.script.*;

//SET UP TRIGGER ZONES FOR LUCKY COIN QUEST

cityFountainTrigger = "cityFountainTrigger"
myRooms.BARTON_103.createTriggerZone(cityFountainTrigger, 230, 400, 850, 800)

northWellTrigger = "northWellTrigger"
myRooms.BARTON_2.createTriggerZone(northWellTrigger, 500, 300, 900, 590)

//CREATE LOGIC FOR TRIGGER ZONES (LUCKY COIN)

//If enter the City Fountain trigger zone, then push an update for the quest
myManager.onTriggerIn(myRooms.BARTON_103, cityFountainTrigger) { event ->
	if( isPlayer(event.actor) && event.actor.isOnQuest(53, 2) && !event.actor.hasQuestFlag( GLOBAL, "Z1BeenToCityFountain" ) ) { 
		if ( event.actor.hasQuestFlag(GLOBAL, "Z1BeenToNorthWell") ) { 
			event.actor.say( "Woohoo! I found the lucky coin!" )
			event.actor.removeMiniMapQuestLocation( "City_Fountain" )
			event.actor.addMiniMapQuestActorName( "BFG-William" )		
			event.actor.updateQuest(53, "BFG-William" )
		} else {
			event.actor.say ("Hmmm...no coin here. Must be the other spot.")
			event.actor.removeMiniMapQuestLocation( "City_Fountain" )
			event.actor.setQuestFlag (GLOBAL, "Z1BeenToCityFountain")
		}
	}
}

//If enter the North Well trigger zone, then push an update for the quest
myManager.onTriggerIn(myRooms.BARTON_2, northWellTrigger) { event ->
	if( isPlayer(event.actor) && event.actor.isOnQuest(53, 2) && !event.actor.hasQuestFlag( GLOBAL, "Z1BeenToNorthWell" ) ) {
		if ( event.actor.hasQuestFlag(GLOBAL, "Z1BeenToCityFountain") ) {
			event.actor.say( "Woohoo! I found the lucky coin!" )
			event.actor.removeMiniMapQuestLocation( "Town_Well" )
			event.actor.addMiniMapQuestActorName( "BFG-William" )		
			event.actor.updateQuest(53, "BFG-William" )
		} else {
			event.actor.say ("Hmmm...no coin here. Must be the other spot.")
			event.actor.removeMiniMapQuestLocation( "Town_Well" )
			event.actor.setQuestFlag (GLOBAL, "Z1BeenToNorthWell")
		}
	}
}


def William = spawnNPC("BFG-William", myRooms.BARTON_303, 600, 600)
William.setDisplayName( "William" )
William.setRotation( 135 )

//--------------------------------------------------------------------
// DEFAULT CONVERSATION                                               
//--------------------------------------------------------------------
def WilliamNeutral = William.createConversation("WilliamNeutral", true, "!QuestStarted_34", "!QuestStarted_53", "!QuestCompleted_53")

def Neutral1 = [id:1]
Neutral1.playertext = "Hello there. You seem quite...penguin-ish..."
Neutral1.result = 2
WilliamNeutral.addDialog(Neutral1, William)

def Neutral2 = [id:2]
Neutral2.npctext = "Beep! beep beepity beep 'William'!"
Neutral2.options = []
Neutral2.options << [text: "Okay. I'm not going to pretend to speak penguin, but I'm betting that you just told me your name is William.", result: 3]
Neutral2.options << [text: "What's that? Your name is Willy? And Timmy's in the well, you say? Do you need help?", result: 5]
Neutral2.options << [text: "Uhhh, nevermind...penguin boy.", result: DONE]
WilliamNeutral.addDialog(Neutral2, William)

def Neutral3 = [id:3]
Neutral3.npctext = "Beep! beep beeeeep BEEP beep Clara beepbeep."
Neutral3.playertext = "Wait a second! I heard 'Clara' in there. That's the name of the guard across the street, right?"
Neutral3.result = 4
WilliamNeutral.addDialog(Neutral3, William)

def Neutral4 = [id:4]
Neutral4.npctext = "BEEP!"
Neutral4.playertext = "Oooookay. I'll just stroll over there and say hi to 'Clara' then. Have a good day, you happy feet kind of guy."
Neutral4.result = DONE
WilliamNeutral.addDialog(Neutral4, William)

def Neutral5 = [id:5]
Neutral5.npctext = "??? Beep beep!"
Neutral5.playertext = "hehehe. Don't worry. I was just kidding. I getcha. Your name is William. But listen, I don't get the beepity beep stuff. What are you saying?"
Neutral5.result = 6
WilliamNeutral.addDialog(Neutral5, William)

def Neutral6 = [id:6]
Neutral6.npctext = "b-beep beep BEEP 'well' beepers."
Neutral6.playertext = "'well'? Are you kidding me? There really *is* something going on in the well?"
Neutral6.result = 7
WilliamNeutral.addDialog(Neutral6, William)

def Neutral7 = [id:7]
Neutral7.npctext = "BEEP!"
Neutral7.playertext = "The well across town? Over near the north gate?"
Neutral7.result = 8
WilliamNeutral.addDialog(Neutral7, William)

def Neutral8 = [id:8]
Neutral8.npctext = "BEEPITY!"
Neutral8.playertext = "But what? You've got to help me out here, buddy!"
Neutral8.result = 9
WilliamNeutral.addDialog(Neutral8, William)

def Neutral9 = [id:9]
Neutral9.npctext = "*cough* Okay, listen and I'll make this quick. I loaned my lucky coin to another guard, Alma, yesterday. She says she forgot that it was mine and made a wish at either the town fountain or at the well. If you could find that coin and return it to me, I'd make it worth your while. Honest!"
Neutral9.playertext = "Wow! The penguin talks! What else can you say?"
Neutral9.result = 10
WilliamNeutral.addDialog(Neutral9, William)

def Neutral10 = [id:10]
Neutral10.npctext = "Beep! BEEP! beep?"
Neutral10.playertext = "Okay, okay. You're back to being a penguin and that's all I'm going to get. Gotcha."
Neutral10.options = []
Neutral10.options << [text:"I'll go hunt around in the fountain and well and see what I can find. No guarantees, but I'll look for your coin, William", quest: 53, result: DONE, exec: { event -> if( event.player.isOnQuest( 287, 4 ) ) { event.player.removeMiniMapQuestActorName( "BFG-William" ) }; event.player.addMiniMapQuestLocation( "City_Fountain", "BARTON_103", 525, 620, "Town Fountain" ); event.player.addMiniMapQuestLocation( "Town_Well", "BARTON_2", 700, 405, "Town Well" ) } ] //start the Lucky Coin quest
Neutral10.options << [text:"I'm sure you're a credit to the Guards, William, but I'm busy right now. You'll have to fish around for another helper.", result: DONE]
Neutral10.result = DONE
WilliamNeutral.addDialog(Neutral10, William)

//--------------------------------------------------------------------
//LUCKY COIN QUEST (INTERIM)                                          
//--------------------------------------------------------------------

def CoinInterim = William.createConversation("CoinInterim", true, "QuestStarted_53:2", "!QuestStarted_53:3", "!QuestStarted_34:2")

def Interim1 = [id:1]
Interim1.npctext = "Beep beep?"
Interim1.playertext = "Yeah, yeah. I know I haven't found your coin yet. I'll get it, I'll get it!"
Interim1.result = 2
CoinInterim.addDialog(Interim1, William)

def Interim2 = [id:2]
Interim2.npctext = "Beep!"
Interim2.playertext = "You're welcome."
Interim2.result = 3
CoinInterim.addDialog(Interim2, William)

def Interim3 = [id:3]
Interim3.playertext = "Sheesh. I must be losing it. I actually understood him that time!"
Interim3.result = DONE
CoinInterim.addDialog(Interim3, William)

//--------------------------------------------------------------------
//LUCKY COIN QUEST (SUCCESS)                                          
//--------------------------------------------------------------------

def CoinReturn = William.createConversation( "CoinReturn", true, "QuestStarted_53:3", "!QuestStarted_34:2" )

def Coin1 = [id:1]
Coin1.npctext = "Beep Beep BEEEEEEP!"
Coin1.playertext = "You seem kind of happy. I'm guessing you saw the coin in my hand, eh?"
Coin1.result = 2
CoinReturn.addDialog(Coin1, William)

def Coin2 = [id:2]
Coin2.npctext = "Beepity beepity!"
Coin2.playertext = "You got it, William. Here you go! One lucky coin, as promised!"
Coin2.result = 3
CoinReturn.addDialog(Coin2, William)

def Coin3 = [id:3]
Coin3.npctext = "Thanks! And...Carpe Carp!"
Coin3.playertext = "What? You spoke human again! But...'Carpe Carp'? I've heard of 'Carpe Diem', or 'Seize the Day', but what the heck does 'Carpe Carp' mean?"
Coin3.result = 4
CoinReturn.addDialog(Coin3, William)

def Coin4 = [id:4]
Coin4.npctext = "beepity!"
Coin4.playertext = "Okay, okay. I'll figure it out myself. Well, I'm glad I could help with your coin, and I'll keep my eye out for a seize-able carp."
Coin4.result = 5
CoinReturn.addDialog(Coin4, William)

def Coin5 = [id:5]
Coin5.npctext = "buh-beep!"
Coin5.playertext = "buh-bye to you too."
Coin5.quest = 53 //push the completion (and reward) for the Lucky Coin quest
Coin5.result = DONE
Coin5.exec = { event ->
	event.player.removeMiniMapQuestActorName( "BFG-William" )		
}
CoinReturn.addDialog(Coin5, William)

//--------------------------------------------------------------------
//LUCKY COIN QUEST (SUCCESS ALT) (from Cookie Quest)                  
//--------------------------------------------------------------------

def CoinReturnAlt = William.createConversation( "CoinReturnAlt", true, "QuestStarted_53:3", "Z01BridgeFromCookieQuest" )

def Coin1A = [id:1]
Coin1A.npctext = "Beep Beep BEEEEEEP!"
Coin1A.playertext = "You seem kind of happy. I'm guessing you saw the coin in my hand, eh?"
Coin1A.result = 2
CoinReturnAlt.addDialog(Coin1A, William)

def Coin2A = [id:2]
Coin2A.npctext = "Beepity beepity!"
Coin2A.playertext = "You got it, William. Here you go! One lucky coin, as promised!"
Coin2A.result = 3
CoinReturnAlt.addDialog(Coin2A, William)

def Coin3A = [id:3]
Coin3A.npctext = "Thanks! And...Carpe Carp!"
Coin3A.playertext = "What? You spoke human again! But...'Carpe Carp'? I've heard of 'Carpe Diem', or 'Sieze the Day', but what the heck does 'Carpe Carp' mean?"
Coin3A.result = 4
CoinReturnAlt.addDialog(Coin3A, William)

def Coin4A = [id:4]
Coin4A.npctext = "beepity!"
Coin4A.playertext = "Okay, okay. I'll figure it out myself. Well, I'm glad I could help with your coin, and I'll keep my eye out for a siezable carp."
Coin4A.result = 5
CoinReturnAlt.addDialog(Coin4A, William)

def Coin5A = [id:5]
Coin5A.npctext = "buh-beep!"
Coin5A.playertext = "buh-bye to you too."
Coin5A.quest = 53 //push the completion (and reward) for the Lucky Coin quest
Coin5A.result = DONE
Coin5A.exec = { event ->
	event.player.removeMiniMapQuestActorName( "BFG-William" )		
}
CoinReturnAlt.addDialog(Coin5A, William)

//--------------------------------------------------------------------
//OLIVIA'S COOKIE Quest (William's Part)                              
//--------------------------------------------------------------------
def WilliamCookies = William.createConversation("WilliamCookies", true, "QuestStarted_34:2", "!william133")

def Cookies1 = [id:1]
Cookies1.playertext = "Hello there...ummm...Guard?"
Cookies1.result = 2
WilliamCookies.addDialog(Cookies1, William)

def Cookies2 = [id:2]
Cookies2.npctext = "Beep."
Cookies2.playertext = "Beep, eh?"
Cookies2.options = []
Cookies2.options << [text:"I...see. So you're a penguin?", result: 3]
Cookies2.options << [text:"Are you a car? A dog's squeak toy? What?", result: 7]
Cookies2.options << [text:"Oh sorry. I was looking for a guard named William.", result: 9]
WilliamCookies.addDialog(Cookies2, William)

def Cookies3 = [id:3]
Cookies3.npctext = "Beep!"
Cookies3.playertext = "I'll take that as a yes. Aren't you a bit far from Antarctica?"
Cookies3.result = 4
WilliamCookies.addDialog(Cookies3, William)

def Cookies4 = [id:4]
Cookies4.npctext = "...beep."
Cookies4.playertext = "Oh sorry. Being that far from home must make you sad. Here, have a cookie."
Cookies4.result = 5
WilliamCookies.addDialog(Cookies4, William)

def Cookies5 = [id:5]
Cookies5.npctext = "Beep beep!"
Cookies5.playertext = "Two for 'no', eh? Okay, are you sure? Leon's mom made them special for you!"
Cookies5.result = 6
WilliamCookies.addDialog(Cookies5, William)

def Cookies6 = [id:6]
Cookies6.npctext = "BEEP! BEEP!"
Cookies6.playertext = "Heh. Okay. I'll try to pawn them off on someone else then. You're pretty wise...for a penguin."
Cookies6.quest = 34
Cookies6.exec = { event ->
	event.player.setQuestFlag( GLOBAL, "Z1WilliamGoodbye" )
	event.player.removeMiniMapQuestActorName( "BFG-William" )

	//if all three deliveries have been made, then push to the convo that says "take Olivia's plate back to her"
	if( event.player.hasQuestFlag( GLOBAL, "Z1JohnGoodbye" ) && event.player.hasQuestFlag( GLOBAL, "Z1MarthaGoodbye" ) && event.player.hasQuestFlag( GLOBAL, "Z1WilliamGoodbye" ) ) {
		William.pushDialog( event.player, "WilliamGoodbye" )
		//event.player.updateQuest( 34, "BFG-William" ) //push completion of William's portion of Olivia's Cookies quest
	}

	//if the player hasn't delivered ALL the cookies, but is on the last leg of the LUCKY COIN quest, then do that conversation after you deliver the cookies to William
	if( ( !event.player.hasQuestFlag( GLOBAL, "Z1JohnGoodbye" ) || !event.player.hasQuestFlag( GLOBAL, "Z1MarthaGoodbye" ) ) && event.player.isOnQuest( 53, 3 ) ) {
		event.player.setQuestFlag( GLOBAL, "Z01BridgeFromCookieQuest" )
		William.pushDialog( event.player, "CoinReturnAlt" )
		//event.player.updateQuest( 34, "BFG-William" ) //push completion of William's portion of Olivia's Cookies quest
	}
}
Cookies6.result = DONE
WilliamCookies.addDialog(Cookies6, William)

def Cookies7 = [id:7]
Cookies7.npctext = "BEEP BEEP!"
Cookies7.playertext = "hehehe. Okay. I'm just kidding. You're a penguin. I get it. But why would a guard be a Penguin?"
Cookies7.result = 8
WilliamCookies.addDialog(Cookies7, William)

def Cookies8 = [id:8]
Cookies8.npctext = "Beep beep beep BEEP beep beepity beep beeper Beepbeepbeep. Beep?"
Cookies8.playertext = "Ummm...yeah. Right. I got ALL of that. Sure. Yeah. Ummm, want a cookie?"
Cookies8.result = 6
WilliamCookies.addDialog(Cookies8, William)

def Cookies9 = [id:9]
Cookies9.npctext = "BEEP! I *am* William."
Cookies9.playertext = "And you're a penguin because..."
Cookies9.result = 10
WilliamCookies.addDialog(Cookies9, William)

def Cookies10 = [id:10]
Cookies10.npctext = "Well, I *like* penguins. beep beep beepbeepbeep!"
Cookies10.playertext = "Oh, right. Of course. Silly me! A Town Guard that's a penguin. That's totally normal!"
Cookies10.result = 11
WilliamCookies.addDialog(Cookies10, William)

def Cookies11 = [id:11]
Cookies11.npctext = "Beep!"
Cookies11.playertext = "I see. William the Penguin. Well...that's a mighty fine bill you've got there, Bill." 
Cookies11.result = 12
WilliamCookies.addDialog(Cookies11, William)

def Cookies12 = [id:12]
Cookies12.npctext = "beeeeep..."
Cookies12.playertext = "I know, I know. hehehe. Okay, Olivia sent me. Care for a cookie?"
Cookies12.result = 6
WilliamCookies.addDialog(Cookies12, William)

//--------------------------------------
//WILLIAM'S GOODBYE                     
//Remind the player to go back to Olivia
//--------------------------------------
def WilliamGoodbye = William.createConversation("WilliamGoodbye", true, "!QuestCompleted_34", "Z1JohnGoodbye", "Z1MarthaGoodbye", "Z1WilliamGoodbye")

def Goodbye1 = [id:1]
Goodbye1.npctext = "beep beep beepity beepity beep"
Goodbye1.playertext = "You're doing it again. You know I don't speak 'beep'."
Goodbye1.result = 2
WilliamGoodbye.addDialog(Goodbye1, William)

def Goodbye2 = [id:2]
Goodbye2.npctext = "Beep. Platebeep, beeper Olivia. Beep?"
Goodbye2.playertext = "Take the plate back to Olivia? I'm worried that I'm starting to understand you, but okay...gotcha, penguin boy."
Goodbye2.result = DONE
Goodbye2.exec = { event ->
	event.player.addMiniMapQuestActorName( "Olivia-VQS" )
	if( event.player.isOnQuest( 53, 3 ) ) {
		William.pushDialog( event.player, "CoinReturn" )
	}
}
WilliamGoodbye.addDialog(Goodbye2, William)

//--------------------------------------------------------------------
//LOOPING AFTERMATH DIALOG                                            
//--------------------------------------------------------------------

def WilliamAftermath = William.createConversation("WilliamAftermath", true, "QuestCompleted_53", "QuestCompleted_34")

def After1 = [id:1]
After1.npctext = "Beep! beepBEEPbeepBEEPbeep!"
After1.playertext = "I'm glad to see you too, William. I hope things are going well for you!"
After1.result = 2
WilliamAftermath.addDialog(After1, William)

def After2 = [id:2]
After2.npctext = "beep n beep"
After2.playertext = "Well, I guess that's the life of a guard. Mostly boredom, punctuated with bits of terrifying action."
After2.result = 3
WilliamAftermath.addDialog(After2, William)

def After3 = [id:3]
After3.npctext = "beep beepity beep!"
After3.playertext = "Yeah, you know it! Have a good one, William!"
After3.result = 4
WilliamAftermath.addDialog(After3, William)

def After4 = [id:4]
After4.npctext = "beepers!"
After4.result = DONE
WilliamAftermath.addDialog(After4, William)


