import com.gaiaonline.mmo.battle.script.*;

// there are 3 different small example/testing scripts in here.

// (1) QUICK TEST SCRIPT
/*
myManager.onEnter( myRooms.BARTON_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z01TESTTESTTEST" )
		event.actor.say( "I just set the Z01TESTTESTTEST flag." )
	}
}

myManager.onExit( myRooms.BARTON_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z01TESTTESTTEST" )
		event.actor.say( "I just UNset the Z01TESTTESTTEST flag." )
	}
}
*/

// (2) MINI-EVENT SPEC TEST SCRIPT
/*
// mini event room combat listener
// compare to onCombat, below.
myManager.onRoomCombat( myRooms.BARTON_1 ) { event ->
	if( event.eventType == com.gaiaonline.mmo.battle.event.Events.COMBAT_HELP )
	{
		println "onRoomCombat, helping: " + event
	}
	if( event.eventType == com.gaiaonline.mmo.battle.event.Events.COMBAT_HARM )
	{
		println "onRoomCombat: harmed: " + event
	}
	
	if( event.effect.stat == com.gaiaonline.mmo.battle.ring.StatusEffect.Stat.DMG )
	{
		println "damage for " + event.effect.amount + " points"
	}
	if( event.effect.stat == com.gaiaonline.mmo.battle.ring.StatusEffect.Stat.SPD )
	{
		println "slowed for " + -event.effect.amount + " points"
	}
}
myManager.onAreaCombat( myRooms.BARTON_1.getArea() ) { event ->
	if( event.eventType == com.gaiaonline.mmo.battle.event.Events.COMBAT_HELP )
	{
		println "onAreaCombat, helping: " + event
	}
	if( event.eventType == com.gaiaonline.mmo.battle.event.Events.COMBAT_HARM )
	{
		println "onAreaCombat: harmed: " + event
	}

	if( event.effect.stat == com.gaiaonline.mmo.battle.ring.StatusEffect.Stat.DMG )
	{
		println "              damage for " + event.effect.amount + " points"
	}
	if( event.effect.stat == com.gaiaonline.mmo.battle.ring.StatusEffect.Stat.SPD )
	{
		println "              slowed for " + -event.effect.amount + " points"
	}
}
// mini event specification
miniEventSpec = makeMiniEventSpec(
		{ attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() > 2
		if( player == attacker && !allowed ) { player.centerPrint( "You cannot attack that monster right now" ); }
		return allowed } )

def spawner1 = myRooms.BARTON_1.spawnSpawner( "testMiniEventSpawner", "garlic", 1 )
spawner1.setMiniEventSpec( miniEventSpec )
spawner1.setPos( 1360, 290 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 10 , 11 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 2.4 )
g1 = spawner1.forceSpawnNow()
// compare to onRoomCombat, above.
myManager.onCombat( g1 ) { event ->
	println "onCombat " + event
}
*/

// (3) LOGIN EVENT SCRIPT EXAMPLES.
/*
myManager.onLogin() { event ->
	println "logged in Anywhere: " + event
}
myManager.onLogin( "Z01_Barton_Town" ) { event ->
	println "logged in to Barton Town: " + event
}
*/
