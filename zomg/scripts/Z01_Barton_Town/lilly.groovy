import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

def LillyNPC = spawnNPC("Lily-VQS", myRooms.BARTON_204, 100, 568)
LillyNPC.setRotation( 45 )
LillyNPC.setDisplayName( "Lily" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	LillyNPC.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/lily_strip.png")
}
//======================================
// RANDOM CONVERSATIONS                 
//======================================

def Random1 = LillyNPC.createConversation("Random1", true, "Z0LillyR1" )

def rand1 = [id:1]
rand1.npctext = "Hi there! Welcome to Barton Town!"
rand1.flag = "!Z0LillyR1"
rand1.exec = { event ->
	selectRandomFlag( event )
}
rand1.result = DONE
Random1.addDialog(rand1, LillyNPC)
//--------------------
def Random2 = LillyNPC.createConversation("Random2", true, "Z0LillyR2" )

def rand2 = [id:1]
rand2.npctext = "I wish I had a puppy. Of course, Rufus probably wouldn't like that very much."
rand2.flag = "!Z0LillyR2"
rand2.exec = { event ->
	selectRandomFlag( event )
}
rand2.result = DONE
Random2.addDialog(rand2, LillyNPC)
//--------------------
def Random3 = LillyNPC.createConversation("Random3", true, "Z0LillyR3" )

def rand3 = [id:1]
rand3.npctext = "If you're interested in knowing more about the Regulars, there are guards all over the place. Stop and say 'hi' to one!"
rand3.flag = "!Z0LillyR3"
rand3.exec = { event ->
	selectRandomFlag( event )
}
rand3.result = DONE
Random3.addDialog(rand3, LillyNPC)
//--------------------
def Random4 = LillyNPC.createConversation("Random4", true, "Z0LillyR4" )

def rand4 = [id:1]
rand4.npctext = "Isn't it a nice day? *Inside* the Town walls anyway. It's a bit scary otherwise..."
rand4.flag = "!Z0LillyR4"
rand4.exec = { event ->
	selectRandomFlag( event )
}
rand4.result = DONE
Random4.addDialog(rand4, LillyNPC)

//======================================
// LOGIC FLOW FOR RANDOM LIST           
//======================================

myManager.onEnter( myRooms.BARTON_204 ) { event ->
	if( isPlayer( event.actor) ) {
		convoFlag = random( randomFlag )
		event.actor.setQuestFlag( GLOBAL, convoFlag )
	}
}

myManager.onExit( myRooms.BARTON_204 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, convoFlag )
	}
}

randomFlag = []
randomFlag << "Z0LillyR1"
randomFlag << "Z0LillyR2"
randomFlag << "Z0LillyR3"
randomFlag << "Z0LillyR4"


def selectRandomFlag( event ) {
	lastFlag = convoFlag
	convoFlag = random( randomFlag )
	if( lastFlag == convoFlag ) {
		selectRandomFlag( event )
	}
	event.player.setQuestFlag( GLOBAL, convoFlag )
}
