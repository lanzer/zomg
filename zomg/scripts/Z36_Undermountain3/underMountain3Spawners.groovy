//Script created by gfern

import chest.createChest;
import chest.reward;
import spawner.createSpawner;
import difficulty.setLevel;

//-----------------------------------------------------
//Constants & Variable Init
//-----------------------------------------------------
monsterLevel = 12;
level = 10;
underDifficulty = 1;
kamilaAttackable = false;
kamilaRetreated = false;
areaInit = false;
questSpeech = false;
openDoor = false;

HATE_RADIUS = 500;
MIN_LEVEL = 11.5;
MAX_LEVEL = 11.5;

//-----------------------------------------------------
//Maps and lists
//-----------------------------------------------------
underMountain3Monsters = ["vampire_LT"];
underSpawnerList = [] as Set;
under3Monsters = [] as Set;
difficultyMap = [1:0, 2:0.5, 3:1.0];

//-----------------------------------------------------
//Level adjusting
//-----------------------------------------------------
l = new setLevel();

def checkLevel() {
	if(myArea.getAllPlayers().size() > 0) {
		player = random(myArea.getAllPlayers());
		underDifficulty = player.getTeam().getAreaVar("Z30_Undermountain", "Z30DeadmansDifficulty");
		if(!underDifficulty) { underDifficulty = 2 }

		player.getCrew().each() {
			if(it.getConLevel() + difficultyMap.get(underDifficulty) > level) {
				levelAdjust();
			}
		}
	}

	myManager.schedule(30) { checkLevel(); }
}

def levelAdjust() {
	level = l.setSpawnerLevel(MIN_LEVEL, MAX_LEVEL, underDifficulty, difficultyMap, player, underSpawnerList);
}

//-----------------------------------------------------
//Spawners
//-----------------------------------------------------
c = new createSpawner();
chest = new createChest();
r = new reward();

//Under 3
//Guards 1
spawner1_1 = c.createStoppedGuardSpawner(myRooms.Under3_1, "spawner1_1", "blood_bat", 1, 460, 410, "Under3_1", monsterLevel, HATE_RADIUS, 350);
spawner1_2 = c.createStoppedGuardSpawner(myRooms.Under3_1, "spawner1_2", "blood_bat", 1, 250, 390, "Under3_1", monsterLevel, HATE_RADIUS, 240);

lootTypeList = ["gold", "orb", "ring"];
lootRarityMap = ["gold":100, "orb":10, "common":20, "uncommon":5, "ring":1];
lootItemMap = ["gold":500..750, "orb":4..8, "common":[], "uncommon":[], "ring":["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"]]
diffMultMap = [1:["gold":0.5, "orb":0.5], 2:["gold":1, "orb":1], 3:["gold":3, "orb":2]];
spawner1_3 = chest.chestOrMonster(0..100, 0, 100, 80, myRooms.Under3_1, "spawner1_3", "mimic_monster", "mimic_chest", 1, 740, 260, "Under3_1", 1, 90, this);
def chestSpawn = spawner1_3.forceSpawnNow();
r.rewardPlayersDMS(chestSpawn, 1, diffMultMap, lootTypeList, lootRarityMap, lootItemMap, 9, 0..5, this);

//Spawn 1
spawner1_1.forceSpawnNow();
spawner1_2.forceSpawnNow();
//spawner1_4.forceSpawnNow();

//Guards 2
spawner2_1 = c.createStoppedGuardSpawner(myRooms.Under3_2, "spawner2_1", "blood_bat", 4, 350, 330, "Under3_2", monsterLevel, HATE_RADIUS, 240);
spawner2_2 = c.createStoppedGuardSpawner(myRooms.Under3_2, "spawner2_2", random(underMountain3Monsters), 1, 650, 350, "Under3_2", monsterLevel, HATE_RADIUS, 350);
spawner2_3 = c.createStoppedGuardSpawner(myRooms.Under3_2, "spawner2_3", "blood_bat", 4, 690, 240, "Under3_2", monsterLevel, HATE_RADIUS, 90);

//Spawn 2
spawner2_1.spawnAllNow();
spawner2_2.forceSpawnNow();
spawner2_3.spawnAllNow();

//Guards 3
spawner3_1 = c.createStoppedGuardSpawner(myRooms.Under3_3, "spawner3_1", random(underMountain3Monsters), 1, 90, 250, "Under3_3", monsterLevel, HATE_RADIUS, 120);
spawner3_2 = c.createStoppedGuardSpawner(myRooms.Under3_3, "spawner3_2", "blood_bat", 4, 270, 420, "Under3_3", monsterLevel, HATE_RADIUS, 240);
spawner3_3 = c.createStoppedGuardSpawner(myRooms.Under3_3, "spawner3_3", random(underMountain3Monsters), 1, 600, 380, "Under3_3", monsterLevel, HATE_RADIUS, 120);
spawner3_4 = c.createStoppedGuardSpawner(myRooms.Under3_3, "spawner3_4", "blood_bat", 4, 420, 280, "Under3_3", monsterLevel, HATE_RADIUS, 120);

//Spawn 3
under3Monsters << spawner3_1.forceSpawnNow();

under3Monsters << spawner3_2.forceSpawnNow();
under3Monsters << spawner3_2.forceSpawnNow();
under3Monsters << spawner3_2.forceSpawnNow();
under3Monsters << spawner3_2.forceSpawnNow();

under3Monsters << spawner3_3.forceSpawnNow();

under3Monsters << spawner3_4.forceSpawnNow();
under3Monsters << spawner3_4.forceSpawnNow();
under3Monsters << spawner3_4.forceSpawnNow();
under3Monsters << spawner3_4.forceSpawnNow();

//Run on death logic to start Kamila retreat
under3Monsters.clone().each() {
	runOnDeath(it) { event ->
		under3Monsters.remove(event.actor);
		if(under3Monsters.size() == 0) {
			kamilaRetreat();
		}
	}
}

//Kamila
kamila_spawner = c.createStoppedGuardSpawner(myRooms.Under3_3, "kamila_spawner", "kamila", 1, 470, 340, "Under3_3", monsterLevel, 0, 120);

def kamilaInvuln = [
	isAllowed : { attacker, target ->
		def player = isPlayer(attacker) ? attacker : target;
		def allowed = kamilaAttackable;
		if(player == attacker && allowed == false) { player.centerPrint("A shroud of dark energy surrounds Kamila") }
		return allowed;
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec;

kamila_spawner.setMiniEventSpec(kamilaInvuln);
kamila = kamila_spawner.forceSpawnNow();

//Kamila Retreat logic
def kamilaRetreat() {
	myArea.getAllPlayers().each() {
		if(it.isOnQuest(364, 4)) { questSpeech = true; it.updateQuest(364, "[NPC] Deva"); }
		if(it.isOnQuest(370) || it.isDoneQuest(370)) { openDoor == true; }
	}
	if(questSpeech == true) {
		kamila.shout("So, the fat man sends his little pawns to track me down?");
		myManager.schedule(5) {
			kamila.shout("Tell me, does he pay you as well as he once paid me?");
			
			myManager.schedule(5) {
				kamila.shout("Have you figured out yet that you are no more than a tool to him? Expendable as a hammer.");
					
				myManager.schedule(5) {
					kamila.shout("No matter, now that I have my army I will exact my revenge.");
							
					myManager.schedule(5) {
						kamila.shout("You tell the fat man I'm coming for him. No hired muscle will prevent my revenge or his death.");
								
						myManager.schedule(5) {
							kamila.shout("And then... I will turn my army loose on Gaia and rule with an iron fist!");
							
							myManager.schedule(5) {
								kamila.dispose();
								
								makeCryptDoor();
							}
						}
					}
				}
			}
		}
	} else {
		kamila.shout("Fools! My victory is assured. You will fall before me in defeat like countless others before.");
		myManager.schedule(5) { 
			kamila.shout("Turn back while you can. Should you dare to enter my crypt you will not live to regret it.");

			myManager.schedule(5) {
				kamila.dispose();

				makeCryptDoor();
			}
		}
	}
}

def makeCryptDoor() {
	doorToCrypt = makeGeneric("CryptEntrance", myRooms.Under3_3, 650, 130);
	doorToCrypt.setUsable(true);
	doorToCrypt.setRange(200);

	doorToCrypt.onUse { event ->
		def crewWarpAllowed = false
		event.player.getCrew().each {
			if( it.isDoneQuest(368) ) {
				crewWarpAllowed = true
			}
		}

		if(crewWarpAllowed) {
			event.player.warp("Under4_5", 1290, 90);
		}
	}
}

onZoneIn() { event ->
	if(areaInit == false) {
		checkLevel();
	}
}
