//Script created by gfern

import spawner.createSpawner;
import difficulty.setLevel;

//-----------------------------------------------------
//Constants & Variable Init
//-----------------------------------------------------
monsterLevel = 12.4;
level = 10;
underDifficulty = 1;
kamilaAttackable = false;

HATE_RADIUS = 1550;
MIN_LEVEL = 12;
MAX_LEVEL = 12;

BLOOD_BAT_ROUNDS = 8
BB_ROUND_SIZE = 6

KAMILA_DMG_INCREASE = 20 // [rc] Percent increase of her dmg by the end of the fight

difficultyMap = [1:0.5, 2:1.0, 3:1.5];
playersForWarp = [] as Set;

kamilaFight = []
kamilaFight << [percent:95, text:"Let me spill your blood onto these holy grounds!",attack:0]
kamilaFight << [percent:75, text:"This is the future! Gaia's doom is imminent!",attack:5]
kamilaFight << [percent:50, text:"Your blood sacrifice will fuel the future.",attack:10]
kamilaFight << [percent:10, text:"The Dark Energy! It is growing! Flooding my veins with ecstasy.",attack:17]
kamilaFight << [percent:5, text:"You ... you're killing me! But I am immortal!",attack:35]

//-----------------------------------------------------
//Level adjusting
//-----------------------------------------------------
l = new setLevel();

def checkLevel() {
	if(myArea.getAllPlayers().size() > 0) {
		player = random(myArea.getAllPlayers());
		underDifficulty = player.getTeam().getAreaVar("Z30_Undermountain", "Z30DeadmansDifficulty");
		
		if(!underDifficulty)
		{
			// happens when ya warp in for debugging
			underDifficulty = 2;
		}

		player.getCrew().each() {
			if(it.getConLevel() + difficultyMap.get(underDifficulty) > level) {
				levelAdjust();
			}
		}
	}

	myManager.schedule(30) { checkLevel(); }
}

def levelAdjust() {
	level = l.setSpawnerLevel(MIN_LEVEL, MAX_LEVEL, underDifficulty, difficultyMap, player, kamila_spawner);
}

//-----------------------------------------------------
//Spawners
//-----------------------------------------------------
creatorOfSpawners = new createSpawner();
kamila_spawner = creatorOfSpawners.createStoppedGuardSpawner(myRooms.Under5_1, "kamila_spawner", "kamila_boss", 1, 1282, 906, "Under5_1", monsterLevel, HATE_RADIUS, 120);

def kamilaInvuln = [
	isAllowed : { attacker, target ->
		def player = isPlayer(attacker) ? attacker : target;
		def allowed = kamilaAttackable;
		if(player == attacker && allowed == false) { player.centerPrint("A shroud of dark energy surrounds Kamila") }
		return allowed;
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec;

kamila_spawner.setMiniEventSpec(kamilaInvuln);
kamila = null;

def fight() {
	kamilaAttackable = true;
	myArea.getAllPlayers().each() { player ->
		freePlayer(player);
	}
	kamila.addHate(random(myArea.getAllPlayers()), 1);
}

onZoneIn() {
	checkStartAction();
}

kamilaSpawnNagle = null;

def synchronized checkStartAction() {
	checkLevel();

	if(!kamilaAttackable)
	{
		myArea.getAllPlayers().each() { player -> 
			controlPlayer(player);
		}
	}
	
	def firstPlayer = myArea.getAllPlayers().toList()[0];
	
	if(null == kamila)
	{
		if(null != kamilaSpawnNagle) {
			kamilaSpawnNagle.cancel()
		}
		kamilaSpawnNagle = myManager.schedule(10) {
			spawnKamila()
		}
	}
}

def enableExitCrystal()
{
	def onUnder5Door = { event ->
		event.player.warp("M1_1", random(1925, 2450), random(1525, 1700), 6);
	}
	def under5Door = makeGeneric("under5Door", myRooms.Under5_1, 200, 700);
	under5Door.setUsable(true);
	under5Door.setRange(200);
	under5Door.onUse(onUnder5Door);
}

bloodBatSpawners = []
bloodBatRoundPercents = []
(BLOOD_BAT_ROUNDS..1).each {
	def percent = Math.round(100/(BLOOD_BAT_ROUNDS+1)*it)
	bloodBatRoundPercents << percent
	bloodBatSpawners <<(creatorOfSpawners.createStoppedSpawner(myRooms.Under5_1, "blood_bat_${percent}", "blood_bat", BB_ROUND_SIZE, 1530, 916, "Under5_1", monsterLevel, HATE_RADIUS))
}

//log("Blood bat rounds at {}", bloodBatRoundPercents)
//log("Blood bat spawners named {}", bloodBatSpawners*.getName())

spawnBloodBats = { health ->
	nextRound = bloodBatRoundPercents[0]
	if(nextRound && health.getPercentageOfMaxHP()<=nextRound) {
		bloodBatRoundPercents -= nextRound
		def spawner = bloodBatSpawners.pop()
		log("Spawning bats round at ${nextRound} with {}.  Remaining list: {}", spawner, bloodBatRoundPercents)
		spawner?.spawnAllNow()
	}

	nextShout = kamilaFight[0]
	if(nextShout && health.didTransition(nextShout.percent)) {
		kamilaFight -= nextShout
		kamila.shout(nextShout.text)
		if(nextShout.attack) {
			//log("Setting Kamila's attack at {} percent bonus", nextShout.attack)
			kamila.setAttackMultiplier(nextShout.attack/100f+1f)
		}
	}
}

myManager.onEnter(myRooms.Under5_1) { event ->
	if(isMonster(event.actor) && event.actor.getMonsterType()=="blood_bat") {
		myArea.getAllPlayers().each {
			event.actor.addHate(it, 10)
			//log("adding hate of {} to {}", it, event.actor)
		}
	}
}

def spawnKamila()
{
	kamila = kamila_spawner.forceSpawnNow();

	myManager.onHealth(kamila, spawnBloodBats)

	
	runOnDeath(kamila) { event ->
		kamila.shout("Oooh yes, I understand now! My blood will be the sacrfice, it just brings your doom closer! I will be one with the darkness... and soon, so will YOU!")
		myArea.getAllPlayers().each() {
			if(it.isOnQuest(370, 3)) {
				it.updateQuest(370, "[NPC] Alastor");
			}
			
			it.setQuestFlag( GLOBAL, "Z38KilledKamila") // used by the twins in BF1_705
	
		}
		
		enableExitCrystal();
	}
	
	myManager.schedule(10) {
		kamila.shout("Welcome to the temple of darkness and pain!")
	}
	
	myManager.schedule(15) { fight(); }
}

